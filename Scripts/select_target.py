import localizable
import sys
import os
import shutil
import zipfile

def selectFile(target, filename):
    if os.path.isdir(filename):
        shutil.rmtree(filename)
        shutil.copytree(filename + '_' + target, filename)
    else:
        os.remove(filename)
        shutil.copy2(filename + '_' + target, filename)


if __name__ == '__main__':

  #parse the params
    usage = """
    This script is used for selecting the android build target

    Usage: python select_target HOME|PRO

    """

    if len(sys.argv) < 2:
        print usage
        sys.exit(1)
    else:
        try:
            target = sys.argv[1]
            androidAppPath = '../proj.android-studio/app/'
            appname = ''

            if os.path.isdir(androidAppPath + 'build'):
                shutil.rmtree(androidAppPath + 'build')

            if target == 'HOME':
                appname = 'IKAWA Home'

            elif target == 'PRO':
                appname = 'IKAWA Pro'

            else:
                raise Exception('Invalid target: ' + target + '\nUse HOME or PRO')
            
            selectFile(target, androidAppPath + "jni/Application.mk")
            selectFile(target, androidAppPath + "google-services.json")
            selectFile(target, androidAppPath + "res")

        except Exception as e:
            print e
            sys.exit(1)

