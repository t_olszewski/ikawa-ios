#!/usr/bin/env python
# encoding: utf-8
"""
untitled.py

Created by Tijn Kooijmans on 2012-03-23.
Copyright (c) 2012 __MyCompanyName__. All rights reserved.
"""

import sys
import os
import xlrd
import codecs
import time
import localizable

def parseSheet(sheetPro, sheetHome, sheetAwaki, fileIOSPro, fileIOSHome, fileIOSAwaki, fileAndroidPro, fileAndroidHome, fileAndroidAwaki, locale):
	
	tags_pro = {}
	tags_home = {}
	tags_awaki = {}

	for r in range(sheetPro.nrows)[1:]:
		row = sheetPro.row(r)
		key = row[0].value
		value = row[1].value
		if (key):
			if (value):
				value = value.replace("\"","\\\"")
				
				#translators sometimes screw up my placeholders, so fix it:
				value = value.replace('d%','%d')
				value = value.replace('@%','%@')
				
				tags_pro[key] = value
				tags_home[key] = value
	
	#override the home tags
	for r in range(sheetHome.nrows)[1:]:
		row = sheetHome.row(r)
		key = row[0].value
		value = row[1].value
		if (key):
			if (value):
				value = value.replace("\"","\\\"")
				
				#translators sometimes screw up my placeholders, so fix it:
				value = value.replace('d%','%d')
				value = value.replace('@%','%@')
				
				tags_home[key] = value
	
	#override the awaki tags
	for r in range(sheetAwaki.nrows)[1:]:
		row = sheetAwaki.row(r)
		key = row[0].value
		value = row[1].value
		if (key):
			if (value):
				value = value.replace("\"","\\\"")
				
				#translators sometimes screw up my placeholders, so fix it:
				value = value.replace('d%','%d')
				value = value.replace('@%','%@')
				
				tags_awaki[key] = value
	
	#write the pro language file for iOS
	for key in tags_pro:
		fileIOSPro.write("\"%s\" = \"%s\";\r\n" % (key, tags_pro[key]))

	#write the home language file for iOS
	for key in tags_home:
		fileIOSHome.write("\"%s\" = \"%s\";\r\n" % (key, tags_home[key]))

	#write the awaki language file for iOS
	for key in tags_awaki:
		fileIOSAwaki.write("\"%s\" = \"%s\";\r\n" % (key, tags_awaki[key]))

	#write the pro language file for Android
	fileAndroidPro.write('<resources>\n)')
	for key in tags_pro:
		if not key.startswith("$"):
			continue
		value = tags_pro[key]		
		if value.startswith("@"):
			value = "\\" + value
		value = value.replace("\'", "\\\'")
		value = value.replace("&", "&amp;")
		fileAndroidPro.write('\t<string name="' + key + '" formatted="false">' + value + '</string>\n')	
	fileAndroidPro.write('\t<string name="app_name" formatted="false">IKAWA Pro</string>\n')
	if locale == 'en':
		fileAndroidPro.write('\t<string name="scheme" formatted="false" translatable="false">ikawapp</string>\n')	
	fileAndroidPro.write('</resources>\n')

	#write the home language file for Android
	fileAndroidHome.write('<resources>\n')
	for key in tags_home:
		if not key.startswith("$"):
			continue
		value = tags_home[key]		
		if value.startswith("@"):
			value = "\\" + value
		value = value.replace("\'", "\\\'")
		value = value.replace("&", "&amp;")
		fileAndroidHome.write('\t<string name="' + key + '" formatted="false">' + value + '</string>\n')	
	fileAndroidHome.write('\t<string name="app_name" formatted="false">IKAWA Home</string>\n')	
	if locale == 'en':
		fileAndroidHome.write('\t<string name="scheme" formatted="false" translatable="false">ikawahome</string>\n')		
	fileAndroidHome.write('</resources>\n')

	#write the awaki language file for Android
	fileAndroidAwaki.write('<resources>\n')
	for key in tags_awaki:
		if not key.startswith("$"):
			continue
		value = tags_awaki[key]
		if value.startswith("@"):
			value = "\\" + value
		value = value.replace("\'", "\\\'")
		value = value.replace("&", "&amp;")
		fileAndroidAwaki.write('\t<string name="' + key + '" formatted="false">' + value + '</string>\n')
	fileAndroidAwaki.write('\t<string name="app_name" formatted="false">IKAWA Awaki</string>\n')
	if locale == 'en':
		fileAndroidAwaki.write('\t<string name="scheme" formatted="false" translatable="false">ikawaawaki</string>\n')
	fileAndroidAwaki.write('</resources>\n')
	
def languageParse(excelBestand, locale):
	
	print "Parsing %s" % excelBestand

	if not os.path.isdir("export/PRO/" + locale + ".lproj"):
		os.makedirs("export/PRO/" + locale + ".lproj")
		os.makedirs("export/HOME/" + locale + ".lproj")
		os.makedirs("export/AWAKI/" + locale + ".lproj")
		os.makedirs("export/PRO/values-" + locale)
		os.makedirs("export/HOME/values-" + locale)
		os.makedirs("export/AWAKI/values-" + locale)

	fileIOSPro = codecs.open("export/PRO/" + locale + ".lproj/Localizable.strings", "w", "utf-16")
	fileIOSHome = codecs.open("export/HOME/" + locale + ".lproj/Localizable.strings", "w", "utf-16")
	fileIOSAwaki = codecs.open("export/AWAKI/" + locale + ".lproj/Localizable.strings", "w", "utf-16")

	fileAndroidPro = codecs.open("export/PRO/values-" + locale + "/strings.xml", "w", "utf-16")
	fileAndroidHome = codecs.open("export/HOME/values-" + locale + "/strings.xml", "w", "utf-16")
	fileAndroidAwaki = codecs.open("export/AWAKI/values-" + locale + "/strings.xml", "w", "utf-16")

	book = xlrd.open_workbook(excelBestand)

	parseSheet(book.sheet_by_name("PRO"), book.sheet_by_name("HOME"), book.sheet_by_name("AWAKI"), fileIOSPro, fileIOSHome, fileIOSAwaki, fileAndroidPro, fileAndroidHome, fileAndroidAwaki, locale)
		
	fileIOSPro.flush()
	fileIOSHome.flush()
	fileIOSAwaki.flush()
	fileAndroidPro.flush()
	fileAndroidHome.flush()
	fileAndroidAwaki.flush()

	fileIOSPro.close()
	fileIOSHome.close()
	fileIOSAwaki.close()
	fileAndroidPro.close()
	fileAndroidHome.close()
	fileAndroidAwaki.close()

def usage():
    print "usage: <excelfile> <locale>"

def main(argv=None):
    if argv is None:
        argv = sys.argv
        			
	languageParse(argv[1], argv[2])

if __name__ == "__main__":
    sys.exit(main())
