//
//  IKActivityItemProvider.swift
//  IKAWApp
//
//  Created by Admin on 7/9/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class IKActivityItemProvider: UIActivityItemProvider {
    var profileUrl: String?
    var mailString: String?
    var mailSubject: String?

    public override var item: Any { // customise the item for the current `activityType`
        
        switch activityType! {
        case UIActivity.ActivityType.mail:
            return mailString ?? ""
        default:
            return ((placeholderItem as? String) ?? "") + "\n" + (profileUrl ?? "")
        }
    }
    
    override func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return mailSubject ?? ""
    }
}
