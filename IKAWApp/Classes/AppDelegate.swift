//
//  AppDelegate.swift
//  IKAWApp
//
//  Created by Admin on 2/26/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import CoreData
import IKRoasterLib
import SideMenu
import Firebase
import FirebaseMessaging
import UserNotifications
import Toast_Swift
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var pollTimer: Timer?
    var bgTask:UIBackgroundTaskIdentifier?
    let gcmMessageIDKey = "gcm.message_id"
    private var useFahrenheit: Bool = {return USE_FAHRENHEIT}()
    private var setClosedLoopFan: Bool = UserDefaults.standard.bool(forKey: "setClosedLoopFan")
    private var setOpenLoopFan: Bool = UserDefaults.standard.bool(forKey: "setOpenLoopFan")

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //        for familyName in UIFont.familyNames {
        //            print(UIFont.fontNames(forFamilyName: familyName))
        //        }
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        #if !(TARGET_DEV)
        Thread.sleep(forTimeInterval: 5.0)
        #endif
        let notificationTypes: UIUserNotificationType
        notificationTypes = [.alert , .sound]
        let newNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
        UIApplication.shared.registerUserNotificationSettings(newNotificationSettings)
        
        if (IK_TWO_SENSOR_SUPPORT) {
            UserDefaults.standard.set(true, forKey: "useSensorBelowId")
        }
        
        #if DEBUG
        UserDefaults.standard.set(true, forKey: "twoSensorSupported")
        #endif
        
        #if TARGET_HOME
        UITabBar.appearance().layer.borderWidth = 0.0
        UITabBar.appearance().clipsToBounds = true
        UITabBar.appearance().tintColor = kColorRed
        
        var style = ToastStyle()
        style.activityBackgroundColor = kColorRed.withAlphaComponent(0.7)
        ToastManager.shared.style = style
        
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuWidth = UIDevice.current.userInterfaceIdiom == .pad ? 300 : (window?.frame.width ?? 375) * 0.85
        SideMenuManager.default.menuAnimationBackgroundColor = UIColorFromRGB(89, g: 90, b: 87)
        SideMenuManager.default.menuAnimationFadeStrength = 0.32
       // UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
        SideMenuManager.default.menuFadeStatusBar = false
        #else
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuWidth = UIDevice.current.userInterfaceIdiom == .pad ? 300 : (window?.frame.width ?? 375) * 0.76
        SideMenuManager.default.menuAnimationBackgroundColor = UIColorFromRGB(33, g: 33, b: 33)
        SideMenuManager.default.menuAnimationFadeStrength = 0.7
        SideMenuManager.default.menuShadowColor = .black
        SideMenuManager.default.menuShadowRadius = 15
        SideMenuManager.default.menuShadowOpacity = 1
        //UIApplication.shared.statusBarView?.backgroundColor = UIColorFromRGB(0, g: 0, b: 0)
        /*
        if let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView {
            statusBar.backgroundColor = kColorDarkGrey1
        }*/
        #endif
        AppCore.sharedInstance.firebaseManager.firebaseInit()
        AppCore.sharedInstance.settingsManager.populateSettings()
        
        if let user = AppCore.sharedInstance.firebaseManager.currentUser, user.isEmailVerified {
            AppCore.sharedInstance.loggingManager.sendDevice()
            AppCore.sharedInstance.loggingManager.sendProfiles()
            AppCore.sharedInstance.loggingManager.logAllRoasts()
            #if TARGET_PRO
            #else
            AppCore.sharedInstance.loggingManager.trackEvent(name: "App Start", label: "")
            #endif
        }
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            if let savedAppVersion = UserDefaults.standard.object(forKey: "appVersion") as? String, appVersion == savedAppVersion {} else {
                AppCore.sharedInstance.firebaseManager.firebaseLogout()
                #if TARGET_PRO
                AppCore.sharedInstance.cropsterManager.logOut()
                AppCore.sharedInstance.syncManager.migrateRoastLogImages()
                #endif
                UserDefaults.standard.set(appVersion, forKey: "appVersion")
            }
        }
        

        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        // [END register_for_notificationzs]
        
        //delete
        //UserDefaults.standard.set(true, forKey: "forceFirmwareUpdateId")
//        var profile = AppCore.sharedInstance.profileManager.base64DecodeProfile(profile: "CAESECJScYybwkiiqXgQ1DvKm1EaFklLQVdBIC0gU2FtcGxlIFJvYXN0IDEiBQgAEOIKIgYIhAcQ9wwiBgjFDxDbDiIGCL4WENsPIgYIvR0QvhAqBQgAEMoBKgYInAYQygEqBgiNChDKASoGCI8OEMEBKgYInRUQrwEqBgi9HRCZATACOgYI5iYQsgE=")
//                profile?.coffeeName = "coffee name"
//                let encodeStr = IKProfileManager.sharedInstance.base64EncodeProfile(profile: profile!)
//                profile = IKProfileManager.sharedInstance.base64DecodeProfile(profile: encodeStr!)
        
        
//                let url = URL(string: "ikawahome://profile/?CAESEFtfALXNI04JqT0SvAXWVpoaF0lLQVdBIC0gU2FtcGxlIFJvYXN0IDIgIgUIABDbCyIGCI0BEPcIIgYIlAQQ2woiBgjvBhCSDCIGCIMMEI4OIgYIsREQ0w8iBgiWFxDOECoFCAAQvAEqBgiWFxCvATACOgYI9CAQzQE=")
//                handleOpenUrl(url: url!)
        
        if IK_SIMULATE_ROASTER {
            IKSimulatorManager.sharedInstance
        }
        
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        #if TARGET_PRO
        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_APP_LAUNCH", label: "")
        #endif
        
        if AppCore.sharedInstance.settingsManager.workingWithoutAccount {
            NavigationHelper.GoToMainScreen()
            window!.makeKeyAndVisible()
            IKNotificationManager.sharedInstance.isWaiting = false
//
//            let url = URL(string: "ikawahome://profile/?CAESEDDVk7+hW0izvhP5vym+WAIaHUlLQVdBIC0gR2Vpc2hhIFBvdXJvdmVyIElubGV0IgUIABDXDyIGCM8DENIQIgYI7AUQnhEiBgiyCxCeEiIGCLUOELESIgYI0REQxhIiBgi6FBDREiIGCPcWEOUSKgUIABDJASoGCPcWEKoBMAE6BgiKIRDQAUImChxlVHpjNk5hdVJxY0dvamlHV0ZVQXJ3Z25acUMzEgAaACIAKAA=")
//            handleOpenUrl(url: url!)
            
            
        } else if !UserDefaults.standard.bool(forKey: "tutorialWasShowed") {
            if let _ = AppCore.sharedInstance.firebaseManager.currentUser {
                AppCore.sharedInstance.firebaseManager.firebaseLogout()
            }
            NavigationHelper.GoToLoginScreen()
            self.window!.makeKeyAndVisible()
            IKNotificationManager.sharedInstance.isWaiting = false
        } else if let user = AppCore.sharedInstance.firebaseManager.currentUser {
            if user.isAnonymous || !user.isEmailVerified {
                NavigationHelper.GoToLoginScreen()
                window!.makeKeyAndVisible()
                IKNotificationManager.sharedInstance.isWaiting = false
                return true
            } else {
                #if TARGET_PRO
                if NetworkReachabilityManager()?.isReachable ?? false {
                    AppCore.sharedInstance.syncManager.getUserInfo { (userInfo) in
                        if (userInfo?.accountName ?? "").isEmpty || ((userInfo?.userType ?? 0) == 0) {
                            NavigationHelper.GoToUpdateAccountScreen()
                            self.window!.makeKeyAndVisible()
                            IKNotificationManager.sharedInstance.isWaiting = false
                            //return true
                        } else {
                            NavigationHelper.GoToMainScreen()
                            AppCore.sharedInstance.syncManager.startProfileSynchronization()
                            self.window!.makeKeyAndVisible()
                            IKNotificationManager.sharedInstance.isWaiting = false
                            //return true
                        }
                    }
                } else {
                    NavigationHelper.GoToMainScreen()
                    AppCore.sharedInstance.syncManager.startProfileSynchronization()
                    self.window!.makeKeyAndVisible()
                    IKNotificationManager.sharedInstance.isWaiting = false
                }

                #elseif TARGET_HOME
                NavigationHelper.GoToMainScreen()
                #endif
            }
        } else {
            NavigationHelper.GoToLoginScreen()
            window!.makeKeyAndVisible()
            IKNotificationManager.sharedInstance.isWaiting = false
            //return true
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        setClosedLoopFan = UserDefaults.standard.bool(forKey: "setClosedLoopFan")
        setOpenLoopFan = UserDefaults.standard.bool(forKey: "setOpenLoopFan")
        useFahrenheit = USE_FAHRENHEIT
        if let roaster = RoasterManager.instance().roaster {
            if (roaster.status.state != IK_STATUS_IDLE) {
               
                 bgTask = application.beginBackgroundTask(withName: "pollRoasterTask") {
                    // Clean up any unfinished task business by marking where you
                    // stopped or ending the task outright.
                    application.endBackgroundTask(self.bgTask!)
                    self.bgTask = UIBackgroundTaskIdentifier.invalid
                }
                pollTimer = Timer(timeInterval: 1, target: self, selector: #selector(pollRoaster), userInfo: nil, repeats: true)
            }
        }
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        print(options)
        handleOpenUrl(url: url)
        return false
    }
    
    func registerPushNotifications() {
       UIApplication.shared.registerForRemoteNotifications()
    }
    
    func unregisterPushNotifications() {
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    func handleOpenUrl(url: URL) {
        if (url.absoluteString.range(of: "/profile") != nil) {
            if let query = url.query {
                if (query.count == 32) {
                    //query contains uuid
                    //TODO: implement!
                } else {
                    guard let profile = AppCore.sharedInstance.profileManager.base64DecodeProfile(profile: query) else {
                        let confirmationDialogView = ButtonsPopupModalView()
                        confirmationDialogView.layout = .vertical
                        confirmationDialogView.title = NSLocalizedString("$profile_format_unsupported", comment: "")
                        confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                        IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                        return
                    }
                    print(profile.name)
                    if profile.type == ProfileTypeUndefined {
                        profile.type = ProfileTypeUser
                    }
                    AddReceiptManager.sharedInstance.createProfile(profile: profile, editMode: false)
                }
            }
        }
            
        else {
            #if TARGET_PRO
            if let range = url.absoluteString.range(of: "/cropster/") {
                let substring = url.absoluteString[range.upperBound..<url.absoluteString.endIndex]
                let  splitArr = substring.split(separator: "/")
                if  splitArr.count == 2 {
                    AppCore.sharedInstance.cropsterManager.setCredentials(key: String(splitArr.first!), secret: String(splitArr.last!))
                }
            }
            #endif
        }
        
        
    }
    
    func openProfile(profile: Profile) {
        #if TARGET_HOME
        NavigationHelper.openProfile(profile: profile)
        #else
        let accountStoryboard = UIStoryboard(name: "ProfileLib", bundle: nil)
        let profileNC = accountStoryboard.instantiateInitialViewController() as! UINavigationController
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = profileNC
        profileNC.topViewController?.performSegue(withIdentifier: "Chart", sender: profile)
        #endif
    }
    
    @objc func pollRoaster() {
        if let roaster = RoasterManager.instance().roaster {
            if (roaster.status.state != IK_STATUS_IDLE) {

            } else {
               pollTimer?.invalidate()
               UIApplication.shared.endBackgroundTask(bgTask!)
                bgTask = UIBackgroundTaskIdentifier.invalid
            }
        }
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        
        if setClosedLoopFan != UserDefaults.standard.bool(forKey: "setClosedLoopFan") && !setClosedLoopFan{
            #if TARGET_PRO
            AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_SETTING_CLOSEDLOOP", label: "")
            #endif
        }
        
        if setOpenLoopFan != UserDefaults.standard.bool(forKey: "setOpenLoopFan") && !setOpenLoopFan {
            #if TARGET_PRO
            AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_SETTING_OPENLOOP", label: "")
            #endif
        }
        
        if useFahrenheit != USE_FAHRENHEIT {
            #if TARGET_PRO
            if !useFahrenheit {
                AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_SETTING_TOGGLE_DEG_F", label: "")
            }
            #endif
            NotificationCenter.default.post(name: Notification.Name.DegreeCelsiusChanged, object: nil)
        }
        
        UserDefaults.standard.set(!UserDefaults.standard.bool(forKey: "forceFirmwareUpdateId"), forKey: "repressFirmwareUpdateMessage")
        AppCore.sharedInstance.loggingManager.sendDevice()
        AppCore.sharedInstance.loggingManager.logAllRoasts()
        //in case firmware update is selected from settings this will start the update.
        if ((IKInterface.instance().isConnected() || IK_SIMULATE_ROASTER) && AppCore.sharedInstance.roastManager.currentRoast == nil) {
            RoasterManager.instance().requestRoasterData()
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if let errorHandler = AppCore.sharedInstance.machineErrorNotificationHandler.lastHandler {
            errorHandler.processNotification()
        }
        if let doneHandler = IKRoastingDoneNotificationHandler.sharedInstance.lastHandler {
            doneHandler.processNotification()
        }
        
        let user = AppCore.sharedInstance.firebaseManager.currentUser
        if user != nil && !(user?.isAnonymous ?? true) || AppCore.sharedInstance.settingsManager.workingWithoutAccount {
            IKInterface.instance()?.connect()
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
    
//    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//        return UIInterfaceOrientationMask(rawValue: UIInterfaceOrientationMask.portrait.rawValue | UIInterfaceOrientationMask.landscape.rawValue)
//    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        IKNotificationManager.sharedInstance.showPushNotification(userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        IKNotificationManager.sharedInstance.showPushNotification(userInfo: userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
        
        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        IKNotificationManager.sharedInstance.showPushNotification(userInfo: userInfo)
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        IKNotificationManager.sharedInstance.showPushNotification(userInfo: userInfo)
        completionHandler()
    }
}
// [END ios_10_message_handling]


extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
