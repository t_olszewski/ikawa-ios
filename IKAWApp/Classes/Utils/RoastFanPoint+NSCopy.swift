//
//  RoastTempPoint+NSCopy.swift
//  IKAWApp
//
//  Created by Admin on 4/18/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import IKRoasterLib
extension RoastFanPoint: NSCopying {
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = RoastFanPoint()
        copy.time = self.time
        copy.power = self.power
        return copy
    }
}
