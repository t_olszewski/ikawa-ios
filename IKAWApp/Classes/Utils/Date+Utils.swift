//
//  Date+Utils.swift
//  IKAWApp
//
//  Created by Admin on 7/5/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

extension Date {
    
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
    
    func dateStr() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        return formatter.string(from: self)
    }
    
    func dateStr2() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy, HH:mm "
        formatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        return formatter.string(from: self)
    }
    
    func dateStr3() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HHmmss"
        formatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        return formatter.string(from: self)
    }
    
    func dateStr4() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy HH:mm"
        return formatter.string(from: self)
    }
}
