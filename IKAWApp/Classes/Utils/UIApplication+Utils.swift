//
//  UIApplication+Utils.swift
//  IKAWApp
//
//  Created by Admin on 6/21/18.
//  Copyright © 2018 Admin. All rights reserved.
//


extension UIApplication {
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
}
