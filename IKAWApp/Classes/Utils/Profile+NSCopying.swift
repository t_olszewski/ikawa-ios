//
//  Profile+NSCopy.swift
//  IKAWApp
//
//  Created by Admin on 4/20/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import IKRoasterLib
extension Profile: NSCopying {
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = Profile()
        copy.uuid = self.uuid
        copy.parentUuid = self.parentUuid
        copy.name = self.name
        copy.schemaVersion = self.schemaVersion
        copy.roastPoints = self.roastPoints
        copy.fanPoints = self.fanPoints
        copy.dateCreated = Date()
        copy.tempSensor = self.tempSensor
        copy.cooldownPoint = self.cooldownPoint
        copy.onServer = self.onServer
        copy.revision = self.revision
        copy.showInLibrary = true
        copy.type = self.type
        copy.dateLastRoasted = Date()
        copy.userId = self.userId
        copy.coffeeName = self.coffeeName
        copy.coffeeId = self.coffeeId
        copy.coffeeWebUrl = self.coffeeWebUrl
        copy.favorite = self.favorite
        return copy
    }
}


