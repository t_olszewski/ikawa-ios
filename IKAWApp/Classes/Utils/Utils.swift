//
//  Utils.swift
//  IKAWApp
//
//  Created by Admin on 7/6/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import IKRoasterLib

import AVFoundation
var player: AVAudioPlayer?

func TempToFahrenheit(temp: Double) -> Double {
    return temp * 1.8 + 32.0
}

func FahrenheitToTemp(temp: Double) -> Double {
    return (temp - 32.0) / 1.8
}

func ConvertCelsiusTempIfNeeded(celTemp: Double) -> Double {
    if USE_FAHRENHEIT {
        return TempToFahrenheit(temp: celTemp)
    } else {
        return celTemp
    }
}

func playSound() {
    guard let url = Bundle.main.url(forResource: "roast_done.wav", withExtension: "") else { return }
    
    do {
        try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
        try AVAudioSession.sharedInstance().setActive(true)
        
        /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
        player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.wav.rawValue)
        
        /* iOS 10 and earlier require the following line:
         player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
        
        guard let player = player else { return }
        
        player.play()
        
    } catch let error {
        print(error.localizedDescription)
    }
}

func clearTempFolder() {
    AppCore.sharedInstance.sqliteManager.dbQueue = nil
    let fileManager = FileManager.default
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    do {
        let filePaths = try fileManager.contentsOfDirectory(atPath: documentsPath)
        for filePath in filePaths {
            if !filePath.contains("roastLogImages") {
                try fileManager.removeItem(atPath: documentsPath + "/" + filePath)
            }
        }
    } catch {
        print("Could not clear temp folder: \(error)")
    }
}

func rounded<T>(value: T, rank: Int?) -> T {
    if value is Float {
        let floatValue = value as! Float
        if let rank = rank {
            let pow = powf(10, Float(rank))
            return (floatValue * pow).rounded(.down) / pow as! T
        }
        return (floatValue * 100).rounded(.down) / 100 as! T
    } else if value is Double {
        let doubleValue = value as! Double
        if let rank = rank {
            let pow = powf(10, Float(rank))
            return Double((doubleValue * Double(pow)).rounded(.down)) / Double(pow) as! T
        }
        return (doubleValue * 100).rounded(.down) / 100 as! T
    } else if value is CGFloat {
        let doubleValue = value as! CGFloat
        if let rank = rank {
            let pow = powf(10, Float(rank))
            return CGFloat((doubleValue * CGFloat(pow)).rounded(.down)) / CGFloat(pow) as! T
        }
        return (doubleValue * 100).rounded(.down) / 100 as! T
    }
    
    
    
    return value
}
