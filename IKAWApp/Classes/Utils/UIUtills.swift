//
//  UIUtills.swift
//  IKAWApp
//
//  Created by Admin on 3/5/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

func UIColorFromHexRGB(_ rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}


func UIColorFromRGB(_ r: Float, g: Float, b: Float) -> UIColor {
    return UIColor(red: CGFloat(r/255), green: CGFloat(g/255), blue: CGFloat(b/255), alpha: CGFloat(1.0))
}

func UIColorFromRGBA(_ r: Float, g: Float, b: Float, alpha: Float) -> UIColor {
    return UIColor(red: CGFloat(r/255), green: CGFloat(g/255), blue: CGFloat(b/255), alpha: CGFloat(alpha))
}

func GetImageFromProfile(profile: Profile, backgroundColor: UIColor?) -> UIImage {
    
    let size = CGSize(width: 140, height: 140)
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    let context = UIGraphicsGetCurrentContext()!
    #if TARGET_HOME
    UIColorFromRGB(0, g: 0, b: 0).setFill()
    UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
    context.setLineWidth(2)
    if profile.tempSensor == TempSensorBelow {
        context.setStrokeColor(kColorExhaust.cgColor)
        context.setFillColor(kColorInlet.withAlphaComponent(0.3).cgColor)
    } else {
        context.setStrokeColor(kColorExhaust.cgColor)
        context.setFillColor(kColorInlet.withAlphaComponent(0.3).cgColor)
    }
    #else
    if let backgroundColor = backgroundColor {
        backgroundColor.setFill()
    } else {
        kColorDarkGrey2.setFill()
    }
    
    UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
    context.setLineWidth(4)
    if profile.tempSensor == TempSensorBelow {
        context.setStrokeColor(kColorInlet.cgColor)
    } else {
        context.setStrokeColor(kColorExhaust.cgColor)
    }
    #endif
    
    let xCoef: Float = Float(size.width) / Float(kTimeAxisMax)
    let yCoef: Float = Float(size.height) / Float(kMaxTemperature)
    
    
    for point in profile.roastPoints {
        let tempPoint = point as! RoastTempPoint
        if (profile.roastPoints.first! as! RoastTempPoint === tempPoint) {
            context.move(to: CGPoint(x: CGFloat(tempPoint.time * xCoef), y: CGFloat(Float(size.height) - tempPoint.temperature * yCoef)))
        } else {
            context.addLine(to: CGPoint(x: CGFloat(tempPoint.time * xCoef), y: CGFloat(Float(size.height) - tempPoint.temperature * yCoef)))
        }
        if tempPoint == profile.roastPoints.last as! RoastTempPoint {
            context.addLine(to: CGPoint(x: CGFloat(tempPoint.time * xCoef), y: CGFloat(Float(size.height))))
            let firstPoint = profile.roastPoints.first as! RoastTempPoint
            context.addLine(to: CGPoint(x: CGFloat(firstPoint.time * xCoef), y: CGFloat(Float(size.height))))
            context.closePath()
        }
    }
    
    context.fillPath()
    
    for point in profile.roastPoints {
        let tempPoint = point as! RoastTempPoint
        if (profile.roastPoints.first! as! RoastTempPoint === tempPoint) {
            context.move(to: CGPoint(x: CGFloat(tempPoint.time * xCoef), y: CGFloat(Float(size.height) - tempPoint.temperature * yCoef)))
        } else {
            context.addLine(to: CGPoint(x: CGFloat(tempPoint.time * xCoef), y: CGFloat(Float(size.height) - tempPoint.temperature * yCoef)))
        }
    }
    context.strokePath()
    
    let img = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return img!
    
}

func GetImageFromRoast(roast: IKRoast, size: CGSize) -> UIImage {
    guard let profileUuid = roast.profileUuid, let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: profileUuid) else {
        return UIImage()
    }
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    let context = UIGraphicsGetCurrentContext()!

    kColorDarkGrey2.setFill()
    UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
    context.setLineWidth(2)

    
    let xCoef: Float = Float(size.width) / Float(kTimeAxisMax)
    let yCoef: Float = Float(size.height) / Float(kMaxTemperature)
    let roastPointsWithoutPreheatingAndCooling = roast.roastPointsWithoutPreheatingAndCooling()
    if profile.tempSensor == TempSensorAbove || profile.tempSensor == TempSensorAboveRobust {
        context.setStrokeColor(kColorExhaust.cgColor)
        for point in roastPointsWithoutPreheatingAndCooling {
            if (roastPointsWithoutPreheatingAndCooling.first! === point) {
                context.move(to: CGPoint(x: CGFloat(point.time * xCoef), y: CGFloat(Float(size.height) - point.temp_above * yCoef)))
            } else {
                context.addLine(to: CGPoint(x: CGFloat(point.time * xCoef), y: CGFloat(Float(size.height) - point.temp_above * yCoef)))
            }
        }
        context.strokePath()
    } else {
        context.setStrokeColor(kColorInlet.cgColor)
        for point in roastPointsWithoutPreheatingAndCooling {
            if (roastPointsWithoutPreheatingAndCooling.first! === point) {
                context.move(to: CGPoint(x: CGFloat(point.time * xCoef), y: CGFloat(Float(size.height) - point.temp_below * yCoef)))
            } else {
                context.addLine(to: CGPoint(x: CGFloat(point.time * xCoef), y: CGFloat(Float(size.height) - point.temp_below * yCoef)))
            }
        }
        context.strokePath()
    }

//    context.setStrokeColor(kColorFanChart.cgColor)
//    for point in roast.roastPointsWithoutPreheating() {
//        if (roast.roastPointsWithoutPreheating().first! === point) {
//            context.move(to: CGPoint(x: CGFloat(point.time * xCoef), y: CGFloat(Float(size.height) - point.fanSet * yCoef)))
//        } else {
//            context.addLine(to: CGPoint(x: CGFloat(point.time * xCoef), y: CGFloat(Float(size.height) - point.fanSet * yCoef)))
//        }
//    }
//    context.strokePath()
    
    let img = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return img!
    
}


