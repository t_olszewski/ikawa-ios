//
//  ChartDataEntry+Utils.swift
//  IKAWApp
//
//  Created by Admin on 7/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import Charts

extension ChartDataEntry {
    
    var temperature: Double {
        set {
            if (USE_FAHRENHEIT) {
                y = TempToFahrenheit(temp: newValue)
            } else {
                y = newValue
            }
        }
        get {
            if (USE_FAHRENHEIT) {
                return FahrenheitToTemp(temp: y)
            } else {
                return y
            }
        }
    }
    
    convenience init(time: Double, temperature: Double) {
        if (USE_FAHRENHEIT) {
            self.init(x: time, y: TempToFahrenheit(temp: temperature))
        } else {
           self.init(x: time, y: temperature)
        }
    }
}
