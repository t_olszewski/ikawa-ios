//
//  Extensions.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 10/4/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut )
        animation.duration = 0.6
        animation.values = [-7.0, 15.0, -5.0, 10.0, 0.0]
        layer.add(animation, forKey: "shake")
    }
    
    var firstResponder: UIView? {
        guard !isFirstResponder else { return self }
        
        for subview in subviews {
            if let firstResponder = subview.firstResponder {
                return firstResponder
            }
        }
        
        return nil
    }
}


extension UITextField {
    
    private func getPositionCursort() -> Int {
        guard let selectedRange = self.selectedTextRange?.start else {
            return 0
        }
        return self.offset(from: self.beginningOfDocument, to: selectedRange)
    }
    
    private func replaceChar(_ newChar: Character) -> Bool {
        if let str = self.text, str.count > 0 {
            if let newPosition = self.position(from: self.selectedTextRange!.start, offset: 1) {
                let selectedTextRange = self.textRange(from: newPosition, to: newPosition)
                let index = getPositionCursort()
                var chars = Array(str.characters)
                if chars[index] != ":" {
                    chars[index] = newChar
                } else {
                    return true
                }
                self.text = String(chars)
                self.selectedTextRange = selectedTextRange
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }
    
    func pushCursorToRight() {
        if let selectedRange = self.selectedTextRange {
            if let newPosition = self.position(from: selectedRange.start, offset: 1) {
                self.selectedTextRange = self.textRange(from: newPosition, to: newPosition)
            }
        }
    }
    
    func pushCursorToLeft() {
        if let selectedRange = self.selectedTextRange {
            if let newPosition = self.position(from: selectedRange.start, offset: -1) {
                self.selectedTextRange = self.textRange(from: newPosition, to: newPosition)
            }
        }
    }
    
    func replaceCharIfNeded(_ replacementString: String) -> Bool{
        if self.text?.count ?? 0 > 0 && !replacementString.isEmpty && ((self.text?.count ?? 0) == 5 || getPositionCursort() == 1 || getPositionCursort() == 0) {
            let cursorPosition = getPositionCursort()
            if cursorPosition >= self.text?.count ?? 1 {
                return true
            } else {
                let char = (Array(replacementString.characters))[0]
                return self.replaceChar(char)
            }
        } else {
            return !((self.text?.count ?? 0) >= 5) || replacementString.isEmpty
        }
    }
    
    func previousNumberIsCorrect() -> Bool {
        let index = getPositionCursort() - 1
        if let text = self.text, text.count >= index {
            let chars = Array(text.characters)
            let number = ((String(chars[index]) as NSString).floatValue)
            return number < 6
        } else {
            return true
        }
    }
    
    func correctTiemFormat() -> Bool {
        if let curentText = self.text, curentText.count > 1 {
            let time = curentText.characters.split{$0 == ":"}.map(String.init)
            if curentText.charOnIndexIs(char: ":", index: 0) {
                if self.getPositionCursort() > 0 && time.count > 0 {
                    if time[0].count >= 2 {
                        return false
                    }
                }
            } else if curentText.charOnIndexIs(char: ":", index: 1) {
                if self.getPositionCursort() > 1 && time.count > 1  {
                    if time[1].count >= 2 {
                        return false
                    }
                }
            } else if curentText.charOnIndexIs(char: ":", index: curentText.count - 1) {
                if self.getPositionCursort() == 0 && time.count > 0 {
                    if time[0].count >= 2 {
                        return false
                    }
                }
            }
        }
        return true
    }
    
        func isLastPositionOfcursor() -> Bool {
        if let selectedRange = self.selectedTextRange {
            let position = self.offset(from: self.beginningOfDocument, to: selectedRange.start)
            return self.text?.count ?? (position + 1) <= position
        } else {
            return false
        }
    }
}

extension String {
    func charOnIndexIs(char: Character, index: Int) -> Bool {
        let chars = Array(self.characters)
        if chars.count > index && chars.count > 0 {
            return chars[index] == char
        } else {
            return false
        }
    }
}


extension UILabel {

    
    
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            }
            else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            
            attributedString.addAttribute(NSAttributedString.Key(rawValue: NSAttributedString.Key.kern.rawValue),
                                           value: newValue,
                                           range: NSRange(location: 0, length: attributedString.length))

            attributedText = attributedString
        }

        get {
            if let currentLetterSpace = attributedText?.attribute(NSAttributedString.Key(rawValue: NSAttributedString.Key.kern.rawValue), at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            }
            else {
                return 0
            }
        }
    }
    @IBInspectable
    var lineSapce: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            }
            else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = newValue
            attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
            attributedText = attributedString
        }
        
        get {
            if let currentLineSpace = attributedText?.attribute(NSAttributedString.Key(rawValue: NSAttributedString.Key.paragraphStyle.rawValue), at: 0, effectiveRange: .none) as? CGFloat {
                return currentLineSpace
            }
            else {
                return 0
            }
        }
    }
}

extension UIButton {
    
    
    
    func addTextSpacing(_ letterSpacing: CGFloat = 2.0){
        let attributedString = NSMutableAttributedString(string: (self.titleLabel?.text!)!)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: letterSpacing, range: NSRange(location: 0, length: (self.titleLabel?.text!.count)!))
        self.setAttributedTitle(attributedString, for: .normal)
    }
    
    var titleAttributedColor: UIColor? {
        set {
            var attributedString: NSMutableAttributedString!
            if let currentAttrString = titleLabel?.attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            } else {
                attributedString = NSMutableAttributedString(string: title(for: .normal) ?? "")
                setTitle(nil, for: .normal)
            }
            if let newValue = newValue {
                attributedString.addAttribute(NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue),
                                              value: newValue,
                                              range: NSRange(location: 0, length: attributedString.length))
                setAttributedTitle(attributedString, for: .normal)
            }
            
        }
        get {
            
            if let currentTitleAttributedColor = attributedTitle(for: .normal)?.attribute(NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue), at: 0, effectiveRange: .none) as? UIColor {
                return currentTitleAttributedColor
            }
            else {
                return nil
            }
        }
    }
    
    func setTitle(title: String) {
        if let attributedText = attributedTitle(for: .normal) {
            let attributes = attributedText.attributes(at: 0, effectiveRange: nil)
            let attrString = NSAttributedString(string: title, attributes: attributes)
            setAttributedTitle(attrString, for: .normal)
        }
    }
    
    func setColor(color: UIColor) {
        if let attributedText = attributedTitle(for: .normal) {
            var attributes = attributedText.attributes(at: 0, effectiveRange: nil)
            attributes[NSAttributedString.Key.foregroundColor] = color
            let attrString = NSAttributedString(string: title(for: .normal) ?? "", attributes: attributes)
            setAttributedTitle(attrString, for: .normal)
        }
        
    }
    #if TARGET_PRO
    override open func draw(_ rect: CGRect) {
        if self.layer.cornerRadius > 0 {
            self.clipsToBounds = true
        }
        adjustFontSize()
        super.draw(rect)
    }
    #endif
    func adjustFontSize() {
        self.titleLabel?.lineBreakMode = .byClipping
        self.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            var attributedString: NSMutableAttributedString!
            if let currentAttrString = titleLabel?.attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            } else {
                attributedString = NSMutableAttributedString(string: title(for: .normal) ?? "")
                setTitle(nil, for: .normal)
            }
            if newValue != 0 {
                attributedString.addAttribute(NSAttributedString.Key(rawValue: NSAttributedString.Key.kern.rawValue),
                                               value: newValue,
                                               range: NSRange(location: 0, length: attributedString.length))
                setAttributedTitle(attributedString, for: .normal)
            }
        }
        get {
            if let currentLetterSpace = attributedTitle(for: .normal)?.attribute(NSAttributedString.Key(rawValue: NSAttributedString.Key.kern.rawValue), at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            }
            else {
                return 0
            }
        }
    }
}

extension UITextField {
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            var attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedString {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            } else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            attributedString.addAttribute(NSAttributedString.Key(rawValue: NSAttributedString.Key.kern.rawValue),
            value: newValue,
            range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }
        get {
            if let currentLetterSpace = attributedText?.attribute(NSAttributedString.Key(rawValue: NSAttributedString.Key.kern.rawValue), at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            }
            else {
                return 0
            }
        }
    }
}

extension UIImage {
    func resizeImage(_ dimension: CGFloat, opaque: Bool, contentMode: UIView.ContentMode = .scaleAspectFit) -> UIImage {
        var width: CGFloat
        var height: CGFloat
        var newImage: UIImage

        let size = self.size
        let aspectRatio =  size.width/size.height

        switch contentMode {
            case .scaleAspectFit:
                if aspectRatio > 1 {                            // Landscape image
                    width = dimension
                    height = dimension / aspectRatio
                } else {                                        // Portrait image
                    height = dimension
                    width = dimension * aspectRatio
                }

        default:
            fatalError("UIIMage.resizeToFit(): FATAL: Unimplemented ContentMode")
        }

        if #available(iOS 10.0, *) {
            let renderFormat = UIGraphicsImageRendererFormat.default()
            renderFormat.opaque = opaque
            let renderer = UIGraphicsImageRenderer(size: CGSize(width: width, height: height), format: renderFormat)
            newImage = renderer.image {
                (context) in
                self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), opaque, 0)
                self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
                newImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
        }

        return newImage
    }
}
