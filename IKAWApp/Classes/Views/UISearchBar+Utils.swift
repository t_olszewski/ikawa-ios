//
//  UISearchBar+Utils.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 4/17/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

extension UISearchBar {
    
    private func getViewElement<T>(type: T.Type) -> T? {
        let svs = subviews.flatMap { $0.subviews }
        guard let element = (svs.filter { $0 is T }).first as? T else { return nil }
        return element
    }
    
    func setTextFieldColor(color: UIColor, textColor: UIColor) {
        var textField: UITextField?
        if #available(iOS 13.0, *) {
            textField = self.searchTextField
        } else {
            textField = getViewElement(type: UITextField.self)
        }
        guard let searchTextField = textField else {
            return
        }
        
        switch searchBarStyle {
        case .minimal:
            searchTextField.layer.backgroundColor = color.cgColor
            searchTextField.layer.cornerRadius = 6
            searchTextField.textColor = textColor
            searchTextField.tintColor = .darkGray
        case .prominent, .default:
            searchTextField.backgroundColor = color
            searchTextField.textColor = textColor
            searchTextField.tintColor = .darkGray
        }
    }
    
    func setTextFieldFont(_ fontName: String = "AvenirNext-Regular") {
        if #available(iOS 13.0, *) {
            self.searchTextField.font = UIFont(name: fontName, size: 14)
        } else {
            getViewElement(type: UITextField.self)?.font = UIFont(name: fontName, size: 14)
        }
    }
}
