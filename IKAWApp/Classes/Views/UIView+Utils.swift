//
//  UIView+Utils.swift
//  IKAWApp
//
//  Created by Admin on 3/30/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

extension UIView {
    
    func createBottomLine(color: UIColor = UIColor.darkGray) {
        let border = CALayer()
        let width = CGFloat(2.0)
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func createShadow(color: UIColor = UIColor.black, opacity: Float = 0.5, x: Double = 0, y: Double = 4, blur: CGFloat = 4) {
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = CGSize(width: x, height: y)
        layer.shadowRadius = blur
    }
    
    func withGradienBackground(color1: UIColor, color2: UIColor, color3: UIColor, color4: UIColor) {
        layer.sublayers = nil
        let layerGradient = CAGradientLayer()
        layerGradient.colors = [color1.cgColor, color2.cgColor, color3.cgColor, color4.cgColor]
        layerGradient.frame = bounds
        layerGradient.startPoint = CGPoint.zero
        layerGradient.endPoint = CGPoint(x: 0, y: 1.5)
        layerGradient.locations = [0.0, 0.2, 0.7, 1.0]
        layer.insertSublayer(layerGradient, at: 0)
    }
}

extension String {
    func maxLength(length: Int) -> String {
        var str = self
        let nsString = str as NSString
        if nsString.length >= length {
            str = nsString.substring(with:
                NSRange(
                    location: 0,
                    length: nsString.length > length ? length : nsString.length)
            ) + "..."
        }
        return  str
    }
}
