//
//  IKRoastNotesCell.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 8/26/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class IKRoastNotesCell: UITableViewCell {
    @IBOutlet weak var notesTitleLabel: UILabel!
    @IBOutlet weak var notesTextLabel: UILabel!
    @IBOutlet weak var notesTextBottomConstraint: NSLayoutConstraint!
    
    func populate(notes: IKRoastNotes) {

        if notes.notes.isEmpty {
            notesTextBottomConstraint.constant = 0
            notesTextLabel.text = ""
            notesTitleLabel.text = NSLocalizedString("$add_notes", comment: "")
        } else {
            notesTextBottomConstraint.constant = 18
            notesTextLabel.text = notes.notes.maxLength(length: 200)
            notesTitleLabel.text = NSLocalizedString("$notes", comment: "")
        }
    }
}
