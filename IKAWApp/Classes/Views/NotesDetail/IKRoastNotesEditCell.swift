//
//  IKRoastNotesEditCell.swift
//  IKAWApp
//
//  Created by Admin on 5/15/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class IKRoastNotesEditCell: UITableViewCell {

    @IBOutlet weak var sampleRoastedTitleLabel: UILabel!
    @IBOutlet weak var sampleRoasterLabel: UILabel!
    @IBOutlet weak var sampleRoastedTextfield: UITextField!
    @IBOutlet weak var sampleRoasterStackView: UIStackView!
    @IBOutlet weak var changeButton: UIButton!
    
    var changeEditState: ((_ cofeType: String,_ isEdit: Bool) -> ())?
    var isEdit = false
    
    override func awakeFromNib() {
        super.awakeFromNib()

        sampleRoastedTextfield.isHidden = true
        
        if let placeholder = sampleRoastedTextfield.placeholder {
            sampleRoastedTextfield.attributedPlaceholder = NSAttributedString(string:placeholder,
                                                                              attributes: [NSAttributedString.Key.foregroundColor: kColorDarkGrey2])
        }
        sampleRoastedTextfield.leftViewMode = UITextField.ViewMode.always
        sampleRoastedTextfield.keyboardAppearance = .dark
        sampleRoastedTextfield.returnKeyType = UIReturnKeyType.done
        sampleRoastedTextfield.delegate = self
        sampleRoastedTitleLabel.text = NSLocalizedString("$note_coffee_sample", comment: "").replacingOccurrences(of: "%s", with: "")
        //let editHandleTap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        //self.addGestureRecognizer(editHandleTap)
        changeButton.addTarget(self, action: #selector(handleTap), for: .touchUpInside) //(editHandleTap)
        //notesTitleLabel.text = NSLocalizedString("$note_notes", comment: "").replacingOccurrences(of: "%s", with: "")
        //let photo = UIImageView()
        //photoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(_:))))
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @objc func handleTap() {
        self.sampleRoastedTextfield.resignFirstResponder()
        changeEditState?(sampleRoastedTextfield.text ?? "", !isEdit)
    }
    
    func populate(notes: IKRoastNotes, isEdit: Bool) {
        sampleRoasterLabel.text = notes.coffeeType
        sampleRoastedTextfield.text = notes.coffeeType
        self.isEdit = isEdit
        sampleRoastedTextfield.isHidden = !isEdit
        sampleRoasterLabel.isHidden = isEdit
        if isEdit {
            DispatchQueue.main.async {
                self.sampleRoastedTextfield.becomeFirstResponder()
            }
            
        }
    }
}

extension IKRoastNotesEditCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        handleTap()
        return true
    }
}
