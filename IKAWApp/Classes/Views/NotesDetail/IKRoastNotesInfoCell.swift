//
//  IKRoastNotesInfoCell.swift
//  IKAWApp
//
//  Created by Admin on 5/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class IKRoastNotesInfoCell: UITableViewCell {
   
    @IBOutlet weak var preheatTempLabel: UILabel!
    @IBOutlet weak var endTempLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var timeAfterFirstLabel: UILabel!
    
    @IBOutlet weak var turingPointLabel: UILabel!
    @IBOutlet weak var colorChangeLabel: UILabel!
    @IBOutlet weak var firstCrackLabel: UILabel!
    @IBOutlet weak var secondCrackLabel: UILabel!
    
    func populate(notes: IKRoastNotes) {
        turingPointLabel.text = notes.turningPointDescription().isEmpty ? "-" : notes.turningPointDescription()
        colorChangeLabel.text = notes.colorChangeDescription().isEmpty ? "-" : notes.colorChangeDescription()
        secondCrackLabel.text = notes.secondCrackDescription().isEmpty ? "-" : notes.secondCrackDescription()
        firstCrackLabel.text = notes.firstCrackDescription().isEmpty ? "-" : notes.firstCrackDescription()
        endTempLabel.text = notes.endTempDescription().isEmpty ? "-" : notes.endTempDescription()
        endTimeLabel.text = notes.endTimeDescription().isEmpty ? "-" : notes.endTimeDescription()
        timeAfterFirstLabel.text = notes.timeAfterFirstDescription().isEmpty ? "-" : notes.timeAfterFirstDescription()
        preheatTempLabel.text = notes.preheatTempDescription().isEmpty ? "-" : notes.preheatTempDescription()
    }
}
