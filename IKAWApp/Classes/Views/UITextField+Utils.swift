//
//  UITextField+Utils.swift
//  IKAWA-Home
//
//  Created by Никита on 10/10/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

extension UITextField {
    func setLeftPaddingPoints(_ amount: CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}
