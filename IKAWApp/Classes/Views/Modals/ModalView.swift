//
//  Modal.swift
//  ModalView
//

import Foundation

import UIKit


class ModalView: NibLoadingView {
    var dismissCallback: (()->())?
    var backgroundView = UIView()
    
    func show(animated:Bool, dismissCallback: (()->())? = nil){
        self.dismissCallback = dismissCallback
        self.backgroundView.alpha = 0
        if var topController = UIApplication.shared.delegate?.window??.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            let window = UIApplication.shared.keyWindow!
            window.addSubview(self)
            //topController.view.window!.addSubview(self)
            self.snp.makeConstraints { (make) in
                make.edges.equalTo(window).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            }
        }
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.backgroundView.alpha = 1.0
            })
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
            }, completion: { (completed) in
                
            })
        }else{
            self.backgroundView.alpha = 1.0
        }
    }
    
    func dismiss(animated:Bool){
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.backgroundView.alpha = 0
            }, completion: { (completed) in
                
            })
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
            }, completion: { (completed) in
                self.removeFromSuperview()
            if let dismissCallback = self.dismissCallback {
                dismissCallback()
            }
            })
        }else{
            self.removeFromSuperview()
            if let dismissCallback = self.dismissCallback {
                dismissCallback()
            }

        }
        
    }
}




