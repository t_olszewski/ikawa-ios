//
//  DoserOpenModalView.swift
//  IKAWApp
//
//  Created by Admin on 7/18/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class DoserOpenModalView: ModalView  {
    @IBOutlet var backgound: UIView!
    @IBOutlet weak var dialog: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundView = backgound
    }
}
