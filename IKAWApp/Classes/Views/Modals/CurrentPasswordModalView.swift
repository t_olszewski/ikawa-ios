//
//  CreateRecipe.swift
//  IKAWA-Home
//
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class CurrentPasswordModalView: ModalView {
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var dialog: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var cancelButtonCallback: (() -> Void)?
    var saveButtonCallback: (( _ password: String) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundView = background
        
        
        dialog.layer.shadowColor = UIColor.black.cgColor
        dialog.layer.shadowOpacity = 0.5
        dialog.layer.shadowOffset = CGSize(width: 0, height: 1)
        dialog.layer.shadowRadius = 4
        saveButton.layer.borderWidth = 2
        saveButton.layer.borderColor = kColorRed.cgColor
        
        #if TARGET_PRO
            textField.keyboardAppearance = .dark
        #else
            textField.keyboardAppearance = .light
        #endif
        
    }
    
    override func show(animated: Bool, dismissCallback: (() -> ())?) {
        super.show(animated: animated, dismissCallback: dismissCallback)
        layoutIfNeeded()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        textField.becomeFirstResponder()
        
    }
    
    override func dismiss(animated: Bool) {
        super.dismiss(animated: animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        saveButton.layer.cornerRadius = saveButton.frame.height / 2
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Actions
    
    @IBAction func onButtonCancelTap() {
        dismiss(animated: true)
        endEditing(true)
        cancelButtonCallback?()
    }
    
    @IBAction func onButtonSaveTap(_ sender: Any) {
        dismiss(animated: true)
        endEditing(true)
        guard textField.text != "" else {
            return
        }
        saveButtonCallback?(textField.text!)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            
            
            if endFrameY >= UIScreen.main.bounds.size.height {
                bottomConstraint.constant = 0
            } else {
                bottomConstraint.constant = endFrame?.size.height ?? 0.0
            }
            
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
}

