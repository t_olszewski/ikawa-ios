//
//  SwapJarsModalView.swift
//  IKAWApp
//
//  Created by Admin on 7/18/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class SwapJarsModalView: ModalView  {
    var callback: (()->())?
    
    @IBOutlet var backgound: UIView!
    @IBOutlet weak var dialog: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundView = backgound
    }
    
    @IBAction func doneButtonTouchUpInside(_ sender: Any) {
        dismiss(animated: true)
        if (callback != nil) {
            callback!()
        }
    }
}
