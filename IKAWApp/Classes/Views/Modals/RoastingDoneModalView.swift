//
//  SwapJarsModalView.swift
//  IKAWApp
//
//  Created by Admin on 7/18/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class RoastingDoneModalView: ModalView  {
    
    var cancelCallback: (()->())?
    var shareCallback: (()->())?
    
    @IBOutlet var backgound: UIView!
    @IBOutlet weak var dialog: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundView = backgound
    }
    
    @IBAction func cancelButtonTouchUpInside(_ sender: Any) {
        if (cancelCallback != nil) {
            cancelCallback!()
        }
        
        dismiss(animated: true)
    }
    
    @IBAction func shareButtonTouchUpInside(_ sender: Any) {
        if (shareCallback != nil) {
            shareCallback!()
        }
        dismiss(animated: true)
    }
}
