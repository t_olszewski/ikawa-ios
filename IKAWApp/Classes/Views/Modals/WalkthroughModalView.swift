//
//  WalkthroughModalView.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 2/3/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

enum StackViewIndex {
    case First, Second, Third
}

class WalkthroughModalView: ModalView {

    @IBOutlet private weak var dialog: UIView!
    @IBOutlet private weak var background: UIView!
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var okButton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    
    
    @IBOutlet private weak var firstStackView: UIStackView!
    @IBOutlet private weak var firstImageViewContainer: UIView!
    @IBOutlet private weak var firstImageView: UIImageView!
    @IBOutlet private weak var firstLabel: UILabel!
    @IBOutlet private weak var firstLabelContainer: UIView!
    
    @IBOutlet private weak var secondStackView: UIStackView!
    @IBOutlet private weak var secondImageViewContainer: UIView!
    @IBOutlet private weak var secondImageView: UIImageView!
    @IBOutlet private weak var secondLabelContainer: UIView!
    @IBOutlet private weak var secondLabel: UILabel!
    
    @IBOutlet private weak var thirdStackView: UIStackView!
    @IBOutlet private weak var thirdImageViewContainer: UIView!
    @IBOutlet private weak var thirdImageView: UIImageView!
    @IBOutlet private weak var thirdLabelContainer: UIView!
    @IBOutlet private weak var thirdLabel: UILabel!
    
    @IBOutlet weak var stackViewTopConstraint: NSLayoutConstraint!
    
    
    var titleIsHidden: Bool = false {
        didSet {
            if titleIsHidden {
                titleLabel.isHidden = true
                stackViewTopConstraint.isActive = false
            } else {
                titleLabel.isHidden = false
                stackViewTopConstraint.isActive = true
            }
        }
    }
    
    var titleText: String = "" {
        didSet {
            titleLabel.text = titleText
        }
    }
    
    var titleButton: String = "" {
        didSet {
            okButton.setTitle(title: titleButton)
        }
    }
    
    var titleIsBold: Bool = false {
        didSet {
            titleLabel.font = UIFont(name: titleIsBold ? kFontAvenirNextDemiBold : kFontAvenirNextMedium , size: 16)
        }
    }
    
    
    override public func show(animated: Bool, dismissCallback: (() -> ())?) {
        super.show(animated: animated, dismissCallback: dismissCallback)
        correctAppearance()
    }
    
    private func correctAppearance() {
        dialog.layer.shadowColor = UIColor.black.cgColor
        dialog.layer.shadowOpacity = 1
        dialog.layer.shadowOffset = CGSize(width: 3, height: 3)
        dialog.layer.shadowRadius = 5
        dialog.layer.masksToBounds = false
        dialog.layer.cornerRadius = 5
        okButton.layer.cornerRadius = 17.5
        background.backgroundColor = UIColorFromRGBA(89, g: 90, b: 87, alpha: 0.7)
    }
    
    func setupHorisontalView(image: UIImage?, title: String?, index: StackViewIndex) {
        switch index {
        case .First:
            firstStackView.isHidden = false
            if let image = image {
                firstImageViewContainer.isHidden = false
                firstImageView.image = image
            } else {
                firstLabel.textAlignment = .center
                firstImageViewContainer.isHidden = true
            }
            if let title = title {
                firstLabelContainer.isHidden = false
                firstLabel.text = title
            } else {
                firstLabelContainer.isHidden = true
            }
        case .Second:
            secondStackView.isHidden = false
            if let image = image {
                secondImageViewContainer.isHidden = false
                secondImageView.image = image
            } else {
                secondLabel.textAlignment = .center
                secondImageViewContainer.isHidden = true
            }
            if let title = title {
                secondLabelContainer.isHidden = false
                secondLabel.text = title
            } else {
                secondLabelContainer.isHidden = true
            }
        case .Third:
            thirdStackView.isHidden = false
            if let image = image {
                thirdImageViewContainer.isHidden = false
                thirdImageView.image = image
            } else {
                thirdLabel.textAlignment = .center
                thirdImageViewContainer.isHidden = true
            }
            if let title = title {
                thirdLabelContainer.isHidden = false
                thirdLabel.text = title
            } else {
                thirdLabelContainer.isHidden = true
            }
        }
    }
    
    @IBAction private func okayButtonAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
}
