//
//  YesNoMadalVIew.swift
//  IKAWA-Home
//
//  Created by Никита on 11/10/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

enum ButtonsModalViewLayout {
    case horisontal, vertical
}


enum ButtonPopupModalViewIndex {
    case First, Second, Third, Fourth
}

class ButtonsPopupModalView: ModalView {
    
    @IBOutlet weak private var background: UIView!
    @IBOutlet weak private var dialog: UIView!
    
    @IBOutlet weak private var firstButton: UIButton!
    @IBOutlet weak private var secondButton: UIButton!
    @IBOutlet weak private var thirdButton: UIButton!
    @IBOutlet weak private var fourthButton: UIButton!
    
    
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var checkBoxContainerView: UIView!
    @IBOutlet weak var checkBoxImageView: UIImageView!
    @IBOutlet weak var checkBoxTitleLabel: UILabel!
    @IBOutlet weak var checkBoxBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var checkBoxHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewRightConstraint: NSLayoutConstraint!
    
    typealias Callback = (() -> Void)
    
    var layout: ButtonsModalViewLayout = .horisontal {
        didSet {
            if layout == .horisontal {
                stackViewLeftConstraint.constant = 28
                stackViewRightConstraint.constant = 28
            } else {
                stackViewLeftConstraint.constant = 87
                stackViewRightConstraint.constant = 87
            }
        }
    }
    var title: String = ""
    var subtitle: String = ""
    
    
    var firstButtonCallback: Callback?
    var secondButtonCallback: Callback?
    var thirdButtonCallback: Callback?
    var fourthButtonCallback: Callback?
    var checkBoxCallback: Callback?
    
    var firstButtonSettings: (title: String, type: ButtonPopupModalViewType, callback: Callback?,image: UIImage?)?
    var secondButtonSettings: (title: String, type: ButtonPopupModalViewType, callback: Callback?,image: UIImage?)?
    var thirdButtonSettings: (title: String, type: ButtonPopupModalViewType, callback: Callback?,image: UIImage?)?
    var fourthButtonSettings: (title: String, type: ButtonPopupModalViewType, callback: Callback?,image: UIImage?)?
    
    var isCheckBoxSelected: Bool = false
    
    deinit {
        print("Deinit")
    }
    
    //MARK: - Public
    public func setUpCheckBox(title: String?, callback: Callback?) {
        checkBoxContainerView.isHidden = false
        checkBoxBottomConstraint.constant = 30
        checkBoxHeightConstraint.constant = 30
        checkBoxCallback = callback
        if let title = title {
            checkBoxTitleLabel.text = title
        }
    }
    
    override public func show(animated: Bool, dismissCallback: (() -> ())?) {
        super.show(animated: animated, dismissCallback: dismissCallback)
        correctAppearance()
    }
    
    public func setUpButton(index: ButtonPopupModalViewIndex, title: String, type: ButtonPopupModalViewType, image: UIImage?, callback: Callback?) {
        print("popup upload")
        switch index {
        case .First:
            firstButtonSettings = (title, type, callback, image)
            self.firstButtonCallback = callback
        case .Second:
            secondButtonSettings = (title, type, callback, image)
            self.secondButtonCallback = callback
        case .Third:
            thirdButtonSettings = (title, type, callback, image)
            self.thirdButtonCallback = callback
        case .Fourth:
            fourthButtonSettings = (title, type, callback, image)
            self.fourthButtonCallback = callback
        }

    }
    
    public func setUpButton(index: ButtonPopupModalViewIndex, title: String, type: ButtonPopupModalViewType, callback: Callback?) {
        print("popup upload")
        switch index {
        case .First:
            firstButtonSettings = (title, type, callback, nil)
            self.firstButtonCallback = callback
        case .Second:
            secondButtonSettings = (title, type, callback, nil)
            self.secondButtonCallback = callback
        case .Third:
            thirdButtonSettings = (title, type, callback, nil)
            self.thirdButtonCallback = callback
        case .Fourth:
            fourthButtonSettings = (title, type, callback, nil)
            self.fourthButtonCallback = callback
        }
    }
    
    //MARK: - Actions
    
    @IBAction private func firstButtonTouchUpInside() {
        dismiss(animated: true)
        firstButtonCallback?()
    }
    
    @IBAction private func secondButtonTouchUpInside(_ sender: Any) {
        dismiss(animated: true)
        secondButtonCallback?()
    }
    
    @IBAction private func thirdButtonTouchUpInside(_ sender: Any) {
        dismiss(animated: true)
        thirdButtonCallback?()
    }
    
    @IBAction private func fourthButtonTouchUpInside(_ sender: Any) {
        dismiss(animated: true)
        fourthButtonCallback?()
    }
    
    @IBAction private func tapOnMarker(_ sender: Any) {
        isCheckBoxSelected = !isCheckBoxSelected
        if isCheckBoxSelected {
            checkBoxImageView.image = UIImage(named: "selectedPointIcon")
        } else {
            checkBoxImageView.image = UIImage(named: "DeselectedPointIcon")
        }
        checkBoxCallback?()
    }
    
    
    private func correctAppearance() {
        dialog.layer.shadowColor = UIColor.black.cgColor
        dialog.layer.shadowOpacity = 1
        dialog.layer.shadowOffset = CGSize(width: 3, height: 3)
        dialog.layer.shadowRadius = 5
        dialog.layer.masksToBounds = false
        dialog.layer.cornerRadius = 5
        
        switch layout {
        case .horisontal:
            self.stackView.axis = .horizontal
        case .vertical:
            self.stackView.axis = .vertical
        }
        if title.isEmpty {
            titleLabel.isHidden = true
        } else {
            titleLabel.isHidden = false
            
            if let attributedText = titleLabel.attributedText {
                let attributes = attributedText.attributes(at: 0, effectiveRange: nil)
                let attrString = NSAttributedString(string: title, attributes: attributes)
                titleLabel.attributedText = attrString
            }
            
            titleLabel.text = title
        }
        
        if subtitle.isEmpty {
            subtitleLabel.isHidden = true
        } else {
            subtitleLabel.isHidden = false
            
            if let attributedText = subtitleLabel.attributedText {
                let attributes = attributedText.attributes(at: 0, effectiveRange: nil)
                let attrString = NSAttributedString(string: subtitle, attributes: attributes)
                subtitleLabel.attributedText = attrString
            }
            
            subtitleLabel.text = subtitle
        }
        
        if  let firstButtonSettings = firstButtonSettings, !(firstButtonSettings.title.isEmpty) {
            firstButton.isHidden = false
            correctAppearance(button: firstButton, title: firstButtonSettings.title, type: firstButtonSettings.type, image: firstButtonSettings.image)
        } else {
            firstButton.isHidden = true
        }

        
        if  let secondButtonSettings = secondButtonSettings, !(secondButtonSettings.title.isEmpty) {
            secondButton.isHidden = false
            correctAppearance(button: secondButton, title: secondButtonSettings.title, type: secondButtonSettings.type, image: secondButtonSettings.image)
        } else {
            secondButton.isHidden = true
        }

        if  let thirdButtonSettings = thirdButtonSettings, !(thirdButtonSettings.title.isEmpty) {
            thirdButton.isHidden = false
            correctAppearance(button: thirdButton, title: thirdButtonSettings.title, type: thirdButtonSettings.type, image: thirdButtonSettings.image)
        } else {
            thirdButton.isHidden = true
        }
        
        if  let fourthButtonSettings = fourthButtonSettings, !(fourthButtonSettings.title.isEmpty) {
            fourthButton.isHidden = false
            correctAppearance(button: fourthButton, title: fourthButtonSettings.title, type: fourthButtonSettings.type, image: fourthButtonSettings.image)
        } else {
            fourthButton.isHidden = true
        }
        
        checkBoxImageView.layer.borderWidth = 1
        checkBoxImageView.layer.borderColor = kColorDarkGrey2.cgColor
        checkBoxContainerView.isHidden = checkBoxCallback == nil

    }
    
    private func correctAppearance(button: UIButton, title: String, type: ButtonPopupModalViewType, image: UIImage?) {
        layoutIfNeeded()
        background.backgroundColor = UIColorFromRGBA(89, g: 90, b: 87, alpha: 0.7)
        
        button.correctAppearance(title: title, type: type, image: image)
        button.addTextSpacing(2)
        button.titleAttributedColor = button.titleColor(for: .normal)
    }
}
