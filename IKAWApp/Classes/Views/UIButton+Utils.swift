//
//  UIButton+Utils.swift
//  IKAWApp
//
//  Created by Admin on 10/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

enum ButtonPopupModalViewType {
    case Cancel, Bordered, Filled
}


extension UIButton {
    func correctAppearance(title: String, type: ButtonPopupModalViewType, image: UIImage?) {
        if let image = image {
            setImage(image, for: .normal)
        }
       
        if let attributedText = attributedTitle(for: .normal) {
            let attributes = attributedText.attributes(at: 0, effectiveRange: nil)
            let attrString = NSAttributedString(string: title, attributes: attributes)
            setAttributedTitle(attrString, for: .normal)
        }
    
        setTitle(title, for: .normal)
        
        switch type {
        case .Cancel:
            backgroundColor = UIColor.clear
            setTitleColor(kColorWhite, for: .normal)
            layer.borderWidth = 0

        case .Bordered:
            backgroundColor = kColorDarkGrey2
            layer.borderColor = kColorWhite.cgColor
            setTitleColor(kColorWhite, for: .normal)
            layer.borderWidth = 1
            layer.cornerRadius = frame.size.height / 2
        case .Filled:
            backgroundColor = kColorWhite
            layer.borderColor = kColorWhite.cgColor
            setTitleColor(kColorDarkGrey2, for: .normal)
            layer.borderWidth = 1
            layer.cornerRadius = frame.size.height / 2
        }
        addTextSpacing()
    }
}
