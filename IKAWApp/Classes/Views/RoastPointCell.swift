//
//  RoastPointCell.swift
//  IKAWApp
//
//  Created by Admin on 4/18/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class RoastPointCell: UITableViewCell {

    @IBOutlet weak var timeTxtField: UITextField!
    @IBOutlet weak var valueTxtField: UITextField!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var rowNumberLabel: UILabel!
    @IBOutlet weak var unit2Label: UILabel!
    
    var doneAction: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        toolBarCustom(textField: timeTxtField)
        toolBarCustom(textField: valueTxtField)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func timeSelectionButtonTouchUpInside(_ sender: Any) {
        timeTxtField.becomeFirstResponder()
    }
    
    @IBAction func tempSelectionButtonTouchUpInside(_ sender: Any) {
        valueTxtField.becomeFirstResponder()
    }
    
    private func toolBarCustom(textField: UITextField) {
        let toolBar = KeyboardInputView.instanceFromNib()
        toolBar.addConfinguration(action: {[weak self] (sender) in
            self?.doneAction!()
        })
        toolBar.setupArrows(textField: textField)
        textField.inputAccessoryView = toolBar
    }
}
