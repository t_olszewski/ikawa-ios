//
//  Const.swift
//  IKAWApp
//
//  Created by Admin on 3/5/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

//Notifications
extension Notification.Name {
    static let ProfileManagerDidChangeList = Notification.Name("kIKProfileManagerDidChangeList")
    static let OpenRoasterReceipt = Notification.Name("kOpenRoasterReceipt")
    static let DidLoginToCropster = Notification.Name("kDidLoginToCropster")
    static let DidLogOut = Notification.Name("kDidLogOut")
    static let DegreeCelsiusChanged = Notification.Name("kDegreeCelsiusChanged")
    static let StartNewRoast = Notification.Name("KStartNewRoast")
    static let RoastDidCanceled = Notification.Name("KRoastDidCanceled")
    static let DidChangeUserData = Notification.Name("kDidChangeUserData")
    static let DidChangeRoastNotesData = Notification.Name("kDidChangeRoastNotesData")
    static let DidStartSync = Notification.Name("kDidStartSync")
    static let DidFinishSync = Notification.Name("kDidFinishSync")
}

let kAuthenticationUrl = "https://www.ikawacoffee.com/firebase-auth.php?token="
let kShopUrl = "https://www.ikawacoffee.com/at-home/app-shop/"
#if TARGET_PRO
let kTermsAndConditions = "https://www.ikawacoffee.com/corporate/legal/terms-of-use-pro-app"
#else
let kTermsAndConditions = "https://www.ikawacoffee.com/corporate/app-privacy-policy-home/"
#endif


//UI

let kFontAvenirNextRegular = "AvenirNext-Regular"
let kFontAvenirNextLTProRegular = "AvenirNextLTPro-Regular"
let kFontAvenirNextDemiBold = "AvenirNext-DemiBold"
let kFontAvenirNextLTProBold = "AvenirNextLTPro-Bold"
let kFontAvenirNextMedium = "AvenirNext-Medium"

#if TARGET_PRO
let kMainColor = UIColorFromRGB(0, g: 0, b: 0)
let kColorDarkGrey1 = UIColorFromRGB(20, g: 20, b: 20)
let kColorDarkGrey2 = UIColorFromRGB(33, g: 32, b: 33)
let kColorLightGrey1 = UIColorFromRGB(216, g: 216, b: 216)
let kColorLightGrey2 = UIColorFromRGB(134, g: 134, b: 134)
let kColorLightGrey3 = UIColorFromRGB(106, g: 106, b: 106)
let kColorRed = UIColorFromRGB(223, g: 35, b: 30)

let kColorInlet = UIColorFromRGB(255, g: 174, b: 87)
let kColorExhaust = UIColorFromRGB(246, g: 110, b: 96)

let kNavigationBarColor = kColorDarkGrey1
let kColorTableCellBackground = kColorDarkGrey2
let kColorTableCellText = UIColor.white
let kColorChartGride = UIColorFromRGBA(155, g: 155, b: 155, alpha: 0.25)
let kColorChartlabelText = UIColorFromRGBA(155, g: 155, b: 155, alpha: 1)
let kColorFanChart = kColorWhite
let kColorAirflow = UIColorFromRGB(118, g: 236, b: 255)

let kColorRoasting = UIColorFromRGB(51, g: 172, b: 231)
let kColorDoserOpen = UIColorFromRGB(66, g: 207, b: 223)
let kColorReadyToRoast = UIColorFromRGB(77, g: 232, b: 210)
let kColorRiseLine = UIColorFromRGB(155, g: 126, b: 255)
let kColorCoolingDown = UIColorFromRGB(52, g: 126, b: 255)
let kColorPreHeating = UIColorFromRGB(0, g: 255, b: 211)
let kColorOnRoaster = UIColorFromRGB(87, g: 255, b: 187)
let kColorHighlightedTextField = UIColorFromRGBA(96, g: 213, b: 226, alpha: 0.48)

#elseif TARGET_HOME
let kMainColor = kColorWhite
let kColorLightGrey1 = UIColorFromRGB(225, g: 226, b: 225)
let kColorLightGrey2 = UIColorFromRGB(89, g: 90, b: 87)
let kColorDarkGrey2 = UIColorFromRGB(34, g: 34, b: 34)
let kColorLightGrey3 = UIColorFromRGB(106, g: 106, b: 106)
let kColorAirflow = UIColorFromRGB(72, g: 171, b: 255)



let kNavigationBarColor = kColorWhite
let kColorTableCellBackground = UIColorFromRGB(255, g: 255, b: 255)
let kColorGreyDark = UIColorFromRGB(37, g: 37, b: 37)
let kColorChartGride = UIColorFromRGB(196, g: 196, b: 192)
let kColorChartlabelText = UIColorFromRGB(156, g: 156, b: 152)
let kColorFanChart = UIColorFromRGB(74, g: 74, b: 74)
let kColorRoasting = UIColorFromRGB(255, g: 96, b: 72)
let kColorRed = UIColorFromRGB(223, g: 35, b: 29)
let kColorRedDark = UIColorFromRGB(166, g: 0, b: 0)
let kColorExhaust = UIColorFromRGB(215, g: 158, b: 63)
let kColorInlet = UIColorFromRGB(225, g: 35, b: 29)
let kColorReadyToRoast = UIColorFromRGB(105, g: 157, b: 71)
let kColorRiseLine = UIColorFromRGB(105, g: 157, b: 71)
let kColorCoolingDown = UIColorFromRGB(72, g: 171, b: 255)
let kColorPreHeating = UIColorFromRGB(223, g: 35, b: 29)
let kColorOnRoaster = UIColorFromRGB(223, g: 35, b: 29)
#endif

let kColorWhite = UIColorFromRGB(255, g: 255, b: 255)
let kColorGrey = UIColorFromRGB(89, g: 90, b: 87)

// Fonts
let kAxisFont = UIFont(name: "AvenirNext-Medium", size: UIDevice.current.userInterfaceIdiom == .phone ? 11.0 : 13.0)!


// Charts
let ROR_ROLLING_AVERAGE = 10
let ROR_DEFAULT_INTERVAL = 15

// Settings
let kRorScale = 2.0

let kMaxRorTemperature = 40.0
let kMinRorTemperature = -20.0

let kMaxRorTemperatureFahrenheit = 60.0
let kMinRorTemperatureFahrenheit = -40.0

let kMaxTemperatureFahrenheit = 600.0

var IK_SIMULATE_ROASTER =        false
var IK_SHOW_DEBUG_INFORMATIOMN =        false
#if TARGET_PRO
let IK_TARGET_PRO = true
let IK_FIREBASE_DATABASE = "pro"
let IK_MAX_COOLDOWN_TIME = 300
let IK_MAX_FAN_POINTS    =  10
let IK_MAX_ROAST_POINTS  =  20
let IK_ENABLE_CROPSTER = true
let IK_TWO_SENSOR_SUPPORT = true

let IK_FIRMWARE_UPDATE_VERSION = 24
let IK_PROFILE_BASEURL = "https://share.ikawa.support/profile/?"

let kTimeAxisMax = 20 * 60
let kTimeAxisMin = 12 * 60 + 30

let kMaxTemperature = 300.0
let kMinTemperature = 25.0

#elseif TARGET_HOME
let IK_TARGET_PRO = false

let IK_FIREBASE_DATABASE = (Configuration.environment == "PROD" || Configuration.environment == "BETA")  ? "home" : "home_dev"
let IK_MAX_COOLDOWN_TIME = 180
let IK_TWO_SENSOR_SUPPORT = false
let IK_ENABLE_CROPSTER = false
let IK_MAX_FAN_POINTS    =  3
let IK_MAX_ROAST_POINTS  =  7

let IK_FIRMWARE_UPDATE_VERSION = 24
let IK_PROFILE_BASEURL = "https://share.ikawa.support/profile_home/?"

let kTimeAxisMin: Double = 12 * 60
let kTimeAxisMax: Double = 15.5 * 60

let kMaxTemperature = 300.0
let kMinTemperature = 25.0

#endif

#if TARGET_DEV
let IK_FORCE_NESPRESSO  =        0
let IK_CONNECT_TO_ANY_ROASTER =  false
let HTTP_LOGGING_ENABLED =       0
let IK_PROFILE_IMAGE_URL =      "https://firebasestorage.googleapis.com/v0/b/ikawa-app-dev.appspot.com/o/{{ENV}}%2Fprofile_images%2F{{UUID}}.png?alt=media"
let IK_FIREBASE_STORAGE =       "gs://ikawa-app-dev.appspot.com"
#else
let IK_CONNECT_TO_ANY_ROASTER =  false
let IK_PROFILE_IMAGE_URL =      "https://firebasestorage.googleapis.com/v0/b/ikawa-app.appspot.com/o/{{ENV}}%2Fprofile_images%2F{{UUID}}.png?alt=media"
let IK_FIREBASE_STORAGE =       "gs://ikawa-app.appspot.com"
#endif


let IK_MIN_FAN_SPEED     =  60.0
let IK_MAX_FAN_SPEED     =  100.0
let IK_MAX_TEMP_ABOVE    =  240.0
let IK_MAX_TEMP_BELOW    =  290.0
let IK_MIN_ROAST_POINTS  =  3
let IK_MIN_FAN_POINTS    =  2



//Profile
let IK_EMPTY_PROFILE_NAME              = "<No Name>"
let IK_CURRENT_DB_SCHEMA_VERSION       = 13
let IK_CURRENT_PROFILE_SCHEMA_VERSION  = 1
let IK_STANDARD_PROFILE_COUNT          = 1
let IK_STANDARD_PROFILE_ID             = "F306CC6268CE45AA936589DBF0DF7E0D"
let IK_PROFILE_NAME_MIN_CHAR           = 1

var USE_FAHRENHEIT: Bool {
    get {
      return UserDefaults.standard.bool(forKey: "useFahrenheit")
    }
}

//Errors
enum AppError: Error {
    case unknownError
    case connectionError
    case invalidCredentials
    case invalidRequest
    case notFound
    case invalidResponse
    case serverError
    case serverUnavailable
    case timeOut
    case unsuppotedURL
}
