//
//  IKChartVC.swift
//  IKAWApp
//
//  Created by Admin on 3/28/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Charts
import IKRoasterLib
import SnapKit
import Toast_Swift

class IKChartLogVC: UIViewController {
    var notes: IKRoastNotes?
    var profile: Profile?
    var editedProfile: Profile?
    @IBOutlet weak var coffeeNameLabel: UILabel!
    @IBOutlet weak var preheatLabel: UILabel!
    @IBOutlet weak var endTempLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var dtLabel: UILabel!
    @IBOutlet weak var chartViewContainer: UIView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var sendToRoastButton: UIButton!
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var sensorButton: UIButton!
    @IBOutlet weak var tableButton: UIButton!
    @IBOutlet weak var rorButton: UIButton!
    @IBOutlet weak var ccButton: UIButton!
    @IBOutlet weak var firstCrackButton: UIButton!
    @IBOutlet weak var secondCrackButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var tableEditView: UIView!
    @IBOutlet weak var sensorButtonView: UIView!
    @IBOutlet weak var rorView: UIView!
    @IBOutlet weak var colorChangeView: UIView!
    @IBOutlet weak var firstCrackView: UIView!
    @IBOutlet weak var secondCrackView: UIView!
    @IBOutlet weak var cancelButtonView: UIView!
    @IBOutlet weak var menuContainer: UIStackView!
    @IBOutlet weak var dtrContainerView: UIView!
    @IBOutlet weak var dtrLabel: UILabel!
    @IBOutlet weak var dtrButton: UIButton!
    @IBOutlet weak var dtrView: UIView!
    @IBOutlet weak var rorButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuContainerWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var archivedLabel: UILabel!
    @IBOutlet weak var restoreButton: UIButton!
    @IBOutlet weak var swipeRoastLogHeaderView: SwipeRoastLogHeaderView!
    @IBOutlet weak var sendProfileProgressView: ProgressIndicatorView!
    @IBOutlet weak var completeImageView: UIImageView!
    
    
    var sendingProfileToRoaster: Bool = false
    var tempChartView: IKTempChartView?
    
    private var buttonEditState: Bool = false
    //MARK: - Life cycle
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector:#selector(didLogout), name: NSNotification.Name.DidLogOut, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(degreeCelsiusChanged), name: NSNotification.Name.DegreeCelsiusChanged, object: nil)
        sensorButton.setImage(UIImage(named: (profile?.tempSensor == TempSensorBelow) ? "Exhaust" : "Inlet"), for: .normal)
        updateContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        RoasterManager.instance().delegates.add(self)
        prepareAppearance()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
        tabBarController?.tabBar.isHidden = false
        
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }

    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if (tempChartView != nil) {
            tempChartView?.viewWillTransition()
        }
        updateOrientationAppearance()
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProfileEditVC" {
            let vc = segue.destination as! ProfileEditVC
            vc.profile = profile
            vc.editedProfile = editedProfile
            vc.callback = {(profileHasChanged: Bool) in
                self.showTemperatureGraph()
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Actions
    
    @IBAction func restoreButtonTouchUpInside(_ sender: Any) {
        guard let profile = profile else {return}
        if AppCore.sharedInstance.profileManager.restoreProfile(profile: profile) {
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_RESTORE", label: "")
            if let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: profile.uuid) {
                self.updateContent()
                self.setupAppearanceForProfile()
                self.updateButtonsAppearanceWithState()
            }

        }
    }
    
    @IBAction func sendToRoastButtonTouchUpInside(_ sender: Any) {
        tempChartView!.removeHighlightForEditableDatasets()
        sendProfile(profile: self.profile)
        sendingProfileToRoaster = true
    }
    
    @IBAction func editButtonTouchUpInside(_ sender: Any) {
        if editedProfile != nil {
            goToViewState()
        } else {
            goToEditState()
        }
        
    }
    
    @IBAction func saveButtonTouchUpInside(_ sender: Any) {
        tempChartView!.removeHighlightForEditableDatasets()
        saveButton.isEnabled = false
        save {[unowned self] (profile) in
            if let vc = UIStoryboard(name: "Chart", bundle: nil).instantiateViewController(withIdentifier: "IKChartVC") as? IKChartVC {
                vc.profile = profile
                self.navigationController?.pushViewController(vc, animated: true)
            }
            

            
//            self.updateAppearance()
//
//            if let roastManager = RoasterManager.instance()?.roaster {
//                if roastManager.status.state == IK_STATUS_IDLE {
//                    self.sendToRoastButton.isHidden = true
//                }
//            }
        }
        self.saveButton.isEnabled = true
    }
    
    @IBAction func sensorButtonTouchUpInside(_ sender: Any) {
        guard editedProfile != nil else {
            return
        }
        
        if (editedProfile?.tempSensor == TempSensorBelow) {
            if ((editedProfile?.getHighestRoastPoint()?.temperature)! > Float(IK_MAX_TEMP_ABOVE)) {
                let alert = ButtonsPopupModalView()
                alert.layout = .vertical
                alert.title = NSLocalizedString("$exhaust_profile_temperature_error_title", comment: "")
                alert.subtitle = String(format: NSLocalizedString("$exhaust_profile_temperature_error_message", comment: ""), Int(IK_MAX_TEMP_ABOVE))
                alert.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                
                IKNotificationManager.sharedInstance.show(view: alert)
            } else {
                editedProfile?.tempSensor = TempSensorAbove
            }
        } else {
            editedProfile?.tempSensor = TempSensorBelow
        }
        updateSensorButtonAppearance()
        showTemperatureGraph()
    }
    
    @IBAction func rorButtonTouchUpInside(_ sender: Any) {
        if let tempChartView = self.tempChartView {
            tempChartView.isRORHidden = !tempChartView.isRORHidden
        }
        updateMarkerButtonsAppearance()
    }
    
    @IBAction func tapOnDTRContainerView(_ sender: Any) {
        dtrContainerView.isHidden = true
        dtrView.isHidden = false
        firstCrackView.isHidden = true
    }
    
    @IBAction func dtrButtonTouchUpInside(_ sender: Any) {
        dtrContainerView.isHidden = false
        dtrView.isHidden = true
        firstCrackView.isHidden = false
    }
    
    
    //MARK: - Markers
    
    @objc func degreeCelsiusChanged() {
        guard profile != nil else {return}
    }
    
    @objc func didLogout() {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    //MARK: - Private
    
    func updateContent() {
        if let profile = notes?.profile() {
            self.profile = profile
            self.title = profile.name
            var shortUuid = ""
            if let uuid = notes?.roast?.uuid, uuid.count >= 3 {
                let index = uuid.index(uuid.startIndex, offsetBy: 3)
                shortUuid = String(uuid[..<index])
            }
            swipeRoastLogHeaderView.populate(profile: profile, uuid:shortUuid, backButtonAction: {
                if (self.editedProfile != nil && (self.tempChartView!.graphWasEdited)) {
                    let confirmationDialogView = ButtonsPopupModalView()
                    confirmationDialogView.layout = .horisontal
                    confirmationDialogView.title = NSLocalizedString("$unsaved_changes", comment: "")
                    confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$cancel", comment: ""), type: .Cancel) {}
                    confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$leave", comment: ""), type: .Bordered) {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            })
        }
        
        if let notes = notes {
            coffeeNameLabel.text = notes.coffeeType
            coffeeNameLabel.letterSpace = 1
            preheatLabel.text = notes.preheatTempDescription().isEmpty ? "" : "Preheat".localized + ": " + notes.preheatTempDescription()
            endTempLabel.text = notes.endTempDescription().isEmpty ? "" : "End Temp".localized + ": " + notes.endTempDescription()
            endTimeLabel.text = notes.endTimeDescription().isEmpty ? "" : "End Time".localized + ": " + notes.endTimeDescription()
            dtLabel.text = notes.dtrTimeDescription().isEmpty ? "" : "DT".localized + ": " + notes.dtrTimeDescription()
            dtrButton.setTitle(notes.dtrDescription(format: "%.0f"), for: .normal)
            updateDTR()
            
        }
    }
    
    func prepareAppearance() {
        sendProfileProgressView.backgroundColor = UIColor.white
        dtrView.isHidden = true
        saveButton.layer.shadowColor = UIColor.black.cgColor
        saveButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        saveButton.layer.shadowOpacity = 0.3
        saveButton.layer.shadowRadius = 2.0
        //saveButton.layer.masksToBounds =  false
        saveButton.layer.cornerRadius = saveButton.frame.size.height/2
        
        cancelButton.layer.cornerRadius = cancelButton.frame.height / 2
        cancelButton.layer.borderWidth = 1
        cancelButton.layer.borderColor = UIColor.white.cgColor
        
        sendToRoastButton.layer.shadowColor = UIColor.black.cgColor
        sendToRoastButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        sendToRoastButton.layer.shadowOpacity = 0.3
        sendToRoastButton.layer.shadowRadius = 2.0
//sendToRoastButton.layer.masksToBounds =  false
        sendToRoastButton.titleLabel?.lineBreakMode = .byWordWrapping
        sendToRoastButton.titleLabel?.numberOfLines = 2
        sendToRoastButton.layer.cornerRadius = sendToRoastButton.frame.size.height/2
        
        guard let profile = profile else {return}
        
        setupAppearanceForProfile()
        tempChartView = IKTempChartView()
        tempChartView?.isCoolingHidden = true
        chartViewContainer.addSubview(tempChartView!)
        tempChartView!.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(self.chartViewContainer).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
        showTemperatureGraph()
        updateAppearance()
    }
    
    private func setupAppearanceForProfile() {
        restoreButton.layer.cornerRadius = restoreButton.bounds.height / 2
        if let profile = profile {
            if profile.archived {
                menuContainerWidthConstraint.constant = 0
                menuContainer.isHidden = true
                sendToRoastButton.isHidden = true
                archivedLabel.isHidden = false
                restoreButton.isHidden = false
            } else {
                menuContainerWidthConstraint.constant = 50
                menuContainer.isHidden = false
                archivedLabel.isHidden = true
                restoreButton.isHidden = true
            }
        }

    }
    
    func updateAppearance() {
        updateButtonsAppearanceWithState()
        updateBackButtonAppearance()
    }
    
    func updateOrientationAppearance() {
        let isLandscape = UIDevice.current.orientation.isFlat ? UIApplication.shared.statusBarOrientation.isLandscape : UIDevice.current.orientation.isLandscape
        if  isLandscape {
            tabBarController?.tabBar.isHidden = true
            if (tempChartView != nil) {
                tempChartView!.chartView.setVisibleXRangeMinimum(Double(4.5))
            }
        } else {
            tabBarController?.tabBar.isHidden = false
            if (tempChartView != nil) {
                tempChartView!.chartView.setVisibleXRangeMinimum(Double(2.5))
            }
        }
    }
    
    func updateStackView() {
        if buttonEditState != isEditState() {
            UIView.animate(withDuration: 0.2, animations: {
                self.editView.isHidden = self.isEditState()
                self.editView.alpha = self.isEditState() ? 0 : 1
                self.tableEditView.isHidden = !self.isEditState()
                self.tableEditView.alpha = !self.isEditState() ? 0 : 1
                self.sensorButtonView.isHidden = !self.isEditState()
                self.rorView.isHidden = false
                self.colorChangeView.isHidden = self.isEditState()
                self.colorChangeView.alpha = self.isEditState() ? 0 : 1
                self.secondCrackView.alpha = self.isEditState() ? 0 : 1
                self.secondCrackView.isHidden = self.isEditState()
                self.firstCrackView.isHidden = self.isEditState()
                
                self.cancelButton.isHidden = !self.isEditState()
                self.cancelButton.alpha = !self.isEditState() ? 0 : 1
                
                self.cancelButtonView.isHidden = !self.isEditState()
               // self.cancelButtonView.alpha = !self.isEditState() ? 0 : 1
                self.rorButtonTopConstraint.constant = self.isEditState() ? 20 : 40
                self.menuContainer.layoutIfNeeded()
            })
            buttonEditState = isEditState()
        }
        
        updateMarkerButtonsAppearance()
    }
    
    private func updateMarkerButtonsAppearance() {
        DispatchQueue.main.async {
            
            if let currentRoast = self.notes?.roast {
                self.ccButton.setImage(UIImage(named: currentRoast.colorChange != nil ? "ColorChange_Highlight" : "ColorChange_Active") , for: .normal)
                self.firstCrackButton.setImage(UIImage(named: currentRoast.firstCrack != nil ? "FirstCrack_Highlight" : "FirstCrack_Active") , for: .normal)
                self.secondCrackButton.setImage(UIImage(named: currentRoast.secondCrack != nil ? "SecondCrack_Highlight" : "SecondCrack_Active") , for: .normal)
            }
        }
        updateRorButtonAppearance()
    }
    
    private func updateRorButtonAppearance() {
        DispatchQueue.main.async {
            if let tempChartView = self.tempChartView {
                self.rorButton.setImage(UIImage(named: tempChartView.isRORHidden ? "RofR_Active" : "RofR_Highlight") , for: .normal)
            }
        }
    }
    
    func updateButtonsAppearanceWithState() {
        updateStackView()
        if isEditState() {
            saveButton.isHidden = false
            sendToRoastButton.isHidden = true
            return
        } else {
            if (RoasterManager.instance().roaster != nil && self.profile != nil && self.isRoasterProfile(profile: self.profile!)) {
                let state = RoasterManager.instance().roaster.status.state
                if state == IK_STATUS_ROASTING { //roasting
                    saveButton.isHidden = true
                    sendToRoastButton.isHidden = true
                    return
                } else {
                    saveButton.isHidden = true
                    sendToRoastButton.isHidden = true
                }
            } else {
                saveButton.isHidden = true
                if RoasterManager.instance().roaster == nil {
                    sendToRoastButton.isHidden = true
                } else if RoasterManager.instance().roaster != nil {
                    let state = RoasterManager.instance().roaster.status.state
                    sendToRoastButton.isHidden = (state != IK_STATUS_IDLE || (profile?.archived ?? true))
                }
                return
            }
        }
        
    }
    
    
    
    func updateSensorButtonAppearance()  {
        if let profile = editedProfile {
            if (profile.tempSensor == TempSensorBelow) {
                sensorButton.setImage(UIImage(named: "Exhaust"), for: .normal)
            } else {
                sensorButton.setImage(UIImage(named: "Inlet"), for: .normal)
            }
        }
        
        
        //        if isEditState() {
        //            if (RoasterManager.instance().roaster != nil && self.profile != nil && self.isRoasterProfile(profile: self.profile!)) {
        //                let state = RoasterManager.instance().roaster.status.state
        //                if state != IK_STATUS_IDLE { //roasting
        //                    sensorButton.isEnabled = false
        //                } else {
        //                    sensorButton.isEnabled = true
        //                }
        //            } else {
        //                sensorButton.isEnabled = true
        //            }
        //        } else {
        //            sensorButton.isEnabled = false
        //        }
    }
    
    func updateBackButtonAppearance() {
        swipeRoastLogHeaderView.hideBackButton(hide: navigationController?.viewControllers.count ?? 0 == 0)
    }
    
    func updateDTR() {
        guard let roast = notes?.roast, let firstCrackPoint = roast.firstCrack, let roastingTime = roast.lastRoastingPoint()?.time  else {
            dtrContainerView.isHidden = true
            return
        }
        dtrContainerView.isHidden = false
        dtrLabel.text = notes?.dtrDescription()
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, animations: {
                self.dtrContainerView.alpha = self.isEditState() ? 0 : 1
            })
        }
    }
    
    func updateChartIfNeeded() {
        if chartViewContainer.subviews.count > 0,  let chartView = chartViewContainer.subviews.last as? IKTempChartView {
            chartView.updateTempPoint(roast: notes?.roast)
            chartView.removeHighlightForRoastDatasets()
        }
    }
    
    func save(callback: @escaping (_ savedProfile: Profile?)->Void) {
        if let userId = AppCore.sharedInstance.firebaseManager.currentUser?.uid {
            editedProfile?.userId = userId
        }
        if (profile?.isReadOnly())! {
            AppCore.sharedInstance.profileManager.showSaveProfileAsCopyAlert(profile: editedProfile!, title: nil) { (saveState, profile) in
                if (saveState == .SAVED_AS || saveState == .OVERWRITTEN) {
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_EDIT", label: "")
                    let needSendToRoaster = self.isRoasterProfile(profile: self.profile!)
                    self.profile = profile
                    self.goToViewState()
                    if needSendToRoaster {
                        self.sendProfile(profile: self.profile)
                    }
                } else {}
                callback(profile)
            }
        } else {
            AppCore.sharedInstance.profileManager.saveProfile(profile: editedProfile!, existsText: NSLocalizedString("$how_to_save", comment: "")) { (saveState, profile) in
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_EDIT", label: "")
                if (profile != nil) {
                    let needSendToRoaster = self.isRoasterProfile(profile: self.profile!)
                    self.profile = profile
                    self.goToViewState()
                    if needSendToRoaster {
                        self.sendProfile(profile: self.profile)
                    }
                } else {
                    let confirmationDialogView = ButtonsPopupModalView()
                    confirmationDialogView.layout = .vertical
                    confirmationDialogView.title = NSLocalizedString("$profile_unsupported", comment: "")
                    confirmationDialogView.subtitle = NSLocalizedString("$inlet_sensor_profile_error_message", comment: "")
                    confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                }
                callback(profile)
            }
        }
    }
    
    func showTemperatureGraph() {
        tempChartView!.populateChart(profile:profile, editedProfile: editedProfile)
        if (self.editedProfile != nil) {
            if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile?.uuid) { //On machine already
                AppCore.sharedInstance.loggingManager.trackScreen(name: "Profile Edit On Roaster Screen")
            } else {
                AppCore.sharedInstance.loggingManager.trackScreen(name: "Profile Edit Screen")
            }
            tempChartView?.goToEditState()
        } else {
            if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile?.uuid) { //On machine already
                AppCore.sharedInstance.loggingManager.trackScreen(name: "Profile On Roaster Scene")
            } else {
                AppCore.sharedInstance.loggingManager.trackScreen(name: "Profile Screen")
            }
            tempChartView?.goToViewState()
        }
        updateChartIfNeeded()
    }
    
    func goToEditState() {
        if let editedProfile =  self.profile?.copy() as? Profile {
            sensorButton.setImage(UIImage(named: (profile?.tempSensor == TempSensorBelow) ? "Exhaust" : "Inlet"), for: .normal)
            self.editedProfile = editedProfile
            self.editedProfile?.uuid = AppCore.sharedInstance.profileManager.getRandomUuid()
            self.editedProfile?.parentUuid = self.profile?.uuid ?? ""
            self.editedProfile?.type = ProfileTypeUser
            showTemperatureGraph()
            updateButtonsAppearanceWithState()
        }
    }
    
    func goToViewState() {
        self.title = profile?.name
        editedProfile = nil
        showTemperatureGraph()
        updateButtonsAppearanceWithState()
    }
    
    func sendProfile(profile: Profile?) {
        guard profile != nil else {
            return
        }
        sendProfileProgressView.startAnimation()
        if (!RoasterManager.instance().roaster.hasBelowSensor() && profile?.tempSensor == TempSensorBelow) {
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$profile_unsupported", comment: "")
            confirmationDialogView.subtitle = NSLocalizedString("$inlet_sensor_profile_error_message", comment: "")
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                self.sendProfileProgressView.stopAnimation()
            }
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            return
        } else if (!RoasterManager.instance().roaster.hasAboveSensor() &&
            (profile?.tempSensor == TempSensorAbove || profile?.tempSensor == TempSensorAboveRobust)) {
            
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$profile_unsupported", comment: "")
            confirmationDialogView.subtitle = ""
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                self.sendProfileProgressView.stopAnimation()
            }
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            return
        }
        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_SEND", label: "")
        RoasterManager.instance().sendProfile(profile)
    }
    
    func isRoasterProfile(profile: Profile) -> Bool {
        if RoasterManager.instance().roaster != nil, let roasterProfile = RoasterManager.instance().profile {
            return profile.uuid == roasterProfile.uuid
        }
        return false
    }
    
    func isEditState() -> Bool {
        return editedProfile != nil
    }
}


extension IKChartLogVC : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        updateAppearance()
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        updateAppearance()
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        updateAppearance()
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
//        guard IKRoastManager.sharedInstance.currentRoast != nil else {
//            return
//        }
//
//        updateAppearance()
//        updateChartIfNeeded()
//        updateDTR()
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        if (self.sendingProfileToRoaster) {
            sendProfileProgressView.stopAnimation()
            
            UIView.animate(withDuration: 0.5, animations: {
                self.completeImageView.isHidden = false
            }, completion: { (success) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.completeImageView.isHidden = true
                }
            })
            
            self.sendingProfileToRoaster = false
            sendToRoastButton.isHidden = true
        }
        updateAppearance()
    }
}

