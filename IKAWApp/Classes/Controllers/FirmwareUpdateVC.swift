//
//  FirmwareUpdateVC.swift
//  IKAWApp
//
//  Created by Admin on 5/30/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class FirmwareUpdateVC: UIViewController {
    @IBOutlet weak var progressBackgroundImageView: UIImageView!
    @IBOutlet weak var progressBarrImageWidthConstraint: NSLayoutConstraint!
    
    var infoIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppCore.sharedInstance.loggingManager.trackScreen(name: "Firmware Update Screen")
        RoasterManager.instance().fwManager.firmwareUpdateVersion = Int32(IK_FIRMWARE_UPDATE_VERSION)
        //RoasterManager.instance().fwManager.delegate = self as FirmwareManagerDelegate
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RoasterManager.instance().fwManager.delegates.add(self)
        UIApplication.shared.isIdleTimerDisabled = true
        
        guard RoasterManager.instance().roaster != nil else {
            dismiss(animated: true) {}
            return
        }
        
        if (!RoasterManager.instance().fwManager.isUpdating()) {
            #if TARGET_PRO
            if (RoasterManager.instance().roaster.firmwareVersion < 20) {
                showInfoDialog()
            } else {
                RoasterManager.instance().flashFirmware()
            }
            #else
                RoasterManager.instance().flashFirmware()
            #endif
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isIdleTimerDisabled = false
        if (RoasterManager.instance().fwManager.delegates.contains(self)) {
            RoasterManager.instance().fwManager.delegates.remove(self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private methods
    func showInfoDialog() {
        var body = ""
        switch infoIndex {
        case 0:
            body = NSLocalizedString("$v20_update_message1", comment: "")
            break
        case 1:
            body = NSLocalizedString("$v20_update_message2", comment: "")
            break
        case 2:
            body = NSLocalizedString("$v20_update_message3", comment: "")
            break
        case 3:
            body = NSLocalizedString("$v20_update_message4", comment: "")
            break
        default:
            break
        }

        let confirmationDialogView = ButtonsPopupModalView()
        confirmationDialogView.layout = .vertical
        confirmationDialogView.title = NSLocalizedString("$v20_update_title", comment: "")
        confirmationDialogView.subtitle = body
        if (infoIndex == 3) {
            
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$update", comment: ""), type: .Bordered) {
                RoasterManager.instance().flashFirmware()
            }
            confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$cancel", comment: ""), type: .Cancel) {}

        } else {
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$next", comment: ""), type: .Bordered) {
                self.infoIndex = self.infoIndex + 1
                self.showInfoDialog()
            }
        }
        IKNotificationManager.sharedInstance.show(view: confirmationDialogView)

        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        #if TARGET_PRO
        return .lightContent
        #else
        return .default
        #endif
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FirmwareUpdateVC: FirmwareManagerDelegate {
    func firmwareProgressChanged(_ progress: Float) {
        progressBarrImageWidthConstraint.constant = CGFloat(progressBackgroundImageView.frame.width) * CGFloat(progress)
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    func firmwareUpdateStarted() {
        #if TARGET_PRO
        AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_SETTING_FIRMWARE_LAUNCH", label: "")
        #else
        AppCore.sharedInstance.loggingManager.trackUIAction(action: "firmware update start", label: "")
        #endif
        
    }
    func firmwareUpdateComplete() {
        #if TARGET_PRO
        AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_SETTING_FIRMWARE_END", label: "")
        #else
        AppCore.sharedInstance.loggingManager.trackUIAction(action: "firmware update complete", label: "")
        #endif
        
    }
    func firmwareUpdateFailed() {
        #if TARGET_PRO
        AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_SETTING_FIRMWARE_END", label: "")
        #else
        AppCore.sharedInstance.loggingManager.trackUIAction(action: "firmware update failed", label: "")
        #endif
        
    }
    func firmwareJumpedToApp() {
        // Goto UpdateSettingsScene scene
        let storyboard = UIStoryboard(name: "SettingsUpdate", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        
        addChild(vc!)
        self.view.addSubview((vc?.view)!)
        vc?.didMove(toParent: self)
        RoasterManager.instance().initRoaster()
        vc?.view.frame = self.view.frame
    }
}
