//
//  IKRoasterStatusesVC.swift
//  IKAWA-Home
//
//  Created by Serg on 24/03/2019.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class IKRoasterStatusesVC: UIViewController {
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var navigationBar: UIView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var roasterImageView: UIImageView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        #if TARGET_PRO
        prepareAppearance()
        #endif
    }
    
    private func prepareAppearance() {
        /*backgroundView.backgroundColor = .black
        navigationBar.backgroundColor = kColorDarkGrey1
        borderView.isHidden = true*/
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateStatus()
        NotificationCenter.default.addObserver(self, selector:#selector(didLogout), name: NSNotification.Name.DidLogOut, object: nil)
        RoasterManager.instance().delegates.add(self)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Actions
    @IBAction func menuButtonTouchUpInside(_ sender: Any) {
        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let nc = menuStoryboard.instantiateInitialViewController()
        present(nc!, animated: true) {
            
        }
    }
    
    //MARK: - Private
    
    @objc func didLogout() {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    func updateStatus() {
        if (IKInterface.instance().isBluetoothEnabled() || IK_SIMULATE_ROASTER) {
            statusLabel.text = NSLocalizedString("$not_connected_title", comment: "")
            statusLabel.letterSpace = 2
            statusImageView.image = UIImage(named: "lookingForRoaster")
            #if TARGET_PRO
            roasterImageView.alpha = 1
            #endif
        } else {
            statusLabel.text = NSLocalizedString("$bluetooth_off_title", comment: "")
            statusLabel.letterSpace = 2
            statusImageView.image = UIImage(named: "bluetoothDisabled")
            #if TARGET_PRO
            roasterImageView.alpha = 0.5
            #endif
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension IKRoasterStatusesVC : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        statusLabel.text = NSLocalizedString("$connecting_title", comment: "")
        statusLabel.letterSpace = 2
        statusImageView.image = UIImage(named: "connecting")
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
   
        if (IKInterface.instance().isBluetoothEnabled() || IK_SIMULATE_ROASTER) {
            statusLabel.text = NSLocalizedString("$not_connected_title", comment: "")
            statusLabel.letterSpace = 2
            statusImageView.image = UIImage(named: "lookingForRoaster")
        } else {
            statusLabel.text = NSLocalizedString("$bluetooth_off_title", comment: "")
            statusLabel.letterSpace = 2
            statusImageView.image = UIImage(named: "bluetoothDisabled")
        }
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        updateStatus()
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {

    }
    
}
