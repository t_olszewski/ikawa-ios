//
//  IKRoastNotesDetailVC.swift
//  IKAWApp
//
//  Created by Admin on 5/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class IKRoastNotesDetailVC: UIViewController {
    @IBOutlet weak var roundImageView: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var notes: IKRoastNotes?
    private var isNoteEdit: Bool = false
    private var isExportingToCropster = false
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        AppCore.sharedInstance.loggingManager.trackScreen(name: "Roast Notes Screen")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        title = NSLocalizedString("$roast_details", comment: "")
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1285303235, green: 0.1236336902, blue: 0.128546536, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        
        if let profile = notes?.profile() {
            profileName.text = profile.getNameWithType()
            profileName.letterSpace = 1
            roundImageView.image = UIImage(named: profile.tempSensor == TempSensorBelow ? "roundInlet" : "roundExhaust")
        }
        
        if let uuid = notes?.roast?.uuid, uuid.count >= 3 {
            let index = uuid.index(uuid.startIndex, offsetBy: 3)
            numberLabel.text = String(uuid[..<index])
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    
    func goToRoastNotesScreen() {
        let roastNotesStoryboard = UIStoryboard(name: "RoastNotes", bundle: nil)
        guard let editRoastDetailVC = roastNotesStoryboard.instantiateViewController(withIdentifier: "IKEditRoastDetailVC") as? IKEditRoastDetailVC else {
            return
        }
        editRoastDetailVC.notesText = notes?.notes
        if let profile = notes?.profile() {
            editRoastDetailVC.imageName = profile.tempSensor == TempSensorBelow ? "roundInlet" : "roundExhaust"
            editRoastDetailVC.profileName = profile.name
        }
        editRoastDetailVC.shortUuid = numberLabel.text ?? ""
        editRoastDetailVC.changeEditState = changeEditState
        self.navigationController?.pushViewController(editRoastDetailVC, animated: true)
    }
    
    // MARK: - Actions
    @IBAction func deleteRoastButtonTouchUpInside(_ sender: Any) {
        if AppCore.sharedInstance.roastNotesManager.deleteRoastNotes(roastNotes: notes!) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func backButtonTouchUpInside(_ sender: Any) {
        if navigationController != nil {
            navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "IKChartLogVC") {
            let vc = segue.destination as! IKChartLogVC
            vc.notes = notes
        }
    }
    
    @IBAction func shareButtonTouchUpInside(_ sender: Any) {
        AppCore.sharedInstance.roastManager.shareRoastLog(roastNotes: notes!)
    }
    

    //MARK: Private
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            } else {
                self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: endFrame?.size.height ?? 0.0, right: 0)
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    @objc func changeEditState(notesText: String, callBack: @escaping(Bool) -> ()) {
        guard let notes = notes else {
            callBack(false)
            return
        }
        notes.notes = notesText
        notes.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
        DispatchQueue.global().async {
            if AppCore.sharedInstance.roastNotesManager.saveRoastNotes(roastNotes: notes, needReloadData: true) {
                DispatchQueue.main.sync {
                    self.tableView.reloadData()
                    callBack(true)
                }
            } else {
                DispatchQueue.main.sync {
                    callBack(false)
                }
            }
        }
    }
}


extension IKRoastNotesDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let roast = notes?.roast, let notes = notes else {
            return UITableViewCell()
        }
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DateCell", for: indexPath)
            let timeLabel = cell.viewWithTag(100) as! UILabel
            let roasterIdLabel = cell.viewWithTag(200) as! UILabel
            let date =  Date(timeIntervalSince1970: roast.date!)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy   HH:mm"
            timeLabel.text = dateFormatter.string(from: date)
            if let machineUuid = notes.roast?.machineUuid {
                roasterIdLabel.text = "$roaster_notes".localized + " \(machineUuid)"
            }
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProportionCell", for: indexPath)
            let dryingLabel = cell.viewWithTag(100) as! UILabel
            let maillardLabel = cell.viewWithTag(200) as! UILabel
            let dtrLabel = cell.viewWithTag(300) as! UILabel
            dryingLabel.text = notes.dryingDescription()
            maillardLabel.text = notes.maillardDescription()
            dtrLabel.text = notes.dtrDescription()
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CoffeeNameCell", for: indexPath)
            let coffeeNameTextField = cell.viewWithTag(100) as! UITextField
            coffeeNameTextField.delegate = self
            coffeeNameTextField.text = notes.coffeeType == "" ? "$enter_coffee_name".localized : notes.coffeeType
            coffeeNameTextField.keyboardAppearance = .dark
            
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as? IKRoastNotesInfoCell else {
                return UITableViewCell()
            }
            cell.populate(notes: notes)
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "GraphCell", for: indexPath)
            if let imageView = cell.viewWithTag(20) as? UIImageView, let roast = notes.roast {
                imageView.image = GetImageFromRoast(roast: roast, size: imageView.frame.size)
                
            }
            return cell
        case 5:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NotesCell", for: indexPath) as? IKRoastNotesCell else {
                return UITableViewCell()
            }
            cell.populate(notes: notes)
            return cell
        case 6:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddImageCell", for: indexPath) as? IKRoastLogAddImageCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            cell.populate(notes: notes)
            return cell
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExportCSVCell", for: indexPath)
            return cell
        case 8:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExportToCropsterCell", for: indexPath)
            let label = cell.viewWithTag(100) as? UILabel
            let imageView = cell.viewWithTag(200) as? UIImageView
            label?.textColor = AppCore.sharedInstance.cropsterManager.isInitialized ? .white : kColorLightGrey3
            imageView?.alpha = AppCore.sharedInstance.cropsterManager.isInitialized ? 1 : 0
            return cell
        case 9:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShareProfileCell", for: indexPath)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let notes = notes else {
            return
        }
        switch indexPath.row {
        case 0: break
        
        case 1: break
            
        case 2:
        
        let cell = tableView.cellForRow(at: indexPath)
        if let coffeeNameLabel = cell?.viewWithTag(100) as? UITextField {
            if !coffeeNameLabel.isFirstResponder {
                coffeeNameLabel.becomeFirstResponder()
            }
        }
        
        case 4:
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_VIEW_ROAST", label: "")
            self.performSegue(withIdentifier: "IKChartLogVC", sender: nil)
            
            
        case 5:
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_ADD_NOTES", label: "")
            goToRoastNotesScreen()
            
        //case 6:
        case 7:
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_EXPORT_CSV", label: "")
            AppCore.sharedInstance.roastNotesManager.exportData(notes: notes) //export as CSV
        case 8:
            guard !isExportingToCropster else {
                return
            }
            if AppCore.sharedInstance.cropsterManager.isInitialized {
                AppCore.sharedInstance.cropsterManager.getMachinesPopupShow({[weak self] (complete) in
                    self?.view.makeToastActivity(.center)
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_EXPORT_CROPSTER", label: "")
                    self?.isExportingToCropster = true
                    AppCore.sharedInstance.cropsterManager.exportToCropster(roastLog: notes, callback: { (error) in
                        self?.isExportingToCropster = false
                        self?.view.hideToastActivity()
                        if let error = error {
                            let alertView = ButtonsPopupModalView()
                            alertView.layout = .vertical
                            alertView.title = NSLocalizedString("$cropster_error_title", comment: "")
                            alertView.subtitle = error.localizedDescription
                            alertView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                            IKNotificationManager.sharedInstance.show(view: alertView)
                        }
                    })
                })
            }
        case 9:
            if let profile = notes.profile() {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_SHARE", label: "")
                AppCore.sharedInstance.profileManager.shareProfile(profile: profile)
            }
            break

        default:
            return
        }

    }
}

extension IKRoastNotesDetailVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.backgroundColor = kColorHighlightedTextField//2
        textField.selectAll(nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.backgroundColor = .clear
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.returnKeyType==UIReturnKeyType.done) {
            textField.backgroundColor = .clear
            if let coffeeType = textField.text, let notes = notes {
                if notes.coffeeType != coffeeType {
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_COFFEE_NAME", label: "")
                }
                notes.coffeeType = coffeeType
                notes.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
                _ = AppCore.sharedInstance.roastNotesManager.saveRoastNotes(roastNotes: notes, needReloadData: true)
                if coffeeType.isEmpty {
                    textField.text = "$enter_coffee_name".localized
                }
            } else {
                textField.text = notes?.coffeeType == "" ? "$enter_coffee_name".localized : notes?.coffeeType
            }
            self.view.endEditing(true)
            return true
        } else {
            return false
        }
    }
}

extension IKRoastNotesDetailVC: IKRoastLogAddImageCellDelegate {
    func stateChanged(cell: IKRoastLogAddImageCell) {
            tableView.beginUpdates()
            tableView.endUpdates()
    }
}
