//
//  IKEditRoastDetailVC.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 5/21/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit

class IKEditRoastDetailVC: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var roundImageView: UIImageView!
    @IBOutlet weak var numberOfRoastLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var profileNameLabel: UILabel!
    
    var changeEditState: ((_ notesText: String, _ callBack: @escaping(_ success: Bool)->()) -> ())?
    var notesText: String?
    var shortUuid: String = ""
    var imageName: String = "roundExhaust"
    var profileName: String?
    var saved = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.text = notesText
        textView.delegate = self
        textView.keyboardAppearance = .dark
        let toolBar = KeyboardInputView.instanceFromNib()
        toolBar.backgroundColor = kColorLightGrey1
        toolBar.doneButton.setTitleColor(kColorDarkGrey1, for: .normal)
        
        toolBar.doneButton.titleLabel?.font =  UIFont(name: "AvenirNext-Medium", size: 14)
        toolBar.addConfinguration(action: { [weak self] (sender) in
            self?.view.endEditing(true)
        })
        textView.inputAccessoryView = toolBar
        
        navigationController?.navigationBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(updateTextView), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateTextView), name: UIResponder.keyboardWillHideNotification, object: nil)
        titleLabel.text = "$notes".localized
        titleLabel.letterSpace = 1
        profileNameLabel.text = profileName ?? ""
        profileNameLabel.letterSpace = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        roundImageView.image = UIImage(named: imageName)
        numberOfRoastLabel.text = shortUuid
        saveButton.layer.borderWidth = 1
        saveButton.layer.borderColor = kColorDarkGrey2.cgColor
        saveButton.layer.cornerRadius = saveButton.frame.height/2
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    //MARK: Actions
    
    @IBAction func backAction(_ sender: Any) {
        if !saved {
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.title = "$exit_without_saving_changes".localized
            confirmationDialogView.setUpButton(index: .First, title: "$exit".localized, type: .Cancel) {
                self.navigationController?.popViewController(animated: true)
            }
            confirmationDialogView.setUpButton(index: .Second, title: "$save_up".localized, type: .Filled) {
                self.save()
            }
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func saveAction(_ sender: Any) {
        save()
    }
    
    private func save() {
        self.view.makeToastActivity(.center)
        view.endEditing(true)
        let text = self.textView.text ?? ""
        changeEditState?(text, {[weak self] (success) in
            if success {
                self?.view.hideToastActivity()
                self?.navigationController?.popViewController(animated: true)
                self?.saved = true
            } else {
                self?.view.hideToastActivity()
                let confirmationDialogView = ButtonsPopupModalView()
                confirmationDialogView.title = NSLocalizedString("$save_failed", comment: "")
                confirmationDialogView.setUpButton(index: .Third, title: NSLocalizedString("$ok", comment: ""), type: .Cancel) {}
                IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            }
        })
 
        
        /*
        let serialQueue = DispatchQueue(label: "SaveQueue")
        serialQueue.async { [weak self] in
            if self?.changeEditState?(text) ?? false {
                self?.saved = true
            } else {
                DispatchQueue.main.sync {
                    confirmationDialogView.title = NSLocalizedString("$save_failed", comment: "")
                    confirmationDialogView.setUpButton(index: .Third, title: NSLocalizedString("$ok", comment: ""), type: .Cancel) {}
                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                }
            }
        }*/
    }
    
    @objc private func updateTextView(notification: Notification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        
        guard let keyboardEndFrameCoordinates = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        let keyboardEndFrame = self.view.convert(keyboardEndFrameCoordinates, to: view.window)
        if notification.name == UIResponder.keyboardWillHideNotification {
            textView.contentInset = UIEdgeInsets.zero
        } else {
            textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: (keyboardEndFrame.height/2) - 10, right: 0)
            textView.scrollIndicatorInsets = textView.contentInset
        }
        
        textView.scrollRangeToVisible(textView.selectedRange)
    }
    
}

extension IKEditRoastDetailVC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars <= 2000
    }
    
    func textViewDidChange(_ textView: UITextView) {
        saved = false
    }
    
    
}
