//
//  IKLoginVC.swift
//  IKAWA-Home
//
//  Created by Никита on 10/10/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class IKLoginVC: UIViewController {
    let userWhichWatchedTutorial = "userWhichWatchedTutorial"
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var signInLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var dontHaveAccountLabel: UILabel!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var agreeTermsButton: UIButton!
    var agreeTerms = false
    private let attributes = [
        NSAttributedString.Key.foregroundColor: kColorLightGrey3,
        NSAttributedString.Key.font : UIFont(name: "AvenirNext-Regular", size: 14)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        AppCore.sharedInstance.loggingManager.trackScreen(name: "Sign-in Screen")
        loginView.layer.shadowColor = UIColor.black.cgColor
        loginView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        loginView.layer.shadowRadius = 3.0
        loginView.layer.shadowOpacity = 0.5

        #if TARGET_HOME
            emailTextField.createBottomLine()
            passwordTextField.createBottomLine()
            signInButton.isEnabled = false
            signInButton.alpha = 0.5
        #else
        
        emailTextField.setLeftPaddingPoints(17)
        passwordTextField.setLeftPaddingPoints(17)
        
        signInButton.isEnabled = false
        signInButton.alpha = 0.5
        #endif
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        signInButton.layer.cornerRadius = signInButton.frame.height / 2
        signInButton.layer.borderWidth = 2
        

        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let lastUsedEmail = UserDefaults.standard.string(forKey: "lastUsedEmail"), !lastUsedEmail.isEmpty {
            emailTextField.text = UserDefaults.standard.string(forKey: "lastUsedEmail")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func forgotPasswordButtonTouchUpInside(_ sender: Any) {
        guard let email = emailTextField.text else {
            return
        }
        #if TARGET_PRO
        if AppCore.sharedInstance.firebaseManager.validateEmailRegEx(email: email) {
            UserDefaults.standard.set(email, forKey: "lastUsedEmail")
        }
        self.performSegue(withIdentifier: "IKForgotPasswordVC", sender: nil)
        #else
        AppCore.sharedInstance.firebaseManager.firebaseResetPassword(email: email, callback: {[weak self] (success, message) in
            self?.view.hideToastActivity()
            }, makeToastActivity: { [weak self] in
                self?.view.makeToastActivity(.center)
        })
        #endif
    }
    
    @IBAction func agreeButtonTouchUpInside(_ sender: Any) {
        if agreeTerms {
            agreeTerms = false
            agreeTermsButton.setImage(UIImage(named: "DeselectedPointIcon"), for: .normal)
            signInButton.isEnabled = false
            signInButton.alpha = 0.5
        } else {
            agreeTerms = true
            agreeTermsButton.setImage(UIImage(named: "selectedPointIcon"), for: .normal)
            signInButton.isEnabled = true
            signInButton.alpha = 1
        }
    }
    
    @IBAction func termsConditionsURLTouchUpInside(_ sender: Any) {
        guard let url = URL(string: kTermsAndConditions) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }

    
    //MARK: - Private
    
    func validateEmail() -> Bool {
        guard let email = emailTextField.text  else {
            return false
        }
        if (AppCore.sharedInstance.firebaseManager.validateEmailRegEx(email: email)) {
            #if TARGET_HOME
            self.emailTextField.textColor = kColorDarkGrey2
            #endif
        } else {
            self.emailTextField.textColor = UIColor.red
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$invalid_email_title", comment: "")
            confirmationDialogView.subtitle = NSLocalizedString("$invalid_email_message", comment: "")
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            confirmationDialogView.show(animated: true) {
            }
        }
        
        return AppCore.sharedInstance.firebaseManager.validateEmailRegEx(email: email)
    }
    
    func setupEmailPlaceholder() {
        emailTextField.attributedPlaceholder = NSAttributedString(string: "$login_email".localized, attributes: attributes as [NSAttributedString.Key : Any])
    }
    
    func setupPasswordPlaceholder() {
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "$login_password".localized, attributes: attributes as [NSAttributedString.Key : Any])
    }
    
    func validatePassword() -> Bool {
        let valid = (self.passwordTextField.text?.count ?? 0) > 7
        if (valid) {
            #if TARGET_HOME
            self.passwordTextField.textColor = kColorDarkGrey2
            #endif
        } else {
            self.passwordTextField.textColor = UIColor.red
            
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$password_too_short_title", comment: "")
            confirmationDialogView.subtitle = NSLocalizedString("$password_too_short_message", comment: "")
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            confirmationDialogView.show(animated: true) {
            }
            //IKNotificationManager.sharedInstance.show(view: confirmationDialogView)

        }
        return valid
    }
    
    
    func validateInput() -> Bool {
        if (validateEmail() && validatePassword()) {
            return true
        } else {
            return false
        }
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            } else {
                scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: endFrame?.size.height ?? 0.0, right: 0)
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    

    
    func saveNameIfNeeded(callback:@escaping ((_ error: Error?)->Void)) {
        
        guard let accountRegistrationParameters = UserDefaults.standard.dictionary(forKey: "accountRegistrationParameters"), let email = accountRegistrationParameters["email"] as? String, email.uppercased() == emailTextField.text?.uppercased() else {
            callback(nil)
            return
        }
        
        UserDefaults.standard.set(nil, forKey: "accountRegistrationParameters")
        
        guard AppCore.sharedInstance.firebaseManager.currentUser != nil else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "user not initialized"])
            callback(error)
            return
        }
        
        let firstName = accountRegistrationParameters["firstName"] as? String
        let lastName = accountRegistrationParameters["lastName"] as? String
        let accountName = accountRegistrationParameters["accountName"] as? String
        let userType = accountRegistrationParameters["userType"] as? Int
        
        if let imageData = accountRegistrationParameters["image"] as? Data, let image = UIImage(data: imageData) {
            AppCore.sharedInstance.firebaseManager.uploadAvatarImage(image: image, callback: { (success, message) in })
        }
        
        AppCore.sharedInstance.loggingManager.updateUserInfo(firstName: firstName ?? "", lastName: lastName ?? "", accountName: accountName ?? "", userType: userType, callback: {(success, message) in
            self.view.hideToast()
            if !success {
                if let message = message {
                    let popup = ButtonsPopupModalView()
                    popup.layout = .vertical
                    popup.title =  message
                    popup.subtitle = ""
                    popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    
                    IKNotificationManager.sharedInstance.show(view: popup)
                    let error = NSError(domain:"com.ikawa", code:401, userInfo: [NSLocalizedDescriptionKey: message])
                }
                let error = NSError(domain:"com.ikawa", code:401, userInfo: [NSLocalizedDescriptionKey: message])
                callback(error)
                return
                
            } else {
                AppCore.sharedInstance.loggingManager.sendUser()
                callback(nil)
            }
        })
    }
    
}

extension IKLoginVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            passwordTextField.resignFirstResponder()
        }
        return true
    }
}
