//
//  SettingsUpdateVC.swift
//  IKAWApp
//
//  Created by Admin on 6/26/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class SettingSet: RoasterSetting {
    var retries = 0
}

class SettingsUpdateVC: UIViewController {

    @IBOutlet weak var progressBackgroundImageView: UIImageView!
    @IBOutlet weak var progressBarrImageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var topLabel: UILabel!
    
    var roasterVoltage = RoasterVoltageUnset
    var receivedInfo = false
    var settingsToApply = [SettingSet]()
    var nSettings = 0
    var counter = 0
    // MARK: - life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppCore.sharedInstance.loggingManager.trackScreen(name: "Update Settings Screen")
        
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        #if TARGET_PRO
        AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_SETTING_SETTING_LAUNCH", label: "")
        #endif
        RoasterManager.instance().delegates.add(self)
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isIdleTimerDisabled = true
//         if (RoasterManager.instance().roaster.firmwareVersion < 20) || RoasterManager.instance().fwManager.firmwareUpdateVersion < 20 {
//            showVoltageDialog()
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //loadSettings() //remove
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isIdleTimerDisabled = false
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - business
    func showVoltageDialog() {
        let confirmationDialogView = ButtonsPopupModalView()
        confirmationDialogView.layout = .vertical
        confirmationDialogView.title = NSLocalizedString("$roaster_voltage_title", comment: "")
        confirmationDialogView.subtitle = NSLocalizedString("$roaster_voltage_message", comment: "")
        confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("220 – 240V", comment: ""), type: .Bordered) {
            self.roasterVoltage = RoasterVoltage230
            if self.receivedInfo {
                self.loadSettings()
            }
        }
        confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("110 – 130V", comment: ""), type: .Filled) {
            self.roasterVoltage = RoasterVoltage120
            if self.receivedInfo {
                self.loadSettings()
            }
        }
        
        IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
    }
    
    func fail() {
        #if TARGET_PRO
        AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_SETTING_SETTING_END", label: "")
        #endif
        let confirmationDialogView = ButtonsPopupModalView()
        confirmationDialogView.layout = .vertical
        confirmationDialogView.title = NSLocalizedString("$update_failed_title", comment: "")
        confirmationDialogView.subtitle = NSLocalizedString("$update_failed_message", comment: "")
        confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
        IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
        UserDefaults.standard.set(false, forKey: "repressFirmwareUpdateMessage")
    }
    
    func finish() {
        #if TARGET_PRO
        AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_SETTING_SETTING_END", label: "")
        #endif
        RoasterManager.instance()?.reboot()
        let key = "firmwareVersionReminded_" + RoasterManager.instance().roaster.name
        UserDefaults.standard.set(IK_FIRMWARE_UPDATE_VERSION, forKey: key)
        UserDefaults.standard.set(false, forKey: "repressFirmwareUpdateMessage")
        dismiss(animated: true) {
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$update_complete_title", comment: "")
            confirmationDialogView.subtitle = ""
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}

            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
        }
    }
    
    func loadSettings() {
        guard settingsToApply.count == 0 else {
            return
        }
        
        let filePath = Bundle.main.url(forResource: "Settings", withExtension: "csv", subdirectory: "firmware")!
        do {
            let text = try String(contentsOf: filePath, encoding: .utf8)
            parseSettingsData(text: text)
        }
        catch {
            print("can't read file")
            return
        }
    }

    func parseSettingsData(text: String) {

        var defaultColumn = 0
        guard let roaster = RoasterManager.instance().roaster else {
            fail()
            return
        }
        let roasterType = roaster.type
        let fwVersion = roaster.firmwareVersion
        if (fwVersion == 19) {
            defaultColumn = 8
        } else if (fwVersion > 19) {
            if (roasterType.rawValue <= RoasterTypeV3.rawValue) {
                defaultColumn = 9;
            } else {
                #if TARGET_PRO
                defaultColumn = 10;
                #else
                defaultColumn = 11;
                #endif
            }
        }
        
        
        let lines = text.components(separatedBy: "\n")
        //skip first line
        var firstLine = true
        for (_, line) in lines.enumerated() {
            
            var line_ = line.replacingOccurrences(of: " ", with:"")
            line_ = line_.replacingOccurrences(of: "\r", with:"")
            line_ = line_.replacingOccurrences(of: "\n", with:"")
            line_ = line_.replacingOccurrences(of: "\t", with:"")
            if (firstLine) {
                firstLine = false
                continue
            }
            let elements = line_.components(separatedBy: ",")
            
            //skip if there is no value
            if (defaultColumn >= elements.count || elements[defaultColumn].count == 0 || elements[0].count == 0)  {continue}
            
            let type = elements[2]
            let settingId = Int32(elements[0])
            var value = Float(elements[defaultColumn])
            var overrideColumn = 0
            
            
            switch (roaster.aboveSensorType) {
            case AboveSensorTypeRobust:
                overrideColumn = 3
                break
            case AboveSensorTypeFast:
                overrideColumn = 4
                break
            case AboveSensorTypeUnset:
                break
            default:
                break
            }
            
            
            if (overrideColumn == 0 || elements[overrideColumn].count == 0) {
                switch (roasterVoltage) {
                case RoasterVoltage100:
                    overrideColumn = 5
                    break
                case RoasterVoltage120:
                    overrideColumn = 6
                    break
                case RoasterVoltage230:
                    overrideColumn = 7
                    break
                case RoasterVoltageUnset:
                    break
                default:
                    break
                }
            }
            
            
            if (overrideColumn != 0 && elements[overrideColumn].count > 0) {
                value = Float(elements[overrideColumn])
            }
            
            let setting = SettingSet()
            setting.settingsId = settingId!
            setting.value = NSNumber(value: value ?? 0)
            
            if (type == "uint8") {
                setting.type = SettingsTypeUint8
            } else if (type == "uint16") {
                setting.type = SettingsTypeUint16
            } else if (type == "uint32") {
                setting.type = SettingsTypeUint32
            } else if (type == "float") {
                setting.type = SettingsTypeFloat
            }
            settingsToApply.append(setting)
        }
        
        nSettings = settingsToApply.count
        print("Applying %d settings", nSettings);
        
        topLabel.text = String(format: NSLocalizedString("$updating_settings", comment: ""), 0, nSettings)
        sendSettings()
  }
 


func sendSettings() {
    for setting in settingsToApply {
        counter = counter + 1
        print(counter, setting.settingsId, setting.value!)
        RoasterManager.instance().updateSetting(setting.settingsId, withValue: setting.value!)
    }
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SettingsUpdateVC : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
    }
    
    func roaster(_ roaster: Roaster!, didReceiveSetting setting: Int32, withValue value: NSNumber!, success: Bool) {
        guard let it = settingsToApply.first(where: {$0.settingsId == setting}) else {
            return
        }
        
        if (!success) {
            if let index = settingsToApply.index(of:it) {
                settingsToApply.remove(at: index)
                NSLog("Setting %d updated", setting)
            }
        } else {
            let expectedValue = it.value
            
            if (fabs((expectedValue?.floatValue)! - value.floatValue) < 0.002) {
                if let index = settingsToApply.index(of:it) {
                    settingsToApply.remove(at: index)
                    NSLog("Setting %d updated", setting)
                }
                
            } else {
                it.retries = it.retries + 1
                if (it.retries < 3) {
                    RoasterManager.instance().updateSetting(it.settingsId, withValue: it.value!)
                    NSLog("Setting %d mismatch! writing value %f", setting, it.value.floatValue);
                } else {
                    if let index = settingsToApply.index(of:it) {
                        settingsToApply.remove(at: index)
                        NSLog("Setting %d failed", setting)
                    }
                }
            }
        }
        
            topLabel.text = String(format: NSLocalizedString("$updating_settings", comment: ""), nSettings - settingsToApply.count, nSettings)
            let progress = Double(nSettings - settingsToApply.count) / Double(nSettings)
            progressBarrImageWidthConstraint.constant = CGFloat(progressBackgroundImageView.frame.width) * CGFloat(progress)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            
            if (settingsToApply.count == 0) {
                finish()
            }
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        receivedInfo = true
        if (RoasterManager.instance().roaster.firmwareVersion < 20) || RoasterManager.instance().fwManager.firmwareUpdateVersion < 20 {
            if (roasterVoltage != RoasterVoltageUnset) {
                loadSettings()
            } else {
                finish()
            }
        } else {
            guard let roaster = RoasterManager.instance().roaster else {
                fail()
                return
            }
            if (roaster.roasterVoltage == RoasterVoltageUnset) {
                showVoltageDialog()
            } else {
                roasterVoltage = roaster.roasterVoltage
                loadSettings()
            }
        }
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        
    }
    
    func roasterDidDidReceiveFirmwareVersion(_ roaster: Roaster!) {
    }
}


