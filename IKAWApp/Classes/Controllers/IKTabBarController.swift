//
//  IKTabBarController.swift
//  IKAWA-Home
//
//  Created by Admin on 10/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import MaterialComponents
import IKRoasterLib

enum TabBarItem: Int {
    case home = 0
    case roast = 1
    #if TARGET_HOME
    case shop = 2
    case addProfile = 3
    #else
    case roastLog = 2
    #endif
}

class IKTabBarController: UITabBarController {
    // Create a bottom navigation bar to add to a view.
    let bottomNavBar = MDCBottomNavigationBar()
    
    var tabBarItem1: UITabBarItem!
    var tabBarItem2: UITabBarItem!
    var tabBarItem3: UITabBarItem!
    var tabBarItem4: UITabBarItem!
    
    static let TabBarHeight: CGFloat = 156
    
    func shouldAutorotate() -> Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        //if UIDevice.current.userInterfaceIdiom == .phone {
            if let nc = selectedViewController as? UINavigationController, nc.topViewController is IKChartVC {
                return .all
            } else {
                return .portrait
            }
//        } else {
//            return .all
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RoasterManager.instance().delegates.add(self)
        bottomNavBar.delegate = self
        #if TARGET_HOME
        AppTheme.applyTheme(to: bottomNavBar)
        #endif
        bottomNavBar.itemTitleFont = UIFont(name: kFontAvenirNextMedium, size: 10)!
        bottomNavBar.itemsContentVerticalMargin = 5
        bottomNavBar.itemsContentInsets = UIEdgeInsets.init(top: 5, left: 0, bottom: 2, right: 0)
        // Always show bottom navigation bar item titles.
        bottomNavBar.titleVisibility = .always
        bottomNavBar.layer.borderWidth = 1
        #if TARGET_HOME
        bottomNavBar.layer.borderColor = kColorLightGrey1.cgColor
        #endif
        
        // Cluster and center the bottom navigation bar items.
        bottomNavBar.alignment = .centered
        
        // Add items to the bottom navigation bar.
        
        #if TARGET_HOME
         bottomNavBar.selectedItemTintColor = kColorRed
        tabBarItem1 = UITabBarItem(title: NSLocalizedString("$recipes", comment: ""), image: UIImage(named: "Home"), tag: TabBarItem.home.rawValue)
        tabBarItem2 = UITabBarItem(title: NSLocalizedString("$roast", comment: ""), image: UIImage(named: "Roast"), tag: TabBarItem.roast.rawValue)
        tabBarItem3 = UITabBarItem(title: NSLocalizedString("$shop", comment: ""), image: UIImage(named: "Shoping-Busket"), tag: TabBarItem.shop.rawValue)
        tabBarItem4 = UITabBarItem(title: NSLocalizedString("$add", comment: ""), image: UIImage(named: "Plus"), tag: TabBarItem.addProfile.rawValue)
        bottomNavBar.items = [tabBarItem1, tabBarItem2, tabBarItem3, tabBarItem4]
        #else
         bottomNavBar.selectedItemTintColor = kColorWhite
        tabBarItem1 = UITabBarItem(title: NSLocalizedString("$library", comment: ""), image: UIImage(named: "Home"), selectedImage:UIImage(named: "HomeSelected"))
        tabBarItem2 = UITabBarItem(title: NSLocalizedString("$menu_roaster", comment: ""), image: UIImage(named: "OnRoaster"), selectedImage:UIImage(named: "OnRoasterSelected"))
        tabBarItem3 = UITabBarItem(title: NSLocalizedString("$menu_roastlog", comment: ""), image: UIImage(named: "RoastLog"), selectedImage: UIImage(named: "RoastLogSelected"))
        bottomNavBar.items = [tabBarItem1, tabBarItem2, tabBarItem3]
        #endif
        
        selectItem(index: TabBarItem.home.rawValue)
        
        #if TARGET_HOME
        bottomNavBar.unselectedItemTintColor = kColorDarkGrey2
        #else
        bottomNavBar.unselectedItemTintColor = kColorWhite
        bottomNavBar.layer.borderWidth = 0
        bottomNavBar.backgroundColor = kColorDarkGrey1
        #endif
        // Select a bottom navigation bar item.
        bottomNavBar.selectedItem = tabBarItem1;
        tabBar.addSubview(bottomNavBar)
        NotificationCenter.default.addObserver(self, selector:#selector(didLogout), name: NSNotification.Name.DidLogOut, object: nil)
    }
    
    func layoutBottomNavBar() {
        let size = bottomNavBar.sizeThatFits(view.bounds.size)
        var bottomNavBarFrame = CGRect(x: 0,
                                       y: 0,
                                       width: size.width,
                                       height: size.height)
        if #available(iOS 11.0, *) {
            bottomNavBarFrame.size.height += view.safeAreaInsets.bottom
        }
        bottomNavBar.frame = bottomNavBarFrame
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        var newFrame = tabBar.frame
        if #available(iOS 11, *) {
            
            newFrame.size.height = 56 + view.safeAreaInsets.bottom
            newFrame.origin.y = view.frame.size.height - newFrame.size.height
        } else {
            newFrame.size.height = 56
            newFrame.origin.y = view.frame.size.height - newFrame.size.height
        }
        tabBar.frame = newFrame
        layoutBottomNavBar()
        tabBar.bringSubviewToFront(bottomNavBar)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Public
    func highlightTab(index: Int) {
        bottomNavBar.selectedItem = [tabBarItem1, tabBarItem2, tabBarItem3, tabBarItem4][index]
    }
    
    func highlightTabIndex() -> Int? {
        guard let selectedItem = bottomNavBar.selectedItem else {
            return nil
        }
        return bottomNavBar.items.index(of: selectedItem)
    }
    
    func selectItem(index: Int) {
        bottomNavBar.selectedItem = [tabBarItem1, tabBarItem2, tabBarItem3, tabBarItem4][index]
        
        if let vc = viewControllers?[index] {
            selectedViewController = vc
        }
    }
    
    func deselectItem() {
        bottomNavBar.selectedItem = nil
    }
    
    func openArchivedProfile() {
        let archivedStoryboard = UIStoryboard(name: "ArchivedProfile", bundle: nil)
        let archivedViewController = archivedStoryboard.instantiateViewController(withIdentifier: "ArchivedProfileVC")
            if let nc = selectedViewController as? UINavigationController {
                nc.pushViewController(archivedViewController, animated: true)
            } //?.pre //.pushViewController(archivedViewController, animated: true)
        

        /*
        if let vc = viewControllers?[3] {
            selectedViewController = vc
            highlightTab(index: 3)//TabBarItem.home.rawValue)
        }*/
    }
    
    func openAccount() {
        selectItem(index: TabBarItem.home.rawValue)
        bottomNavBar.selectedItem = nil
        if let nc = selectedViewController as? UINavigationController {
            let accountStoryboard = UIStoryboard(name: "Account", bundle: nil)
            let accountVC = accountStoryboard.instantiateViewController(withIdentifier: "AccountVC")
            
            nc.pushViewController(accountVC, animated: true)
        }
    }
    
    
    func openTutorial() {
        selectItem(index: TabBarItem.home.rawValue)
        if let nc = selectedViewController as? UINavigationController {
            let tutorialStoryboard = UIStoryboard(name: "Tutorial", bundle: nil)
            let vc = tutorialStoryboard.instantiateViewController(withIdentifier: "IKTutorialVC")
            hide()
            nc.pushViewController(vc, animated: true)
        }
    }
    
    func hide() {
        tabBar.isHidden = true
        bottomNavBar.isHidden = true
    }
    
    func show() {
        tabBar.isHidden = false
        bottomNavBar.isHidden = false
    }
    
    // MARK: - Private
    @objc func didLogout() {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

extension IKTabBarController: MDCBottomNavigationBarDelegate {
    func bottomNavigationBar(_ bottomNavigationBar: MDCBottomNavigationBar, didSelect item: UITabBarItem) {
        switch item {
        case tabBarItem1: // profile library
            #if TARGET_PRO
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TAB_BAR_LIBRARY", label: "")
            #endif
            if let vc = viewControllers?[TabBarItem.home.rawValue] {
                if let nc = vc as? UINavigationController {
                    nc.popToRootViewController(animated: true)
                }
                selectedViewController = vc
            }
            
        case tabBarItem2: // roaster profile
            #if TARGET_PRO
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TAB_BAR_ON_ROASTER", label: "")
            if RoasterManager.instance().roaster != nil {
                let notFirstOpen = UserDefaults.standard.bool(forKey: "notFirstTimeTheyConnectAndRoasterPageOpen")
                if !notFirstOpen{
                    UserDefaults.standard.set(true, forKey: "notFirstTimeTheyConnectAndRoasterPageOpen")
                    let walkthroughModalView = WalkthroughModalView()
                    walkthroughModalView.titleText = "$label_roast_events".localized
                    walkthroughModalView.setupHorisontalView(image: UIImage(named: "ColorChange_Active") , title: "$mark_CC".localized, index: .First)
                    walkthroughModalView.setupHorisontalView(image: UIImage(named: "FirstCrack_Active_Small") , title: "$mark_FC".localized, index: .Second)
                    walkthroughModalView.show(animated: true, dismissCallback: {
                    })
                }
            }
            #endif
            if RoasterManager.instance().roaster != nil {
                if let nc = selectedViewController as? UINavigationController, let chartVC = nc.topViewController as? IKChartVC, chartVC.profile?.isRoasterProfile() ?? false, bottomNavBar.selectedItem == tabBarItem2 {
                } else {
                    if let nc = viewControllers?[TabBarItem.home.rawValue] as? UINavigationController {
                        nc.popToRootViewController(animated: false)
                        NotificationCenter.default.post(name: Notification.Name.OpenRoasterReceipt, object: nil)
                    }
                    selectItem(index: TabBarItem.home.rawValue)
                    highlightTab(index: TabBarItem.roast.rawValue)
                }
                
            } else {
                if let vc = viewControllers?[TabBarItem.roast.rawValue] {
                    if let nc = vc as? UINavigationController {
                        nc.popToRootViewController(animated: true)
                    }
                    selectedViewController = vc
                }
            }
            
            break
            
        case tabBarItem3:
            #if TARGET_HOME
            // shop
            if let viewcontrollers_ = viewControllers, let selectedViewController = selectedViewController {
                selectItem(index: viewcontrollers_.firstIndex(of: selectedViewController) ?? TabBarItem.home.rawValue) // deselect tab
            }
            NavigationHelper.openShop()
            #else
            //roast log
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TAB_BAR_ROAST_LOG", label: "")
            if let vc = viewControllers?[TabBarItem.roastLog.rawValue] {
                if let nc = vc as? UINavigationController {
                    nc.popToRootViewController(animated: true)
                }
                selectedViewController = vc
            }
            #endif
        case tabBarItem4: // add profile
            if let viewcontrollers_ = viewControllers, let selectedViewController = selectedViewController {
                selectItem(index: viewcontrollers_.firstIndex(of: selectedViewController) ?? TabBarItem.home.rawValue) // deselect tab
            }
            AddReceiptManager.sharedInstance.showAddRecepieDialogView()
            
        default: break
            
        }
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            UINavigationController.attemptRotationToDeviceOrientation()
    }
}

extension IKTabBarController : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        if bottomNavBar.selectedItem == tabBarItem2 {
            selectItem(index: TabBarItem.roast.rawValue)
        }
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        
        if bottomNavBar.selectedItem == tabBarItem2 {
            if let navigationController = selectedViewController as? UINavigationController, let _ = navigationController.topViewController as? IKChartVC {
            } else {
                NavigationHelper.openProfile(profile: profile)
                NavigationHelper.highlightRoastTab()
            }
        }
    }
}
