//
//  IKAccountVC.swift
//  IKAWA-Home
//
//  Created by Admin on 10/30/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseUI
import IHKeyboardAvoiding
import TOCropViewController

class IKAccountVC: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var addPhotoIcon: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    #if TARGET_HOME
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var restoreBackupButton: UIButton!
    #endif
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    let imagePicker = UIImagePickerController()
    var nameWasChanged = false
    var emailWasChanged = false
    var passwordWasChanged = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        KeyboardAvoiding.avoidingView = scrollView
        self.tabBarController?.tabBar.isHidden = true
        emailTextField.text = (AppCore.sharedInstance.firebaseManager.currentUser?.email ?? "")
        loadAvatar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Actions
    @IBAction func restoreBackUpButtonTouchUpInside(_ sender: Any) {
        view.endEditing(true)
        AppCore.sharedInstance.syncManager.restoreRecentUserLibrary()
    }
    
    @IBAction func onTapAvatarImage(_ sender: Any) {
        
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            return
        }
        var firstTitle = ""
        var secondTitle = ""
        var thirdTitle = ""
        let confirmationDialogView = ButtonsPopupModalView()
        #if TARGET_PRO
        confirmationDialogView.title = "$edit_profile_image".localized
        firstTitle = "$add_up".localized
        secondTitle = "$remove_up".localized
        thirdTitle = "$cancel_up".localized
        #else
        confirmationDialogView.title = "$select_profile_picture".localized
        firstTitle = "$select_photo".localized
        secondTitle = "$delete_photo".localized
        thirdTitle = "$cancel".localized
        #endif
        
        
        confirmationDialogView.layout = .vertical
        confirmationDialogView.setUpButton(index: .First, title: firstTitle, type: .Filled) {
            
            let selectDialogView = ButtonsPopupModalView()
            selectDialogView.title = ""
            selectDialogView.layout = .vertical
            
            selectDialogView.setUpButton(index: .First, title: "$camera_up".localized, type: .Bordered) {
                self.imagePicker.delegate = self
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = .camera
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
            selectDialogView.setUpButton(index: .Second, title: "$gallery_up".localized, type: .Filled) {
                self.imagePicker.delegate = self
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = .photoLibrary
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
            selectDialogView.setUpButton(index: .Third, title: "$cancel".localized, type: .Cancel) {
                
            }
            IKNotificationManager.sharedInstance.show(view: selectDialogView)
        }
        confirmationDialogView.setUpButton(index: .Second, title: secondTitle, type: .Bordered) {
            AppCore.sharedInstance.firebaseManager.deleteAvatarImage {[weak self] (success, message) in
                if success {
                    self?.view.hideToastActivity()
                    CasheManager.sharedInstance.clearAvatarImage {
                        self?.loadAvatar()
                    }
                    self?.avatarImageView.image = UIImage(named: "UserPlaceholder")
                    self?.addPhotoIcon.isHidden = false
                }
            }
        }
        
        confirmationDialogView.setUpButton(index: .Third, title: thirdTitle, type: .Cancel) {}
        IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
        
        
    }
    
    @IBAction func backButtonTouchUpInside(_ sender: Any) {
        view.endEditing(true)
        dismiss(animated: true) {
            
        }
    }
    
    
    @IBAction func saveButtonTouchUpInside(_ sender: Any) {
        view.endEditing(true)
        nameWasChanged = false
        emailWasChanged = false
        passwordWasChanged = false
        
        //view.endEditing(true)
        guard AppCore.sharedInstance.firebaseManager.currentUser != nil else {
            return
        }
        
        self.view.makeToastActivity(.center)
        saveNameIfNeeded { (error) in
            if let error = error {
                self.view.hideToastActivity()
                self.showError(error: error)
            } else {
                if self.needSaveEmail() || self.needSavePassword() {
                    self.login(callback: { (error) in
                        if let error = error {
                            self.view.hideToastActivity()
                            self.showError(error: error)
                        } else {
                            
                            self.savePassword(callback: { (error) in
                                if let error = error {
                                    self.view.hideToastActivity()
                                    //self.showError(error: error)
                                } else {
                                    if self.needSaveEmail() {
                                        self.saveEmail(callback: { (error) in
                                            if error != nil {
                                                self.view.hideToastActivity()
                                                //self.showError(error: error)
                                            } else {
                                                self.view.hideToastActivity()
                                                self.showConfirmationPopUp{
                                                    if let user = AppCore.sharedInstance.firebaseManager.currentUser {
                                                        user.sendEmailVerification(completion: { (error) in
                                                            if let error = error {
                                                                self.view.hideToastActivity()
                                                                self.showError(error: error)
                                                            } else {
                                                                let popup = ButtonsPopupModalView()
                                                                popup.layout = .vertical
                                                                popup.title =  "$confirm_email_message".localized
                                                                popup.subtitle = ""
                                                                popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                                                                    self.logOut()
                                                                }
                                                                IKNotificationManager.sharedInstance.show(view: popup)
                                                            }
                                                        })
                                                    }
                                                }
                                                
                                            }
                                        })
                                    } else {
                                        self.view.hideToastActivity()
                                        self.showConfirmationPopUp{}
                                    }
                                    
                                }
                                
                            })
                        }
                        
                    })
                } else {
                    self.view.hideToastActivity()
                    if self.nameWasChanged {
                        self.showConfirmationPopUp{}
                    }
                    
                }
                
                
            }
        }
    }
    
    func showError(error: Error) {
        let popup = ButtonsPopupModalView()
        popup.layout = .vertical
        popup.title =  error.localizedDescription
        popup.subtitle = ""
        popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
        }
        IKNotificationManager.sharedInstance.show(view: popup)
    }
    
    func needSaveEmail() -> Bool {
        guard let user = AppCore.sharedInstance.firebaseManager.currentUser else {
            return false
        }
        
        if user.email != emailTextField.text && emailTextField.text != nil {
            return true
        }
        return false
    }
    
    func needSavePassword() -> Bool {
        #if TARGET_HOME
        guard AppCore.sharedInstance.firebaseManager.currentUser != nil else {
            return false
        }
        
        if "********" != passwordTextField.text && passwordTextField.text != nil {
            return true
        }
        return false
        #else
        return false
        #endif
    }
    
    func saveNameIfNeeded(callback:@escaping ((_ error: Error?)->Void)) {
    }
    
    func saveEmail(callback:@escaping ((_ error: Error?)->Void)) {
        
        guard needSaveEmail() else {
            callback(nil)
            return
        }
        
        
        guard let user = AppCore.sharedInstance.firebaseManager.currentUser else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "user not initialized"])
            callback(error)
            return
        }
        
        guard validateEmail() else {
            let error = NSError(domain:"com.ikawa", code:409, userInfo:[NSLocalizedDescriptionKey: "wrong email"])
            callback(error)
            return
        }
        
        AppCore.sharedInstance.firebaseManager.updateEmail(email: emailTextField.text!) { (error) in
            if let error = error {
                
                let popup = ButtonsPopupModalView()
                popup.layout = .vertical
                popup.title =  error.localizedDescription
                popup.subtitle = ""
                popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                
                IKNotificationManager.sharedInstance.show(view: popup)
                self.view.hideToast()
                callback(error)
                return
                
            } else {
                self.emailWasChanged = true
                callback(nil)
            }
            
        }
    }
    
    func savePassword(callback:@escaping ((_ error: Error?)->Void)) {
        guard needSavePassword() else {
            callback(nil)
            return
        }
        #if TARGET_HOME
        guard let user = AppCore.sharedInstance.firebaseManager.currentUser else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "user not initialized"])
            callback(error)
            return
        }
        
        guard self.validatePassword() else {
            let error = NSError(domain:"com.ikawa", code:409, userInfo:[NSLocalizedDescriptionKey: "wrong password"])
            callback(error)
            return
        }
        AppCore.sharedInstance.firebaseManager.updatePassword(password: self.passwordTextField.text!) { (error) in
            if let error = error {
                let popup = ButtonsPopupModalView()
                popup.layout = .vertical
                popup.title =  error.localizedDescription
                popup.subtitle = ""
                popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                
                IKNotificationManager.sharedInstance.show(view: popup)
                self.view.hideToast()
                callback(error)
                return
                
            } else {
                self.passwordWasChanged = true
                callback(nil)
            }
            
        }
        #endif
        
    }
    
    
    func login(callback:@escaping ((_ error: Error?)->Void))  {
        
        guard let user = AppCore.sharedInstance.firebaseManager.currentUser, let email = user.email else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "user not initialized"])
            callback(error)
            return
        }
        
        let currenPasswordModalView = CurrentPasswordModal()
        currenPasswordModalView.titleLabel.text = "$current_password".localized
        currenPasswordModalView.cancelButtonCallback = {
            DispatchQueue.main.async {
                self.view.hideToastActivity()
            }
            return
        }
        currenPasswordModalView.saveButtonCallback = {(password) in
            AppCore.sharedInstance.firebaseManager.firebaseLogin(email: email, password: password, callback: { (success, message) in
                if success {
                    callback(nil)
                } else {
                    //                        let popup = ButtonsPopupModalView.fromNib()
                    //                        popup.layout = .vertical
                    //                        popup.title = message ?? NSLocalizedString("$login_failed", comment: "")
                    //                        popup.subtitle = ""
                    //                        popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    //
                    //                        IKNotificationManager.sharedInstance.show(view: popup)
                    let error = NSError(domain:"com.ikawa", code:409, userInfo:[NSLocalizedDescriptionKey: message ?? "Error"])
                    callback(error)
                }
            })
        }
        currenPasswordModalView.show(animated: true) {
            
        }
        //IKNotificationManager.sharedInstance.show(view: currenPasswordModalView)
    }
    
    // MARK: - Private
    
    func logOut() {
        view.endEditing(true)
        AppCore.sharedInstance.firebaseManager.firebaseLogout()
        #if TARGET_PRO
        AppCore.sharedInstance.cropsterManager.logOut()
        #endif
        NavigationHelper.GoToLoginScreen()
        if let appDelegate  = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.unregisterPushNotifications()
        }
    }
    
    func validateEmail() -> Bool {
        let valid = self.emailTextField.text?.validateEmail() ?? false
        
        if (valid) {
            #if TARGET_HOME
            self.emailTextField.textColor = kColorDarkGrey2
            #endif
        } else {
            #if TARGET_HOME
            self.emailTextField.textColor = UIColor.red
            #endif
            let popup = ButtonsPopupModalView()
            popup.layout = .vertical
            popup.title = NSLocalizedString("$invalid_email_title", comment: "")
            popup.subtitle = NSLocalizedString("$invalid_email_message", comment: "")
            popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            IKNotificationManager.sharedInstance.show(view: popup)
        }
        
        return valid
    }
    
    func validatePassword() -> Bool {
        #if TARGET_HOME
        let valid = self.passwordTextField.text?.validatePassword() ?? false
        if (valid) {
           // self.passwordTextField.textColor = kColorDarkGrey2
        } else {
           // self.passwordTextField.textColor = UIColor.red
            
            let popup = ButtonsPopupModalView()
            popup.layout = .vertical
            popup.title = NSLocalizedString("$password_too_short_title", comment: "")
            popup.subtitle = NSLocalizedString("$password_too_short_message", comment: "")
            popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            popup.show(animated: true) {
                
            }
            //IKNotificationManager.sharedInstance.show(view: popup)
        }
        return valid
        #endif
        
        return false
    }
    
    
    func prepareAppearance() {
        view.layoutIfNeeded()
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.height / 2
        avatarImageView.layer.masksToBounds = true
        
        
        saveButton.layer.cornerRadius = saveButton.frame.height / 2

        
        #if TARGET_HOME
        saveButton.layer.borderWidth = 2
        saveButton.layer.borderColor = kColorRed.cgColor
        restoreBackupButton.layer.cornerRadius = saveButton.frame.height / 2
        
        #endif
        //        if let userName = IKFirebaseManager.sharedInstance.currentUser?.displayName {
        //            let arr = userName.split(separator: " ")
        //            if arr.count > 0 {
        //                firstNameTextField.text = String(arr[0])
        //            }
        //            if arr.count > 1 {
        //                lastNameTextField.text = String(arr[1])
        //            }
        //        }
        
    }
    
    func loadAvatar() {
        CasheManager.sharedInstance.getAvatarImage { (image, error) in
            if let image = image {
                self.avatarImageView.image = image
                self.addPhotoIcon.isHidden = true
            } else {
                self.avatarImageView.image = UIImage(named: "UserPlaceholder")
                self.addPhotoIcon.isHidden = false
            }
        }
    }
    
    func didSelectAvatarImage(img: UIImage) {
        let image = fixOrientation(img: img)
        self.view.makeToastActivity(.center)
        
        AppCore.sharedInstance.firebaseManager.uploadAvatarImage(image: image) { (success, message) in
            self.view.hideToastActivity()
            SDImageCache.shared.clearMemory()
            SDImageCache.shared.clearDisk(onCompletion: {
                self.loadAvatar()
            })
            
        }
    }
    
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    func showConfirmationPopUp(callback: @escaping (()->())) {
        let popup = ButtonsPopupModalView()
        popup.layout = .vertical
        popup.title = "$success".localized
        popup.subtitle = "$changes_have_been_saved".localized
        popup.setUpButton(index: .First, title: "$ok".localized, type: .Bordered) {
            callback()
        }
        
        IKNotificationManager.sharedInstance.show(view: popup)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        prepareAppearance()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}

extension IKAccountVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // textField.textColor = kColorDarkGrey2
    }
    
    private func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) {
        #if TARGET_HOME
        if textField == passwordTextField && string == "" {
            passwordTextField.text = ""
        }
        #endif
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension IKAccountVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate, TOCropViewControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated:true, completion: nil)
        let initialImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        //            let image = self.fixOrientation(img: initialImage)
        let cropViewController = TOCropViewController(croppingStyle: .circular, image: initialImage)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropToCircularImage image: UIImage, with cropRect: CGRect, angle: Int) {
        cropViewController.dismiss(animated: true) {
            self.didSelectAvatarImage(img: image)
        }
    }
}
