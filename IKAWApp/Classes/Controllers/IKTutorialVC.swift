//
//  IKTutorialVC.swift
//  IKAWA-Home
//
//  Created by Admin on 12/7/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import MaterialComponents

class IKTutorialVC: IKBaseVC {
    
    enum Align: String {
        case Center = "center"
        case Left = "left"
        case Right = "right"
    }
    
    let slideTextKey = "text"
    let slideIconKey = "icon"
    let slideTextAlignKey = "align"
    var tutorialArray:[Dictionary<String, String>] = []
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var slideContainerView: UIView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var tipsLabel: UILabel!
    @IBOutlet var backNavigationBarButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet private weak var backgroundView: UIView!
    @IBOutlet private weak var borderView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var titleView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        menuButton.isHidden = true// self.tabBarController == nil
        pageControl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        
        okButton.layer.cornerRadius = okButton.frame.height / 2
        okButton.layer.borderWidth = 2
        
        //view.backgroundColor
        
        tutorialArray.append([slideTextKey : NSLocalizedString("$tutorial_text_1", comment: ""), slideIconKey : "tutorial_1", slideTextAlignKey : Align.Center.rawValue])
        tutorialArray.append([slideTextKey : NSLocalizedString("$tutorial_text_2", comment: ""), slideIconKey : "tutorial_2",  slideTextAlignKey : Align.Left.rawValue])
        tutorialArray.append([slideTextKey : NSLocalizedString("$tutorial_text_3", comment: ""), slideIconKey : "tutorial_3", slideTextAlignKey : Align.Right.rawValue])
        tutorialArray.append([slideTextKey : NSLocalizedString("$tutorial_text_4", comment: ""), slideIconKey : "tutorial_4", slideTextAlignKey : Align.Left.rawValue])
        
        #if TARGET_PRO

        tutorialArray.append([slideTextKey : NSLocalizedString("$tutorial_text_5", comment: ""), slideIconKey : "tutorial_5", slideTextAlignKey : Align.Left.rawValue])
        skipButton.setTitleColor(.white, for: .normal)
        backButton.setTitleColor(.white, for: .normal)
        backgroundView.backgroundColor = UIColor.black
        borderView.backgroundColor = .clear
        tipsLabel.textColor = .white
        textLabel.textColor = .white
        signInTitleLabel.textColor = .white
        okButton.layer.borderColor = kColorWhite.cgColor
        okButton.setTitleColor(.white, for: .normal)
        okButton.backgroundColor =  kMainColor
        titleView.backgroundColor = kColorDarkGrey1
        pageControl.tintColor = .white
        pageControl.currentPageIndicatorTintColor = .white
        #else
        tutorialArray.append([slideTextKey : NSLocalizedString("$tutorial_text_4", comment: ""), slideIconKey : "tutorial_4", slideTextAlignKey : Align.Left.rawValue])
        okButton.layer.borderColor = kColorRed.cgColor
        #endif
        
        populateSlide(index: pageControl.currentPage)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Private
    
    func populateSlide(index: Int) {
        guard tutorialArray.count > index else {
            return
        }
        tipsLabel.isHidden = index != 0
        
        let slide = tutorialArray[index]
        textLabel.text = slide[slideTextKey]
        switch slide[slideTextAlignKey] {
        case Align.Center.rawValue:
            textLabel.textAlignment = .center
        case Align.Left.rawValue:
            textLabel.textAlignment = .left
        case Align.Right.rawValue:
            textLabel.textAlignment = .right
        default:
            textLabel.textAlignment = .center
        }
        iconImageView.image = UIImage(named: slide[slideIconKey]!)
        iconImageView.isHidden = slide[slideIconKey]?.isEmpty ?? true
        if index == (tutorialArray.count - 1) {
            skipButton.isHidden = true
            backButton.isHidden = false
            setView(view: okButton, hidden: false)
        } else {
            skipButton.isHidden = false
            backButton.isHidden = true
            setView(view: okButton, hidden: true)
        }
    }
    
    func next() {
        if pageControl.numberOfPages > (pageControl.currentPage + 1) {
            
            pageControl.currentPage = pageControl.currentPage + 1
            populateSlide(index: pageControl.currentPage)
                let transition = CATransition()
                transition.duration = 0.5
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromRight
            transition.fillMode = CAMediaTimingFillMode.both
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                slideContainerView.layer.add(transition, forKey: kCATransition)
        }
    }
    
    func previous() {
        if pageControl.currentPage > 0 {
            pageControl.currentPage = pageControl.currentPage - 1
            populateSlide(index: pageControl.currentPage)
                let transition = CATransition()
                transition.duration = 0.5
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromLeft
            transition.fillMode = CAMediaTimingFillMode.both
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                slideContainerView.layer.add(transition, forKey: kCATransition)
        }
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration:(pageControl.currentPage == (pageControl.numberOfPages - 1)) ? 0 : 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    // MARK: - Actions
    @IBAction func rightSwipe(_ sender: Any) {
        previous()
    }
    
    @IBAction func leftSwipe(_ sender: Any) {
        next()
    }
    
    @IBAction func okButtonTouchUpInside(_ sender: Any) {
        NavigationHelper.GoToMainScreen()
    }
    
    @IBAction func backTouchUpInside(_ sender: Any) {
        pageControl.currentPage = pageControl.currentPage - 1
        populateSlide(index: pageControl.currentPage)
        if pageControl.currentPage != (pageControl.numberOfPages - 1) {
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromLeft
            transition.fillMode = CAMediaTimingFillMode.both
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            slideContainerView.layer.add(transition, forKey: kCATransition)
        }
    }
    
    @IBAction func menuButtonTouchUpInside(_ sender: Any) {
        guard self.tabBarController != nil else {
            return
        }
        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let nc = menuStoryboard.instantiateInitialViewController()
        present(nc!, animated: true) {
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
