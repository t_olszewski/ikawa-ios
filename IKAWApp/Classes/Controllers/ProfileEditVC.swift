//
//  ProfileEditVC.swift
//  IKAWApp
//
//  Created by Admin on 4/18/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class ProfileEditVC: UIViewController {
    var profile: Profile!
    var editedProfile: Profile!
    var roastPoints = [RoastTempPoint]()
    var fanPoints = [RoastFanPoint]()
    var cooldownPoint: RoastFanPoint!
    var callback: ((_ profileHasChanged: Bool)->())?
    var selectedFanPoint: RoastFanPoint?
    var selectedRoastPoint: RoastTempPoint?
    var selectedCooldownPoint: Bool?
    var profileHasChanged: Bool = false
    
    @IBOutlet weak var statusBarView: IKStatusBarView!
    @IBOutlet weak var tableView: UITableView!
    private var tagEditedTextField: Int?
    private var startEditing: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppCore.sharedInstance.loggingManager.trackScreen(name: "PRO_TABLE_LAUNCH")
        //UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        roastPoints = editedProfile.roastPoints.map({element in return (element as! RoastTempPoint).copy() as! RoastTempPoint})
        fanPoints = editedProfile.fanPoints.map({element in return (element as! RoastFanPoint).copy() as! RoastFanPoint})
        NotificationCenter.default.addObserver(self, selector:#selector(degreeCelsiusChanged), name: NSNotification.Name.DegreeCelsiusChanged, object: nil)
        cooldownPoint = editedProfile.cooldownPoint.copy() as? RoastFanPoint
        tableView.reloadData()
        statusBarView.populate(profile: profile)
        statusBarView.thumbnail.isHidden = true
        statusBarView.statusLabel.text = "$editing_profile".localized
        setupStartFocus()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.statusBarView.thumbnail.isHidden = true
        RoasterManager.instance().delegates.add(self.statusBarView)
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
      //  UIApplication.shared.isIdleTimerDisabled = false
      //  UIApplication.shared.statusBarView?.backgroundColor = UIColor.black
        if (RoasterManager.instance().delegates.contains(self.statusBarView)) {
            RoasterManager.instance().delegates.remove(self.statusBarView)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        if (RoasterManager.instance().delegates.contains(self.statusBarView)) {
            RoasterManager.instance().delegates.remove(self.statusBarView)
        }
    }
    
    //MARK: Actions
    
    private func setupStartFocus() {
        if let roastFanPoint = selectedFanPoint {
            for (index, fanPoint) in fanPoints.enumerated() {
                if fanPoint.power == roastFanPoint.power && fanPoint.time == roastFanPoint.time {
                    let indexPath = IndexPath(row: index, section: 1)
                    setupFocus(indexPath)
                }
            }
        } else if let roastTempPoint = selectedRoastPoint {
            for (index, roastPoint) in roastPoints.enumerated() {
                if roastPoint.temperature == roastTempPoint.temperature && roastPoint.time == roastTempPoint.time {
                    let indexPath = IndexPath(row: index, section: 0)
                    setupFocus(indexPath)
                }
            }
        } else if (selectedCooldownPoint ?? false) {
            let indexPath = IndexPath(row: 0, section: 2)
            setupFocus(indexPath)
        }
    }
    
    @objc func degreeCelsiusChanged() {
        tableView.reloadData()
    }
    
    @IBAction func backButtonTouchUpInside(_ sender: Any) {
        if tagEditedTextField == nil {
            self.view.endEditing(true)
            editedProfile.roastPoints = roastPoints
            editedProfile.fanPoints = fanPoints
            editedProfile.cooldownPoint = cooldownPoint
            editedProfile.roastPoints = roastPoints
            if let callback = self.callback {
                callback(profileHasChanged)
            }
            
            navigationController?.popViewController(animated: true)
        } else {
            self.view.endEditing(true)
        }
    }
    
    @IBAction func menuButtonTouchUpInside(_ sender: Any) {
        if tagEditedTextField == nil {
            let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
            let nc = menuStoryboard.instantiateInitialViewController()
            present(nc!, animated: true) {}
        } else {
            self.view.endEditing(true)
        }
    }
    /*
    @IBAction func sensorButtonTouchUpInside(_ sender: UIButton) {
        self.view.endEditing(true)
        if (editedProfile?.tempSensor == TempSensorBelow) {
            if ((getHighestRoastPoint()?.temperature)! > Float(IK_MAX_TEMP_ABOVE)) {
                
                let alert = ButtonsPopupModalView()
                alert.layout = .vertical
                alert.title = NSLocalizedString("$exhaust_profile_temperature_error_title", comment: "")
                alert.subtitle = String(format: NSLocalizedString("$exhaust_profile_temperature_error_message", comment: ""), Int(IK_MAX_TEMP_ABOVE))
                alert.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                
                IKNotificationManager.sharedInstance.show(view: alert)

            } else {
                editedProfile?.tempSensor = TempSensorAbove
            }
        } else {
            editedProfile?.tempSensor = TempSensorBelow
        }
        updateSensorButtonAppearance(sensorButton: sender)
    }*/
    /*
    @IBAction func saveBtnPressed(_ sender: Any) {
        self.view.endEditing(true)
        editedProfile.roastPoints = roastPoints
        editedProfile.fanPoints = fanPoints
        editedProfile.cooldownPoint = cooldownPoint
        editedProfile.roastPoints = roastPoints
        if ((callback) != nil) {
            callback!(editedProfile)
        }
        
        self.dismiss(animated: true) {
            
        }
    }*/
    
    @IBAction func deleteBtnPressed(_ sender: Any) {
        //self.view.endEditing(true)
        let btn = sender as! UIButton
        
        let isRoastCellEdited = ((self.view.viewWithTag(btn.tag - 400) as? UITextField)?.isFirstResponder ?? false) || ((self.view.viewWithTag(btn.tag - 500) as? UITextField)?.isFirstResponder ?? false) || (tagEditedTextField == nil)
        let isFanCellEdited = ((self.view.viewWithTag(btn.tag - 400) as? UITextField)?.isFirstResponder ?? false) || ((self.view.viewWithTag(btn.tag - 300) as? UITextField)?.isFirstResponder ?? false) || tagEditedTextField == nil
        
        func deletePoint() {
            
            if (btn.tag >= 600) && isFanCellEdited {
                tagEditedTextField = nil
                fanPoints.remove(at: btn.tag - 600)
                self.view.endEditing(true)
                
                CATransaction.begin()
                CATransaction.setCompletionBlock({
                    self.tableView.isUserInteractionEnabled = true
                    /*self.tableView.reloadRows(at: [IndexPath(row: (btn.tag - 600) - 1, section: 0), IndexPath(row: (btn.tag - 600), section: 0)], with: .none)*/
                    self.tableView.reloadData()
                })
                tableView.beginUpdates()
                self.tableView.isUserInteractionEnabled = false
                    self.tableView.deleteRows(at: [IndexPath(row: (btn.tag - 600), section: 1)] , with: .left)
                tableView.endUpdates()
                CATransaction.commit()
                checkPoints(true)
                //
                return
                
                
            } else if (btn.tag >= 500) && isRoastCellEdited {
                tagEditedTextField = nil
                roastPoints.remove(at: btn.tag - 500)
                self.checkPoints(true)
                self.view.endEditing(true)
                
                CATransaction.begin()
                CATransaction.setCompletionBlock({
                    self.tableView.isUserInteractionEnabled = true
                    /*
                    self.tableView.reloadRows(at: [IndexPath(row: (btn.tag - 500) - 1, section: 0), IndexPath(row: (btn.tag - 500), section: 0)], with: .none)*/
                    self.tableView.reloadData()
                })
                tableView.beginUpdates()
                self.tableView.isUserInteractionEnabled = false
                    self.tableView.deleteRows(at: [IndexPath(row: (btn.tag - 500), section: 0)] , with: .right)
                tableView.endUpdates()
                CATransaction.commit()
                return
            }
            checkPoints()
        }
        let key = "removePointConfirmationDialog" /*+ (IKFirebaseManager.sharedInstance.currentUser?.uid ?? "")*/
        if UserDefaults.standard.bool(forKey: key) && (isRoastCellEdited || isFanCellEdited) {
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .horisontal
            confirmationDialogView.subtitle = ((btn.tag >= 600) ? "$warning_delete_fan_point" : "$warning_delete_point").localized
            confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$alert_delete", comment: ""), type: .Filled) {
                deletePoint()
            }
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$cancel_up", comment: ""), type: .Cancel) {}
            confirmationDialogView.setUpCheckBox(title: nil, callback: { [weak confirmationDialogView] in
                AppCore.sharedInstance.settingsManager.confirmationDialog(name: "removePointConfirmationDialog", hide:  !(confirmationDialogView?.isCheckBoxSelected ?? false))
            })
            confirmationDialogView.show(animated: true, dismissCallback: {})
        } else {
            deletePoint()
        }
    }

    @IBAction func addButtonPressed(_ sender: Any) {
        if tagEditedTextField == nil {
            self.view.endEditing(true)
            let btn = sender as! UIButton
            if (btn.tag >= 600) { // fan point
                if fanPoints.count > 9 {
                    tagEditedTextField = nil
                    showPopup(message: "$warning_max_fan_points".localized)
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_MAX_FAN_POINTS", label: "")
                    return
                } else {
                    let lastFanPoint = fanPoints.last
                    let newFanPoint = RoastFanPoint()
                    newFanPoint.time = (lastFanPoint?.time)!
                    newFanPoint.power = (lastFanPoint?.power)!
                    fanPoints.append(newFanPoint)
                    
                    let lastRoastPoint = roastPoints.last
                    let newRoastPoint = RoastTempPoint()
                    newRoastPoint.time = (lastFanPoint?.time)!
                    newRoastPoint.temperature = (lastRoastPoint?.temperature)!
                    roastPoints[roastPoints.count - 1] = newRoastPoint
                    
                    self.tableView.reloadData()
                    let indexPath = IndexPath(row: fanPoints.count - 2, section: 1)
                    setupFocus(indexPath)
                }
            } else if (btn.tag >= 500) {// temp point
                if roastPoints.count > 19 {
                    tagEditedTextField = nil
                    showPopup(message: "$warning_max_temp_points".localized)
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_MAX_TEMP_POINTS", label: "")
                    return
                } else {
                    let lastRoastPoint = roastPoints.last
                    let newRoastPoint = RoastTempPoint()
                    newRoastPoint.time = (lastRoastPoint?.time)!
                    newRoastPoint.temperature = (lastRoastPoint?.temperature)!
                    roastPoints.append(newRoastPoint)
                    
                    let lastFanPoint = fanPoints.last
                    let newFanPoint = RoastFanPoint()
                    newFanPoint.time = (lastFanPoint?.time)!
                    newFanPoint.power = (lastFanPoint?.power)!
                    fanPoints[fanPoints.count - 1] = newFanPoint
                    self.tableView.reloadData()
                    let indexPath = IndexPath(row: roastPoints.count - 2, section: 0)
                    setupFocus(indexPath)
                }
            }
        } else {
            view.endEditing(true)
        }
    }
    
    //MARK: Private
    private func setupFocus(_ indexPath: IndexPath) {
        if !(tableView.indexPathsForVisibleRows?.contains(indexPath) ?? true) {
            tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
        if let cell = tableView.cellForRow(at: indexPath) as? RoastPointCell {
            cell.timeTxtField.becomeFirstResponder()
            cell.timeTxtField.backgroundColor = kColorHighlightedTextField
            cell.timeTxtField.textColor = .white
        }
    }
    
    func updateSensorButtonAppearance(sensorButton: UIButton)  {
        
        if ((RoasterManager.instance().roaster != nil && RoasterManager.instance().roaster.status.state == IK_STATUS_ROASTING)
            || !AppCore.sharedInstance.settingsManager.supportsTwoSensors()) {
            sensorButton.isEnabled = false
        } else {
            sensorButton.isEnabled = true
        }
        
        if (editedProfile?.tempSensor == TempSensorBelow) {
            sensorButton.setTitle(NSLocalizedString("$inlet", comment: ""), for: .normal)
            sensorButton.backgroundColor = kColorExhaust
        } else {
            sensorButton.setTitle(NSLocalizedString("$exhaust", comment: ""), for: .normal)
            sensorButton.backgroundColor = kColorRed
        }
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            } else {
                self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: endFrame?.size.height ?? 0.0, right: 0)
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    func checkPoints(_ dontReloadTableView: Bool = false) {
        //sort
        
        profileHasChanged = true
        roastPoints.sort {$0.time < $1.time}
        fanPoints.sort {$0.time < $1.time}
        
        //check value ranges
        let useSensorBelow = editedProfile.tempSensor == TempSensorBelow
        roastPoints = roastPoints.map { (point) -> RoastTempPoint in
            point.time = min(Float(kTimeAxisMax), point.time)
            
            point.temperature = max(Float(kMinTemperature), point.temperature)
            point.temperature = min(useSensorBelow ? Float(IK_MAX_TEMP_BELOW) : Float(IK_MAX_TEMP_ABOVE), point.temperature)
            return point
        }

        fanPoints = fanPoints.map { (point) -> RoastFanPoint in
            point.time = min(Float(kTimeAxisMax), point.time)
            point.power = min(Float(IK_MAX_FAN_SPEED), max(Float(IK_MIN_FAN_SPEED), point.power))
            return point
        }
        
        //check begin points
        var fanPoint = fanPoints[0]
        fanPoint.time = 0
        fanPoints[0] = fanPoint
        
        var roastPoint = roastPoints[0]
        roastPoint.time = 0
        roastPoints[0] = roastPoint
        
        //check end points
        fanPoint = fanPoints[fanPoints.count - 1]
        roastPoint = roastPoints[roastPoints.count - 1]
        fanPoint.time = roastPoint.time
        fanPoints[fanPoints.count - 1] = fanPoint
        
        //check cooldown point
        fanPoint = cooldownPoint
        fanPoint.time = min(roastPoint.time + Float(IK_MAX_COOLDOWN_TIME), max(roastPoint.time + 60, fanPoint.time))
        fanPoint.power = min(Float(IK_MAX_FAN_SPEED), max(Float(IK_MIN_FAN_SPEED), fanPoint.power))
        cooldownPoint = fanPoint
        
        //update table
        if !dontReloadTableView {
            tableView.reloadData()
        }
    }
    
    func getHighestRoastPoint() -> RoastTempPoint? {
        guard (roastPoints.count > 0) else {
            return nil
        }
        var result: RoastTempPoint = roastPoints.first!
        for point in roastPoints {
            if (point.temperature > result.temperature) {
                result = point
            }
        }
        return result
    }
    
    private func showPopup(message: String) {
        let confirmationDialogView = ButtonsPopupModalView()
        confirmationDialogView.layout = .vertical
        confirmationDialogView.subtitle = message
        confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Filled) {
            if let textFieldTag = self.tagEditedTextField, let textField = self.tableView.viewWithTag(textFieldTag) as? UITextField {
                textField.becomeFirstResponder()
            }
        }
        IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
    }
}

extension ProfileEditVC: UITableViewDelegate, UITableViewDataSource {
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return roastPoints.count
        case 1:
            return fanPoints.count
        case 2:
            return 1
        default:
            return 0
        }
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:RoastPointCell!
        switch indexPath.section {
        case 0:
            /*
            if (indexPath.row >= roastPoints.count) {
                if (roastPoints.count < IK_MAX_ROAST_POINTS) {
                    cell = tableView.dequeueReusableCell(withIdentifier: "addPoint", for: indexPath)
                } else {
                    cell = UITableViewCell()
                }
                
            } else {
 */
                cell = tableView.dequeueReusableCell(withIdentifier: "RoastPointCell", for: indexPath) as? RoastPointCell
                let roastPoint = roastPoints[indexPath.row]
                
                cell.timeTxtField.text = String(format: "%0.2d:%0.2d", Int(roastPoint.time / 60), Int(roastPoint.time.truncatingRemainder(dividingBy: 60)))
                cell.timeTxtField.tag = indexPath.row + 0
                cell.valueTxtField.text =  FormatHelper.tempFormat(temperature: roastPoint.temperature, "%.0f", false)
                cell.valueTxtField.tag = indexPath.row + 100
                cell.unit2Label.text = USE_FAHRENHEIT ? "℉" : "°C"
                cell.deleteButton.tag = indexPath.row + 500
                cell.deleteButton.isHidden = roastPoints.count <= IK_MIN_ROAST_POINTS || indexPath.row == 0 || (indexPath.row + 1) == roastPoints.count
                cell.addButton.isHidden = true
                cell.addButton.tag = indexPath.row + 500
                if (indexPath.row + 1) == roastPoints.count && roastPoints.count <= IK_MAX_ROAST_POINTS {
                    cell.addButton.isHidden = false
                    cell.deleteButton.isHidden = true
                }
                cell.rowNumberLabel.text = String(format: "%d", indexPath.row)
                cell.timeTxtField.isUserInteractionEnabled = !(indexPath.row == 0)
            break
        case 1:
            /*
            if (indexPath.row >= fanPoints.count) {
                if (roastPoints.count < IK_MAX_FAN_POINTS) {
                    cell = tableView.dequeueReusableCell(withIdentifier: "addPoint", for: indexPath)
                } else {
                    cell = UITableViewCell()
                }
            } else {
 */
                cell = tableView.dequeueReusableCell(withIdentifier: "RoastPointCell", for: indexPath) as? RoastPointCell
                let fanPoint = fanPoints[indexPath.row]
                cell.timeTxtField.text = String(format: "%0.2d:%0.2d", Int(fanPoint.time / 60), Int(fanPoint.time.truncatingRemainder(dividingBy: 60)))
                cell.timeTxtField.tag = indexPath.row + 200
                cell.valueTxtField.text = String(format: "%0.f", fanPoint.power)
                cell.valueTxtField.tag = indexPath.row + 300
                cell.unit2Label.text = "%"
                cell.deleteButton.tag = indexPath.row + 600
                cell.deleteButton.isHidden = fanPoints.count <= IK_MIN_FAN_POINTS || indexPath.row == 0
                cell.addButton.isHidden = true
                cell.addButton.tag = indexPath.row + 600
                if (indexPath.row + 1) == fanPoints.count && fanPoints.count <= IK_MAX_FAN_POINTS {
                    cell.addButton.isHidden = false
                    cell.deleteButton.isHidden = true
                }
                cell.rowNumberLabel.text = String(format: "%d", indexPath.row)
                cell.timeTxtField.isUserInteractionEnabled = !(indexPath.row == 0)
            break
        case 2:
            cell = tableView.dequeueReusableCell(withIdentifier: "RoastPointCell", for: indexPath) as? RoastPointCell
            if let fanPoint = cooldownPoint, let lastFanPoint = fanPoints.last {
                let duration = fanPoint.time - lastFanPoint.time
                cell.timeTxtField.text = String(format: "%0.2d:%0.2d", Int(duration / 60), Int(duration.truncatingRemainder(dividingBy: 60)))
                cell.timeTxtField.tag = 401
                cell.valueTxtField.text = String(format: "%0.f", fanPoint.power)
                cell.valueTxtField.tag = 402
                cell.unit2Label.text = "%"
                cell.deleteButton.isHidden = true
                cell.addButton.isHidden = true
                cell.rowNumberLabel.text = "-"
            }
            
            break
        default:
            cell = nil
        }
        cell.timeTxtField.backgroundColor = tagEditedTextField == cell.timeTxtField.tag ? kColorHighlightedTextField : UIColor.clear
        cell.valueTxtField.backgroundColor = tagEditedTextField == cell.valueTxtField.tag ? kColorHighlightedTextField : UIColor.clear
        cell.doneAction = {[weak self] in
            self?.view.endEditing(true)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 77
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell")
        if let titleLabel = cell?.viewWithTag(100) as? UILabel, let tempLabel = cell?.viewWithTag(120) as? UILabel {
            switch section {
            case 0:
                titleLabel.text = "$temperature_points".localized
                titleLabel.letterSpace = 1
                tempLabel.text = (editedProfile?.tempSensor == TempSensorBelow ? "$inlet" : "$exhaust").localized + "$editing_profile_temp".localized
                tempLabel.letterSpace = 0.58
                //infoButton.isHidden = false
            case 1:
                titleLabel.text = NSLocalizedString("$fan_points", comment: "")
                titleLabel.letterSpace = 1
                tempLabel.text = "$editing_profile_fan".localized
                tempLabel.letterSpace = 0.58
            case 2:
                titleLabel.text = NSLocalizedString("$cool_down", comment: "")
                titleLabel.letterSpace = 1
                tempLabel.text = "$editing_profile_fan".localized
                tempLabel.letterSpace = 0.58
            default:
                titleLabel.text = ""
            }
        }
        
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        tableView.deselectRow(at: indexPath, animated: true)
        if (indexPath.section == 0 && indexPath.row == roastPoints.count) {
            let lastRoastPoint = roastPoints.last
            let newRoastPoint = RoastTempPoint()
            newRoastPoint.time = (lastRoastPoint?.time)! + 10
            newRoastPoint.temperature = (lastRoastPoint?.temperature)!
            roastPoints.append(newRoastPoint)
            
            let lastFanPoint = fanPoints.last
            let newFanPoint = RoastFanPoint()
            newFanPoint.time = (lastFanPoint?.time)! + 10
            newFanPoint.power = (lastFanPoint?.power)!
            fanPoints[fanPoints.count - 1] = newFanPoint
            
            //cooldownPoint.time = cooldownPoint.time + 10
            
            checkPoints()
            
        } else if (indexPath.section == 1 && indexPath.row == fanPoints.count) {
            let lastFanPoint = fanPoints.last
            let newFanPoint = RoastFanPoint()
            newFanPoint.time = (lastFanPoint?.time)! + 10
            newFanPoint.power = (lastFanPoint?.power)!
            fanPoints.append(newFanPoint)
            
            let lastRoastPoint = roastPoints.last
            let newRoastPoint = RoastTempPoint()
            newRoastPoint.time = (lastFanPoint?.time)! + 10
            newRoastPoint.temperature = (lastRoastPoint?.temperature)!
            roastPoints[roastPoints.count - 1] = newRoastPoint
            
            //cooldownPoint.time = cooldownPoint.time + 10
            
            checkPoints()

        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}

extension ProfileEditVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if tagEditedTextField != nil {
            
            if (textField.tag == 401) {
                let duration = dateDecoder(date: textField.text)
                if duration < 60 {
                    tagEditedTextField = textField.tag
                    showPopup(message: "$warning_min_cool_down".localized)
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_MIN_COOL_DOWN", label: "")
                    return
                } else if duration > 300 {
                    tagEditedTextField = textField.tag
                    showPopup(message: "$warning_max_cool_down".localized)
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_MAX_COOL_DOWN", label: "")
                    return
                }
                cooldownPoint?.time = (fanPoints.last?.time ?? 0) + duration
                
            } else if (textField.tag == 402) {
                cooldownPoint.power = Float(textField.text!) ?? 0
                if cooldownPoint.power < 60 {
                    tagEditedTextField = textField.tag
                    showPopup(message: "$warning_fan_higher_60".localized)
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_FAN_HEIGHER_60", label: "")
                    return
                } else if cooldownPoint.power > 100 {
                    tagEditedTextField = textField.tag
                    showPopup(message: "$warning_less_than_100".localized)
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_LESS_THAN_100", label: "")
                    return
                }
                
            } else if (textField.tag >= 300) {
                let fanPoint = fanPoints[textField.tag - 300]
                fanPoint.power = Float(textField.text!) ?? 0
                
                if fanPoint.power < 60 {
                    tagEditedTextField = textField.tag
                    showPopup(message: "$warning_fan_higher_60".localized)
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_FAN_HEIGHER_60", label: "")
                    return
                } else if fanPoint.power > 100 {
                    tagEditedTextField = textField.tag
                    showPopup(message: "$warning_less_than_100".localized)
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_LESS_THAN_100", label: "")
                    return
                }
                
            } else if (textField.tag >= 200) {
                
                let fanPoint = fanPoints[textField.tag - 200]
                let duration = (cooldownPoint?.time ?? 0) - (fanPoints.last?.time ?? 0)
                fanPoint.time = dateDecoder(date: textField.text)
                cooldownPoint?.time = (fanPoints.last?.time ?? 0) + duration
                roastPoints.last?.time = fanPoints.last?.time ?? 0
                
                if textField.tag == 200 + fanPoints.count - 1 { //last element
                    if fanPoint.time <= fanPoints[textField.tag - 201].time || fanPoint.time <= roastPoints[roastPoints.count - 2].time {
                        tagEditedTextField = textField.tag
                        showPopup(message: "$warning_point_must_finale".localized)
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_POINT_MUST_FINALE", label: "")
                        return
                    } else if fanPoint.time > Float(kTimeAxisMax) {
                        tagEditedTextField = textField.tag
                        showPopup(message: "$warning_20min_max".localized)
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_20_MIN_MAX", label: "")
                        return
                    }
                } else {
                    if fanPoint.time >= fanPoints.last?.time ?? 0 {
                        tagEditedTextField = textField.tag
                        showPopup(message: "$warning_less_total_fan_time".localized)
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_SET_END_TIME", label: "")
                        return
                    } else {
                        var count = 0
                        for roastTempPoint in fanPoints {
                            count = Int(roastTempPoint.time) == Int(fanPoint.time) ? count + 1 : count
                        }
                        if count > 1 {
                            tagEditedTextField = textField.tag
                            showPopup(message: "$warning_already_fan_point".localized)
                            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_ALREADY_FAN_POINT", label: "")
                            return
                        }
                    }
                }
                
            } else if (textField.tag >= 100) { // RoastPoint  temp text field
                let roastPoint = roastPoints[textField.tag - 100]
                if textField.text != "." {
                    roastPoint.temperature = USE_FAHRENHEIT ? Float(FahrenheitToTemp(temp: Double(textField.text!) ?? 0)) : Float(textField.text!) ?? 0
                }
                
                if roastPoint.temperature < (/*USE_FAHRENHEIT ? 77 : */25) {
                    tagEditedTextField = textField.tag
                    showPopup(message: String(format: "$warning_temperature_higher_than".localized, USE_FAHRENHEIT ? "77°F" : "25°C"))
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_TEMPERATURE_HIGHER_THAN", label: "")
                    return
                } else if roastPoint.temperature > ((editedProfile?.tempSensor == TempSensorBelow) ? (/*USE_FAHRENHEIT ? 554 :*/ 290) : (/*USE_FAHRENHEIT ? 464 :*/ 240)) {
                    tagEditedTextField = textField.tag
                    if (editedProfile?.tempSensor == TempSensorBelow) {
                        showPopup(message: String(format: "$warning_temperature_less_than".localized, USE_FAHRENHEIT ? "554°F" : "290°C"))
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_TEMPERATURE_LESS_THAN", label: "")
                    } else {
                        showPopup(message: String(format: "$warning_temperature_less_than".localized, USE_FAHRENHEIT ? "464°F" : "240°C"))
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_TEMPERATURE_LESS_THAN", label: "")
                    }
                    return
                }
                
            } else  { // roast Point time text field
                
                let duration = (cooldownPoint?.time ?? 0) - (roastPoints.last?.time ?? 0)
                let roastPoint = roastPoints[textField.tag]
                roastPoint.time = dateDecoder(date: textField.text)
                cooldownPoint?.time = (roastPoints.last?.time ?? 0) + duration
                
                if textField.tag == roastPoints.count - 1 { //last element
                    if roastPoint.time <= roastPoints[textField.tag - 1].time || roastPoint.time <= fanPoints[fanPoints.count - 2].time {
                        tagEditedTextField = textField.tag
                        showPopup(message: "$warning_point_must_finale".localized)
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_POINT_MUST_FINALE", label: "")
                        return
                    } else if roastPoint.time > Float(kTimeAxisMax) {
                        tagEditedTextField = textField.tag
                        showPopup(message: "$warning_20min_max".localized)
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_20_MIN_MAX", label: "")
                        return
                    }
                } else { // not last element
                    if roastPoint.time >= roastPoints.last?.time ?? 0 {
                        tagEditedTextField = textField.tag
                        showPopup(message: "$warning_less_total_roast_time".localized)
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_SET_END_TIME", label: "")
                        return
                    } else {
                        var count = 0
                        for roastTempPoint in roastPoints {
                            if Int(roastTempPoint.time) == Int(roastPoint.time) {
                                count = count + 1
                            }
                        }
                        if count > 1 {
                            tagEditedTextField = textField.tag
                            showPopup(message: "$warning_already_temp_point".localized)
                            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_TABLE_POPUP_ALREADY_TEMP_POINT", label: "")
                            return
                        }
                        
                    }
                }
            }
            tagEditedTextField = nil
            checkPoints()
        }
        startEditing = true
        tagEditedTextField = nil
        textField.backgroundColor = .clear
        textField.textColor = .white
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.keyboardType = .numberPad
        
        if tagEditedTextField != nil {
            view.endEditing(true)
            return true
        }
        textField.backgroundColor = kColorHighlightedTextField
        textField.textColor = .white
        tagEditedTextField = textField.tag
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let tag = textField.tag
        textField.backgroundColor = .clear
        if ((tag == 401) || (tag >= 200 && tag < 300) || (tag < 100)) {
            if string.isEmpty && range.length > 1 {
                correctDeletingSelection(textField: textField, range: range)
            }
            
            let data = ((string as NSString).floatValue)
            if !string.isEmpty && !textField.correctTiemFormat() {
                textField.shake()
                return false
            }
            if range.location != 5 {
                startEditing = false
            }
            
            switch range.location {
            case 0:
                if string.isEmpty && textField.text?.charOnIndexIs(char: ":", index: 0) ?? false {
                    textField.shake()
                    textField.pushCursorToLeft()
                    return false
                }
                if data > 5 {
                    textField.shake()
                    return false
                } else {
                    return textField.replaceCharIfNeded(string)
                }
            case 1:
                if string.isEmpty && textField.text?.charOnIndexIs(char: ":", index: 1) ?? false {
                    textField.shake()
                    textField.pushCursorToLeft()
                    return false
                }
                if !textField.previousNumberIsCorrect() && !string.isEmpty {
                    textField.shake()
                    textField.pushCursorToLeft()
                    return false
                } else {
                    return textField.replaceCharIfNeded(string)
                }
            case 2:
                if !string.isEmpty {
                    if textField.text?.characters.contains(":") ?? true {
                        //return textField.replaceCharIfNeded(string)
                    } else {
                        textField.text = textField.text! + ":"
                    }
                    
                    
                    if data > 5 {
                        textField.shake()
                        return false
                    } else {
                        textField.pushCursorToRight()
                        return textField.replaceCharIfNeded(string)
                    }
                } else if textField.text?.charOnIndexIs(char: ":", index: 2) ?? false {
                    textField.shake()
                    textField.pushCursorToLeft()
                    return false
                } else {
                    return true
                }
            case 3:
                if data > 5 {
                    textField.shake()
                    return false
                } else {
                    return textField.replaceCharIfNeded(string)
                }
            case 4:
                if !textField.previousNumberIsCorrect() && !string.isEmpty {
                    textField.shake()
                    textField.pushCursorToLeft()
                    return false
                } else {
                    return textField.replaceCharIfNeded(string)
                }
            case 5:
                if startEditing {
                    textField.text = string
                    startEditing = false
                } else {
                    textField.shake()
                }
                return false
            default:
                textField.shake()
                return false
            }
            startEditing = false
        } else if startEditing && isLastPositionOfcursor(textField) {
            startEditing = false
            textField.text = ""
            return true
        } else if tag >= 100 && tag < 200 {
            
            return !((textField.text?.characters.contains(".") ?? false && string == ".") || (textField.text?.count ?? 0 > 2 && !string.isEmpty))
        } else if tag >= 300 && tag <= 402 && tag != 401 {
            return !(textField.text?.count ?? 0 > 2 && !string.isEmpty)
        }
        return true    }
    
    private func correctDeletingSelection(textField: UITextField, range: NSRange) {
        if let text = textField.text {
            var replaceText: Bool = false
            let textArray: [String.Element] = Array(text.characters)
            for i in range.location...(range.length - 1) {
                if textArray[i] == ":" {
                    replaceText = true
                }
            }
            if replaceText {
                if let replaceStart = textField.position(from: textField.beginningOfDocument, offset: range.location),
                    let replaceEnd = textField.position(from: replaceStart, offset: range.length),
                    let textRange = textField.textRange(from: replaceStart, to: replaceEnd) {
                    textField.replace(textRange, withText: ":")
                }
            }
        }
    }
    
    func dateDecoder(date: String?) -> Float {
        
        guard let date = date else {
            return 0
        }
        var currentStr = date
        if date.charOnIndexIs(char: ":", index: 0){
            currentStr = "00" + date
        } else if date.charOnIndexIs(char: ":", index: date.count - 1) {
            currentStr = date + "00"
        }
        
        let time = currentStr.characters.split{$0 == ":"}.map(String.init)
        if time.count == 2 {
            return ((Float(time[0]) ?? 0) * 60) + (Float(time[1]) ?? 0)
        }
        return 0
    }
    
    func isLastPositionOfcursor(_ textField: UITextField) -> Bool {
        if let selectedRange = textField.selectedTextRange {
            let position = textField.offset(from: textField.beginningOfDocument, to: selectedRange.start)
            return textField.text?.count ?? (position + 1) <= position
        } else {
            return false
        }
    }
}














