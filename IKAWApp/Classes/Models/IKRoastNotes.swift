//
//  IKRoastNotes.swift
//  IKAWApp
//
//  Created by Admin on 5/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import IKRoasterLib

class IKRoastNotes: NSObject {
    var archived: Bool = false
    var dateLastEdited: Date?
    var roast: IKRoast?
    var coffeeType: String = ""
    var notes: String = ""
    var photo: String = ""
    var extraInfo: String = ""
    var coffeeId: String = ""
    var firstCrackTime: Double = 0
    var firstCrackTemp: Double = 0
    var secondCrackTime: Double = 0
    var secondCrackTemp: Double = 0
    var dtr: Double = 0
    var colorChangeTime: Double = 0
    var colorChangeTemp: Double = 0
    
    init(roast: IKRoast?) {
        self.roast = roast
        super.init()
    }
    
    func turningPointDescription(_ format: String = "%@ (%@)") -> String {
        guard let turnPoint = roast?.turningPoint() else {
            return ""
        }
        return String(format: format, FormatHelper.timeFormat(seconds: round(turnPoint.x)), FormatHelper.tempFormat(temperature: round(turnPoint.y)))
    }
    
    func colorChangeDescription(_ format: String = "%@ (%@)") -> String {
        guard (colorChangeTemp > 0 && colorChangeTime > 0) else {
            return ""
        }
        return String(format: format, FormatHelper.timeFormat(seconds: round(colorChangeTime)), FormatHelper.tempFormat(temperature: round(colorChangeTemp)))
    }
    
    func firstCrackDescription(_ format: String = "%@ (%@)") -> String {
        guard (firstCrackTime > 0 && firstCrackTemp > 0) else {
            return ""
        }
        return String(format: format, FormatHelper.timeFormat(seconds: round(firstCrackTime)), FormatHelper.tempFormat(temperature: round(firstCrackTemp)))
    }

    func secondCrackDescription(_ format: String = "%@ (%@)") -> String {
        guard (secondCrackTime > 0 && secondCrackTemp > 0) else {
            return ""
        }
        return String(format: format, FormatHelper.timeFormat(seconds: round(secondCrackTime)), FormatHelper.tempFormat(temperature: round(secondCrackTemp)))
    }
    
    func preheatTempDescription(_ tempFormat: String? = nil) -> String {
        guard let tempAbove = roast?.preheatingPoints().last?.temp_above else {
            return ""
        }
        guard let format = tempFormat else {
            return FormatHelper.tempFormat(temperature: round(tempAbove))
        }
        return FormatHelper.tempFormat(temperature: round(tempAbove), format)
    }
    
    func timeAfterFirstDescription(_ timeFormat: String? = nil) -> String {
        guard let endTime = endTimeRounded(), firstCrackTime != 0 else {
            return ""
        }
        guard let format = timeFormat else {
            return FormatHelper.timeFormat(seconds: endTime - Float(round(firstCrackTime)))
        }
        return FormatHelper.timeFormat(seconds: endTime - Float(round(firstCrackTime)), stringFormat: format)
    }
    
    func endTempDescription(_ tempFormat: String? = nil) -> String {
        guard let temperature = roast?.lastRoastingPoint()?.temperature else {
            return ""
        }
        guard let format = tempFormat else {
            return FormatHelper.tempFormat(temperature: round(temperature))
        }
        return FormatHelper.tempFormat(temperature: round(temperature), format)
    }
    
    func endTimeDescription(_ timeFormat: String? = nil) -> String {
        guard let time = endTimeRounded() else {
            return ""
        }
        guard let format = timeFormat else {
            return FormatHelper.timeFormat(seconds: time)
        }
        return FormatHelper.timeFormat(seconds: time, stringFormat: format)
    }
    
    func dtrTimeDescription(_ timeFormat: String? = nil) -> String {
        guard let lastRoastingPoint = roast?.lastRoastingPoint()?.time, firstCrackTime != 0 else {
            return ""
        }
        let dtr = round(lastRoastingPoint) - Float(round(firstCrackTime))
        guard let format = timeFormat else {
            return FormatHelper.timeFormat(seconds: dtr)
        }
        return FormatHelper.timeFormat(seconds: dtr, stringFormat: format)
    }
    
    func dryingDescription(format: String = "%.1f") -> String {
        guard let endTime = endTimeRounded(), colorChangeTime != 0 else {
            return "N/A"
        }
        let drying = (Float(round(colorChangeTime)) / endTime) * 100
        return String(format: format, drying) + "%"
    }
    
    func maillardDescription(format: String = "%.1f") -> String {
        guard let endTime = endTimeRounded(), firstCrackTime != 0, colorChangeTime != 0 else {
            return "N/A"
        }
        let maillard = Float(round(firstCrackTime) - round(colorChangeTime))  / endTime * 100
        return String(format: format, maillard) + "%"
    }
    
    
    func dtrDescription(format: String = "%.1f") -> String {
        guard let endTime = endTimeRounded(), firstCrackTime != 0  else {
            return "N/A"
        }
        let dtr = Float(endTime - Float(round(firstCrackTime))) / endTime * 100
        return String(format: format, dtr) + "%"
    }
    
    func endTimeRounded() -> Float? {
        guard let endTime = roast?.lastRoastingPoint()?.time else {
            return nil
        }
        return round(endTime)
    }
    
    
    func getDescription() -> String {
        var description = ""
        let turningPointLabel = NSLocalizedString("$note_turning_point", comment: "") + " %@"
        let colorChangeLabel = NSLocalizedString("$note_color_change", comment: "") + " %@"
        description += String(format: NSLocalizedString("$note_date_and_time", comment: "").replacingOccurrences(of: "%s", with: "%@"), roast!.getReadableDate()) + "<br>"
        let lastPoint = (roast?.lastRoastingPoint())!
        
        if (USE_FAHRENHEIT) {
            description += (String(format: NSLocalizedString("$note_end_temp_f", comment: ""), Int(TempToFahrenheit(temp: Double(lastPoint.temperature)))) + "<br>")
        } else {
            description += (String(format: NSLocalizedString("$note_end_temp_c", comment: ""), Int(lastPoint.temperature)) + "<br>")
        }
        description += String(format: NSLocalizedString("$note_end_time_ms", comment: ""), Int(lastPoint.time / 60), Int(lastPoint.time.truncatingRemainder(dividingBy: 60))) + "<br>"
        description += String(format: turningPointLabel, turningPointDescription().isEmpty ? "-" : turningPointDescription()) + "<br>"
        description += String(format: colorChangeLabel, colorChangeDescription().isEmpty ? "-" : colorChangeDescription()) + "<br>"
        description += String(format: NSLocalizedString("$note_first_crack", comment: "").replacingOccurrences(of: "%s", with: "%@"), firstCrackDescription().isEmpty ? "-" : firstCrackDescription() ) + "<br>"
        description += String(format: NSLocalizedString("$note_second_crack", comment: "").replacingOccurrences(of: "%s", with: "%@"), secondCrackDescription().isEmpty ? "-" : secondCrackDescription()) + "<br>"
        description += String(format: NSLocalizedString("$note_dtr", comment: "").replacingOccurrences(of: "%s", with: "%@"), dtrDescription().isEmpty ? "-" : dtrDescription()) + "<br>"
        description += String(format: NSLocalizedString("$note_coffee_sample", comment: "").replacingOccurrences(of: "%s", with: "%@"), coffeeType) + "<br>"
        description += String(format: NSLocalizedString("$note_notes", comment: "").replacingOccurrences(of: "%s", with: "%@"), notes) + "<br>"
        return description
    }
    
    func profile() -> Profile? {
        return AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: (roast?.profileUuid)!)
    }
    
    
    func serializeToCropsterJson(machineId: String) -> String {
        guard let roast = roast else {
            return ""
        }
        
        var result = ""
        let dataDict = NSMutableDictionary()
        
        let attributesDict = NSMutableDictionary()
        
        guard let roastStartDate = roast.date,
            let lastRoastingPoint = roast.lastRoastingPoint()?.time else {
            return ""
        }
        
        attributesDict["endDate"] = Int(Float(roastStartDate) + lastRoastingPoint) * 1000
        
        attributesDict["endWeight"] = ["amount" : 50, "unit" : "G"]
        attributesDict["notes"] = ""
        
        let attributesSourceDict = NSMutableDictionary()
        attributesSourceDict["lotId"] = coffeeId
        attributesSourceDict["weight"] = ["amount" : 50, "unit" : "G"]
        attributesDict["sources"] = [attributesSourceDict]
        
        attributesDict["startDate"] = Int(roastStartDate) * 1000
        
        dataDict["attributes"] = attributesDict
        
        let relationshipsDict = NSMutableDictionary()
        
        relationshipsDict["machine"] =  ["data" : ["id" : machineId,
                                                    "type" : "machines"]]
        
        relationshipsDict["profile"] = ["data" : ["id" : profile()?.cropsterId, "type" : "profiles"]]
        
        dataDict["relationships"] = relationshipsDict
        
        dataDict["type"] = "roastingActions"
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
    
    func serializeToCropsterEndTempJson(processingId: String) -> String {
        var result = ""
        let dataDict = NSMutableDictionary()
        
        guard let temp = roast?.lastRoastingPoint()?.temperature else {
            return result
        }
        
        dataDict["attributes"] = ["measure" : ["amount" : temp, "unit" : "CELSIUS"],
                                  "name" : "endTemperature"]
        dataDict["type"] = "processingMeasures"
        
        dataDict["relationships"] = ["processing" : ["data" : ["id" : processingId, "type" : "processings"]]]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
    
    
    func serializeToCropsterTurningPointJson(processingId: String) -> String {
        var result = ""
        let dataDict = NSMutableDictionary()
        
        guard let turningPoint = roast?.turningPoint()?.x else {
            return result
        }
        
        dataDict["attributes"] = ["event" : "turningPoint",
                                  "time" : Int(turningPoint * 1000)]
        dataDict["type"] = "processingComments"
        
        dataDict["relationships"] = ["processing" : ["data" : ["id" : processingId, "type" : "processings"]]]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
    
    
    func serializeToCropsterDtrJson(processingId: String) -> String {
        var result = ""
        let dataDict = NSMutableDictionary()
        
        dataDict["attributes"] = ["measure" : ["amount" : Int(dtr), "unit" : "PERCENT"],
                                  "name" : "developmentTimeRatio"]
        dataDict["type"] = "processingMeasures"
        
        dataDict["relationships"] = ["processing" : ["data" : ["id" : processingId, "type" : "processings"]]]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
    
    
    func serializeToCropsterDevelopmentTimeJson(processingId: String) -> String {
        var result = ""
        let dataDict = NSMutableDictionary()
        
        guard let lastRoastingPoint = roast?.lastRoastingPoint()?.time else {
            return result
        }
        let amount = Int(lastRoastingPoint - Float(firstCrackTime))
        dataDict["attributes"] = ["measure" : ["amount" : amount, "unit" : "SECOND"],
                                  "name" : "developmentTime"]
        dataDict["type"] = "processingMeasures"
        
        dataDict["relationships"] = ["processing" : ["data" : ["id" : processingId, "type" : "processings"]]]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
    
    
    func serializeToCropsterFirstCrackJson(processingId: String) -> String {
        var result = ""
        let dataDict = NSMutableDictionary()
        
        dataDict["attributes"] = ["event" : "firstCrack",
                                  "time" : Int(firstCrackTime * 1000)]
        dataDict["type"] = "processingComments"
        
        dataDict["relationships"] = ["processing" : ["data" : ["id" : processingId, "type" : "processings"]]]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
    
    func serializeToCropsterSecondCrackJson(processingId: String) -> String {
        var result = ""
        let dataDict = NSMutableDictionary()
        
        dataDict["attributes"] = ["event" : "secondCrack",
                                  "time" : Int(secondCrackTime * 1000)]
        dataDict["type"] = "processingComments"
        
        dataDict["relationships"] = ["processing" : ["data" : ["id" : processingId, "type" : "processings"]]]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
    
    func serializeToCropsterColorChangeJson(processingId: String) -> String {
        var result = ""
        let dataDict = NSMutableDictionary()
        
        dataDict["attributes"] = ["event" : "colorChange",
                                  "time" : Int(colorChangeTime * 1000)]
        dataDict["type"] = "processingComments"
        
        dataDict["relationships"] = ["processing" : ["data" : ["id" : processingId, "type" : "processings"]]]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
}
