//
//  RoasterStatus+Utils.swift
//  IKAWApp
//
//  Created by Admin on 5/21/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

extension RoasterStatus {
    
    func roastPointDescription() -> String! {
        var result = "time,fan set,setpoint,fan speed,temp above,state,heater,p,i,d,j\n"
        result.append(",\(fan_power),\(fanSpeed),\(temp_above),\(stateName()!),\(heater),\(p),\(i),\(d),\(j)\n")
        return result
    }
    
}

