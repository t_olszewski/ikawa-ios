//
//  ExProfile.swift
//  IKAWApp
//
//  Created by Admin on 3/29/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

extension Profile {
    func  roastPointsToRateOfRisePoints(rollingAverage: Int = 10) -> [RoastTempPoint]? {
        if (self.roastPoints.count < 2) {
            return nil
        }
        var rorData = [RoastTempPoint]()
        var curRoastDataIndex = 1
        var prevPoint = self.roastPoints[0] as! RoastTempPoint
        var curPoint = self.roastPoints[1] as! RoastTempPoint
        var curRor = Double((curPoint.temperature - prevPoint.temperature) / (curPoint.time - prevPoint.time) * 60)
        var rorAverage = Array(repeating: curRor, count: rollingAverage)
        var rorAverageIndex = 0
        for i in 0..<Int((self.roastPoints.last as! RoastTempPoint).time) {
            
            if (curRoastDataIndex == roastPoints.count) {
                break
            }
            
            if (i >= Int((roastPoints[curRoastDataIndex] as! RoastTempPoint).time)) {
                curRoastDataIndex = curRoastDataIndex + 1
                prevPoint = curPoint
                curPoint = self.roastPoints[curRoastDataIndex] as! RoastTempPoint
                curRor = Double((curPoint.temperature - prevPoint.temperature) / (curPoint.time - prevPoint.time) * 60)
            }
            rorAverage[rorAverageIndex] = curRor
         
            var averageRor: Double! = 0
            for rorPerSecond in rorAverage {
                averageRor = averageRor + rorPerSecond
            }
            averageRor = averageRor / Double(rollingAverage)
            if (rollingAverage > 1) {
                rorAverageIndex = rorAverageIndex + 1
                if (rorAverageIndex == rollingAverage) {
                    rorAverageIndex = 0
                }
                
            }
            
            let rorPoint = RoastTempPoint()
            rorPoint.time = Float(i)
            rorPoint.temperature = Float(averageRor)
            rorData.append(rorPoint)
        
        }
        return rorData
    }
    
    func tempPointsToString(points: [RoastTempPoint]) -> String! {
        var resultString = String()
        for point in points {
            resultString.append("\(point.time),\(point.temperature)")
            if (points.last != point) {
                resultString.append(",")
            }
        }
        return resultString
    }
    
    func fanPointsToString(points: [RoastFanPoint]) -> String! {
        var resultString = String()
        for point in points {
            resultString.append("\(point.time),\(point.power)")
            if (points.last != point) {
                resultString.append(",")
            }
        }
        return resultString
    }
    
    func isReadOnly() -> Bool {
        return uuid == IK_STANDARD_PROFILE_ID || type == ProfileTypeIkawa
    }
    
    func getHighestRoastPoint() -> RoastTempPoint? {
        guard (roastPoints != nil && roastPoints.count > 0) else {
            return nil
        }
        var result: RoastTempPoint = roastPoints.first as! RoastTempPoint
        for point in roastPoints {
            if ((point as! RoastTempPoint).temperature > result.temperature) {
                result = point as! RoastTempPoint
            }
        }
        return result
    }
    
    func checkPoints() {
        //sort
        roastPoints.sort {($0 as! RoastTempPoint).time < ($1 as! RoastTempPoint).time}
        fanPoints.sort {($0 as! RoastTempPoint).time < ($1 as! RoastTempPoint).time}
        
        //check value ranges
        let useSensorBelow = tempSensor == TempSensorBelow
        roastPoints = roastPoints.map { (point) -> RoastTempPoint in
            let p = point as! RoastTempPoint
            p.time = min(1200, p.time)
            p.temperature = min(useSensorBelow ? Float(IK_MAX_TEMP_BELOW) : Float(IK_MAX_TEMP_ABOVE), p.temperature)
            return p
        }
        
        fanPoints = fanPoints.map { (point) -> RoastFanPoint in
            let p = point as! RoastFanPoint
            p.time = min(1200, p.time)
            p.power = min(Float(IK_MAX_FAN_SPEED), max(Float(IK_MIN_FAN_SPEED), p.power))
            return p
        }
        
        //check begin points
        var fanPoint = fanPoints[0] as! RoastFanPoint
        fanPoint.time = 0
        fanPoints[0] = fanPoint
        
        var roastPoint = roastPoints[0] as! RoastTempPoint
        roastPoint.time = 0
        roastPoints[0] = roastPoint
        
        //check end points
        fanPoint = fanPoints[fanPoints.count - 1] as! RoastFanPoint
        roastPoint = roastPoints[roastPoints.count - 1] as! RoastTempPoint
        fanPoint.time = roastPoint.time
        fanPoints[fanPoints.count - 1] = fanPoint
        
        //check cooldown point
        fanPoint = cooldownPoint
        fanPoint.time = min(roastPoint.time + Float(IK_MAX_COOLDOWN_TIME), max(roastPoint.time + 60, fanPoint.time))
        fanPoint.power = min(Float(IK_MAX_FAN_SPEED), max(Float(IK_MIN_FAN_SPEED), fanPoint.power))
        cooldownPoint = fanPoint
    }
    
    func getNameWithRevision() -> String {
        if (revision > 0) {
            return "\(name!) (v.\(revision))"
        } else {
            return name!
        }
    }
    
    func getNameWithType() -> String {
//        #if TARGET_PRO
        return getNameWithRevision().replacingOccurrences(of: "\n", with: " ");
//        #else
//        var prefix = ""
//        switch type {
//        case ProfileTypeIkawa:
//            prefix = String(format: "\n%@", NSLocalizedString("$recommended_roast", comment: ""))
//        case ProfileTypeUser:
//            prefix = ""
//        case ProfileType3rdParty:
//            prefix = String(format: "\n%@", NSLocalizedString("$3rd_party_roast", comment: ""))
//        default:
//            prefix = ""
//        }
//        return name + prefix
//        return name
//        #endif
    }
    
    func isRoasterProfile() -> Bool {
        guard (IKInterface.instance()?.isConnected() == true || IK_SIMULATE_ROASTER), let roasterProfile = RoasterManager.instance().profile else {
            return false
        }
        
        return uuid == roasterProfile.uuid
    }
    
    func roastTime() -> String {
        let time = cooldownPoint.time
        if  time >= 0 {
            let minutes = Int(time / 60)
            let minutesStr = minutes < 10 ? "0\(minutes)" : "\(minutes)"
            let sec = Int(time.truncatingRemainder(dividingBy: 60))
            let secStr = sec < 10 ? "0\(sec)" : "\(sec)"
            return "\(minutesStr):\(secStr)"
        } else {
            return "00:00"
        }
        
    }
    
    func isNeverUsed() -> Bool {
        return dateLastRoasted == Date(timeIntervalSince1970: 1494872272) && (revision == 0)
    }

}

extension Profile {
     #if TARGET_PRO
    func serializeToCropsterJson(machineId: String) -> String {
        var result = ""
        let dataDict = NSMutableDictionary()
        
        let attributesDict = NSMutableDictionary()
        attributesDict["erpId"] = uuid
        attributesDict["name"] = name
        
        dataDict["attributes"] = attributesDict
        
        let relationshipsDict = NSMutableDictionary()
        let relationshipsGroupDict = NSMutableDictionary()
        let relationshipsGroupDataDict = NSMutableDictionary()
        relationshipsGroupDataDict["id"] = AppCore.sharedInstance.cropsterManager.group!
        relationshipsGroupDataDict["type"] = "groups"
        relationshipsGroupDict["data"] = relationshipsGroupDataDict
        relationshipsDict["group"] = relationshipsGroupDict
        dataDict["relationships"] = relationshipsDict
        
        let restrictedMachinesDict = ["data" : [["id" : machineId,
                                                 "type" : "machines"]]]
        relationshipsDict["restrictedMachines"] = restrictedMachinesDict
        dataDict["type"] = "profiles"
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
    #endif
    func serializeToJson() -> String {
        let dict = NSMutableDictionary()
        var result = ""

        dict["creator_id"] = AppCore.sharedInstance.firebaseManager.currentUser?.uid ?? ""
        dict["device_created_id"] = AppCore.sharedInstance.settingsManager.deviceUuid
        dict["profile_id"] = uuid
        dict["parent_id"] = parentUuid
        dict["date_created"] = Date().dateStr()
        dict["scheme_version"] = schemaVersion
        dict["roast_points"] = serializeRoastPoints()
        dict["fan_points"] = serializeFanPoints()
        dict["roast_duration"] = cooldownPoint.time
        dict["cooldown_point"] = "\(cooldownPoint.time),\(cooldownPoint.power)"
        dict["temp_sensor"] = tempSensor.rawValue
        dict["name"] = name
        
        #if TARGET_HOME
        dict["coffee_id"] = coffeeId
        dict["coffee_name"] = coffeeName
        dict["coffee_web_url"] = coffeeWebUrl
        dict["user_id"] = userId
        #endif

        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }

        return result
    }
    
    func serializeRoastPoints() -> String {
        var result = ""
        if ((roastPoints as! [RoastTempPoint]).count == 0) {
            return result
        }
        for p in (roastPoints as! [RoastTempPoint]) {
            if (p != (roastPoints as! [RoastTempPoint]).first) {
               result.append(",")
            }
            result.append("\(p.time),\(p.temperature)")
        }
        print(result)
        return result
    }
    
    func serializeFanPoints() -> String {
        var result = ""
        if ((fanPoints as! [RoastFanPoint]).count == 0) {
            return result
        }
        for p in (fanPoints as! [RoastFanPoint]) {
            if (p != (fanPoints as! [RoastFanPoint]).first) {
                result.append(",")
            }
            result.append("\(p.time),\(p.power)")
        }
        print(result)
        return result
    }
    
    func getSummary() -> String {
        var result = ""
        result += "Profile name: \(name!) \n"
        result += "Profile uuid: \(uuid!) \n"
        guard let hex = AppCore.sharedInstance.profileManager.base64EncodeProfile(profile: self) else {
            return result
        }
        result += "Profile url: \(IK_PROFILE_BASEURL + hex) \n"
        return result
    }
    
    func roastingProgressTime() -> Double {
        if roastPoints != nil {
            if let lastPoint = roastPoints.last as? RoastTempPoint {
                return Double(lastPoint.time)
            }
        }
        return 0
    }

    
    
    func finishTemp() -> Double {
        if roastPoints != nil {
            if let lastPoint = roastPoints.last as? RoastTempPoint {
                return Double(lastPoint.temperature)
            }
        }
        return 0
    }
    
}

