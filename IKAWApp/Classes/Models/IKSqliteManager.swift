//
//  IKSqliteManager.swift
//  IKAWApp
//
//  Created by Admin on 3/23/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import GRDB
import IKRoasterLib

class IKSqliteManager: NSObject {
    
    var dbQueue: DatabaseQueue?
    var schemeVersion = 0
    var dbName: String!
    var needMigrateDB: Bool
    required init(dbName: String, needMigrateDB: Bool = true) {
        self.dbName = dbName
        self.needMigrateDB = needMigrateDB
        super.init()
        _ = self.openDb(fileName: self.dbName)
    }
    
    deinit {
        print("!!!! deinit")
    }
    
    func removeDB() -> Bool {
        dbQueue = nil
        let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        print(documentDirectoryURL.absoluteString)
        let dbFilePathDocumentsDirectoty = documentDirectoryURL.appendingPathComponent(self.dbName)
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: dbFilePathDocumentsDirectoty.path) {
            do {
                try fileManager.removeItem(atPath: dbFilePathDocumentsDirectoty.path)
            }
            catch {
                return false
            }
        }
        return true
    }
    
    func isDBExists(fileName: String) -> Bool {
        let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        print(documentDirectoryURL.absoluteString)
        let dbFilePathDocumentsDirectoty = documentDirectoryURL.appendingPathComponent(fileName)
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: dbFilePathDocumentsDirectoty.path) {
            return true
        }
        return false
    }
    
    func openDb(fileName: String)-> DatabaseQueue? {
        dbQueue = nil
        let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        print(documentDirectoryURL.absoluteString)
        let dbFilePathDocumentsDirectoty = documentDirectoryURL.appendingPathComponent(fileName)
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: dbFilePathDocumentsDirectoty.path) {
            print("FILE AVAILABLE")
        } else {
            print("FILE NOT AVAILABLE")
            let dbFilePathBundleDirectory = Bundle.main.url(forResource: fileName, withExtension: "", subdirectory: "")!
            if fileManager.fileExists(atPath: dbFilePathBundleDirectory.path) {
                print("FILE AVAILABLE")
                do {
                    try fileManager.copyItem(at: dbFilePathBundleDirectory, to: dbFilePathDocumentsDirectoty)
                }
                catch let error {
                    // handle error here
                    print("Error trying to open DB: \(error.localizedDescription)")
                    return nil
                }
                
            } else {
                return nil
            }
        }
        
        
        do {
            dbQueue = try DatabaseQueue(path: dbFilePathDocumentsDirectoty.path)
        }
        catch let error {
            // handle error here
            print("Error trying to open DB: \(error.localizedDescription)")
            return nil
        }
        
        _ = setSchemeVersion()
        
        // Migrate the database to the latest version.
        if needMigrateDB {
            if schemeVersion < IK_CURRENT_DB_SCHEMA_VERSION {
                _ = migrateDb()
                UserDefaults.standard.set(true, forKey: "db_changed")
                UserDefaults.standard.set(true, forKey: "NeedMergeWithAccount")
                UserDefaults.standard.synchronize()
            }
        }
        
        return dbQueue
    }
    
    func closeDB() {
        dbQueue = nil
    }
    
    func setSchemeVersion() -> Bool {
        do {
            try dbQueue!.inDatabase { db in
                let rows = try Row.fetchCursor(db, "SELECT schemeVersion FROM version")
                if let row = try rows.next() {
                    schemeVersion = row["schemeVersion"]
                }
            }
        }
        catch let error {
            // handle error here
            print("Error trying to open DB: \(error.localizedDescription)")
            schemeVersion = 0 // Missing table set scheme version to 0
        }
        return true
    }
    
    //MARK: - Migration
    func migrateDb() -> Bool {
        
        while schemeVersion < IK_CURRENT_DB_SCHEMA_VERSION {
            switch schemeVersion {
            case 0:
                if (migrateToV1() != true) { return false}
                break
            case 1:
                if (migrateToV2() != true) { return false}
                break
            case 2:
                if (migrateToV3() != true) { return false}
                break
            case 3:
                if (migrateToV4() != true) { return false}
                break
            case 5:
                if (migrateToV6() != true) { return false}
                break
            case 6:
                if (migrateToV7() != true) { return false}
                break
            case 7:
                if (migrateToV8() != true) { return false}
                break
            case 8:
                if (migrateToV9() != true) { return false}
                break
            case 9:
                if (migrateToV10() != true) { return false}
                break
            case 10:
                if (migrateToV11() != true) { return false}
                break
            case 11:
                if (migrateToV12() != true) { return false}
                break
            case 12:
                if (migrateToV13() != true) { return false}
                break
            default:
                break
            }
        }
        return true
    }
    
    func migrateToV1() -> Bool {
        // Create version table
        do {
            try dbQueue?.inDatabase { db in
                try db.execute("CREATE TABLE version (id INTEGER, schemeVersion INTEGER)")
            }
        }
        catch let error {
            // handle error here
            print("CREATE TABLE version (id INTEGER, schemeVersion INTEGER): \(error.localizedDescription)")
            return false
        }
        
        // Insert version number
        do {
            try dbQueue?.inDatabase { db in
                try db.execute("INSERT INTO version (id, schemeVersion) VALUES (0, 1)")
            }
        }
        catch let error {
            // handle error here
            print("INSERT INTO version (id, schemeVersion) VALUES (0, 1): \(error.localizedDescription)")
            return false
        }
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute("ALTER TABLE profile ADD COLUMN cooldownPoint STRING DEFAULT '0,0';")
            }
        }
        catch let error {
            // handle error here
            print("ALTER TABLE profile ADD COLUMN cooldownPoint STRING DEFAULT '0,0';: \(error.localizedDescription)")
            return false
        }
        
        /*
         * In this version we added an onServer column in the profile table
         */
        do {
            try dbQueue?.inDatabase { db in
                try db.execute("ALTER TABLE profile ADD COLUMN onServer INTEGER DEFAULT 0;")
            }
        }
        catch let error {
            // handle error here
            print("ALTER TABLE version ADD COLUMN onServer INTEGER DEFAULT 0;: \(error.localizedDescription)")
            return false
        }
        // Update scheme version
        schemeVersion = 1
        return true
    }
    
    func migrateToV2() -> Bool {
        
        // Create roastNotes table
        var query = "CREATE TABLE \"roastNotes\" (\"roastId\" TEXT not null, \"firstCrackTime\" FLOAT, \"firstCrackTemp\" FLOAT, \"secondCrackTime\" FLOAT, \"secondCrackTemp\" FLOAT, \"DTR\" FLOAT, \"coffeeType\" TEXT, \"notes\" TEXT, \"photo\" TEXT, \"extraInfo\" TEXT, primary key (\"roastId\"))"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        // Add profile revision and showInLibrary
        query = "ALTER TABLE profile ADD COLUMN revision INTEGER DEFAULT 0;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        query = "ALTER TABLE profile ADD COLUMN showInLibrary INTEGER DEFAULT 1;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        query = "UPDATE version SET schemeVersion = 2;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        
        // Update scheme version
        schemeVersion = 2
        return true
    }
    
    
    func migrateToV3() -> Bool {
        var query = "ALTER TABLE profile ADD COLUMN profileType INTEGER DEFAULT 0;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        query = "UPDATE version SET schemeVersion = 3;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        // Update scheme version
        schemeVersion = 3
        return true
    }
    
    func migrateToV4() -> Bool {
        
        #if TARGET_PRO
        var query = "ALTER TABLE profile ADD COLUMN tempSensor INTEGER DEFAULT 0;"
        #else
        var query = "ALTER TABLE profile ADD COLUMN tempSensor INTEGER DEFAULT 1;"
        #endif
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        query = "UPDATE version SET schemeVersion = 4;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        // Update scheme version
        schemeVersion = 4
        return true
    }
    
    func migrateToV5() -> Bool {
        
        var query = "ALTER TABLE profile ADD COLUMN dateLastRoasted INTEGER DEFAULT 0;"
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        query = "UPDATE profile SET dateLastRoasted = (SELECT roastLog.roastDate FROM roastLog WHERE roastLog.profileUUID = profile.UUID ) WHERE EXISTS ( SELECT * FROM roastLog WHERE roastLog.profileUUID = profile.UUID)"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        query = "UPDATE version SET schemeVersion = 5;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        // Update scheme version
        schemeVersion = 5
        return true
    }
    
    
    func migrateToV6() -> Bool {
        
        var query = "ALTER TABLE profile ADD COLUMN cropsterId CHAR;"
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        query = "ALTER TABLE roastNotes ADD COLUMN coffeeId TEXT;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        
        //        #if TARGET_HOME
        //        query = "INSERT INTO profile (UUID, parentUUID, name, schemeVersion, roastPoints, fanPoints, dateCreated, onServer, cooldownPoint, revision, showInLibrary, tempSensor, dateLastRoasted, profileType) VALUES ('104B2224E70744F69EBB3D4F1AA3F7CE', '', 'La Concepcion' || x'0a' || 'Honduras | 50g', '1', '0.00,50.00,29.40,121.10,67.60,234.40,186.00,234.00,285.80,249.70,395.40,241.70', '0.00,80.39,395.40,64.71', '1494872272', '1','534.400024,80.784317', 0, 1, 1, '1494872272', '1');"
        //        do {
        //            try dbQueue?.inDatabase { db in
        //                try db.execute(query)
        //            }
        //        }
        //        catch let error {
        //            // handle error here
        //            print("SQL error: " + query + " \(error.localizedDescription)")
        //            return false
        //        }
        //        #endif
        
        
        query = "UPDATE version SET schemeVersion = 6;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        // Update scheme version
        schemeVersion = 6
        return true
    }
    
    
    func migrateToV7() -> Bool {
        
        var query = "ALTER TABLE roastNotes ADD COLUMN colorChangeTime FLOAT;"
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        query = "ALTER TABLE roastNotes ADD COLUMN colorChangeTemp FLOAT;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        query = "UPDATE version SET schemeVersion = 7;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        // Update scheme version
        schemeVersion = 7
        return true
    }
    
    func migrateToV8() -> Bool {
        
        var query = "ALTER TABLE profile ADD COLUMN coffeeId TEXT;"
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        query = "ALTER TABLE profile ADD COLUMN userId TEXT;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        
        // Create coffee table
        query = "CREATE TABLE \"coffee\" (\"ID\" TEXT not null, \"coffeeName\" TEXT, \"creatorId\" TEXT, \"seller\" TEXT, \"webLink\" TEXT, primary key (\"ID\"))"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        
        query = "INSERT INTO coffee (ID, coffeeName, creatorId, seller, webLink) VALUES " +
            "('260', 'Finca Puerta Verde Guatemala | 50g', 'IKAWA', 'IKAWA', ''), " +
            "('265', 'Andino Especial Colombia | 50g', 'IKAWA', 'IKAWA', ''), " +
            "('269', 'Sitio Bela Vista Brazil | 50g', 'IKAWA', 'IKAWA', ''), " +
            "('273', 'Kochere Debo Ethiopia | 50g', 'IKAWA', 'IKAWA', ''), " +
            "('277', 'Shakiso Ethiopia Natural | 50g', 'IKAWA', 'IKAWA', ''), " +
            "('281', 'Muninya Hill Burundi | 50g', 'IKAWA', 'IKAWA', ''), " +
        "('8585', 'La Concepcion Honduras | 50g', 'IKAWA', 'IKAWA', ''); "
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
            
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        for query in [
            "UPDATE profile SET coffeeId = '260', name = 'IKAWA Suggested Roast' WHERE UUID = 'AD90F637FEC044789D66269D27A20635'",
            "UPDATE profile SET coffeeId = '265', name = 'IKAWA Suggested Roast' WHERE UUID = 'A487B179A72E451AB3177E70015FBC7D'",
            "UPDATE profile SET coffeeId = '269', name = 'IKAWA Suggested Roast' WHERE UUID = '69DC599FF20045FD864E658A879FA071'",
            "UPDATE profile SET coffeeId = '273', name = 'IKAWA Suggested Roast' WHERE UUID = 'E317D02150774C82B73232B4368C221B'",
            "UPDATE profile SET coffeeId = '277', name = 'IKAWA Suggested Roast' WHERE UUID = 'BE3C582774814E5480DDAA13083C9F72'",
            "UPDATE profile SET coffeeId = '281', name = 'IKAWA Suggested Roast' WHERE UUID = '063196EDA6EE4294973A0B02545623D7'",
            "UPDATE profile SET coffeeId = '8585', name = 'IKAWA Suggested Roast' WHERE UUID = '104B2224E70744F69EBB3D4F1AA3F7CE'",
            ] {
                do {
                    try dbQueue?.inDatabase { db in
                        try db.execute(query)
                    }
                }
                catch let error {
                    // handle error here
                    print("SQL error: " + query + " \(error.localizedDescription)")
                    return false
                }
        }
        
        
        
        
        
        //        query = "INSERT INTO profile (UUID, coffeeId, name, schemeVersion, roastPoints, fanPoints, cooldownPoint, onServer, dateCreated, revision, showInLibrary, tempSensor, dateLastRoasted, profileType, parentUUID) VALUES "
        //            + "('DDCBB1B6F1EB4C7B9613294956F274FC', '8568', 'Light Filter | 50g', '1', '0.00,50.00,29.40,121.10,67.60,174.70,170.90,207.70,305.40,239.50,461.70,237.00', '0.00,80.00,461.70,64.71', '600.70,78.43', '1','1494872272', 0, 1, 1, '1494872272', '1', ''), "
        //            + "('7AEE603C4147428592E6ADED290E8ECC', '8585', 'Medium Filter | 50g', '1', '0.00,50.00,29.40,121.10,67.60,234.40,186.00,234.00,285.80,249.70,395.40,241.70', '0.00,80.39,395.40,64.71', '534.40,80.78', '1','1494872272', 0, 1, 1, '1494872272', '1', ''), "
        //            + "('7F7430F501ED4856B8F65DE488482293', '281', 'Espresso | 50g', '1', '0.00,50.00,34.00,200.80,119.30,157.80,465.70,250.30,515.10,257.00,582.10,258.30', '0.00,80.00,210.60,63.14,582.10,68.63', '697.60,80.00', '1','1494872272', 0, 1, 1, '1494872272', '1', ''), "
        //            + "('B4E2C04263F147BF8C3A48434CFF041E', '265', 'Espresso | 50g', '1', '0.00,50.00,34.00,200.80,71.10,149.00,465.70,253.80,519.80,254.40,582.10,253.10', '0.00,79.22,178.90,63.14,582.10,68.63', '697.60,74.90', '1','1494872272', 0, 1, 1, '1494872272', '1', ''), "
        //            + "('92317A75B51B4644A6ACEAF8F3C9A102', '260', '2nd Crack Espresso | 50g', '1', '0.00,50.00,60.20,186.80,437.10,250.30,514.20,262.10,604.00,288.50', '0.00,80.00,181.10,65.88,604.00,68.63', '719.50,74.90', '1','1494872272', 0, 1, 1, '1494872272', '1', ''), "
        //            + "('26F43408A01D41F5B2E2E5314DFF9E59', '277', 'Traditional Espresso | 50g ', '1', '0.00,50.00,60.20,186.80,239.90,220.70,471.10,250.30,526.20,257.00,596.80,258.30', '0.00,79.61,150.60,63.14,596.80,68.63', '712.30,74.90', '1','1494872272', 0, 1, 1, '1494872272', '1', ''), "
        //            + "('41B5DF54376E4EA282F41087B24776AC', '269', 'Third Wave Espresso | 50g', '1', '0.00,50.00,76.30,226.70,101.60,205.40,211.60,232.10,351.80,243.20,431.50,244.80', '0.00,79.22,170.70,63.14,431.50,74.51', '547.00,66.67', '1','1494872272', 0, 1, 1, '1494872272', '1', ''), "
        //            + "('2273BA4EF7C44C30A7E205AF7D1CBA97', '8568', 'Espresso | 50g', '1', '0.00,50.00,34.00,200.80,119.30,157.80,465.70,253.30,515.10,257.00,582.10,258.30', '0.00,80.00,210.60,63.14,582.10,68.63', '697.60,80.00', '1','1494872272', 0, 1, 1, '1494872272', '1', ''), "
        //            + "('BFBD37DABFAB499588B8E6992AB30C56', '8585', 'Third Wave Espresso | 50g', '1', '0.00,50.00,144.30,240.40,200.00,213.90,300.60,238.30,403.90,246.60,481.20,248.70', '0.00,79.22,241.30,66.67,481.20,80.00', '596.70,66.67', '1','1494872272', 0, 1, 1, '1494872272', '1', '');"
        //
        //        do {
        //            try dbQueue?.inDatabase { db in
        //                try db.execute(query)
        //            }
        //        }
        //
        //        catch let error {
        //            // handle error here
        //            print("SQL error: " + query + " \(error.localizedDescription)")
        //            return false
        //        }
        
        
        query = "UPDATE version SET schemeVersion = 8;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        
        //create table user
        query = "CREATE TABLE IF NOT EXISTS \"user\" ( \"userId\" TEXT NOT NULL, \"downloadUrl\" TEXT,  PRIMARY KEY (\"userId\"))"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        
        ;
        
        // Update scheme version
        schemeVersion = 8
        return true
    }
    
    func migrateToV9() -> Bool {
        
        
        for query in [
            "DELETE FROM profile WHERE UUID = '063196EDA6EE4294973A0B02545623D7'",
            "DELETE FROM profile WHERE UUID = 'A487B179A72E451AB3177E70015FBC7D'",
            "DELETE FROM profile WHERE UUID = '69DC599FF20045FD864E658A879FA071'",
            "DELETE FROM profile WHERE UUID = 'E317D02150774C82B73232B4368C221B'",
            "DELETE FROM profile WHERE UUID = 'BE3C582774814E5480DDAA13083C9F72'",
            "DELETE FROM profile WHERE UUID = 'AD90F637FEC044789D66269D27A20635'",
            "DELETE FROM profile WHERE UUID = '104B2224E70744F69EBB3D4F1AA3F7CE'",
            
            
            "DELETE FROM profile WHERE UUID = 'DDCBB1B6F1EB4C7B9613294956F274FC'",
            "DELETE FROM profile WHERE UUID = 'D6ECD3DC6EE748CD8E195AC381BCCD8F'",
            "DELETE FROM profile WHERE UUID = '42271A4F4CDD45CCA45A8EF6A8F9E1EC'",
            "DELETE FROM profile WHERE UUID = 'F4A39CA0A2934AB29E4FA712655663F6'",
            "DELETE FROM profile WHERE UUID = '028B935B30AD4699ADBC11F8910389EF'",
            "DELETE FROM profile WHERE UUID = 'A22A2B23E9D640F9824BCD02EC62FA3F'",
            "DELETE FROM profile WHERE UUID = 'B2E3648A83E9495588EED020A7C6344A'",
            "DELETE FROM profile WHERE UUID = '063196EDA6EE4294973A0B02545623D7'",
            "DELETE FROM profile WHERE UUID = 'BE3C582774814E5480DDAA13083C9F72'",
            "DELETE FROM profile WHERE UUID = 'A487B179A72E451AB3177E70015FBC7D'",
            "DELETE FROM profile WHERE UUID = '69DC599FF20045FD864E658A879FA071'",
            "DELETE FROM profile WHERE UUID = 'AD90F637FEC044789D66269D27A20635'",
            "DELETE FROM profile WHERE UUID = '7AEE603C4147428592E6ADED290E8ECC'",
            
            "DELETE FROM profile WHERE UUID = '101D3768025F4930BA7D3F3E01AA81A5'",
            "DELETE FROM profile WHERE UUID = 'BFDA369DF291485D97E409F9E28CA74F'",
            "DELETE FROM profile WHERE UUID = '5D981E1545584086AAD979C74CF9E455'",
            "DELETE FROM profile WHERE UUID = '5A7BF579D42B473DA6470B76C4534473'"
            ] {
                do {
                    try dbQueue?.inDatabase { db in
                        try db.execute(query)
                    }
                }
                catch let error {
                    // handle error here
                    print("SQL error: " + query + " \(error.localizedDescription)")
                    return false
                }
        }
        
        #if TARGET_HOME
        
        for query in [
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('DDCBB1B6F1EB4C7B9613294956F274FC', '', 'Ethiopia Wenago', '', '1', '0.0,50.0,29.4,121.1,67.6,174.7,170.9,207.7,305.4,239.5,461.7,237.0', '0.0,80.0,461.7,64.7', '1554808148', '600.7,78.4', '0', '0', '1', '1', '1', '1494872272');",
            
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('D6ECD3DC6EE748CD8E195AC381BCCD8F', '', 'Brazil Sitio Esperança Filter', '', '1', '0.0,50.0,21.3,84.4,70.1,209.5,161.4,232.8,255.9,255.6,364.1,215.8', '0.0,80.0,90.0,60.8,364.1,60.0', '1554808181', '424.1,77.3', '0', '0', '1', '1', '1', '1494872272');",
            
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('42271A4F4CDD45CCA45A8EF6A8F9E1EC', '', 'Brazil Sitio Esperança Espresso', '', '1', '0.0,50.0,21.3,84.4,204.7,266.7,284.6,251.5,379.7,237.5', '0.0,80.0,297.6,60.0,379.7,79.6', '1554808200', '444.9,80.0', '0', '0', '1', '1', '1', '1494872272');",
            
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('F4A39CA0A2934AB29E4FA712655663F6', '', 'Kenya AA Maganjo Filter', '', '1', '0.0,50.0,176.4,243.6,214.0,262.3,241.1,243.6,387.8,239.3', '0.0,77.6,387.8,63.5', '1554808277', '481.9,80.0', '0', '0', '1', '1', '1', '1494872272');",
            
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('028B935B30AD4699ADBC11F8910389EF', '', 'Kenya AA Maganjo Espresso', '', '1', '0.0,50.0,65.2,177.2,241.1,243.6,249.0,261.9,332.0,250.6,433.8,233.8', '0.0,77.6,433.8,63.5', '1554808304', '527.9,80.0', '0', '0', '1', '1', '1', '1494872272');",
            
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('A22A2B23E9D640F9824BCD02EC62FA3F', '', 'Ethiopia Hafurso Waro Filter', '', '1', '0.0,50.0,29.4,121.1,109.9,219.2,206.7,246.7,305.4,239.5,369.2,226.7', '0.0,80.4,369.2,65.1', '1554808336', '508.2,78.4', '0', '0', '1', '1', '1', '1494872272');",
            
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('B2E3648A83E9495588EED020A7C6344A', '', 'Ethiopia Hafurso Waro Espresso', '', '1', '0.0,50.0,110.3,177.6,265.4,261.8,325.2,243.0,436.6,243.2', '0.0,77.6,436.6,63.5', '1554808354', '530.7,80.0', '0', '0', '1', '1', '1', '1494872272');",
            
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('063196EDA6EE4294973A0B02545623D7', '', 'Burundi Muninya Hill', '', '1', '0.00,50.00,44.10,212.00,105.50,250.10,167.90,244.50,220.00,232.20,361.20,240.90', '0.0,77.7,361.2,63.5', '1494872272', '455.3,80.0', '0', '0', '1', '1', '1', '1494872272');",
            
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('BE3C582774814E5480DDAA13083C9F72', '', 'Ethiopia Shakiso Espresso', '', '1', '0.00,50.00,43.20,199.00,95.50,245.70,151.40,230.20,203.40,245.50,419.40,239.10', '0.0,71.0,419.4,62.4', '1494872272', '492.2,84.7', '0', '0', '1', '1', '1', '1494872272');",
            
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('A487B179A72E451AB3177E70015FBC7D', '', 'Colombia Andino Especial', '', '1', '0.00,50.00,69.20,236.80,89.90,159.60,112.90,195.90,370.20,236.10,520.00,242.00', '0.0,79.2,89.4,66.7,520.0,80.0', '1494872272', '635.5,66.7', '0', '0', '1', '1', '1', '1494872272');",
            
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('69DC599FF20045FD864E658A879FA071', '', 'Brazil Sitio Bela Vista', '', '1', '0.00,50.00,21.30,84.40,384.90,266.10,459.80,239.90', '0.0,80.0,459.8,60.0', '1494872272', '525.0,80.0', '0', '0', '1', '1', '1', '1494872272');",
            
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('AD90F637FEC044789D66269D27A20635', '', 'Guatemala Finca Puerta Verde', '', '1', '0.00,50.00,70.60,80.30,120.10,129.80,181.10,214.90,285.30,242.60,410.70,262.10', '0.0,84.7,150.6,72.9,410.7,65.9', '1494872272', '590.6,80.4', '0', '0', '1', '1', '1', '1494872272');",
            
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('7AEE603C4147428592E6ADED290E8ECC', '', 'Honduras La Concepción', '', '1', '0.00,50.00,29.40,121.10,67.60,234.40,186.00,234.00,285.80,249.70,395.40,241.70', '0.0,80.4,395.4,64.7', '1494872272', '534.4,80.8', '0', '0', '1', '1', '1', '1494872272');"
            ] {
                do {
                    try dbQueue?.inDatabase { db in
                        try db.execute(query)
                    }
                }
                catch let error {
                    // handle error here
                    print("SQL error: " + query + " \(error.localizedDescription)")
                    return false
                }
        }
        
        #elseif TARGET_PRO
        for query in [
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('55639775F1084C7089BDDC0032C162E6', '', 'IKAWA - Sample Roast 1', '', '1', '0.0,137.8,90.0,165.5,198.9,188.3,287.8,201.1,377.3,211.0', '0.0,79.21569,79.6,79.21569,129.3,79.21569,180.7,75.68628,271.7,68.62746,377.3,60.000004', '1575373516', '496.6,69.803925', '0', '0', '1', '1', '2', '1494872272');",
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('5B5F00B5CD234E09A93D12BC05D6569A', '', 'IKAWA - Sample Roast 2', '', '1', '0.0,149.9,14.1,114.3,53.2,137.1,87.9,155.4,153.9,180.6,222.5,200.3,296.6,212.6', '0.0,73.725494,296.6,68.62746', '1575373550', '421.2,80.39216', '0', '0', '1', '1', '2', '1494872272');",
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('838A8034E60F40F5B78C983F9024C8ED', '', 'IKAWA - Geisha Pourover', '', '1', '0.0,155.1,53.3,171.0,145.9,190.6,180.8,195.4,225.7,199.2,261.8,201.0,297.5,202.1', '0.0,79.21569,297.5,66.66667', '1575373822', '427.3,78.43137', '0', '0', '1', '1', '0', '1494872272');",
            "INSERT INTO profile (UUID, parentUUID, name, coffeeId, schemeVersion, roastPoints, fanPoints, dateCreated, cooldownPoint, onServer, revision, showInLibrary, profileType, tempSensor, dateLastRoasted) VALUES "
                + "('93024A2C76944060994AD466F1D4E392', '', 'IKAWA - Geisha Pourover Inlet', '', '1', '0.0,200.7,46.3,213.0,74.8,220.6,145.8,233.4,184.5,235.3,225.7,237.4,261.8,238.5,293.5,240.5', '0.0,78.82353,293.5,66.66667', '1575373865', '423.4,81.56863', '0', '0', '1', '1', '1', '1494872272');"
            ] {
                do {
                    try dbQueue?.inDatabase { db in
                        try db.execute(query)
                    }
                }
                catch let error {
                    // handle error here
                    print("SQL error: " + query + " \(error.localizedDescription)")
                    return false
                }
        }
        #endif
        
        var query = "UPDATE version SET schemeVersion = 9;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        // Update scheme version
        schemeVersion = 9
        return true
    }
    
    func migrateToV10() -> Bool {
        /*
         * In this version we added an lastEdited column in the profile table
         */
        do {
            try dbQueue?.inDatabase { db in
                try db.execute("ALTER TABLE profile ADD COLUMN dateLastEdited INTEGER DEFAULT 0;")
            }
        }
        catch let error {
            // handle error here
            print("ALTER TABLE version ADD COLUMN dateLastEdited INTEGER DEFAULT 0;: \(error.localizedDescription)")
            return false
        }
        /*
         * In this version we added an archived column in the profile table
         */
        do {
            try dbQueue?.inDatabase { db in
                try db.execute("ALTER TABLE profile ADD COLUMN archived INTEGER DEFAULT 0;")
            }
        }
        catch let error {
            // handle error here
            print("ALTER TABLE version ADD COLUMN archived INTEGER DEFAULT 0;: \(error.localizedDescription)")
            return false
        }
        // Update scheme version
        let query = "UPDATE version SET schemeVersion = 10;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        // Update scheme version
        schemeVersion = 10
        return true
    }
    
    func migrateToV11() -> Bool {
        /*
         * In this version we added an lastEdited column in the profile table
         */
        do {
            try dbQueue?.inDatabase { db in
                try db.execute("ALTER TABLE profile ADD COLUMN favorite INTEGER DEFAULT 0;")
            }
        }
        catch let error {
            // handle error here
            print("ALTER TABLE profile ADD COLUMN favorite INTEGER DEFAULT 0;: \(error.localizedDescription)")
            return false
        }
        
        // Update scheme version
        let query = "UPDATE version SET schemeVersion = 11;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        // Update scheme version
        schemeVersion = 11
        return true
    }
    
    func migrateToV12() -> Bool {
        /*
         * In this version we added an lastEdited column in the profile table
         */
        do {
            try dbQueue?.inDatabase { db in
                try db.execute("ALTER TABLE roastNotes ADD COLUMN dateLastEdited INTEGER DEFAULT 0;")
            }
        }
        catch let error {
            // handle error here
            print("ALTER TABLE roastNotes ADD COLUMN dateLastEdited INTEGER DEFAULT 0;: \(error.localizedDescription)")
            return false
        }
       
        // Update scheme version
        let query = "UPDATE version SET schemeVersion = 12;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        // Update scheme version
        schemeVersion = 12
        return true
    }

    
    func migrateToV13() -> Bool {
        /*
         * In this version we added an lastEdited column in the profile table
         */
        do {
            try dbQueue?.inDatabase { db in
                try db.execute("ALTER TABLE roastNotes ADD COLUMN archived INTEGER DEFAULT 0;")
            }
        }
        catch let error {
            // handle error here
            print("ALTER TABLE roastNotes ADD COLUMN archived INTEGER DEFAULT 0;: \(error.localizedDescription)")
            return false
        }
       
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute("UPDATE profile SET archived = 1, showInLibrary = 1 WHERE showInLibrary = 0;")
            }
        }
        catch let error {
            // handle error here
            print("UPDATE profile SET archived = 1, showInLibrary = 1 WHERE showInLibrary = 0;: \(error.localizedDescription)")
            return false
        }
        
        
        // Update scheme version
        let query = "UPDATE version SET schemeVersion = 13;"
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        // Update scheme version
        schemeVersion = 13
        return true
    }
    
    func sqlencode(str : String) -> String {
        return str.replacingOccurrences(of: "\'", with: "\'\'")
    }
    
    func readCoffees() -> [Dictionary<String, Any>]? {
        var coffeesArrayData = [Dictionary<String, Any>]()
        do {
            try dbQueue?.inDatabase { db in
                let rows = try Row.fetchCursor(db, "SELECT * FROM coffee")
                while let row = try rows.next() {
                    var coffeeData = [String : Any]()
                    coffeeData["coffeeId"] = row["ID"]
                    coffeeData["coffeeName"] = row["coffeeName"]
                    coffeeData["creatorId"] = row["creatorId"]
                    coffeeData["seller"] = row["seller"]
                    coffeeData["webLink"] = row["webLink"]
                    
                    coffeesArrayData.append(coffeeData)
                }
            }
        }
        catch let error {
            // handle error here
            print("Error trying to open DB: \(error.localizedDescription)")
            return nil
        }
        return coffeesArrayData
    }
    
    func readProfiles() -> [Dictionary<String, Any>]? {
        var profilesArrayData = [Dictionary<String, Any>]()
        do {
            try dbQueue?.inDatabase { db in
                let rows = try Row.fetchCursor(db, "SELECT * FROM profile, coffee WHERE profile.coffeeId = coffee.ID")
                while let row = try rows.next() {
                    var profileData = [String : Any]()
                    profileData["UUID"] = row["UUID"]
                    profileData["parentUUID"] = row["parentUUID"]
                    profileData["name"] = row["name"]
                    profileData["schemeVersion"] = row["schemeVersion"]
                    profileData["roastPoints"] = row["roastPoints"]
                    profileData["fanPoints"] = row["fanPoints"]
                    profileData["dateCreated"] = row["dateCreated"]
                    profileData["tempSensorOn"] = row["tempSensorOn"]
                    profileData["cooldownPoint"] = row["cooldownPoint"] as String
                    profileData["onServer"] = row["onServer"]
                    profileData["revision"] = row["revision"]
                    profileData["showInLibrary"] = row["showInLibrary"]
                    profileData["archived"] = row["archived"]
                    profileData["favorite"] = row["favorite"]
                    profileData["profileType"] = row["profileType"]
                    profileData["tempSensor"] = row["tempSensor"]
                    profileData["dateLastRoasted"] = row["dateLastRoasted"]
                    profileData["dateLastEdited"] = row["dateLastEdited"]
                    profileData["cropsterId"] = row["cropsterId"] ?? ""
                    
                    profileData["userId"] = row["userId"]
                    profileData["coffeeName"] = row["coffeeName"]
                    profileData["coffeeId"] = row["coffeeId"]
                    profileData["coffeeWebUrl"] = row["coffeeWebUrl"]
                    profilesArrayData.append(profileData)
                }
            }
        }
        catch let error {
            // handle error here
            print("Error trying to open DB: \(error.localizedDescription)")
            return nil
        }
        
        
        do {
            try dbQueue?.inDatabase { db in
                let rows = try Row.fetchCursor(db, "SELECT * FROM profile WHERE (profile.coffeeId IS NULL OR profile.coffeeId = '')")
                while let row = try rows.next() {
                    var profileData = [String : Any]()
                    profileData["UUID"] = row["UUID"]
                    profileData["parentUUID"] = row["parentUUID"]
                    profileData["name"] = row["name"]
                    profileData["schemeVersion"] = row["schemeVersion"]
                    profileData["roastPoints"] = row["roastPoints"]
                    profileData["fanPoints"] = row["fanPoints"]
                    profileData["dateCreated"] = row["dateCreated"]
                    profileData["tempSensorOn"] = row["tempSensorOn"]
                    profileData["cooldownPoint"] = row["cooldownPoint"] as String
                    profileData["onServer"] = row["onServer"]
                    profileData["revision"] = row["revision"]
                    profileData["showInLibrary"] = row["showInLibrary"]
                    profileData["archived"] = row["archived"]
                    profileData["favorite"] = row["favorite"]
                    profileData["profileType"] = row["profileType"]
                    profileData["tempSensor"] = row["tempSensor"]
                    profileData["dateLastRoasted"] = row["dateLastRoasted"]
                    profileData["dateLastEdited"] = row["dateLastEdited"]
                    profileData["cropsterId"] = row["cropsterId"] ?? ""
                    
                    profileData["userId"] = row["userId"]
                    profileData["coffeeName"] = ""
                    profileData["coffeeId"] = ""
                    profileData["coffeeWebUrl"] = ""
                    profilesArrayData.append(profileData)
                }
            }
        }
        catch let error {
            // handle error here
            print("Error trying to open DB: \(error.localizedDescription)")
            return nil
        }
        
        
        
        return profilesArrayData
    }
    
    
    func addProfile(profile: Profile) -> Bool {
        
        var query = "INSERT INTO profile (UUID, parentUUID, name, schemeVersion, roastPoints, fanPoints, dateCreated, onServer, cooldownPoint, revision, showInLibrary, archived, tempSensor, dateLastRoasted, dateLastEdited, cropsterId, coffeeId, userId, profileType) VALUES ("
        query.append("'" + profile.uuid + "', ")
        if (profile.parentUuid != nil) {
            query.append("'" + profile.parentUuid + "', ")
        } else {
            query.append("'" + "', ")
        }
        
        
        query.append("'" + profile.name + "', ")
        query.append("'\(profile.schemaVersion)', ")
        query.append("'" + profile.tempPointsToString(points: profile.roastPoints as! [RoastTempPoint] ) + "', ")
        query.append("'" + profile.fanPointsToString(points: profile.fanPoints as! [RoastFanPoint] ) + "', ")
        query.append("'\(Int((profile.dateCreated).timeIntervalSince1970))', ")
        query.append("'0',")
        query.append("'\(profile.cooldownPoint.time),\(profile.cooldownPoint.power)', ")
        query.append("\(profile.revision), ")
        query.append("\(profile.showInLibrary ? 1 : 0), ")
        query.append("\(profile.archived ? 1 : 0), ")
        query.append("\(profile.tempSensor.rawValue), ")
        query.append("'\(Int((profile.dateLastRoasted).timeIntervalSince1970))', ")
        query.append("'\(Int((profile.dateLastEdited ?? Date()).timeIntervalSince1970))', ")
        query.append("'" + (profile.cropsterId ?? "") + "', ")
        query.append("'" + (profile.coffeeId ?? "") + "', ")
        query.append("'" + (profile.userId ?? "") + "', ")
        query.append("'\(profile.type.rawValue)')")
        
        
        print("Query: " + query)
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error adding profile: " + query + " \(error.localizedDescription)")
            return false
        }
        print("Succesfully added profile")
        return true
    }
    
    
    func addNewProfile(profile: Profile, parentUUID: String) -> Bool {
        
        profile.parentUuid = parentUUID
        // If profile is already in the database don't add it.
        if ((AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: profile.uuid)) != nil) {
            return false
        }
        
        
        profile.onServer = 0
        profile.favorite = false
        var query = "INSERT INTO profile (UUID, parentUUID, name, schemeVersion, roastPoints, fanPoints, dateCreated, onServer, cooldownPoint, revision, showInLibrary, archived, favorite, tempSensor, dateLastRoasted, dateLastEdited, cropsterId, coffeeId, userId, profileType) VALUES ("
        query.append("'" + profile.uuid + "', ")
        if (profile.parentUuid != nil) {
            query.append("'" + profile.parentUuid + "', ")
        } else {
            query.append("'" + "', ")
        }
        
        
        
        query.append("'" + profile.name + "', ")
        query.append("'\(profile.schemaVersion)', ")
        query.append("'" + profile.tempPointsToString(points: profile.roastPoints as! [RoastTempPoint] ) + "', ")
        query.append("'" + profile.fanPointsToString(points: profile.fanPoints as! [RoastFanPoint] ) + "', ")
        query.append("'\(Int((Date()).timeIntervalSince1970))', ")
        query.append("'0',")
        query.append("'\(profile.cooldownPoint.time),\(profile.cooldownPoint.power)', ")
        query.append("\(profile.revision), ")
        query.append("\(profile.showInLibrary ? 1 : 0), ")
        query.append("\(profile.archived ? 1 : 0), ")
        query.append("\(profile.favorite ? 1 : 0), ")
        query.append("\(profile.tempSensor.rawValue), ")
        query.append("'\(Int((Date(timeIntervalSince1970: 1494872272)).timeIntervalSince1970))', ")
        query.append("'\(Int((Date()).timeIntervalSince1970))', ")
        query.append("'" + (profile.cropsterId ?? "") + "', ")
        query.append("'" + (profile.coffeeId ?? "") + "', ")
        query.append("'" + (profile.userId ?? "") + "', ")
        query.append("'\(profile.type.rawValue)')")
        
        print("Query: " + query)
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error adding profile: " + query + " \(error.localizedDescription)")
            return false
        }
        print("Succesfully added profile")
        
        AppCore.sharedInstance.loggingManager.sendProfile(profile: profile)
        #if TARGET_PRO
        AppCore.sharedInstance.cropsterManager.sendProfile(profile: profile) { (error) in
            
        }
        #endif
        UserDefaults.standard.set(true, forKey: "db_changed")
        UserDefaults.standard.set(true, forKey: "NeedMergeWithAccount")
        UserDefaults.standard.synchronize()
        return true
    }
    
    
    func updateProfile(profile: Profile) -> Bool {
        var query = "UPDATE profile "
        query.append(" SET UUID = '" + profile.uuid + "', ")
        query.append("parentUUID = '" + (profile.parentUuid ?? "") + "', ")
        query.append("name = '" + profile.name + "', ")
        query.append("schemeVersion = \(profile.schemaVersion), ")
        query.append("roastPoints = '" + profile.tempPointsToString(points: profile.roastPoints as! [RoastTempPoint] ) + "', ")
        query.append("fanPoints = '" + profile.fanPointsToString(points: profile.fanPoints as! [RoastFanPoint] ) + "', ")
        query.append("dateCreated = '\(Int((profile.dateCreated ?? Date()).timeIntervalSince1970))', ")
        query.append("onServer = \(profile.onServer), ")
        query.append("cooldownPoint ='\(profile.cooldownPoint.time),\(profile.cooldownPoint.power)', ")
        query.append("revision = \(profile.revision), ")
        query.append("showInLibrary = \(profile.showInLibrary ? 1 : 0), ")
        query.append("archived = \(profile.archived ? 1 : 0), ")
        query.append("favorite = \(profile.favorite ? 1 : 0), ")
        query.append("tempSensor = \(profile.tempSensor.rawValue), ")
        query.append("dateLastRoasted = '\(Int((profile.dateLastRoasted ?? Date()).timeIntervalSince1970))', ")
        query.append("dateLastEdited = '\(Int((profile.dateLastEdited ?? Date()).timeIntervalSince1970))', ")
        query.append("cropsterId = '" + (profile.cropsterId ?? "") + "', ")
        query.append("coffeeId = '" + (profile.coffeeId ?? "") + "', ")
        query.append("userId = '" + (profile.userId ?? "") + "'")
        query.append(" WHERE UUID = '" + profile.uuid + "'")
        
        print("Query: " + query)
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        UserDefaults.standard.set(true, forKey: "db_changed")
        UserDefaults.standard.set(true, forKey: "NeedMergeWithAccount")
        UserDefaults.standard.synchronize()
        return true
    }
    
    
    func addCoffee(coffee: IKCoffee) -> Bool {
        // If coffee is already in the database don't add it.
        if ((AppCore.sharedInstance.profileManager.findCoffeeInListByUuid(uuid: coffee.coffeeId)) != nil) {
            return false
        }
        
        var query = "INSERT INTO coffee (ID, coffeeName, creatorId, seller, webLink) VALUES ("
        query.append("'" + coffee.coffeeId + "', ")
        
        query.append("'" + (coffee.name ?? "") + "', ")
        query.append("'" + (coffee.creatorId ?? "") +  "', ")
        query.append("'" + (coffee.seller ?? "") +  "', ")
        query.append("'" + (coffee.webLink ?? "") + "')")
        
        print("Query: " + query)
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error adding profile: " + query + " \(error.localizedDescription)")
            return false
        }
        print("Succesfully added profile")
        
        UserDefaults.standard.set(true, forKey: "db_changed")
        UserDefaults.standard.set(true, forKey: "NeedMergeWithAccount")
        UserDefaults.standard.synchronize()
        
        NotificationCenter.default.post(name: Notification.Name.ProfileManagerDidChangeList, object: nil)
        return true
    }
    
    func deleteRoastNotes(roastId: String?) -> Bool {
        guard roastId != nil else {
            return false
        }
        
        let query = "DELETE FROM roastNotes WHERE roastId LIKE '" + roastId! + "'";
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        #if TARGET_HOME
        AppCore.sharedInstance.loggingManager.trackEvent(name: "Roast notes - deleted", label: "")
        #endif
        UserDefaults.standard.set(true, forKey: "db_changed")
        UserDefaults.standard.set(true, forKey: "NeedMergeWithAccount")
        UserDefaults.standard.synchronize()
        return true
    }
    
    func deleteAllRoastNotes() -> Bool {
        let query = "DELETE FROM roastNotes";
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        UserDefaults.standard.set(true, forKey: "db_changed")
        UserDefaults.standard.set(true, forKey: "NeedMergeWithAccount")
        UserDefaults.standard.synchronize()
        return true
    }
    
    func deleteAllRoastLogs() -> Bool {
        let query = "DELETE FROM roastLog";
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        UserDefaults.standard.set(true, forKey: "db_changed")
        UserDefaults.standard.set(true, forKey: "NeedMergeWithAccount")
        UserDefaults.standard.synchronize()
        return true
    }
    
    func saveRoastNotes(roastNotesData: [String : Any]?) -> Bool {
        if (roastNotesData == nil) {
            return false
        }
        
        var query = "DELETE FROM roastNotes WHERE roastId LIKE '" + (roastNotesData!["roastId"] as! String) + "'";
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        
        // Insert roast into the database
        query = "INSERT INTO roastNotes (roastId, firstCrackTime, firstCrackTemp, secondCrackTime, secondCrackTemp, colorChangeTime, colorChangeTemp, DTR, coffeeType, notes, photo, extraInfo, coffeeId, dateLastEdited, archived) VALUES ("
        query += "'" + (roastNotesData!["roastId"] as! String) + "', "
        query += "" + String(format: "%f", roastNotesData!["firstCrackTime"] as! Double) + ", "
        query += "" + String(format: "%f", roastNotesData!["firstCrackTemp"] as! Double) + ", "
        query += "" + String(format: "%f", roastNotesData!["secondCrackTime"] as! Double) + ", "
        query += "" + String(format: "%f", roastNotesData!["secondCrackTemp"] as! Double) + ", "
        query += "" + String(format: "%f", roastNotesData!["colorChangeTime"] as! Double) + ", "
        query += "" + String(format: "%f", roastNotesData!["colorChangeTemp"] as! Double) + ", "
        query += "" + String(format: "%f", roastNotesData!["DTR"] as! Double) + ", "
        query += "'" + sqlencode(str: (roastNotesData!["coffeeType"] as! String)) + "', "
        query += "'" + sqlencode(str: (roastNotesData!["notes"] as! String)) + "', "
        query += "'" + sqlencode(str: (roastNotesData!["photo"] as! String)) + "', "
        query += "'" + sqlencode(str: (roastNotesData!["extraInfo"] as! String)) + "', "
        query += "'" + sqlencode(str: (roastNotesData!["coffeeId"] as! String)) + "', "
        query += "" + String(format: "%d", Int(((roastNotesData!["dateLastEdited"] as? Date) ?? AppCore.sharedInstance.settingsManager.internetTime()).timeIntervalSince1970)) + ", "
        query += "" + (((roastNotesData!["archived"] as? Bool) ?? false) ? "1" : "0") + ")"
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        
        UserDefaults.standard.set(true, forKey: "db_changed")
        UserDefaults.standard.set(true, forKey: "NeedMergeWithAccount")
        UserDefaults.standard.synchronize()
        return true;
        
    }
    
    func roastNotes() -> [Dictionary<String, Any>]? {
        let query = "SELECT roastId, firstCrackTime, firstCrackTemp, secondCrackTime, secondCrackTemp, colorChangeTime, colorChangeTemp, DTR, coffeeType, notes, photo, extraInfo, coffeeId, dateLastEdited, archived FROM roastNotes"
        var roastNotesArrayData = [Dictionary<String, Any>]()
        do {
            try dbQueue!.inDatabase { db in
                let rows = try Row.fetchCursor(db, query)
                while let row = try rows.next() {
                    
                    var roastNotesData = [String : Any]()
                    roastNotesData["roastId"] = row["roastId"]
                    roastNotesData["firstCrackTime"] = row["firstCrackTime"]
                    roastNotesData["firstCrackTemp"] = row["firstCrackTemp"]
                    roastNotesData["secondCrackTime"] = row["secondCrackTime"]
                    roastNotesData["secondCrackTemp"] = row["secondCrackTemp"]
                    roastNotesData["colorChangeTime"] = row["colorChangeTime"]
                    roastNotesData["colorChangeTemp"] = row["colorChangeTemp"]
                    roastNotesData["DTR"] = row["DTR"]
                    roastNotesData["coffeeType"] = row["coffeeType"]
                    roastNotesData["notes"] = row["notes"]
                    roastNotesData["photo"] = row["photo"]
                    roastNotesData["extraInfo"] = row["extraInfo"]
                    roastNotesData["coffeeId"] = row["coffeeId"]
                    roastNotesData["dateLastEdited"] = row["dateLastEdited"]
                    roastNotesData["archived"] = row["archived"]
                    roastNotesArrayData.append(roastNotesData)
                }
            }
        }
        catch let error {
            // handle error here
            print("Error trying to open DB: \(error.localizedDescription)")
        }
        
        return roastNotesArrayData
    }
    
    func roastForUuid(uuid: String) -> Dictionary<String, Any>? {
        var roastData = [String : Any]()
        let query = "SELECT UUID, profileUUID, machineUUID, roastDate, roastData FROM roastLog WHERE UUID LIKE '" + uuid + "'";
        do {
            try dbQueue!.inDatabase { db in
                let rows = try Row.fetchCursor(db, query)
                if let row = try rows.next() {
                    roastData["UUID"] = row["UUID"]
                    roastData["profileUUID"] = row["profileUUID"]
                    roastData["machineUUID"] = row["machineUUID"]
                    roastData["roastDate"] = row["roastDate"]
                    roastData["roastData"] = row["roastData"]
                }
            }
        }
        catch let error {
            // handle error here
            print("Error trying to open DB: \(error.localizedDescription)")
        }
        return roastData
    }

    
    func roastNotes(forRoastId roastId: String) -> Dictionary<String, Any>? {
        let query = "SELECT roastId, firstCrackTime, firstCrackTemp, secondCrackTime, secondCrackTemp, colorChangeTime, colorChangeTemp, DTR, coffeeType, notes, photo, extraInfo, coffeeId, dateLastEdited, archived FROM roastNotes WHERE roastId LIKE '" + roastId + "'"
        var roastNotesData = [String : Any]()
        do {
            try dbQueue!.inDatabase { db in
                let rows = try Row.fetchCursor(db, query)
                while let row = try rows.next() {
                    
                    
                    roastNotesData["roastId"] = row["roastId"]
                    roastNotesData["firstCrackTime"] = row["firstCrackTime"]
                    roastNotesData["firstCrackTemp"] = row["firstCrackTemp"]
                    roastNotesData["secondCrackTime"] = row["secondCrackTime"]
                    roastNotesData["secondCrackTemp"] = row["secondCrackTemp"]
                    roastNotesData["colorChangeTime"] = row["colorChangeTime"]
                    roastNotesData["colorChangeTemp"] = row["colorChangeTemp"]
                    roastNotesData["DTR"] = row["DTR"]
                    roastNotesData["coffeeType"] = row["coffeeType"]
                    roastNotesData["notes"] = row["notes"]
                    roastNotesData["photo"] = row["photo"]
                    roastNotesData["extraInfo"] = row["extraInfo"]
                    roastNotesData["coffeeId"] = row["coffeeId"]
                    roastNotesData["dateLastEdited"] = row["dateLastEdited"]
                    roastNotesData["archived"] = row["archived"]
                }
            }
        }
        catch let error {
            // handle error here
            print("Error trying to open DB: \(error.localizedDescription)")
        }
        return roastNotesData
        
    }
    
    
    func saveRoast(roastData: Dictionary<String, Any>?) -> Bool {
        if (roastData == nil) {
            return false
        }
        
        // Delete existing roast with this uuid
        var query = "DELETE FROM roastLog WHERE UUID LIKE '" + (roastData!["UUID"] as! String) + "'"
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        // Insert roast into the database
        query = "INSERT INTO roastLog (UUID, profileUUID, machineUUID, roastDate, roastData) VALUES (";
        query += "'" + (roastData!["UUID"] as! String) + "', ";
        query += "'" + (roastData!["profileUUID"] as! String) + "', ";
        query += "'" + (roastData!["machineUUID"] as! String) + "', ";
        query += "'" + String(Int(roastData!["roastDate"] as! TimeInterval)) + "', ";
        query += "'" + (roastData!["roastData"] as! String) + "') ";
        
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        UserDefaults.standard.set(true, forKey: "db_changed")
        UserDefaults.standard.set(true, forKey: "NeedMergeWithAccount")
        UserDefaults.standard.synchronize()
        return true
    }
    
    
    func markDataOnServer(roastUdid: String) -> Bool {
        let query = "UPDATE roastLog SET dataOnServer = 1 WHERE UUID LIKE '" + roastUdid + "'";
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        
        UserDefaults.standard.set(true, forKey: "db_changed")
        UserDefaults.standard.set(true, forKey: "NeedMergeWithAccount")
        UserDefaults.standard.synchronize()
        return true;
    }
    
    func markCsvOnServer(roastUdid: String) -> Bool {
        let query = "UPDATE roastLog SET csvOnServer = 1 WHERE UUID LIKE '" + roastUdid + "'";
        do {
            try dbQueue?.inDatabase { db in
                try db.execute(query)
            }
        }
        catch let error {
            // handle error here
            print("SQL error: " + query + " \(error.localizedDescription)")
            return false
        }
        
        
        UserDefaults.standard.set(true, forKey: "db_changed")
        UserDefaults.standard.set(true, forKey: "NeedMergeWithAccount")
        UserDefaults.standard.synchronize()
        return true;
    }
    
    
    func roastLogs() -> [Dictionary<String, Any>]? {
        let query = "SELECT UUID, profileUUID, machineUUID, roastDate, roastData, csvOnServer, dataOnServer FROM roastLog WHERE csvOnServer = 0 OR dataOnServer = 0"
        var roastLogsArrayData = [Dictionary<String, Any>]()
        do {
            try dbQueue?.inDatabase { db in
                let rows = try Row.fetchCursor(db, query)
                while let row = try rows.next() {
                    
                    var roastLogData = [String : Any]()
                    roastLogData["UUID"] = row["UUID"] as! String
                    roastLogData["profileUUID"] = row["profileUUID"] as! String
                    roastLogData["machineUUID"] = row["machineUUID"] as! String
                    roastLogData["roastDate"] = TimeInterval(row["roastDate"] as! String)
                    roastLogData["roastData"] = row["roastData"] as! String
                    roastLogData["csvOnServer"] = row["csvOnServer"] as! Int64
                    roastLogData["dataOnServer"] = row["dataOnServer"] as! Int64
                    
                    roastLogsArrayData.append(roastLogData)
                }
            }
        }
        catch let error {
            // handle error here
            print("Error trying to open DB: \(error.localizedDescription)")
        }
        
        return roastLogsArrayData
    }
}
