//
//  ToolsManager.swift
//  IKAWApp
//
//  Created by Admin on 1/21/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Alamofire
import FirebaseStorage
import Toast_Swift
import Alamofire

class ToolsFirebaseManager: NSObject {
    func backupList(userId: String, callback:@escaping ((_ error: Error?, _ backupList: [DataSnapshot])->())) {
        let vc = UIApplication.shared.keyWindow?.rootViewController
                vc!.view.makeToastActivity(.center)
                var db = Database.database().reference()
                db = db.child(IK_FIREBASE_DATABASE)
                var backupsQuery: DatabaseQuery?

                    backupsQuery = db.child("device_backups").queryOrdered(byChild: "user_id").queryEqual(toValue: userId)/*.queryLimited(toLast: 30)*/
                    
                backupsQuery?.observeSingleEvent(of: .value, with: { (snapshot) in
                    vc!.view.hideToastActivity()
                    if (snapshot.childrenCount > 0) {
                        let childrens = snapshot.children.allObjects.sorted(by: { (first , saecond) -> Bool in
                            if let firstValue = (first as! DataSnapshot).value, let secondValue = (saecond as! DataSnapshot).value {
                                let dictFirst = firstValue  as! Dictionary<String, AnyObject>
                                let timestampFirst = dictFirst["timestamp"] as! NSNumber
                                
                                let dictSecond = secondValue  as! Dictionary<String, AnyObject>
                                let timestampSecond = dictSecond["timestamp"] as! NSNumber
                                return timestampFirst.doubleValue < timestampSecond.doubleValue
                            }
                            return true
                        })
                        callback(nil, (childrens as? [DataSnapshot]) ?? [])
                    }
                    else {
                        callback(nil, [])
                    }
                })
    }

    func deleteRecentBackups(userId: String, callback:@escaping ((_ error: Error?)->())) {
        let vc = UIApplication.shared.keyWindow?.rootViewController
        vc!.view.makeToastActivity(.center)
        var db = Database.database().reference()
        db = db.child(IK_FIREBASE_DATABASE)
        var backupsQuery: DatabaseQuery?

            backupsQuery = db.child("recent_device_backups").queryOrdered(byChild: "user_id").queryEqual(toValue: userId)/*.queryLimited(toLast: 30)*/
            
        backupsQuery?.observeSingleEvent(of: .value, with: { (snapshot) in
            vc!.view.hideToastActivity()
            if (snapshot.childrenCount > 0) {
                let childrens = snapshot.children.allObjects.sorted(by: { (first , saecond) -> Bool in
                    if let firstValue = (first as! DataSnapshot).value, let secondValue = (saecond as! DataSnapshot).value {
                        let dictFirst = firstValue  as! Dictionary<String, AnyObject>
                        let timestampFirst = dictFirst["timestamp"] as! NSNumber
                        
                        let dictSecond = secondValue  as! Dictionary<String, AnyObject>
                        let timestampSecond = dictSecond["timestamp"] as! NSNumber
                        return timestampFirst.doubleValue < timestampSecond.doubleValue
                    }
                    return true
                })
                for snapshot in childrens {
                    (snapshot as! DataSnapshot).ref.removeValue()
                }
                callback(nil)
            }
            else {
                callback(nil)
            }
        })
    }
    
    func deleteNewBackups(from backupList: [DataSnapshot], callback: @escaping ((_ error: Error?)->())) {
        if backupList.count > 0 {
            let child = backupList.last as! DataSnapshot
            if let value = child.value {
                let dict = value  as! Dictionary<String, AnyObject>
                let timestamp = dict["timestamp"]
                let date = Date.init(timeIntervalSince1970: (timestamp?.doubleValue)!)
                let dateFormater = DateFormatter()
                if ((timestamp as! NSNumber).intValue % 86400 == 0) {
                    dateFormater.dateFormat = "dd/MM/yyyy"
                } else {
                    dateFormater.dateFormat = "dd/MM/yyyy HH:mm"
                }
                let deviceName = dict["device_type"] as? String
                let dateString = String(format: "%@ (%@)", dateFormater.string(from: date), deviceName ?? "")
                let path = dict["path"] as? String
                restoreBackupAnDeleteIfNeeded(path: path!) { (error, needDelete) in
                    if error != nil {
                        callback(error)
                    } else if needDelete {
                                let confirmDialogView = ButtonsPopupModalView()
                                confirmDialogView.layout = .vertical
                        confirmDialogView.title = "$backup_will_deleted".localized
                                confirmDialogView.subtitle = path! + " " + dateString
                                confirmDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                                    print("removed backup file: " + path!)
                                    child.ref.removeValue()
                                    let storage = Storage.storage()
                                    let storageRef = storage.reference(forURL: IK_FIREBASE_STORAGE)
                                    let spaceRef = storageRef.child(path!)
                                    spaceRef.delete { (error) in
                                        print("error wen delete file: " + path!)
                                    }
                                    var backupList_ = backupList
                                    backupList_.removeLast()
                                    self.deleteNewBackups(from: backupList_) { (error) in
                                        if error != nil {
                                            callback(error)
                                        }
                                    }
                        }
                        confirmDialogView.setUpButton(index: .Second, title: NSLocalizedString("$Cancel", comment: ""), type: .Filled) {
                            callback(nil)
                        }

                        IKNotificationManager.sharedInstance.show(view: confirmDialogView)
                        
                    } else if !needDelete {
                        var backupList_ = backupList
                        backupList_.removeLast()
                        self.deleteNewBackups(from: backupList_) { (error) in
                            if error != nil {
                                callback(error)
                            }
                        }
                    }
                }
            }
            
        } else {
            callback(nil)
        }
    }
    
    func restoreBackupAnDeleteIfNeeded(path: String, callback:@escaping ((_ error: Error?, _ needDelete: Bool)->())) {
        //uploadUserLibrary(roundTimestamp: false)
        let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let dbFilePath = documentDirectoryURL.appendingPathComponent("ikawa.sqlite")
        let archDbFilePath = documentDirectoryURL.appendingPathComponent("tmp.db")
        let tempDbFilePath = documentDirectoryURL.appendingPathComponent("temp_ikawa.sqlite")
        do {
            try FileManager.default.removeItem(at: archDbFilePath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        
        do {
            try FileManager.default.removeItem(at: tempDbFilePath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        
        if (AppCore.sharedInstance.firebaseManager.currentUser == nil) {
            return
        }
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: IK_FIREBASE_STORAGE)
        let spaceRef = storageRef.child(path)
        // Download to the local filesystem
          spaceRef.write(toFile: archDbFilePath) { (url, error) in
              if (error == nil && url != nil) {
                  print("Success downloading library")
                  //                do {
                  //                    try FileManager.default.removeItem(at: dbFilePath)
                  //                }
                  //                catch let error as NSError {
                  //                    print("Ooops! Something went wrong: \(error)")
                  //                }
                  //
                  var zipArchive: ZZArchive?
                  do {
                      zipArchive = try ZZArchive.init(url: archDbFilePath)
                  }
                  catch let error as NSError {
                      print("Failed to open library zip: \(error)")
                      callback(error, false)
                      return;
                  }
                  
                  let firstArchiveEntry = zipArchive?.entries[0]
                  
                  var dbData: Data?
                  do {
                      dbData = try firstArchiveEntry?.newData()
                  }
                  catch let error as NSError {
                      print("Failed to uncompress library zip: \(error)")
                      callback(error, false)
                      return;
                  }
                  do {
                      try dbData?.write(to: tempDbFilePath/*dbFilePath*/)
                  }
                  catch let error as NSError {
                      print("Failed to uncompress library zip: \(error)")
                      callback(error, false)
                      return;
                  }
                      
                      
                  let tempSQLiteManager = IKSqliteManager(dbName: "temp_ikawa.sqlite", needMigrateDB: false)
                  if tempSQLiteManager.schemeVersion > 7 {
                      callback(nil, true)
                  } else {
                      callback(nil, false)
                  }
                      
                      

                  
              } else {
                  callback(error, false)
              }
          }
        
    }

    func deleteBackupsAfterAppUpdate(userId: String) {
        backupList(userId: userId) { (error, snapshots) in
            if (snapshots.count > 0) {
                let confirmationDialogView = ButtonsPopupModalView()
                confirmationDialogView.layout = .horisontal
                confirmationDialogView.title = NSLocalizedString("Do you want to remove new backups?", comment: "")
                confirmationDialogView.subtitle = ""
                
                confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$yes", comment: ""), type: .Bordered) {
                    self.deleteNewBackups(from: snapshots) { (error) in
                        if error == nil {
                            let confirmationDialogView = ButtonsPopupModalView()
                            confirmationDialogView.layout = .horisontal
                            confirmationDialogView.title = NSLocalizedString("Success", comment: "")
                            confirmationDialogView.subtitle = ""
                            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                            }
                            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                        } else {
                            let confirmationDialogView = ButtonsPopupModalView()
                            confirmationDialogView.layout = .horisontal
                            confirmationDialogView.title = NSLocalizedString("Error", comment: "")
                            confirmationDialogView.subtitle = error?.localizedDescription ?? "Unknown error"
                            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                            }
                            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                        }
                    }
                }
                confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$no", comment: ""), type: .Cancel) {}
                
                IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                
            } else {
                let confirmationDialogView = ButtonsPopupModalView()
                confirmationDialogView.layout = .horisontal
                confirmationDialogView.title = NSLocalizedString("$no_backups", comment: "")
                confirmationDialogView.subtitle = ""
                confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                }
                IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            }
        }
    }
    
    func restoreBackup(userId: String) {
        backupList(userId: userId) { (error, snapshots) in
            if (snapshots.count > 0) {
                let confirmationDialogView = ButtonsPopupModalView()
                confirmationDialogView.layout = .horisontal
                confirmationDialogView.title = NSLocalizedString("$restore_alert_title", comment: "")
                confirmationDialogView.subtitle = NSLocalizedString("$restore_alert_message", comment: "")
                
                confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$yes", comment: ""), type: .Bordered) {
                    let alertSheet = UIAlertController(title: NSLocalizedString("$restore_sheet_title", comment: ""), message: NSLocalizedString("$restore_alert_message", comment: ""), preferredStyle: UIAlertController.Style.actionSheet)

                    for i in 0...(snapshots.count - 1) {
                        let child = snapshots[snapshots.count - i - 1] as! DataSnapshot
                        if let value = child.value {
                            let dict = value  as! Dictionary<String, AnyObject>
                            let timestamp = dict["timestamp"]
                            let deviceName = dict["device_type"] as? String
                            let path = dict["path"] as? String
                            let date = Date.init(timeIntervalSince1970: (timestamp?.doubleValue)!)
                            let dateFormater = DateFormatter()
                            if ((timestamp as! NSNumber).intValue % 86400 == 0) {
                                dateFormater.dateFormat = "dd/MM/yyyy"
                            } else {
                                dateFormater.dateFormat = "dd/MM/yyyy HH:mm"
                            }
                            let dateString = String(format: "%@ (%@)", dateFormater.string(from: date), deviceName ?? "")
                            print(dateString + " " + path!)
                            alertSheet.addAction(UIAlertAction(title: dateString, style: .default, handler: { action in
                                self.restoreBackup(path: path!, needMergingWithLocalBackup: false, callback: { (error) in
                                    if let error = error {
                                        let errorDialogView = ButtonsPopupModalView()
                                        errorDialogView.layout = .vertical
                                        errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                                        errorDialogView.subtitle = error.localizedDescription
                                        errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                                        
                                        IKNotificationManager.sharedInstance.show(view: errorDialogView)
                                    }
                                })
                            }))
                        }
                        
                    }
                    alertSheet.addAction(UIAlertAction(title: NSLocalizedString("$no", comment: ""), style: .cancel, handler: { action in
                        
                    }))
                    if let vc = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController {
                        if let popoverPresentationController = alertSheet.popoverPresentationController {
                            popoverPresentationController.sourceView = vc.view
                            popoverPresentationController.sourceRect = CGRect(x: vc.view.bounds.size.width / 2.0, y: vc.view.bounds.size.height / 2.0, width: 1, height: 1)
                            popoverPresentationController.permittedArrowDirections = []
                        }
                        
                        vc.present(alertSheet, animated: true, completion: {})
                    }
                }
                confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$no", comment: ""), type: .Cancel) {}
                
                IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                
            } else {
                let confirmationDialogView = ButtonsPopupModalView()
                confirmationDialogView.layout = .horisontal
                confirmationDialogView.title = NSLocalizedString("$no_backups", comment: "")
                confirmationDialogView.subtitle = ""
                confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                }
                IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            }
        }
    }
    
    func restoreBackup(path: String, needMergingWithLocalBackup: Bool, callback:@escaping ((_ error: Error?)->())) {
        //uploadUserLibrary(roundTimestamp: false)
        let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let dbFilePath = documentDirectoryURL.appendingPathComponent("ikawa.sqlite")
        let archDbFilePath = documentDirectoryURL.appendingPathComponent("tmp.db")
        let tempDbFilePath = documentDirectoryURL.appendingPathComponent("temp_ikawa.sqlite")
        do {
            try FileManager.default.removeItem(at: archDbFilePath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        
        do {
            try FileManager.default.removeItem(at: tempDbFilePath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        
        if (AppCore.sharedInstance.firebaseManager.currentUser == nil) {
            return
        }
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: IK_FIREBASE_STORAGE)
        let spaceRef = storageRef.child(path)
        // Download to the local filesystem
        spaceRef.write(toFile: archDbFilePath) { (url, error) in
            if (error == nil && url != nil) {
                print("Success downloading library")
                //                do {
                //                    try FileManager.default.removeItem(at: dbFilePath)
                //                }
                //                catch let error as NSError {
                //                    print("Ooops! Something went wrong: \(error)")
                //                }
                //
                var zipArchive: ZZArchive?
                do {
                    zipArchive = try ZZArchive.init(url: archDbFilePath)
                }
                catch let error as NSError {
                    print("Failed to open library zip: \(error)")
                    callback(error)
                    return;
                }
                
                let firstArchiveEntry = zipArchive?.entries[0]
                
                var dbData: Data?
                do {
                    dbData = try firstArchiveEntry?.newData()
                }
                catch let error as NSError {
                    print("Failed to uncompress library zip: \(error)")
                    callback(error)
                    return;
                }
                do {
                    try dbData?.write(to: tempDbFilePath/*dbFilePath*/)
                }
                catch let error as NSError {
                    print("Failed to uncompress library zip: \(error)")
                    callback(error)
                    return;
                }
                return


                    
                    
                    
                    
                    _ = AppCore.sharedInstance.sqliteManager.removeDB() //removing local DB
                    let fileManager = FileManager.default
                    do {
                        try fileManager.copyItem(at: tempDbFilePath, to: dbFilePath)
                    } catch {
                        print("Unable to copy file: \(tempDbFilePath)")
                        callback(nil)
                        return
                    }
                    AppCore.sharedInstance.profileManager.reloadData()
                    #if TARGET_PRO
                    AppCore.sharedInstance.roastNotesManager.updateAllRoastNotes()
                    #endif
                    callback(nil)

            } else {
                callback(error)
            }
        }
    }

}



