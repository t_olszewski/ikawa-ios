//
//  IKProfileManager.swift
//  IKAWApp
//
//  Created by Admin on 3/16/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import Firebase
import Toast_Swift
import zipzap
import IKRoasterLib
import FirebaseDatabase
import FirebaseStorage
import Alamofire


enum Sort: Int
{
    case NAME_ASC
    case NAME_DESC
    case DATE_CREATED_ASC
    case DATE_CREATED_DESC
    case DATE_LAST_ROASTED_ASC
    case DATE_LAST_ROASTED_DESC
    case DATE_LAST_USED_ASC
    case DATE_LAST_USED_DESC
    case DATE_LAST_EDITED_DESC
}

enum SaveState: Int
{
    case FAILED
    case CANCELED
    case OVERWRITTEN
    case SAVED
    case SAVED_AS
    case ALREADY_EXISTS
    case UNSUPPORTED
}

class IKProfileManager: NSObject {
    
    typealias Callback = (_ success: Bool) -> Void
    typealias SaveProfileCallback = (_ state: SaveState, _ profile: Profile?) -> Void
    
    var profilesInLibrary : [Profile]?
    var profiles : [Profile]?
    var coffees : [IKCoffee]?
    var sortType: Sort = .DATE_LAST_USED_DESC {
        didSet {
            reloadData()
        }
    }
    
    private let sqliteManager: IKSqliteManager
    
    required init(sqliteManager: IKSqliteManager) {
        self.sqliteManager = sqliteManager
        super.init()
        self.reloadData()
    }
    
    
    func parseProfileDataArray(profileDataArray: [Dictionary<String, Any>]) -> [Profile]? {
        var profiles = [Profile]()
        for profileData in profileDataArray {
            let profile = Profile()
            profile.uuid = profileData["UUID"] as? String
            profile.parentUuid = profileData["parentUUID"] as? String
            profile.name = profileData["name"] as? String
            profile.schemaVersion = Int32((profileData["schemeVersion"] as? String) ?? "0")!
            // roastPoints
            let roastPointsString = profileData["roastPoints"] as! String
            profile.roastPoints = self.tempPointsFromString(string: roastPointsString)
            // fanPoints
            let fanPointsString = profileData["fanPoints"] as! String
            profile.fanPoints = self.fanPointsFromString(string: fanPointsString)
            let dateCreatedTimeinterval = TimeInterval(profileData["dateCreated"] as! Int64)
            profile.dateCreated = Date(timeIntervalSince1970: dateCreatedTimeinterval)
            profile.revision = Int32(profileData["revision"] as! Int64)
            profile.onServer = Int32(profileData["onServer"] as! Int64)
            if let cooldownPointString = profileData["cooldownPoint"] as? String {
                if let cooldownPoints = self.fanPointsFromString(string: cooldownPointString) {
                    if let cooldownPoint = cooldownPoints.first as? RoastFanPoint {
                        profile.cooldownPoint = cooldownPoint
                    }
                    
                }
            }
            profile.revision = Int32(profileData["revision"] as! Int64)
            profile.showInLibrary = (Int32(profileData["showInLibrary"] as? Int64 ?? 0)) > 0 ? true : false
            profile.archived = (Int32(profileData["archived"] as? Int64 ?? 0)) > 0 ? true : false
            profile.favorite = (Int32(profileData["favorite"] as? Int64 ?? 0)) > 0 ? true : false
            profile.type = ProfileType(rawValue: Int(profileData["profileType"] as! Int64))
            profile.tempSensor = TempSensor(rawValue: UInt32(profileData["tempSensor"] as! Int64))
            
            let dateLastRoastedTimeinterval = TimeInterval(profileData["dateLastRoasted"] as! Int64)
            profile.dateLastRoasted = Date(timeIntervalSince1970: dateLastRoastedTimeinterval)
            let dateLastEditedTimeinterval = TimeInterval(profileData["dateLastEdited"] as? Int64 ?? 0)
            profile.dateLastEdited = Date(timeIntervalSince1970: dateLastEditedTimeinterval)
            
            if let cropsterId = profileData["cropsterId"] {
                profile.cropsterId = cropsterId as? String
            }
            profile.userId = profileData["userId"] as? String
            profile.coffeeId = profileData["coffeeId"] as? String
            profile.coffeeName = profileData["coffeeName"] as? String
            profile.coffeeWebUrl = profileData["coffeeWebUrl"] as? String
            profiles.append(profile)
            
        }
        
        
        return profiles
    }
    
    func parseCoffeeDataArray(coffeeDataArray: [Dictionary<String, Any>]) -> [IKCoffee]? {
        var coffees = [IKCoffee]()
        for coffeeData in coffeeDataArray {
            let coffee = IKCoffee()
            coffee.coffeeId = coffeeData["coffeeId"] as? String
            coffee.name = coffeeData["coffeeName"] as? String
            coffee.creatorId = coffeeData["creatorId"] as? String
            coffee.seller = coffeeData["seller"] as? String
            coffee.webLink = coffeeData["webLink"] as? String
            coffees.append(coffee)
        }
        return coffees
    }
    
    func tempPointsFromString(string: String) -> [Any]! {
        var resultArray = [Any]()
        
        let stringArray = string.components(separatedBy: ",")
        var time :Float!
        var temp :Float!
        var index = 0
        for item in stringArray {
            if (index % 2 == 0) {
                time = Float(item)
            } else {
                temp = Float(item)
                let tp = RoastTempPoint()
                tp.time = time
                tp.temperature = temp
                resultArray.append(tp)
            }
            index = index + 1
            
        }
        
        return resultArray
    }
    
    
    
    func fanPointsFromString(string: String) -> [Any]! {
        var resultArray = [Any]()
        
        let stringArray = string.components(separatedBy: ",")
        var time :Float!
        var power :Float!
        var index = 0
        for item in stringArray {
            if (index % 2 == 0) {
                time = Float(item)
            } else {
                power = Float(item)
                let tp = RoastFanPoint()
                tp.time = time
                tp.power = power
                resultArray.append(tp)
            }
            index = index + 1
        }
        
        return resultArray
    }
    
    
    
    
    
    func reloadData() {
        if let _ = self.sqliteManager.openDb(fileName: "ikawa.sqlite") {
            if let profilesDataArray = self.sqliteManager.readProfiles() {
                //print(profilesDataArray)
                self.profiles = self.parseProfileDataArray(profileDataArray: profilesDataArray)
                self.profilesInLibrary = self.profiles?.filter({ (profile) -> Bool in
                    return profile.showInLibrary
                })
            }
            
            if let coffeesDataArray = self.sqliteManager.readCoffees() {
                //print(coffeesDataArray)
                self.coffees = self.parseCoffeeDataArray(coffeeDataArray: coffeesDataArray)
            }
        }
        sort()
        if let roasterProfile = RoasterManager.instance()?.profile, let updatedProfile = findProfileInListByUuid(uuid: roasterProfile.uuid) {
             RoasterManager.instance()?.profile = updatedProfile
        }
        NotificationCenter.default.post(name: Notification.Name.ProfileManagerDidChangeList, object: nil)
    }
    
    func getRandomUuid() -> String {
        return UUID().uuidString.replacingOccurrences(of: "-", with: "")
    }
    
    //MARK: Profile saving
    func showSavingProfileAlert(profile: Profile, callback: @escaping SaveProfileCallback) {
        let confirmationDialogView = ButtonsPopupModalView()
        confirmationDialogView.layout = .vertical
        confirmationDialogView.title = "$save_roast_profile_title".localized
        confirmationDialogView.subtitle = "$save_roast_profile_subtitle".localized
        let profileUuidExists = self.findProfileInListByUuid(uuid: profile.uuid)
        let profileNameExists = self.profileForNameAndCoffeeName(name: profile.name, coffeeName: profile.coffeeName ?? "")
        
        confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$save_new", comment: "").uppercased(), type: (profileNameExists?.isReadOnly() ?? true) ? .Bordered : .Filled) {
            #if TARGET_PRO
            if profile.isRoasterProfile() {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_EDIT_SAVEASNEW", label: "")
            } else {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_GRAPH_SAVEASNEW", label: "")
            }
            #endif
            
            self.showSaveProfileAsCopyAlert(profile: profile, title: nil, callback: callback)
        }
        
        confirmationDialogView.setUpButton(index: .Second, title: (profileNameExists?.isReadOnly() ?? true) ? "" : NSLocalizedString("$overwrite", comment: "").uppercased(), type: .Bordered) {
            #if TARGET_PRO
            if profile.isRoasterProfile() {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_EDIT_OVERWRITE", label: "")
            } else {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_GRAPH_OVERWRITE", label: "")
            }
            #endif
            if profileUuidExists != nil {
                profileUuidExists?.showInLibrary = true
                _ = self.sqliteManager.updateProfile(profile: profileUuidExists!)
                callback(.OVERWRITTEN, self.findProfileInListByUuid(uuid: profileUuidExists!.uuid))
            } else
    //            if profileNameExists != nil {
                if let oldProfile = profile.parentUuid == nil ? profileNameExists : self.findProfileInListByUuid(uuid: profile.parentUuid) {
                        if (AppCore.sharedInstance.profileManager.overwriteProfile(oldProfile: oldProfile, newProfile: profile)) {
                            callback(.OVERWRITTEN, self.findProfileInListByUuid(uuid: profile.uuid))
                        } else {
                            callback(.CANCELED, nil)
                        }
                    } else {
                        callback(.CANCELED, nil)
                    }
                    
//                } else {
//                    let savedProfile = profile.copy() as! Profile
//                    savedProfile.uuid = self.getRandomUuid()
//                    savedProfile.type = ProfileTypeUser
//                    if (AppCore.sharedInstance.profileManager.overwriteProfile(oldProfile: profile, newProfile: savedProfile)) {
//                        callback(.OVERWRITTEN, self.findProfileInListByUuid(uuid: savedProfile.uuid))
//                    } else {
//                        callback(.CANCELED, nil)
//                    }
//            }
        }
        
        
        if !profile.isRoasterProfile() {
            confirmationDialogView.setUpButton(index: .Third, title: NSLocalizedString("$cancel_up", comment: ""), type: .Cancel) {}
        }

        
        IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
        
    }
    
    func showSaveProfileAsCopyAlert(profile: Profile, title: String?, callback: @escaping SaveProfileCallback) {
        let createNewRecipeDialogView = CreateRecipeModalView()
        createNewRecipeDialogView.recipeNameTextField.text = profile.name
        if let title = title {
            createNewRecipeDialogView.titleLabel.text = title
        }

        createNewRecipeDialogView.saveButtonCallback = {(recipeName) in
            profile.uuid = AppCore.sharedInstance.profileManager.getRandomUuid()
            profile.type = ProfileTypeUser
            profile.name = recipeName
            //profile.coffeeName = (savingAlert.textFields![1] as UITextField).text
            profile.revision = 0
            if (profile.name.count < IK_PROFILE_NAME_MIN_CHAR) {
                var message: String
                if (IK_PROFILE_NAME_MIN_CHAR == 1) {
                    message = NSLocalizedString("$invalid_name_message_1", comment: "")
                } else {
                    message = NSLocalizedString("$invalid_name_message_n", comment: "")
                }
                
                let confirmationDialogView = ButtonsPopupModalView()
                confirmationDialogView.layout = .vertical
                confirmationDialogView.title = message
                confirmationDialogView.subtitle = ""
                confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                
                IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                
            }
            if let existingProfile =
                AppCore.sharedInstance.profileManager.profileForName(name: profile.name, lowercased: true), existingProfile.showInLibrary {
                let formatStr = NSLocalizedString("$profile_name_exists", comment: "").replacingOccurrences(of: "%s", with: "%@")
                self.showSaveProfileAsCopyAlert(profile: profile, title: String(format: formatStr, recipeName), callback: { (state, profile) in
                    callback(state, profile)
                })
            } else {
                if(self.sqliteManager.addNewProfile(profile: profile, parentUUID: profile.parentUuid ?? ""))
                {
                    self.reloadData()
                    callback(.SAVED_AS, self.findProfileInListByUuid(uuid: profile.uuid))
                    
                }
                else
                {
                    callback(.FAILED, nil)
                }
            }
            
        }
        
        IKNotificationManager.sharedInstance.show(view: createNewRecipeDialogView)
    }
    
    func saveProfile(profile: Profile, existsText: String, callback: @escaping SaveProfileCallback) {
        /* First check if this profile is allowed. HOME app only with Below sensor */
        #if TARGET_HOME
        if (profile.tempSensor != TempSensorBelow) {
            callback(.UNSUPPORTED, nil)
            return
        }
        #else
        
        if (!AppCore.sharedInstance.settingsManager.supportsTwoSensors() && profile.tempSensor == TempSensorBelow && profile.isRoasterProfile()) {
            callback(.UNSUPPORTED, nil)
            return
        }
        #endif
        /* Set the temp sensor to robust if using a robust sensor */
        if (UserDefaults.standard.integer(forKey: "aboveSensorTypeId") == AboveSensorTypeRobust.rawValue && profile.tempSensor == TempSensorAbove) {
            profile.tempSensor = TempSensorAboveRobust
        }
        
        /* IKAWA profile saving logic:
         
         Saving happens in the following occasions, user experience should be consistent:
         - When importing a shared profile via a URL
         - When connecting to a roaster
         - When editing a profile in your library
         - When editing the profile on the roaster
         
         Important: profiles can never be changed! When changing a profile, a new uuid is generated. So if two profiles have the same uuid, we know they are the same.
         
         All dialogs should have a cancel button to interrupt the saving procedure.
         
         Logic:
         
         if uuid and (name+coffeeName) don't exist, just save
         if uuid is standard profile, save as copy
         if uuid exists, don't do anything
         if (name+coffeeName) exist, ask to overwrite or save as copy
         
         on save as copy, create new profile uuid and enter name+coffeeName
         if enter name+coffeeName and name+coffeeName exists, ask for a different name
         
         on overwrite, replace the profile with new one. */
        
        let profileUuidExists = findProfileInListByUuid(uuid: profile.uuid)
        let profileForNameAndCoffeeName = self.profileForNameAndCoffeeName(name: profile.name, coffeeName: profile.coffeeName ?? "")
        
        
        if !((profile.coffeeId ?? "").isEmpty) {
            if AppCore.sharedInstance.profileManager.findCoffeeInListByUuid(uuid: profile.coffeeId) != nil {
            } else {
                let coffee = IKCoffee()
                coffee.name = profile.coffeeName
                coffee.coffeeId = profile.coffeeId
                coffee.creatorId = profile.userId
                coffee.webLink = profile.coffeeWebUrl
                
                if self.sqliteManager.addCoffee(coffee: coffee) {
                    profile.coffeeName = coffee.name
                    profile.coffeeId = coffee.coffeeId
                    profile.coffeeWebUrl = coffee.webLink
                } else {
                    return
                }
                
            }
        }
        
        
        if (profileUuidExists == nil && profileForNameAndCoffeeName == nil) {
            if(self.sqliteManager.addNewProfile(profile: profile, parentUUID: profile.parentUuid ?? ""))
            {
                reloadData()
                callback(.SAVED, findProfileInListByUuid(uuid: profile.uuid))
                self.reloadData()
            }
            else
            {
                callback(.FAILED, nil)
            }
        }
        else if (profileUuidExists != nil && profileForNameAndCoffeeName == nil) || (profileUuidExists != nil && (profileUuidExists?.showInLibrary ?? false)) { // if uuid exists and not deleted, don't do anything
            callback(.ALREADY_EXISTS, findProfileInListByUuid(uuid: profile.uuid))
        }
        else if (profile.isReadOnly()) { // if uuid is standard profile, save as copy
            showSaveProfileAsCopyAlert(profile: profile, title: nil, callback: callback)
        }
        else if (profileForNameAndCoffeeName != nil || profileUuidExists != nil) {
            showSavingProfileAlert(profile: profile, callback: callback)
        }
        
    }
    
//    func overwriteProfileWithName(name: String, newProfile: Profile) -> Bool {
//        if let oldProfile = profileForName(name: name) {
//            return overwriteProfile(oldProfile: oldProfile, newProfile: newProfile)
//        }
//        return false
//
//    }
    
    func overwriteProfile(oldProfile: Profile, newProfile: Profile) -> Bool {
        if let profileMaxRevision = profileForName(name: oldProfile.name) {
            newProfile.revision = profileMaxRevision.revision + 1
            _ = archiveProfile(profile: oldProfile)
            let result = self.sqliteManager.addNewProfile(profile: newProfile, parentUUID: oldProfile.uuid ?? "")
            reloadData()
            return result
        }

        return false
    }
    
    func removeProfile(profile: Profile) -> Bool {
        // only hide it
        profile.showInLibrary = false
        profile.archived = false
        profile.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
        let result = self.sqliteManager.updateProfile(profile: profile)
        reloadData()
        return result
    }
    
    func archiveProfile(profile: Profile) -> Bool {
        profile.showInLibrary = true
        profile.archived = true
        profile.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
        let result = sqliteManager.updateProfile(profile: profile)
        reloadData()
        return result
    }
    
    func restoreProfile(profile: Profile) -> Bool {
        // only hide it
        profile.showInLibrary = true
        profile.archived = false
        profile.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
        let result = self.sqliteManager.updateProfile(profile: profile)
        reloadData()
        return result
    }
    
    func favorProfile(isFavorite: Bool, profile: Profile) -> Bool {
        profile.favorite = isFavorite
        profile.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
        let result = self.sqliteManager.updateProfile(profile: profile)
        #if TARGET_PRO
        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LIBRARY_FAVOURITE", label: "")
        #endif
        reloadData()
        return result
    }
    
    func profileForName(name: String, lowercased: Bool = false) -> Profile? {
        //return the profile with the highest revision.
        var foundP: Profile?
        if let profileList = profiles {
            for p in profileList {
                if ( (lowercased ? p.name.lowercased() : p.name) == ( lowercased ? name.lowercased() : name) && (p.showInLibrary || p.archived)) {
                    if (foundP == nil || p.revision > foundP!.revision) {
                        foundP = p
                    }
                }
            }
        }
        return foundP
    }
    
    func profileForNameAndCoffeeName(name: String, coffeeName: String) -> Profile? {
        //return the profile with the highest revision.
        var foundP: Profile?
        if let profileList = profiles {
            for p in profileList {
                if (p.name == name && p.coffeeName == coffeeName && p.showInLibrary) {
                    if (foundP == nil || p.revision > foundP!.revision) {
                        foundP = p
                    }
                }
            }
        }
        return foundP
    }
    
    func findProfileInListByUuid(uuid: String) -> Profile? {
        if let profiles =  profiles?.filter({$0.uuid == uuid}), profiles.count > 0 {
            return profiles.first
        }
        return nil
    }
    
    func sort() {
        switch sortType {
        case .NAME_ASC:
            profilesInLibrary?.sort(by: { (profile1, profile2) -> Bool in
                return profile1.name > profile2.name
            })
            break
        case .NAME_DESC:
            profilesInLibrary?.sort(by: { (profile1, profile2) -> Bool in
                return profile1.name < profile2.name
            })
            break
        case .DATE_CREATED_ASC:
            profilesInLibrary?.sort(by: { (profile1, profile2) -> Bool in
                return profile1.dateLastEdited < profile2.dateLastEdited
            })
            break
        case .DATE_CREATED_DESC:
            profilesInLibrary?.sort(by: { (profile1, profile2) -> Bool in
                return profile1.dateLastEdited > profile2.dateLastEdited
            })
            break
        case .DATE_LAST_ROASTED_ASC:
            profilesInLibrary?.sort(by: { (profile1, profile2) -> Bool in
                return profile1.dateLastRoasted < profile2.dateLastRoasted
            })
            break
        case .DATE_LAST_ROASTED_DESC:
            profilesInLibrary?.sort(by: { (profile1, profile2) -> Bool in
                return profile1.dateLastRoasted > profile2.dateLastRoasted
            })
            break
        case .DATE_LAST_USED_DESC:
            profilesInLibrary?.sort(by: { (profile1, profile2) -> Bool in
                return profile1.dateLastUsed() > profile2.dateLastUsed()
            })
            break
        case .DATE_LAST_USED_ASC:
            profilesInLibrary?.sort(by: { (profile1, profile2) -> Bool in
                return profile1.dateLastUsed() < profile2.dateLastUsed()
            })
            break
        case .DATE_LAST_EDITED_DESC:
            profilesInLibrary?.sort(by: { (profile1, profile2) -> Bool in
                return profile1.dateLastEdited > profile2.dateLastEdited
            })
            break
        }  
    }
    
    func shareProfile(profile: Profile) {
        //    profile.uuid = "DDCBB1B6F1EB4C7B9613294911111111"
        //    profile.name = "Serg recomended"
        guard let hex = base64EncodeProfile(profile: profile) else {
            return
        }
        
        let profileUuid = profile.uuid
        AppCore.sharedInstance.firebaseManager.uploadProfileIcon(uuid: profileUuid!)
        let profileUrl = IK_PROFILE_BASEURL + hex
        #if TARGET_PRO
        let htmlFile = Bundle.main.path(forResource: "profile_email_pro", ofType: "html")
        #else
        let htmlFile = Bundle.main.path(forResource: "profile_email", ofType: "html")
        #endif
        var htmlMail = (try? String(contentsOfFile: htmlFile!))!
        var iconUrl = IK_PROFILE_IMAGE_URL
        iconUrl = iconUrl.replacingOccurrences(of: "{{ENV}}", with: IK_FIREBASE_DATABASE)
        iconUrl = iconUrl.replacingOccurrences(of: "{{UUID}}", with: profileUuid!)
        
        htmlMail = htmlMail.replacingOccurrences(of: "{{profile_icon_url}}", with: iconUrl)
        htmlMail = htmlMail.replacingOccurrences(of: "{{profile_url}}", with: profileUrl)
        htmlMail = htmlMail.replacingOccurrences(of: "{{profile_uuid}}", with: profileUuid!)
        htmlMail = htmlMail.replacingOccurrences(of: "{{profile_name}}", with: profile.name!)
        
        let shareText = String(format: NSLocalizedString("$share_twitter_pro_profile", comment: ""), profile.name)
        let provider = IKActivityItemProvider(placeholderItem: shareText)
        provider.mailString = htmlMail
        provider.profileUrl = profileUrl
        provider.mailSubject = String(format: NSLocalizedString("$share_email_subject_pro_profile", comment: ""), profile.name)
        
        let excludeActivities = [UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.print, UIActivity.ActivityType.addToReadingList/*, UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook*/]
        let activityController = UIActivityViewController(activityItems: [provider], applicationActivities: nil)
        activityController.excludedActivityTypes = excludeActivities
        
        func completionHandler(activityType: UIActivity.ActivityType?, shared: Bool, items: [Any]?, error: Error?) {
            if (shared) {
                switch activityType {
                case UIActivity.ActivityType.mail:
                    print("Shared Profile: Mail")
                    AppCore.sharedInstance.loggingManager.trackUIAction(action: "Shared Profile", label: "Mail")
                case UIActivity.ActivityType.postToFacebook:
                    print("Shared Profile: Facebook")
                    AppCore.sharedInstance.loggingManager.trackUIAction(action: "Shared Profile", label: "Facebook")
                case UIActivity.ActivityType.postToTwitter:
                    print("Shared Profile: Twitter")
                    AppCore.sharedInstance.loggingManager.trackUIAction(action: "Shared Profile", label: "Twitter")
                case UIActivity.ActivityType.message:
                    print("Shared Profile: Message")
                    AppCore.sharedInstance.loggingManager.trackUIAction(action: "Shared Profile", label: "Message")
                case UIActivity.ActivityType.copyToPasteboard:
                    print("Shared Profile: Copy")
                    AppCore.sharedInstance.loggingManager.trackUIAction(action: "Shared Profile", label: "Copy")
                case .none:
                    break
                case .some(_):
                    break
                }
                
            }
            else {
                print("Bad user canceled sharing :(")
            }
        }
        
        activityController.completionWithItemsHandler = completionHandler
        let vc = UIApplication.shared.keyWindow?.rootViewController
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
            activityController.popoverPresentationController?.sourceView = vc?.view
            activityController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            activityController.popoverPresentationController?.sourceRect = CGRect(x: 648, y: 984, width: 50, height: 50)
            
        }
        vc?.present(activityController, animated: true, completion: {
            
        })
    }
    
    func base64EncodeProfile(profile: Profile) -> String? {
        guard let profileData = profile.convertToProtobuf() else {
            return nil
        }
        return profileData.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }
    
    func base64DecodeProfile(profile: String) -> Profile? {
        guard let data = Data(base64Encoded: profile) else { return nil }
        print("Profile %@", profile)
        return Profile.convert(fromProtobuf: data)
    }
    
    //MARK: - coffee
    func coffeeForName(name: String) -> IKCoffee? {
        if let filteredArray = self.coffees?.filter({$0.name == name}) {
            return filteredArray.first
        }
        return nil
    }
    
    func findCoffeeInListByUuid(uuid: String) -> IKCoffee? {
        if let coffees =  coffees?.filter({$0.coffeeId == uuid}) {
            return coffees.first
        }
        return nil
    }
    
}
