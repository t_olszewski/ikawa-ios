//
//  IKRoast.swift
//  IKAWApp
//
//  Created by Admin on 4/2/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import IKRoasterLib

class IKRoast: NSObject {

    private var storedTurningPoint: CGPoint?
    private var storedLastRoastingPoint: RoastTempPoint?
    var roaster: Roaster?
    var machineUuid: String?
    var profileUuid: String?
    var roastData: String?
    var uuid: String?
    var csvOnServer: Bool?
    var dataOnServer: Bool?
    var firstCrack: CGPoint?
    var secondCrack: CGPoint?
    var colorChange: CGPoint?
    var date: TimeInterval?
    var roastPoints_: [RoasterStatus]?
    var roastPoints: [RoasterStatus] {
        get {
            if roastPoints_ == nil {
                roastPoints_ = self.parseRoastData()
            }
            return roastPoints_ ?? [RoasterStatus]()
        }
        set {
            roastPoints_ = newValue
        }
    }
    
    
    init(machineUuid: String!, profileUuid: String!) {
        self.date = Date().timeIntervalSince1970
        self.uuid = UUID().uuidString.replacingOccurrences(of: "-", with: "")
        self.machineUuid = machineUuid
        self.profileUuid = profileUuid
        super.init()
    }
    
    
    func roastPointsWithoutPreheating() -> [RoasterStatus] {
        roastPoints.filter { (status) -> Bool in
            return status.time > 0
        }
    }

    func roastPointsWithoutPreheatingAndCooling() -> [RoasterStatus] {
        roastPoints.filter { (status) -> Bool in
            return status.time > 0 && status.state != IK_STATUS_COOLING
        }
    }
    
    func preheatingPoints() -> [RoasterStatus] {
        roastPoints.filter { (status) -> Bool in
            return status.state == IK_STATUS_HEATING
        }
    }
    
    func serializeRoastPoints(roundTime: Bool = false) {
        if (roastPoints.count == 0) {
            roastData = ""
        }
       var stream = "time,fan set,setpoint,fan speed,temp above,state,heater,p,i,d,temp below, temp board, j, ror_above\n"
        var time: Int = 0
        var t: Float = 0
        for p in roastPoints {
            if (roundTime) {
                if (Int(p.time.rounded()) != time) {
                    time = Int(p.time.rounded())
                    t = Float(time)
                }
            } else {
                t = p.time
            }
            
            stream.append("\(t),\(p.fanSet),\(p.setpoint),\(p.fanSpeed)")
            stream.append(",\(p.temp_above),\(p.stateName()!),")
            stream.append("\(p.heater),\(p.p),\(p.i),\(p.d),\(p.temp_below),\(p.temp_board),\(p.j),\(p.ror_above)\n")
        }
        print(stream)
        roastData = stream
    }
    
    
    func serializeToJson() -> String {
        let dict = NSMutableDictionary()
        var result = ""
        guard let currentUser = AppCore.sharedInstance.firebaseManager.currentUser  else {
            return result
        }
        dict["creator_id"] = currentUser.uid 
        dict["app_device_id"] = AppCore.sharedInstance.settingsManager.deviceUuid
        dict["roast_profile_id"] = profileUuid
        dict["roaster_id"] = machineUuid
        dict["roast_id"] = uuid
        dict["date"] = Date().dateStr()
        dict["timezone_offset"] = TimeZone.current.secondsFromGMT() / 60 / 60
        if let regionCode = Locale.current.regionCode {
            dict["country"] = regionCode
        }
        if let roastCount = roaster?.roastCount {
            dict["number_of_roasts"] = roastCount
        }
        
        if let lastPreheatingPoint = preheatingPoints().last {
            dict["pre_heat_power"] = lastPreheatingPoint.heater
            dict["pre_heat_temp"] = lastPreheatingPoint.temp_above
        }
        
        if let colorChange = colorChange {
            dict["colour_change_time"] = colorChange.x
            dict["colour_change_temp"] = colorChange.y
        }

        if let firstCrack = firstCrack {
            dict["1st_crack_time"] = firstCrack.x
            dict["1st_crack_temp"] = firstCrack.y
        }
        
        if let secondCrack = secondCrack {
            dict["2nd_crack_time"] = secondCrack.x
            dict["2nd_crack_temp"] = secondCrack.y
        }
        
        if let lastRoastingPoint = lastRoastingPoint() {
            dict["finish_point_time"] = lastRoastingPoint.time
            dict["finish_point_temp"] = lastRoastingPoint.temperature
        }
        
        if let profileUuid = profileUuid, let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: profileUuid), let coffeeName = profile.coffeeName {
            dict["coffee_name"] = coffeeName
        }

        if let uuid = uuid, let notesData = AppCore.sharedInstance.sqliteManager.roastNotes(forRoastId: uuid) {
             dict["coffee_name"] = notesData["coffeeType"] ?? ""
             dict["notes"] = notesData["notes"] ?? ""
             dict["photo"] =  uuid
        }
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
        
    }
    
    func parseRoastData() -> [RoasterStatus] {
        var points = [RoasterStatus]()
        guard roastData != nil else {
            return points
        }
        
        let lines = roastData!.components(separatedBy: "\n")
        //skip first line
        var firstLine = true
        for line in lines {
            if (firstLine) {
                firstLine = false
                continue
            }
            let point = RoasterStatus()
            let elements = line.components(separatedBy: ",")
            if (elements.count >= 10) {
                point.time = Float(elements[0])!
                point.fanSet = Float(elements[1])!
                point.setpoint = Float(elements[2])!
                point.fanSpeed = Int32(elements[3])!
                point.temp_above = Float(elements[4])!
                point.state = stateFromString(state: elements[5])
                point.heater = Int32(elements[6])!
                point.p = Float(elements[7])!
                point.i = Float(elements[8])!
                point.d = Float(elements[9])!
                if (elements.count >= 12) {
                    point.temp_below =  Float(elements[10])!
                    point.temp_board = Float(elements[11])!
                }
                if (elements.count >= 13) {
                    point.j = Float(elements[12])!
                }
                if (elements.count >= 14) {
                    point.ror_above = Float(elements[13])!
                }
                points.append(point)
            }
        }
        return points
    }
    
    func stateFromString(state: String) -> RoasterState {
        switch state {
        case "busy":
            return IK_STATUS_BUSY
        case "cooling":
            return IK_STATUS_COOLING
        case "pre-heating":
            return IK_STATUS_HEATING
        case "idle":
            return IK_STATUS_IDLE
        case "doser open":
            return IK_STATUS_OPEN
        case "error":
            return IK_STATUS_PROBLEM
        case "ready for roast":
            return IK_STATUS_READY
        case "roasting":
            return IK_STATUS_ROASTING
        case "ready to blowover":
            return IK_STATUS_READY_TO_BLOWOVER
        case "test mode":
            return IK_STATUS_TEST_MODE
        case "unknown":
            return IK_STATUS_UNKNOWN
        default:
            return IK_STATUS_UNKNOWN
        }
    }
    
    func roastDataPoints() -> [RoastTempPoint] {
        var result = [RoastTempPoint]()
        guard let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: profileUuid!) else {
            return result
        }
        
        for status in self.roastPoints {
            let point = RoastTempPoint()
            point.time = Float(status.time)
            if (profile.tempSensor == TempSensorBelow) {
                point.temperature = status.temp_below
            } else {
                point.temperature = status.temp_above
            }
            result.append(point)
        }
        return result
    }
    
    func roastPointsToRateOfRiseIntervalPoints(interval: Int = 15) -> [RoastTempPoint] {
        let roastPoints = self.roastDataPoints()
        if (roastPoints.count < 2) {
            return [RoastTempPoint]()
        }
        var rorData = [RoastTempPoint]()
        var prevPoint = roastPoints[0]
        var rorValue: Float = 0
        
        
        for i in 1..<roastPoints.count {
            let curPoint = roastPoints[i]
            if (curPoint.time >= (prevPoint.time + Float(interval))) {
                rorValue = (curPoint.temperature - prevPoint.temperature) / (curPoint.time - prevPoint.time) * 60
                //print("\(rorValue)")
                var rorPoint = RoastTempPoint()
                rorPoint.time = prevPoint.time
                rorPoint.temperature = Float(rorValue)
                rorData.append(rorPoint)
                
                rorPoint = RoastTempPoint()
                rorPoint.time = curPoint.time
                rorPoint.temperature = Float(rorValue)
                rorData.append(rorPoint)
                
                prevPoint = curPoint
            }
            
        }
        
        return rorData
    }
    
    func turningPoint() -> CGPoint? {
        guard storedTurningPoint == nil else {
            return storedTurningPoint
        }
        guard let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: profileUuid!) else {
            return nil
        }
        
        guard let time = roastPointsWithoutPreheating().first?.time, time < Float(5) else {return nil}
            
        var minPoint: RoasterStatus?
        var minTemp: Float = Float.greatestFiniteMagnitude
        for point in roastPointsWithoutPreheating() {
            var temp: Float
//            if (profile.tempSensor == TempSensorBelow) {
//                temp = point.temp_below
//            } else {
                temp = point.temp_above
//            }
            
            if (point.state == IK_STATUS_ROASTING && temp < minTemp) {
                minTemp = temp
                minPoint = point
            }
            if (point.time > 30) {
              //  if (profile.tempSensor == TempSensorBelow) {
              //      temp = (minPoint?.temp_below) ?? 0
              //  } else {
                    temp = (minPoint?.temp_above) ?? 0
              //  }
                storedTurningPoint = CGPoint(x: CGFloat(minPoint?.time ?? 0), y: CGFloat(temp))
                return storedTurningPoint
            }
        }
    return CGPoint.zero
    }

    
    func lastRoastingPoint() -> RoastTempPoint? {
        guard storedLastRoastingPoint == nil else {
            return storedLastRoastingPoint
        }
        let resultPoint = RoastTempPoint()
        resultPoint.time = 0
        resultPoint.temperature = 0
        for point in roastPoints.reversed() {
            if point.state == IK_STATUS_ROASTING {
                resultPoint.time = point.time
                resultPoint.temperature = point.temp_above
                return resultPoint
            }
        }
        return nil
    }

    func endRoastingPoint() -> RoastTempPoint? {
        guard storedLastRoastingPoint == nil else {
            return storedLastRoastingPoint
        }
        let resultPoint = RoastTempPoint()
        resultPoint.time = 0
        resultPoint.temperature = 0
        var previousStatus: RoasterStatus?
        for point in roastPoints.reversed() {
            if let previousStatus = previousStatus, point.state == IK_STATUS_ROASTING && previousStatus.state == IK_STATUS_COOLING {
                resultPoint.time = point.time
                resultPoint.temperature = point.temp_above
                return resultPoint
            }
            previousStatus = point
        }
        return nil
    }
    
    func getReadableDate() -> String {
        return (Date(timeIntervalSince1970: date!)).dateStr2()
    }
    
    
    func temp(forTime time: Float) -> Float? {
        for point in roastPoints {
            if time >= point.time {
                return point.temp_above
            }
        }
        return nil
    }
    
    func serializeAboveTemperatureToCropsterJson(processingId: String) -> String {
        var result = ""
        let dataDict = NSMutableDictionary()
        
        var pointArray = [[String : AnyObject]]()
        var time  = 0
        for roastPoint in roastPoints {
            if Int(roastPoint.time) != time && roastPoint.time <= lastRoastingPoint()?.time ?? 0  {
                time = Int(roastPoint.time)
                pointArray.append(["time" : time * 1000 as AnyObject,
                                   "value" : roastPoint.temp_above as AnyObject])
            }
        }
        dataDict["attributes"] = ["duration" : Int(lastRoastingPoint()?.time ?? 0) * 1000,
                                  "name" : "beanTemperature",
                                  "unit" : "CELSIUS",
                                  "values" : pointArray]
        dataDict["type"] = "processingCurves"
        
        dataDict["relationships"] = ["processing" : ["data" : ["id" : processingId, "type" : "processings"]]]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
    
    
    func serializeAboveCoolDownTemperatureToCropsterJson(processingId: String) -> String {
        var result = ""
        let dataDict = NSMutableDictionary()
        
        var pointArray = [[String : AnyObject]]()
        var time  = 0
        for roastPoint in roastPoints {
            if Int(roastPoint.time) != time && roastPoint.time > lastRoastingPoint()?.time ?? 0  {
                time = Int(roastPoint.time)
                pointArray.append(["time" : time * 1000 as AnyObject,
                                   "value" : roastPoint.temp_above as AnyObject])
            }
        }
        dataDict["attributes"] = ["duration" : Int(lastRoastingPoint()?.time ?? 0) * 1000,
                                  "name" : "beanCoolDownTemperature",
                                  "unit" : "CELSIUS",
                                  "values" : pointArray]
        dataDict["type"] = "processingCurves"
        
        dataDict["relationships"] = ["processing" : ["data" : ["id" : processingId, "type" : "processings"]]]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
    
    
    func serializeBelowTemperatureToCropsterJson(processingId: String) -> String {
        var result = ""
        let dataDict = NSMutableDictionary()
        
        var pointArray = [[String : AnyObject]]()
        var time  = 0
        for roastPoint in roastPoints {
            if Int(roastPoint.time) != time && roastPoint.time <= lastRoastingPoint()?.time ?? 0  {
                time = Int(roastPoint.time)
                pointArray.append(["time" : time * 1000 as AnyObject,
                                   "value" : roastPoint.temp_below as AnyObject])
            }
        }
        dataDict["attributes"] = ["duration" : Int(lastRoastingPoint()?.time ?? 0) * 1000,
                                  "name" : "inletTemperature",
                                  "unit" : "CELSIUS",
                                  "values" : pointArray]
        dataDict["type"] = "processingCurves"
        
        dataDict["relationships"] = ["processing" : ["data" : ["id" : processingId, "type" : "processings"]]]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
    
    
    func serializeBelowCoolDownTemperatureToCropsterJson(processingId: String) -> String {
        var result = ""
        let dataDict = NSMutableDictionary()
        
        var pointArray = [[String : AnyObject]]()
        var time  = 0
        for roastPoint in roastPoints {
            if Int(roastPoint.time) != time && roastPoint.time > lastRoastingPoint()?.time ?? 0  {
                time = Int(roastPoint.time)
                pointArray.append(["time" : time * 1000 as AnyObject,
                                   "value" : roastPoint.temp_below as AnyObject])
            }
        }
        dataDict["attributes"] = ["duration" : Int(lastRoastingPoint()?.time ?? 0) * 1000,
                                  "name" : "inletCoolDownTemperature",
                                  "unit" : "CELSIUS",
                                  "values" : pointArray]
        dataDict["type"] = "processingCurves"
        
        dataDict["relationships"] = ["processing" : ["data" : ["id" : processingId, "type" : "processings"]]]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: ["data" : dataDict],
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
}
