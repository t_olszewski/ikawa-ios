//
//  IKNotificationsManager.swift
//  IKAWApp
//
//  Created by Admin on 4/5/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class IKRoastingDoneNotificationHandler: NSObject {
    static let sharedInstance = IKRoastingDoneNotificationHandler()
    var lastHandler:IKRoastingDoneNotificationHandler?
    
    
    func notiRoastingDone() {
        
        if let currentRoast = AppCore.sharedInstance.roastManager.currentRoast {
            currentRoast.serializeRoastPoints()
            if let roastData = currentRoast.roastData {
                let data = roastData.data(using: .ascii, allowLossyConversion: true)!
                let fileName = "last_completed_roast.csv"
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                let filePath = URL(fileURLWithPath: documentsPath).appendingPathComponent(fileName)
                do {
                    try data.write(to: filePath)
                }
                catch let error {
                    // handle error here
                    print("Error writing file: \(error.localizedDescription)")
                }
            }
        }
        processNotification()
    }
    
    
    
    func processNotification() {
        if (UIApplication.shared.applicationState == .active) {
            #if TARGET_PRO
            self.lastHandler = nil
            let roast = AppCore.sharedInstance.roastManager.lastRoast

            if (roast != nil && roast?.uuid != nil)  {
                
                guard let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: roast?.profileUuid ?? ""), let lastProfileRoastPoint = (profile.roastPoints as? [RoastTempPoint])?.last else {
                    return
                }
                roast?.date = AppCore.sharedInstance.settingsManager.internetTime().timeIntervalSince1970
                if abs(lastProfileRoastPoint.time - (roast?.lastRoastingPoint()?.time ?? 0)) > 3 { // interrupted roasting
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_STATUS_COOLDOWN_MANUAL", label: "")
                    let confirmationDialogView = ButtonsPopupModalView()
                    confirmationDialogView.layout = .vertical
                    confirmationDialogView.title = "$save_this_roast".localized
                    confirmationDialogView.subtitle = "$would_you_save_this_roast_to_Roast_Log".localized
                    confirmationDialogView.setUpButton(index: .First, title: "$save_up".localized, type: .Filled, callback: {
                       _ = AppCore.sharedInstance.roastNotesManager.createRoastNotesForRoast(roast: roast)
                    })

                    confirmationDialogView.setUpButton(index: .Second, title: "$no_up".localized, type: .Bordered, callback: {
                    })
                    

                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                    
                } else {
                    if let roastNotes = AppCore.sharedInstance.roastNotesManager.createRoastNotesForRoast(roast: roast) {
                    let confirmationDialogView = ButtonsPopupModalView()
                    confirmationDialogView.layout = .vertical
                    confirmationDialogView.title = "$roast_complete".localized
                    confirmationDialogView.subtitle = "$roast_complete_message".localized
                    confirmationDialogView.setUpButton(index: .First, title: "$add_notes_up".localized, type: .Filled, callback: {
                        #if TARGET_PRO
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_END_ADD_NOTES", label: "")
                        #endif
                        AppCore.sharedInstance.roastNotesManager.goToRoastNotestScreen(roastNotes: roastNotes)
                    })

                    let isLoginToCropster = AppCore.sharedInstance.cropsterManager.isInitialized
                    confirmationDialogView.setUpButton(index: .Second, title: (isLoginToCropster ? "$export_up" : "$export_csv_up").localized, type: .Bordered, image: (isLoginToCropster ? UIImage(named: "ExportIcon") : nil) , callback: {
                        self.lastHandler = nil
                        if isLoginToCropster {
                            #if TARGET_PRO
                            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_END_EXPORT_CROPSTER", label: "")
                            #endif
                            
                            AppCore.sharedInstance.cropsterManager.getMachinesPopupShow({(complete) in
                                UIApplication.shared.keyWindow?.makeToastActivity(.center)
                                AppCore.sharedInstance.cropsterManager.exportToCropster(roastLog: roastNotes, callback: { (error) in
                                    UIApplication.shared.keyWindow?.hideToastActivity()
                                    if let error = error {
                                        let alertView = ButtonsPopupModalView()
                                        alertView.layout = .vertical
                                        alertView.title = NSLocalizedString("$cropster_error_title", comment: "")
                                        alertView.subtitle = error.localizedDescription
                                        alertView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                                        IKNotificationManager.sharedInstance.show(view: alertView)
                                    }
                                })
                            })

                        } else {
                            #if TARGET_PRO
                            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_END_EXPORT_CVS", label: "")
                            #endif
                            AppCore.sharedInstance.roastNotesManager.exportData(notes: roastNotes)
                            //AppCore.sharedInstance.roastManager.shareRoastLog(roastNotes: roastNotes)
                        }
                    })
                    confirmationDialogView.setUpButton(index: .Third, title: "$open_roast_log_up".localized, type: .Bordered, callback: {
                        #if TARGET_PRO
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_END_LAUNCH_LOG", label: "")
                        #endif
                        
                        _ = AppCore.sharedInstance.roastNotesManager.goToRoastNotesDetailScreen(roastNotes: roastNotes)
                        confirmationDialogView.isUserInteractionEnabled = true
                    })
                    confirmationDialogView.setUpButton(index: .Fourth, title: "$dismiss_up".localized, type: .Cancel, callback: {
                        #if TARGET_PRO
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_END_DISMISS", label: "")
                        #endif
                    })

                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                }
                    AppCore.sharedInstance.roastManager.lastRoast = nil
                    
                }
                
            }


            
            #elseif TARGET_HOME
            
//            let roastingDoneModalView = RoastingDoneModalView.fromNib()
//            roastingDoneModalView.cancelCallback = {() in
//                self.lastHandler = nil
//                }
//            roastingDoneModalView.shareCallback = {() in
//                self.lastHandler = nil
//                guard IKRoastManager.sharedInstance.currentRoast?.profileUuid != nil else {return}
//                if let profile = (IKProfileManager.sharedInstance.findProfileInListByUuid(uuid: (IKRoastManager.sharedInstance.currentRoast?.profileUuid)!)) {
//                    IKProfileManager.sharedInstance.shareProfile(profile: profile)
//                }
//            }
//            IKNotificationManager.sharedInstance.show(view: roastingDoneModalView)

            #endif
        } else {
            let notification = UILocalNotification()
            notification.fireDate = Date(timeIntervalSinceNow: 1)
            notification.alertBody = NSLocalizedString("$roasting_done", comment: "")
            notification.alertAction = "View"
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.applicationIconBadgeNumber = 0
            UIApplication.shared.scheduleLocalNotification(notification)
            lastHandler = self
        }
    }
}
