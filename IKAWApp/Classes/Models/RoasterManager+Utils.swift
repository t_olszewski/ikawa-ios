//
//  RoasterManager+Utils.swift
//  IKAWApp
//
//  Created by Admin on 4/24/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import IKRoasterLib

extension RoasterManager {
    func serializeRoasterData() -> String! {
        var result = ""
        if (roaster == nil) {
            result = "Roaster not connected\n"
        } else {
            result = "Roaster name: \(roaster.name ?? "-")\n"
            result.append("Roaster id: \(roaster.roasterId)\n")
            result.append("Roaster type:  \(roaster.typeName()!)\n")
            result.append("Roaster variant: \(roaster.variantName()!)\n")
            result.append("Firmware version: \(roaster.firmwareVersion)\n")
            
        }
        return result
    }
}
