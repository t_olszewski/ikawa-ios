//
//  IKPushNotificationManager.swift
//  IKAWA-Home
//
//  Created by Admin on 11/12/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class IKNotificationManager: NSObject {
    
static let sharedInstance = IKNotificationManager()
    var modalViews: [ModalView] = []
    var showedView: ModalView?
    var isWaiting = true {
        didSet {
            if !isWaiting {
                self.update()
            }
        }
    }
    
    func update() {
        if let showedView = showedView {
            UIApplication.shared.keyWindow?.bringSubviewToFront(showedView)
        } else {
            if (self.modalViews.count > 0) {
                self.show(view: self.modalViews.remove(at: 0))
            }
        }
    }
    
    func show(view: ModalView) {
        if showedView == nil && !isWaiting && UIApplication.shared.keyWindow != nil{
            showedView = view
            view.show(animated: true, dismissCallback: {
                let serialQueue = DispatchQueue(label: "com.ikawa")
                serialQueue.sync {
                    self.showedView = nil
                    if (self.modalViews.count > 0) {
                        self.show(view: self.modalViews.remove(at: 0))
                    }
                }
            })
            UIApplication.shared.keyWindow?.bringSubviewToFront(showedView!)
        } else {
            if (showedView != nil) {
                UIApplication.shared.keyWindow?.bringSubviewToFront(showedView!)
            }
         modalViews.append(view)
        }
    }
    

    
    func showPushNotification(userInfo: [AnyHashable: Any]) {
        
        var coffeeUrl = userInfo["coffee_url"] as? String ?? ""
        let profileUrl = userInfo["profile_url"] as? String ?? ""
        let title = userInfo["title"] as? String ?? ""
        let message =  userInfo["message"] as? String ?? ""
        print(userInfo)
//        if let aps: [AnyHashable: Any] = userInfo["aps"] as? Dictionary {
//            if let alert: [AnyHashable: Any] = aps["alert"] as? Dictionary {
//                if let message_ = alert["body"] as? String {
//                    message = message_
//                }
//
//                if let title_ = alert["title"] as? String {
//                    title = title_
//                }
//            }
//        }
        
        let pushNotificationView = PushNotificationView()


        if (!coffeeUrl.isEmpty && !profileUrl.isEmpty) {
            pushNotificationView.firstButtonTitle = NSLocalizedString("$buy_coffee", comment: "")
            pushNotificationView.secondButtonTitle = NSLocalizedString("$add_profile_to_libary", comment: "")
            pushNotificationView.firstButtonCallback = {
                #if TARGET_HOME
                NavigationHelper.openShopWithCoffee(url: coffeeUrl)
                #endif
            }
            pushNotificationView.secondButtonCallback = {
                if let url = URL(string: profileUrl) {
                    if let appDelegate =  UIApplication.shared.delegate as? AppDelegate {
                        appDelegate.handleOpenUrl(url: url)
                    }
                }
            }
            
            //pushNotificationView.thumbnailImageView.isHidden = true
            pushNotificationView.title = profile(fromUrlStr: profileUrl)?.name ?? ""
            pushNotificationView.subtitle = ""
            pushNotificationView.descr = message
            
            show(view: pushNotificationView)
        }

        else if (coffeeUrl.isEmpty && !profileUrl.isEmpty) {
            pushNotificationView.firstButtonTitle = NSLocalizedString("$add_profile_to_libary", comment: "")
            pushNotificationView.secondButtonTitle = ""
            pushNotificationView.firstButtonCallback = {
                if let profile = self.profile(fromUrlStr: profileUrl) {
                    AddReceiptManager.sharedInstance.createProfile(profile: profile, editMode: false)
                } else {
                    let dialogView = ButtonsPopupModalView()
                    dialogView.layout = .vertical
                    dialogView.title = NSLocalizedString("$profile_format_unsupported", comment: "")
                    dialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    IKNotificationManager.sharedInstance.show(view: dialogView)
                }
            }
            
            //pushNotificationView.thumbnailImageView.isHidden = true
            pushNotificationView.title = profile(fromUrlStr: profileUrl)?.name ?? ""
            pushNotificationView.subtitle = ""
            pushNotificationView.descr = message
            
            show(view: pushNotificationView)
        }
        
        else if (!coffeeUrl.isEmpty && profileUrl.isEmpty) {
            pushNotificationView.firstButtonTitle = coffeeUrl.isEmpty ? "" : NSLocalizedString("$find_out_more", comment: "")
            pushNotificationView.secondButtonTitle = ""
            pushNotificationView.firstButtonCallback = {
                guard let url = URL(string: coffeeUrl) else {
                    return //be safe
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            pushNotificationView.secondButtonCallback = {}
            
            pushNotificationView.hideThumbnail()
            pushNotificationView.title = title
            pushNotificationView.subtitle = ""
            pushNotificationView.descr = message
            
            show(view: pushNotificationView)
        }
    }
    
    
    func profile(fromUrlStr profileUrl:String) -> Profile? {
        if let url = URL(string: profileUrl) {
            if (url.absoluteString.range(of: "/profile") != nil) {
                if let query = url.query {
                    if (query.count == 32) {

                    } else {
                        
                        if let profile = AppCore.sharedInstance.profileManager.base64DecodeProfile(profile: query)  {
                            if profile.type == ProfileTypeUndefined {
                                profile.type = ProfileTypeIkawa
                            }
                            return profile
                        }
                    }
                }
            }
        }
        return nil
    }
    
    func registerAPN() {
        
    }
}
