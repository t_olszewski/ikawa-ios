//
//  IKSimulatorManager.swift
//  IKAWApp
//
//  Created by Serg on 02/04/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class IKSimulatorManager: NSObject {
static let sharedInstance = IKSimulatorManager()
    var simulateRoast: IKRoast?
    let timing = 0.01
    var roast: IKRoast?
    var currentPoint: RoasterStatus?
    var roaster: Roaster?
    
    override init() {
        super.init()
        RoasterManager.instance().delegates.add(self)
        let peripheral = IKPeripheral()
        //peripheral.peripheral.state = 2
        //        peripheral.peripheral = CBPeripheral()
        //        peripheral.peripheral.name = "simulation"
        IKInterface.instance()?.setValue(peripheral, forKey: "connectedPeripheral")
        if (RoasterManager.instance().roaster == nil) {
            RoasterManager.instance().roaster = Roaster()
            RoasterManager.instance().roaster.name = "simulator"
        }
        RoasterManager.instance().perform(NSSelectorFromString("peripheralDidConnect:"), with: peripheral)
        
        //        roast.roastPoints = [RoasterStatus]()
        //        for i in 0...300 {
        //            let roastPoint = RoasterStatus()
        //            roastPoint.time = Float(i)
        //            roastPoint.temp_above = Float(Int(arc4random_uniform(6)) + 100)
        //            roastPoint.temp_below = Float(Int(arc4random_uniform(6)) + 50)
        //            roastPoint.temp_board = Float(Int(arc4random_uniform(6)) + 20)
        //            //roastPoint.setpoint = Double(roasterStatus.setpoint)
        //            roastPoint.fanSpeed = Int32(Int(arc4random_uniform(6)) + 40)
        ////            roastPoint.state = roasterStatus.state
        ////            roastPoint.heater = Int(roasterStatus.heater)
        ////            roastPoint.p = Double(roasterStatus.p)
        ////            roastPoint.i = Double(roasterStatus.i)
        ////            roastPoint.d = Double(roasterStatus.d)
        ////            roastPoint.j = Double(roasterStatus.j)
        //            roast.roastPoints.append(roastPoint)
        //        }
        //        IKRoastManager.sharedInstance.currentRoast = roast
    }

    deinit {
        
    }
    
    func startSimulating() {
        if (AppCore.sharedInstance.roastManager.currentRoast != nil) {
            return
        }
        self.currentPoint = nil
        roast = IKRoast(machineUuid: "123", profileUuid: "simulator")
        let filePath = Bundle.main.url(forResource: "simulateRoastData", withExtension: "", subdirectory: "")!
        do {
            let text = try String(contentsOf: filePath, encoding: .utf8)
            roast!.roastData = text
            roast!.parseRoastData()
            if (roast!.roastPoints.count > 0) {
                self.getNextRoastPoint()
            }
        }
        catch {
            print("can't read file")
            return
        }
    }
    
    
    func getNextRoastPoint() {
        DispatchQueue.main.asyncAfter(deadline: .now() + timing) {
            
            if (RoasterManager.instance().roaster == nil) {
                RoasterManager.instance().roaster = Roaster()
                RoasterManager.instance().roaster.name = "simulator"
            }
            
            if (self.currentPoint == nil) {
                self.currentPoint = self.roast!.roastPoints.first
            }
           
            for delegate  in RoasterManager.instance().delegates {
                if ((delegate as AnyObject).responds(to: #selector(RoasterManagerDelegate.roasterDidUpdateStatus(_:)))) {
                    RoasterManager.instance().roaster.status = self.currentPoint
                    RoasterManager.instance().roaster.error = Int32(RoasterErrorBoardTooHot.rawValue)
                    RoasterManager.instance().roaster.status.error_code = RoasterManager.instance().roaster.error
                    (delegate as! RoasterManagerDelegate).roasterDidUpdateStatus!(RoasterManager.instance().roaster)
                }
            }
            let index = self.roast!.roastPoints.index(of: self.currentPoint!)
            if (index! < (self.roast!.roastPoints.count - 1)) {
                self.currentPoint = self.roast!.roastPoints[index! + 1]
                self.getNextRoastPoint()
            }
        }
    }
    
    func simulatedRoast() -> IKRoast? {
        roast = IKRoast(machineUuid: "123", profileUuid: "simulator")
        let filePath = Bundle.main.url(forResource: "simulateRoastData", withExtension: "", subdirectory: "")!
        do {
            let text = try String(contentsOf: filePath, encoding: .utf8)
            roast!.roastData = text
            roast!.parseRoastData()
        }
        catch {
            print("can't read file")
            return nil
        }
        return roast
    }
    
}



extension IKSimulatorManager : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        RoasterManager.instance().roaster = roaster
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        RoasterManager.instance().roaster = roaster
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            let peripheral = IKPeripheral()
            //peripheral.peripheral.state = 2
            //        peripheral.peripheral = CBPeripheral()
            //        peripheral.peripheral.name = "simulation"
            IKInterface.instance()?.setValue(peripheral, forKey: "connectedPeripheral")
            if (RoasterManager.instance().roaster == nil) {
                RoasterManager.instance().roaster = Roaster()
                RoasterManager.instance().roaster.name = "simulator"
            }
            RoasterManager.instance().perform(NSSelectorFromString("peripheralDidConnect:"), with: peripheral)
        })
        

        
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        //RoasterManager.instance().delegates
        
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        
    }
}
