//
//  Protocols.swift
//  IKAWApp
//
//  Created by Admin on 4/3/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import IKRoasterLib

protocol ChartViewProtocol {
    func updateTempPoint(roast: IKRoast?)
    func updateChartData()
    func goToEditState()
    func goToViewState()
    func viewWillTransition()
    #if TARGET_PRO
    func enableMarkersEditing(enable: Bool)
    func selectedRoastPoint() -> RoastTempPoint?
    func selectedFanPoint() -> RoastFanPoint?
    #endif
}
