//
//  IKRoasterErrorManager.swift
//  IKAWApp
//
//  Created by Admin on 5/21/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib
import MessageUI

class IKMachineErrorNotificationHandler: NSObject {
    
    var lastHandler:IKMachineErrorNotificationHandler?
    var lastError: Int32 = Int32(RoasterErrorNone.rawValue)
    override init() {
        super.init()
        RoasterManager.instance().delegates.add(self)
    }
    
    deinit {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
}
    
    func processNotification() {
        if (UIApplication.shared.applicationState == .active) {
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$roaster_issue_title", comment: "")
            
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$contact_support", comment: ""), type: .Bordered) {
                [unowned self] in
                #if TARGET_PRO
                AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_CONTACT_SUPPORT", label: "")
                #endif
                self.lastHandler = nil
                self.contactSupport()
            }
            confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$cancel", comment: ""), type: .Cancel) {
                [unowned self] in
                self.lastHandler = nil
            }
      
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
        } else {
            let notification = UILocalNotification()
            notification.fireDate = Date(timeIntervalSinceNow: 1)
            notification.alertBody = NSLocalizedString("$roaster_issue_title", comment: "")
            notification.alertAction = "View"
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.applicationIconBadgeNumber = 0
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    lastHandler = self
    
    }
    
}


extension IKMachineErrorNotificationHandler : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {

    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
    }
    
    func roaster(_ roaster: Roaster!, didReceiveSetting setting: Int32, withValue value: NSNumber!, success: Bool) {

    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        if (roaster.error > 1 && roaster.error != lastError) {
            lastError = roaster.error
            processNotification()
            
            #if TARGET_PRO
            switch roaster.error {
            case Int32(RoasterErrorBoardTooHot.rawValue) :
                AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_ERROR_2", label: "")
            case Int32(RoasterErrorAboveBeansTooHot.rawValue) :
                AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_ERROR_3", label: "")
            case Int32(RoasterErrorAboveBeansTooCold.rawValue) :
                AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_ERROR_4", label: "")
            case Int32(RoasterErrorFanNotSpinning.rawValue) :
                AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_ERROR_5", label: "")
            case Int32(RoasterErrorMotorFailure.rawValue) :
                AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_ERROR_6", label: "")
            case Int32(RoasterErrorBelowBeansTooHot.rawValue) :
                AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_ERROR_7", label: "")
            case Int32(RoasterErrorBelowBeansTooCold.rawValue) :
                AppCore.sharedInstance.loggingManager.trackUIAction(action: "PRO_ERROR_8", label: "")
            default:
                break
            }
            #endif
            AppCore.sharedInstance.loggingManager.logMachineError(error: roaster.error)
            if let currentRoast = AppCore.sharedInstance.roastManager.currentRoast {
                AppCore.sharedInstance.loggingManager.logRoast(roast: currentRoast)
            }
            

//            switch Int(roaster.status.error_code) {
//            case Int(RoasterErrorBoardTooHot.rawValue):
//                print(roaster.errorName())
//
//            default :
//                print(roaster.errorName())
//
//            }
        }
        
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        
    }
}

extension IKMachineErrorNotificationHandler : MFMailComposeViewControllerDelegate {
    
    func contactSupport() {
        if MFMailComposeViewController.canSendMail() {
            if let mailComposeViewController = configuredMailComposeViewController() {
                let appDelegate  = UIApplication.shared.delegate as! AppDelegate
                let viewController = appDelegate.window!.rootViewController!
                viewController.present(mailComposeViewController, animated: true, completion: nil)
            }
        } else {
            self.showSendMailErrorAlert()
        }
    }
    

    func configuredMailComposeViewController() -> MFMailComposeViewController? {
        guard RoasterManager.instance().roaster != nil, let errorName = RoasterManager.instance().roaster.errorName() else {
            return nil
        }
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["support@ikawacoffee.com"])
        mailComposerVC.setSubject("Roaster Issue")
        
        
        let roasterStatus = RoasterManager.instance().roaster.status.roastPointDescription()!
        let roastData = RoasterManager.instance().serializeRoasterData()!
        
        mailComposerVC.setMessageBody(String(format: "Issue: %@\n\nCurrent roaster status:\n%@\n\n%@App version: %@, %@\nFirmware version: %@, %@\nRoaster type: %@\nRoaster variant: %@\nRoaster id: %@\n\nwww.ikawacoffee.com\ninfo@ikawacoffee.com\n",
                                             errorName, roasterStatus, roastData,
                                             Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! CVarArg,
                                             Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! CVarArg,
                                             UserDefaults.standard.object(forKey: "firmwareVersionSettingsId") as! CVarArg,
                                             UserDefaults.standard.object(forKey: "firmwareRevisionSettingsId") as! CVarArg,
                                             UserDefaults.standard.object(forKey: "roasterTypeSettingsId") as! CVarArg,
                                             UserDefaults.standard.object(forKey: "roasterVariantSettingsId") as! CVarArg,
                                             UserDefaults.standard.object(forKey: "roasterIdSettingsId") as! CVarArg
                                             ),
                                      isHTML: false)
        
        if let currentRoast = AppCore.sharedInstance.roastManager.currentRoast {
            currentRoast.serializeRoastPoints()
            if let roastData = currentRoast.roastData {
                let data = roastData.data(using: .ascii, allowLossyConversion: true)!
                mailComposerVC.addAttachmentData(data, mimeType: "text/csv", fileName: "current_roast.csv")
            }
        }
        
        let fileName = "last_completed_roast.csv"
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath = URL(fileURLWithPath: documentsPath).appendingPathComponent(fileName)
       
        if  let data = NSData( contentsOfFile: filePath.path) {
            mailComposerVC.addAttachmentData(Data(referencing: data), mimeType: "text/csv", fileName: "last_completed_roast.csv")
        }
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let alert = UIAlertController(title: "Error", message: "No mail editor configured on device.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
        }))
        let vc = UIApplication.shared.keyWindow?.rootViewController
        vc!.present(alert, animated: true, completion: {})
        lastHandler = nil
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case .cancelled, .saved:
            break
        case .sent:
            let alert = UIAlertController(title: "Success", message: "The message was sent successfully.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
            }))
            let vc = UIApplication.shared.keyWindow?.rootViewController
            vc!.present(alert, animated: true, completion: {})
            AppCore.sharedInstance.loggingManager.trackUIAction(action: "Roast Error Mailed", label: "")
        case .failed:
            let alert = UIAlertController(title: "Error", message: "The message could not be send.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
            }))
            let vc = UIApplication.shared.keyWindow?.rootViewController
            vc!.present(alert, animated: true, completion: {})
        }
        controller.dismiss(animated: true, completion: nil)

    }
}
