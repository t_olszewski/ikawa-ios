//
//  Roaster+Utils.swift
//  IKAWApp
//
//  Created by Admin on 4/23/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import IKRoasterLib

extension Roaster {
    
    func isUpdateAvailable() -> Int {
        if variant == RoasterVariantNESPRESSO {return 0}
        if (UserDefaults.standard.bool(forKey: "forceFirmwareUpdateId")) {
            return IK_FIRMWARE_UPDATE_VERSION
        }
        
        if (firmwareVersion < IK_FIRMWARE_UPDATE_VERSION) {
            #if TARGET_PRO
            //only show this message once a week
            let now =  NSDate().timeIntervalSince1970
            let snoozeTime = UserDefaults.standard.double(forKey: "firmwareUpdateSnoozeTime")
            if (snoozeTime > now) {
                return 0;
            }
            //snooze for a week
            UserDefaults.standard.set(now + 3600 * 24 * 7, forKey: "firmwareUpdateSnoozeTime")
            #endif
            return IK_FIRMWARE_UPDATE_VERSION;
        }
        return 0
    }
    
    func hasBelowSensor() -> Bool {
        return (type.rawValue > RoasterTypeV2.rawValue && variant == RoasterVariantPRO) || variant == RoasterVariantHOME
    }

    func hasAboveSensor() -> Bool {
        return variant != RoasterVariantHOME
    }
    
    func serializeToJson() -> String {
        let dict = NSMutableDictionary()
        var result = ""
        if let profile = RoasterManager.instance().profile {
            dict["creator_id"] = profile.uuid
        } else {
            dict["creator_id"] = ""
        }
        
        dict["roaster_id"] = roasterId
        dict["roaster_type"] = typeName()
        dict["roaster_variant"] = variantName()
        dict["bluetooth_name"] = name
        dict["firmware_revision"] = firmwareBuild
        dict["firmware_version"] = firmwareVersion
        dict["last_used"] = Date().dateStr()
        dict["number_of_roasts"] = roastCount
        dict["last_user_id"] = AppCore.sharedInstance.firebaseManager.currentUser?.uid ?? ""
        dict["voltage"] = roasterVoltage.rawValue
        dict["above_sensor_type"] = aboveSensorType.rawValue
        dict["time_multiplier"] = timeMultiplier
        dict["last_profile_id"] = RoasterManager.instance()?.profile.uuid ?? ""
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
}
