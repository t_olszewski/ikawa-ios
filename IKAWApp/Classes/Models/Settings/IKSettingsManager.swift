//
//  IKSettings.swift
//  IKAWApp
//
//  Created by Admin on 2/28/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Foundation
import IKRoasterLib
import KeychainAccess
import TrueTime

class IKSettingsManager: NSObject {
    let timeClient = TrueTimeClient.sharedInstance
    var isFirmwareUpdateVCShowed = false
    var _deviceUuid : String?
    var deviceUuid:String? {
        get {
            if (_deviceUuid == nil) {
                let appName = Bundle.main.infoDictionary![kCFBundleNameKey! as String] as! String
                let keychain = Keychain(service: appName)
                var strApplicationUUID = keychain["password"]
                
                if (strApplicationUUID == nil)
                {
                    strApplicationUUID = UIDevice.current.identifierForVendor?.uuidString
                    strApplicationUUID = strApplicationUUID?.replacingOccurrences(of: "-", with: "")
                    keychain["password"] = strApplicationUUID;
                }
                _deviceUuid = strApplicationUUID
            }
            return _deviceUuid
        }
    }
    
    var workingWithoutAccount: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "workingWithoutAccount")
        }
        
        set (newValue) {
            UserDefaults.standard.set(newValue, forKey: "workingWithoutAccount")
            UserDefaults.standard.synchronize()
        }
    }
    
    override init() {
        super.init()
        RoasterManager.instance().delegates.add(self)
        RoasterManager.instance().fwManager.delegates.add(self)
        timeClient.start()
    }
    
    deinit {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    //MARK: Public
    
    public func internetTime() -> Date {
        return timeClient.referenceTime?.now() ?? Date()
    }
    
    public func populateSettings() {
        
        UserDefaults.standard.setValue(appVersion(), forKey: "appVersionSettingsId")
        UserDefaults.standard.setValue(roasterName(), forKey: "roasterNameSettingsId")
        UserDefaults.standard.setValue(roasterId(), forKey: "roasterIdSettingsId")
        UserDefaults.standard.setValue(roasterTypeName(), forKey: "roasterTypeSettingsId")
        UserDefaults.standard.setValue(roasterVariantName(), forKey: "roasterVariantSettingsId")
        UserDefaults.standard.setValue(roasterVoltageName(), forKey: "roasterVoltageSettingsId")
        UserDefaults.standard.setValue(roasterAboveSensorName(), forKey: "aboveSensorSettingsId")
        UserDefaults.standard.setValue(roasterFirmwareVersion(), forKey: "firmwareVersionSettingsId")
        UserDefaults.standard.setValue(roasterFirmwareRevision(), forKey: "firmwareRevisionSettingsId")
        UserDefaults.standard.setValue(roasterDeviceId1(), forKey: "deviceId1SettingsId")
        UserDefaults.standard.setValue(roasterDeviceId2(), forKey: "deviceId2SettingsId")
        UserDefaults.standard.setValue(fanOpenLoopControl(), forKey: "fanOpenLoopControl")
        UserDefaults.standard.setValue(roastCount(), forKey: "roastCount")
        UserDefaults.standard.setValue(roastCountSinceService(), forKey: "roastCountSinceService")
        UserDefaults.standard.setValue(lastServiceDate(), forKey: "lastServiceDate")
        
        confirmationDialog(name: "hideSendReceiptConfirmationDialog", hide: UserDefaults.standard.bool(forKey: "hideSendReceiptConfirmationDialog"))
        confirmationDialog(name: "swapJarsConfirmationDialog", hide: UserDefaults.standard.bool(forKey: "swapJarsConfirmationDialog"))
        confirmationDialog(name: "finishConfirmationDialog", hide: UserDefaults.standard.bool(forKey: "finishConfirmationDialog"))
        confirmationDialog(name: "doserIsOpenConfirmationDialog", hide: UserDefaults.standard.bool(forKey: "doserIsOpenConfirmationDialog"))
        confirmationDialog(name: "removePointConfirmationDialog", hide: UserDefaults.standard.bool(forKey: "removePointConfirmationDialog"))
        confirmationDialog(name: "dontShowArchiveDialog", hide: UserDefaults.standard.bool(forKey: "dontShowArchiveDialog"))
        
         if let roaster = RoasterManager.instance().roaster {
            if ((IK_TWO_SENSOR_SUPPORT && roaster.type.rawValue > RoasterTypeV2.rawValue && roaster.variant == RoasterVariantPRO) || IK_SIMULATE_ROASTER) {
                UserDefaults.standard.set(true, forKey: "twoSensorSupported")
            } else {
                UserDefaults.standard.set(false, forKey: "twoSensorSupported")
            }
            UserDefaults.standard.set(roaster.aboveSensorType.rawValue, forKey: "aboveSensorTypeId")
        }
        UserDefaults.standard.synchronize()
    }
    
    func supportsTwoSensors() -> Bool {
        if (IK_TWO_SENSOR_SUPPORT) {
            return UserDefaults.standard.bool(forKey: "twoSensorSupported")
        } else {
            return false
        }
        
    }
    
    func aboveSensorType() -> AboveSensorType {
        return AboveSensorType(rawValue: UInt32(UserDefaults.standard.integer(forKey: "aboveSensorTypeId")))
    }
    
    func isTrackingDisabled() -> Bool {
       return UserDefaults.standard.bool(forKey: "trackingDisabled")
    }
    
    //MARK: Private
    func appVersion() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return "\(version), \(build)"
    }
    
    func roasterName() -> String {
        guard let roaster = RoasterManager.instance().roaster else {
            return "-"
        }
        return roaster.name ?? "-"
    }

    func roasterId() -> String {
        if let roaster = RoasterManager.instance().roaster {
            if (roaster.roasterId != 0) {
                return "\(roaster.roasterId)"
            } else {
                return "-"
            }
        } else {
            return "-"
        }
    }
    
    
    func roasterTypeName() -> String {
        if let roaster = RoasterManager.instance().roaster {
            return roaster.typeName()
        } else {
            return "-"
        }
    }
    
    func roasterVariantName() -> String {
        if let roaster = RoasterManager.instance().roaster {
           return roaster.variantName()
        } else {
            return "-"
        }
    }

    func roasterVoltageName() -> String {
        if let roaster = RoasterManager.instance().roaster {
            switch roaster.roasterVoltage {
            case RoasterVoltageUnset:
                return "0"
            case RoasterVoltage100:
                return "100"
            case RoasterVoltage120:
                return "120"
            case RoasterVoltage230:
                return "230"
            default:
                return "Unknown Value"
            }
        } else {
            return "-"
        }
    }
    
    func roasterAboveSensorName() -> String {
        if let roaster = RoasterManager.instance().roaster {
            switch roaster.aboveSensorType {
            case AboveSensorTypeUnset:
                return "Unset"
            case AboveSensorTypeFast:
                return "Fast"
            case AboveSensorTypeRobust:
                return "Strong"
            default:
                return "Unknown Value"
            }
        } else {
            return "-"
        }
    }
    
    func roasterFirmwareVersion() -> String {
        if let roaster = RoasterManager.instance().roaster {
            return "\(roaster.firmwareVersion)"
            
        } else {
            return "-"
        }
    }
    
    func roasterFirmwareRevision() -> String {
        if let roaster = RoasterManager.instance().roaster {
            if (roaster.firmwareBuild == nil) {
                return "-"
            } else {
                return "\(roaster.firmwareBuild!)"
            }
        } else {
            return "-"
        }
    }
    
    func roasterDeviceId1() -> String {
        if let uuid = deviceUuid {
            let toIndex = uuid.index(uuid.startIndex, offsetBy: (uuid.count / 2))
            return String(uuid[uuid.startIndex..<toIndex])
        } else {
            return "-"
        }
    }

    func roasterDeviceId2() -> String {
        if let uuid = deviceUuid {
            let fromIndex = uuid.index(uuid.startIndex, offsetBy: (uuid.count / 2))
            return String(uuid[fromIndex..<uuid.endIndex])
        } else {
            return "-"
        }
    }
    
    func fanOpenLoopControl() -> String {
        if let roaster = RoasterManager.instance().roaster {
            if (roaster.fanOpenLoopControl == nil) {
                return "-"
            } else {
                return roaster.fanOpenLoopControl as String
            }
        } else {
            return "-"
        }
    }

    func roastCount() -> String {
        if let roaster = RoasterManager.instance().roaster {
            return "\(roaster.roastCount)"
        } else {
            return "-"
        }
    }

    func roastCountSinceService() -> String {
        if let roaster = RoasterManager.instance().roaster {
            return "\(roaster.roastCountSinceService)"
        } else {
            return "Not set"
        }
    }
    
    func lastServiceDate() -> String {
        if let roaster = RoasterManager.instance().roaster {
            if (roaster.lastServiceDate == nil) {
                return "Not set"
            } else {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                return formatter.string(from: roaster.lastServiceDate)
            }
        } else {
            return "Not set"
        }
    }
    
    func confirmationDialog(name: String, hide: Bool) {
        let key = name + (AppCore.sharedInstance.firebaseManager.currentUser?.uid ?? "")
        UserDefaults.standard.set(hide, forKey: key)
        UserDefaults.standard.set(hide, forKey: name)
        UserDefaults.standard.synchronize()
    }

    //MARK: firmware
    func processReceiveFirmwareVersion() {
        if  let firmwareBuild = RoasterManager.instance().roaster.firmwareBuild {
            UserDefaults.standard.set(String(RoasterManager.instance().roaster.firmwareVersion), forKey: "firmwareVersionSettingsId")
            UserDefaults.standard.set(firmwareBuild, forKey: "firmwareRevisionSettingsId")
        }
        
        let newVersion: Int = RoasterManager.instance().roaster.isUpdateAvailable()
        //RoasterManager.instance().fwManager.firmwareUpdateVersion = Int32(newVersion)
        
        if (newVersion > 0) {
            if ((UserDefaults.standard.bool(forKey: "repressFirmwareUpdateMessage") && !UserDefaults.standard.bool(forKey: "forceFirmwareUpdateId")) || UserDefaults.standard.bool(forKey: "firmwareUpdateInProgress")) {
                //only repress once
                
                return
            }
            UserDefaults.standard.set(true, forKey: "repressFirmwareUpdateMessage")
            var title = ""
            var body = ""
                #if TARGET_HOME
                title = NSLocalizedString("$update_available_title", comment: "")
                body = NSLocalizedString("$update_available_message", comment: "")
                #else
                title = NSLocalizedString("$update_available_title", comment: "").replacingOccurrences(of: "%d", with: String(newVersion))
                body =  NSLocalizedString("$update_available_message", comment: "").replacingOccurrences(of: "%d", with: String(newVersion))
                #endif
            
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = title
            confirmationDialogView.subtitle = body
            
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$later", comment: ""), type: .Bordered) {
                UserDefaults.standard.set(false, forKey: "forceFirmwareUpdateId")
                UserDefaults.standard.set(IK_FIRMWARE_UPDATE_VERSION, forKey: "firmwareVersionReminded_" + self.roasterName())
            }
            
            confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$update_now", comment: ""), type: .Filled) {
                // Goto FirmwareUpdateScene scene
                UserDefaults.standard.set(false, forKey: "forceFirmwareUpdateId")
                let storyboard = UIStoryboard(name: "FirmwareUpdate", bundle: nil)
                let vc = storyboard.instantiateInitialViewController()
                let topVc = UIApplication.shared.keyWindow?.rootViewController
                topVc?.present(vc!, animated: true, completion: nil)
                self.isFirmwareUpdateVCShowed = true
            }

            confirmationDialogView.setUpButton(index: .Third, title: NSLocalizedString("$ok", comment: ""), type: .Cancel) {
                UserDefaults.standard.set(false, forKey: "forceFirmwareUpdateId")
            }
            
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
        }
    }
}

extension IKSettingsManager : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        UserDefaults.standard.set(false, forKey: "repressFirmwareUpdateMessage")
        populateSettings()
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        let user = AppCore.sharedInstance.firebaseManager.currentUser
        
        if (user != nil && !(user?.isAnonymous ?? true)) || AppCore.sharedInstance.settingsManager.workingWithoutAccount{
        } else { //not logined user
           IKInterface.instance()?.disconnect()
           IKInterface.instance()?.stopScan()
        }
        
        if isFirmwareUpdateVCShowed {
            if let topVc = UIApplication.shared.keyWindow?.rootViewController, let firmwareUpdateVC = topVc.presentedViewController as? FirmwareUpdateVC  {
                firmwareUpdateVC.dismiss(animated: true) {
                    self.isFirmwareUpdateVCShowed = false
                }
                let confirmationDialogView = ButtonsPopupModalView()
                confirmationDialogView.layout = .vertical
                confirmationDialogView.title = NSLocalizedString("$update_failed_title", comment: "")
                confirmationDialogView.subtitle = NSLocalizedString("$update_failed_message", comment: "")
                confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            }
        }
    }
    
    func roaster(_ roaster: Roaster!, didReceiveSetting setting: Int32, withValue value: NSNumber!, success: Bool) {
        populateSettings()
        
        #if TARGET_PRO
        // don't allow connection to HOME roasters
        if (roaster.variant == RoasterVariantHOME && !IK_CONNECT_TO_ANY_ROASTER) {
            
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$unsupported_roaster_title", comment: "")
            confirmationDialogView.subtitle = NSLocalizedString("$unsupported_roaster_message", comment: "")
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            
            if (IKInterface.instance().isConnected() || IK_SIMULATE_ROASTER) {
                IKInterface.instance().blockPeripheral()
                IKInterface.instance()?.reconnect()
            }
            return
        }
        #else
        // don't allow connection to PRO roasters
        if ((roaster.variant == RoasterVariantPRO || roaster.variant == RoasterVariantNESPRESSO) && !IK_CONNECT_TO_ANY_ROASTER) {
            
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$unsupported_roaster_title", comment: "")
            confirmationDialogView.subtitle = NSLocalizedString("$unsupported_roaster_message", comment: "")
            
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            
            
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)

            if (IKInterface.instance().isConnected() || IK_SIMULATE_ROASTER) {
                IKInterface.instance().blockPeripheral()
                IKInterface.instance().reconnect()
            }
            return
        }
        #endif
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
    }

    func roasterDidUpdateStatus(_ roaster: Roaster!) {

    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        
    }
    
    func roasterDidDidReceiveFirmwareVersion(_ roaster: Roaster!) {
        processReceiveFirmwareVersion()
    }
    
    func roasterDidUpdateMachType(_ roaster: Roaster!) {
        if (UserDefaults.standard.bool(forKey: "setOpenLoopFan") || UserDefaults.standard.bool(forKey: "setClosedLoopFan")) {
            
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .horisontal
            confirmationDialogView.title = NSLocalizedString("$change_fan_control_title", comment: "")
            confirmationDialogView.subtitle = NSLocalizedString("$change_fan_control_message", comment: "")
            
            
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                if UserDefaults.standard.bool(forKey: "setClosedLoopFan") {
                    RoasterManager.instance()?.updateSetting(Int32(SettingIdFanOpenLoopControl.rawValue), withValue: NSNumber(value: 0))
                } else if UserDefaults.standard.bool(forKey: "setOpenLoopFan") {
                    RoasterManager.instance()?.updateSetting(Int32(SettingIdFanOpenLoopControl.rawValue), withValue: NSNumber(value: 1))
                }
                RoasterManager.instance()?.reboot()
                UserDefaults.standard.set(false, forKey: "setOpenLoopFan")
                UserDefaults.standard.set(false, forKey: "setClosedLoopFan")
            }
            confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$cancel", comment: ""), type: .Cancel) {
                UserDefaults.standard.set(false, forKey: "setOpenLoopFan")
                UserDefaults.standard.set(false, forKey: "setClosedLoopFan")
            }

            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
        }
    }
}


extension IKSettingsManager: FirmwareManagerDelegate {
    func firmwareProgressChanged(_ progress: Float) {
        if !isFirmwareUpdateVCShowed && !IKNotificationManager.sharedInstance.isWaiting {
            let storyboard = UIStoryboard(name: "FirmwareUpdate", bundle: nil)
            let vc = storyboard.instantiateInitialViewController()
            let topVc = UIApplication.shared.keyWindow?.rootViewController
            topVc?.present(vc!, animated: true, completion: nil)
            self.isFirmwareUpdateVCShowed = true
        }
    }
    func firmwareUpdateStarted() {
        if !isFirmwareUpdateVCShowed && !IKNotificationManager.sharedInstance.isWaiting {
            let storyboard = UIStoryboard(name: "FirmwareUpdate", bundle: nil)
            let vc = storyboard.instantiateInitialViewController()
            let topVc = UIApplication.shared.keyWindow?.rootViewController
            topVc?.present(vc!, animated: true, completion: nil)
            self.isFirmwareUpdateVCShowed = true
        }
    }
    func firmwareUpdateComplete() {
        
    }
    func firmwareUpdateFailed() {
        if isFirmwareUpdateVCShowed {
            if let topVc = UIApplication.shared.keyWindow?.rootViewController, let firmwareUpdateVC = topVc.presentedViewController as? FirmwareUpdateVC  {
                firmwareUpdateVC.dismiss(animated: true) {
                    self.isFirmwareUpdateVCShowed = false
                }
                let confirmationDialogView = ButtonsPopupModalView()
                confirmationDialogView.layout = .vertical
                confirmationDialogView.title = NSLocalizedString("$update_failed_title", comment: "")
                confirmationDialogView.subtitle = NSLocalizedString("$update_failed_message", comment: "")
                confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            }
        }
    }
    func firmwareJumpedToApp() {
        
    }
}
