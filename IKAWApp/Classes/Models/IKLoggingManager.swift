//
//  IKLoggingManager.swift
//  IKAWApp
//
//  Created by Admin on 7/5/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class IKLoggingManager: NSObject {
    typealias SendDataCallback = (_ success: Bool, _ message: String?) -> Void
    var deviceUpToDate = false
    
    func sendDevice() {
        guard !AppCore.sharedInstance.settingsManager.isTrackingDisabled() else {
            return
        }
        
        let deviceUuid = AppCore.sharedInstance.settingsManager.deviceUuid
        let dict = NSMutableDictionary()
        var result = ""
        dict["device_id"] = deviceUuid
        dict["last_user_id"] = AppCore.sharedInstance.firebaseManager.currentUser?.uid ?? ""
        dict["app_version"] = AppCore.sharedInstance.settingsManager.appVersion()
        dict["device_type"] =  UIDevice.current.model
        dict["os_version"] = UIDevice.current.systemVersion
        dict["last_session"] = AppCore.sharedInstance.settingsManager.internetTime().dateStr()
        dict["timezone_offset"] = TimeZone.current.secondsFromGMT() / 60 / 60
        dict["profile_count"] = AppCore.sharedInstance.profileManager.profilesInLibrary?.count
        if let regionCode = Locale.current.regionCode {
            dict["country"] = regionCode
        }
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        AppCore.sharedInstance.firebaseManager.firebaseSendData(type: "appdevice", key: deviceUuid!, json: result) { (success, message) in
            if (success) {
                // Updated or created device succesfully
                self.deviceUpToDate = true
                self.sendProfiles();
                
                if (UserDefaults.standard.bool(forKey: "db_changed") && AppCore.sharedInstance.syncManager.needCreateBackup()) {
                    AppCore.sharedInstance.syncManager.uploadUserLibrary(roundTimestamp: false, callback: { (error) in
                        if let error = error {
                            let errorDialogView = ButtonsPopupModalView()
                            errorDialogView.layout = .vertical
                            errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                            errorDialogView.subtitle = error.localizedDescription
                            errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                            
                            IKNotificationManager.sharedInstance.show(view: errorDialogView)
                        } else {
                            UserDefaults.standard.set(false, forKey: "db_changed")
                            UserDefaults.standard.set(false, forKey: "NeedMergeWithAccount")
                        }
                    })
                    
                }
            }
        }
    }
    
    
    func sendProfiles() {
        if let profiles = AppCore.sharedInstance.profileManager.profiles {
            for profile in profiles {
                sendProfile(profile: profile)
            }
        }
    }
    
    func sendProfile(profile: Profile) {
        guard !AppCore.sharedInstance.settingsManager.isTrackingDisabled() else {
            return
        }
        if (profile.onServer == 0) {
            let uuid = profile.uuid
            AppCore.sharedInstance.firebaseManager.firebaseSendData(type: "roastprofile", key:uuid!, json: profile.serializeToJson()) { (success, message) in
                if (success) {
                    if let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: uuid!) {
                        profile.onServer = 1
                        _ = AppCore.sharedInstance.sqliteManager.updateProfile(profile: profile)
                    }
                }
            }
        }
    }
    
    func sendRoaster(roaster: Roaster) {
        guard !AppCore.sharedInstance.settingsManager.isTrackingDisabled() else {
            return
        }
        #if TARGET_HOME
        if (roaster.variant != RoasterVariantHOME) {return}
        #else
        if (roaster.variant == RoasterVariantHOME) {return}
        #endif
        
        guard roaster.roasterId > 0 else {return}
        
        AppCore.sharedInstance.firebaseManager.firebaseSendData(type: "roaster", key: String(roaster.roasterId), json: roaster.serializeToJson()) { (success, message) in
            
        }
        
        
        let dict = NSMutableDictionary()
        var result = ""
        if let currentUser = AppCore.sharedInstance.firebaseManager.currentUser {
           dict[currentUser.uid] = AppCore.sharedInstance.settingsManager.internetTime().dateStr()
        }
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        AppCore.sharedInstance.firebaseManager.firebaseSendData(type: "roasterusers", key: String(roaster.roasterId), json: result) { (success, message) in
            
        }
        
    }
    
    
    func sendUserRoasters(roaster: Roaster) {
        guard !AppCore.sharedInstance.settingsManager.isTrackingDisabled() else {
            return
        }
        
        guard let currentUser = AppCore.sharedInstance.firebaseManager.currentUser else {
            return
        }
        
        #if TARGET_HOME
        if (roaster.variant != RoasterVariantHOME) {return}
        #else
        if (roaster.variant == RoasterVariantHOME) {return}
        #endif
        
        guard roaster.roasterId > 0 else {return}
        

        let dict = NSMutableDictionary()
        var result = ""

        
        dict[String(roaster.roasterId)] = AppCore.sharedInstance.settingsManager.internetTime().dateStr()
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        AppCore.sharedInstance.firebaseManager.firebaseSendData(type: "userroasters", key: String(currentUser.uid), json: result) { (success, message) in
            
        }
        
    }
    
    
    func updateUserInfo(firstName: String, lastName: String, accountName: String, userType: Int?, callback: @escaping SendDataCallback) {
        guard let user = AppCore.sharedInstance.firebaseManager.currentUser else {
            callback(false, "user not found")
            return
        }

        if (user.uid == "anonymous") {
            callback(false, "anonymous")
            return
        }
        
        let dict = NSMutableDictionary()
        var result = ""
        dict["account_name"] = accountName
        
        if let userType = userType {
            dict["user_type"] = userType
        }
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .utf8)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        AppCore.sharedInstance.firebaseManager.firebaseUpdateData(type: "user_details", key: user.uid, json: result) { (success, message) in
            callback(success, message)
        }
    }
    
    func sendUser() {
        guard let user = AppCore.sharedInstance.firebaseManager.currentUser else {
            return
        }
        
        if (user.uid == "anonymous") {
            return
        }
        
        let deviceUuid = AppCore.sharedInstance.settingsManager.deviceUuid
        
        let dict = NSMutableDictionary()
        var result = ""
        dict["email"] = user.email ?? ""
        dict["last_active"] = AppCore.sharedInstance.settingsManager.internetTime().dateStr()
        dict["last_appdevice"] = deviceUuid
        dict["last_roaster_used"] = UserDefaults.standard.string(forKey: "roasterIdSettingsId")
        
        AppCore.sharedInstance.syncManager.getUserInfo { (userInfo) in
            if let userType = userInfo?.userType {
                dict["user_type"] = userType
            }
            
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: dict,
                options: []) {
                let theJSONText = String(data: theJSONData,
                                         encoding: .ascii)
                print("JSON string = \(theJSONText!)")
                result = theJSONText!
            }
            
            AppCore.sharedInstance.firebaseManager.firebaseUpdateData(type: "user", key: user.uid, json: result) { (success, message) in

            }
            
        }
    }
    
    func logAllRoasts() {
        if let logs = AppCore.sharedInstance.sqliteManager.roastLogs() {
            for log in logs {
                
                if let machineUUID = log["machineUUID"] as? String, let profileUUID = log["profileUUID"] as? String {
                    let roast = IKRoast(machineUuid: machineUUID, profileUuid: profileUUID)
                    roast.uuid = log["UUID"] as? String
                    roast.date = log["roastDate"] as? TimeInterval
                    roast.roastData = log["roastData"] as? String
                    roast.parseRoastData()
                    
                    if ((log["csvOnServer"] as! Int64) == 0) {
                        AppCore.sharedInstance.firebaseManager.uploadRoastCsv(csv: roast.roastData!, uuid: roast.uuid!, profile: roast.profileUuid!, roaster: roast.machineUuid!)
                    }
                    if ((log["dataOnServer"] as! Int64) == 0) {
                        AppCore.sharedInstance.loggingManager.logRoast(roast: roast)
                    }
                }
            }
        }
    }
    
    func logRoast(roast: IKRoast) {
        guard !AppCore.sharedInstance.settingsManager.isTrackingDisabled() else {
            return
        }
        AppCore.sharedInstance.firebaseManager.firebaseSendData(type: "roast", key: roast.uuid!, json: roast.serializeToJson()) { (success, message) in
            if (success) {
               _ = AppCore.sharedInstance.sqliteManager.markDataOnServer(roastUdid: roast.uuid!)
            }
        }
    }
    
    
    func trackEvent(name: String, label: String) {
        AppCore.sharedInstance.firebaseManager.trackUIAction(action: name, label: label)
    }
    
    func trackScreen(name: String) {
        AppCore.sharedInstance.firebaseManager.trackScreen(screenName: name)
    }
    
    func trackUIAction(action: String, label: String) {
        AppCore.sharedInstance.firebaseManager.trackUIAction(action: action, label: label)
    }
    
    func logMachineError(error: Int32) {
        guard !AppCore.sharedInstance.settingsManager.isTrackingDisabled() else {
            return
        }
        AppCore.sharedInstance.firebaseManager.firebaseSendData(type: "machineerror", key: "", json: serializeErrorToJson(error: error)) { (success, message) in
            
        }
    }
    
    func serializeErrorToJson(error: Int32) -> String {
        let dict = NSMutableDictionary()
        var result = ""
        dict["roaster_id"] = RoasterManager.instance().roaster.roasterId
        dict["app_device_id"] = AppCore.sharedInstance.settingsManager.deviceUuid
        if let currentRoast = AppCore.sharedInstance.roastManager.currentRoast {
            if let profileUuid = currentRoast.profileUuid {
                dict["roast_profile_id"]   = profileUuid
            }
            dict["roast_id"] = currentRoast.uuid
            
        }
        dict["error_code"]      = error
        
        let utcTimeZoneStr = AppCore.sharedInstance.settingsManager.internetTime().dateStr()
        
        dict["date"]            = utcTimeZoneStr
        dict["last_state"]      = RoasterManager.instance().roaster.status.state.rawValue
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        return result
    }
    
    
    
}
