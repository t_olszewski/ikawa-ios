//
//  ExRoasterManager.swift
//  IKAWApp
//
//  Created by Admin on 3/7/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class IKRoastManager: NSObject {
    var currentRoast :IKRoast?
    var lastRoast :IKRoast?
    var previousState :RoasterState?
    var doserOpenModalView: ButtonsPopupModalView?
    var swapJarsModalView: ButtonsPopupModalView?
    var finishModalView: ButtonsPopupModalView?
    var isProfileUpdating = false
    let sqliteManager: IKSqliteManager
    
    private var currentState: RoasterState? {
        willSet {
            if currentState != newValue {
                if let state = newValue {
                    sendLogState(state: state)
                }
            }
        }
    }
    
    init(sqliteManager: IKSqliteManager) {
        self.sqliteManager = sqliteManager
        super.init()
    }
    
    deinit {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    //MARK: - Public
    func saveRoast(roast: IKRoast?) -> Bool {
        if (roast == nil) {
            return false
        }
        var roastData = [String : Any]()
        roastData["UUID"] = roast?.uuid
        roastData["profileUUID"] = roast?.profileUuid
        roastData["machineUUID"] = roast?.machineUuid
        roastData["roastDate"] = roast?.date
        roastData["roastData"] = roast?.roastData
        return sqliteManager.saveRoast(roastData: roastData)
    }
    
    func roastForUuid(uuid: String) -> IKRoast? {
        var roast: IKRoast?
        if let roastData = sqliteManager.roastForUuid(uuid: uuid) {
            if (roastData["machineUUID"] != nil && roastData["profileUUID"] != nil) {
                roast = IKRoast(machineUuid: (roastData["machineUUID"] as! String), profileUuid: roastData["profileUUID"] as? String)
                roast?.roastData = roastData["roastData"] as? String
                roast?.date = TimeInterval(roastData["roastDate"] as! String)
                roast?.uuid = roastData["UUID"] as? String
                roast?.csvOnServer = roastData["csvOnServer"] as? Bool
                roast?.dataOnServer = roastData["dataOnServer"] as? Bool
            }
        }
        return roast
    }
    
    func roastAndCoolDownProgress() -> Float {
        if RoasterManager.instance().roaster == nil {
            return 0
        }
        if (currentRoast?.roastPoints.count == 0) {
            return 0
        }
        
        guard let lastPoint = currentRoast?.roastPoints.last else {
            return 0
        }
        
        return (RoasterManager.instance().profile.cooldownPoint == nil) ? 0 : (lastPoint.time) / Float(RoasterManager.instance().profile.cooldownPoint.time)
    }
    
    func roastProgress() -> Float {
        guard let lastPoint = currentRoast?.roastPoints.last else {
            return 0
        }
        
        guard RoasterManager.instance().profile != nil, let profilePoints = RoasterManager.instance().profile.roastPoints, let lastProfilePoint = profilePoints.last as? RoastTempPoint else {
            return 0
        }
        
        return Float(lastPoint.time) / Float(lastProfilePoint.time)
    }
    
    func roastTimeLeft() -> String? {
        guard let lastPoint = currentRoast?.roastPoints.last else {
            return nil
        }
        let secLeft = RoasterManager.instance().profile.cooldownPoint.time - lastPoint.time
        if secLeft >= 0 {
            let minutes = Int(secLeft / 60)
            let minutesStr = minutes < 10 ? "0\(minutes)" : "\(minutes)"
            let sec = Int(secLeft.truncatingRemainder(dividingBy: 60))
            let secStr = sec < 10 ? "0\(sec)" : "\(sec)"
            return "\(minutesStr):\(secStr)"
        } else {
            return nil
        }
        
    }
    
    func roastTime() -> String? {
        guard let lastPoint = currentRoast?.roastPoints.last?.time else {
            return nil
        }
        if lastPoint >= 0 {
            return FormatHelper.timeFormat(seconds: lastPoint)
        } else {
            return nil
        }
    }
    #if TARGET_PRO
    func shareRoastLog(roastNotes: IKRoastNotes) {
        guard let profile = roastNotes.profile() else {return}
        guard let hex = AppCore.sharedInstance.profileManager.base64EncodeProfile(profile: profile) else {
            return
        }
        let profileUuid = profile.uuid
        AppCore.sharedInstance.firebaseManager.uploadProfileIcon(uuid: profileUuid!)
        var profileUrl = IK_PROFILE_BASEURL + hex
        var profileName = profile.name
        var htmlFile: String = ""
        #if TARGET_PRO
        if (profileUuid != nil && profileUuid!.count > 0) {
            htmlFile = Bundle.main.path(forResource: "roast_email", ofType: "html")!
        } else {
            htmlFile = Bundle.main.path(forResource: "roast_without_profile_email", ofType: "html")!
        }
        
        #else
        htmlFile = Bundle.main.path(forResource: "profile_email", ofType: "html")!
        #endif
        
        var htmlMail = (try? String(contentsOfFile: htmlFile))!
        var iconUrl = IK_PROFILE_IMAGE_URL
        iconUrl = iconUrl.replacingOccurrences(of: "{{ENV}}", with: IK_FIREBASE_DATABASE)
        iconUrl = iconUrl.replacingOccurrences(of: "{{UUID}}", with: profileUuid!)
        
        htmlMail = htmlMail.replacingOccurrences(of: "{{profile_icon_url}}", with: iconUrl)
        htmlMail = htmlMail.replacingOccurrences(of: "{{profile_url}}", with: profileUrl)
        if (profileUuid != nil) {htmlMail = htmlMail.replacingOccurrences(of: "{{profile_uuid}}", with: profileUuid!)}
        if (profileName != nil) {htmlMail = htmlMail.replacingOccurrences(of: "{{profile_name}}", with: profileName!)}
        let roastDetails = roastNotes.getDescription()
        htmlMail = htmlMail.replacingOccurrences(of: "{{roast_details}}", with: roastDetails)
        if (roastNotes.photo.count > 0) {
            htmlMail = htmlMail.replacingOccurrences(of: "{{photo_header}}", with: "<p style=\"font-family:\'Helvetica Neue\',verdana; font-size:13px; margin:15px 0 10px 0;\"><b>Photo:</b></p>")
        } else {
            htmlMail = htmlMail.replacingOccurrences(of: "{{photo_header}}", with: "")
        }
        
        if (profileName?.count == 0) {
            profileName = "Unknown profile"
        }
        
        if (profileUrl.count == 0) {
            profileUrl = "http://www.ikawacoffee.com"
        }
        
        var shareText = ""
        
        if (roastNotes.coffeeType.count > 0) {
            shareText = String(format: NSLocalizedString("$share_twitter_pro_roast_coffee", comment: ""), roastNotes.coffeeType, profile.name)
        } else {
            shareText = String(format: NSLocalizedString("$share_twitter_pro_roast", comment: ""), profile.name)
        }
        
        let provider = IKActivityItemProvider(placeholderItem: shareText)
        provider.mailString = htmlMail
        provider.profileUrl = profileUrl
        
        if (roastNotes.coffeeType.count > 0) {
            provider.mailSubject = String(format: NSLocalizedString("$share_email_subject_pro_roast_coffee", comment: ""), roastNotes.coffeeType, profile.name)
        } else {
            provider.mailSubject = String(format: NSLocalizedString("$share_email_subject_pro_roast", comment: ""), profile.name)
        }
        
        let excludeActivities = [UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.print, UIActivity.ActivityType.addToReadingList/*, UIActivity.ActivityType.airDrop*/]
        var items: [Any]
        if (roastNotes.photo.count > 0) {
            let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let filePath = documentDirectoryURL.appendingPathComponent(roastNotes.photo)
            if let shareImage = UIImage(contentsOfFile: filePath.path)?.imageByScalingProportionallyToSize(CGSize(width: 640, height: 640)) {
                items = [provider, shareImage]
            } else {
                items = [provider]
            }
        } else {
            items = [provider]
        }
        
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityController.excludedActivityTypes = excludeActivities
        
        func completionHandler(activityType: UIActivity.ActivityType?, shared: Bool, items: [Any]?, error: Error?) {
            if (shared) {
                switch activityType {
                case UIActivity.ActivityType.mail:
                    print("Shared Profile: Mail")
                    AppCore.sharedInstance.loggingManager.trackUIAction(action: "Shared Profile", label: "Mail")
                case UIActivity.ActivityType.postToFacebook:
                    print("Shared Profile: Facebook")
                    AppCore.sharedInstance.loggingManager.trackUIAction(action: "Shared Profile", label: "Facebook")
                case UIActivity.ActivityType.postToTwitter:
                    print("Shared Profile: Twitter")
                    AppCore.sharedInstance.loggingManager.trackUIAction(action: "Shared Profile", label: "Twitter")
                case UIActivity.ActivityType.message:
                    print("Shared Profile: Message")
                    AppCore.sharedInstance.loggingManager.trackUIAction(action: "Shared Profile", label: "Message")
                case UIActivity.ActivityType.copyToPasteboard:
                    print("Shared Profile: Copy")
                    AppCore.sharedInstance.loggingManager.trackUIAction(action: "Shared Profile", label: "Copy")
                case .none:
                    break
                case .some(_):
                    break
                }
                
            }
            else {
                print("Bad user canceled sharing :(")
            }
        }
        
        activityController.completionWithItemsHandler = completionHandler
        let vc = UIApplication.shared.keyWindow?.rootViewController
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
            activityController.popoverPresentationController?.sourceView = vc?.view
            activityController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            activityController.popoverPresentationController?.sourceRect = CGRect(x: 648, y: 984, width: 50, height: 50)
            
        }
        vc?.present(activityController, animated: true, completion: {
            
        })
    }
    #endif
    //MARK: - Private
    
    private func sendLogState(state: RoasterState) {
        switch state {
        case IK_STATUS_HEATING:
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_STATUS_PREHEAT", label: "")
        case IK_STATUS_READY:
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_STATUS_READYTOROAST", label: "")
        case IK_STATUS_COOLING:
            if currentRoast?.lastRoastingPoint() == nil {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_STATUS_COOLDOWN", label: "")
            }
        default:
            break
        }
    }
    
    func startNewRoast() {
        let profileOnRoaster = RoasterManager.instance().profile
        if (profileOnRoaster == nil) {
            return
        }
        profileOnRoaster?.dateLastRoasted = Date()
        _ = sqliteManager.updateProfile(profile: profileOnRoaster!)
        _ = RoasterManager.instance().roaster
        currentRoast = IKRoast(machineUuid: AppCore.sharedInstance.settingsManager.roasterId(), profileUuid: profileOnRoaster?.uuid)
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    func isRoasting() -> Bool {
        return currentRoast != nil
    }
    
}

extension IKRoastManager : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        currentRoast = nil
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    func roaster(_ roaster: Roaster!, didReceiveSetting setting: Int32, withValue value: NSNumber!, success: Bool) {

    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {

    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        #if TARGET_HOME
        if (roaster.status.state == IK_STATUS_OPEN && isRoasting()) {
            if doserOpenModalView == nil {
                if !UserDefaults.standard.bool(forKey: "doserIsOpenConfirmationDialog" + (AppCore.sharedInstance.firebaseManager.currentUser?.uid ?? "")) {
                    doserOpenModalView = ButtonsPopupModalView()
                    doserOpenModalView?.layout = .vertical
                    doserOpenModalView?.title = NSLocalizedString("$close_doser", comment: "")
                    doserOpenModalView?.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                        
                    }
                    doserOpenModalView?.setUpCheckBox(title: nil) {
                        [weak doserOpenModalView] in
                        AppCore.sharedInstance.settingsManager.confirmationDialog(name: "doserIsOpenConfirmationDialog", hide: self.doserOpenModalView?.isCheckBoxSelected ?? false)
                    }
                    doserOpenModalView?.show(animated: true, dismissCallback: {})
                }
            }
        } else if (doserOpenModalView != nil) {
            doserOpenModalView?.dismiss(animated: true)
            doserOpenModalView = nil
        }
        let key = "swapJarsConfirmationDialog" + (AppCore.sharedInstance.firebaseManager.currentUser?.uid ?? "")
        if !UserDefaults.standard.bool(forKey: key) {
            if swapJarsModalView == nil {
                if !UserDefaults.standard.bool(forKey: "swapJarsConfirmationDialog") {
                    swapJarsModalView = ButtonsPopupModalView()
                    swapJarsModalView?.layout = .vertical
                    swapJarsModalView?.title = NSLocalizedString("$swap_jars", comment: "")
                    swapJarsModalView?.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                        
                    }
                    swapJarsModalView?.setUpCheckBox(title: nil) {
                        [weak swapJarsModalView] in
                        AppCore.sharedInstance.settingsManager.confirmationDialog(name: "swapJarsConfirmationDialog", hide: self.swapJarsModalView?.isCheckBoxSelected ?? false)
                    }
                    swapJarsModalView?.show(animated: true, dismissCallback: {})
                }
            }
        } else if (swapJarsModalView != nil) {
            swapJarsModalView?.dismiss(animated: true)
            swapJarsModalView = nil
        }
        
        #endif
    
        
        
        
        
        // Check: Roasting is done?
        if (isRoasting() && roaster.status.state == IK_STATUS_IDLE) {
            // Check if we actually started roasting (t > 0)?
            if ((currentRoast?.roastPoints.last?.time)! > 0) {
                print("Roasting done!")
                currentRoast?.serializeRoastPoints()
                lastRoast = currentRoast
                IKRoastingDoneNotificationHandler.sharedInstance.notiRoastingDone()
                
                if (!UserDefaults.standard.bool(forKey: "disableSounds")) {
                    //make sound after 1 sec
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        playSound()
                    }
                }
                RoasterManager.instance()?.requestRoasterData()
                #if TARGET_HOME
                let key = "finishConfirmationDialog" + (AppCore.sharedInstance.firebaseManager.currentUser?.uid ?? "")
                if !UserDefaults.standard.bool(forKey: key), finishModalView == nil {
                    finishModalView = ButtonsPopupModalView()
                    finishModalView?.layout = .vertical
                    finishModalView?.title = NSLocalizedString("$finish_title", comment: "")
                    finishModalView?.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                        
                    }
                    finishModalView?.setUpCheckBox(title: nil) {
                        [weak finishModalView] in
                        AppCore.sharedInstance.settingsManager.confirmationDialog(name: "finishConfirmationDialog", hide: self.finishModalView?.isCheckBoxSelected ?? false)
                    }
                    finishModalView?.show(animated: true, dismissCallback: {})
                }
                #endif
            } else {
                print("Roast canceled!")
                NotificationCenter.default.post(name: Notification.Name.RoastDidCanceled, object: nil)
            }
            
            //lastRoast = currentRoast
            currentRoast = nil
            UIApplication.shared.isIdleTimerDisabled = false
        }
        
        // Check Started roasting?
        let roasterState = RoasterManager.instance().roaster.status.state
        if ((roasterState == IK_STATUS_HEATING || roasterState == IK_STATUS_ROASTING || roasterState == IK_STATUS_COOLING) && (currentRoast == nil || previousState == IK_STATUS_IDLE)) {
            AppCore.sharedInstance.loggingManager.trackUIAction(action: "roast started", label: "")
            print("Roast started!")
            NotificationCenter.default.post(name: Notification.Name.StartNewRoast, object: nil)
            startNewRoast()
        }
        
        previousState = roasterState
        
        if let roast = currentRoast {
            roast.roaster = roaster
            if let roasterStatus = RoasterManager.instance().roaster.status {
                if let lastRoastPoint = roast.roastPoints.last,  roasterStatus.time - lastRoastPoint.time > 5 {
                    roast.roastPoints.removeAll()
                    NotificationCenter.default.post(name: NSNotification.Name.StartNewRoast, object: nil)
                }
                
//                if (roast.roastPoints.count > 1 && roast.roastPoints[roast.roastPoints.index(of: roast.roastPoints.last!)! - 1].time > roast.roastPoints.last!.time) {
//                    return
//                }
//
//                if (roasterStatus.time == 0 && roast.roastPoints.count > 0) {
//                    roast.roastPoints[roast.roastPoints.count - 1] = roasterStatus
//                } else {
                    roast.roastPoints.append(roasterStatus)
//                }
                
            }
        }
        

        
        #if TARGET_PRO
        func calculateState() -> RoasterState {
            var state = RoasterManager.instance().roaster.status.state
            if (state == IK_STATUS_IDLE && (IKStatusBarView.previousState == IK_STATUS_OPEN || IKStatusBarView.previousState == IK_STATUS_READY)) {
                state = IK_STATUS_READY
            }
            return state
        }
        currentState = calculateState()
        #endif
        
        
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        
        guard !isProfileUpdating else {
            return
        }
        
        AppCore.sharedInstance.loggingManager.sendRoaster(roaster: roaster)
        AppCore.sharedInstance.loggingManager.sendUserRoasters(roaster: roaster)
        
        if let currentRoast = currentRoast {
            currentRoast.profileUuid = profile.uuid
        }
        
        let profileUuidExists = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: profile.uuid)
        let profileForNameAndCoffeeName = AppCore.sharedInstance.profileManager.profileForName(name: profile.name)
        
        if profileUuidExists != nil {
            RoasterManager.instance().profile = profileUuidExists
            profile.revision = profileUuidExists!.revision
            let key = "dontShowArchiveDialog" //+ (AppCore.sharedInstance.firebaseManager.currentUser?.uid ?? "")
            if UserDefaults.standard.bool(forKey: key), profileUuidExists?.archived ?? false {
                let confirmationDialogView = ButtonsPopupModalView()
                confirmationDialogView.layout = .vertical
                confirmationDialogView.title = NSLocalizedString("$profile_on_roaster_already_exists_within_archive", comment: "")
                confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                    
                }
                confirmationDialogView.setUpCheckBox(title: nil) {
                    [unowned confirmationDialogView] in
                    AppCore.sharedInstance.settingsManager.confirmationDialog(name: "dontShowArchiveDialog", hide: !confirmationDialogView.isCheckBoxSelected)
                }
                IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            }
        }
        

        guard (profileUuidExists != nil && !(profileUuidExists?.showInLibrary ?? false)) || profileUuidExists == nil else {
            return
        }
        
        if profileUuidExists != nil && profileForNameAndCoffeeName == nil {
            if !(profileUuidExists?.showInLibrary ?? false) {
                profileUuidExists?.showInLibrary = true
                _ = sqliteManager.updateProfile(profile: profileUuidExists!)
            }
            return
        }
        
        isProfileUpdating =  true
        if profile.type == ProfileTypeUndefined {
            profile.type = ProfileTypeUser
        }
        AppCore.sharedInstance.profileManager.saveProfile(profile: profile.copy() as! Profile, existsText: NSLocalizedString("$profile_exists", comment: "").replacingOccurrences(of: "%s", with: profile.name)) { (savedState, profile) in
            self.isProfileUpdating = false
            if (savedState == .OVERWRITTEN || savedState == .SAVED || savedState == .SAVED_AS) {
                RoasterManager.instance().sendProfile(profile)
            }
        }

    }
}
