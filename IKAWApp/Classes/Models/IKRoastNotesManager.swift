//
//  IKRoastNotesManager.swift
//  IKAWApp
//
//  Created by Admin on 5/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import GRDB
import MessageUI
import IKRoasterLib

class IKRoastNotesManager: NSObject {
    var displayedRoastNotesList: [IKRoastNotes]?
    var roastNotesList: [IKRoastNotes]?
    let sqliteManager: IKSqliteManager
    var roastManager: IKRoastManager
    
    init(sqliteManager: IKSqliteManager, roastManager: IKRoastManager) {
        self.sqliteManager = sqliteManager
        self.roastManager = roastManager
        super.init()
        self.updateAllRoastNotes()
    }

    
    func createRoastNotesForRoast(roast: IKRoast?) -> IKRoastNotes? {
         guard let roast = roast else {
            return nil
        }
        
        _ = AppCore.sharedInstance.roastManager.saveRoast(roast: roast)
        if let roaster = roast.roaster {
            AppCore.sharedInstance.loggingManager.sendRoaster(roaster: roaster)
            AppCore.sharedInstance.loggingManager.sendUserRoasters(roaster: roaster)
        }
        
        AppCore.sharedInstance.loggingManager.logRoast(roast: roast)

        
        let roastNotes = IKRoastNotes(roast: roast)
        roastNotes.firstCrackTemp = Double(roast.firstCrack?.y ?? 0)
        roastNotes.firstCrackTime = Double(roast.firstCrack?.x ?? 0)
        roastNotes.secondCrackTemp = Double(roast.secondCrack?.y ?? 0)
        roastNotes.secondCrackTime = Double(roast.secondCrack?.x ?? 0)
        roastNotes.colorChangeTemp = Double(roast.colorChange?.y ?? 0)
        roastNotes.colorChangeTime = Double(roast.colorChange?.x ?? 0)
        roastNotes.dateLastEdited = Date(timeIntervalSince1970: roast.date ?? AppCore.sharedInstance.settingsManager.internetTime().timeIntervalSince1970)
        
        
        if (roastNotes.firstCrackTime > 0) {
            if AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: (roast.profileUuid)!) != nil {
                if let roastingTime = roast.lastRoastingPoint()?.time {
                    let roastingTime_r = rounded(value: roastingTime, rank: 0)
                    let firstCrackPointTime_r = rounded(value: roastNotes.firstCrackTime, rank: 0)
                    roastNotes.dtr = Double((roastingTime_r - Float(firstCrackPointTime_r)) / roastingTime_r * 100)
                }
            }
        }
        if (saveRoastNotes(roastNotes: roastNotes, needReloadData: true)) {
            return roastNotes
        }
        return nil
    }
    
    
    func saveRoastNotes(roastNotes: IKRoastNotes, needReloadData: Bool) -> Bool {
        var roastNotesData = [String : Any]()
        roastNotesData["roastId"] = roastNotes.roast?.uuid
        roastNotesData["firstCrackTime"] = roastNotes.firstCrackTime
        roastNotesData["firstCrackTemp"] = roastNotes.firstCrackTemp
        roastNotesData["secondCrackTime"] = roastNotes.secondCrackTime
        roastNotesData["secondCrackTemp"] = roastNotes.secondCrackTemp
        roastNotesData["colorChangeTime"] = roastNotes.colorChangeTime
        roastNotesData["colorChangeTemp"] = roastNotes.colorChangeTemp
        roastNotesData["DTR"] = roastNotes.dtr
        roastNotesData["coffeeType"] = roastNotes.coffeeType
        roastNotesData["notes"] = roastNotes.notes
        roastNotesData["photo"] = roastNotes.photo
        roastNotesData["extraInfo"] = roastNotes.extraInfo
        roastNotesData["coffeeId"] = roastNotes.coffeeId
        roastNotesData["dateLastEdited"] = roastNotes.dateLastEdited
        roastNotesData["archived"] = roastNotes.archived
        
        let result = sqliteManager.saveRoastNotes(roastNotesData: roastNotesData)
        if let roast = roastNotes.roast {
            AppCore.sharedInstance.loggingManager.logRoast(roast: roast)
        }
        if needReloadData {
            updateAllRoastNotes()
        }
        return result
    }
    
    func deleteRoastNotes(roastNotes: IKRoastNotes) -> Bool {
        roastNotes.archived = true
        roastNotes.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
        //let result = sqliteManager.deleteRoastNotes(roastId: roastNotes.roast?.uuid)
        //updateAllRoastNotes()
        return saveRoastNotes(roastNotes: roastNotes, needReloadData: true)
        
    }
    
    func goToRoastNotesDetailScreen(roastNotes: IKRoastNotes) -> UIViewController {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let vc = UIStoryboard(name: "RoastNotes", bundle: nil).instantiateViewController(withIdentifier: "IKRoastNotesDetailVC") as! IKRoastNotesDetailVC
        vc.notes = roastNotes
        (appDelegate?.window?.rootViewController as? IKTabBarController)?.selectItem(index: 2)
        ((appDelegate?.window?.rootViewController as? IKTabBarController)?.selectedViewController as? UINavigationController)?.pushViewController(vc, animated: true)
        return vc
    }
    
    func goToRoastNotestScreen(roastNotes: IKRoastNotes) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let roastNotesStoryboard = UIStoryboard(name: "RoastNotes", bundle: nil)
        let vc = roastNotesStoryboard.instantiateViewController(withIdentifier: "IKRoastNotesDetailVC") as! IKRoastNotesDetailVC
        let editRoastDetailVC = roastNotesStoryboard.instantiateViewController(withIdentifier: "IKEditRoastDetailVC") as? IKEditRoastDetailVC
        vc.notes = roastNotes
        (appDelegate?.window?.rootViewController as? IKTabBarController)?.selectItem(index: 2)
        ((appDelegate?.window?.rootViewController as? IKTabBarController)?.selectedViewController as? UINavigationController)?.viewControllers.append(vc)
        ((appDelegate?.window?.rootViewController as? IKTabBarController)?.selectedViewController as? UINavigationController)?.pushViewController(editRoastDetailVC!, animated: true)
        
        editRoastDetailVC?.notesText = roastNotes.notes
        editRoastDetailVC?.changeEditState = vc.changeEditState
        editRoastDetailVC?.profileName = roastNotes.profile()?.getNameWithType() //vc.profileName.text
        
        
        if let uuid = roastNotes.roast?.uuid, uuid.count >= 3 {
            let index = uuid.index(uuid.startIndex, offsetBy: 3)
            editRoastDetailVC?.shortUuid = String(uuid[..<index])
        }
        
        
    }
    
    func parseRoastNotesArray(roastNotesDataArray: [Dictionary<String, Any>]) -> [IKRoastNotes]? {
        var result = [IKRoastNotes]()
        
        for roastNotesData in roastNotesDataArray {
            let roastNotes = IKRoastNotes(roast: nil)
            roastNotes.roast = roastManager.roastForUuid(uuid: (roastNotesData["roastId"]  as? String) ?? "")
            //roastNotes.roast?.parseRoastData()
            roastNotes.firstCrackTime = roastNotesData["firstCrackTime"] as! Double
            roastNotes.firstCrackTemp = roastNotesData["firstCrackTemp"] as! Double
            roastNotes.secondCrackTime = roastNotesData["secondCrackTime"] as! Double
            roastNotes.secondCrackTemp = roastNotesData["secondCrackTemp"] as! Double
            if (roastNotesData["colorChangeTime"] != nil) {
                roastNotes.colorChangeTime = roastNotesData["colorChangeTime"] as! Double
            }
            if (roastNotesData["colorChangeTemp"] != nil) {
                roastNotes.colorChangeTemp = roastNotesData["colorChangeTemp"] as! Double
            }
            
            roastNotes.dtr = roastNotesData["DTR"] as! Double
            roastNotes.coffeeType = (roastNotesData["coffeeType"] as? String) ?? ""
            roastNotes.notes = (roastNotesData["notes"] as? String) ?? ""
            roastNotes.photo = (roastNotesData["photo"] as? String) ?? ""
            roastNotes.extraInfo = (roastNotesData["extraInfo"] as? String) ?? ""
            roastNotes.coffeeId = (roastNotesData["coffeeId"] as? String) ?? ""
            let dateLastEditedTimeinterval = TimeInterval(roastNotesData["dateLastEdited"] as? Int64 ?? 0)
            roastNotes.dateLastEdited = Date(timeIntervalSince1970: dateLastEditedTimeinterval)
            roastNotes.archived = ((roastNotesData["archived"] as? Int64) ?? 0) > 0 ? true : false
            
            if let roast = roastNotes.roast {
                if (roastNotes.firstCrackTime > 0) {
                    roast.firstCrack = CGPoint(x: roastNotes.firstCrackTime, y: roastNotes.firstCrackTemp)
                }
                
                if (roastNotes.secondCrackTime > 0) {
                    roast.secondCrack = CGPoint(x: roastNotes.secondCrackTime, y: roastNotes.secondCrackTemp)
                }
                
                if (roastNotes.colorChangeTime > 0) {
                    roast.colorChange = CGPoint(x: roastNotes.colorChangeTime, y: roastNotes.colorChangeTemp)
                }
            }
            
            result.append(roastNotes)
        }
        return result
    }

    func updateAllRoastNotes() {
        guard let roastNotesDataArray = sqliteManager.roastNotes() else {
            return
        }
        guard let result = parseRoastNotesArray(roastNotesDataArray: roastNotesDataArray) else {
            return
        }
        roastNotesList = result
        displayedRoastNotesList = result.filter({ (roastNotes) -> Bool in
            !roastNotes.archived
        }).sorted(by: { (val1, val2) -> Bool in
            if (val1.roast != nil && val2.roast != nil) {
                return val1.roast!.date ?? 0 > val2.roast!.date ?? 0
            } else {
                return false
            }
            
        })
        NotificationCenter.default.post(name: NSNotification.Name.DidChangeRoastNotesData, object: nil)
    }
    
}


extension IKRoastNotesManager : MFMailComposeViewControllerDelegate {
    
    func exportData(notes: IKRoastNotes) {
        if MFMailComposeViewController.canSendMail() {
            if let mailComposeViewController = configuredMailComposeViewController(notes: notes) {
                let appDelegate  = UIApplication.shared.delegate as! AppDelegate
                let viewController = appDelegate.window!.rootViewController!
                viewController.present(mailComposeViewController, animated: true, completion: nil)
            }
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    
    func configuredMailComposeViewController(notes: IKRoastNotes) -> MFMailComposeViewController? {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        var mailSubject: String
        if (notes.coffeeType.count > 0) {
            mailSubject = String(format: NSLocalizedString("$share_email_subject_pro_roast_data_coffee", comment: ""), notes.coffeeType, notes.profile()?.name ?? "")
        } else {
            mailSubject = String(format: NSLocalizedString("$share_email_subject_pro_roast_data", comment: ""), notes.profile()?.name ?? "")
        }
        
        mailComposerVC.setSubject(mailSubject)
        
        let roastData = RoasterManager.instance().serializeRoasterData()!
        let profileData = notes.profile()?.getSummary()
        mailComposerVC.setMessageBody(String(format: "%@\n%@\nApp version: %@, %@\n\nwww.ikawacoffee.com\ninfo@ikawacoffee.com\n",
                                             roastData , profileData ?? "", Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! CVarArg, Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! CVarArg),
                                      isHTML: false)
        
        if let roast = notes.roast {
            roast.serializeRoastPoints()
            if let roastData = roast.roastData {
                let data = roastData.data(using: .ascii, allowLossyConversion: true)!
                let fileName = String(format: "IKAWA %@.csv", Date().dateStr3())
                mailComposerVC.addAttachmentData(data, mimeType: "text/csv", fileName: fileName)
            }
        }
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let alert = UIAlertController(title: "Error", message: "No mail editor configured on device.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
        }))
        let vc = UIApplication.shared.keyWindow?.rootViewController
        vc!.present(alert, animated: true, completion: {})
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case .cancelled, .saved:
            break
        case .sent:
            let alert = UIAlertController(title: "Success", message: "The csv data was sent successfully.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
            }))
            let vc = UIApplication.shared.keyWindow?.rootViewController
            vc!.present(alert, animated: true, completion: {})
            AppCore.sharedInstance.loggingManager.trackUIAction(action: "Roast CSV Mailed", label: "")
        case .failed:
            let alert = UIAlertController(title: "Error", message: "The message could not be send.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
            }))
            let vc = UIApplication.shared.keyWindow?.rootViewController
            vc!.present(alert, animated: true, completion: {})
        }
        controller.dismiss(animated: true, completion: nil)
        
    }
}
