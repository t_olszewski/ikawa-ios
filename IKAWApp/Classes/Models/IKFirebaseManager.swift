//
//  IKAccountManager.swift
//  IKAWApp
//
//  Created by Admin on 3/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import SDWebImage
import Alamofire

final class IKFirebaseManager: NSObject {
    
    typealias RegisterCallback = (_ success: Bool, _ message: String?) -> Void
    typealias LoginCallback = (_ success: Bool, _ message: String?) -> Void
    typealias SendDataCallback = (_ success: Bool, _ message: String?) -> Void
    typealias loadAvatarUrlCallback = (_ url: URL?, _ error: Error?) -> Void
    
    var currentUser: User?
    
    public func firebaseInit() {
        
        let filename = String(format: "GoogleService-Info-%@", Configuration.environment)
        let filePath = Bundle.main.path(forResource: filename, ofType: "plist")
        let options = FirebaseOptions(contentsOfFile:filePath!)
        FirebaseApp.configure(options: options!)
        Database.database().isPersistenceEnabled = true
        currentUser = Auth.auth().currentUser
        //Send all the existing profiles to firebase once
        if(currentUser != nil && UserDefaults.standard.bool(forKey: "send_profiles_to_firebase")) {
            for profile in AppCore.sharedInstance.profileManager.profiles! {
                profile.onServer = 0
            }
            UserDefaults.standard.set(true, forKey: "send_profiles_to_firebase")
        }
        
        
        if (currentUser != nil) {
            print("User logged in")
            if (!currentUser!.isAnonymous) {
                AppCore.sharedInstance.loggingManager.sendUser()
            }
            
            //            #if TARGET_PRO
            //            // check for backups if it's the first run
            //            if (UserDefaults.standard.bool(forKey: "checked_for_backups") == false) {
            //                UserDefaults.standard.set(true, forKey: "checked_for_backups")
            //                UserDefaults.standard.synchronize()
            //                IKProfileManager.sharedInstance.restoreUserLibrary()
            //            }
            //            #endif
            
            
        } else {
            print("User not logged in")
            //log in anonymously
            /*self.firebaseLoginAnonimous(callback: { (success, message) in
                if (!success) {
                    print("Anonymous authentication failed")
                } else {
                    print("Anonymous authentication success")
                    //                    #if TARGET_PRO
                    //                    // check for backups if it's the first run
                    //                    if (UserDefaults.standard.bool(forKey: "checked_for_backups") == false) {
                    //                        UserDefaults.standard.set(true, forKey: "checked_for_backups")
                    //                        UserDefaults.standard.synchronize()
                    //                        IKProfileManager.sharedInstance.restoreUserLibrary()
                    //                    }
                    //                    #endif
                }
            })*/
        }
        Auth.auth().useAppLanguage()
    }
    
    
    func firebaseRegisterUser(email: String, password: String, callback: @escaping RegisterCallback) {
        Auth.auth().createUser(withEmail: email.lowercased(), password: password) { (authDataResult, error) in
            if (error != nil) {
                callback(false, error?.localizedDescription)
            } else if (authDataResult != nil) {
                authDataResult!.user.sendEmailVerification(completion: { (error) in
                    if (error == nil) {
                        callback(true, "")
                    } else {
                        callback(false, error?.localizedDescription)
                    }
                })
            }
        }
    }
    
    
    func firebaseLogin(email: String, password: String, callback: @escaping LoginCallback) {
        
        Auth.auth().signIn(withEmail: email.lowercased(), password: password) { (authDataResult, error) in
            if (error != nil) {
                print("Authentication failed")
                #if TARGET_PRO
                AppCore.sharedInstance.loggingManager.trackEvent(name: "Login failed", label: (error?.localizedDescription) ?? "")
                #else
                AppCore.sharedInstance.loggingManager.trackEvent(name: "Login failed", label: (error?.localizedDescription) ?? "")
                #endif
                if error!._code == 17020 {
                    callback(false, "$network_error_login_message".localized)
                } else if error!._code == 17009  {
                    callback(false, "$incorrect_sign_in_details".localized)
                } else if error!._code == 17011 {
                    callback(false, "$password_reset_failed_user_deleted".localized)
                } else {
                    callback(false, error?.localizedDescription)
                }
                
            } else if (authDataResult != nil) {
                if (authDataResult!.user.isEmailVerified) {
                    self.currentUser = authDataResult!.user
                    #if TARGET_PRO
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_IN_SUCCESS", label: "")
                    #else
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "Login success", label: "")
                    #endif
                    //IKProfileManager.sharedInstance.uploadUserLibrary(roundTimestamp: false)
                    AppCore.sharedInstance.loggingManager.sendUser()
                    callback(true, "")
                } else {
                    self.isOldUser(callback: { (isOldUser) in
//                        if let isOldUser = isOldUser, isOldUser {
//                            self.currentUser = authDataResult!.user
//                            print("Authentication success")
//                            AppCore.sharedInstance.loggingManager.trackEvent(name: "Login success", label: "")
//                            //IKProfileManager.sharedInstance.uploadUserLibrary(roundTimestamp: false)
//                            AppCore.sharedInstance.loggingManager.sendUser()
//                            callback(true, "")
//                        } else {
                            self.firebaseLogout()
                            print("Authentication failed")
                            #if TARGET_HOME
                            AppCore.sharedInstance.loggingManager.trackEvent(name: "Login failed", label: (error?.localizedDescription) ?? "")
                            #endif
                            authDataResult!.user.sendEmailVerification(completion: nil)
                            callback(false, NSLocalizedString("$email_not_verified", comment:""))
//                        }
                    })
                }
            } else {
                callback(false, "")
            }
        }
    }
    /*
     #if TARGET_PRO
     #else
     #endif
     */
    
    func isOldUser(callback: @escaping ((_ isOldUser: Bool?)->())) {
        guard let userId = Auth.auth().currentUser?.uid else {
            callback(nil)
            return
        }
        var db = Database.database().reference()
        db = db.child(IK_FIREBASE_DATABASE)
        db.child("user").child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
            callback(snapshot.exists())
        }) { (error) in
            callback(false)
        }
    }
    
    func firebaseLoginAnonimous(callback: @escaping LoginCallback) {
        Auth.auth().signInAnonymously { (authDataResult, error) in
            if (error != nil) {
                callback(false, error?.localizedDescription)
            } else if (authDataResult != nil) {
                self.currentUser = authDataResult!.user
                callback(true, "")
            }
        }
    }
    
    func validateEmailRegEx(email: String) -> Bool {
        if email.isEmpty {
            return false
        }
        let emailRegEx = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\\]?)$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func firebaseResetPassword(email: String, callback: @escaping RegisterCallback, makeToastActivity: ()->()) {
        if validateEmailRegEx(email: email) {
            makeToastActivity()
            Auth.auth().sendPasswordReset(withEmail: email.lowercased()) { (error) in
                if (error == nil) {
                    callback(true, "")
                } else {
                    #if TARGET_HOME
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "Password reset failed", label: "")
                    #endif
                    let errorDialogView = ButtonsPopupModalView()
                    errorDialogView.layout = .vertical
                    errorDialogView.title = NSLocalizedString("$password_reset_failed", comment: "")
                    #if TARGET_PRO
                    if error!._code == 17011 {
                        errorDialogView.subtitle = "$password_reset_failed_user_deleted".localized
                    } else if error!._code == 17020 {
                        errorDialogView.subtitle = "$network_error_login_message".localized
                    } else {
                        errorDialogView.subtitle = ""
                    }
                    #else
                    errorDialogView.subtitle = ""
                    #endif
                    errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    errorDialogView.show(animated: true, dismissCallback: {
                        
                    })

                    callback(false, error?.localizedDescription)
                }
            }
        } else {
            let errorDialogView = ButtonsPopupModalView()
            errorDialogView.layout = .vertical
            errorDialogView.title = NSLocalizedString("$email_not_verified", comment: "")
            errorDialogView.subtitle = ""
            errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            errorDialogView.show(animated: true, dismissCallback: {
                
            })
        }
    }
    
    
    func firebaseLogout() {
        #if TARGET_HOME
        AppCore.sharedInstance.loggingManager.trackEvent(name: "Logout", label: "")
        #endif
        do {
            try Auth.auth().signOut()
            currentUser = nil
        } catch let error {
            // handle error here
            print("Error trying to sign out of Firebase: \(error.localizedDescription)")
        }
    }
    
    func firebaseSendData(type: String, key: String, json: String, callback: @escaping SendDataCallback) {
        if (self.currentUser == nil) {
            return
        }
        
        var objectKey = key
        objectKey = objectKey.replacingOccurrences(of: "#", with: "")
        objectKey = objectKey.replacingOccurrences(of: "$", with: "")
        objectKey = objectKey.replacingOccurrences(of: ".", with: "")
        objectKey = objectKey.replacingOccurrences(of: "[", with: "")
        objectKey = objectKey.replacingOccurrences(of: "]", with: "")
        
        var db = Database.database().reference()
        db = db.child(IK_FIREBASE_DATABASE)
        db = db.child(type)
        if (objectKey.count > 0) {
            db = db.child(objectKey)
        } else {
            db = db.childByAutoId()
        }
        
        let data = json.data(using: .utf8)
        do {
            let dictionary = try JSONSerialization.jsonObject(with: data!) as! [String:Any]
            
            db.setValue(dictionary, withCompletionBlock: { (error, ref) in
                if (error != nil) {
                    print("Error writing %@: %@", type, error?.localizedDescription ?? "")
                    callback(false, error?.localizedDescription)
                } else {
                    print("Success writing %@", type)
                    callback(true, "")
                }
            })
        } catch let error {
            print("Error creating dictionary: %@", error.localizedDescription)
        }
    }
    
    func firebaseUpdateData(type: String, key: String, json: String, callback: @escaping SendDataCallback) {
        if (self.currentUser == nil) {
            callback(false, "user not found")
            return
        }
        
        var objectKey = key
        objectKey = objectKey.replacingOccurrences(of: "#", with: "")
        objectKey = objectKey.replacingOccurrences(of: "$", with: "")
        objectKey = objectKey.replacingOccurrences(of: ".", with: "")
        objectKey = objectKey.replacingOccurrences(of: "[", with: "")
        objectKey = objectKey.replacingOccurrences(of: "]", with: "")
        
        var db = Database.database().reference()
        db = db.child(IK_FIREBASE_DATABASE)
        db = db.child(type)
        if (objectKey.count > 0) {
            db = db.child(objectKey)
        } else {
            db = db.childByAutoId()
        }
        
        let data = json.data(using: .utf8)
        do {
            let dictionary = try JSONSerialization.jsonObject(with: data!) as! [String:Any]
            
            db.updateChildValues(dictionary, withCompletionBlock: { (error, ref) in
                if (error != nil) {
                    print("Error writing %@: %@", type, error?.localizedDescription ?? "")
                    callback(false, error?.localizedDescription)
                } else {
                    print("Success writing %@", type)
                    callback(true, "")
                }
            })
        } catch let error {
            callback(false, error.localizedDescription)
            print("Error creating dictionary: %@", error.localizedDescription)
        }
    }
    
    func updateUserName(userName: String, callback: @escaping ((_ error: Error?)->Void)) {
        if let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest() {
            changeRequest.displayName = userName
            changeRequest.commitChanges { (error) in
                callback(error)
            }
        } else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "user not initialized"])
            callback(error)
        }
    }
    
    func updateEmail(email: String, callback: @escaping ((_ error: Error?)->Void)) {
        if let user = Auth.auth().currentUser {
            user.updateEmail(to: email) { (error) in
                callback(error)
            }
        } else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "user not initialized"])
            callback(error)
        }
    }
    
    func updatePassword(password: String, callback: @escaping ((_ error: Error?)->Void)) {
        if let user = Auth.auth().currentUser {
            user.updatePassword(to: password) { (error) in
                callback(error)
            }
        } else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "user not initialized"])
            callback(error)
        }
    }
    
    func uploadProfileIcon(uuid: String) {
        guard (AppCore.sharedInstance.firebaseManager.currentUser != nil) else {return}
        
        guard let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: uuid) else {
            return
        }
        let image = GetImageFromProfile(profile: profile, backgroundColor: profile.archived ? kColorLightGrey3 : nil)
        
        //scale the image to 55x55
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 55/scale, height: 55/scale), false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: 55/scale, height: 55/scale))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        let imageData = scaledImage!.pngData()
        let path = String(format: "%@/profile_images/%@.png", IK_FIREBASE_DATABASE, uuid)
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: IK_FIREBASE_STORAGE)
        let spaceRef = storageRef.child(path)
        
        let newMetadata = StorageMetadata()
        newMetadata.contentType = "image/png"
        spaceRef.putData(imageData!, metadata: newMetadata) { (metadata, error) in
            if (error != nil) {
                print(String(format: "Error uploading image: %@", (error?.localizedDescription)!))
            } else {
                print("Success uploading image")
            }
        }
    }
    
    func deleteAvatarImage(callback:  @escaping SendDataCallback) {
        guard let userId = AppCore.sharedInstance.firebaseManager.currentUser?.uid else {
            callback(false, "$user_is_not_authorised")
            return
        }
        let path = String(format: "%@/avatars/%@", IK_FIREBASE_DATABASE, userId)
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: IK_FIREBASE_STORAGE)
        let spaceRef = storageRef.child(path)
        spaceRef.delete(completion: { (error) in
            if (error == nil) {
                CasheManager.sharedInstance.clearAvatarImage {}
                callback(true, "")
            } else {
                callback(false, "$failed_to_delete_the_avatar_image".localized)
            }
        })
    }
    
    func uploadAvatarImage(image: UIImage, callback: @escaping SendDataCallback) {
        guard let userId = AppCore.sharedInstance.firebaseManager.currentUser?.uid else {
            callback(false, "user is not authorised")
            return
        }
        
        //scale the image to 240x240
        let scale = UIScreen.main.scale
        
        if image.size.height > image.size.width {
            let sizeCoef = image.size.height / image.size.width
            UIGraphicsBeginImageContextWithOptions(CGSize(width: 240/scale, height: 240/scale), false, 0.0)
            image.draw(in: CGRect(x: 0, y: -((240/scale) * sizeCoef - 240/scale)/2, width: 240/scale, height: (240/scale) * sizeCoef))
        } else {
            let sizeCoef = image.size.width / image.size.height
            UIGraphicsBeginImageContextWithOptions(CGSize(width: 240/scale, height: 240/scale), false, 0.0)
            image.draw(in: CGRect(x: -((240/scale) * sizeCoef - 240/scale)/2, y: 0, width: (240/scale) * sizeCoef, height: 240/scale))
        }
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CasheManager.sharedInstance.setAvatarImage(scaledImage!, callback: nil)
        let imageData = scaledImage!.pngData()
        let path = String(format: "%@/avatars/%@", IK_FIREBASE_DATABASE, userId)
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: IK_FIREBASE_STORAGE)
        let spaceRef = storageRef.child(path)
        
        let newMetadata = StorageMetadata()
        newMetadata.contentType = "image/png"
        spaceRef.putData(imageData!, metadata: newMetadata) { (metadata, error) in
            if (error != nil) {
                print(String(format: "Error uploading image: %@", (error?.localizedDescription)!))
                callback(false, String(format: "Error uploading image: %@", (error?.localizedDescription)!))
            } else {
                print("Success uploading image")
                callback(true, "Success uploading image")
            }
        }
    }
    
    
    
    func avatarUrlReference(userId: String) -> StorageReference? {
        let path = String(format: "%@/avatars/%@", IK_FIREBASE_DATABASE, userId)
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: IK_FIREBASE_STORAGE)
        let spaceRef = storageRef.child(path)
        return spaceRef
    }

    
    func uploadRoastImage(image: UIImage, fileName: String, callback: @escaping SendDataCallback) {
        let compressedImage = image.compressImage()
        let imageData = compressedImage.pngData()
        let storageRef = roastLogPhotoUrlReference(roasatId: fileName)
        let newMetadata = StorageMetadata()
        newMetadata.contentType = "image/png"
        let uploadTask = storageRef?.putData(imageData!, metadata: newMetadata) { (metadata, error) in
            if (error != nil) {
                print(String(format: "Error uploading image: %@", (error?.localizedDescription)!))
                callback(false, String(format: "Error uploading image: %@", (error?.localizedDescription)!))
            } else {
                print("Success uploading image")
                callback(true, "Success uploading image")
            }
        }
        DispatchQueue.main.async {
            uploadTask?.withTimout(time: 60, block: {})
        }
        
    }
    
    func deleteRoastImage(roasatId: String, callback:  @escaping SendDataCallback) {
        if let spaceRef = roastLogPhotoUrlReference(roasatId: roasatId) {
            spaceRef.delete(completion: { (error) in
                if (error == nil) {
                    callback(true, "")
                } else {
                    callback(false, "$failed_to_delete_image".localized)
                }
            })
        }
    }
    
    func roastLogPhotoUrlReference(roasatId: String) -> StorageReference? {
        let path = String(format: "%@/roasts_photos/%@", IK_FIREBASE_DATABASE, roasatId)
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: IK_FIREBASE_STORAGE)
        let spaceRef = storageRef.child(path)
        return spaceRef
    }

//    func roastLogPhotoUrl(roastId: String) -> StorageReference? {
//        let path = String(format: "%@/roasts_photos/%@", IK_FIREBASE_DATABASE, roastId)
//        let storage = Storage.storage()
//        let storageRef = storage.reference(forURL: IK_FIREBASE_STORAGE)
//        let spaceRef = storageRef.child(path)
//        return spaceRef
//    }
    
    func roastLogPhotoUrl(roastId: String, completionHandler: @escaping (_ url: URL?) -> Void) {
        // Create a reference to the file you want to download
        guard let roastLogRef = roastLogPhotoUrlReference(roasatId: roastId) else {
            completionHandler(nil)
            return
        }
        // Fetch the download URL
        roastLogRef.downloadURL { url, error in
            if error != nil {
                completionHandler(nil)
            } else {
                completionHandler(url)
            }
        }
    }
    
    
    
    func avatarUrl(userId: String, completionHandler: @escaping (_ url: URL?) -> Void) {
        // Create a reference to the file you want to download
        guard let avatarRef = avatarUrlReference(userId: userId) else {
            completionHandler(nil)
            return
        }
        // Fetch the download URL
        avatarRef.downloadURL { url, error in
            if error != nil {
                completionHandler(nil)
            } else {
                completionHandler(url)
            }
        }
    }
        
    
    func uploadRoastCsv(csv: String, uuid: String, profile: String, roaster: String) {
        if (AppCore.sharedInstance.firebaseManager.currentUser == nil) {
            return
        }
        
        let filename = "\(uuid).csv"
        let path = String(format: "%@/roasts/%@", IK_FIREBASE_DATABASE, filename)
        
        let csvData = csv.data(using: .utf8)
        
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: IK_FIREBASE_STORAGE)
        let spaceRef = storageRef.child(path)
        
        let newMetadata = StorageMetadata()
        newMetadata.customMetadata = ["user": AppCore.sharedInstance.firebaseManager.currentUser?.uid,
                                      "profile": profile,
                                      "roaster": roaster] as? [String : String]
        newMetadata.contentType = "text/csv"
        spaceRef.putData(csvData!, metadata: newMetadata) { (metadata, error) in
            if (error != nil) {
                print(String(format: "Error uploading roast: %@", (error?.localizedDescription)!))
            } else {
                print("Success uploading roast")
                _ = AppCore.sharedInstance.sqliteManager.markCsvOnServer(roastUdid: uuid)
            }
        }
        
    }
    
    // MARK: - Tracking
    func trackUIAction(action: String, label: String) {
        guard !AppCore.sharedInstance.settingsManager.isTrackingDisabled() else {
            return
        }
        #if TARGET_PRO
        
        #else
        var act = action.lowercased()
        act = act.replacingOccurrences(of: "-", with: "_")
        act = act.replacingOccurrences(of: ":", with: "_")
        act = act.replacingOccurrences(of: " ", with: "_")
        #endif
        Analytics.logEvent(action, parameters: ["label" : label])
    }
    
    func trackScreen(screenName: String) {
        guard !AppCore.sharedInstance.settingsManager.isTrackingDisabled() else {
            return
        }
        #if TARGET_PRO
        Analytics.logEvent(screenName, parameters: ["label" : "screen"])
        #else
        var name = screenName.lowercased()
        name = name.replacingOccurrences(of: "-", with: "_")
        name = name.replacingOccurrences(of: " ", with: "_")
        Analytics.setScreenName(name, screenClass: name)
        #endif
    }
    
}

extension StorageUploadTask {
    func withTimout(time: TimeInterval, block: @escaping () -> Void) {
        Timer.scheduledTimer(timeInterval: time, target: self, selector: #selector(timeOut), userInfo: ["callback" : block], repeats: false)
    }
    
    
    @objc func timeOut(timer: Timer) {
        let userInfo = timer.userInfo as? Dictionary<String, AnyObject>
        if let block = userInfo?["callback"] as? () -> Void {
            self.cancel()
            block()
        }
    }
}
