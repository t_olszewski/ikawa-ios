//
//  IKCropsterMachine.swift
//  IKAWA-Pro
//
//  Created by Admin on 9/25/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class IKCropsterMachine: NSObject, Decodable {
    var id: String?
    var type :String?
    var name: String?
    enum CodingKeys: String, CodingKey {
        case id
        case type
        case attributes
        
    }
    
    enum AttributesCodingKeys: String, CodingKey {
        case name
    }
    

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try container.decode(String.self, forKey: .id)
        self.type = try? container.decode(String.self, forKey: .type)
       
        // Nested
        let attributesContainer = try container.nestedContainer(keyedBy: AttributesCodingKeys.self, forKey: .attributes)
        self.name = try attributesContainer.decode(String.self, forKey: .name)
    }
    
}
