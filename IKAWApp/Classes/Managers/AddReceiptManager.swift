//
//  AddReceiptHelper.swift
//  IKAWA-Home
//
//  Created by Admin on 11/5/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import QRCodeReader
import AVFoundation
import IKRoasterLib

class AddReceiptManager: NSObject {
    static let sharedInstance = AddReceiptManager()
    
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    func showAddRecepieDialogView() {
        let addRecepieDialogView = ButtonsPopupModalView()
        addRecepieDialogView.layout = .vertical
        addRecepieDialogView.title = ""
        addRecepieDialogView.subtitle = NSLocalizedString("$modal_add_new_recipe", comment: "")
        addRecepieDialogView.setUpButton(index: .First, title: NSLocalizedString("$create_new_recipe", comment: ""), type: .Bordered) {
            self.showCreateNewRecipeDialogView(recipeName: nil, title: nil)
        }
        
        addRecepieDialogView.setUpButton(index: .Second, title: NSLocalizedString("$scan_qr", comment: ""), type: .Filled) {
            self.showScanner()
        }
        
        addRecepieDialogView.setUpButton(index: .Third, title: NSLocalizedString("$cancel", comment: ""), type: .Cancel) {}
        IKNotificationManager.sharedInstance.show(view: addRecepieDialogView)
    }
    
    
    func showScanner() {
        // Retrieve the QRCode content
        // By using the delegate pattern
        readerVC.delegate = self
        
        // Or by using the closure pattern
//        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
//            print(result)
//        }
        
        // Presents the readerVC as modal form sheet
        readerVC.modalPresentationStyle = .formSheet
        let vc = UIApplication.shared.keyWindow?.rootViewController
        vc!.present(readerVC, animated: true, completion: nil)
        AppCore.sharedInstance.loggingManager.trackUIAction(action: "open qr scanner", label: "")
    }
    
    func showCreateNewRecipeDialogView(recipeName: String?, title: String?) {
        let createNewRecipeDialogView = CreateRecipeModalView()
        if let title = title {
            createNewRecipeDialogView.titleLabel.text = title
        }
        createNewRecipeDialogView.recipeNameTextField.text = recipeName ?? ""
        createNewRecipeDialogView.cancelButtonCallback = {}
        createNewRecipeDialogView.saveButtonCallback = {(recipeName) in
            if let profile = AppCore.sharedInstance.profileManager.profileForName(name: recipeName) {
                if (profile.showInLibrary || profile.archived) {
                    let formatStr = NSLocalizedString("$profile_name_exists", comment: "").replacingOccurrences(of: "%s", with: "%@")
                    self.showCreateNewRecipeDialogView(recipeName: recipeName, title: String(format: formatStr, recipeName))
                    return
                }
            }

            guard let profile = AppCore.sharedInstance.profileManager.base64DecodeProfile(profile: "CAESEAYxlu2m7kKUlzoLAlRWI9caGk11bmlueWEgSGlsbApCdXJ1bmRpIHwgNTBnIgUIABD0AyIGCLkDEMgQIgYInwgQxRMiBgiPDRCNEyIGCJgREJISIgYInBwQ6RIqBQgAEMYBKgYInBwQogEwAToGCMkjEMwB") else {
                return
            }
            
            profile.uuid = AppCore.sharedInstance.profileManager.getRandomUuid()
            profile.type = ProfileTypeUser
            if let userId = AppCore.sharedInstance.firebaseManager.currentUser?.uid {
                profile.userId = userId
            }
            profile.name = recipeName
            self.createProfile(profile: profile, editMode: true)
        }
        IKNotificationManager.sharedInstance.show(view: createNewRecipeDialogView)
    }
    
    func createProfile(profile: Profile, editMode: Bool) {
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        if (appDelegate.window!.rootViewController as? IKTabBarController) == nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: { [weak self] in
                self?.createProfile(profile: profile, editMode: editMode)
            })
        } else {
            let msg = NSLocalizedString("$profile_name_exists", comment: "").replacingOccurrences(of: "%s", with: profile.name)
            AppCore.sharedInstance.profileManager.saveProfile(profile: profile, existsText: msg) { (saveState, profile) in
                profile?.shouldOpenInEditMode = editMode
                switch saveState {
                case .ALREADY_EXISTS:
                    profile?.showInLibrary = true
                    _ = AppCore.sharedInstance.sqliteManager.updateProfile(profile: profile!)
                    self.openProfile(profile: profile!)
                case .SAVED, .SAVED_AS, .OVERWRITTEN:
                    self.openProfile(profile: profile!)
                case .FAILED, .CANCELED:
                    break
                case .UNSUPPORTED:
                    let confirmationDialogView = ButtonsPopupModalView()
                    confirmationDialogView.layout = .vertical
                    confirmationDialogView.title = NSLocalizedString("$profile_unsupported", comment: "")
                    #if TARGET_PRO
                    confirmationDialogView.subtitle = NSLocalizedString("$inlet_sensor_profile_error_message", comment: "")
                    #else
                    confirmationDialogView.subtitle = NSLocalizedString("$use_ikawa_pro", comment: "")
                    #endif
                    confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                }
            }
        }
    }
    
    func openProfile(profile: Profile) {
        NavigationHelper.openProfile(profile: profile)
    }
    
}

extension AddReceiptManager: QRCodeReaderViewControllerDelegate {
    // MARK: - QRCodeReaderViewController Delegate Methods
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        if let url = URL(string: result.value) {
            if (url.absoluteString.range(of: "/profile") != nil) {
                if let query = url.query {
                    if (query.count == 32) {
                        //query contains uuid
                        //TODO: implement!
                    } else {
                        
                        if let profile = AppCore.sharedInstance.profileManager.base64DecodeProfile(profile: query)  {
                            if profile.type == ProfileTypeUndefined {
                                profile.type = ProfileTypeIkawa
                            }
                            createProfile(profile: profile, editMode: false)
                        } else {
                            self.showWrongQRCodeAlert(title: NSLocalizedString("$profile_format_unsupported", comment: ""))
                            return
                        }
                    }
                }
            } else {
                self.showWrongQRCodeAlert(title: NSLocalizedString("$invalid_QR_code", comment: ""))
            }
        } else {
            self.showWrongQRCodeAlert(title: NSLocalizedString("$profile_format_unsupported", comment: ""))
        }
        
        readerVC.dismiss(animated: true, completion: nil)
    }
    
    func showWrongQRCodeAlert(title: String) {
        let confirmationDialogView = ButtonsPopupModalView()
        confirmationDialogView.layout = .vertical
        confirmationDialogView.title = title
        confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
        IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
        return
    }
    
    //This is an optional delegate method, that allows you to be notified when the user switches the cameraName
    //By pressing on the switch camera button
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        let cameraName = newCaptureDevice.device.localizedName
            print("Switching capturing to: \(cameraName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        readerVC.dismiss(animated: true, completion: nil)
    }
}

