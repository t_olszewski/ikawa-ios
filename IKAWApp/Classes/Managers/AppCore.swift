//
//  AppCore.swift
//  IKAWApp
//
//  Created by Admin on 9/17/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import IKRoasterLib

class AppCore: NSObject {
    static let sharedInstance = AppCore()
    
    var sqliteManager: IKSqliteManager
    var profileManager: IKProfileManager
    #if TARGET_PRO
    var cropsterManager: CropsterManager
    var roastNotesManager: IKRoastNotesManager
    #endif
    var settingsManager: IKSettingsManager
    var firebaseManager: IKFirebaseManager
    var roastManager: IKRoastManager
    var machineErrorNotificationHandler: IKMachineErrorNotificationHandler
    var loggingManager: IKLoggingManager
    var syncManager: SyncManager
    
    
    override init() {
        loggingManager = IKLoggingManager()
        sqliteManager = IKSqliteManager(dbName: "ikawa.sqlite")
        profileManager = IKProfileManager(sqliteManager: sqliteManager)
        roastManager = IKRoastManager(sqliteManager: sqliteManager)
        RoasterManager.instance().delegates.add(roastManager)
        #if TARGET_PRO
        cropsterManager = CropsterManager()
        roastNotesManager = IKRoastNotesManager(sqliteManager: sqliteManager, roastManager: roastManager)
        #endif
        settingsManager = IKSettingsManager()
        firebaseManager = IKFirebaseManager()
        machineErrorNotificationHandler = IKMachineErrorNotificationHandler()
        syncManager = SyncManager()
        super.init()
        //self.createFakeProfiles(count: 900)
//        self.createFakeRoastNotres(count: 100)
//        sqliteManager.deleteAllRoastNotes()
//        sqliteManager.deleteAllRoastLogs()
        
    }
    
    func logout() {
        settingsManager.workingWithoutAccount = false
        UserDefaults.standard.removeObject(forKey: "tutorialWasShowed")
        UserDefaults.standard.synchronize()
        AppCore.sharedInstance.firebaseManager.firebaseLogout()
        AppCore.sharedInstance.cropsterManager.logOut()
        clearTempFolder()
        profileManager.reloadData()
        roastNotesManager.updateAllRoastNotes()
        if let appDelegate  = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.unregisterPushNotifications()
        }
        NotificationCenter.default.post(name: Notification.Name.DidLogOut, object: nil)
        AppCore.sharedInstance.syncManager.onLogout()
    }
    
    
    
    func createFakeProfiles(count: Int) {
        if let profile = profileManager.profiles?.first {
            for counter in 0...count {
                profile.uuid = profileManager.getRandomUuid()
                profile.type = ProfileTypeUser
                profile.name = "\(counter)"
                profile.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
                sqliteManager.addProfile(profile: profile)
                print("created \(counter) profile from \(count)")
            }
        }
    }
    #if TARGET_PRO
    func createFakeRoastNotres(count: Int) {
        if let roastNotes = roastNotesManager.roastNotesList?.first, let roast = roastNotes.roast {
            for counter in 0...count {
                roast.uuid = UUID().uuidString.replacingOccurrences(of: "-", with: "") + "\(counter)"
                roastNotes.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
                if roastManager.saveRoast(roast: roast) {
                    _ = roastNotesManager.saveRoastNotes(roastNotes: roastNotes, needReloadData: false)
                    print("created \(counter) profile from \(count)")
                }
            }
        }
    }
    #endif

}
