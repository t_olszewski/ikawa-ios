//
//  CasheManager.swift
//  IKAWApp
//
//  Created by Admin on 10/25/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import SDWebImage
import Alamofire

class CasheManager {
    static let sharedInstance = CasheManager()
    
    //MARK: - Avatar
    func getAvatarImage( callback: @escaping ((_ image : UIImage?,_ error: Error?)->()) ) {
        var isCashe = false
        guard let userId = AppCore.sharedInstance.firebaseManager.currentUser?.uid else {
            callback(nil, nil)
            return
        }
        
        #if TARGET_PRO
        if let image = AppCore.sharedInstance.syncManager.localImage(folder: "avatars", fileName: userId) {
            isCashe = true
            callback(image, nil)
        }
        #endif
        
        if let cashedImage = SDImageCache.shared.imageFromCache(forKey: userId) {
            isCashe = true
            callback(cashedImage, nil)
        }
        
        if NetworkReachabilityManager()?.isReachable ?? false {
            AppCore.sharedInstance.firebaseManager.avatarUrl(userId: userId) { (url) in
                if let url = url {
                    SDWebImageDownloader.shared.downloadImage(with: url, options: [], progress: nil) { (image, data, error, success) in
                        SDImageCache.shared.store(image, forKey: userId) {
                            
                        }
                        callback(image, error)
                    }
                } else {
                    if !isCashe {
                        callback(nil, nil)
                    }
                }
            }
        } else {
            if !isCashe {
                callback(nil, nil)
            }
        }

    }
    
    func setAvatarImage(_ image: UIImage, callback: (()->())?) {
        guard let userId = AppCore.sharedInstance.firebaseManager.currentUser?.uid else {
            callback?()
            return
        }
        SDImageCache.shared.store(image, forKey: userId) {
            callback?()
        }
    }
    
    func clearAvatarImage(callback: @escaping (()->())) {
        guard let userId = AppCore.sharedInstance.firebaseManager.currentUser?.uid else {
            callback()
            return
        }
        SDImageCache.shared.removeImage(forKey: userId) {
            callback()
        }
    }
    
    //MARK: - RoastLog Image
    func getRoastLogImage(_ roastId: String, callback: @escaping ((_ image : UIImage?,_ error: Error?)->()) ) {
        var isCashe = false
        let key = roastId
        

        if let image = AppCore.sharedInstance.syncManager.localImage(folder: "roastLogImages", fileName: key) {
            isCashe = true
            callback(image.compressImage(), nil)
        }
        
        if let cashedImage = SDImageCache.shared.imageFromCache(forKey: key) {
            isCashe = true
            callback(cashedImage, nil)
        }

        if NetworkReachabilityManager()?.isReachable ?? false {
            AppCore.sharedInstance.firebaseManager.roastLogPhotoUrl(roastId: roastId) { (url) in
                if let url = url {
                    SDWebImageDownloader.shared.downloadImage(with: url, options: [], progress: nil) { (image, data, error, success) in
                        SDImageCache.shared.store(image, forKey: key) {
                            
                        }
                        callback(image, error)
                    }
                } else {
                    callback(nil, nil)
                }
            }
        } else {
            if !isCashe {
                callback(nil, nil)
            }
        }

    }
    
    func setRoastLogImage(_ roastId: String, _ image: UIImage, callback: (()->())?) {
        
        let imageData = image.pngData()
        DispatchQueue.main.async {
            let image = UIImage(data: imageData!)
            SDImageCache.shared.store(image, forKey: roastId) {
                callback?()
            }
        }
        
        
    }
    
    func clearRoastLogImage(_ roastId: String, callback: @escaping (()->())) {
        let key = roastId
        SDImageCache.shared.removeImage(forKey: key) {
            callback()
        }
    }
}
