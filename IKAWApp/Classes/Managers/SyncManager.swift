//
//  SyncManager.swift
//  IKAWApp
//
//  Created by Admin on 9/16/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Alamofire
import FirebaseStorage
import Toast_Swift
import Alamofire

class SyncManager: NSObject {
    var dbSynchronizationTimer: Timer?
    var recentBackupDBQuery: DatabaseQuery?
    var userDBQuery: DatabaseQuery?
    private let kLastBackupDate = "LastBackupDate.ikawa.com"
    var dateMergedBackup: Date?
    var isBackupUpploading = false
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    var isSyncProcess = false
    var userInfo: IKUserInfo?
    
    override init() {
        super.init()
        self.startNetworkReachabilityObserver()
    }
    
    func startNetworkReachabilityObserver() {

        reachabilityManager?.listener = { [unowned self] status in
            switch status {

                case .notReachable:
                    print("The network is not reachable")

                case .unknown :
                    print("It is unknown whether the network is reachable")

                case .reachable(.ethernetOrWiFi):
                    print("The network is reachable over the WiFi connection")
                    self.synchronizeRoastLogImages()
                    self.synchronizeAvatarImage()

                case .reachable(.wwan):
                    print("The network is reachable over the WWAN connection")
                    self.synchronizeRoastLogImages()
                    self.synchronizeAvatarImage()
                }
            }

            // start listening
            reachabilityManager?.startListening()
    }
    
    func onLogout() {
        stopAccountSynchronization()
        dateMergedBackup = nil
        UserDefaults.standard.removeObject(forKey: kLastBackupDate)
        UserDefaults.standard.synchronize()
    }
    
    func stopAccountSynchronization() {
        self.userInfo = nil
        userDBQuery?.removeAllObservers()
    }
    
    func getUserInfo(callback:@escaping (_ userInfo: IKUserInfo?)->()) -> () {
        if let userInfo = self.userInfo {
            callback(userInfo)
        } else {
            startAccountSynchronization {(userInfo) in
                callback(userInfo)
            }
        }

    }
    
    func startAccountSynchronization(callback:@escaping (_ userInfo: IKUserInfo?)->())  {
        guard let currentUser = AppCore.sharedInstance.firebaseManager.currentUser else {
            callback(nil)
            return
        }
        
        var isCallBackCalled = false
        
        var db = Database.database().reference()
        db = db.child(IK_FIREBASE_DATABASE)
        userDBQuery = db.child("user_details").child(currentUser.uid)
        userDBQuery?.observe(.value, with: { (snapshot) in
            
            // Get user value
            if let userDict = snapshot.value as? NSDictionary {
                #if TARGET_PRO
                if let cropster = userDict["cropster"] as? NSDictionary {
                    if let group = cropster["group"] as? String, let secret = cropster["secret"] as? String, let key = cropster["key"] as? String, let email = cropster["email"] as? String {
                        AppCore.sharedInstance.cropsterManager.group = group
                        AppCore.sharedInstance.cropsterManager.secret = secret
                        AppCore.sharedInstance.cropsterManager.key = key
                        AppCore.sharedInstance.cropsterManager.email = email
                        AppCore.sharedInstance.cropsterManager.isInitialized = true
                    }
                }
                #endif
                self.userInfo = IKUserInfo()
                self.userInfo?.accountName = userDict["account_name"] as? String ?? ""
                self.userInfo?.userType = userDict["user_type"] as? Int ?? 0
                NotificationCenter.default.post(name: Notification.Name.DidChangeUserData, object: nil)
            }
            if !isCallBackCalled {
                callback(self.userInfo)
                isCallBackCalled = true
            }
        })
    }
    
    
    func startProfileSynchronization()  {
        if (AppCore.sharedInstance.firebaseManager.currentUser == nil) {
            return
        }
        
        dbSynchronizationTimer?.invalidate()
        dbSynchronizationTimer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(uploadBackupIfNeeded), userInfo: nil, repeats: true)
        
        var db = Database.database().reference()
        db = db.child(IK_FIREBASE_DATABASE)
        recentBackupDBQuery = db.child("recent_device_backups").queryOrdered(byChild: "user_id").queryEqual(toValue: AppCore.sharedInstance.firebaseManager.currentUser!.uid).queryLimited(toLast: 100)
        
        recentBackupDBQuery?.observe(.value, with: { (snapshot) in
            guard self.dbSynchronizationTimer != nil else {
                return
            }
            let childrens = snapshot.children.allObjects.sorted(by: { (first , second) -> Bool in
                if let firstValue = (first as! DataSnapshot).value, let secondValue = (second as! DataSnapshot).value {
                    let dictFirst = firstValue  as! Dictionary<String, AnyObject>
                    let timestampFirst = dictFirst["timestamp"] as! NSNumber
                    
                    let dictSecond = secondValue  as! Dictionary<String, AnyObject>
                    let timestampSecond = dictSecond["timestamp"] as! NSNumber
                    return timestampFirst.doubleValue < timestampSecond.doubleValue
                }
                return true
            })
            
            if childrens.count > 0 {
                if let dict = ((childrens.last as? DataSnapshot)?.value as? Dictionary<String, AnyObject>) {
                    if let timestamp = dict["timestamp"] as? NSNumber {
                        let backupDate = Date(timeIntervalSince1970: timestamp.doubleValue)
                        if let lastBackupDate = UserDefaults.standard.object(forKey: self.kLastBackupDate) as? Date, lastBackupDate >= backupDate || backupDate == self.dateMergedBackup {
                            
                        } else {
                            //need update from backup
                            self.dateMergedBackup = backupDate
                            if let path = dict["path"] as? String {
                                
                                if IK_SHOW_DEBUG_INFORMATIOMN {
                                    let confirmationDialogView = ButtonsPopupModalView()
                                    confirmationDialogView.layout = .vertical
                                    confirmationDialogView.title = (dict["device_type"] as? String) ?? ""
                                    confirmationDialogView.subtitle = backupDate.dateStr()
                                    confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Filled) {}
                                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                                }

                                self.synchronizeWithBackup(path: path, callback: { (error) in
                                    UserDefaults.standard.set(Date(timeIntervalSince1970: timestamp.doubleValue), forKey: self.kLastBackupDate)
                                })
                            }
                            
                        }
                    }
                }
            }
            
        })
    }
    
    func stopSynchronization() {
        recentBackupDBQuery?.removeAllObservers()
        dbSynchronizationTimer?.invalidate()
        dbSynchronizationTimer = nil
        
    }
    
    func migrateRoastLogImages() {
        let fileManager = FileManager.default
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let roasterImagesFolderPath = documentsPath + "/roastLogImages"
        if !fileManager.fileExists(atPath: roasterImagesFolderPath) {
            do {
                try fileManager.createDirectory(atPath: roasterImagesFolderPath, withIntermediateDirectories: true, attributes: nil)
                if let roastNotesData = AppCore.sharedInstance.sqliteManager.roastNotes() {
                    
                    for noteData in roastNotesData {
                        if let photo = noteData["photo"] as? String, !photo.isEmpty {
                            
                            let filePath = documentsPath + "/" + photo
                            if fileManager.fileExists(atPath: filePath) {
                                
                                do {
                                    try fileManager.copyItem(atPath: filePath, toPath: documentsPath + "/roastLogImages/" + photo)
                                    try fileManager.removeItem(atPath: filePath)
                                }
                                
                                catch let error {
                                print("error migration roastlog image \(error.localizedDescription)")
                                }
                                

                            }
                        }
                    }
                }
            } catch let error {
                print("error migration roastlog image \(error.localizedDescription)")
            }
        }
    }
    
    func synchronizeRoastLogImages()  {
        DispatchQueue.global().async {
                    if NetworkReachabilityManager()?.isReachable ?? false, let user  = AppCore.sharedInstance.firebaseManager.currentUser, !user.isAnonymous {
                        let fileManager = FileManager.default
                        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                        let roastLogImagesPath = documentsPath + "/roastLogImages"
                        if let filePaths = try? fileManager.contentsOfDirectory(atPath: roastLogImagesPath) {
                            let group = DispatchGroup()
                            for filePath in filePaths {
                                if let fileName = NSURL(fileURLWithPath: filePath).lastPathComponent {
                                    if let image = UIImage(contentsOfFile:documentsPath + "/roastLogImages/" + filePath) {
                                        group.enter()
                                        autoreleasepool {
                                        AppCore.sharedInstance.firebaseManager.uploadRoastImage(image: image, fileName: fileName) { (success, message) in
                                            group.leave()
                                            if success {
                                                CasheManager.sharedInstance.setRoastLogImage(fileName, image.compressImage(), callback: nil)
                                                do {
                                                    try fileManager.removeItem(atPath: documentsPath + "/roastLogImages/" + filePath)
                                                    print("roast log image: " + fileName + " was uploaded")
                                                } catch {
                                                    print("Could not clear temp folder: \(error)")
                                                }
                                            }
                                        }
                                        }
                                        group.wait()
                                    }
                                }
                                
                            }
                        }
                    }
        }
    }
    
    func synchronizeAvatarImage()  {
        if NetworkReachabilityManager()?.isReachable ?? false, let user  = AppCore.sharedInstance.firebaseManager.currentUser, !user.isAnonymous {
            let fileManager = FileManager.default
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let imagesPath = documentsPath + "/avatars"
            if let filePaths = try? fileManager.contentsOfDirectory(atPath: imagesPath) {
                for filePath in filePaths {
                    if let fileNme = NSURL(fileURLWithPath: filePath).lastPathComponent, let roastId = fileNme.split(separator: ".").first {
                        if let image = UIImage(contentsOfFile:documentsPath + "/avatars/" + filePath) {
                            let roastIdStr = String(roastId)
                            AppCore.sharedInstance.firebaseManager.uploadAvatarImage(image: image) { (success, message) in
                                if success {
                                    do {
                                           try fileManager.removeItem(atPath: documentsPath + "/avatars/" + filePath)
                                        print("avatar image: " + roastIdStr + " was uploaded")
                                       } catch {
                                           print("Could not clear temp folder: \(error)")
                                       }
                                }
                            }
                        }
                    }

                }
            }
        }
    }
    
    
    @objc func uploadBackupIfNeeded() {
        guard NetworkReachabilityManager()?.isReachable ?? false else {
            return
        }
        if UserDefaults.standard.bool(forKey: "db_changed") && !isBackupUpploading {
            isBackupUpploading = true
            uploadUserLibrary(roundTimestamp: false) { (error) in
                self.isBackupUpploading = false
                if let error = error {
                    let errorDialogView = ButtonsPopupModalView()
                    errorDialogView.layout = .vertical
                    errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                    errorDialogView.subtitle = error.localizedDescription
                    errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    
                    IKNotificationManager.sharedInstance.show(view: errorDialogView)
                } else {
                    self.dateMergedBackup = nil
                    UserDefaults.standard.set(false, forKey: "db_changed")
                    UserDefaults.standard.set(false, forKey: "NeedMergeWithAccount")
                }
            }
        }
    }
    
    func synchronizeWithBackup(path: String, callback: @escaping ((_ error: Error?)->())) {
        stopSynchronization()
        isSyncProcess = true
        NotificationCenter.default.post(name: NSNotification.Name.DidStartSync, object: nil)
        self.restoreBackup(path: path, needMergingWithLocalBackup: true, callback: { (error) in
            self.isSyncProcess = false
            NotificationCenter.default.post(name: NSNotification.Name.DidFinishSync, object: nil)
            self.startProfileSynchronization()
            if let error = error {
                let errorDialogView = ButtonsPopupModalView()
                errorDialogView.layout = .vertical
                errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                errorDialogView.subtitle = error.localizedDescription
                errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                IKNotificationManager.sharedInstance.show(view: errorDialogView)
            }
            callback(error)
        })
        
        /*
        let alertView = ButtonsPopupModalView()
        alertView.layout = .vertical
        alertView.title = NSLocalizedString("Recipe Library is now up to date across all your logged in devices", comment: "")
        alertView.subtitle = ""
        alertView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
            let vc = UIApplication.shared.keyWindow?.rootViewController
            vc!.view.makeToastActivity(.center)
            self.restoreBackup(path: path, needMergingWithLocalBackup: true, callback: { (error) in
                vc!.view.hideToastActivity()
                self.startProfileSynchronization()
                if let error = error {
                    let errorDialogView = ButtonsPopupModalView()
                    errorDialogView.layout = .vertical
                    errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                    errorDialogView.subtitle = error.localizedDescription
                    errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    IKNotificationManager.sharedInstance.show(view: errorDialogView)
                }
                callback(error)
            })
        }
         IKNotificationManager.sharedInstance.show(view: alertView)
 */
        /*
        if let vc = UIApplication.shared.keyWindow?.rootViewController {
            // create a new style
            var style = ToastStyle()
            style.backgroundColor = kColorLightGrey3.withAlphaComponent(0.8)
            // this is just one of many style options
            style.messageColor = .white
            
            // present the toast with the new style
            vc.view.makeToast("Recipe Library is now up to date across all your logged in devices", duration: 2.0, position: .top, style: style)

            // toggle "tap to dismiss" functionality
            ToastManager.shared.isTapToDismissEnabled = true

            // toggle queueing behavior
            ToastManager.shared.isQueueEnabled = true
            
            self.restoreBackup(path: path, needMergingWithLocalBackup: true, callback: { (error) in
//                self.startProfileSynchronization()
                if let error = error {
                    let errorDialogView = ButtonsPopupModalView()
                    errorDialogView.layout = .vertical
                    errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                    errorDialogView.subtitle = error.localizedDescription
                    errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    IKNotificationManager.sharedInstance.show(view: errorDialogView)
                }
                callback(error)
            })
            
        }

 */
    }
    
    func getLastBackups(callback:@escaping ((_ error: Error?, _ backups: [DataSnapshot]?)->())) {
        if (AppCore.sharedInstance.firebaseManager.currentUser == nil) {
            callback(nil, nil)
            return
        }
        var db = Database.database().reference()
        db = db.child(IK_FIREBASE_DATABASE)
        var backupsQuery: DatabaseQuery?
        backupsQuery = db.child("recent_device_backups").queryOrdered(byChild: "user_id").queryEqual(toValue: AppCore.sharedInstance.firebaseManager.currentUser!.uid)/*.queryLimited(toLast: 30)*/
        
        backupsQuery?.observeSingleEvent(of: .value, with: { (snapshot) in
            let childrens = snapshot.children.allObjects.sorted(by: { (first , second) -> Bool in
                if let firstValue = (first as! DataSnapshot).value, let secondValue = (second as! DataSnapshot).value {
                    let dictFirst = firstValue  as! Dictionary<String, AnyObject>
                    let timestampFirst = dictFirst["timestamp"] as! NSNumber
                    
                    let dictSecond = secondValue  as! Dictionary<String, AnyObject>
                    let timestampSecond = dictSecond["timestamp"] as! NSNumber
                    return timestampFirst.doubleValue < timestampSecond.doubleValue
                }
                return true
            })
            callback(nil, childrens as? [DataSnapshot])
        })
    }
    
    func restoreLastBackup(needMergingWithLocalBackup: Bool, callback:@escaping ((_ error: Error?)->())) -> Void {
        if (AppCore.sharedInstance.firebaseManager.currentUser == nil) {
            callback(nil)
            return
        }
        getLastBackups { (error, snapshots) in
            if (snapshots?.count ?? 0 > 0) {
                if let child = snapshots?.last, let value = child.value {
                    let dict = value  as! Dictionary<String, AnyObject>
                    let path = dict["path"] as? String
                    self.restoreBackup(path: path!, needMergingWithLocalBackup: needMergingWithLocalBackup, callback: { (error) in
                        if let error = error {
                            let errorDialogView = ButtonsPopupModalView()
                            errorDialogView.layout = .vertical
                            errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                            errorDialogView.subtitle = error.localizedDescription
                            errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                            
                            IKNotificationManager.sharedInstance.show(view: errorDialogView)
                        } else {
                            if let timestamp = dict["timestamp"] as? NSNumber {
                                UserDefaults.standard.set(Date(timeIntervalSince1970: timestamp.doubleValue), forKey: self.kLastBackupDate)
                            }
                            callback(error)
                        }
                    })
                } else {
                    callback(nil)
                }
                
            } else {
                self.copyBackupsToRecentBackups(callback: { (success) in
                    if success {
                        self.restoreLastBackup(needMergingWithLocalBackup: needMergingWithLocalBackup, callback: { (error) in
                            callback(error)
                        })
                    } else {
                        callback(nil)
                    }
                })
            }
        }
    }
    
    func copyBackupsToRecentBackups(callback:@escaping ((_ success: Bool)->())) {
        var db = Database.database().reference()
        db = db.child(IK_FIREBASE_DATABASE)
        var backupsQuery: DatabaseQuery?
        backupsQuery = db.child("device_backups").queryOrdered(byChild: "user_id").queryEqual(toValue: AppCore.sharedInstance.firebaseManager.currentUser!.uid)
        
        backupsQuery?.observeSingleEvent(of: .value, with: { (snapshot) in
            if (snapshot.childrenCount > 0) {
                guard var childrens = snapshot.children.allObjects.sorted(by: { (first , second) -> Bool in
                    if let firstValue = (first as! DataSnapshot).value, let secondValue = (second as! DataSnapshot).value {
                        let dictFirst = firstValue  as! Dictionary<String, AnyObject>
                        let timestampFirst = dictFirst["timestamp"] as! NSNumber
                        
                        let dictSecond = secondValue  as! Dictionary<String, AnyObject>
                        let timestampSecond = dictSecond["timestamp"] as! NSNumber
                        return timestampFirst.doubleValue < timestampSecond.doubleValue
                    }
                    return true
                }) as? [DataSnapshot] else {
                    callback(true)
                    return
                }
                
                var resultArray  = [DataSnapshot]()
                for children in childrens {
                    if resultArray.isEmpty {
                        resultArray.append(children)
                    } else {
                        if let fistVal = resultArray.last?.value as? Dictionary<String, AnyObject>, let secondVal = children.value as? Dictionary<String, AnyObject>{
                            if let firstTimestasmp = fistVal["timestamp"] as? NSNumber, let secondTimestamp = secondVal["timestamp"] as? NSNumber {
                                let firstDate = Date(timeIntervalSince1970: firstTimestasmp.doubleValue)
                                let secondDate = Date(timeIntervalSince1970: secondTimestamp.doubleValue)
                                if !Calendar.current.isDate(firstDate, inSameDayAs: secondDate) {
                                    resultArray.append(children)
                                }
                            }
                        }
                    }
                }
                childrens = resultArray
                var counter = 0
                while ((counter < 3) && (counter < childrens.count)) {
                    let child = childrens[counter] as! DataSnapshot
                    if let value = child.value {
                        db.child("recent_device_backups").child(child.key).setValue(value)
                    }
                    counter += 1
                }
                callback(true)
            } else {
                callback(false)
            }
        })
        
    }
    
    func uploadUserLibrary(roundTimestamp: Bool, callback:@escaping ((_ error: NSError?)->())) {
//        if !UserDefaults.standard.bool(forKey: "db_changed") {
//            callback(nil)
//            return
//        }
        AppCore.sharedInstance.sqliteManager.openDb(fileName: "ikawa.sqlite")
        if (AppCore.sharedInstance.firebaseManager.currentUser == nil || AppCore.sharedInstance.firebaseManager.currentUser!.isAnonymous)  {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "not initialized"])
            callback(error)
            return
        }
        let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let dbFilePath = documentDirectoryURL.appendingPathComponent("ikawa.sqlite")
        var fileData: Data?
        do {
            try fileData = Data.init(contentsOf: dbFilePath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
            callback(error)
            return
        }
        
        let zipUrl = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("iikawadb.zip")!
        
        do {
            try FileManager.default.removeItem(at: zipUrl)
        } catch {
            
        }
        
        
        var newArchive: ZZArchive?
        do {
            newArchive = try ZZArchive.init(url: zipUrl, options: ["ZZOpenOptionsCreateIfMissingKey" : NSNumber(booleanLiteral: true)])
        }
        catch let error as NSError {
            print("Failed to open library zip: \(error)")
            callback(error)
            return
        }
        
        let archiveEntry = ZZArchiveEntry(fileName: "ikawa.sqlite", compress: true) { (error) -> Data? in
            return  fileData!
        }
        
        
        do {
            try newArchive?.updateEntries([archiveEntry])
        }
        catch let error as NSError {
            print("Failed to updateEntries: \(error)")
            callback(error)
            return
        }
        
        do {
            try fileData = Data.init(contentsOf: zipUrl)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
            callback(error)
            return
        }
        var timestamp = Int(AppCore.sharedInstance.settingsManager.internetTime().timeIntervalSince1970)
        if let dateMergedBackup = dateMergedBackup, !UserDefaults.standard.bool(forKey: "db_changed") {
            timestamp = Int(dateMergedBackup.timeIntervalSince1970)
        }
        
        let devicename = AppCore.sharedInstance.settingsManager.deviceUuid
        let filename = String(format: "%ld.zip", timestamp)
        let path = String(format: "%@/device_backups/%@/%@", IK_FIREBASE_DATABASE, devicename!, filename)
        
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: IK_FIREBASE_STORAGE)
        let spaceRef = storageRef.child(path)
        
        let newMetadata = StorageMetadata()
        newMetadata.contentType = "application/zip"
        newMetadata.customMetadata = ["user_id": (AppCore.sharedInstance.firebaseManager.currentUser?.uid)!]
        spaceRef.putData(fileData!, metadata: newMetadata) { (metadata, error) in
            if (error != nil) {
                print("Ooops! Something went wrong: \(error!)")
                callback(error as NSError?)
            } else {
                print("Success uploading library" + path)
                var db = Database.database().reference()
                db = db.child(IK_FIREBASE_DATABASE)
                guard let deviceUuid = AppCore.sharedInstance.settingsManager.deviceUuid else {
                    return
                }
                guard let currentUserUid = (AppCore.sharedInstance.firebaseManager.currentUser?.uid) else {
                    return
                }
                let backup = ["device_type" : UIDevice.current.name,
                              "device_id" : deviceUuid,
                              "user_id" : currentUserUid,
                              "timestamp" : NSNumber(value: timestamp),
                              "path" : path] as [String : Any]
                UserDefaults.standard.set(Date(timeIntervalSince1970: Double(timestamp)), forKey: self.kLastBackupDate)
                db.child("device_backups").child(String(format:"%@-%ld", devicename!, timestamp)).setValue(backup)
                db.child("recent_device_backups").child(String(format:"%@-%ld", devicename!, timestamp)).setValue(backup)
                UserDefaults.standard.set(false, forKey: "NeedMergeWithAccount")
                self.cleanRecentBackups {
                    callback(nil)
                }
            }
        }
    }
    
    
    func restoreBackup(path: String, needMergingWithLocalBackup: Bool, callback:@escaping ((_ error: Error?)->())) {
        //uploadUserLibrary(roundTimestamp: false)
        let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let dbFilePath = documentDirectoryURL.appendingPathComponent("ikawa.sqlite")
        let archDbFilePath = documentDirectoryURL.appendingPathComponent("tmp.db")
        let tempDbFilePath = documentDirectoryURL.appendingPathComponent("temp_ikawa.sqlite")
        do {
            try FileManager.default.removeItem(at: archDbFilePath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        
        do {
            try FileManager.default.removeItem(at: tempDbFilePath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        
        if (AppCore.sharedInstance.firebaseManager.currentUser == nil) {
            return
        }
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: IK_FIREBASE_STORAGE)
        let spaceRef = storageRef.child(path)
        // Download to the local filesystem
        spaceRef.write(toFile: archDbFilePath) { (url, error) in
            if (error == nil && url != nil) {
                print("Success downloading library")
                //                do {
                //                    try FileManager.default.removeItem(at: dbFilePath)
                //                }
                //                catch let error as NSError {
                //                    print("Ooops! Something went wrong: \(error)")
                //                }
                //
                var zipArchive: ZZArchive?
                do {
                    zipArchive = try ZZArchive.init(url: archDbFilePath)
                }
                catch let error as NSError {
                    print("Failed to open library zip: \(error)")
                    callback(error)
                    return;
                }
                
                let firstArchiveEntry = zipArchive?.entries[0]
                
                var dbData: Data?
                do {
                    dbData = try firstArchiveEntry?.newData()
                }
                catch let error as NSError {
                    print("Failed to uncompress library zip: \(error)")
                    callback(error)
                    return;
                }
                do {
                    try dbData?.write(to: tempDbFilePath/*dbFilePath*/)
                }
                catch let error as NSError {
                    print("Failed to uncompress library zip: \(error)")
                    callback(error)
                    return;
                }
                UserDefaults.standard.set(false, forKey: "NeedMergeWithAccount")
                if needMergingWithLocalBackup {
                    DispatchQueue.global(qos: .background).async {
                        let mergeResult = self.mergeBackupAndLocalDb()
                        if mergeResult.isProfileChanges || mergeResult.isRoasLogChanges {
                            UserDefaults.standard.set(true, forKey: "db_changed")
                        }  else {
                            self.dateMergedBackup = nil
                        }
                        
                        
                        
                        
                        _ = AppCore.sharedInstance.sqliteManager.removeDB() //removing local DB
                        let fileManager = FileManager.default
                        do {
                            try fileManager.copyItem(at: tempDbFilePath, to: dbFilePath)
                        } catch {
                            print("Unable to copy file: \(tempDbFilePath)")
                            DispatchQueue.main.async {
                                callback(nil)
                            }
                            return
                        }
                        DispatchQueue.main.async {
                            AppCore.sharedInstance.profileManager.reloadData()
                            #if TARGET_PRO
                            AppCore.sharedInstance.roastNotesManager.updateAllRoastNotes()
                            #endif
                            callback(nil)
                        }

                    }
                    
                } else {
                    UserDefaults.standard.set(true, forKey: "db_changed")
                    
                    
                    
                    
                    _ = AppCore.sharedInstance.sqliteManager.removeDB() //removing local DB
                    let fileManager = FileManager.default
                    do {
                        try fileManager.copyItem(at: tempDbFilePath, to: dbFilePath)
                    } catch {
                        print("Unable to copy file: \(tempDbFilePath)")
                        callback(nil)
                        return
                    }
                    AppCore.sharedInstance.profileManager.reloadData()
                    #if TARGET_PRO
                    AppCore.sharedInstance.roastNotesManager.updateAllRoastNotes()
                    #endif
                    callback(nil)
                    
                }

                
            } else {
                callback(error)
            }
        }
    }
    
    func mergeBackupAndLocalDb() -> (isProfileChanges: Bool, isRoasLogChanges: Bool) {
        //sync profiles
        let restoredSqliteManager = IKSqliteManager(dbName: "temp_ikawa.sqlite")
        var isProfileChanges = false
        var isRoastLogChanges = false
        if let localProfiles = AppCore.sharedInstance.profileManager.profiles, let restoredProfilesDataArray = restoredSqliteManager.readProfiles(), let restoredProfiles = AppCore.sharedInstance.profileManager.parseProfileDataArray(profileDataArray: restoredProfilesDataArray){
            print("!!!!!!!!!!!!!! 1")
            
            for localProfile in localProfiles {
                
                if let findedRestoredProfile = (restoredProfiles.filter { (profile) -> Bool in
                    return localProfile.uuid == profile.uuid
                }).first {
                    if localProfile.dateLastEdited > findedRestoredProfile.dateLastEdited {
                        _ = restoredSqliteManager.updateProfile(profile: localProfile)
                        isProfileChanges = true
                    }
                    
                } else {
                    _ = restoredSqliteManager.addProfile(profile: localProfile)
                    isProfileChanges = true
                }
                
            }
        }
        print("!!!!!!!!!!!!!! 2")
        
        //sync roast logs
        #if TARGET_PRO
        let restoredRoastManager = IKRoastManager(sqliteManager: restoredSqliteManager)
        let restoredRoastNotesManager = IKRoastNotesManager(sqliteManager: restoredSqliteManager, roastManager: restoredRoastManager)
        if let restoredRoastNotesDataArray = restoredSqliteManager.roastNotes(), let restoredRoastNotes = restoredRoastNotesManager.parseRoastNotesArray(roastNotesDataArray: restoredRoastNotesDataArray)  {
            
            if let localRoastNotes = AppCore.sharedInstance.roastNotesManager.roastNotesList {
                print("!!!!!!!!!!!!!! 3")
                for localRoastNote in localRoastNotes {
                    if let findedRestoredRoastNote = (restoredRoastNotes.filter { (roastNote) -> Bool in
                        return localRoastNote.roast?.uuid == roastNote.roast?.uuid
                    }).first {
                        if let localRoastNoteDateLastEdited = localRoastNote.dateLastEdited,  let findedRestoredRoastNoteDateLastEdited = findedRestoredRoastNote.dateLastEdited, localRoastNoteDateLastEdited > findedRestoredRoastNoteDateLastEdited {
                            if restoredRoastManager.saveRoast(roast: localRoastNote.roast) {
                                if restoredRoastNotesManager.saveRoastNotes(roastNotes: localRoastNote, needReloadData: false) {
                                    isRoastLogChanges = true
                                }
                            }
                        }
                    } else {
                        if restoredRoastManager.saveRoast(roast: localRoastNote.roast) {
                            if restoredRoastNotesManager.saveRoastNotes(roastNotes: localRoastNote, needReloadData: false) {
                                isRoastLogChanges = true
                            }
                        }
                    }
                }
            }
        }
        #endif
        print("!!!!!!!!!!!!!! 4")
        
        restoredSqliteManager.closeDB()
        return (isProfileChanges, isRoastLogChanges)
    }
    
    
    func cleanRecentBackups(callback:@escaping (()->())) {
        if (AppCore.sharedInstance.firebaseManager.currentUser == nil) {
            callback()
            return
        }
        getLastBackups { (error, snapshots) in
            if snapshots?.count ?? 0 > 1 {
                for i in 1...(snapshots!.count - 1) {
                    
                    let child = snapshots![i]
                    let previousChild = snapshots![i - 1]
                    if let value = child.value, let previousValue = previousChild.value {
                        let dict = value  as! Dictionary<String, AnyObject>
                        let timestamp = dict["timestamp"]
                        let date = Date.init(timeIntervalSince1970: (timestamp?.doubleValue)!)
                        
                        let prevDict = previousValue  as! Dictionary<String, AnyObject>
                        let prevTimestamp = prevDict["timestamp"]
                        let prevDate = Date.init(timeIntervalSince1970: (prevTimestamp?.doubleValue)!)
                        
                        if Calendar.current.isDate(date, inSameDayAs: prevDate) {
                            previousChild.ref.removeValue()
                        }
                    }
                }
            }
            let profilesCount = 3
            if snapshots?.count ?? 0 > profilesCount {
                for i in 0...(snapshots!.count - 1 - profilesCount) {
                    let child = snapshots![i]
                    child.ref.removeValue()
                }
            }
            callback()
        }
    }
    
    func restoreRecentUserLibrary() -> Void {
        if (AppCore.sharedInstance.firebaseManager.currentUser == nil) {
            return
        }
        let vc = UIApplication.shared.keyWindow?.rootViewController
        vc!.view.makeToastActivity(.center)
        var db = Database.database().reference()
        db = db.child(IK_FIREBASE_DATABASE)
        var backupsQuery: DatabaseQuery?
        if (AppCore.sharedInstance.firebaseManager.currentUser?.isAnonymous)! {
            let deviceUDID = AppCore.sharedInstance.settingsManager.deviceUuid
            backupsQuery = db.child("recent_device_backups").queryOrdered(byChild: "device_id").queryEqual(toValue: deviceUDID!).queryLimited(toLast: 30)
        } else {
            backupsQuery = db.child("recent_device_backups").queryOrdered(byChild: "user_id").queryEqual(toValue: AppCore.sharedInstance.firebaseManager.currentUser!.uid)/*.queryLimited(toLast: 30)*/
            
        }
        
        backupsQuery?.observeSingleEvent(of: .value, with: { (snapshot) in
            vc!.view.hideToastActivity()
            if (snapshot.childrenCount > 0) {
                let confirmationDialogView = ButtonsPopupModalView()
                confirmationDialogView.layout = .horisontal
                confirmationDialogView.title = NSLocalizedString("$restore_alert_title", comment: "")
                confirmationDialogView.subtitle = NSLocalizedString("$restore_alert_message", comment: "")
                
                confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$no", comment: ""), type: .Cancel) {}
                
                confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$yes", comment: ""), type: .Bordered) {
                    let alertSheet = UIAlertController(title: NSLocalizedString("$restore_sheet_title", comment: ""), message: NSLocalizedString("$restore_alert_message", comment: ""), preferredStyle: UIAlertController.Style.actionSheet)
                    let childrens = snapshot.children.allObjects.sorted(by: { (first , second) -> Bool in
                        if let firstValue = (first as! DataSnapshot).value, let secondValue = (second as! DataSnapshot).value {
                            let dictFirst = firstValue  as! Dictionary<String, AnyObject>
                            let timestampFirst = dictFirst["timestamp"] as! NSNumber
                            
                            let dictSecond = secondValue  as! Dictionary<String, AnyObject>
                            let timestampSecond = dictSecond["timestamp"] as! NSNumber
                            return timestampFirst.doubleValue < timestampSecond.doubleValue
                        }
                        return true
                    })
                    var profilesCount = 3
                    for i in 0...(childrens.count - 1) {
                        
                        //                        if profilesCount == 0 {
                        //                            break
                        //                        }
                        profilesCount = profilesCount - 1
                        let child = childrens[childrens.count - i - 1] as! DataSnapshot
                        if let value = child.value {
                            let dict = value  as! Dictionary<String, AnyObject>
                            let timestamp = dict["timestamp"]
                            let deviceName = dict["device_type"] as? String
                            let path = dict["path"] as? String
                            let date = Date.init(timeIntervalSince1970: (timestamp?.doubleValue)!)
                            let dateFormater = DateFormatter()
                            if ((timestamp as! NSNumber).intValue % 86400 == 0) {
                                dateFormater.dateFormat = "dd/MM/yyyy"
                            } else {
                                dateFormater.dateFormat = "dd/MM/yyyy HH:mm"
                            }
                            let dateString = String(format: "%@ (%@)", dateFormater.string(from: date), deviceName ?? "")
                            alertSheet.addAction(UIAlertAction(title: dateString, style: .default, handler: { action in
                                self.restoreBackup(path: path!, needMergingWithLocalBackup: false, callback: { (error) in
                                    if let error = error {
                                        let errorDialogView = ButtonsPopupModalView()
                                        errorDialogView.layout = .vertical
                                        errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                                        errorDialogView.subtitle = error.localizedDescription
                                        errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                                        
                                        IKNotificationManager.sharedInstance.show(view: errorDialogView)
                                    } else {
                                        let messageBox = ButtonsPopupModalView()
                                        messageBox.layout = .horisontal
                                        messageBox.subtitle = NSLocalizedString("$restore_alert_message_done", comment: "")
                                        messageBox.setUpButton(index: .First, title:NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                                            messageBox.dismiss(animated: true)
                                        }
                                        IKNotificationManager.sharedInstance.show(view: messageBox)
                                    }
                                })
                            }))
                        }
                        
                    }
                    alertSheet.addAction(UIAlertAction(title: NSLocalizedString("$no", comment: ""), style: .cancel, handler: { action in
                        
                    }))
                    if let vc = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController {
                        if let popoverPresentationController = alertSheet.popoverPresentationController {
                            popoverPresentationController.sourceView = vc.view
                            popoverPresentationController.sourceRect = CGRect(x: vc.view.bounds.size.width / 2.0, y: vc.view.bounds.size.height / 2.0, width: 1, height: 1)
                            popoverPresentationController.permittedArrowDirections = []
                        }
                        
                        vc.present(alertSheet, animated: true, completion: {})
                    }
                    
                }
                
                IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            }
        })
    }
    
    func restoreUserLibrary() -> Void {
        if (AppCore.sharedInstance.firebaseManager.currentUser == nil) {
            return
        }
        let vc = UIApplication.shared.keyWindow?.rootViewController
        vc!.view.makeToastActivity(.center)
        var db = Database.database().reference()
        db = db.child(IK_FIREBASE_DATABASE)
        var backupsQuery: DatabaseQuery?
        if (AppCore.sharedInstance.firebaseManager.currentUser?.isAnonymous)! {
            let deviceUDID = AppCore.sharedInstance.settingsManager.deviceUuid
            backupsQuery = db.child("device_backups").queryOrdered(byChild: "device_id").queryEqual(toValue: deviceUDID!).queryLimited(toLast: 30)
        } else {
            backupsQuery = db.child("device_backups").queryOrdered(byChild: "user_id").queryEqual(toValue: AppCore.sharedInstance.firebaseManager.currentUser!.uid)/*.queryLimited(toLast: 30)*/
            
        }
        
        backupsQuery?.observeSingleEvent(of: .value, with: { (snapshot) in
            vc!.view.hideToastActivity()
            if (snapshot.childrenCount > 0) {
                let confirmationDialogView = ButtonsPopupModalView()
                confirmationDialogView.layout = .horisontal
                confirmationDialogView.title = NSLocalizedString("$restore_alert_title", comment: "")
                confirmationDialogView.subtitle = NSLocalizedString("$restore_alert_message", comment: "")
                
                confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$yes", comment: ""), type: .Bordered) {
                    let alertSheet = UIAlertController(title: NSLocalizedString("$restore_sheet_title", comment: ""), message: NSLocalizedString("$restore_alert_message", comment: ""), preferredStyle: UIAlertController.Style.actionSheet)
                    let childrens = snapshot.children.allObjects.sorted(by: { (first , saecond) -> Bool in
                        if let firstValue = (first as! DataSnapshot).value, let secondValue = (saecond as! DataSnapshot).value {
                            let dictFirst = firstValue  as! Dictionary<String, AnyObject>
                            let timestampFirst = dictFirst["timestamp"] as! NSNumber
                            
                            let dictSecond = secondValue  as! Dictionary<String, AnyObject>
                            let timestampSecond = dictSecond["timestamp"] as! NSNumber
                            return timestampFirst.doubleValue < timestampSecond.doubleValue
                        }
                        return true
                    })
                    var profilesCount = 3
                    for i in 0...(childrens.count - 1) {
                        
                        //                        if profilesCount == 0 {
                        //                            break
                        //                        }
                        profilesCount = profilesCount - 1
                        let child = childrens[childrens.count - i - 1] as! DataSnapshot
                        if let value = child.value {
                            let dict = value  as! Dictionary<String, AnyObject>
                            let timestamp = dict["timestamp"]
                            let deviceName = dict["device_type"] as? String
                            let path = dict["path"] as? String
                            let date = Date.init(timeIntervalSince1970: (timestamp?.doubleValue)!)
                            let dateFormater = DateFormatter()
                            if ((timestamp as! NSNumber).intValue % 86400 == 0) {
                                dateFormater.dateFormat = "dd/MM/yyyy"
                            } else {
                                dateFormater.dateFormat = "dd/MM/yyyy HH:mm"
                            }
                            let dateString = String(format: "%@ (%@)", dateFormater.string(from: date), deviceName ?? "")
                            alertSheet.addAction(UIAlertAction(title: dateString, style: .default, handler: { action in
                                self.restoreBackup(path: path!, needMergingWithLocalBackup: false, callback: { (error) in
                                    if let error = error {
                                        let errorDialogView = ButtonsPopupModalView()
                                        errorDialogView.layout = .vertical
                                        errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                                        errorDialogView.subtitle = error.localizedDescription
                                        errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                                        
                                        IKNotificationManager.sharedInstance.show(view: errorDialogView)
                                    }
                                })
                            }))
                        }
                        
                    }
                    alertSheet.addAction(UIAlertAction(title: NSLocalizedString("$no", comment: ""), style: .cancel, handler: { action in
                        
                    }))
                    if let vc = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController {
                        if let popoverPresentationController = alertSheet.popoverPresentationController {
                            popoverPresentationController.sourceView = vc.view
                            popoverPresentationController.sourceRect = CGRect(x: vc.view.bounds.size.width / 2.0, y: vc.view.bounds.size.height / 2.0, width: 1, height: 1)
                            popoverPresentationController.permittedArrowDirections = []
                        }
                        
                        vc.present(alertSheet, animated: true, completion: {})
                    }
                }
                confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$no", comment: ""), type: .Cancel) {}
                
                IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                
            }
        })
    }
    
    func needCreateBackup() -> Bool {
        if let lastBackupDate = UserDefaults.standard.object(forKey: kLastBackupDate) as? Date {
            if Calendar.current.isDateInToday(lastBackupDate) {
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }
    
    func saveLocalImage(image: UIImage, folder: String, fileName: String) -> Error? {
        let fileManager = FileManager.default
        if let documentDirectory = try? fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false) {
            
            // New Folder
            let folderUrl = documentDirectory.appendingPathComponent(folder)
            if !fileManager.fileExists(atPath: folderUrl.path) {
                do {
                    try FileManager.default.createDirectory(atPath: folderUrl.path, withIntermediateDirectories: false, attributes: nil)
                } catch let error {
                    print(error.localizedDescription)
                    return error
                }
            }
            
            do {
                
                let fileURL = folderUrl.appendingPathComponent("\(fileName)")
                if let imageData = image.sd_imageData() {
                    try imageData.write(to: fileURL)
                }
            } catch {
                return error
            }
        }
        return nil
    }
    
    func localImage(folder: String, fileName: String) -> UIImage? {
        let fileManager = FileManager.default
        if let documentDirectory = try? fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false) {
            
            // New Folder
            let fileUrl = documentDirectory.appendingPathComponent(folder).appendingPathComponent(fileName)
            return UIImage(contentsOfFile: fileUrl.path)
        }
        return nil
    }
}
