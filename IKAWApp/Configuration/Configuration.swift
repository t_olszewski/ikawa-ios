
import Foundation

struct Configuration {

    static let environment: String = {
        if let result = infoDict["ENVIRONMENT"] as? String {
            return result
        }
        return ""
    }()

    fileprivate static var infoDict: [String: Any] {
        get {
            if let dict = Bundle.main.infoDictionary {
                return dict
            } else {
                fatalError("Plist file not found")
            }
        }
    }

}
