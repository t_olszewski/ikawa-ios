//
//  IKPolicyVC.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 4/23/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class IKPolicyVC: UIViewController {
    
    var email: String!
    var password: String!
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var goToPolicyButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var loginBackgroundImageView: UIImageView!
    
    var callback: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        okButton.layer.cornerRadius = okButton.frame.height / 2
        okButton.layer.borderColor = kColorRed.cgColor
        okButton.layer.borderWidth = 2
        loginBackgroundImageView.layer.opacity = 0.27
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Actiond
    
    @IBAction func goToPolicyButtonTouchUpInside(_ sender: Any) {
        //https://7puzn1bawno25nd4f1awju71-wpengine.netdna-ssl.com/wp-content/uploads/2018/05/IKAWA-Privacy-Policy-for-website-FINAL.pdf
        guard let url = URL(string: kTermsAndConditions) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    @IBAction func okButtonTouchUpInside(_ sender: Any) {
        //self.view.makeToastActivity(.center)
        guard let callback = callback else {
            return
        }
        callback()
        self.navigationController?.popViewController(animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
