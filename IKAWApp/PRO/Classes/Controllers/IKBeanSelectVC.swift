//
//  IKBeanSelectVC.swift
//  IKAWA-Pro
//
//  Created by Admin on 2/27/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class IKBeanSelectVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cancelSearchButton: UIButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    var callback: ((IKCoffeeBean) -> Void)?
    var lots: [IKCoffeeBean]?
    var displayArray: [IKCoffeeBean]?
    var searchStr = ""
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideSearch()
        searchBar.delegate = self
        UIApplication.shared.keyWindow?.makeToastActivity(.center)
        AppCore.sharedInstance.cropsterManager.getLots { (lots, error) in
            UIApplication.shared.keyWindow?.hideToastActivity()
            if error != nil {
                let errorDialogView = ButtonsPopupModalView()
                errorDialogView.layout = .vertical
                errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                errorDialogView.subtitle = NSLocalizedString("$cropster_error_message", comment: "")
                errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                
                IKNotificationManager.sharedInstance.show(view: errorDialogView)
            } else {
                self.lots = lots
                self.search()
                self.tableView.reloadData()
            }
            
        }
    }
    
    // MARK: - Actions
    @IBAction func searchButtonTouchUpInside(_ sender: Any) {
        showSearch()
        searchBar.becomeFirstResponder()
    }
    
    @IBAction func cancelSearchButtonTouchUpInside(_ sender: Any) {
        searchBar.text = ""
        searchStr = ""
        search()
        hideSearch()
    }
    
    @IBAction func backButtonTouchUpInside(_ sender: Any) {
        if navigationController != nil {
            navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func showSearch() {
        if searchContainerView.isHidden{
            searchContainerView.snp.remakeConstraints { (make) in
                make.height.equalTo(56)
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
            searchContainerView.isHidden = false
            searchButton.isHidden = true
        }
    }
    
    func hideSearch() {
        if (!searchContainerView.isHidden) {
            searchContainerView.snp.remakeConstraints { (make) in
                make.height.equalTo(0)
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
            self.searchContainerView.isHidden = true
        }
        
        searchBar.resignFirstResponder()
        searchButton.isHidden = false
    }
    
    func search() {
        if searchStr.count > 0 {
            displayArray = lots?.filter { (lot) -> Bool in
                lot.coffeeType?.lowercased().contains(searchStr.lowercased()) ?? false
            }
        } else {
            displayArray = lots
        }
        tableView.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension IKBeanSelectVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let lots = displayArray {
            return lots.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        guard let lot = displayArray?[indexPath.row] else {
            return cell
        }
        if let coffeeNameLabel = cell.viewWithTag(100) as? UILabel {
            coffeeNameLabel.text = lot.coffeeType
        }
        if let weightLabel = cell.viewWithTag(200) as? UILabel {
            weightLabel.text = lot.weight
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let callback = callback {
            if let lot = displayArray?[indexPath.row] {
               callback(lot)
            }
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
        navigationController?.popViewController(animated: true)
    }
    
}

extension IKBeanSelectVC: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            searchBar.resignFirstResponder()
            return false
        }
        searchStr = NSString(string: searchBar.text!).replacingCharacters(in: range, with: text)
        search()
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setTextFieldColor(color: .white, textColor: #colorLiteral(red: 0.1285303235, green: 0.1236336902, blue: 0.128546536, alpha: 1))
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setTextFieldColor(color: #colorLiteral(red: 0.18204588494, green: 0.17412940423, blue: 0.17667689226, alpha: 1), textColor: .white)
    }
}
