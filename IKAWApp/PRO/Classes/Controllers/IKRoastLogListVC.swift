//
//  IKRoastLogVC.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 4/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib
import SwipeCellKit

class IKRoastLogListVC: UIViewController {
    

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: RoastLogListHeaderView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    var displayArray: [IKRoastNotes]?
    var searchStr = ""
    
    
    //MARK: - Life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppCore.sharedInstance.loggingManager.trackScreen(name: "Roast Log Screen")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(updateTable), name: NSNotification.Name.DidChangeRoastNotesData, object: nil)
        
        headerView.delegate = self
        self.tableView.contentInset = UIEdgeInsets(top: self.tableView.contentInset.top, left: 0, bottom: 0, right: 0)
        prepareAppearance()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // statusBarView.populate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.tableView.contentInset = UIEdgeInsets(top: self.tableView.contentInset.top, left: 0, bottom: 0, right: 0)
            } else {
                self.tableView.contentInset = UIEdgeInsets(top: self.tableView.contentInset.top, left: 0, bottom: endFrame?.size.height ?? 0.0, right: 0)
            }
            
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
        
    }
    
    func prepareAppearance() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 71.0
        updateTable()
    }
    
    func getDisplayArray() -> [IKRoastNotes] {
        return AppCore.sharedInstance.roastNotesManager.displayedRoastNotesList ?? []
    }
    
    @objc func updateTable() {
        if searchStr.count > 0 {
            displayArray = getDisplayArray().filter { (roastLog) -> Bool in
                var str = ""
                
                if let uuid = roastLog.roast?.uuid, uuid.count >= 3 {
                    let index = uuid.index(uuid.startIndex, offsetBy: 3)
                    str = String(uuid[..<index]).lowercased()
                }
                
                if  let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: roastLog.roast?.profileUuid ?? "") {
                    
                    return profile.name.lowercased().contains(searchStr.lowercased()) || str.lowercased().contains(searchStr.lowercased())
                } else {
                    return false
                }
            }
        } else {
            displayArray = getDisplayArray()
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
    }
}

extension IKRoastLogListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let displayArray = displayArray else {
            return UITableViewCell()
        }
        
        let roastNotes = displayArray[indexPath.row]
        guard let roast = roastNotes.roast else {
            return UITableViewCell()
        }
        
        guard let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: roastNotes.roast?.profileUuid ?? "") else {
            return tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SwipeTableViewCell
        let profileViewCell = cell.contentView.viewWithTag(50) as! IKRoastLogViewCell
        profileViewCell.populate(profile: profile, roastNotes: roastNotes)
        cell.delegate = self
        
        let selecedBackgroundView = UIView()
        selecedBackgroundView.backgroundColor = kColorLightGrey3
        cell.selectedBackgroundView = selecedBackgroundView
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let roastNotes = displayArray?[indexPath.row] {
            let vc = UIStoryboard(name: "RoastNotes", bundle: nil).instantiateViewController(withIdentifier: "IKRoastNotesDetailVC") as! IKRoastNotesDetailVC
            vc.notes = roastNotes
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension IKRoastLogListVC: RoastLogListHeaderViewDelegate {
    func didTapMenuButton() {
        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let nc = menuStoryboard.instantiateInitialViewController()
        present(nc!, animated: true) {}
    }
    
    func search(text:String) {
        searchStr = text
        updateTable()
    }
    
    func layoutHeaderViewDidChanged() {
        headerViewHeightConstraint.constant = headerView.maxHeight()
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
}

extension IKRoastLogListVC: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        if orientation == .right {
            if let roastLog = self.displayArray?[indexPath.row] {
                var actionArray = [SwipeAction]()
                let deleteAction = SwipeAction(style: .destructive, title: NSLocalizedString("$delete", comment: "")) {  action, indexPath in
                    
                    let confirmationDialogView = ButtonsPopupModalView()
                    confirmationDialogView.layout = .horisontal
                    confirmationDialogView.title = NSLocalizedString("$delete_roastlog_message", comment: "")
                    confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$cancel_up", comment: ""), type: .Cancel) {}
                    confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$alert_delete", comment: ""), type: .Bordered) {
                        UIApplication.shared.keyWindow?.makeToastActivity(.center)
                        self.displayArray?.remove(at: indexPath.row)
                        tableView.beginUpdates()
                        tableView.deleteRows(at: [indexPath], with: .fade)
                        tableView.endUpdates()
                        DispatchQueue.global(qos: .background).async {
                            if AppCore.sharedInstance.roastNotesManager.deleteRoastNotes(roastNotes: roastLog) {
                                DispatchQueue.main.async {
                                    UIApplication.shared.keyWindow?.hideToastActivity()
                                }
                            }
                        }
                    }
                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                }
                deleteAction.backgroundColor = kColorRed
                actionArray.append(deleteAction)
                /*
                let shareAction = SwipeAction(style: .default, title: NSLocalizedString("$share", comment: "")) {
                    action, indexPath in
                     AppCore.sharedInstance.roastManager.shareRoastLog(roastNotes: roastLog)
                }
                shareAction.backgroundColor = kColorLightGrey1
                shareAction.textColor = kColorDarkGrey1
                shareAction.hidesWhenSelected = true
                actionArray.append(shareAction)*/
                return actionArray
            } else {
                return nil
            }
        } else {
           return nil
        }
    }
    
    func visibleRect(for tableView: UITableView) -> CGRect? {
        return tableView.bounds
    }
}
