//
//  PreCreateAccountVC.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 9/4/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit

class PreCreateAccountVC: UIViewController {
    
    @IBOutlet weak var wthoutAccountButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var agreeTermsButton: UIButton!
    @IBOutlet weak var termsConditions: UIButton!
    var agreeTerms = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createAccountButton.layer.cornerRadius = createAccountButton.frame.height/2
        if UIDevice.current.userInterfaceIdiom == .pad {
            let attrs = [NSAttributedString.Key.font.rawValue :UIFont(name: kFontAvenirNextMedium, size: 19)!,
                         NSAttributedString.Key.underlineStyle : 1, NSAttributedString.Key.foregroundColor: UIColor.white
                ] as! [NSAttributedString.Key : Any]
            let attributedTitle = NSAttributedString(string: "$terms_condition".localized, attributes: attrs)
            termsConditions.setAttributedTitle(attributedTitle, for: .normal)
        }
        updateAgreeTermsButton()
    }
    @IBAction func continueWithoutAnAccount(_ sender: Any) {
        AppCore.sharedInstance.settingsManager.workingWithoutAccount = true
        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_WITHOUT_ACCOUNT", label: "")
        NavigationHelper.GoToMainScreen()
        
    }
    @IBAction func signInButtonTouchUpInside(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func agreeTermsButtonTouchUpInside(_ sender: Any) {
        updateAgreeTermsButton()
    }
    
    @IBAction func termsButtonTouchUpInside(_ sender: Any) {
        guard let url = URL(string: kTermsAndConditions) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func updateAgreeTermsButton() {
        if agreeTerms {
            agreeTerms = false
            agreeTermsButton.setImage(UIImage(named: "DeselectedPointIcon"), for: .normal)
            wthoutAccountButton.isEnabled = false
            wthoutAccountButton.letterSpace = 1.71
            wthoutAccountButton.setColor(color: kColorLightGrey2)
        } else {
            agreeTerms = true
            agreeTermsButton.setImage(UIImage(named: "selectedPointIcon"), for: .normal)
            wthoutAccountButton.isEnabled = true
            wthoutAccountButton.letterSpace = 1.71
            wthoutAccountButton.setColor(color: .white)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
