//
//  IKChartVC.swift
//  IKAWApp
//
//  Created by Admin on 3/28/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Charts
import IKRoasterLib
import SnapKit
import Toast_Swift

class IKChartVC: UIViewController {

    var profileHasChanged: Bool = false
    var profile: Profile?
    var editedProfile: Profile?
    @IBOutlet weak var swipeStatusBarView: IKSwipeStatusBarView!
    @IBOutlet weak var chartViewContainer: UIView!
    
    @IBOutlet weak var shareButtonCenteringConstraint: NSLayoutConstraint!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var sendToRoastButton: UIButton!
    @IBOutlet weak var chartHeaderView: ChartHeaderView!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var sensorButton: UIButton!
    @IBOutlet weak var tableButton: UIButton!
    @IBOutlet weak var rorButton: UIButton!
    @IBOutlet weak var ccButton: UIButton!
    @IBOutlet weak var firstCrackButton: UIButton!
    @IBOutlet weak var secondCrackButton: UIButton!
    

    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var tableEditView: UIView!
    @IBOutlet weak var sensorButtonView: UIView!
    @IBOutlet weak var rorView: UIView!
    @IBOutlet weak var colorChangeView: UIView!
    @IBOutlet weak var firstCrackView: UIView!
    @IBOutlet weak var secondCrackView: UIView!
    @IBOutlet weak var cancelButtonView: UIView!
    @IBOutlet weak var menuContainer: UIStackView!
    @IBOutlet weak var dtrContainerView: UIView!
    @IBOutlet weak var dtrLabel: UILabel!
    @IBOutlet weak var rorButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuContainerWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var archivedLabel: UILabel!
    @IBOutlet weak var restoreButton: UIButton!
    @IBOutlet weak var sendProfileProgressView: ProgressIndicatorView!
    @IBOutlet weak var completeImageView: UIImageView!
    
    
    var sendingIndicatorTimer: Timer?
    var numberOfImage = 1
    
    var sendingProfileToRoaster: Bool = false
    var tempChartView: IKTempChartView?
    
    private var buttonEditState: Bool = false
    
    private var ccEdited = false
    private var fcEdited = false
    private var scEdited = false
    
    //MARK: - Life cycle
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector:#selector(didLogout), name: NSNotification.Name.DidLogOut, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(degreeCelsiusChanged), name: NSNotification.Name.DegreeCelsiusChanged, object: nil)
        sensorButton.setImage(UIImage(named: (profile?.tempSensor == TempSensorBelow) ? "Exhaust" : "Inlet"), for: .normal)
        chartHeaderView.delegate = self
        chartHeaderView.titleLabel.text = "$roast_profile_up".localized
        openWalkthroughPopup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        self.swipeStatusBarView.statusBarView.thumbnail.isHidden = true
        RoasterManager.instance().delegates.add(self.swipeStatusBarView.statusBarView)
        RoasterManager.instance().delegates.add(self)
        prepareAppearance()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
        //tabBarController?.tabBar.isHidden = false
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
        if (RoasterManager.instance().delegates.contains(self.swipeStatusBarView.statusBarView)) {
            RoasterManager.instance().delegates.remove(self.swipeStatusBarView.statusBarView)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if (tempChartView != nil) {
            tempChartView?.viewWillTransition()
        }
        updateOrientationAppearance()
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProfileEditVC" {
            if profile?.isRoasterProfile() ?? false {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_TABLE_LAUNCH", label: "")
            } else {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_GRAPH_TABLE", label: "")
            }
            profileHasChanged = self.tempChartView!.graphWasEdited
            let vc = segue.destination as! ProfileEditVC
            if let selectedFanPoint = tempChartView?.selectedFanPoint(), let index = (editedProfile?.fanPoints as? [RoastFanPoint])?.firstIndex(of: selectedFanPoint) {
                if index > 0 {
                    vc.selectedFanPoint = selectedFanPoint
                }
            }
            
            if let selectedRoastPoint = tempChartView?.selectedRoastPoint(), let index = (editedProfile?.roastPoints as? [RoastTempPoint])?.firstIndex(of: selectedRoastPoint) {
                if index > 0 {
                    vc.selectedRoastPoint = selectedRoastPoint
                }
            }
            
            vc.selectedCooldownPoint = tempChartView?.selectedCooldownPint()
            
            vc.profile = profile
            vc.editedProfile = editedProfile
            vc.callback = {(profileHasChanged: Bool) in
                self.profileHasChanged = profileHasChanged
                self.showTemperatureGraph()
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Actions
    
    private func openWalkthroughPopup() {
        guard let profile = profile else {
            return
        }
        if RoasterManager.instance().roaster != nil && !isRoasterProfile(profile: profile) {
            let notFirstOpen = UserDefaults.standard.bool(forKey: "notFirstTimeTheyConnectToRoaster")
            if !notFirstOpen{
                UserDefaults.standard.set(true, forKey: "notFirstTimeTheyConnectToRoaster")
                let walkthroughModalView = WalkthroughModalView()
                walkthroughModalView.titleIsHidden = true
                walkthroughModalView.setupHorisontalView(image: nil, title: "$press_send_to_load_a_profile".localized, index: .First)
                walkthroughModalView.show(animated: true, dismissCallback: {
                })
            }
        }
    }
    
    @IBAction func sendToRoastButtonTouchUpInside(_ sender: Any) {
        sendToRoastButton.isEnabled = false
        tempChartView!.removeHighlightForEditableDatasets()
        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_GRAPH_SEND", label: "")
        sendProfile(profile: self.profile)
        sendToRoastButton.isEnabled = true
        
    }

    @IBAction func editButtonTouchUpInside(_ sender: Any) {
        if profile?.isRoasterProfile() ?? false {
            updateDTR()
        }
        if editedProfile != nil {
            if profile?.isRoasterProfile() ?? false {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_EDIT_CANCEL", label: "")
            } else {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_GRAPH_CANCEL_EDIT", label: "")
            }
            
            goToViewState()
        } else {
            if profile?.isRoasterProfile() ?? false {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_EDIT_PROFILE", label: "")
            } else {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_GRAPH_EDIT", label: "")
            }
            goToEditState()
        }
    }

    @IBAction func shareButtonTouchUpInside(_ sender: Any) {
        if let profile = profile {
            AppCore.sharedInstance.profileManager.shareProfile(profile: profile)
        }
    }
    
    @IBAction func saveButtonTouchUpInside(_ sender: Any) {
        tempChartView!.removeHighlightForEditableDatasets()
        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_EDIT_SAVE", label: "")
        saveButton.isEnabled = false
        save { (profile) in
            guard let profile = profile else {
                return
            }
            self.updateAppearance()
            
            if let roastManager = RoasterManager.instance()?.roaster {
                if roastManager.status.state == IK_STATUS_IDLE && profile.isRoasterProfile() {
                    self.sendToRoastButton.isHidden = true
                    self.shareButton.isHidden = true
                    self.shareButtonCenteringConstraint.priority = UILayoutPriority(rawValue: UILayoutPriority.RawValue(self.sendToRoastButton.isHidden ? 999 : 100))
                } else {
                    self.sendToRoastButton.isHidden = false
                    self.shareButton.isHidden = self.sendingProfileToRoaster
                    self.shareButtonCenteringConstraint.priority = UILayoutPriority(rawValue: UILayoutPriority.RawValue(self.sendToRoastButton.isHidden ? 999 : 100))
                }
            }
        }
        self.saveButton.isEnabled = true
        profileHasChanged = false
    }
    
    @IBAction func sensorButtonTouchUpInside(_ sender: Any) {
        if (editedProfile?.tempSensor == TempSensorBelow) {
            if ((editedProfile?.getHighestRoastPoint()?.temperature)! > Float(IK_MAX_TEMP_ABOVE)) {
                let alert = ButtonsPopupModalView()
                alert.layout = .vertical
                alert.title = NSLocalizedString("$exhaust_profile_temperature_error_title", comment: "")
                alert.subtitle = String(format: NSLocalizedString("$exhaust_profile_temperature_error_message", comment: ""), Int(IK_MAX_TEMP_ABOVE))
                alert.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                
                IKNotificationManager.sharedInstance.show(view: alert)
            } else {
                editedProfile?.tempSensor = TempSensorAbove
            }
        } else {
            if RoasterManager.instance().roaster != nil && !(RoasterManager.instance().roaster.hasBelowSensor()) {

                if !(profile?.isRoasterProfile() ?? false) {
                    editedProfile?.tempSensor = TempSensorBelow
                } else {
                    let confirmationDialogView = ButtonsPopupModalView()
                    confirmationDialogView.layout = .vertical
                    confirmationDialogView.title = "$profile_type_not_supported".localized
                    confirmationDialogView.subtitle = "$inlet_sensor_profile_not_supported_message".localized
                    confirmationDialogView.setUpButton(index: .First, title: "$ok".localized, type: .Filled) {}
                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                }
            } else {
                editedProfile?.tempSensor = TempSensorBelow
            }
            
        }
        updateSensorButtonAppearance()
        showTemperatureGraph()
    }
    
    @IBAction func rorButtonTouchUpInside(_ sender: Any) {
        if let tempChartView = self.tempChartView {
            tempChartView.isRORHidden = !tempChartView.isRORHidden
            if (!tempChartView.isRORHidden) {
                if profile?.isRoasterProfile() ?? false {
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_ROR_ON", label: "")
                } else {
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_GRAPH_ROR", label: "")
                }
                
            } else {
                if profile?.isRoasterProfile() ?? false {
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_ROR_OFF", label: "")
                }
            }
        }
        updateMarkerButtonsAppearance()
    }
    
    //MARK: - Markers
    
    @IBAction func colorChangedButtonTouchUpInside(_ sender: Any) {
        ccButton.isHighlighted = true
        if let currentRoast = AppCore.sharedInstance.roastManager.currentRoast {
            if currentRoast.colorChange != nil {
                currentRoast.colorChange = nil
                tempChartView?.removeHighlightForColarChangeDataset()
            } else {
                if let currentTempPoint = currentRoast.roastPointsWithoutPreheating().last {
                    updateMarkerButtonsAppearance()
                    tempChartView?.colorChangeSet.clear()
                    currentRoast.colorChange = CGPoint(x: CGFloat(currentTempPoint.time), y: CGFloat(currentTempPoint.temp_above))
                    
                    if ccEdited {
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_CC_EDIT", label: "")
                    } else {
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_CC", label: "")
                        ccEdited = true
                    }
                    
                }
            }
        }
        updateStackView()
        //highlightButton()
        ccButton.isHighlighted = true
    }
    
    @IBAction func firstCrackButtonTouchUpInside(_ sender: Any) {
        if let currentRoast = AppCore.sharedInstance.roastManager.currentRoast {
            if currentRoast.secondCrack == nil {
                if currentRoast.firstCrack != nil {
                    currentRoast.firstCrack = nil
                    tempChartView?.removeHighlightForFirstCrackDataset()
                } else {
                    if let currentTempPoint = currentRoast.roastPointsWithoutPreheating().last {
                        updateMarkerButtonsAppearance()
                        tempChartView?.firstCrackSet.clear()
                        currentRoast.firstCrack = CGPoint(x: CGFloat(currentTempPoint.time), y: CGFloat(currentTempPoint.temp_above))
                        if fcEdited {
                            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_FC_EDIT", label: "")
                        } else {
                            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_FC", label: "")
                            fcEdited = true
                        }
                    }
                }
            }
        }
        updateStackView()
    }
    
    @IBAction func secondCrackButtonTouchUpInside(_ sender: Any) {
        if let currentRoast = AppCore.sharedInstance.roastManager.currentRoast {
            if currentRoast.secondCrack != nil {
                currentRoast.secondCrack = nil
                tempChartView?.removeHighlightForSecongCrackDataset()
            } else {
                if let currentTempPoint = currentRoast.roastPointsWithoutPreheating().last {
                    updateMarkerButtonsAppearance()
                    tempChartView?.secondCrackSet.clear()
                    currentRoast.secondCrack = CGPoint(x: CGFloat(currentTempPoint.time), y: CGFloat(currentTempPoint.temp_above))
                    if scEdited {
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_SC_EDIT", label: "")
                    } else {
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ROAST_SC", label: "")
                        scEdited = true
                    }
                    
                }
            }
        }
        updateStackView()
    }
    
    @objc func degreeCelsiusChanged() {
        guard profile != nil else {return}
        swipeStatusBarView.profile = profile
        tempChartView?.populateChart(profile:profile, editedProfile: editedProfile)
    }
    
    @objc func didLogout() {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    //MARK: - Private
    func prepareAppearance() {
        sendProfileProgressView.backgroundColor = UIColor.white
        
        cancelButton.layer.cornerRadius = cancelButton.frame.height / 2
        cancelButton.layer.borderWidth = 1
        cancelButton.layer.borderColor = UIColor.white.cgColor
        
        saveButton.layer.shadowColor = UIColor.black.cgColor
        saveButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        saveButton.layer.shadowOpacity = 0.3
        saveButton.layer.shadowRadius = 2.0
        //saveButton.layer.masksToBounds =  false
        saveButton.layer.cornerRadius = saveButton.frame.size.height/2
        
        sendToRoastButton.layer.shadowColor = UIColor.black.cgColor
        sendToRoastButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        sendToRoastButton.layer.shadowOpacity = 0.3
        sendToRoastButton.layer.shadowRadius = 2.0
        //sendToRoastButton.layer.masksToBounds =  false
        sendToRoastButton.titleLabel?.lineBreakMode = .byWordWrapping
        sendToRoastButton.titleLabel?.numberOfLines = 2
        sendToRoastButton.layer.cornerRadius = sendToRoastButton.frame.size.height/2

        guard let profile = profile else {return}
        swipeStatusBarView.profile = profile
        if tempChartView == nil {
            tempChartView = IKTempChartView()
        }
        chartViewContainer.addSubview(tempChartView!)
        tempChartView!.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(self.chartViewContainer).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
        showTemperatureGraph()
        updateAppearance()
        if profile.archived {
            setupAppearanceForArchivedProfile()
        }
    }
    
    private func setupAppearanceForArchivedProfile() {
        menuContainerWidthConstraint.constant = 0
        menuContainer.isHidden = true
        archivedLabel.isHidden = false
        if profile?.isRoasterProfile() ?? false {
            chartHeaderView.titleLabel.text = "$roaster_up".localized
        } else {
            chartHeaderView.titleLabel.text = "$archived_profile_uppercased".localized
        }
        restoreButton.layer.cornerRadius = restoreButton.bounds.height / 2
        restoreButton.isHidden = false
        sendToRoastButton.isHidden = true
        shareButton.isHidden = true
    }
    
    @IBAction func restoreButtonTouchUpInside(_ sender: Any) {
        guard let profile = profile else {return}
        if AppCore.sharedInstance.profileManager.restoreProfile(profile: profile) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func updateAppearance() {
        swipeStatusBarView.profile = profile
        if isEditState() {
            swipeStatusBarView.statusBarView.statusLabel.text = "$editing_profile".localized
        }
        if (profile?.isRoasterProfile() ?? false) || sendingProfileToRoaster == true {
            chartHeaderView.titleLabel.text = "$roaster_up".localized
            NavigationHelper.highlightRoastTab()
            swipeStatusBarView.statusBarView.thumbnail.isHidden = false
        }
        updateButtonsAppearanceWithState()
        updateBackButtonAppearance()
    }
    
    func updateOrientationAppearance() {
        let isLandscape = UIDevice.current.orientation.isFlat ? UIApplication.shared.statusBarOrientation.isLandscape : UIDevice.current.orientation.isLandscape
        if  isLandscape {
            tabBarController?.tabBar.isHidden = true
            if (tempChartView != nil) {
                tempChartView!.chartView.setVisibleXRangeMinimum(Double(4.5))
            }
        } else {
            tabBarController?.tabBar.isHidden = false
            if (tempChartView != nil) {
                tempChartView!.chartView.setVisibleXRangeMinimum(Double(2.5))
            }
        }
    }
    
    
    private func sensorButtonIsHidden() -> Bool {
        if isEditState() {
            if (RoasterManager.instance().roaster != nil && self.profile != nil && self.isRoasterProfile(profile: self.profile!)) && RoasterManager.instance().roaster.status.state != IK_STATUS_IDLE {
                if editedProfile?.tempSensor != profile?.tempSensor && profile?.tempSensor != nil {
                    editedProfile?.tempSensor = profile?.tempSensor ?? TempSensorBelow
                    updateSensorButtonAppearance()
                    showTemperatureGraph()
                }
                if buttonEditState && !sensorButtonView.isHidden{
                    let walkthroughModalView = WalkthroughModalView()
                    walkthroughModalView.titleIsHidden = true
                    walkthroughModalView.setupHorisontalView(image: nil, title: "$changing_the_sensor_type_during_a_roast".localized, index: .First)
                    walkthroughModalView.titleButton = "$ok".localized
                    walkthroughModalView.show(animated: true, dismissCallback: nil)
                }

                return true
            } else {
                return false
            }
        } else {
            return true
        }

    }
    
    func updateStackView() {
        self.sensorButtonView.isHidden = self.sensorButtonIsHidden()
        if buttonEditState != isEditState() {
            UIView.animate(withDuration: 0.2, animations: {
                self.editView.isHidden = self.isEditState()
                self.editView.alpha = self.isEditState() ? 0 : 1
                self.tableEditView.isHidden = !self.isEditState()
                self.tableEditView.alpha = !self.isEditState() ? 0 : 1
                self.sensorButtonView.isHidden = self.sensorButtonIsHidden()
                self.rorView.isHidden = false
                self.colorChangeView.isHidden = self.isEditState()
                self.colorChangeView.alpha = self.isEditState() ? 0 : 1
                self.secondCrackView.alpha = self.isEditState() ? 0 : 1
                self.secondCrackView.isHidden = self.isEditState()
                self.firstCrackView.isHidden = self.isEditState()
                
                self.cancelButton.isHidden = !self.isEditState()
                self.cancelButton.alpha = !self.isEditState() ? 0 : 1
                
                self.cancelButtonView.isHidden = !self.isEditState()
                self.rorButtonTopConstraint.constant = self.isEditState() ? 20 : 40
                self.menuContainer.layoutIfNeeded()
            })
            buttonEditState = isEditState()
        }
        
        func enableMarkerButtons(isEnabled: Bool) {
            if let currentRoast = AppCore.sharedInstance.roastManager.currentRoast {
                ccButton.isEnabled = isEnabled
                firstCrackButton.isEnabled = isEnabled//(currentRoast.colorChange != nil) ? isEnabled : false
                secondCrackButton.isEnabled = (currentRoast.firstCrack != nil) ? isEnabled : false
                self.tempChartView?.enableMarkersEditing(enable: isEnabled)
            } else {
                ccButton.isEnabled = false
                firstCrackButton.isEnabled = false
                secondCrackButton.isEnabled = false
                self.tempChartView?.enableMarkersEditing(enable: false)
            }
            
        }
        
        if self.isRoasterProfile(profile: self.profile!) == true {
            let state = RoasterManager.instance().roaster.status.state
            if state == IK_STATUS_ROASTING { //roasting
                enableMarkerButtons(isEnabled: true)
            } else {
                enableMarkerButtons(isEnabled: false)
            }
        } else {
            enableMarkerButtons(isEnabled: false)
        }
        updateMarkerButtonsAppearance()
    }
    
    private func updateMarkerButtonsAppearance() {
        DispatchQueue.main.async {
            if let currentRoast = AppCore.sharedInstance.roastManager.currentRoast {
                self.ccButton.setImage(UIImage(named: currentRoast.colorChange != nil ? "ColorChange_Highlight" : "ColorChange_Active") , for: .normal)
                self.firstCrackButton.setImage(UIImage(named: currentRoast.firstCrack != nil ? "FirstCrack_Highlight" : "FirstCrack_Active") , for: .normal)
                self.secondCrackButton.setImage(UIImage(named: currentRoast.secondCrack != nil ? "SecondCrack_Highlight" : "SecondCrack_Active") , for: .normal)
            }
        }
        updateRorButtonAppearance()
    }
    
    private func updateRorButtonAppearance() {
        DispatchQueue.main.async {
            if let tempChartView = self.tempChartView {
                self.rorButton.setImage(UIImage(named: tempChartView.isRORHidden ? "RofR_Active" : "RofR_Highlight") , for: .normal)
            }
        }
    }
    
    func updateButtonsAppearanceWithState() {
        updateStackView()
        if isEditState() {
            saveButton.isHidden = false
            sendToRoastButton.isHidden = true
            shareButton.isHidden = true
            return
        } else {
            if (RoasterManager.instance().roaster != nil && self.profile != nil && self.isRoasterProfile(profile: self.profile!)) {
                let state = RoasterManager.instance().roaster.status.state
                if state == IK_STATUS_ROASTING { //roasting
                    saveButton.isHidden = true
                    sendToRoastButton.isHidden = true
                    shareButton.isHidden = true
                    return
                } else {
                    saveButton.isHidden = true
                    sendToRoastButton.isHidden = true
                    shareButton.isHidden = true
                    shareButtonCenteringConstraint.priority = UILayoutPriority(rawValue: UILayoutPriority.RawValue(sendToRoastButton.isHidden ? 999 : 100))
                }
            } else {
                saveButton.isHidden = true
                if RoasterManager.instance().roaster == nil {
                    sendToRoastButton.isHidden = true
                    shareButton.isHidden = (profile?.archived ?? false) ? true : false
                    shareButtonCenteringConstraint.priority = UILayoutPriority(rawValue: UILayoutPriority.RawValue(sendToRoastButton.isHidden ? 999 : 100))
                    
                } else if RoasterManager.instance().roaster != nil {
                    let state = RoasterManager.instance().roaster.status.state
                    sendToRoastButton.isHidden = (profile?.archived ?? false) ? true : state != IK_STATUS_IDLE
                    shareButtonCenteringConstraint.priority = UILayoutPriority(rawValue: UILayoutPriority.RawValue(sendToRoastButton.isHidden ? 999 : 100))
                    shareButton.isHidden = (profile?.archived ?? false) ? true : sendingProfileToRoaster
                }
                return
            }
        }
       
    }
    

    
    func updateSensorButtonAppearance()  {
        if let profile = editedProfile {
            if (profile.tempSensor == TempSensorBelow) {
                sensorButton.setImage(UIImage(named: "Exhaust"), for: .normal)
            } else {
                sensorButton.setImage(UIImage(named: "Inlet"), for: .normal)
            }
        }

        
//        if isEditState() {
//            if (RoasterManager.instance().roaster != nil && self.profile != nil && self.isRoasterProfile(profile: self.profile!)) {
//                let state = RoasterManager.instance().roaster.status.state
//                if state != IK_STATUS_IDLE { //roasting
//                    sensorButton.isEnabled = false
//                } else {
//                    sensorButton.isEnabled = true
//                }
//            } else {
//                sensorButton.isEnabled = true
//            }
//        } else {
//            sensorButton.isEnabled = false
//        }
    }
    
    func updateBackButtonAppearance() {
        
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        if let tabBarController = appDelegate.window!.rootViewController as? IKTabBarController, tabBarController.highlightTabIndex() != 1  {
            swipeStatusBarView.statusBarView.showBackButton {
                if (self.editedProfile != nil && (self.tempChartView!.graphWasEdited) || self.profileHasChanged) {
                    let confirmationDialogView = ButtonsPopupModalView()
                    confirmationDialogView.layout = .vertical
                    
                    confirmationDialogView.title = "$cancel_edit".localized
                    confirmationDialogView.subtitle = "$cancel_without_saving".localized
                    confirmationDialogView.setUpButton(index: .First, title: "$yes_up".localized, type: .Filled) {
                        self.navigationController?.popViewController(animated: true)
                    }
                    confirmationDialogView.setUpButton(index: .Second, title: "$no_up".localized, type: .Cancel) {
                        
                    }
                    
                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        } else {
            swipeStatusBarView.statusBarView.hideBackButton()
        }
    }
    
    func updateDTR() {
        guard let roast = AppCore.sharedInstance.roastManager.currentRoast, let firstCrackPoint = roast.firstCrack, let roastingTime = roast.lastRoastingPoint()?.time  else {
            dtrContainerView.isHidden = true
            return
        }
        dtrContainerView.isHidden = false
        
        let roastingTime_r = round(roastingTime)
        let firstCrackPointTime_r = round(firstCrackPoint.x)
        let dtr = Double((roastingTime_r - Float(firstCrackPointTime_r)) / roastingTime_r * 100)
        
        dtrLabel.text = String(format: "DTR\n%.1f%%", dtr)
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, animations: {
                self.dtrContainerView.alpha = self.isEditState() ? 0 : 1
            })
        }
    }
    
    func updateChartIfNeeded() {
        if (chartViewContainer.subviews.count > 0 && AppCore.sharedInstance.roastManager.currentRoast?.profileUuid == self.profile?.uuid) {
            let chartView = chartViewContainer.subviews.last as! ChartViewProtocol
            chartView.updateTempPoint(roast: AppCore.sharedInstance.roastManager.currentRoast)
        }
    }
    
    func save(callback: @escaping (_ savedProfile: Profile?)->Void) {
        
        if let userId = AppCore.sharedInstance.firebaseManager.currentUser?.uid {
            editedProfile?.userId = userId
        }
        if (profile?.isReadOnly())! {
            AppCore.sharedInstance.profileManager.showSaveProfileAsCopyAlert(profile: editedProfile!, title: nil) { (saveState, profile) in
                if (saveState == .SAVED_AS || saveState == .OVERWRITTEN) {
                    let needSendToRoaster = self.isRoasterProfile(profile: self.profile!)
                    self.profile = profile
                    self.goToViewState()
                    if needSendToRoaster {
                        self.sendProfile(profile: self.profile)
                    }
                } else {
                    
                }
                callback(profile)
            }
        } else {
            AppCore.sharedInstance.profileManager.saveProfile(profile: editedProfile!, existsText: NSLocalizedString("$how_to_save", comment: "")) { (saveState, profile) in
                if (profile != nil) {
                    let needSendToRoaster = self.isRoasterProfile(profile: self.profile!)
                    self.profile = profile
                    self.goToViewState()
                    if needSendToRoaster {
                        self.sendProfile(profile: self.profile)
                    }
                } else {
                    let confirmationDialogView = ButtonsPopupModalView()
                    confirmationDialogView.layout = .vertical
                    confirmationDialogView.title = "Saving profile error".localized
                    confirmationDialogView.subtitle = ""
                    confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                }
                callback(profile)
            }
        }
    }
    
    func showTemperatureGraph() {
        tempChartView!.populateChart(profile:profile, editedProfile: editedProfile)
        if (self.editedProfile != nil) {
            if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile?.uuid) { //On machine already
                AppCore.sharedInstance.loggingManager.trackScreen(name: "Profile Edit On Roaster Screen")
            } else {
                AppCore.sharedInstance.loggingManager.trackScreen(name: "Profile Edit Screen")
            }
            tempChartView?.goToEditState()
        } else {
            if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile?.uuid) { //On machine already
                AppCore.sharedInstance.loggingManager.trackScreen(name: "Profile On Roaster Scene")
            } else {
                AppCore.sharedInstance.loggingManager.trackScreen(name: "Profile Screen")
            }
            tempChartView?.goToViewState()
        }
        updateChartIfNeeded()
    }
    
    func goToEditState() {
        if let editedProfile =  self.profile?.copy() as? Profile {
            sensorButton.setImage(UIImage(named: (profile?.tempSensor == TempSensorBelow) ? "Exhaust" : "Inlet"), for: .normal)
            self.editedProfile = editedProfile
            self.editedProfile?.uuid = AppCore.sharedInstance.profileManager.getRandomUuid()
            self.editedProfile?.parentUuid = self.profile?.uuid ?? ""
            self.editedProfile?.type = ProfileTypeUser
            swipeStatusBarView.statusBarView.statusLabel.text = "$editing_profile".localized
            showTemperatureGraph()
            updateButtonsAppearanceWithState()
        }
    }

    func goToViewState() {
        profileHasChanged = false
        self.title = profile?.name
        editedProfile = nil
        showTemperatureGraph()
        swipeStatusBarView.profile = profile
        updateButtonsAppearanceWithState()
    }
    
    func sendProfile(profile: Profile?) {
        guard profile != nil else {
            return
        }
        sendProfileProgressView.startAnimation()
        if (!RoasterManager.instance().roaster.hasBelowSensor() && profile?.tempSensor == TempSensorBelow) {
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = "$profile_type_not_supported".localized
            confirmationDialogView.subtitle = "$inlet_sensor_profile_not_supported_message".localized
            confirmationDialogView.setUpButton(index: .First, title: "$ok".localized, type: .Filled) {
                self.sendProfileProgressView.stopAnimation()
            }
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            return
        } else if (!RoasterManager.instance().roaster.hasAboveSensor() &&
            (profile?.tempSensor == TempSensorAbove || profile?.tempSensor == TempSensorAboveRobust)) {
            
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$profile_unsupported", comment: "")
            confirmationDialogView.subtitle = ""
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                self.sendProfileProgressView.stopAnimation()
            }
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            return
        }
        sendingProfileToRoaster = true
        
        RoasterManager.instance().sendProfile(profile)
    }
    
    func isRoasterProfile(profile: Profile) -> Bool {
        if RoasterManager.instance().roaster != nil, let roasterProfile = RoasterManager.instance().profile {
            return profile.uuid == roasterProfile.uuid
        }
        return false
    }
    
    func isEditState() -> Bool {
        return editedProfile != nil
    }
}


extension IKChartVC : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        updateAppearance()
        //chartHeaderView.titleLabel.text = "$roaster_up".localized
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        updateAppearance()
        chartHeaderView.titleLabel.text = "$roast_profile_up".localized
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        updateAppearance()
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        updateAppearance()
        guard AppCore.sharedInstance.roastManager.currentRoast != nil else {
            return
        }
        
        updateChartIfNeeded()
        if profile?.isRoasterProfile() ?? false {
            updateDTR()
        }
        
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        if (self.sendingProfileToRoaster) {
            sendProfileProgressView.stopAnimation()
            UIView.animate(withDuration: 0.5, animations: {
                self.completeImageView.isHidden = false
            }, completion: { (success) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.completeImageView.isHidden = true
                }
            })
            self.sendingProfileToRoaster = false
            sendToRoastButton.isHidden = true
            //shareButton.isHidden = true
        }
        updateAppearance()
    }
}

extension IKChartVC: ChartHeaderViewDelegate {
    func didTapMenuButton() {
        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let nc = menuStoryboard.instantiateInitialViewController()
        present(nc!, animated: true) {}
    }
}
