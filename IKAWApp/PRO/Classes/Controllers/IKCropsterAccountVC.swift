//
//  IKCropsterAccountVC.swift
//  IKAWA-Pro
//
//  Created by Admin on 8/15/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class IKCropsterAccountVC: UIViewController {

    @IBOutlet weak var logInCropsterButton: UIButton!
    @IBOutlet weak var logOutCropsterButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var signInTitleLabel: UILabel!
    @IBOutlet weak var signInContainerView: UIView!
    @IBOutlet weak var accountContainerView: UIView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var requestIntegrationButton: UIButton!
    
    @IBOutlet weak var learMoreButton: UIButton!
    
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImageView.layer.masksToBounds = true
        
        logInCropsterButton.layer.borderColor = UIColor.white.cgColor
        logInCropsterButton.layer.borderWidth = 1
        logInCropsterButton.layer.cornerRadius = logInCropsterButton.frame.height / 2

        logOutCropsterButton.layer.borderColor = UIColor.white.cgColor
        logOutCropsterButton.layer.borderWidth = 1
        logOutCropsterButton.layer.cornerRadius = logOutCropsterButton.frame.height / 2
        
        NotificationCenter.default.addObserver(self, selector: #selector(didLoginToCropster), name: NSNotification.Name.DidLoginToCropster, object: nil)
        requestIntegrationButton.setTitle(title: "$cropster_request_integration".localized.uppercased())
        learMoreButton.setTitle(title: "$learn_more".localized.uppercased())
        updateUppearance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppCore.sharedInstance.syncManager.getUserInfo { [weak self] (userInfo) in
            self?.profileName.text = "\(userInfo?.accountName ?? "")"
        }
        loadAvatar()
    }
    
    private func loadAvatar() {
        CasheManager.sharedInstance.getAvatarImage { (image, error) in
            if let image = image {
                self.profileImageView.image = image
            } else {
                self.profileImageView.image = UIImage(named: "UserPlaceholder")
            }
        }
    }

    func updateUppearance() {
        if AppCore.sharedInstance.cropsterManager.isInitialized {
            signInTitleLabel.text = "$you_are_connected_to_cropster".localized
            emailLabel.text = AppCore.sharedInstance.cropsterManager.email
            signInContainerView.isHidden = true
            accountContainerView.isHidden = false
        } else {
            signInTitleLabel.text = "$connect_your_with_cropster below".localized
            signInContainerView.isHidden = false
            accountContainerView.isHidden = true
        }
    }
    
    override func viewDidLayoutSubviews() {
        profileImageView.layer.cornerRadius = profileImageView.bounds.width / 2
    }
    
    //MARK: Actioins
    
    @IBAction func requestIntegrationButtonTouchUpInside(_ sender: Any) {
        if let url = URL(string:"https://docs.google.com/forms/d/e/1FAIpQLSfVzgZVnMtc-m0-1-IMldSFaJ6Acr60NHkZ0BZ4YRq1doljdQ/viewform") {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func lernMoreButtonTouchUpInside(_ sender: Any) {
        if let url = URL(string:"https://www.ikawacoffee.com/for-professionals/cropster/") {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func logInCropsterButtonTouchUpInside(_ sender: Any) {
        if let url = URL(string: "https://c-sar.cropster.com/ikawa") {/*https://c-sar.cropster.com/group/apiaccess?machineType=Ikawa*/
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func logOutCropsterButtonTouchUpInside(_ sender: Any) {
        AppCore.sharedInstance.cropsterManager.logOut()
        updateUppearance()
    }
    
    
    @IBAction func backButtonTouchUpInside(_ sender: Any) {
        if let nc = navigationController {
            nc.popViewController(animated: true)
        } else {
            dismiss(animated: true) {
                
            }
        }
    }
    
    @objc func didLoginToCropster() {
        updateUppearance()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
