//
//  IKMenuVC_Home.swift
//  IKAWApp
//
//  Created by Admin on 3/21/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib
import SDWebImage
import FirebaseUI
import Alamofire

class IKMenuVC: IKBaseVC {
    
    enum MenuItem {
        case menuCreateNewRecipe
        case menuArchivedProfiles
        case menuOnlineLibrary
        case menuInformation
        case menuAccount
        case menuSettings
        case menuCropster
        case menuLogout
        case menuDisconnect
    }
    
    /*
    let menuItems: [MenuItem] = [
        .menuCreateNewRecipe,
        .menuArchivedProfiles,
        .menuOnlineLibrary,
        .menuInformation,
        .menuAccount,
        .menuSettings,
        .menuCropster,
        .menuLogout
    ]*/
    var menuItems: [MenuItem] = [
        .menuOnlineLibrary,
        .menuArchivedProfiles,
        .menuAccount,
        .menuInformation
    ]
    
    
    // MARK: - Constnats
    
    private struct Constants {
        static let tutorialUrlString: String = "https://www.ikawacoffee.com/for-professionals/pro-app-3"
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var simulatorSwitch: UISwitch!
    @IBOutlet weak var debugInformationSwitch: UISwitch!
    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var settingsView: UIView!
    
    let userPlaceholderImage = UIImage(named: "MenuImagePlaceholder")
    @IBAction func backupButtonTouchUpInside(_ sender: Any) {
        
        let userId = /*AppCore.sharedInstance.firebaseManager.currentUser!.uid */"Egv0hjcH4kNrI105U0xeJhrTHwX2"
        let tool = ToolsFirebaseManager()
        tool.deleteRecentBackups(userId: userId) { (error) in
            tool.deleteBackupsAfterAppUpdate(userId: userId)
        }
        tool.restoreBackup(userId: userId)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        versionLabel.backgroundColor = kColorDarkGrey1
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelButton)))
        #if TARGET_DEV
        /*
        versionLabel.text = "Version: \((Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String) + "  Build:") \(Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String)"*/
        versionLabel.text = ""
        #else
        versionLabel.text = ""
        #endif

        prepareAppearance()
        if Configuration.environment != "DEV" {
          //  tableView.tableFooterView = nil
        }
        simulatorSwitch.isOn = IK_SIMULATE_ROASTER
        debugInformationSwitch.isOn = IK_SHOW_DEBUG_INFORMATIOMN
        view.backgroundColor = .clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.layoutIfNeeded()
        view.updateConstraintsIfNeeded()
        if IKInterface.instance()?.isConnected() ?? false {
            menuItems = [
                .menuOnlineLibrary,
                .menuArchivedProfiles,
                .menuAccount,
                .menuInformation,
                .menuDisconnect
            ]
        } else {
            menuItems = [
                .menuOnlineLibrary,
                .menuArchivedProfiles,
                .menuAccount,
                .menuInformation
            ]
        }
        loadAvatar()
        updateContent()
        RoasterManager.instance().delegates.add(self)
        NotificationCenter.default.addObserver(self, selector: #selector(updateContent), name: Notification.Name.DidChangeUserData, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profileImage.layer.cornerRadius = profileImage.bounds.width / 2
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    // MARK: - Private
    @objc private func settingsAction() {
        UIView.animate(withDuration: 0.2, animations: {
            self.settingsView.backgroundColor = #colorLiteral(red: 0.4606838916, green: 0.4554176879, blue: 0.4350762527, alpha: 1)
        }, completion: { (complete) in
            UIView.animate(withDuration: 2, animations: {
                self.settingsView.backgroundColor = kColorDarkGrey1
            })
        })
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            } else {
                UIApplication.shared.openURL(settingsUrl)
            }
        }
    }
    
    func prepareAppearance() {
        self.navigationController!.navigationBar.barStyle = .default
        profileView.backgroundColor = kColorDarkGrey1
        if let profileDividerView = profileView.viewWithTag(100) {
            profileDividerView.backgroundColor = kColorDarkGrey1
        }
        profileName.textColor = UIColor.white
        
        view.backgroundColor = kMainColor
        tableView.backgroundColor = kColorDarkGrey1
//        if AppCore.sharedInstance.firebaseManager.currentUser?.email == nil {
//            tableView.tableHeaderView = nil
//        } else {
            profileImage.layer.cornerRadius = profileImage.bounds.width / 2
            profileImage.layer.masksToBounds = true
            profileImage.image = userPlaceholderImage //UIImage(named: "UserPlaceholder")
    //    }
    }
    
    @objc func updateContent() {
        profileName.text = ""
        if AppCore.sharedInstance.settingsManager.workingWithoutAccount {
            profileName.text = "$login_sign_in".localized
        } else {
            AppCore.sharedInstance.syncManager.getUserInfo { (userInfo) in
                self.profileName.text = userInfo?.accountName ?? ""
            }
            
        }
    }
    
    func loadAvatar() {
        CasheManager.sharedInstance.getAvatarImage { (image, error) in
            if let image = image {
                self.profileImage.layer.cornerRadius = self.profileImage.bounds.width / 2
                self.profileImage.image = image
            } else {
                self.profileImage.layer.cornerRadius = 0
                self.profileImage.image = self.userPlaceholderImage //UIImage(named: "UserPlaceholder")
            }
        }
    }
    
    //MARK: - Actions
    
    @IBAction func tapOnSettings(_ sender: Any) {
        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_MENU_SETTINGS", label: "")
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            } else {
                UIApplication.shared.openURL(settingsUrl)
            }
        }
    }
    
    @IBAction func tapOnAccount(_ sender: Any) {
        dismiss(animated: true, completion: {})
        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_MENU_ACCOUNT", label: "")
        if AppCore.sharedInstance.settingsManager.workingWithoutAccount {
            NavigationHelper.GoToLoginScreen()
            AppCore.sharedInstance.settingsManager.workingWithoutAccount = false
        } else {
            NavigationHelper.GoToAccountScreen()
        }
        

    }
    
    @IBAction func simulatorSwitchValueChanged(_ sender: Any) {
        IK_SIMULATE_ROASTER = simulatorSwitch.isOn
        IK_SHOW_DEBUG_INFORMATIOMN = debugInformationSwitch.isOn
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        dismiss(animated: true, completion: {})
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
}

extension IKMenuVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier:"Cell", for: indexPath)
        cell.contentView.backgroundColor = kColorDarkGrey1
        
        let titleLabel = cell.viewWithTag(11) as! UILabel
        titleLabel.textColor = UIColor.white
        
        
        let thumbnail = cell.viewWithTag(12) as! UIImageView
        
        
        switch menuItems[indexPath.row] {
        case .menuCreateNewRecipe:
            titleLabel.text = "$menu_create_new_recipe".localized
            thumbnail.image = UIImage.init(named: "MenuPlus")
        case .menuArchivedProfiles:
            titleLabel.text = "$archived_profiles".localized
            thumbnail.image = UIImage.init(named: "menuArchived")
        case .menuOnlineLibrary:
            titleLabel.text = "$menu_online_library".localized
            thumbnail.image = UIImage.init(named: "OnlineLibrary")
        case .menuInformation:
            titleLabel.text = "$menu_information".localized
            thumbnail.image = UIImage.init(named: "MenuTutorial")
        case .menuAccount:
            titleLabel.text = "$menu_account".localized
            thumbnail.image = UIImage.init(named: "MenuAccount")
        case .menuSettings:
            titleLabel.text = "$menu_settings".localized
            thumbnail.image = UIImage.init(named: "MenuSettings")
        case .menuCropster:
            if AppCore.sharedInstance.cropsterManager.isInitialized {
                titleLabel.text = "$cropster_disconnect".localized
                thumbnail.image = UIImage.init(named: "cropsterIcon")
            } else {
                titleLabel.text = "$cropster_connect".localized
                thumbnail.image = UIImage.init(named: "cropsterIcon")
            }
        case .menuLogout:
            titleLabel.text = "$menu_logout".localized
            thumbnail.image = UIImage.init(named: "MenuLogout")
        case .menuDisconnect:
            titleLabel.text = "$menu_disconnect".localized
            thumbnail.image = UIImage.init(named: "MenuBluetoothDisabled")
        
        }
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = kColorLightGrey3
        cell.selectedBackgroundView = bgColorView
        if let dividerView = cell.viewWithTag(100) {
            dividerView.isHidden = true
            //dividerView.backgroundColor = kColorLightGrey2
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch menuItems[indexPath.row] {
        
        case .menuArchivedProfiles:
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_MENU_ARCHIVE_LIBRARY", label: "")
            dismiss(animated: true, completion: {})
            NavigationHelper.GoToArchivedProfileScreen()
            
        case .menuSettings: //settings
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
        case .menuInformation: //info
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_MENU_TUTORIAL", label: "")
            guard let url = URL(string: Constants.tutorialUrlString ) else { return }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
            //NavigationHelper.GoToTutorialScreen()
            dismiss(animated: true, completion: {
                
            })
            
        case .menuLogout: //info
            UserDefaults.standard.set("", forKey: "lastUsedEmail")
            if NetworkReachabilityManager()?.isReachable ?? false {
                if UserDefaults.standard.bool(forKey: "db_changed") {
                    UIApplication.shared.keyWindow?.makeToastActivity(.center)
                    AppCore.sharedInstance.syncManager.stopSynchronization()
                    AppCore.sharedInstance.syncManager.uploadUserLibrary(roundTimestamp: false, callback: { (error) in
                        if let error = error {
                            let errorDialogView = ButtonsPopupModalView()
                            errorDialogView.layout = .vertical
                            errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                            errorDialogView.subtitle = error.localizedDescription
                            errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                            
                            IKNotificationManager.sharedInstance.show(view: errorDialogView)
                        }
                        UIApplication.shared.keyWindow?.hideToastActivity()
                        self.logout()
                        
                    })
                    
                } else {
                    AppCore.sharedInstance.syncManager.stopSynchronization()
                    logout()
                }
                
            } else {
                let errorDialogView = ButtonsPopupModalView()
                errorDialogView.layout = .vertical
                errorDialogView.title = NSLocalizedString("$no_internet_connection", comment: "")
                errorDialogView.subtitle = ""
                errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                }
                IKNotificationManager.sharedInstance.show(view: errorDialogView)
            }

        case .menuCropster:
            dismiss(animated: true, completion: {})
            NavigationHelper.GoToCropsterAccountScreen()
            
        case .menuAccount:
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_MENU_ACCOUNT", label: "")
            dismiss(animated: true, completion: {})
            if AppCore.sharedInstance.settingsManager.workingWithoutAccount {
                NavigationHelper.GoToRegistrationScreen()
                AppCore.sharedInstance.settingsManager.workingWithoutAccount = false
            } else {
                NavigationHelper.GoToAccountScreen()
            }
            
            
        case .menuCreateNewRecipe:
            dismiss(animated: true, completion: {})
            AddReceiptManager.sharedInstance.showAddRecepieDialogView()
            
        case .menuOnlineLibrary:
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_MENU_ONLINE_LIBRARY", label: "")
            if let url = URL(string: "https://www.ikawacoffee.com/pro-sample-roaster-profiles/") {
                UIApplication.shared.openURL(url)
            }
        case .menuDisconnect:
            dismiss(animated: true, completion: {})
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_MENU_DISCONNECT", label: "")
            IKInterface.instance()?.reconnect()
            
        default: break
            
        }
    }
    
    func logout() {
        AppCore.sharedInstance.logout()
        NavigationHelper.GoToLoginScreen()
    }
}

extension IKMenuVC : RoasterManagerDelegate {
    //MARK: - RoasterManagerDelegate
    
    func roasterDidConnect(_ roaster: Roaster!) {
        print("!!!!!!!!!  roasterDidConnect")
        menuItems = [
            .menuOnlineLibrary,
            .menuArchivedProfiles,
            .menuAccount,
            .menuInformation,
            .menuDisconnect
        ]
        tableView.reloadData()
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        print("!!!!!!!!!  roasterDidDisconnect")
        menuItems = [
            .menuOnlineLibrary,
            .menuArchivedProfiles,
            .menuAccount,
            .menuInformation
        ]
        tableView.reloadData()
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
    }
}






