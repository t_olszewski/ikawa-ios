//
//  IKProfileLibVC.swift
//  IKAWApp
//
//  Created by Admin on 3/22/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib
import SwipeCellKit

private enum ProfileListType {
    case favourites
    case all
    case search
}

class IKProfileLibVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerView: ProfileLibHeaderView!
    @IBOutlet weak var allProfileButton: UIButton!
    @IBOutlet weak var allProfileBorderView: UIView!
    @IBOutlet weak var favouritesProfileButton: UIButton!
    @IBOutlet weak var switchView: UIView!
    @IBOutlet weak var switchViewHeightConstraint: NSLayoutConstraint!
    var swipeCellTransitionContext: SwipeActionTransitioningContext?
    var displayArray: [Profile]?
    var searchStr = ""
    
    
    private var currentProfileListType: ProfileListType = .all {
        
        willSet {
            previousProfileListType = self.currentProfileListType
        }
        didSet {
            switch currentProfileListType {
            case .all:
                AppCore.sharedInstance.profileManager.sortType = .DATE_LAST_USED_DESC
                updateTable()
                displayArray = (AppCore.sharedInstance.profileManager.profilesInLibrary ?? [Profile]()).filter { (profile) -> Bool in
                    !profile.archived
                }
            case .favourites:
                AppCore.sharedInstance.profileManager.sortType = .DATE_LAST_EDITED_DESC
                updateTable()
                displayArray = (AppCore.sharedInstance.profileManager.profilesInLibrary ?? [Profile]()).filter { (profile) -> Bool in
                    !profile.archived && profile.favorite
                }
            case .search:
                AppCore.sharedInstance.profileManager.sortType = .DATE_LAST_USED_DESC
                updateTable()
                displayArray = (AppCore.sharedInstance.profileManager.profilesInLibrary ?? [Profile]()).filter { (profile) -> Bool in
                    !profile.archived
                }
            }
            
        }
    }
    lazy private var previousProfileListType : ProfileListType = currentProfileListType
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentProfileListType = .all
        NotificationCenter.default.addObserver(self, selector:#selector(updateTable), name: NSNotification.Name.ProfileManagerDidChangeList, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(didLogout), name: NSNotification.Name.DidLogOut, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(degreeCelsiusChanged), name: NSNotification.Name.DegreeCelsiusChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openRoasterProfile), name: NSNotification.Name.OpenRoasterReceipt, object: nil)
        AppCore.sharedInstance.profileManager.sortType = .DATE_LAST_USED_DESC
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        

        _ = UITapGestureRecognizer(target: self, action: #selector(self.roasterProfileTap(sender:)))
        //statusBarView.addGestureRecognizer(tap)
       
        AppCore.sharedInstance.loggingManager.trackScreen(name: "Library Screen")
        
        headerView.delegate = self
     
        self.tableView.contentInset = UIEdgeInsets(top: self.tableView.contentInset.top, left: 0, bottom: 0, right: 0)
        
        prepareAppearance()
        RoasterManager.instance().delegates.add(self)
        if IKInterface.instance().isConnected() || IK_SIMULATE_ROASTER {
            headerView.swipeStatusBarView.statusBarView.populate(profile: RoasterManager.instance().profile)
            
        }
        headerView.updateStatusBarView()

        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTable()
        layoutHeaderViewDidChanged()
        navigationController?.isNavigationBarHidden = true
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
        updateSwitchView()
        //AppCore.sharedInstance.createFakeRoastNotres(count: 100)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        prepareAppearance()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "Chart" || segue.identifier == "OpenRoasterProfile") {
           let chartVC = segue.destination as! IKChartVC
            chartVC.profile = sender as? Profile
        }
    }
    
    //MARK: - Private
    
    @objc func keyboardNotification(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.tableView.contentInset = UIEdgeInsets(top: self.tableView.contentInset.top, left: 0, bottom: 0, right: 0)
            } else {
                self.tableView.contentInset = UIEdgeInsets(top: self.tableView.contentInset.top, left: 0, bottom: endFrame?.size.height ?? 0.0, right: 0)
            }

            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
 
    }
    
    func prepareAppearance() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 70.0
        updateTable()
    }
    
    private func updateSwitchView() {
        tableView.reloadData()
        var translationX = allProfileButton.bounds.width
        switch currentProfileListType {
        case .all :
            allProfileButton.setColor(color: kColorWhite)
            favouritesProfileButton.setColor(color: kColorLightGrey3)
            
            translationX = 0
        case .favourites :
            allProfileButton.setColor(color: kColorLightGrey3)
            favouritesProfileButton.setColor(color: kColorWhite)
        default:
            ()
        }

        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.allProfileBorderView.transform = CGAffineTransform(translationX: translationX , y: 0)
        }, completion: nil)

    }
    
    // MARK: - Actions
    
    @objc func roasterProfileTap(sender: UITapGestureRecognizer? = nil) {
        performSegue(withIdentifier: "Chart", sender: RoasterManager.instance().profile)
    }
    
    @objc func degreeCelsiusChanged() {
        tableView.reloadData()
    }
    
    @IBAction func allProfileButtonTouchUpInside(_ sender: Any) {
        if currentProfileListType != .all {
            currentProfileListType = .all
            updateSwitchView()
        }
    }
    
    @IBAction func favouritesButtonTouchUpInside(_ sender: Any) {
        if currentProfileListType != .favourites {
            currentProfileListType = .favourites
            updateSwitchView()
        }
    }
    
    @IBAction func openArhivedProfileList(_ sender: Any) {
                NavigationHelper.GoToArchivedProfileScreen()
    }
    /*
    private func getCountOfArchivedProfiles() -> Int {
        return (IKProfileManager.sharedInstance.profiles ?? [Profile]()).filter { (profile) -> Bool in
            return profile.archived
        }.count
    }
    */
    @objc func updateTable() {
        if searchStr.count > 0 {
            displayArray = (AppCore.sharedInstance.profileManager.profilesInLibrary ?? [Profile]()).filter { (profile) -> Bool in
                
                !profile.archived && (profile.name.lowercased().contains(searchStr.lowercased()) || profile.coffeeName.lowercased().contains(searchStr.lowercased()))
            }
            
        } else {
            switch currentProfileListType {
            case .all:
                displayArray = (AppCore.sharedInstance.profileManager.profilesInLibrary ?? [Profile]()).filter { (profile) -> Bool in
                    !profile.archived
                }
            case .favourites:
                displayArray = (AppCore.sharedInstance.profileManager.profilesInLibrary ?? [Profile]()).filter { (profile) -> Bool in
                    !profile.archived && profile.favorite
                }
            default: ()
            }
            
        }
        tableView.reloadData()
       // updateArchivedView()
    }
    
    @objc func didLogout() {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    
    /*
    private func favouritesProfile() -> [Profile] {
        return (IKProfileManager.sharedInstance.profilesInLibrary ?? [Profile]()).filter { (profile) -> Bool in
            !profile.archived && profile.favorite
        }
    }*/
}

extension IKProfileLibVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let displayArray = displayArray else {
            return UITableViewCell()
        }
        let profile = displayArray[indexPath.row] //isFavouritesPage ? favouritesProfile()[indexPath.row] : displayArray[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SwipeTableViewCell
        cell.delegate = self
        let profileViewCell = cell.contentView.viewWithTag(50) as! IKProfileViewCell
        profileViewCell.populate(profile: profile)
        //cell.clipsToBounds = true
        let selecedBackgroundView = UIView()
        selecedBackgroundView.backgroundColor = kColorLightGrey3
        cell.selectedBackgroundView = selecedBackgroundView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let displayArray = displayArray else {
            return
        }
        tableView.deselectRow(at: indexPath, animated: true)
        let profile = displayArray[indexPath.row]
        performSegue(withIdentifier: "Chart", sender: profile)
        
        if (IK_SIMULATE_ROASTER) {
            RoasterManager.instance().profile = profile
            IKSimulatorManager.sharedInstance.startSimulating()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        guard let profile = displayArray?[indexPath.row] else {
            return 0
        }
        if (IK_SIMULATE_ROASTER) {
            if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile.uuid) {
                return 0
            }
        } else {
            if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile.uuid && (IKInterface.instance().isConnected() || IK_SIMULATE_ROASTER)) {
                return 0
            }
        }
        
        return 71
    }
    
    private func openWalkthroughPopup() {
        let notFirstOpen = UserDefaults.standard.bool(forKey: "notFirstTimeTheyArchiveProfile")
        if !notFirstOpen {
            UserDefaults.standard.set(true, forKey: "notFirstTimeTheyArchiveProfile")
            let walkthroughModalView = WalkthroughModalView()
             walkthroughModalView.titleIsHidden = true
             
             walkthroughModalView.setupHorisontalView(image: UIImage(named: "menuArchived") , title: nil, index: .First)
            walkthroughModalView.setupHorisontalView(image: nil , title: "$archived_profile_can_be_found".localized , index: .Second)
             walkthroughModalView.show(animated: true, dismissCallback: nil)
        }
    }
}

extension IKProfileLibVC: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard let profiles = displayArray else {
            return nil
        }
        if orientation == .right {
            if !profiles.isEmpty {
                let profile = profiles[indexPath.row]
                var actionArray = [SwipeAction]()
                
                let archiveAction = SwipeAction(style: .default, title: NSLocalizedString("$archive", comment: "")) {
                    action, indexPath in
                    if AppCore.sharedInstance.profileManager.archiveProfile(profile: profile) {
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LIBRARY_ARCHIVE", label: "")
                    }
                    self.openWalkthroughPopup()
                }
                archiveAction.font = UIFont(name: "AvenirNext-Medium", size: 10)
                archiveAction.image = UIImage(named: "menuArchived")?.resizeImage(17, opaque: true)
                archiveAction.backgroundColor = kColorDarkGrey1
                actionArray.append(archiveAction)
                
                let shareAction = SwipeAction(style: .default, title: NSLocalizedString("$share", comment: "")) {
                    action, indexPath in
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LIBRARY_SHARE", label: "")
                    AppCore.sharedInstance.profileManager.shareProfile(profile: profile)
                }
                shareAction.font = UIFont(name: "AvenirNext-Medium", size: 10)
                shareAction.image = UIImage(named: "shareImage")
                shareAction.hidesWhenSelected = true
                shareAction.backgroundColor = kColorLightGrey1
                shareAction.textColor = kColorDarkGrey1
                actionArray.append(shareAction)
                return actionArray
            } else {
                return nil
            }
        } else {
            if !profiles.isEmpty {
                let profile = profiles[indexPath.row]
                let favoriteAction = SwipeAction(style: .default, title: nil, handler: { action, indexPath in
                    _ = AppCore.sharedInstance.profileManager.favorProfile(isFavorite: !profile.favorite, profile: profile)
                    let image = UIImage(named: profile.favorite ? "FavoriteHighlight" : "Favorite" )
                    self.swipeCellTransitionContext?.button.setImage(image, for: .normal)
                    
                    switch self.currentProfileListType {
                    case .all :
                        (tableView.cellForRow(at: indexPath)?.contentView.viewWithTag(50) as! IKProfileViewCell).favoriteImage.isHidden = !profile.favorite
                    case .favourites :
                        ()
                    default:
                        ()
                    }
                })
                favoriteAction.transitionDelegate = self
                favoriteAction.image = UIImage(named: profile.favorite ? "FavoriteHighlight" : "Favorite" )
                favoriteAction.backgroundColor = kColorLightGrey3
                favoriteAction.hidesWhenSelected = true
                return [favoriteAction]
            } else {
                return nil
            }
        }
        
    }
    
    func visibleRect(for tableView: UITableView) -> CGRect? {
        return tableView.bounds
    }
    
}

extension IKProfileLibVC: SwipeActionTransitioning {
    func didTransition(with context: SwipeActionTransitioningContext) -> Void {
        swipeCellTransitionContext = context
    }
}

extension IKProfileLibVC: ProfileLibHeaderViewDelegate {
    func didTapMenuButton() {
        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let nc = menuStoryboard.instantiateInitialViewController()
        present(nc!, animated: true) {
            
        }
    }
    
    func search(text:String) {
        searchStr = text
        updateTable()
    }
    
    func layoutHeaderViewDidChanged() {
        headerViewHeightConstraint.constant = headerView.maxHeight()
        switchViewHeightConstraint.constant = headerView.searchContainerView.isHidden ? 45 : 0
        if !headerView.searchContainerView.isHidden {
            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LIBRARY_SEARCH", label: "")
        }
        if !headerView.searchContainerView.isHidden {
            currentProfileListType = .search
            tableView.reloadData()
        } else {
            if currentProfileListType == .search {
                currentProfileListType = previousProfileListType
            }
            tableView.reloadData()
        }
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    func tapOnPrfile(profile: Profile) {
        performSegue(withIdentifier: "Chart", sender: profile)
    }
    
    @objc func openRoasterProfile() {
        if let profile = RoasterManager.instance().profile {
            performSegue(withIdentifier: "OpenRoasterProfile", sender: profile)
        }
    }
    
}


extension IKProfileLibVC : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        updateTable()
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        updateTable()
    }
    
    func roaster(_ roaster: Roaster!, didReceiveSetting setting: Int32, withValue value: NSNumber!, success: Bool) {
        
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {

    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        updateTable()
    }
}
