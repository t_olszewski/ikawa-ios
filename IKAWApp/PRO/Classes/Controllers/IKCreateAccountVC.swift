//
//  IKCreateAccountVC.swift
//  IKAWA-Home
//
//  Created by Никита on 10/10/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import FirebaseUI
import AVFoundation
import TOCropViewController

class IKCreateAccountVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField?
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var agreeTermsButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var bestBuisnessTextField: UITextField!
    private var pickerView = UIPickerView()
    @IBOutlet weak var bestBuisnessLabel: UILabel!
    @IBOutlet weak var termsConditions: UIButton!
    let imagePicker = UIImagePickerController()
    
    var agreeTerms = true
    var bestBuisnessArray = ["", "$bestBuisnessText1".localized, "$bestBuisnessText2".localized, "$bestBuisnessText3".localized, "$bestBuisnessText4".localized, "$bestBuisnessText5".localized, "$bestBuisnessText6".localized, "$bestBuisnessText7".localized, "$bestBuisnessText8".localized, "$bestBuisnessText9".localized, "$bestBuisnessText10".localized, "$bestBuisnessText11".localized]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // view.layoutIfNeeded()
        AppCore.sharedInstance.loggingManager.trackScreen(name: "Sign-up Screen")
        #if TARGET_PRO
        firstNameTextField.delegate = self
        //view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        bestBuisnessTextField.rightView = UIImageView(image: UIImage(named: "ArrowDown"))
        bestBuisnessTextField.rightViewMode = .always
        if UIDevice.current.userInterfaceIdiom == .pad {
            let attrs = [NSAttributedString.Key.font.rawValue :UIFont(name: kFontAvenirNextMedium, size: 19)!,
                         NSAttributedString.Key.underlineStyle : 1, NSAttributedString.Key.foregroundColor: UIColor.white
                ] as! [NSAttributedString.Key : Any]
            let attributedTitle = NSAttributedString(string: "$terms_condition".localized, attributes: attrs)
            termsConditions.setAttributedTitle(attributedTitle, for: .normal)
        }
        #else
        titleLabel.attributedText = NSLocalizedString("$create_account_title", comment: "").attributedString(font: UIFont(name: kFontAvenirNextDemiBold, size: 20)!, color: kColorDarkGrey2)
        lastNameTextField?.text = ""
        createAccountButton.titleLabel?.text = NSLocalizedString("$create_account_title_button", comment: "")
        createAccountButton.layer.borderColor = kColorRed.cgColor
        createAccountButton.layer.borderWidth = 2
        #endif
        bestBuisnessTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        createAccountButton.layer.cornerRadius = createAccountButton.frame.height / 2
        firstNameTextField.text = ""
        emailTextField.text = ""
        passwordTextField.text = ""
        updateAgreeTermsButton()
        KeyboardAvoiding.avoidingView = self.scrollView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupPickerView()
    }
    
    #if TARGET_PRO
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        firstNameTextField.createBottomLine(color: kColorLightGrey3)
        emailTextField.createBottomLine(color: kColorLightGrey3)
        passwordTextField.createBottomLine(color: kColorLightGrey3)
        bestBuisnessTextField.createBottomLine(color: kColorLightGrey3)
    }
    #endif
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Actions
    
    @IBAction func agreeTermsButtonTouchUpInside(_ sender: Any) {
        updateAgreeTermsButton()
    }
    

    
    @IBAction func backButtonTouchUpInside(_ sender: Any) {
        view.endEditing(true)
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc private func pickImage(sender: UITapGestureRecognizer) {
        
        let selectDialogView = ButtonsPopupModalView()
        selectDialogView.title = ""
        selectDialogView.layout = .vertical
        
        selectDialogView.setUpButton(index: .First, title: "$camera_up".localized, type: .Bordered) {
            guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                return
            }
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        selectDialogView.setUpButton(index: .Second, title: "$gallery_up".localized, type: .Filled) {
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        selectDialogView.setUpButton(index: .Third, title: "$cancel".localized, type: .Cancel) {
            
        }
        IKNotificationManager.sharedInstance.show(view: selectDialogView)
    }
    
    func setupPickerView() {
        pickerView.dataSource = self
        pickerView.delegate = self
        pickerView.backgroundColor = kColorDarkGrey1
        pickerView.tintColor = kColorDarkGrey1
        pickerView.showsSelectionIndicator = true
        bestBuisnessTextField.inputView = pickerView
        let toolBar = KeyboardInputView.instanceFromNib()
        toolBar.addConfinguration(action: { [weak self] (sender) in
            self?.view.endEditing(true)
        })
        bestBuisnessTextField.inputAccessoryView = toolBar
    }
    
    func createToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = .white
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(hideKeyboard))
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }
    
    func didSelectAvatarImage(img: UIImage) {
        let image = fixOrientation(img: img)
        self.profileImageView.image = image
    }
    
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    @IBAction func registerButtonTouchUpInside(_ sender: Any) {
        self.view.endEditing(true)
        if (firstNameTextField.text?.isEmpty ?? true) || (passwordTextField.text?.isEmpty ?? true) || (emailTextField.text?.isEmpty ?? true) || (bestBuisnessTextField.text?.isEmpty ?? true) {
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.subtitle = NSLocalizedString("$required_fields", comment: "")
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Filled) {}
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            return
        }
        guard validateEmail() && agreeTerms else {
            return
        }
        #if TARGET_PRO
        guard validatePassword() && agreeTerms else {
            return
        }
        var accountRegistrationParameters = [String: Any]()
        accountRegistrationParameters["email"] = emailTextField.text as Any
        accountRegistrationParameters["accountName"] = firstNameTextField.text as Any
        if pickerView.selectedRow(inComponent: 0) > 0 {
            accountRegistrationParameters["userType"] = pickerView.selectedRow(inComponent: 0)
        }
        UserDefaults.standard.set(accountRegistrationParameters, forKey: "accountRegistrationParameters")
        
        
        #else
        guard validatePassword() && agreeTerms else {
            return
        }
        let lastName = lastNameTextField?.text != nil ? lastNameTextField?.text : ""
        UserDefaults.standard.set( ["email" : emailTextField.text,
                                        "firstName" : firstNameTextField.text,
                                        "lastName" : lastName], forKey: "accountRegistrationParameters")
        #endif
        UserDefaults.standard.synchronize()
        self.view.makeToastActivity(.center)
        AppCore.sharedInstance.firebaseManager.firebaseRegisterUser(email: emailTextField.text!, password: passwordTextField.text!, callback: { [weak self] (success, message) in
            DispatchQueue.main.async {
                self?.view.hideToastActivity()
            }
            if (!success) {
                let popup = ButtonsPopupModalView()
                popup.layout = .vertical
                popup.title = NSLocalizedString("$registration_failed", comment: "")
                popup.subtitle = (message ?? "").replacingOccurrences(of: ".", with: "")
                popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                }
                popup.show(animated: true, dismissCallback: {
                    
                })
                //IKNotificationManager.sharedInstance.show(view: popup)
                
            } else {
                UserDefaults.standard.set(self?.emailTextField.text, forKey: "lastUsedEmail")
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_CREATE_ACCOUNT", label: "")
                self?.view.endEditing(true)
                let popup = ButtonsPopupModalView()
                popup.layout = .vertical
                popup.title = NSLocalizedString("$registration_success", comment: "")
                popup.subtitle = "$confirm_email_message".localized
                popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                    NavigationHelper.GoToLoginScreen()
                }
                popup.show(animated: true, dismissCallback: {
                    
                })
                //IKNotificationManager.sharedInstance.show(view: popup)
                
            }
        })

    }
    
    @objc private func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func signInButtonTouchUpInside(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func termsButtonTouchUpInside(_ sender: Any) {
        #if TARGET_PRO
        guard let url = URL(string: kTermsAndConditions) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
//        guard let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "IKPolicyVC") as? IKPolicyVC else {
//            return
//        }
//        vc.callback = {[weak self] in
//            self?.agreeTerms = true
//        }
//        self.navigationController?.pushViewController(vc, animated: true)
//        #else
//        guard let url = URL(string: kTermsAndConditions) else {
//            return //be safe
//        }
//        if #available(iOS 10.0, *) {
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//        } else {
//            UIApplication.shared.openURL(url)
//        }
        #endif
    }
    
    @IBAction func firstNameEditButtonTouchUpInside(_ sender: Any) {
        firstNameTextField.becomeFirstResponder()
    }

    @IBAction func lastNameEditButtonTouchUpInside(_ sender: Any) {
        lastNameTextField?.becomeFirstResponder()
    }
    
    @IBAction func emailEditButtonTouchUpInside(_ sender: Any) {
        emailTextField.becomeFirstResponder()
    }
    
    @IBAction func passwordEditButtonTouchUpInside(_ sender: Any) {
        passwordTextField.becomeFirstResponder()
    }
    
    //MARK: - Private
    
    func updateAgreeTermsButton() {
        if agreeTerms {
            agreeTerms = false
            agreeTermsButton.setImage(UIImage(named: "DeselectedPointIcon"), for: .normal)
            createAccountButton.isEnabled = false
            createAccountButton.alpha = 0.5
        } else {
            agreeTerms = true
            agreeTermsButton.setImage(UIImage(named: "selectedPointIcon"), for: .normal)
            createAccountButton.isEnabled = true
            createAccountButton.alpha = 1
        }
    }
    
    
    
    func validateEmail() -> Bool {
        let valid = self.emailTextField.text?.validateEmail() ?? false
        
        if (valid) {
            self.emailTextField.textColor = UIColor.white
        } else {
            self.emailTextField.textColor = UIColor.red
            
            let popup = ButtonsPopupModalView()
            popup.layout = .vertical
            popup.title = NSLocalizedString("$invalid_email_title", comment: "")
            popup.subtitle = NSLocalizedString("$invalid_email_message", comment: "")
            popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            popup.show(animated: true) {
                
            }
            //IKNotificationManager.sharedInstance.show(view: popup)
        }
        
        return valid
    }
    
    func validatePassword() -> Bool {
        let valid = self.passwordTextField.text?.validatePassword() ?? false
        if (valid) {
            self.passwordTextField.textColor = UIColor.white
        } else {
            self.passwordTextField.textColor = UIColor.red
            
            let popup = ButtonsPopupModalView()
            popup.layout = .vertical
            popup.title = NSLocalizedString("$password_too_short_title", comment: "")
            popup.subtitle = NSLocalizedString("$password_too_short_message", comment: "")
            popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            popup.show(animated: true) {
                
            }
            //IKNotificationManager.sharedInstance.show(view: popup)
        }
        return valid
    }
    
    
    func validateInput() -> Bool {
        if (validateEmail() && validatePassword()) {
            return true
        } else {
            return false
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
}


extension IKCreateAccountVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == firstNameTextField {
            emailTextField.becomeFirstResponder()
        } else if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField  {
            passwordTextField.becomeFirstResponder()
            self.view.endEditing(true)
            
        }
        

        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == bestBuisnessTextField {
            if textField.text?.isEmpty ?? true {
                bestBuisnessLabel.text = "$which_fits_buisness".localized
            } else {
                bestBuisnessLabel.text = "$user_type".localized
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == bestBuisnessTextField {
            bestBuisnessLabel.text = "$user_type".localized
        }
        return true
    }
}

extension IKCreateAccountVC: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bestBuisnessArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //bestBuisnessLabel.text = pickerView.selectedRow(inComponent: 0) > 0 ? "$user_type".localized : "$which_fits_buisness".localized
        bestBuisnessTextField.text = bestBuisnessArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let atributees : [NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) :UIFont(name: "AvenirNext-Medium", size: 14)!, NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.white]
        let string = bestBuisnessArray[row]
        return NSAttributedString(string: string, attributes: atributees)
    }
    
}

extension IKCreateAccountVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate, TOCropViewControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated:true, completion: nil)
        let initialImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        //            let image = self.fixOrientation(img: initialImage)
        let cropViewController = TOCropViewController(croppingStyle: .circular, image: initialImage)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropToCircularImage image: UIImage, with cropRect: CGRect, angle: Int) {
        cropViewController.dismiss(animated: true) {
            self.didSelectAvatarImage(img: image)
        }
    }
}
