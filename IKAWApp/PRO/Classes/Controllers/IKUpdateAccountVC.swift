//
//  IKCreateAccountVC.swift
//  IKAWA-Home
//
//  Created by Никита on 10/10/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import FirebaseUI
import AVFoundation
import TOCropViewController

class IKUpdateAccountVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField?
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var updateAccountButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var agreeTermsButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var bestBuisnessTextField: UITextField!
    private var pickerView = UIPickerView()
    @IBOutlet weak var bestBuisnessLabel: UILabel!
    @IBOutlet weak var termsConditions: UIButton!
    
    var agreeTerms = true
    var bestBuisnessArray = ["", "$bestBuisnessText1".localized, "$bestBuisnessText2".localized, "$bestBuisnessText3".localized, "$bestBuisnessText4".localized, "$bestBuisnessText5".localized, "$bestBuisnessText6".localized, "$bestBuisnessText7".localized, "$bestBuisnessText8".localized, "$bestBuisnessText9".localized, "$bestBuisnessText10".localized, "$bestBuisnessText11".localized]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // view.layoutIfNeeded()
        firstNameTextField.delegate = self
        bestBuisnessTextField.rightView = UIImageView(image: UIImage(named: "ArrowDown"))
        bestBuisnessTextField.rightViewMode = .always
        if UIDevice.current.userInterfaceIdiom == .pad {
            let attrs = [NSAttributedString.Key.font.rawValue :UIFont(name: kFontAvenirNextMedium, size: 19)!,
                         NSAttributedString.Key.underlineStyle : 1,
                ] as! [NSAttributedString.Key : Any]
            let attributedTitle = NSAttributedString(string: "$terms_condition".localized, attributes: attrs)
            termsConditions.setAttributedTitle(attributedTitle, for: .normal)
        }
        bestBuisnessTextField.delegate = self
        emailTextField.delegate = self
        updateAccountButton.layer.cornerRadius = updateAccountButton.frame.height / 2
        firstNameTextField.text = ""
        emailTextField.text = AppCore.sharedInstance.firebaseManager.currentUser?.email ?? ""
        updateAgreeTermsButton()
        
        KeyboardAvoiding.avoidingView = self.scrollView
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        setupPickerView()
        AppCore.sharedInstance.syncManager.getUserInfo { (userInfo) in
            self.firstNameTextField.text = userInfo?.accountName ?? ""
            self.bestBuisnessTextField.text = self.bestBuisnessArray[abs(userInfo?.userType ?? 0)]

        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        firstNameTextField.createBottomLine(color: kColorLightGrey3)
        emailTextField.createBottomLine(color: kColorLightGrey3)
        bestBuisnessTextField.createBottomLine(color: kColorLightGrey3)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Actions
    
    @IBAction func agreeTermsButtonTouchUpInside(_ sender: Any) {
        updateAgreeTermsButton()
    }
    

    
    @IBAction func backButtonTouchUpInside(_ sender: Any) {
        view.endEditing(true)
        AppCore.sharedInstance.logout()
        navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    func setupPickerView() {
        pickerView.dataSource = self
        pickerView.delegate = self
        pickerView.backgroundColor = kColorDarkGrey1
        pickerView.tintColor = kColorDarkGrey1
        pickerView.showsSelectionIndicator = true
        bestBuisnessTextField.inputView = pickerView
        let toolBar = KeyboardInputView.instanceFromNib()
        toolBar.addConfinguration(action: { [weak self] (sender) in
            self?.view.endEditing(true)
        })
        bestBuisnessTextField.inputAccessoryView = toolBar
    }
    
    func createToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = .white
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(hideKeyboard))
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }
    
    func didSelectAvatarImage(img: UIImage) {
        let image = fixOrientation(img: img)
        self.profileImageView.image = image
    }
    
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    @IBAction func updateButtonTouchUpInside(_ sender: Any) {
        self.view.endEditing(true)

        guard agreeTerms else {
            return
        }
        
        
        guard let accountName = firstNameTextField.text, let selectedUserType = bestBuisnessArray.index(of: self.bestBuisnessTextField.text ?? ""), selectedUserType > 0, !accountName.isEmpty else {
                        let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.subtitle = "$required_fields_name_user_type".localized
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Filled) {}
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            return
        }
        
        self.view.makeToastActivity(.center)
        
        AppCore.sharedInstance.loggingManager.updateUserInfo(firstName: "", lastName: "", accountName: accountName, userType: selectedUserType)  { (success, message) in
            if !success {
                self.view.hideToastActivity()
                if let message = message {
                    let popup = ButtonsPopupModalView()
                    popup.layout = .vertical
                    popup.title =  message
                    popup.subtitle = ""
                    popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    popup.show(animated: true, dismissCallback: {
                        
                    })
                }
            } else {
                AppCore.sharedInstance.syncManager.synchronizeRoastLogImages()
                AppCore.sharedInstance.syncManager.restoreLastBackup(needMergingWithLocalBackup: true, callback: { (error) in
                    if let error = error {
                        print("callback error login \(error)")
                    }
                    AppCore.sharedInstance.syncManager.uploadUserLibrary(roundTimestamp: false) { (error) in
                        self.view.hideToastActivity()
                        NavigationHelper.GoToMainScreen()
                        AppCore.sharedInstance.syncManager.startProfileSynchronization()
                    }
                    
                })
            }
        }
    }
    
    @objc private func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func termsButtonTouchUpInside(_ sender: Any) {
        guard let url = URL(string: kTermsAndConditions) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func firstNameEditButtonTouchUpInside(_ sender: Any) {
        firstNameTextField.becomeFirstResponder()
    }

    @IBAction func lastNameEditButtonTouchUpInside(_ sender: Any) {
        lastNameTextField?.becomeFirstResponder()
    }
    
    @IBAction func emailEditButtonTouchUpInside(_ sender: Any) {
        emailTextField.becomeFirstResponder()
    }
    
    //MARK: - Private
    
    func updateAgreeTermsButton() {
        if agreeTerms {
            agreeTerms = false
            agreeTermsButton.setImage(UIImage(named: "DeselectedPointIcon"), for: .normal)
        } else {
            agreeTerms = true
            agreeTermsButton.setImage(UIImage(named: "selectedPointIcon"), for: .normal)
        }
    }
    
    func validateEmail() -> Bool {
        let valid = self.emailTextField.text?.validateEmail() ?? false
        
        if (valid) {
            self.emailTextField.textColor = kColorDarkGrey2
        } else {
            self.emailTextField.textColor = UIColor.red
            
            let popup = ButtonsPopupModalView()
            popup.layout = .vertical
            popup.title = NSLocalizedString("$invalid_email_title", comment: "")
            popup.subtitle = NSLocalizedString("$invalid_email_message", comment: "")
            popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            popup.show(animated: true) {
                
            }
            //IKNotificationManager.sharedInstance.show(view: popup)
        }
        
        return valid
    }
    
    
    func validateInput() -> Bool {
        if validateEmail() {
            return true
        } else {
            return false
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
//        if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.height {
//            UIView.animate(withDuration: 0.1, animations: { () -> Void in
//                self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight == 0 ? 280 : (keyboardHeight + 45), right: 0)
//                self.view.layoutIfNeeded()
//            })
//        }

    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
//        UIView.animate(withDuration: 0.1, animations: { () -> Void in
//            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//            self.view.layoutIfNeeded()
//        })
    }
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
}


extension IKUpdateAccountVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == firstNameTextField {
            emailTextField.becomeFirstResponder()
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == bestBuisnessTextField {
            if textField.text?.isEmpty ?? true {
                bestBuisnessLabel.text = "$which_fits_buisness".localized
            } else {
                bestBuisnessLabel.text = "$user_type".localized
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == bestBuisnessTextField {
            bestBuisnessLabel.text = "$user_type".localized
        }
        return true
    }
}

extension IKUpdateAccountVC: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bestBuisnessArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //bestBuisnessLabel.text = pickerView.selectedRow(inComponent: 0) > 0 ? "$user_type".localized : "$which_fits_buisness".localized
        bestBuisnessTextField.text = bestBuisnessArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let atributees : [NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) :UIFont(name: "AvenirNext-Medium", size: 14)!, NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.white]
        let string = bestBuisnessArray[row]
        return NSAttributedString(string: string, attributes: atributees)
    }
    
}

