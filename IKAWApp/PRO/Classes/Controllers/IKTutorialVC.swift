//
//  IKTutorialVC.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 11/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents

class IKTutorialProVC: UIViewController {
    
    @IBOutlet weak var contatinerView: UIView!
    @IBOutlet weak var pageControllerImageView: UIImageView!
    @IBOutlet weak var skipButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppCore.sharedInstance.loggingManager.trackScreen(name: "PRO_WELCOME")
    }
    
    @IBAction func skipAction(_ sender: Any) {
        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_WELCOME_SKIP", label: "")
        NavigationHelper.GoToMainScreen()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "IKTutorialPageProVC"){
            let embedVC = segue.destination as! IKTutorialPageProVC
            embedVC.swipeDelegate = self
        }
    }
    
}

extension IKTutorialProVC: IKTutorialPageProVCDelegate {
    func swipePageViewController(newIndex: Int) {
        skipButton.isHidden = newIndex == 4 ? true : false
        pageControllerImageView.image = UIImage(named: "tutorial_\(newIndex)_1")
    }
}


class IKTutorialPageProVC: UIPageViewController {
    
    lazy var viewControllerList: [UIViewController] = {
        let sb = UIStoryboard(name: "Tutorial", bundle: nil)
        let vc1 = sb.instantiateViewController(withIdentifier: "TutorialPage1")
        let vc2 = sb.instantiateViewController(withIdentifier: "TutorialPage2")
        let vc3 = sb.instantiateViewController(withIdentifier: "TutorialPage3")
        let vc4 = sb.instantiateViewController(withIdentifier: "TutorialPage4")
        let vc5 = sb.instantiateViewController(withIdentifier: "TutorialPage5")
        return [vc1, vc2, vc3, vc4, vc5]
    }()
    
    weak var swipeDelegate: IKTutorialPageProVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        if let firstViewController = viewControllerList.first {
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
}

extension IKTutorialPageProVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex =  viewControllerList.index(of: viewController) else {return nil}
        let previeousIndex = vcIndex - 1
        guard previeousIndex >= 0 else {return nil}
        guard viewControllerList.count > previeousIndex else {return nil}
        
        return viewControllerList[previeousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex =  viewControllerList.index(of: viewController) else {return nil}
        let nextIndex = vcIndex + 1
        guard viewControllerList.count != nextIndex else {return nil}
        guard viewControllerList.count > nextIndex else {return nil}
        
        return viewControllerList[nextIndex]
    }
    
    func presentationCount(for _: UIPageViewController) -> Int {
        return viewControllerList.count
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (!completed)
        {
            return
        }
        swipeDelegate?.swipePageViewController(newIndex: pageViewController.viewControllers!.first?.view.tag ?? 0)
        
    }
}

class IKBaseTutorialVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let okeyButton = self.view.viewWithTag(250) as? UIButton {
            okeyButton.layer.cornerRadius = okeyButton.frame.height/2
        }
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        NavigationHelper.GoToMainScreen()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}

protocol IKTutorialPageProVCDelegate: class {
    func swipePageViewController(newIndex: Int)
}
