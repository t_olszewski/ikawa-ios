//
//  IKLoginVC_Home.swift
//  IKAWA-Home
//
//  Created by Admin on 12/28/18.
//  Copyright © 2018 Admin. All rights reserved.
//


class IKLoginProVC: IKLoginVC {
    //@IBOutlet weak var loginBackgroundImageView: UIImageView!
    @IBOutlet weak var termsConditions: UIButton!
    @IBOutlet weak var continueWithoutAccountButton: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareApirance()
    }
    //agreeTermsButton
    func prepareApirance() {
        //loginBackgroundImageView.layer.opacity = 0.17
        signInButton.layer.borderColor = kColorWhite.cgColor
        /* emailTextField.defaultTextAttributes = [NSAttributedStringKey.kern.rawValue: 0, NSAttributedStringKey.font.rawValue :UIFont(name: kFontAvenirNextRegular, size: 14)!, NSAttributedStringKey.foregroundColor.rawValue: kColorDarkGrey1]*/
        setupEmailPlaceholder()
        /* passwordTextField.defaultTextAttributes = [NSAttributedStringKey.kern.rawValue: 0, NSAttributedStringKey.font.rawValue :UIFont(name: kFontAvenirNextRegular, size: 14)!, NSAttributedStringKey.foregroundColor.rawValue: kColorDarkGrey1]*/
        passwordTextField.layer.masksToBounds = true
        emailTextField.layer.masksToBounds = true
        passwordTextField.layer.cornerRadius = 8
        emailTextField.layer.cornerRadius = 8
        setupPasswordPlaceholder()
        if UIDevice.current.userInterfaceIdiom == .pad {
            let attrs = [NSAttributedString.Key.font.rawValue: UIFont(name: kFontAvenirNextMedium, size: 19)!,
                NSAttributedString.Key.underlineStyle: 1,
                NSAttributedString.Key.foregroundColor: UIColor.white
            ] as! [NSAttributedString.Key: Any]
            let attributedTitle = NSAttributedString(string: "$terms_condition".localized, attributes: attrs)
            termsConditions.setAttributedTitle(attributedTitle, for: .normal)
        }
    }

    @IBAction func withoutAccountButtonAction(_ sender: Any) {
        self.performSegue(withIdentifier: "ContinueWithoutAccount", sender: nil)
    }
    @IBAction func agreeButtonAction(_ sender: Any) {
        //updateWithoutAccountButton()
    }

    private func updateWithoutAccountButton() {
        let attrs = [NSAttributedString.Key.font.rawValue: UIFont(name: kFontAvenirNextMedium, size: (UIDevice.current.userInterfaceIdiom == .pad ? 16 : 12))!,
            NSAttributedString.Key.underlineStyle: (agreeTerms ? 0 : 1), NSAttributedString.Key.kern.rawValue: 0.86
        ] as! [NSAttributedString.Key: Any]

        let attributedTitle = NSAttributedString(string: "$continue_without_an_account".localized, attributes: attrs)
        continueWithoutAccountButton.setAttributedTitle(attributedTitle, for: .normal)
        //continueWithoutAccountButton.letterSpace = 0.86
    }


    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        passwordTextField.text = ""
    }

    func loginHandler() {
        if (self.validateInput()) {
            self.view.makeToastActivity(.center)
            AppCore.sharedInstance.firebaseManager.firebaseLogin(email: self.emailTextField.text!, password: self.passwordTextField.text!, callback: { (success, message) in
                if (!success) {
                    self.view.endEditing(true)
                    self.view.hideToastActivity()
                    let confirmationDialogView = ButtonsPopupModalView()
                    confirmationDialogView.layout = .vertical
                    if !(message?.isEmpty ?? true) {
                        if message == "$incorrect_sign_in_details".localized {
                        } else {
                            confirmationDialogView.title = "$login_failed".localized
                        }
                        confirmationDialogView.subtitle = message ?? ""
                    } else {
                        confirmationDialogView.title = "$Incorrect_sign_in_popup_message".localized
                        confirmationDialogView.subtitle = ""
                    }


                    confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Filled) { }
                    confirmationDialogView.show(animated: true, dismissCallback: { })

                } else {
                    func login() {
                        UserDefaults.standard.setValue(self.emailTextField.text, forKey: "lastUsedEmail")

                        self.saveNameIfNeeded(callback: { (error) in
                            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                                appDelegate.registerPushNotifications()
                            }
                            AppCore.sharedInstance.syncManager.getUserInfo { (userInfo) in
                                if userInfo?.accountName?.isEmpty ?? true || (userInfo?.userType ?? 0) == 0 {
                                    self.view.hideToastActivity()
                                    self.performSegue(withIdentifier: "IKUpdateAccountVC", sender: nil)
                                } else {
                                    AppCore.sharedInstance.syncManager.synchronizeRoastLogImages()
                                    AppCore.sharedInstance.syncManager.restoreLastBackup(needMergingWithLocalBackup: true, callback: { (error) in
                                        if let error = error {
                                            print("callback error login \(error)")
                                        }
                                        AppCore.sharedInstance.syncManager.uploadUserLibrary(roundTimestamp: false) { (error) in
                                            self.view.hideToastActivity()
                                            NavigationHelper.GoToMainScreen()
                                            AppCore.sharedInstance.syncManager.startProfileSynchronization()
                                        }
                                    })
                                }
                            }
                            if let error = error {
                                self.view.hideToastActivity()
                                let errorDialogView = ButtonsPopupModalView()
                                errorDialogView.layout = .vertical
                                errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                                errorDialogView.subtitle = error.localizedDescription
                                errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) { }
                                errorDialogView.show(animated: true, dismissCallback: { })
                            }


                        })
                    }
                    
                    if UserDefaults.standard.bool(forKey: "db_changed") && UserDefaults.standard.bool(forKey: "NeedMergeWithAccount") {
                        self.view.hideToastActivity()
                        let confirmationDialogView = ButtonsPopupModalView()
                        confirmationDialogView.layout = .horisontal
                        confirmationDialogView.title = "$merge_data_title".localized
                        confirmationDialogView.subtitle = "$merge_data_subtitle".localized
                        confirmationDialogView.setUpButton(index: .First, title: "$cancel_up".localized, type: .Cancel, callback: {
                            AppCore.sharedInstance.firebaseManager.firebaseLogout()
                        })
                        confirmationDialogView.setUpButton(index: .Second, title: "$continue_up".localized, type: .Filled, callback: {
                            self.view.makeToastActivity(.center)
                            login()
                        })
                        confirmationDialogView.show(animated: true, dismissCallback: { })
                    } else {
                        login()
                    }

                }
            })
        }
    }

    //MARK: - Actions
    @IBAction func loginButtonTouchUpInside(_ sender: Any) {
        loginHandler()
    }

}
