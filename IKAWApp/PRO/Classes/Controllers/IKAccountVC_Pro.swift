//
//  IKAccountVC_Pro.swift
//  IKAWA-Pro
//
//  Created by Admin on 4/22/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib
import SDWebImage
import FirebaseUI
import Alamofire

class IKAccountVC_Pro: IKAccountVC {
    @IBOutlet weak var accountNameTextField: UITextField!
    @IBOutlet weak var cropsterButton: UIButton!
    @IBOutlet weak var lastSync: UILabel!
    @IBOutlet weak var accountNameLabel: UILabel!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accountNameTextField.text = ""
        setupCropsterButton()
        
        if let dateLastSync = UserDefaults.standard.object(forKey: "LastBackupDate.ikawa.com") as? Date {
            lastSync.isHidden = false
            lastSync.text = "$menu_last_sync".localized + dateLastSync.dateStr4()
        } else {
            lastSync.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppCore.sharedInstance.syncManager.getUserInfo { (userInfo) in
            self.accountNameTextField.text = userInfo?.accountName ?? ""
            self.accountNameLabel.text = userInfo?.accountName ?? ""
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
        
    private func setupCropsterButton() {
        let cropsterStr = "$cropster".localized + "       "
        let align = NSMutableParagraphStyle();
        align.alignment = NSTextAlignment.center;
        align.firstLineHeadIndent = 10.0;
        align.headIndent = 10.0;
        align.tailIndent = -10.0;
        let para = NSMutableAttributedString();
        let img = NSTextAttachment();
        img.image = UIImage(named: "rightArrow");
        img.bounds = CGRect(x: 0, y: -2, width: img.image!.size.width, height: img.image!.size.height)
        let nas = NSAttributedString(attachment: img).mutableCopy() as! NSMutableAttributedString;
        nas.addAttribute(NSAttributedString.Key.paragraphStyle, value: align, range: NSRange(location: 0, length: nas.length))
        para.append(NSMutableAttributedString(string: cropsterStr))
        para.append(nas)
        cropsterButton.setAttributedTitle(para, for: .normal)
    }
    
    //MARK: - Private
    
    func logout() {
        AppCore.sharedInstance.logout()
        NavigationHelper.GoToLoginScreen()
    }
    
    private func signOut() {
        UserDefaults.standard.set("", forKey: "lastUsedEmail")
        if NetworkReachabilityManager()?.isReachable ?? false {
            if UserDefaults.standard.bool(forKey: "db_changed") {
                UIApplication.shared.keyWindow?.makeToastActivity(.center)
                AppCore.sharedInstance.syncManager.stopSynchronization()
                AppCore.sharedInstance.syncManager.uploadUserLibrary(roundTimestamp: false, callback: { (error) in
                    if let error = error {
                        let errorDialogView = ButtonsPopupModalView()
                        errorDialogView.layout = .vertical
                        errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                        errorDialogView.subtitle = error.localizedDescription
                        errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                        
                        IKNotificationManager.sharedInstance.show(view: errorDialogView)
                    }
                    UIApplication.shared.keyWindow?.hideToastActivity()
                    self.logout()
                    
                })
                
            } else {
                AppCore.sharedInstance.syncManager.stopSynchronization()
                logout()
            }
            
        } else {
            let errorDialogView = ButtonsPopupModalView()
            errorDialogView.layout = .vertical
            errorDialogView.title = NSLocalizedString("$no_internet_connection", comment: "")
            errorDialogView.subtitle = ""
            errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
            }
            IKNotificationManager.sharedInstance.show(view: errorDialogView)
        }
    }
    
    @IBAction func signOutAction(_ sender: Any) {
        let confirmationDialogView = ButtonsPopupModalView()
        confirmationDialogView.layout = .horisontal
        confirmationDialogView.subtitle = "$menu_sign_out_of_your_account".localized
        confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$cancel_up", comment: ""), type: .Cancel ) {}
        confirmationDialogView.setUpButton(index: .Second, title: "$menu_sign_out_popup".localized, type: .Filled, callback: {
            self.signOut()
        })
        IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
        
        
    }
    
    @IBAction func changePasswordAction(_ sender: Any) {
        
        let confirmationDialogView = ButtonsPopupModalView()
        confirmationDialogView.layout = .horisontal
        confirmationDialogView.title = "$confirmation_password_changing".localized
        confirmationDialogView.subtitle = ""
        confirmationDialogView.setUpButton(index: .First, title: "$cancel_up".localized, type: .Cancel ) {}
        confirmationDialogView.setUpButton(index: .Second, title: ("$reset".localized).uppercased(), type: .Filled ) {
            let email = (AppCore.sharedInstance.firebaseManager.currentUser?.email ?? "")
            AppCore.sharedInstance.firebaseManager.firebaseResetPassword(email: email, callback: {[weak self] (success, message) in
                self?.view.hideToastActivity()
                if success {
                    let confirmationDialogView = ButtonsPopupModalView()
                    confirmationDialogView.layout = .horisontal
                    confirmationDialogView.title = "$menu_password_reset_title".localized
                    confirmationDialogView.subtitle = "$menu_password_reset_subtitle".localized
                    confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered ) {}
                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                }
                }, makeToastActivity: {
                    self.view.makeToastActivity(.center)
            })
        }
        IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
    }
    //
    @IBAction func cropsterAction(_ sender: Any) {
        /*dismiss(animated: true, completion: {})
        NavigationHelper.GoToCropsterAccountScreen()*/
        performSegue(withIdentifier: "IKCropsterAccountVC", sender: nil)
    }
    @IBAction func editLoginButtonAction(_ sender: Any) {
        accountNameTextField.becomeFirstResponder()
    }
    @IBAction func editEmailButtonAction(_ sender: Any) {
        emailTextField.becomeFirstResponder()
    }
    
    override func saveNameIfNeeded(callback:@escaping ((_ error: Error?)->Void)) {
        guard let user = AppCore.sharedInstance.firebaseManager.currentUser else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "user not initialized"])
            callback(error)
            return
        }
        
        guard let name = accountNameTextField.text, !name.isEmpty else {
            self.view.hideToastActivity()
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.subtitle = "$account_required_fields".localized
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Filled) {}
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            return
        }
        
        var accountName = ""
        
        AppCore.sharedInstance.syncManager.getUserInfo { (userInfo) in
            accountName = userInfo?.accountName ?? ""
            if accountName != self.accountNameTextField.text {
                AppCore.sharedInstance.loggingManager.updateUserInfo(firstName: "", lastName: "", accountName: self.accountNameTextField?.text ?? "", userType:nil, callback: {(success, message) in
                    self.view.hideToast()
                    if !success {
                        if let message = message {
                            let popup = ButtonsPopupModalView()
                            popup.layout = .vertical
                            popup.title =  message
                            popup.subtitle = ""
                            popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                            
                            IKNotificationManager.sharedInstance.show(view: popup)
                            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "user not initialized"])
                            callback(error)
                        }
                        return
                        
                    } else {
                        self.accountNameLabel.text = self.accountNameTextField?.text ?? accountName
                        self.nameWasChanged = true
                        callback(nil)
                    }
                })
            } else {
                callback(nil)
            }
        }
        
    }
    
   override func didSelectAvatarImage(img: UIImage) {
    guard let userId = AppCore.sharedInstance.firebaseManager.currentUser?.uid else {
        return
    }
    let image = fixOrientation(img: img)
        if !(NetworkReachabilityManager()?.isReachable ?? false) {
            
            if let error = AppCore.sharedInstance.syncManager.saveLocalImage(image: image, folder: "avatars", fileName: userId) {
                DispatchQueue.main.async {
                    let errorDialogView = ButtonsPopupModalView()
                    errorDialogView.layout = .vertical
                    errorDialogView.title = "Error".localized
                    errorDialogView.subtitle = error.localizedDescription
                    errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    
                    IKNotificationManager.sharedInstance.show(view: errorDialogView)
                }
            } else {
                DispatchQueue.main.async {
                    self.avatarImageView.image = img
                }
            }
        } else {
            self.view.makeToastActivity(.center)
            AppCore.sharedInstance.firebaseManager.uploadAvatarImage(image: image) { (success, message) in
                self.view.hideToastActivity()
                self.loadAvatar()
            }
        }
    }
}
