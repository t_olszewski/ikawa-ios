//
//  ArchivedProfileVC.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 5/14/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib
import SwipeCellKit

class ArchivedProfileVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: RoastLogListHeaderView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    var displayArray: [Profile]?
    var searchStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppCore.sharedInstance.loggingManager.trackScreen(name: "PRO_ARCHIVE_LAUNCH")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        headerView.delegate = self
        self.tableView.contentInset = UIEdgeInsets(top: self.tableView.contentInset.top, left: 0, bottom: 0, right: 0)
        prepareAppearance()
        
        tableView.delegate = self
        tableView.dataSource = self
        headerView.searchBar.placeholder = "$search_profiles".localized
        headerView.titleLabel.text = NSLocalizedString("$archived_profiles_uppercased", comment: "")
        // statusBarView.populate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        displayArray = getDisplayArray()
        updateTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.tableView.contentInset = UIEdgeInsets(top: self.tableView.contentInset.top, left: 0, bottom: 0, right: 0)
            } else {
                self.tableView.contentInset = UIEdgeInsets(top: self.tableView.contentInset.top, left: 0, bottom: endFrame?.size.height ?? 0.0, right: 0)
            }
            
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
        
    }
    
    func prepareAppearance() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 70.0
        updateTable()
    }
    
    func getDisplayArray() -> [Profile] {
        return (AppCore.sharedInstance.profileManager.profilesInLibrary ?? [Profile]()).filter { (profile) -> Bool in
            return profile.archived
        }
    }
    
    func updateTable() {
        if searchStr.count > 0 {
            displayArray = getDisplayArray().filter { (profile) -> Bool in
                profile.name.lowercased().contains(searchStr.lowercased())
            }
        } else {
            displayArray = getDisplayArray()
        }
        tableView.reloadData()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "Chart") {
            let chartVC = segue.destination as! IKChartVC
            chartVC.profile = sender as? Profile
        }
    }
}

extension ArchivedProfileVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var displayArray = displayArray else {
            return UITableViewCell()
        }
        
        let profile = displayArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SwipeTableViewCell
        cell.delegate = self
        let profileViewCell = cell.contentView.viewWithTag(50) as! IKProfileViewCell
        profileViewCell.populate(profile: profile)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let displayArray = displayArray else {
            return
        }
        tableView.deselectRow(at: indexPath, animated: true)
        let profile = displayArray[indexPath.row]
        performSegue(withIdentifier: "Chart", sender: profile)
    }
    
}

extension ArchivedProfileVC: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        if orientation == .left {
            let archiveAction = SwipeAction(style: .default, title: NSLocalizedString("$restore", comment: "")) {
                action, indexPath in
                if (self.displayArray?[indexPath.row]) != nil {
                    if let profile = self.displayArray?[indexPath.row] {
                        if AppCore.sharedInstance.profileManager.restoreProfile(profile: profile) {
                            AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ARCHIVE_RESTORE", label: "")
                            self.displayArray?.remove(at: indexPath.row)
                            tableView.beginUpdates()
                            tableView.deleteRows(at: [indexPath], with: .fade)
                            tableView.endUpdates()
                        }
                    }
                }
                
            }
            archiveAction.backgroundColor = kColorDarkGrey2
            archiveAction.textColor = kColorLightGrey1
            archiveAction.hidesWhenSelected = true
            return [archiveAction]
        }
        return nil
    }
    
    func visibleRect(for tableView: UITableView) -> CGRect? {
        return tableView.bounds
    }
    
}

extension ArchivedProfileVC: RoastLogListHeaderViewDelegate {
    func didTapMenuButton() {
        
        //self.navigationController?.popViewController(animated: true)
        
        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let nc = menuStoryboard.instantiateInitialViewController()
        present(nc!, animated: true) {
    
        }
    }
    
    func search(text:String) {
        searchStr = text
        updateTable()
    }
    
    func layoutHeaderViewDidChanged() {
        headerViewHeightConstraint.constant = headerView.maxHeight()
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
}

extension ArchivedProfileVC: SwipableTableViewCellDelegate {
    func swipableTableViewCellDidSwipeLeftWithCell(cell: UITableViewCell) {
        let indexPath = tableView.indexPath(for: cell)! //archive
        if (displayArray?[indexPath.row]) != nil {
            displayArray?.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
    }
    
    func swipableTableViewCellDidSwipeRightWithCell(cell: UITableViewCell) {
        let indexPath = tableView.indexPath(for: cell)! //delete
        if (displayArray?[indexPath.row]) != nil {
            displayArray?.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
    }
    
}
