//
//  CropsterManager.swift
//  IKAWA-Pro
//
//  Created by Admin on 2/22/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Alamofire
import IKRoasterLib

class CropsterManager: NSObject {
    public var isInitialized = false {
        willSet {
            if isInitialized != newValue && newValue {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_ACCOUNT_CROPSTER", label: "")
            }
        }
    }
    public var group: String? {
        didSet {
            UserDefaults.standard.set(group, forKey: cropsterGroup)
        }
    }
    
    public var key: String? {
        didSet {
            UserDefaults.standard.set(key, forKey: cropsterKey)
        }
    }
    public var secret: String? {
        didSet {
            UserDefaults.standard.set(secret, forKey: cropsterSecret)
        }
    }

    public var machineId: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: cropsterMachineId)
        }
        get {
            return UserDefaults.standard.string(forKey: cropsterMachineId)
        }
    }

    public var email: String? {
        didSet {
            UserDefaults.standard.set(email, forKey: cropsterEmail)
        }
    }
    

    private let cropsterSecret = "cropster_secret.icawa.com"
    private let cropsterKey = "cropster_key.icawa.com"
    private let cropsterGroup = "cropster_group.icawa.com"
    private let cropsterMachineId = "cropster_macine_id.icawa.com"
    private let cropsterEmail = "cropster_email.icawa.com"
    
    
    override init() {
        group = UserDefaults.standard.string(forKey: cropsterGroup)
        key = UserDefaults.standard.string(forKey: cropsterKey)
        secret = UserDefaults.standard.string(forKey: cropsterSecret)
        super.init()
        if let group = group, !group.isEmpty, let key = key, !key.isEmpty, let secret = secret, !secret.isEmpty {
            isInitialized = true
            //self.sendProfiles()
        }
    }
    //MARK: - Public
    public func sendProfiles() {
        guard isInitialized, let profiles = AppCore.sharedInstance.profileManager.profiles, profiles.count > 0 else {
            return
        }
        
        let asyncOperation = AsyncOperation(numberOfSimultaneousActions: 1, dispatchQueueLabel: "AnyString")
        asyncOperation.whenCompleteAll = {
            print("All Done")
        }

        if let profiles = AppCore.sharedInstance.profileManager.profiles {
            for profile in profiles {
                asyncOperation.run{ completeClosure in
                    self.sendProfile(profile: profile, callback: {(error) in
                        if let error = error {
                            if (error as NSError).code == 401 {
                                self.logOut()
                            }
                        }
//                        if let error = error {
//                            let alertView = ButtonsPopupModalView.fromNib()
//                            alertView.layout = .vertical
//                            alertView.title = error.localizedDescription
//                            alertView.firstButtonTitle = NSLocalizedString("$ok", comment: "")
//                            alertView.firstButtonCallback = {
//                            }
//                            IKNotificationManager.sharedInstance.show(view: alertView)
//                            completeClosure()
//                        }
                        completeClosure()
                    })
                }

            }
        }
    }
    
    func sendProfile(profile: Profile, callback:@escaping ((_ error: Error?)->())) {
        guard isInitialized else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "not initialized"])
            callback(error)
            return
        }
        
        guard let profiles = AppCore.sharedInstance.profileManager.profiles, profiles.count > 0 else {
            callback(nil)
            return
        }
        
        // Fist check if profile exists
        getCropsterProfile(profile: profile) { (cropsterId, error) in
            guard error == nil else {
                callback(error)
                return
            }
            
            if let cropsterId = cropsterId, !cropsterId.isEmpty  {
                profile.cropsterId = cropsterId
                _ = AppCore.sharedInstance.sqliteManager.updateProfile(profile: profile)
                callback(nil)
                return
            } else {
                //First check if the machine exists in cropster
               // self.getMachineId(roasterId: UserDefaults.standard.string(forKey: "roasterIdSettingsId"), callback: { (machineId, error) in
//                    guard error == nil else {
//                        callback(error)
//                        return
//                    }
                    
                if let machineId = self.machineId, !machineId.isEmpty  {
                        // Post to server
                        Alamofire.request("https://c-sar.cropster.com/api/v2/profiles", method: .post, parameters: nil, encoding: profile.serializeToCropsterJson(machineId: machineId), headers: self.getHeaders()).validate().responseJSON { (response) in
                            guard response.result.isSuccess else {
                                if let error = response.result.error {
                                    print("Error while post profile to the cropster: \(error)")
                                }
                                callback(error)
                                return
                            }
                            
                            guard let value = response.result.value as? [String: AnyObject],
                                let data = value["data"] as? [String: AnyObject] else {
                                    let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "Error while post profile to the cropster"])
                                    callback(error)
                                    return
                            }
                            
                            if data.isEmpty {
                                let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "Error while post profile to the cropster"])
                                callback(error)
                                return
                            } else { //success
                                if let cropsterId = data["id"] as? String {
                                    profile.cropsterId = cropsterId
                                    _ = AppCore.sharedInstance.sqliteManager.updateProfile(profile: profile)
                                    callback(nil)
                                    return
                                }
                            }
                            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "Error while post profile to the cropster"])
                            callback(error)
                            return
                        }


                    } else {
                        let error = NSError(domain:"com.ikawa", code:204, userInfo:[NSLocalizedDescriptionKey: "Machine doesn't exist on the cropster server"])
                        callback(error)
                        return
                    }
                    
                //})
            }
            
        }
        
    }
    
    
    func getMachines(callback: @escaping ((_ machines: [IKCropsterMachine]?, _ error: Error?)->())) {
            guard isInitialized, let group = group else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "not initialized"])
            callback(nil, error)
            return
        }
        
        Alamofire.request("https://c-sar.cropster.com/api/v2/machines?filter[machines][group]=" + group + "&filter[machines][machineType]=IKAWA", method: .get, parameters: nil, encoding: URLEncoding.default, headers: getHeaders()).validate().responseJSON { (response) in
            guard response.result.isSuccess else {
                if let error = response.result.error {
                    print("Error while get machine ID: \(error)")
                }
                callback(nil, nil)
                return
            }
            
            guard let value = response.result.value as? [String: AnyObject],
                let data = value["data"] as? [[String: AnyObject]] else {
                    callback(nil, nil)
                    return
            }
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let parsedResult: [IKCropsterMachine]? = try JSONDecoder().decode([IKCropsterMachine].self, from: jsonData)
                callback(parsedResult, nil)
            } catch {
                callback(nil, error)
            }
        }
        
    }
    
    func getMachineId(roasterId: String?, callback: @escaping ((_ machineId: String?,_ error: Error?)->())) {
        guard isInitialized, let group = group, let machineId = roasterId else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "not initialized"])
            callback(nil, error)
            return
        }
        
        Alamofire.request("https://c-sar.cropster.com/api/v2/machines?filter[machines][group]=" + group + "&filter[machines][erpId]=" + machineId, method: .get, parameters: nil, encoding: URLEncoding.default, headers: getHeaders()).validate().responseJSON { (response) in
            guard response.result.isSuccess else {
                if let error = response.result.error {
                    print("Error while get machine ID: \(error)")
                }
                callback(nil, nil)
                return
            }
            
            guard let value = response.result.value as? [String: AnyObject],
                let data = value["data"] as? [[String: AnyObject]] else {
                    callback(nil, nil)
                    return
            }
            
            if data.isEmpty {
                callback(nil, nil)
                return
            } else {
                if let machineId = data[0]["id"] as? String {
                    callback(machineId, nil)
                    return
                }
            }
            callback(nil, nil)
            return
        }
    }
    
    func getCropsterProfile(profile: Profile, callback: @escaping ((_ cropsterId: String?,_ error: Error?)->())) {
        guard isInitialized, let group = group else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "not initialized"])
            callback(nil, error)
            return
        }
        
        Alamofire.request("https://c-sar.cropster.com/api/v2/profiles?filter[profiles][erpId]=" + profile.uuid + "&filter[profiles][group]=" + group, method: .get, parameters: nil, encoding: URLEncoding.default, headers: getHeaders()).validate().responseJSON { (response) in
            guard response.result.isSuccess else {
                if let error = response.result.error {
                    print("Error while check if profile exists: \(error)")
                }
                callback(nil, nil)
                return
            }
            
            guard let value = response.result.value as? [String: AnyObject],
                let data = value["data"] as? [[String: AnyObject]] else {
                    callback(nil, nil)
                    return
            }
            
            if data.isEmpty {
                callback(nil, nil)
                return
            } else {
                if let cropsterId = data[0]["id"] as? String {
                    callback(cropsterId, nil)
                    return
                }
            }
            callback(nil, nil)
            return
            
        }
    }
    
    public func logOut() {
        isInitialized = false
        key = nil
        secret = nil
        group = nil
        
    }
    
    func getUserEmail(callback: @escaping ((_ email: String?, _ error: Error?)->())) {
//                guard isInitialized else {
//                let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "not initialized"])
//                callback(nil, error)
//                return
//            }
            
            Alamofire.request("https://c-sar.cropster.com/api/v2/loginInfo/current", method: .get, parameters: nil, encoding: URLEncoding.default, headers: getHeaders()).validate().responseJSON { (response) in
                guard response.result.isSuccess else {
                    if let error = response.result.error {
                        print("Error while get machine ID: \(error)")
                    }
                    callback(nil, nil)
                    return
                }
                
                guard let value = response.result.value as? [String: AnyObject],
                    let data = value["data"] as? [String: AnyObject],
                    let attributes = data["attributes"] as? [String: AnyObject],
                    let userId = attributes["userId"] as? String else {
                        callback(nil, nil)
                        return
                }
                
                Alamofire.request("https://c-sar.cropster.com/api/v2/users/\(userId)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: self.getHeaders()).validate().responseJSON { (response) in
                    guard response.result.isSuccess else {
                        if let error = response.result.error {
                            print("Error while get machine ID: \(error)")
                        }
                        callback(nil, nil)
                        return
                    }
                    
                    guard let value = response.result.value as? [String: AnyObject],
                        let data = value["data"] as? [String: AnyObject],
                        let attributes = data["attributes"] as? [String: AnyObject],
                        let email = attributes["email"] as? String else {
                            callback(nil, nil)
                            return
                    }
                    callback(email, nil)
                    
                }
                
            }
    }
    
    public func setCredentials(key: String?, secret: String?) {
        self.key = key
        self.secret = secret
        getUserEmail { (email, error) in
            self.email = email
            
             UIApplication.shared.keyWindow?.makeToastActivity(.center)
            self.getGroups { (groups) in
                 UIApplication.shared.keyWindow?.hideToastActivity()
                if let groups = groups {
                    if groups.count > 1 {
                        let alertSheet = UIAlertController(title: NSLocalizedString("$Choose_your_group", comment: ""), message: nil, preferredStyle: UIAlertController.Style.actionSheet)
                        for groupId in groups {
                            alertSheet.addAction(UIAlertAction(title: groupId, style: .default, handler: { action in
                                self.selectGroup(newGroup: groupId)
                            }))
                        }
                        alertSheet.addAction(UIAlertAction(title: NSLocalizedString("$cancel", comment: ""), style: .cancel, handler: { action in
                            self.logOut()
                        }))
                        if let vc = (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController, let presentedViewController = vc.presentedViewController {
                            if let popoverPresentationController = alertSheet.popoverPresentationController {
                                popoverPresentationController.sourceView = vc.presentedViewController?.view
                                popoverPresentationController.sourceRect = CGRect(x: presentedViewController.view.bounds.width / 2.0, y: presentedViewController.view.bounds.height / 2.0, width: 1, height: 1)
                                popoverPresentationController.permittedArrowDirections = []
                            }
                            presentedViewController.present(alertSheet, animated: true, completion: {})
                        }
                    } else if groups.count == 1 {
                        self.selectGroup(newGroup: groups.first!)
                    } else {
                        self.logOut()
                        let errorDialogView = ButtonsPopupModalView()
                        errorDialogView.layout = .vertical
                        errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                        errorDialogView.subtitle = NSLocalizedString("$cropster_error_message", comment: "")
                        errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                        
                        IKNotificationManager.sharedInstance.show(view: errorDialogView)
                    }
                }
            }
        }
    }
    
    func selectGroup(newGroup: String) {
        if group != newGroup { /* in case group change, remove all the cropster profile ids */
            if let profiles = AppCore.sharedInstance.profileManager.profiles {
                for profile in profiles {
                    profile.cropsterId = ""
                    _ = AppCore.sharedInstance.sqliteManager.updateProfile(profile: profile)
                }
            }

        }
        group = newGroup
        isInitialized = true
        self.sendCredentials()
        NotificationCenter.default.post(name: NSNotification.Name.DidLoginToCropster, object: nil)
    }
    
    func getMachinesPopupShow(_ callback: ((_ complete: Bool?)->())? = nil) {
        UIApplication.shared.keyWindow?.makeToastActivity(.center)
        getMachines { (machines, error) in
            UIApplication.shared.keyWindow?.hideToastActivity()
            func showErrorDialogView() {
                let errorDialogView = ButtonsPopupModalView()
                errorDialogView.layout = .vertical
                errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                errorDialogView.subtitle = NSLocalizedString("$cropster_error_message", comment: "")
                errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                
                IKNotificationManager.sharedInstance.show(view: errorDialogView)
            }
            if let machines = machines {
                if machines.count > 0 {
                    
                    let alertSheet = UIAlertController(title: NSLocalizedString("$Choose_machine", comment: ""), message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom != .pad ?  UIAlertController.Style.actionSheet : UIAlertController.Style.alert)
                    for machine in machines {
                        alertSheet.addAction(UIAlertAction(title: machine.name, style: .default, handler: { action in
                            self.machineId = machine.id
                            self.sendCredentials()
                            self.sendProfiles()
                            
                            if let callback = callback {
                                callback(true)
                            }
                            
                        }))
                    }
                    alertSheet.addAction(UIAlertAction(title: NSLocalizedString("$cancel", comment: ""), style: .cancel, handler: { action in
                       /* if callback == nil {
                            self.logOut()
                        }*/
                    }))
                    if let vc = (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController/*.presentedViewController */{
                        let presentedViewController = vc
                        if let popoverPresentationController = alertSheet.popoverPresentationController {
                            popoverPresentationController.sourceView = vc.presentedViewController?.view
                            popoverPresentationController.sourceRect = CGRect(x: presentedViewController.view.bounds.width / 2.0, y: presentedViewController.view.bounds.height / 2.0, width: 1, height: 1)
                            popoverPresentationController.permittedArrowDirections = []
                        }
                        presentedViewController.present(alertSheet, animated: true, completion: {})
                    }
                } else {
                    showErrorDialogView()
                }
            } else {
                showErrorDialogView()
            }
        }
    }
    
    func getLots(callback: @escaping ( _ groups: [IKCoffeeBean]?,_ error: Error?) -> Void) {
        guard isInitialized, let group = group else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "not initialized"])
            callback(nil, error)
            return
        }
        Alamofire.request("https://c-sar.cropster.com/api/v2/lots?filter[lots][group]=" + group + "&filter[lots][isSample]=true&filter[lots][processingStep]=coffee.green&page[number]=0&page[size]=1000", method: .get, parameters: nil, encoding: URLEncoding.default, headers: getHeaders()).validate().responseJSON { (response) in
            guard response.result.isSuccess else {
                if let error = response.result.error {
                    print("Error while fetching lots: \(error)")
                }
                callback(nil, response.result.error)
                return
            }
            
            guard let value = response.result.value as? [String: AnyObject],
                let data = value["data"] as? [[String: AnyObject]] else {
                    let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "Error while fetching lots"])
                    callback(nil, error)
                    return
            }
            
            var lots: [IKCoffeeBean] = []
            for lot in data {
                let coffeeBean = IKCoffeeBean()
                coffeeBean.coffeeType =  lot["attributes"]?["name"] as? String
                coffeeBean.coffeeId = lot["id"] as? String
                if let actualWeight = (lot["attributes"]?["actualWeight"] as? [String: AnyObject]),
                    let weight = actualWeight["amount"] as? Double,
                    let unit = actualWeight["unit"] as? String{
                    coffeeBean.weight = String(format: "%.2f %@", weight, unit)
                }
                
                lots.append(coffeeBean)
            }
            callback(lots, nil)
        }
        
    }
    
    
    
    func logRoast(roastNotes: IKRoastNotes, callback:@escaping ((_ error: Error?)->())) {
        guard isInitialized else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "not initialized"])
            callback(error)
            return
        }
        
        guard let profiles = AppCore.sharedInstance.profileManager.profiles, profiles.count > 0, let roast = roastNotes.roast else {
            callback(nil)
            return
        }
        
        //First check if the machine exists in cropster
 //       self.getMachineId(roasterId: UserDefaults.standard.string(forKey: "roasterIdSettingsId"), callback: { (machineId, error) in
//            guard error == nil else {
//                callback(error)
//                return
//            }
            
            if let machineId = self.machineId, !machineId.isEmpty  {
                // Post to server
                Alamofire.request("https://c-sar.cropster.com/api/v2/roastingActions", method: .post, parameters: nil, encoding: roastNotes.serializeToCropsterJson(machineId: machineId), headers: self.getHeaders()).validate().responseJSON { (response) in
                    guard response.result.isSuccess else {
                        if let error = response.result.error {
                            print("Error while post roast log to the cropster: \(error)")
                            callback(error)
                        } else {
                            callback(nil)
                        }
                        return
                    }
                    
                    guard let value = response.result.value as? [String: AnyObject],
                        let data = value["data"] as? [String: AnyObject] else {
                            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "Error while post roast log to the cropster"])
                            callback(error)
                            return
                    }
                    
                    if data.isEmpty {
                        let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "Error while post roast log to the cropster"])
                        callback(error)
                        return
                    } else { //success
                        
                        if let relationships = data["relationships"] as? [String: AnyObject], let processing = relationships["processing"] as? [String: AnyObject], let dataProcessing = processing["data"] as? [String: AnyObject], let processingId = dataProcessing["id"] as? String/*,
                            let lastRoast = data["id"] as? String*/{
                            // Post to server
                            self.postRequest(path: "processingCurves", body: roast.serializeAboveTemperatureToCropsterJson(processingId: processingId), callback: { (value, error) in
                                guard error == nil else {
                                    callback(error)
                                    return
                                }
                                
                                self.postRequest(path: "processingCurves", body: roast.serializeAboveCoolDownTemperatureToCropsterJson(processingId: processingId), callback: { (value, error) in
                                    guard error == nil else {
                                        callback(error)
                                        return
                                    }
                                    
                                    self.postRequest(path: "processingCurves", body: roast.serializeBelowTemperatureToCropsterJson(processingId: processingId), callback: { (value, error) in
                                        guard error == nil else {
                                            callback(error)
                                            return
                                        }
                                        
                                        self.postRequest(path: "processingCurves", body: roast.serializeBelowCoolDownTemperatureToCropsterJson(processingId: processingId), callback: { (value, error) in
                                            guard error == nil else {
                                                callback(error)
                                                return
                                                
                                            }
                                            
                                            
                                            self.postRequest(path: "processingMeasures", body: roastNotes.serializeToCropsterEndTempJson(processingId: processingId), callback: { (value, error) in
                                                guard error == nil else {
                                                    callback(error)
                                                    return
                                                    
                                                }
                                                
                                                self.postRequest(path: "processingComments", body: roastNotes.serializeToCropsterTurningPointJson(processingId: processingId), callback: { (value, error) in
                                                    guard error == nil else {
                                                        callback(error)
                                                        return
                                                    }
                                                    
                                                    if roastNotes.dtr > 0 {
                                                        self.postRequest(path: "processingMeasures", body: roastNotes.serializeToCropsterDtrJson(processingId: processingId), callback: { (value, error) in
                                                        })
                                                    }
                                                    if roastNotes.firstCrackTime > 0 {
                                                        self.postRequest(path: "processingMeasures", body: roastNotes.serializeToCropsterDevelopmentTimeJson(processingId: processingId), callback: { (value, error) in
                                                        })
                                                    }
                                                    if roastNotes.firstCrackTime > 0 {
                                                        self.postRequest(path: "processingComments", body: roastNotes.serializeToCropsterFirstCrackJson(processingId: processingId), callback: { (value, error) in
                                                            
                                                        })
                                                    }
                                                    if roastNotes.secondCrackTime > 0 {
                                                        self.postRequest(path: "processingComments", body: roastNotes.serializeToCropsterSecondCrackJson(processingId: processingId), callback: { (value, error) in
                                                            
                                                        })
                                                    }
                                                    if roastNotes.colorChangeTime > 0 {
                                                        self.postRequest(path: "processingComments", body: roastNotes.serializeToCropsterColorChangeJson(processingId: processingId), callback: { (value, error) in
                                                        })
                                                    }
                                                    
                                                    callback(nil)
                                                    return
                                                })
                                            })
                                            
                                            
                                        })
                                        
                                    })
                                    
                                })
                                
                                
                            })
                        } else {
                            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "Error while post roast log to the cropster"])
                            callback(error)
                            return
                        }
                    }
                }
            } else {
                let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: NSLocalizedString("$add_machine_message", comment: "")])
                callback(error)
                return
            }
            
    //    })
        
        
        
    }
    
    func exportToCropster(roastLog: IKRoastNotes, callback: @escaping ((_ error: Error?)->())) {
        
        //First check if criopsterProfileId exists in cropster
        guard let profile = roastLog.profile() else {
            let error = NSError(domain:"com.ikawa", code:404, userInfo:[NSLocalizedDescriptionKey: "profile not found"])
            callback(error)
            return
        }
        getCropsterProfile(profile: profile) { (cropsterId, error) in
            guard error == nil else {
                callback(error)
                return
            }
            
            if let cropsterId = cropsterId, !cropsterId.isEmpty  {
                profile.cropsterId = cropsterId
                _ = AppCore.sharedInstance.sqliteManager.updateProfile(profile: profile)
                let storyboard = UIStoryboard(name: "Cropster", bundle: nil)
                guard let beanSelectVC = storyboard.instantiateViewController(withIdentifier: "IKBeanSelectVC") as? IKBeanSelectVC else {
                    callback(nil)
                    return
                }
                
                beanSelectVC.callback = {(coffeeBean) in
                    roastLog.coffeeId = coffeeBean.coffeeId ?? ""
                    roastLog.coffeeType = coffeeBean.coffeeType ?? ""
                    roastLog.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
                    _ = AppCore.sharedInstance.roastNotesManager.saveRoastNotes(roastNotes: roastLog,needReloadData: true)
                    UIApplication.shared.keyWindow?.makeToastActivity(.center)
                    AppCore.sharedInstance.cropsterManager.logRoast(roastNotes: roastLog, callback: { (error) in
                        UIApplication.shared.keyWindow?.hideToastActivity()
                        if error != nil {
                            let alertView = ButtonsPopupModalView()
                            alertView.layout = .vertical
                            alertView.title = NSLocalizedString("$cropster_error_title", comment: "")
                            alertView.subtitle = NSLocalizedString("$cropster_error_message", comment: "")
                            alertView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                            IKNotificationManager.sharedInstance.show(view: alertView)
                        } else {
                            let alertView = ButtonsPopupModalView()
                            alertView.layout = .vertical
                            alertView.title = NSLocalizedString("$export_to_cropster", comment: "")
                            alertView.subtitle = NSLocalizedString("$cropster_export_success", comment: "")
                            
                            alertView.setUpButton(index: .First, title: NSLocalizedString("$dismiss", comment: ""), type: .Cancel) {}
                            alertView.setUpButton(index: .Second, title: NSLocalizedString("$cropster_view_roast", comment: ""), type: .Bordered) {
                                if let url = URL(string: "https://c-sar.cropster.com/apps/roast/overview") {
                                    UIApplication.shared.openURL(url)
                                }
                            }
                            IKNotificationManager.sharedInstance.show(view: alertView)
                        }
                        callback(error)
                    })
                }
                callback(nil)
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                ((appDelegate?.window?.rootViewController as? IKTabBarController)?.selectedViewController as? UINavigationController)?.pushViewController(beanSelectVC, animated: true)
                /*
                 let appDelegate  = UIApplication.shared.delegate as! AppDelegate
                 appDelegate.window!.rootViewController?.present(beanSelectVC, animated: true, completion: {
                 
                 })*/
            } else {
                let error = NSError(domain:"com.ikawa", code:404, userInfo:[NSLocalizedDescriptionKey: "cropsterId not found"])
                callback(error)
                return
            }
        }

        
    }
    
    //MARK: - Private
    
    func sendCredentials() {
        guard let user = AppCore.sharedInstance.firebaseManager.currentUser else {
            return
        }
        if (user.uid == "anonymous") {
            return
        }
        
        let dict = NSMutableDictionary()
        var result = ""
        let cropsterDict = NSMutableDictionary()
        cropsterDict["group"] = self.group
        cropsterDict["secret"] = self.secret
        cropsterDict["key"] = self.key
        cropsterDict["machine_id"] = self.machineId
        cropsterDict["email"] = self.email
        dict["cropster"] = cropsterDict
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            result = theJSONText!
        }
        
        AppCore.sharedInstance.firebaseManager.firebaseUpdateData(type: "user_details", key: user.uid, json: result) { (success, message) in

        }
    }
    
    private func getHeaders() -> [String: String] {
        guard let key = self.key, let secret = self.secret else {
            return ["":""]
        }
        let credentialData = "\(String(describing: key)):\(String(describing: secret))".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)",
            "Content-Type": "application/vnd.api+json;charset=UTF-8",
            "Accept": "application/vnd.api+json;charset=UTF-8"]
        return headers
    }
    
    func getGroups(callback: @escaping ( _ groups: [String]?) -> Void) {
        getRequest(path: "groups") { (value, error) in
            guard error == nil else {
                callback(nil)
                return
            }
            
            guard let value = value, let data = value["data"] as? [[String: AnyObject]] else {
                callback(nil)
                return
            }
            
            var groups: [String] = []
            for group in data {
                if let groupId = group["id"] as? String {
                    groups.append(groupId)
                }
            }
            callback(groups)
        }
    }
    
    
    func postRequest(path: String, body: String, callback:@escaping ((_ value: [String: AnyObject]?, _ error: Error?)->())) {
        guard isInitialized else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "not initialized"])
            callback(nil, error)
            return
        }
        
        Alamofire.request("https://c-sar.cropster.com/api/v2/" + path, method: .post, parameters: nil, encoding: body, headers: getHeaders()).validate().responseJSON { (response) in
            guard response.result.isSuccess else {
                if let error = response.result.error {
                    print("Error while fetching groups: \(error)")
                }
                callback(nil, response.result.error)
                return
            }
            
            guard let value = response.result.value as? [String: AnyObject] else {
                    callback(nil, nil)
                    return
            }
            
            callback(value, nil)
        }
    }
    
    func getRequest(path: String, callback:@escaping ((_ value: [String: AnyObject]?, _ error: Error?)->())) {
        Alamofire.request("https://c-sar.cropster.com/api/v2/" + path, method: .get, parameters: nil, encoding: URLEncoding.default, headers: getHeaders()).validate().responseJSON { (response) in
            guard response.result.isSuccess else {
                if let error = response.result.error {
                    print("Error while fetching groups: \(error)")
                }
                callback(nil, response.result.error)
                return
            }
            
            guard let value = response.result.value as? [String: AnyObject] else {
                callback(nil, nil)
                return
            }
            
            callback(value, nil)
        }
    }
    
    
}


extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
}

