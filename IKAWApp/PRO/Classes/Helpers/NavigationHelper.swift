//
//  NavigationHelper.swift
//  IKAWA-Home
//
//  Created by Admin on 10/26/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class NavigationHelper: NSObject {

    class func GoToLoginScreen() {
        UserDefaults.standard.removeObject(forKey: "tutorialWasShowed")
        UserDefaults.standard.synchronize()

        IKInterface.instance()?.disconnect()
        IKInterface.instance()?.stopScan()
        let accountStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let accountNC = accountStoryboard.instantiateInitialViewController()
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = accountNC
    }
    
    class func GoToRegistrationScreen() {
        let accountStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let accountNC = accountStoryboard.instantiateInitialViewController() as? UINavigationController
        if let vc = accountNC?.topViewController as? IKLoginProVC {
            UserDefaults.standard.removeObject(forKey: "tutorialWasShowed")
            UserDefaults.standard.synchronize()

            IKInterface.instance()?.disconnect()
            IKInterface.instance()?.stopScan()
            vc.performSegue(withIdentifier: "IKCreateAccountVCNoAnimation", sender: nil)
            let appDelegate  = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = accountNC
        }
        
    }
    
    class func GoToMainScreen() {
        if !UserDefaults.standard.bool(forKey: "tutorialWasShowed") {
            NavigationHelper.GoToTutorialScreen()
        } else {
            IKInterface.instance()?.connect()
            let accountStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let accountNC = accountStoryboard.instantiateInitialViewController()
            let appDelegate  = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = accountNC
            AppCore.sharedInstance.syncManager.startAccountSynchronization { (userInfo) in
                
            }
        }
    }
    
    
    class func GoToAccountScreen() {
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        //        if let tabBarController = appDelegate.window!.rootViewController as? IKTabBarController {
        //            tabBarController.openAccount()
        //        }
        //
        let accountStoryboard = UIStoryboard(name: "Account", bundle: nil)
        let accountVC = accountStoryboard.instantiateViewController(withIdentifier: "AccountNC")
        appDelegate.window!.rootViewController?.present(accountVC, animated: true, completion: {
            
        })
    }
    
    class func GoToCropsterAccountScreen() {
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        let accountStoryboard = UIStoryboard(name: "Cropster", bundle: nil)
        let accountVC = accountStoryboard.instantiateViewController(withIdentifier: "IKCropsterAccountVC")
        appDelegate.window!.rootViewController?.present(accountVC, animated: true, completion: {
            
        })
    }
    
    class func GoToArchivedProfileScreen() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let tabBarController = appDelegate.window!.rootViewController as? IKTabBarController {
            tabBarController.deselectItem()
            tabBarController.openArchivedProfile()
        }
    }
    
    class func GoToTutorialScreen() {
        UserDefaults.standard.set(true, forKey: "tutorialWasShowed")
        UserDefaults.standard.synchronize()
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        if let tabBarController = appDelegate.window!.rootViewController as? IKTabBarController {
            tabBarController.openTutorial()
        } else {
            let tutorialStoryboard = UIStoryboard(name: "Tutorial", bundle: nil)
            let tutorialNC = tutorialStoryboard.instantiateInitialViewController()
            appDelegate.window!.rootViewController = tutorialNC
            IKNotificationManager.sharedInstance.update()
        }
    }
    
    class func openProfile(profile: Profile) {
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        var profileLibVC: IKProfileLibVC?
        if let tabbar = appDelegate.window!.rootViewController as? IKTabBarController {
            tabbar.selectItem(index: TabBarItem.home.rawValue)
            if let nc = tabbar.viewControllers?[TabBarItem.home.rawValue] as? UINavigationController  {
                nc.popToRootViewController(animated: false)
                profileLibVC = nc.viewControllers[0] as? IKProfileLibVC
            }
        } else {
            GoToMainScreen()
            if let tabbar = appDelegate.window!.rootViewController as? IKTabBarController {
                tabbar.selectItem(index: TabBarItem.home.rawValue)
                if let nc = tabbar.viewControllers?[TabBarItem.home.rawValue] as? UINavigationController  {
                    nc.popToRootViewController(animated: false)
                    profileLibVC = nc.viewControllers[0] as? IKProfileLibVC
                }
            }
            
        }
        
        if let profileLibVC = profileLibVC {
            profileLibVC.performSegue(withIdentifier: "Chart", sender: profile)
        }
        
    }
    
    class func GoToUpdateAccountScreen() {
        if let user = AppCore.sharedInstance.firebaseManager.currentUser, user.isEmailVerified {
            let appDelegate  = UIApplication.shared.delegate as! AppDelegate
            let accountStoryboard = UIStoryboard(name: "Login", bundle: nil)
            if let accountNC = accountStoryboard.instantiateInitialViewController() as? UINavigationController {
                let updateAccountVC = accountStoryboard.instantiateViewController(withIdentifier: "IKUpdateAccountVC")
                accountNC.pushViewController(updateAccountVC, animated: false)
                appDelegate.window!.rootViewController = accountNC
            }
        }
        
    }
    
    class func highlightRoastTab() {
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        if let tabbar = appDelegate.window!.rootViewController as? IKTabBarController {
            if tabbar.bottomNavBar.selectedItem != nil && tabbar.highlightTabIndex() != TabBarItem.roast.rawValue {
                tabbar.highlightTab(index: TabBarItem.roast.rawValue)
            }
        }
    }
}
