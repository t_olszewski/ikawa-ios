//
//  DefaultFillFormatter.swift
//  Charts
//
//  Copyright 2015 Daniel Cohen Gindi & Philipp Jahoda
//  A port of MPAndroidChart for iOS
//  Licensed under Apache License 2.0
//
//  https://github.com/danielgindi/Charts
//

import Foundation
import CoreGraphics
import Charts

#if !os(OSX)
    import UIKit
#endif

/// Default formatter that calculates the position of the filled line.
//@objc(ChartDefaultFillFormatter)
open class FillFormatter: NSObject, IFillFormatter
{
    private var deltaY: Double
    public typealias Block = (
        _ dataSet: ILineChartDataSet,
        _ dataProvider: LineChartDataProvider) -> CGFloat
    
    @objc open var block: Block?
    
    public init(deltaY: Double = 0) {
        self.deltaY = deltaY
    }
    
    @objc public init(block: @escaping Block, deltaY: Double = 0)
    {
        self.block = block
        self.deltaY = deltaY
    }
    
    @objc public static func with(block: @escaping Block) -> FillFormatter?
    {
        return FillFormatter(block: block)
    }
    
    open func getFillLinePosition(
        dataSet: ILineChartDataSet,
        dataProvider: LineChartDataProvider) -> CGFloat
    {
        guard block == nil else { return block!(dataSet, dataProvider) }
        var fillMin: CGFloat = 0.0

        if dataSet.yMax > 0.0 && dataSet.yMin < 0.0
        {
            fillMin = 0.0
        }
        else if let data = dataProvider.data
        {
            let max = data.yMax > 0.0 ? 0.0 : dataProvider.chartYMax + deltaY
            let min = data.yMin < 0.0 ? 0.0 : dataProvider.chartYMin + deltaY

            fillMin = CGFloat(dataSet.yMin >= 0.0 ? min : max)
        }

        return fillMin
    }
}
