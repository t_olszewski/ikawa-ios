//
//  FormatHelper.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 8/28/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class FormatHelper {
    
    class func timeFormat<T>(seconds: T, stringFormat: String = "%d:%0.2d") -> String {
        func format(_ float: Float) -> String {
            return String(format: stringFormat, (Int(float / 60)), Int(round(float.truncatingRemainder(dividingBy: 60))))
        }
        if seconds is Int, let intValue = seconds as? Int {
            let floatValue = Float(intValue)
            return format(floatValue)
        }
        if seconds is Double, let doubleValue = seconds as? Double {
            let floatValue = Float(doubleValue)
            return format(floatValue)
        }
        if seconds is CGFloat, let floatValue = seconds as? CGFloat {
            return format(Float(floatValue))
        }
        if seconds is Float, let floatValue = seconds as? Float {
            return format(floatValue)
        }
        return ""
    }
    
    class func tempFormat<T>(temperature: T,_ stringFormat: String = "%.0f",_ showDegreeMeasure: Bool = true) -> String {
        func format(_ float: Double) -> String {
            return String(format: stringFormat + (showDegreeMeasure ? (USE_FAHRENHEIT ? "℉" : "℃") : ""), USE_FAHRENHEIT ? round(TempToFahrenheit(temp: float)) : round(float))
        }
        if temperature is Int, let intValue = temperature as? Int {
            let floatValue = Double(intValue)
            return format(floatValue)
        }
        if temperature is Double, let doubleValue = temperature as? Double {
            let floatValue = doubleValue
            return format(floatValue)
        }
        if temperature is CGFloat, let floatValue = temperature as? CGFloat {
            let floatValue = Double(floatValue)
            return format(floatValue)
        }
        if temperature is Float, let floatValue = temperature as? Float {
            let floatValue = Double(floatValue)
            return format(floatValue)
        }
        return ""
    }
}
