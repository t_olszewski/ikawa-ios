//
//  CurrenPasswordModalPro.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 11/6/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import IHKeyboardAvoiding
import UIKit

class CurrentPasswordModal: ModalView {
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var dialog: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    //subtitleLabel
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    
    var cancelButtonCallback: (() -> Void)?
    var saveButtonCallback: (( _ password: String) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundView = background
        textField.keyboardAppearance = .dark
        dialog.layer.shadowColor = UIColor.black.cgColor
        dialog.layer.shadowOpacity = 0.5
        dialog.layer.shadowOffset = CGSize(width: 0, height: 1)
        dialog.layer.shadowRadius = 4
    }
    
    override func show(animated: Bool, dismissCallback: (() -> ())?) {
        super.show(animated: animated, dismissCallback: dismissCallback)
        correctAppearance()
        textField.becomeFirstResponder()
        textField.setLeftPaddingPoints(12)
        
    }
    
    override func dismiss(animated: Bool) {
        super.dismiss(animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    //MARK: - Actions
    
    @IBAction func onButtonCancelTap() {
        dismiss(animated: true)
        endEditing(true)
        cancelButtonCallback?()
    }
    
    @IBAction func onButtonSaveTap(_ sender: Any) {
        dismiss(animated: true)
        endEditing(true)
        guard textField.text != "" else {
            return
        }
        saveButtonCallback?(textField.text!)
    }
    
    @IBAction func clearButtonAction(_ sender: Any) {
        textField.text = ""
    }

    
    private func correctAppearance() {
        dialog.layer.shadowColor = UIColor.black.cgColor
        dialog.layer.shadowOpacity = 1
        dialog.layer.shadowOffset = CGSize(width: 3, height: 3)
        dialog.layer.shadowRadius = 5
        dialog.layer.masksToBounds = false
        dialog.layer.cornerRadius = 5
        
        KeyboardAvoiding.avoidingView = dialog
        
        cancelButton.correctAppearance(title: "$cancel".localized, type: .Cancel, image: nil)
        okButton.correctAppearance(title: "$ok".localized, type: .Filled, image: nil)


    }
    
}
