//
//  CreateRecipe.swift
//  IKAWA-Home
//
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class CreateRecipeModalView: ModalView {
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var dialog: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    //@IBOutlet weak var recipeNameTitleLabel: UILabel!
    @IBOutlet weak var recipeNameTextField: UITextField!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var clearButton: UIButton!
    
    var cancelButtonCallback: (() -> Void)?
    var saveButtonCallback: (( _ recipeName: String) -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func show(animated: Bool, dismissCallback: (() -> ())?) {
        super.show(animated: animated, dismissCallback: dismissCallback)
        correctAppearance()
        recipeNameTextField.becomeFirstResponder()
        recipeNameTextField.setLeftPaddingPoints(12)
        recipeNameTextField.selectAll(nil)
        //recipeNameTextField.backgroundColor = kColorHighlightedTextField
       // recipeNameTextField.subviews[1].subviews[0].backgroundColor = kColorHighlightedTextField
    }
    
    //MARK: - Actions
    
    @IBAction func onButtonCancelTap() {
        dismiss(animated: true)
        endEditing(true)
        cancelButtonCallback?()
    }
    
    @IBAction func onButtonSaveTap(_ sender: Any) {
        dismiss(animated: true)
        endEditing(true)
        guard recipeNameTextField.text != "" else {
            return
        }
        saveButtonCallback?(recipeNameTextField.text!)
    }
    
    @IBAction func receiptNameTextFieldChanged(_ sender: Any) {
        if let receipName = recipeNameTextField.text {
            if receipName.count > 30 {
                let index = receipName.index(receipName.startIndex, offsetBy: 30)
                let substring = receipName[..<index]
                recipeNameTextField.text =  String(substring)
            }
        }
    }
    @IBAction func clearButtonAction(_ sender: Any) {
        recipeNameTextField.text = ""
    }
    
    private func correctAppearance() {
        dialog.layer.shadowColor = UIColor.black.cgColor
        dialog.layer.shadowOpacity = 1
        dialog.layer.shadowOffset = CGSize(width: 3, height: 3)
        dialog.layer.shadowRadius = 5
        dialog.layer.masksToBounds = false
        dialog.layer.cornerRadius = 5
        
        KeyboardAvoiding.avoidingView = dialog
        
        cancelButton.correctAppearance(title: "$cancel".localized, type: .Cancel, image: nil)
        saveButton.correctAppearance(title: "$save".localized, type: .Filled, image: nil)


    }
}

