//
//  PushNotificationView.swift
//  IKAWA-Pro
//
//  Created by Admin on 10/31/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

class PushNotificationView: ModalView {
    @IBOutlet weak var rightHeaderMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerStackView: UIStackView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak private var background: UIView!
    @IBOutlet weak private var dialog: UIView!
    
    @IBOutlet weak private var firstButton: UIButton!
    @IBOutlet weak private var secondButton: UIButton!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var descrioptionLabel: UILabel!
    typealias Callback = (() -> ())
    var title: String = ""{
        didSet {
            if title.isEmpty {
               titleLabel.isHidden = true
               // headerStackViewHeightConstraint.constant = subtitleLabel.isHidden ? 0 : subtitleLabel.frame.height
            } else {
               titleLabel.isHidden = false
               titleLabel.text = title
               titleLabel.letterSpace = 1
            }

        }
    }
    
    var subtitle: String = ""{
        didSet {
            if subtitle.isEmpty {
               subtitleLabel.isHidden = true
            } else {
                subtitleLabel.isHidden = false
                subtitleLabel.text = subtitle
                subtitleLabel.letterSpace = 1
            }
            
        }
    }
    
    var descr: String = ""{
        didSet {
            if descr.isEmpty {
                descrioptionLabel.isHidden = true
            } else {
                descrioptionLabel.isHidden = false
                descrioptionLabel.text = descr
                descrioptionLabel.letterSpace = 1
            }
            
        }
    }
    
    var firstButtonTitle: String = ""{
        didSet {
            if firstButtonTitle.isEmpty {
                firstButton.isHidden = true
            } else {
                secondButton.isHidden = false
            }

        }
    }
    
    var secondButtonTitle: String = ""{
        didSet {
            if secondButtonTitle.isEmpty {
                secondButton.isHidden = true
            } else {
                secondButton.isHidden = false
            }
        }
    }
    
    var firstButtonCallback: Callback?
    var secondButtonCallback: Callback?
    
    //MARK: - Lifecicle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Public
    func hideThumbnail() {
        thumbnailImageView.isHidden = true
        rightHeaderMarginConstraint.priority = UILayoutPriority(1000)
    }

    
    override public func show(animated: Bool, dismissCallback: (() -> ())?) {
        super.show(animated: animated, dismissCallback: dismissCallback)
        correctAppearance()
    }
    
    
    private func correctAppearance() {
        dialog.layer.shadowColor = UIColor.black.cgColor
        dialog.layer.shadowOpacity = 1
        dialog.layer.shadowOffset = CGSize(width: 3, height: 3)
        dialog.layer.shadowRadius = 5
        dialog.layer.masksToBounds = false
        dialog.layer.cornerRadius = 5
        
        firstButton.correctAppearance(title: firstButtonTitle, type: .Filled, image: nil)
        secondButton.correctAppearance(title: secondButtonTitle, type: .Bordered, image: nil)

    }
    
    @IBAction func closeButtonTouchUpInside(_ sender: Any) {
        dismiss(animated: true)
    }
    @IBAction func firstButtonTouchUpInside() {
        dismiss(animated: true)
        firstButtonCallback?()
    }
    
    @IBAction func secondButtonTouchUpInside(_ sender: Any) {
        dismiss(animated: true)
        secondButtonCallback?()
    }
    

}

