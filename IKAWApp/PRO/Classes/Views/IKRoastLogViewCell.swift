//
//  IKRoastLogCell.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 4/19/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SnapKit
import IKRoasterLib

class IKRoastLogViewCell: NibLoadingView {
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    
    private static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = NSLocale.current//Locale(identifier: "en_US")
        formatter.dateFormat = "dd MMMM yyyy (h:mma)"
        return formatter
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileNameLabel.textColor = kColorTableCellText
    }
    
    func populate(profile: Profile, roastNotes: IKRoastNotes) {
        guard let roast = roastNotes.roast else {
            return
        }
        if let uuid = roast.uuid, uuid.count >= 3 {
            let index = uuid.index(uuid.startIndex, offsetBy: 3)
            numberLabel.text = String(uuid[..<index])
        }
        
        profileNameLabel.text = profile.getNameWithType()
        if let roastingTimeInSec = roast.lastRoastingPoint()?.time {
            infoLabel.text = String(format: "%@ to %@", FormatHelper.timeFormat(seconds: round(roastingTimeInSec)), roastNotes.endTempDescription())
            infoLabel.letterSpace = 0.5
        }
        var dateStr = ""
        if let roastDateTimeInterval = roast.date {
            dateStr = (IKRoastLogViewCell.formatter.string(from: Date(timeIntervalSince1970: roastDateTimeInterval))).replacingOccurrences(of: "AM", with: "am").replacingOccurrences(of: "PM", with: "pm")//.lowercased()
            //dateStr.replacingOccurrences(of: "AM", with: "am")
            //dateStr.replacingOccurrences(of: "PM", with: "pm")
        }
        dateLabel.text = dateStr
        dateLabel.letterSpace = 0.5
        if profile.tempSensor == TempSensorBelow {
            thumbnailImageView.image = UIImage(named: "roundInlet")
        } else {
            thumbnailImageView.image = UIImage(named: "roundExhaust")
        }
        
    }

}

