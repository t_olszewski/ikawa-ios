//
//  IKSwipeStatusBarView.swift
//  IKAWA-Pro
//
//  Created by Admin on 8/26/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import SwipeCellKit
import SnapKit
import IKRoasterLib

class IKSwipeStatusBarView: NibLoadingView {
    @IBOutlet weak var tableView: UITableView!
    var swipeCellTransitionContext: SwipeActionTransitioningContext?
    var profile: Profile? {
        didSet {
            statusBarView.populate(profile: profile)
            tableView.reloadData()
        }
    }
    var statusBarView: IKStatusBarView!
    let cell = SwipeTableViewCell()
    
    
    
    //MARK: - Lifecicle
    override func awakeFromNib() {
        super.awakeFromNib()
        cell.delegate = self
        statusBarView = IKStatusBarView()
        cell.contentView.addSubview(statusBarView)
        statusBarView.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(cell.contentView).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
    }
    
    
    // MARK: - Notification
    @objc func profileManagerDidChange() {
        self.tableView.reloadData()
    }
    
}

extension IKSwipeStatusBarView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let profile = profile else {
            return UITableViewCell()
        }
        
        //statusBarView.populate(profile: profile)

        return cell
    }
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        guard let profile = displayArray?[indexPath.row] else {
//            return 0
//        }
//        if (IK_SIMULATE_ROASTER) {
//            if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile.uuid) {
//                return 0
//            }
//        } else {
//            if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile.uuid && (IKInterface.instance().isConnected() || IK_SIMULATE_ROASTER)) {
//                return 0
//            }
//        }
//
//        return 71
//    }
}

extension IKSwipeStatusBarView: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard let profile = profile else {
            return nil
        }
        if orientation == .right {
            
            let favoriteAction = SwipeAction(style: .default, title: nil, handler: { action, indexPath in
                NotificationCenter.default.removeObserver(self, name: Notification.Name.ProfileManagerDidChangeList, object: nil)
                _ = AppCore.sharedInstance.profileManager.favorProfile(isFavorite: !profile.favorite, profile: profile)
                let image = UIImage(named: profile.favorite ? "FavoriteHighlight" : "Favorite" )
                self.swipeCellTransitionContext?.button.setImage(image, for: .normal)
                NotificationCenter.default.addObserver(self, selector:#selector(self.profileManagerDidChange), name: NSNotification.Name.ProfileManagerDidChangeList, object: nil)
                self.statusBarView.favoriteImage.isHidden = !profile.favorite
                
            })
            favoriteAction.transitionDelegate = self
            favoriteAction.image = UIImage(named: profile.favorite ? "FavoriteHighlight" : "Favorite" )
            favoriteAction.backgroundColor = kColorLightGrey3
            favoriteAction.hidesWhenSelected = true
            var actionArray = [SwipeAction]() //[favoriteAction]
            if /*RoasterManager.instance().roaster != nil && profile.isRoasterProfile()*/ true {
                let shareAction = SwipeAction(style: .default, title: "$share".localized) {
                    action, indexPath in
                    AppCore.sharedInstance.profileManager.shareProfile(profile: profile)
                }
                shareAction.font = UIFont(name: "AvenirNext-Medium", size: 10)
                shareAction.image = UIImage(named: "shareImage")
                shareAction.hidesWhenSelected = true
                shareAction.backgroundColor = kColorLightGrey1
                shareAction.textColor = kColorDarkGrey1
                actionArray.append(shareAction)
            }
            actionArray.append(favoriteAction)
            return actionArray
        }
        return nil
    }
    
    func visibleRect(for tableView: UITableView) -> CGRect? {
        return tableView.bounds
    }
    
}

extension IKSwipeStatusBarView: SwipeActionTransitioning {
    func didTransition(with context: SwipeActionTransitioningContext) -> Void {
        swipeCellTransitionContext = context
    }
}
