//
//  SyncIndicator.swift
//  IKAWA-Pro
//
//  Created by Admin on 10/31/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

class ProgressIndicatorView: NibLoadingView {
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    var isAnimating = false
    
    let colors = [UIColorFromRGB(216, g: 216, b: 216), UIColorFromRGB(134, g: 134, b: 134), UIColorFromRGB(106, g: 106, b: 106)]
    var views: [UIView]!
    //MARK: - Lifecicle
    override func awakeFromNib() {
        super.awakeFromNib()
        views = [view1, view2, view3]
        views.forEach { (view) in
            view.layer.cornerRadius = view.frame.size.width / 2
        }
        isHidden = true
    }
    
    deinit {
    }
    
    //MARK: - Public
    func startAnimation(counter: Int = 0) {
        isHidden = false
        isAnimating = true
        let startIndex = (counter % views.count)
        UIView.animate(withDuration: 0.4, animations: {
            self.views[startIndex].backgroundColor = self.colors[0]
            self.views[(startIndex + 1) % self.views.count].backgroundColor = startIndex == 2 ? self.colors[2] : self.colors[1]
            self.views[(startIndex + 2) % self.views.count].backgroundColor = startIndex == 2 ? self.colors[1] : self.colors[2]
            }, completion: { (success) in
            if self.isAnimating {
                self.startAnimation(counter: counter + 1)
            }
        })
    }
    
    func stopAnimation() {
        isHidden = true
        isAnimating = false
        layer.removeAllAnimations()
    }
    

    
}


