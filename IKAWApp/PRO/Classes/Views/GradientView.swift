//
//  GradientView.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 4/26/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class GradientView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.withGradienBackground(color1: #colorLiteral(red: 0.07676819712, green: 0.07686258107, blue: 0.07678283006, alpha: 1), color2: #colorLiteral(red: 0.09793549031, green: 0.09804504365, blue: 0.09795244783, alpha: 1), color3: #colorLiteral(red: 0.1482854784, green: 0.1484311223, blue: 0.1483080685, alpha: 1), color4: #colorLiteral(red: 0.15315786, green: 0.1533069313, blue: 0.1531809866, alpha: 1))
    }
}
