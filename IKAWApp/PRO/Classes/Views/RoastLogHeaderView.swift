//
//  RoastLogHeaderView.swift
//  IKAWA-Pro
//
//  Created by Admin on 10/8/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class RoastLogHeaderView: NibLoadingView {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var roundImageView: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var profileName: UILabel!

    var backButtonAction: (()->())?
    
    @IBAction func backButtonTouchUpInside(_ sender: Any) {
        backButtonAction?()
    }
    
}
