//
//  SyncIndicator.swift
//  IKAWA-Pro
//
//  Created by Admin on 10/31/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

class SyncIndicatorView: NibLoadingView {
    @IBOutlet weak var image: UIImageView!
    var timer: Timer?
    var isAnimating: Bool {
        get {
           return !image.isHidden
        }
    }
    private var needShowAnimation = false
    
    //MARK: - Lifecicle
    override func awakeFromNib() {
        super.awakeFromNib()
        image.isHidden = true
        
       NotificationCenter.default.addObserver(self, selector: #selector(startAnimation), name: NSNotification.Name.DidStartSync, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(stopAnimation), name: NSNotification.Name.DidFinishSync, object: nil)
        if AppCore.sharedInstance.syncManager.isSyncProcess {
            startAnimation()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Public
    @objc func startAnimation() {
        needShowAnimation = true
        timer?.invalidate()
        timer = nil
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(timerDidFinish), userInfo: nil, repeats: false)
        image.isHidden = false
        let fullRotation = CABasicAnimation(keyPath: "transform.rotation")
        fullRotation.isRemovedOnCompletion = false
        fullRotation.delegate = self
        fullRotation.fromValue = NSNumber(floatLiteral: 0)
        fullRotation.toValue = NSNumber(floatLiteral: Double(CGFloat.pi * 2))
        fullRotation.duration = 1.0
        fullRotation.repeatCount = MAXFLOAT
        image.layer.add(fullRotation, forKey: "360")
    }
    
    @objc func stopAnimation() {
        needShowAnimation = false
        guard timer == nil else {
            return
        }
        image.isHidden = true
        image.layer.removeAllAnimations()
    }
    
    @objc func timerDidFinish() {
        timer?.invalidate()
        timer = nil
        if !needShowAnimation {
            stopAnimation()
        }

    }
}

extension SyncIndicatorView: CAAnimationDelegate {
    func animationDidStart(_ anim: CAAnimation) {
        
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        
    }
}
