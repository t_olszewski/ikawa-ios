//
//  IKRoasLogAddImageCell.swift
//  IKAWA-Pro
//
//  Created by Admin on 8/28/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage
import TOCropViewController
import Alamofire

enum CellState {
    case open
    case close
    case empty
}

protocol IKRoastLogAddImageCellDelegate: AnyObject {
    //func tapActionButton(cell: IKRoastLogAddImageCell)
    func stateChanged(cell: IKRoastLogAddImageCell)
}

class IKRoastLogAddImageCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var photoContainerView: UIView!
    let imagePicker = UIImagePickerController()
    private var notes: IKRoastNotes?
    public var state = CellState.empty {
        didSet {
            switch state {
            case .open:
                if let notes = self.notes, !notes.photo.isEmpty, photoImageView.image == nil {
                    self.makeToastActivity(.center)
                    CasheManager.sharedInstance.getRoastLogImage(notes.photo) { (image, error) in
                        self.hideToastActivity()
                        if let image = image {
                            self.photoImageView.image = image.roundedImage
                        }
                    }
                }
                photoContainerView.isHidden = false
                actionButton.setImage(UIImage(named: "ArrowUp"), for: .normal)
            case .close:
                photoContainerView.isHidden = true
                actionButton.setImage(UIImage(named: "ArrowDown"), for: .normal)
            case .empty:
                photoContainerView.isHidden = true
                actionButton.setImage(UIImage(named: "AddButton2"), for: .normal)
            }
        }
    }
    public weak var delegate: IKRoastLogAddImageCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        state = .close
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    
    func populate(notes: IKRoastNotes) {
        self.notes = notes
        if  !notes.photo.isEmpty, let photoRef = AppCore.sharedInstance.firebaseManager.roastLogPhotoUrlReference(roasatId: notes.photo) {
            titleLabel.text = "$photo".localized
        } else {
            self.state = .empty
            titleLabel.text = "$add_photo".localized
        }
    }

    
    //MARK: Actions
    @IBAction func actionButtonTouchUpInside(_ sender: Any) {
        if state == .close {
            state = .open
            delegate?.stateChanged(cell: self)
        } else if state == .open {
            state = .close
            delegate?.stateChanged(cell: self)
        }
        
        if state == .empty {
            
            let selectDialogView = ButtonsPopupModalView()
            selectDialogView.title = ""
            selectDialogView.layout = .vertical
            
            selectDialogView.setUpButton(index: .First, title: "$camera_up".localized, type: .Bordered) {
                guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                    return
                }
                self.imagePicker.delegate = self
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = .camera
                if let topController = UIApplication.shared.delegate?.window??.rootViewController {
                    topController.present(self.imagePicker, animated: true, completion: nil)
                }
            }
            
            selectDialogView.setUpButton(index: .Second, title: "$gallery_up".localized, type: .Filled) {
                self.imagePicker.delegate = self
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = .photoLibrary
                if let topController = UIApplication.shared.delegate?.window??.rootViewController {
                    topController.present(self.imagePicker, animated: true, completion: nil)
                }
            }
            
            selectDialogView.setUpButton(index: .Third, title: "$cancel".localized, type: .Cancel) {
                
            }
            IKNotificationManager.sharedInstance.show(view: selectDialogView)
        }
        
    }
    
    @IBAction func deleteButtonTouchUpInside(_ sender: Any) {
        if let roastId = notes?.roast?.uuid {
            
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .horisontal
            confirmationDialogView.title = NSLocalizedString("$delete_this_photo", comment: "")
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$cancel_up", comment: ""), type: .Cancel) {}
            confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$alert_delete", comment: ""), type: .Filled) {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_REMOVE_PHOTO", label: "")
                self.makeToastActivity(.center)
                 DispatchQueue.global(qos: .background).async { [unowned self] in
                                let fileManager = FileManager.default
                                do {
                                    let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
                                    let fileURL = documentDirectory.appendingPathComponent("roastLogImages/\(roastId)")
                                    try fileManager.removeItem(at: fileURL)
                                } catch {
                                    print(error)
                                }
                                CasheManager.sharedInstance.clearRoastLogImage(roastId, callback: {})
                                if AppCore.sharedInstance.settingsManager.workingWithoutAccount || !(NetworkReachabilityManager()?.isReachable ?? false) {
                                    DispatchQueue.main.async {
                                                                            self.hideToastActivity()
                                        self.titleLabel.text = "$add_photo".localized
                                        self.notes?.photo = ""
                                        self.notes?.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
                                        _ = AppCore.sharedInstance.roastNotesManager.saveRoastNotes(roastNotes: self.notes!, needReloadData: true)
                                        self.state = .empty
                                        self.delegate?.stateChanged(cell: self)
                                        
                                        let errorDialogView = ButtonsPopupModalView()
                                        errorDialogView.layout = .vertical
                                        errorDialogView.title = NSLocalizedString("$no_internet_connection", comment: "")
                                        errorDialogView.subtitle = ""
                                        errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                                        }
                                        IKNotificationManager.sharedInstance.show(view: errorDialogView)
                                        
                                    }

                                } else {
                                    AppCore.sharedInstance.firebaseManager.deleteRoastImage(roasatId: roastId) { [unowned self] (success, message) in
                                        DispatchQueue.main.async {
                                            self.hideToastActivity()
                                            
                                            self.titleLabel.text = "$add_photo".localized
                                            self.notes?.photo = ""
                                            self.notes?.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
                                            _ = AppCore.sharedInstance.roastNotesManager.saveRoastNotes(roastNotes: self.notes!, needReloadData: true)
                                            self.state = .empty
                                            self.delegate?.stateChanged(cell: self)
                                        }

                //                        if success {
                //                            self.titleLabel.text = "$add_photo".localized
                //                            self.notes?.photo = ""
                //                            self.notes?.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
                //                            _ = AppCore.sharedInstance.roastNotesManager.saveRoastNotes(roastNotes: self.notes!, needReloadData: true)
                //                            self.state = .empty
                //                            self.delegate?.stateChanged(cell: self)
                //                        } else {
                //                            if let message = message {
                //                                let errorDialogView = ButtonsPopupModalView()
                //                                errorDialogView.layout = .vertical
                //                                errorDialogView.title = ""
                //                                errorDialogView.subtitle = message
                //                                errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                //                                errorDialogView.show(animated: true, dismissCallback: {})
                //                            }
                //                        }
                                    }
                                }
                }

            }
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
        }
    }
    
    //MARK: Private
    func loadImage(url: URL?) {
        self.makeToastActivity(.center)
        SDWebImageManager.shared.loadImage(with: url, options: .refreshCached, progress: nil) { (image, data, error, cashType, success, url) in
            self.hideToastActivity()
            if let image = image {
                self.photoImageView.image = image.roundedImage
            }
        }
    }
    
}

extension IKRoastLogAddImageCell: UIImagePickerControllerDelegate,
UINavigationControllerDelegate, TOCropViewControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated:true, completion: nil)
        let initialImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        //            let image = self.fixOrientation(img: initialImage)
        let cropViewController = TOCropViewController(image: initialImage)
        cropViewController.delegate = self
        cropViewController.aspectRatioPreset = .presetSquare
        cropViewController.aspectRatioLockEnabled = true
        if let topController = UIApplication.shared.delegate?.window??.rootViewController {
            topController.present(cropViewController, animated: true, completion: nil)
        }
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        cropViewController.dismiss(animated: true) {
            self.makeToastActivity(.center)
            DispatchQueue.global(qos: .background).async { [unowned self] in
                let image = self.fixOrientation(img: image)
                if let roastId = self.notes?.roast?.uuid {
                    
                    let scale: CGFloat = 1.0
                    let minSize = min(image.size.height, image.size.width)
                    UIGraphicsBeginImageContextWithOptions(CGSize(width: minSize / scale, height: minSize / scale), false, 1.0)
                    image.draw(in: CGRect(x: -(image.size.width - minSize) / 2 / scale, y: -(image.size.height - minSize) / 2 / scale, width: image.size.width / scale, height: image.size.height / scale))
                    let cropedImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    
                    if let cropedImage_ = cropedImage {
                        let scaledImage = cropedImage_.compressImage()
                        AppCore.sharedInstance.loggingManager.trackEvent(name: "PRO_LOG_ADD_PHOTO", label: "")
                        CasheManager.sharedInstance.setRoastLogImage(roastId, scaledImage) {
                            
                        }
                        if (AppCore.sharedInstance.settingsManager.workingWithoutAccount || !(NetworkReachabilityManager()?.isReachable ?? false)) {
                            self.saveImageLocaly(image: scaledImage, roastId: roastId)
                        } else {
                            AppCore.sharedInstance.firebaseManager.uploadRoastImage(image: scaledImage, fileName: roastId) { (success, message) in
                                if success {
                                    DispatchQueue.main.async {
                                        self.photoImageView.image = scaledImage.roundedImage
                                        self.titleLabel.text = "$photo".localized
                                        self.photoImageView.image = scaledImage.roundedImage
                                        self.hideToastActivity()
                                        self.notes?.photo = roastId
                                        self.notes?.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
                                        _ = AppCore.sharedInstance.roastNotesManager.saveRoastNotes(roastNotes: self.notes!, needReloadData: true)
                                        self.state = .open
                                        self.delegate?.stateChanged(cell: self)
                                    }
                                } else {
                                    self.saveImageLocaly(image: scaledImage, roastId: roastId)
                                }
                            }
                            
                        }
                    }
                }
            }
            
        }
    }
    
    func saveImageLocaly(image: UIImage, roastId: String) {
        if let error = AppCore.sharedInstance.syncManager.saveLocalImage(image: image, folder: "roastLogImages", fileName: "\(roastId)") {
            DispatchQueue.main.async {
                let errorDialogView = ButtonsPopupModalView()
                errorDialogView.layout = .vertical
                errorDialogView.title = "Error".localized
                errorDialogView.subtitle = error.localizedDescription
                errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                
                IKNotificationManager.sharedInstance.show(view: errorDialogView)
            }
        } else {
            DispatchQueue.main.async {
                self.photoImageView.image = image.roundedImage
                self.titleLabel.text = "$photo".localized
                self.photoImageView.image = image.roundedImage
                self.hideToastActivity()
                self.notes?.photo = roastId
                self.notes?.dateLastEdited = AppCore.sharedInstance.settingsManager.internetTime()
                _ = AppCore.sharedInstance.roastNotesManager.saveRoastNotes(roastNotes: self.notes!, needReloadData: true)
                self.state = .open
                self.delegate?.stateChanged(cell: self)
            }
        }
    }
    
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
}

