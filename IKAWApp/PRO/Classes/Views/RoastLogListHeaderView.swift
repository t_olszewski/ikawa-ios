//
//  ProfileLibHeaderView.swift
//  IKAWA-Home
//
//  Created by Admin on 10/15/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SnapKit
import IKRoasterLib

protocol RoastLogListHeaderViewDelegate: AnyObject {
    func didTapMenuButton()
    func search(text:String)
    func layoutHeaderViewDidChanged()
}

class RoastLogListHeaderView: NibLoadingView {

    fileprivate struct Constants {
        static let headerViewNavBarHeight: CGFloat = 56
        static let headerViewSearchBarHeight: CGFloat = 56
        static let headerViewStatusViewHeight: CGFloat = 72
    }
    
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cancelSearchButton: UIButton!
    
    weak var delegate: RoastLogListHeaderViewDelegate?
    var searchStr = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        searchBar.delegate = self
        searchBar.placeholder = NSLocalizedString("$search_roast_logs", comment: "")
        searchBar.setImage(UIImage(), for: .clear, state: .normal)
        searchBar.setTextFieldFont()
        searchBar.keyboardAppearance = UIKeyboardAppearance.dark
        hideSearch()
    }
    
    //MARK: - Actions
    
    func setupPlaceholder() {
        
    }
    
    @IBAction func menuButtonTouchUpInside(_ sender: Any) {
        if let delegate = delegate {
            resetSearch()
            hideSearch()
            delegate.didTapMenuButton()
        }
    }
    
    @IBAction func searchButtonTouchUpInside(_ sender: Any) {
        showSearch()
        searchBar.becomeFirstResponder()
    }

    @IBAction func cancelSearchButtonTouchUpInside(_ sender: Any) {
        searchBar.text = ""
        resetSearch()
        hideSearch()
    }
    
    //MARK: - Private
    func resetSearch() {
        searchStr = ""
        if let delegate = delegate {
            delegate.search(text: "")
        }
    }
    
    func hideSearch() {
        if (!searchContainerView.isHidden) {
            searchContainerView.isHidden = true
            searchContainerView.snp.remakeConstraints { (make) in
                make.height.equalTo(0)
            }
            if let delegate = delegate {
                delegate.layoutHeaderViewDidChanged()
            }
        }
        searchBar.resignFirstResponder()
        searchButton.isHidden = false
    }
    
    func showSearch() {
        if searchContainerView.isHidden, let delegate = delegate {
            searchContainerView.snp.remakeConstraints { (make) in
                make.height.equalTo(56)
            }
            searchContainerView.isHidden = false
            delegate.layoutHeaderViewDidChanged()
            searchButton.isHidden = true
        }
    }
    
    func minHeight() -> CGFloat {
        return maxHeight()
    }
    
    func maxHeight() -> CGFloat {
        return Constants.headerViewNavBarHeight +
            (searchContainerView.isHidden ? 0 : Constants.headerViewSearchBarHeight)
    }
}



extension RoastLogListHeaderView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            searchBar.resignFirstResponder()
            return false
        }
        searchStr = NSString(string: searchBar.text!).replacingCharacters(in: range, with: text)
        if let delegate = delegate {
            delegate.search(text: searchStr)
        }
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setTextFieldColor(color: .white, textColor: .black)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setTextFieldColor(color: #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1), textColor: .white)
    }
}
