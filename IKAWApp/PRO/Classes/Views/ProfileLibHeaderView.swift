//
//  ProfileLibHeaderView.swift
//  IKAWA-Home
//
//  Created by Admin on 10/15/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SnapKit
import IKRoasterLib

protocol ProfileLibHeaderViewDelegate: AnyObject {
    func didTapMenuButton()
    func search(text:String)
    func layoutHeaderViewDidChanged()
    func tapOnPrfile(profile: Profile)
}

class ProfileLibHeaderView: NibLoadingView {

    fileprivate struct Constants {
        static let headerViewNavBarHeight: CGFloat = 56
        static let headerViewSearchBarHeight: CGFloat = 56
        static let headerViewStatusViewHeight: CGFloat = 72
    }
    
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var swipeStatusBarView: IKSwipeStatusBarView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cancelSearchButton: UIButton!
    
    weak var delegate: ProfileLibHeaderViewDelegate?
    var searchStr = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        searchBar.delegate = self
        searchBar.placeholder = NSLocalizedString("$search_profiles", comment: "")
        searchBar.setImage(UIImage(), for: .clear, state: .normal)
        searchBar.setTextFieldFont()
        searchBar.setTextFieldColor(color: #colorLiteral(red: 0.18204588494, green: 0.17412940423, blue: 0.17667689226, alpha: 1), textColor:#colorLiteral(red: 0.3414238095, green: 0.3501413465, blue: 0.3372663856, alpha: 1))
        searchBar.keyboardAppearance = UIKeyboardAppearance.dark
        RoasterManager.instance().delegates.add(swipeStatusBarView.statusBarView)
        RoasterManager.instance().delegates.add(self)
        NotificationCenter.default.addObserver(self, selector:#selector(degreeCelsiusChanged), name: NSNotification.Name.DegreeCelsiusChanged, object: nil)
        updateStatusBarView()
        hideSearch()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
        if (RoasterManager.instance().delegates.contains(self.swipeStatusBarView.statusBarView)) {
            RoasterManager.instance().delegates.remove(self.swipeStatusBarView.statusBarView)
        }
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    //MARK: - Actions
    
    @IBAction func menuButtonTouchUpInside(_ sender: Any) {
        if let delegate = delegate {
            resetSearch()
            hideSearch()
            delegate.didTapMenuButton()
        }
    }
    
    @IBAction func searchButtonTouchUpInside(_ sender: Any) {
        updateStatusBarView()
        showSearch()
        searchBar.becomeFirstResponder()
    }

    @IBAction func tapOnProfileView(_ sender: Any) {
        if let delegate = delegate {
            if let profile = RoasterManager.instance().profile {
                delegate.tapOnPrfile(profile: profile)
            }
        }
    }
    
    @IBAction func cancelSearchButtonTouchUpInside(_ sender: Any) {
        searchBar.text = ""
        resetSearch()
        hideSearch()
    }
    
    @objc func degreeCelsiusChanged() {
        updateStatusBarView()
    }
    
    //MARK: - Private
    func resetSearch() {
        searchStr = ""
        if let delegate = delegate {
            delegate.search(text: "")
        }
    }
    
    func updateStatusBarView() {
        var needShowStatusBarView = false
        if IK_SIMULATE_ROASTER {
            needShowStatusBarView = true
        } else if (IKInterface.instance().isConnected() || IK_SIMULATE_ROASTER) && swipeStatusBarView.statusBarView.profile?.isRoasterProfile() ?? false {
            needShowStatusBarView = true
        }
        
        if needShowStatusBarView , let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: RoasterManager.instance().profile?.uuid ?? "") {
            swipeStatusBarView.profile = profile
            showStatusBar()
        } else {
            hideStatusBar()
        }
    }
    
    func hideStatusBar() {
        if (!swipeStatusBarView.isHidden) {
            swipeStatusBarView.isHidden = true
            swipeStatusBarView.snp.remakeConstraints { (make) in
                make.height.equalTo(0)
            }
            if let delegate = delegate {
                delegate.layoutHeaderViewDidChanged()
            }
        }
    }
    
    func showStatusBar() {
        if swipeStatusBarView.isHidden, let delegate = delegate {
            swipeStatusBarView.snp.remakeConstraints { (make) in
                make.height.equalTo(72)
            }
            swipeStatusBarView.isHidden = false
            delegate.layoutHeaderViewDidChanged()
        }
    }
    
    func hideSearch() {
        if (!searchContainerView.isHidden) {
            searchContainerView.isHidden = true
            searchContainerView.snp.remakeConstraints { (make) in
                make.height.equalTo(0)
            }
            if let delegate = delegate {
                delegate.layoutHeaderViewDidChanged()
            }
        }
        searchBar.resignFirstResponder()
        searchButton.isHidden = false
    }
    
    func showSearch() {
        if searchContainerView.isHidden, let delegate = delegate {
            searchContainerView.snp.remakeConstraints { (make) in
                make.height.equalTo(56)
            }
            searchContainerView.isHidden = false
            delegate.layoutHeaderViewDidChanged()
            searchButton.isHidden = true
        }
    }
    
    func minHeight() -> CGFloat {
        return maxHeight()
    }
    
    func maxHeight() -> CGFloat {
        return Constants.headerViewNavBarHeight +
            (searchContainerView.isHidden ? 0 : Constants.headerViewSearchBarHeight) +
            (swipeStatusBarView.isHidden ? 0 : Constants.headerViewStatusViewHeight)
    }
}

extension ProfileLibHeaderView: RoasterManagerDelegate {
    //MARK: - RoasterManagerDelegate
    func roasterDidConnect(_ roaster: Roaster!) {
        updateStatusBarView()
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        resetSearch()
        updateStatusBarView()
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        updateStatusBarView()
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        if let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: RoasterManager.instance().profile?.uuid ?? "") {
            swipeStatusBarView.profile = profile
        }
        updateStatusBarView()
    }
}

extension ProfileLibHeaderView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){}
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            searchBar.resignFirstResponder()
            return false
        }
        searchStr = NSString(string: searchBar.text!).replacingCharacters(in: range, with: text)
        updateStatusBarView()
        
        if let delegate = delegate {
            delegate.search(text: searchStr)
        }
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setTextFieldColor(color: .white, textColor: #colorLiteral(red: 0.1285303235, green: 0.1236336902, blue: 0.128546536, alpha: 1))
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setTextFieldColor(color: #colorLiteral(red: 0.18204588494, green: 0.17412940423, blue: 0.17667689226, alpha: 1), textColor: .white)
    }
}
