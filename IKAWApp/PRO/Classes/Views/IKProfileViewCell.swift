//
//  IKProfileViewCell.swift
//  IKAWApp
//
//  Created by Admin on 6/4/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SnapKit
import IKRoasterLib

class IKProfileViewCell: NibLoadingView {
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var favoriteImage: UIImageView!
    @IBOutlet weak var basicView: UIView!
    @IBOutlet weak var rightArrow: UIImageView!
    
    private static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileNameLabel.textColor = kColorTableCellText
    }
    
    func populate(profile: Profile) {
        profileNameLabel.text = profile.getNameWithType()
        profileNameLabel.letterSpace = 1
        let roastingTime = profile.roastingProgressTime()
        infoLabel.text = String(format: "%@ | %@", FormatHelper.timeFormat(seconds: roastingTime), FormatHelper.tempFormat(temperature: profile.finishTemp()))
        
        thumbnailImageView.image = GetImageFromProfile(profile: profile, backgroundColor: profile.archived ? kColorLightGrey3 : nil)
        
        if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile.uuid) {
            progressView.isHidden = false
            updateProgress()
        } else {
            progressView.isHidden = true
        }
        
        var dateStr = ""
        
        if profile.isNeverUsed() {
            dateStr = "$new".localized
        } else if let dateLastUsed = profile.dateLastUsed() {
            if Calendar.current.isDateInToday(dateLastUsed) || dateLastUsed.timeIntervalSinceNow > 0 {
                dateStr = NSLocalizedString("$today", comment: "")
            } else if Calendar.current.isDateInYesterday(dateLastUsed) {
                dateStr = NSLocalizedString("$yesterday", comment: "")
            } else if Date().startOfWeek! <= dateLastUsed {
                dateStr = NSLocalizedString("$this_week", comment: "")
            } else {
                dateStr = IKProfileViewCell.formatter.string(from: dateLastUsed)
            }
        }

        dateLabel.text = dateStr
        dateLabel.letterSpace = 0.5
        infoLabel.letterSpace = 0.5
        
        if profile.archived {
            basicView.backgroundColor = kColorLightGrey3
            favoriteImage.isHidden = !profile.favorite
            profileNameLabel.textColor = kColorLightGrey1
            infoLabel.textColor = kColorLightGrey1
            dateLabel.textColor = kColorLightGrey1
            rightArrow.image = UIImage(named: "rightArrowLG1")
        } else {
            rightArrow.image = UIImage(named: "rightArrow")
            basicView.backgroundColor = kColorDarkGrey2
            favoriteImage.isHidden = !profile.favorite
            profileNameLabel.textColor = kColorWhite
            infoLabel.textColor = kColorWhite
            dateLabel.textColor = kColorWhite
        }
    }
    
    func updateProgress() {
        let progress = AppCore.sharedInstance.roastManager.roastAndCoolDownProgress()
        guard progress > 0 else {
            progressView.isHidden = true
            return
        }
        progressView.isHidden = false
        self.progressView.snp.remakeConstraints { (make) in
            
            make.width.equalTo(thumbnailImageView).multipliedBy(CGFloat(AppCore.sharedInstance.roastManager.roastAndCoolDownProgress())).priority(800)
        }
        switch (RoasterManager.instance().roaster.status.state) {
        case IK_STATUS_ROASTING:
            progressImageView.image = UIImage(color: kColorExhaust)
            break
        case IK_STATUS_COOLING:
            progressImageView.image = UIImage(color: kColorAirflow)
            break
        default:
            progressView.isHidden = true
            break
        }
    }
}

extension IKProfileViewCell : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {

    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {

    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        updateProgress()
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        
        if let profileInLib = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: profile.uuid) {
            profile.revision = profileInLib.revision
        }
        self.populate(profile: profile)
    }
}

