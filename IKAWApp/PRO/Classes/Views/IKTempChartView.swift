//
//  IKTempChartView.swift
//  IKAWApp
//
//  Created by Admin on 3/27/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Charts
import IKRoasterLib

public class MinuteFormatter: NSObject, IAxisValueFormatter
{
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String
    {
         return (value == 0) ? "" : String(format: "%.0f", value / 60)
    }
}

public class TemperatureFormatter: NSObject, IAxisValueFormatter
{
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String
    {
        if (USE_FAHRENHEIT) {
           return value == 0 ? "" : String(format: "%.0f℉", value)
        }
        return value == 0 ? "" : String(format: "%.0f°C", value)
    }
}

public class RORTemperatureFormatter: NSObject, IAxisValueFormatter
{
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String
    {
        var result = ""
        if (USE_FAHRENHEIT) {
            result = value == 0 ? "" :String(format: "%.0f℉/min", (value - kMaxTemperatureFahrenheit) / (kRorScale * 4.0) * 1.8)
        } else {
            result = value == 0 ? "" :String(format: "%.0f°C/min", (value - (kMaxTemperature / 2.0 * kRorScale)) / (2.0 * kRorScale))
        }
        
        return result
    }
}

class IKTempChartView: NibLoadingView {
    
    @IBOutlet weak var chartView: InteractiveChartView!
    var roast: IKRoast?
    var profile: Profile!
    var editedProfile: Profile!
    var timelineSet: LineChartDataSet!
    var editedTempProfileSet: LineChartDataSet!
    var tempProfileSet: LineChartDataSet!
    var editedFanProfileSet: LineChartDataSet!
    var fanProfileSet: LineChartDataSet!
    var tempAboveSet: LineChartDataSet!
    var tempBelowSet: LineChartDataSet!
    var firstCrackSet: LineChartDataSet!
    var secondCrackSet: LineChartDataSet!
    var colorChangeSet: LineChartDataSet!
    var turningPointSet: LineChartDataSet!
    var lastRoastingPointSet: LineChartDataSet!
    var rorSet: LineChartDataSet!
    var rorProfileSet: LineChartDataSet!
    var powerHeaterSet: LineChartDataSet!
    var isEditState = false
    var lastTapedPoint: CGPoint?
    var dataSets: [ChartDataSet]?
    var isWaitungAfterPointDeleting = false
    var cooldownEntryOldValue: ChartDataEntry?
    var graphWasEdited = false
    var entryWasRermoved = false
    var isRORHidden = true {
        didSet {
            if isRORHidden == true {
               hideROR()
            } else {
               showROR()
            }
            
        }
    }
    var isCoolingHidden = false
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        NotificationCenter.default.addObserver(self, selector:#selector(clearCurrentRoast), name: NSNotification.Name.StartNewRoast, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(clearCurrentRoast), name: NSNotification.Name.RoastDidCanceled, object: nil)
        chartView.interactiveDelegate = self
        chartView.delegate = self

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        chartView.interactiveDelegate = self
        chartView.delegate = self
    }
    
    //MARK: - Business
    func populateChart(profile :Profile?, editedProfile: Profile?) {
        self.profile = profile
        if editedProfile == nil {
            self.editedProfile = profile
        } else {
            self.editedProfile = editedProfile
        }
        
        chartView.minOffset = 0
        chartView.extraLeftOffset = 5
        chartView.extraTopOffset = 20
        if UIDevice.current.userInterfaceIdiom == .pad {
           chartView.extraBottomOffset = -25
        } else {
           chartView.extraBottomOffset = -15
        }
        
        chartView.backgroundColor = kColorDarkGrey2//UIColor.black
        chartView.scaleXEnabled = false
        chartView.scaleYEnabled = false
        chartView.xAxis.gridLineDashLengths = [10, 0]
        chartView.xAxis.gridLineDashPhase = 0
        chartView.xAxis.gridColor = kColorChartGride
        chartView.xAxis.gridLineWidth = 0.5
        chartView.xAxis.labelTextColor = kColorChartlabelText
        chartView.xAxis.labelFont = kAxisFont
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.valueFormatter = MinuteFormatter()

        chartView.xAxis.axisMaximum =  Double(kTimeAxisMax + 5 * 60)
        
        chartView.xAxis.axisMinimum = 0
        chartView.xAxis.drawAxisLineEnabled = false
        chartView.xAxis.granularityEnabled = true
        chartView.xAxis.granularity = 59.999
        chartView.xAxis.labelCount = 25
        chartView.pinchZoomEnabled = false
        chartView.scaleXEnabled = false
        chartView.scaleYEnabled = false
        chartView.doubleTapToZoomEnabled = false
        
        let leftAxis = chartView.leftAxis
        leftAxis.removeAllLimitLines()
        if (USE_FAHRENHEIT) {
            leftAxis.axisMaximum = kMaxTemperatureFahrenheit
        } else {
            leftAxis.axisMaximum = kMaxTemperature
        }
        leftAxis.axisMinimum = 0
        leftAxis.gridLineDashLengths = [5, 0]
        leftAxis.drawLimitLinesBehindDataEnabled = true
        leftAxis.gridColor = kColorChartGride
        leftAxis.gridLineWidth = 0.5
        leftAxis.labelTextColor = kColorChartlabelText
        leftAxis.labelFont = kAxisFont
        leftAxis.valueFormatter = TemperatureFormatter()
        leftAxis.drawGridLinesEnabled = true
        leftAxis.granularityEnabled = true
        leftAxis.granularity = 1
        leftAxis.yOffset = -5
        leftAxis.labelPosition = .insideChart
        leftAxis.drawAxisLineEnabled = true
        leftAxis.axisLineWidth = 0.5
        leftAxis.axisLineColor = kColorChartGride
        leftAxis.granularityEnabled = true
        if (USE_FAHRENHEIT) {
            leftAxis.granularity = 100
        } else {
            leftAxis.granularity = 50
        }
        leftAxis.labelCount = 40
        
        
        
        let leftAxis2 = chartView.leftAxis2
        leftAxis2.removeAllLimitLines()
        if (USE_FAHRENHEIT) {
            leftAxis2.axisMaximum = kMaxTemperatureFahrenheit * kRorScale
        } else {
            leftAxis2.axisMaximum = kMaxTemperature * kRorScale
        }
        leftAxis2.axisMinimum = 0
        leftAxis2.gridLineDashLengths = [5, 0]
        leftAxis2.drawLimitLinesBehindDataEnabled = true
        leftAxis2.gridColor = .clear
        leftAxis2.gridLineWidth = 0.5
        leftAxis2.labelTextColor = kColorChartlabelText
        leftAxis2.labelFont = kAxisFont
        leftAxis2.valueFormatter = RORTemperatureFormatter()
        leftAxis2.drawGridLinesEnabled = true
        leftAxis2.granularityEnabled = true
        leftAxis2.granularity = 50 * kRorScale
        leftAxis2.yOffset = 10
        leftAxis2.labelPosition = .insideChart
        leftAxis2.labelTextColor = kColorLightGrey3
        leftAxis2.granularityEnabled = true
        leftAxis2.labelCount = 5
        leftAxis2.drawAxisLineEnabled = false
        
        let rightAxis = chartView.rightAxis
        rightAxis.removeAllLimitLines()
        if (USE_FAHRENHEIT) {
            rightAxis.axisMaximum = kMaxTemperatureFahrenheit
        } else {
            rightAxis.axisMaximum = kMaxTemperature
        }
        rightAxis.axisMinimum = 0
        rightAxis.gridLineDashLengths = [5, 0]
        rightAxis.drawLimitLinesBehindDataEnabled = true
        rightAxis.gridColor = kColorChartGride
        rightAxis.gridLineWidth = 0.5
        rightAxis.labelTextColor = kColorChartlabelText
        rightAxis.labelFont = kAxisFont
        rightAxis.valueFormatter = TemperatureFormatter()
        rightAxis.drawGridLinesEnabled = true
        rightAxis.granularityEnabled = true
        rightAxis.granularity = 1
        rightAxis.yOffset = -5
        rightAxis.labelPosition = .insideChart
        rightAxis.enabled = false
        rightAxis.granularityEnabled = true
        if (USE_FAHRENHEIT) {
            rightAxis.granularity = 100
        } else {
            rightAxis.granularity = 50
        }
        rightAxis.labelCount = 40
        
        chartView.legend.form = .none
        
      //  chartView.animate(xAxisDuration: 0.1)
        chartView.chartDescription?.enabled = false
        updateChartData()
        
        let marker = ChartMarker()
        marker.chartView = chartView
        chartView.marker = marker
        updateAppearance()
        removeHighlightForEditableDatasets()
    }
    
    func updateChartData() {
        if (profile == nil) {
            return
        }
        
        //time graph
        timelineSet = LineChartDataSet(values: nil, label: nil)
        timelineSet.editable = false
        timelineSet.drawIconsEnabled = false
        timelineSet.drawCirclesEnabled = false
        timelineSet.lineDashLengths = [5, 0]
        timelineSet.highlightLineDashLengths = [5, 5]
        timelineSet.setColor(kColorExhaust)
        timelineSet.lineWidth = 2
        timelineSet.drawCircleHoleEnabled = false
        timelineSet.drawValuesEnabled = false
        timelineSet.valueTextColor = kColorChartGride
       // timelineSet.valueFont = .systemFont(ofSize: 9)
        timelineSet.visible = false
        timelineSet.setDrawHighlightIndicators(false)
        timelineSet.dataSetType = .timelineSet
        
        // temperature graph
        
        let profileTempValues = (profile!.roastPoints as! [RoastTempPoint]).map { (point) -> ChartDataEntry in
            ChartDataEntry(time: Double(point.time), temperature: Double(point.temperature))
        }
        
        var color = kColorInlet
        if let profile = profile {
            if profile.tempSensor != TempSensorBelow {
                color = kColorExhaust
            }
        }

        
        tempProfileSet = LineChartDataSet(values: profileTempValues, label: nil)
        tempProfileSet.editable = false
        tempProfileSet.drawIconsEnabled = false
        tempProfileSet.lineDashLengths = [5, 0]
        tempProfileSet.highlightLineDashLengths = [5, 5]
        
        tempProfileSet.highlightLineWidth = 2
        tempProfileSet.highlightColor = color
        tempProfileSet.setDrawHighlightIndicators(true)
        
        tempProfileSet.setColor(UIColor.clear)
        tempProfileSet.setCircleColor(color)
        tempProfileSet.lineWidth = 0
        tempProfileSet.circleRadius = 0
        tempProfileSet.drawCircleHoleEnabled = false
        tempProfileSet.circleHoleRadius = 0
        tempProfileSet.circleHoleColor = UIColor.black
        tempProfileSet.drawValuesEnabled = false
        tempProfileSet.valueTextColor = kColorChartGride
        tempProfileSet.fillColor = color
        tempProfileSet.fillAlpha = 0.3
        tempProfileSet.drawFilledEnabled = true
        tempProfileSet.dataSetType = .tempProfileSet
        
        chartView.setVisibleYRangeMinimum(60, axis: tempProfileSet.axisDependency)
        
        let editedProfileTempValues = (editedProfile!.roastPoints as! [RoastTempPoint]).map { (point) -> ChartDataEntry in
            ChartDataEntry(time: Double(point.time), temperature: Double(point.temperature))
        }
        
        if (editedProfileTempValues.count > 1) {
            editedProfileTempValues.first?.isRemovable = false
            editedProfileTempValues.last?.isRemovable = false
        }
        
        if let profile = editedProfile {
            if profile.tempSensor == TempSensorBelow {
                color = kColorInlet
            } else {
                color = kColorExhaust
            }
        }
        
        editedTempProfileSet = LineChartDataSet(values: editedProfileTempValues, label: nil)
        
//        if let firstEntry = editedProfileTempValues.first {
//            editedTempProfileSet.circleIndexesNotToDraw = [editedTempProfileSet.entryIndex(entry: firstEntry)]
//            chartView.notEditablePoints = [firstEntry]
//        }
        
        editedTempProfileSet.editable = false
        editedTempProfileSet.drawIconsEnabled = false
        editedTempProfileSet.lineDashLengths = [5, 0]
        editedTempProfileSet.highlightLineDashLengths = [5, 5]
        
        editedTempProfileSet.highlightLineWidth = 2
        editedTempProfileSet.highlightColor = color
        editedTempProfileSet.setDrawHighlightIndicators(true)
        
        editedTempProfileSet.setColor(color)
        editedTempProfileSet.setCircleColor(color)
        editedTempProfileSet.lineWidth = 3
        editedTempProfileSet.circleRadius = 4
        editedTempProfileSet.drawCircleHoleEnabled = false
        editedTempProfileSet.circleHoleRadius = 3
        editedTempProfileSet.circleHoleColor = UIColor.black
        editedTempProfileSet.drawValuesEnabled = false
        editedTempProfileSet.valueTextColor = kColorChartGride
        editedTempProfileSet.dataSetType = .editedTempProfileSet
        
        // cooling graph
        
        let coolingValues = (profile!.fanPoints as! [RoastFanPoint]).map { (point) -> ChartDataEntry in
            return ChartDataEntry(x: Double(point.time), y: Double(point.power))
        }
        
        fanProfileSet = LineChartDataSet(values: coolingValues, label: nil)
        
        if  let cooldownPoint = profile!.cooldownPoint {
            let lastFanPoint = coolingValues.last
            cooldownEntryOldValue = lastFanPoint?.copy() as? ChartDataEntry
            lastFanPoint?.isRemovable = false
            let dataEntry = ChartDataEntry(x: Double(lastFanPoint!.x + 20), y: Double(cooldownPoint.power))
            _ = fanProfileSet.addEntryOrdered(dataEntry)
            fanProfileSet.circleIndexesNotToDraw = [fanProfileSet.entryIndex(entry: dataEntry)]
            dataEntry.isRemovable = false
            if (chartView.notEditablePoints != nil) {
                chartView.notEditablePoints?.append(dataEntry)
            } else {
                chartView.notEditablePoints = [dataEntry]
            }
            
            _ = fanProfileSet.addEntryOrdered(ChartDataEntry(x: Double(cooldownPoint.time), y: Double(cooldownPoint.power)))
            fanProfileSet.values.first?.isRemovable = false
            fanProfileSet.values.last?.isRemovable = false
        }
        
        
        
        fanProfileSet.editable = false
        fanProfileSet.drawIconsEnabled = false
        
        fanProfileSet.lineDashLengths = [5, 0]
        fanProfileSet.highlightLineDashLengths = [5, 5]
        
        fanProfileSet.highlightLineWidth = 0
        fanProfileSet.highlightColor = kColorFanChart
        fanProfileSet.setDrawHighlightIndicators(true)
        
        fanProfileSet.setColor(kColorFanChart)
        fanProfileSet.setCircleColor(kColorFanChart)
        fanProfileSet.lineWidth = 0
        fanProfileSet.circleRadius = 0
        fanProfileSet.drawCircleHoleEnabled = false
        fanProfileSet.circleHoleRadius = 0
        fanProfileSet.circleHoleColor = UIColor.black
        fanProfileSet.drawValuesEnabled = false
        fanProfileSet.valueTextColor = kColorChartGride
        fanProfileSet.valueFont = .systemFont(ofSize: 9)
        fanProfileSet.fillColor = UIColor.white
        fanProfileSet.fillAlpha = 0.2
        fanProfileSet.drawFilledEnabled = true
        fanProfileSet.dataSetType = .fanProfileSet
        
        let editedCoolingValues = (editedProfile!.fanPoints as! [RoastFanPoint]).map { (point) -> ChartDataEntry in
            return ChartDataEntry(x: Double(point.time), y: Double(point.power))
        }
        
        editedFanProfileSet = LineChartDataSet(values: editedCoolingValues, label: nil)
        
        if  let editedCooldownPoint = editedProfile!.cooldownPoint {
            let editedLastFanPoint = editedCoolingValues.last
            cooldownEntryOldValue = editedLastFanPoint?.copy() as? ChartDataEntry
            editedLastFanPoint?.isRemovable = false
            let dataEntry = ChartDataEntry(x: Double(editedLastFanPoint!.x + 20), y: Double(editedCooldownPoint.power))
            _ = editedFanProfileSet.addEntryOrdered(dataEntry)
            editedFanProfileSet.circleIndexesNotToDraw = [editedFanProfileSet.entryIndex(entry: dataEntry)]
            dataEntry.isRemovable = false
            if (chartView.notEditablePoints != nil) {
                chartView.notEditablePoints?.append(dataEntry)
            } else {
                chartView.notEditablePoints = [dataEntry]
            }
            
            _ = editedFanProfileSet.addEntryOrdered(ChartDataEntry(x: Double(editedCooldownPoint.time), y: Double(editedCooldownPoint.power)))
            editedFanProfileSet.values.first?.isRemovable = false
            editedFanProfileSet.values.last?.isRemovable = false
        }
        
        editedFanProfileSet.editable = false
        editedFanProfileSet.drawIconsEnabled = false
        editedFanProfileSet.lineDashLengths = [5, 0]
        editedFanProfileSet.highlightLineDashLengths = [5, 5]
        
        editedFanProfileSet.highlightLineWidth = 2
        editedFanProfileSet.highlightColor = kColorFanChart
        editedFanProfileSet.setDrawHighlightIndicators(true)
        
        editedFanProfileSet.setColor(kColorFanChart)
        editedFanProfileSet.setCircleColor(kColorFanChart)
        editedFanProfileSet.lineWidth = 3
        editedFanProfileSet.circleRadius = 4
        editedFanProfileSet.drawCircleHoleEnabled = false
        editedFanProfileSet.circleHoleRadius = 3
        editedFanProfileSet.circleHoleColor = UIColor.black
        editedFanProfileSet.drawValuesEnabled = false
        editedFanProfileSet.valueTextColor = kColorChartGride
        editedFanProfileSet.valueFont = .systemFont(ofSize: 9)
        editedFanProfileSet.drawFilledEnabled = false
        editedFanProfileSet.dataSetType = .editedFanProfileSet
        
        // temperature graph above
        
        let tempAboveValues = [ChartDataEntry]()
        
        tempAboveSet = LineChartDataSet(values: tempAboveValues, label: nil)
        tempAboveSet.editable = false
        tempAboveSet.drawIconsEnabled = false
        tempAboveSet.drawCirclesEnabled = false
        tempAboveSet.lineDashLengths = [5, 0]
        tempAboveSet.highlightLineDashLengths = [5, 5]
        tempAboveSet.setDrawHighlightIndicators(false)
        tempAboveSet.setColor(kColorExhaust)
        tempAboveSet.lineWidth = 2
        tempAboveSet.setCircleColor(kColorExhaust)
        tempAboveSet.circleRadius = 4
        tempAboveSet.drawCircleHoleEnabled = false
        tempAboveSet.drawValuesEnabled = false
        tempAboveSet.valueTextColor = kColorChartGride
        tempAboveSet.valueFont = .systemFont(ofSize: 9)
        tempAboveSet.dataSetType = .tempAboveSet
        
        // temperature graph below
        let tempBelowValues = [ChartDataEntry]()
        
        tempBelowSet = LineChartDataSet(values: tempBelowValues, label: nil)
        tempBelowSet.editable = false
        tempBelowSet.drawIconsEnabled = false
        tempBelowSet.drawCirclesEnabled = true
        tempBelowSet.lineDashLengths = [5, 0]
        tempBelowSet.highlightLineDashLengths = [5, 5]
        tempBelowSet.setColor(kColorInlet)
        tempBelowSet.lineWidth = 2
        tempBelowSet.drawCircleHoleEnabled = false
        tempBelowSet.setCircleColor(kColorInlet)
        tempBelowSet.circleRadius = 4
        tempBelowSet.drawValuesEnabled = false
        tempBelowSet.valueTextColor = kColorChartGride
        tempBelowSet.valueFont = .systemFont(ofSize: 9)
        tempBelowSet.setDrawHighlightIndicators(false)
        tempBelowSet.dataSetType = .tempBelowSet
        
        // first crack
        
        firstCrackSet = LineChartDataSet(values: [ChartDataEntry](), label: nil)
        firstCrackSet.editable = false
        firstCrackSet.drawIconsEnabled = false
        firstCrackSet.drawCirclesEnabled = false
        firstCrackSet.lineDashLengths = [5, 5]
        firstCrackSet.highlightLineDashLengths = [5, 5]
        firstCrackSet.setDrawHighlightIndicators(false)
        firstCrackSet.setColor(UIColor.white)
        firstCrackSet.lineWidth = 2
        firstCrackSet.setCircleColor(UIColor.white)
        firstCrackSet.circleRadius = 5
        firstCrackSet.drawCircleHoleEnabled = false
        firstCrackSet.drawValuesEnabled = false
        firstCrackSet.valueTextColor = UIColor.white
        firstCrackSet.valueFont = .systemFont(ofSize: 9)
        firstCrackSet.isMarkerLine = true
        firstCrackSet.dataSetType = .firstCrackSet
        
        // second crack
        
        secondCrackSet = LineChartDataSet(values: [ChartDataEntry](), label: nil)
        secondCrackSet.editable = false
        secondCrackSet.drawIconsEnabled = false
        secondCrackSet.drawCirclesEnabled = false
        secondCrackSet.lineDashLengths = [5, 5]
        secondCrackSet.highlightLineDashLengths = [5, 5]
        secondCrackSet.setDrawHighlightIndicators(false)
        secondCrackSet.setColor(UIColor.white)
        secondCrackSet.lineWidth = 2
        secondCrackSet.setCircleColor(UIColor.white)
        secondCrackSet.circleRadius = 5
        secondCrackSet.drawCircleHoleEnabled = false
        secondCrackSet.drawValuesEnabled = false
        secondCrackSet.valueTextColor = UIColor.white
        secondCrackSet.valueFont = .systemFont(ofSize: 9)
        secondCrackSet.isMarkerLine = true
        secondCrackSet.dataSetType = .secondCrackSet
        // color changed
        
        colorChangeSet = LineChartDataSet(values: [ChartDataEntry](), label: nil)
        colorChangeSet.editable = false
        colorChangeSet.drawIconsEnabled = false
        colorChangeSet.drawCirclesEnabled = false
        colorChangeSet.lineDashLengths = [5, 5]
        colorChangeSet.highlightLineDashLengths = [5, 5]
        colorChangeSet.setDrawHighlightIndicators(false)
        colorChangeSet.setColor(UIColor.white)
        colorChangeSet.lineWidth = 2
        colorChangeSet.setCircleColor(UIColor.white)
        colorChangeSet.circleRadius = 5
        colorChangeSet.drawCircleHoleEnabled = false
        colorChangeSet.drawValuesEnabled = false
        colorChangeSet.valueTextColor = UIColor.white
        colorChangeSet.valueFont = .systemFont(ofSize: 9)
        colorChangeSet.isMarkerLine = true
        colorChangeSet.dataSetType = .colorChangeSet

        // turning point
        
        turningPointSet = LineChartDataSet(values: [ChartDataEntry](), label: nil)
        turningPointSet.editable = false
        turningPointSet.drawIconsEnabled = false
        turningPointSet.drawCirclesEnabled = true
        turningPointSet.lineDashLengths = [5, 0]
        turningPointSet.highlightLineDashLengths = [5, 5]
        turningPointSet.setDrawHighlightIndicators(false)
        turningPointSet.setColor(UIColor.white)
        turningPointSet.lineWidth = 0
        turningPointSet.setCircleColor(UIColor.white)
        turningPointSet.circleRadius = 5
        turningPointSet.drawCircleHoleEnabled = false
        turningPointSet.drawValuesEnabled = false
        turningPointSet.valueTextColor = kColorChartGride
        turningPointSet.valueFont = .systemFont(ofSize: 9)
        turningPointSet.dataSetType = .turningPointSet
        
        lastRoastingPointSet = LineChartDataSet(values: [ChartDataEntry](), label: nil)
        lastRoastingPointSet.editable = false
        lastRoastingPointSet.drawIconsEnabled = false
        lastRoastingPointSet.drawCirclesEnabled = true
        lastRoastingPointSet.lineDashLengths = [5, 0]
        lastRoastingPointSet.highlightLineDashLengths = [5, 5]
        lastRoastingPointSet.setDrawHighlightIndicators(false)
        lastRoastingPointSet.setColor(UIColor.white)
        lastRoastingPointSet.lineWidth = 0
        lastRoastingPointSet.setCircleColor(UIColor.white)
        lastRoastingPointSet.circleRadius = 5
        lastRoastingPointSet.drawCircleHoleEnabled = false
        lastRoastingPointSet.drawValuesEnabled = false
        lastRoastingPointSet.valueTextColor = kColorChartGride
        lastRoastingPointSet.valueFont = .systemFont(ofSize: 9)
        lastRoastingPointSet.dataSetType = .lastRoastingPointSet
        
        // ror
        let rorValues = [ChartDataEntry]()
        
        rorSet = LineChartDataSet(values: rorValues, label: nil)
        rorSet.editable = false
        rorSet.drawIconsEnabled = false
        rorSet.drawCirclesEnabled = false
        rorSet.lineDashLengths = [5, 0]
        rorSet.highlightLineDashLengths = [5, 5]
        rorSet.setDrawHighlightIndicators(false)
        rorSet.setColor(kColorWhite)
        rorSet.lineWidth = 2
        rorSet.setCircleColor(kColorWhite)
        rorSet.circleRadius = 10
        rorSet.drawCircleHoleEnabled = false
        rorSet.drawValuesEnabled = false
        rorSet.valueTextColor = kColorAirflow
        rorSet.valueFont = .systemFont(ofSize: 9)
        rorSet.visible = false
        rorSet.dataSetType = .rorSet
        // power curve
        
        let powerDuringValues = [ChartDataEntry]()
        
        powerHeaterSet = LineChartDataSet(values: powerDuringValues, label: nil)
        powerHeaterSet.editable = false
        powerHeaterSet.drawIconsEnabled = false
        powerHeaterSet.drawCirclesEnabled = false
        powerHeaterSet.lineDashLengths = [5, 0]
        powerHeaterSet.highlightLineDashLengths = [5, 5]
        powerHeaterSet.setDrawHighlightIndicators(false)
        powerHeaterSet.setColor(kColorRiseLine)
        powerHeaterSet.lineWidth = 2
        powerHeaterSet.setCircleColor(kColorRiseLine)
        powerHeaterSet.circleRadius = 10
        powerHeaterSet.drawCircleHoleEnabled = false
        powerHeaterSet.drawValuesEnabled = false
        powerHeaterSet.valueTextColor = kColorRiseLine
        powerHeaterSet.valueFont = .systemFont(ofSize: 9)
        powerHeaterSet.dataSetType = .powerHeaterSet
        
        // profile of rise graph
        
        let profileRORPoints = profile!.roastPointsToRateOfRisePoints()
        
        let tempValues = (AppCore.sharedInstance.roastManager.currentRoast?.roastPoints.count ?? 0)! > 1 ? [] : profileRORPoints!.map { (i) -> ChartDataEntry in
            let point = i
//            let temp = USE_FAHRENHEIT ? (TempToFahrenheit(temp: Double(point.temperature * Float(kRorScale))) + kMaxTemperature * kRorScale) : Double(point.temperature) + (kMaxTemperature / 2 * kRorScale)
            
            let temp = USE_FAHRENHEIT ?
                (Double(point.temperature * 2.0) * Double(kRorScale) + kMaxTemperature)
                :
                Double(point.temperature * Float(kRorScale)) + (kMaxTemperature / 2)
            
            return ChartDataEntry(x: Double(point.time), y: temp)
        }
        
        color = kColorWhite
        rorProfileSet = LineChartDataSet(values: tempValues, label: nil)
        rorProfileSet.editable = false
        rorProfileSet.drawIconsEnabled = false
        rorProfileSet.lineDashLengths = [5, 0]
        rorProfileSet.highlightLineDashLengths = [5, 5]
        rorProfileSet.setColor(color)
        rorProfileSet.setCircleColor(color)
        rorProfileSet.lineWidth = 2
        rorProfileSet.circleRadius = 0
        rorProfileSet.drawCircleHoleEnabled = false
        rorProfileSet.drawValuesEnabled = false
        rorProfileSet.valueTextColor = kColorWhite
        rorProfileSet.valueFont = kAxisFont
        rorProfileSet.dataSetType = .rorProfileSet
//        var gradientColors = [UIColor.clear.cgColor,
//                              color.cgColor]
//        var gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
//        rorProfileSet.fillAlpha = 0.5
//        rorProfileSet.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
//        rorProfileSet.drawFilledEnabled = true
        dataSets = [timelineSet, fanProfileSet, tempProfileSet, editedFanProfileSet, editedTempProfileSet, tempBelowSet, tempAboveSet, turningPointSet, lastRoastingPointSet, rorSet, rorProfileSet, powerHeaterSet, secondCrackSet, colorChangeSet, firstCrackSet]
        let data = LineChartData(dataSets: dataSets)
        
        chartView.data = data
        checkPoints()
    }
    
    func removeHighlightForEditableDatasets() {
        if editedTempProfileSet != nil {
             editedTempProfileSet.selectedEntry = nil
        }
        if editedFanProfileSet != nil {
            editedFanProfileSet.selectedEntry = nil
        }
        
        let highlights = chartView.highlighted.filter { (highlight) -> Bool in
            return highlight.dataSetIndex != dataSets?.firstIndex(of: editedTempProfileSet)! && highlight.dataSetIndex != dataSets?.firstIndex(of: editedFanProfileSet)!
        }
        chartView.highlightValues(highlights)
        chartView.selectedData = nil;
    }

    func removeHighlightForRoastDatasets() {
        if tempAboveSet != nil {
            tempAboveSet.selectedEntry = nil
        }
        if tempBelowSet != nil {
            tempBelowSet.selectedEntry = nil
        }
        
        let highlights = chartView.highlighted.filter { (highlight) -> Bool in
            return highlight.dataSetIndex != dataSets?.firstIndex(of: tempAboveSet)! && highlight.dataSetIndex != dataSets?.firstIndex(of: tempBelowSet)!
        }
        chartView.highlightValues(highlights)
        chartView.selectedData = nil;
    }
    
    func removeHighlightForColarChangeDataset() {
        if colorChangeSet != nil {
            colorChangeSet.selectedEntry = nil
            colorChangeSet.clear()
        }
    }

    func removeHighlightForFirstCrackDataset() {
        if firstCrackSet != nil {
            firstCrackSet.selectedEntry = nil
            firstCrackSet.clear()
        }
    }

    func removeHighlightForSecongCrackDataset() {
        if secondCrackSet != nil {
            secondCrackSet.selectedEntry = nil
            secondCrackSet.clear()
        }
    }
    
    func populateProfileFromChartValues() {
        editedProfile.roastPoints = editedTempProfileSet.values.map{
            let value = RoastTempPoint()
            value.time = Float($0.x)
            value.temperature = Float($0.temperature)
            return value
        }
        
        var editedFanPoints = [RoastFanPoint]()
        for index in 0...editedFanProfileSet.entryCount - 3 {
            let point = RoastFanPoint()
            let chartEntry = editedFanProfileSet.values[index]
            point.time = Float(chartEntry.x)
            point.power = Float(chartEntry.y)
            editedFanPoints.append(point)
        }
        
        editedProfile.fanPoints = editedFanPoints
        let cooldownPoint = RoastFanPoint()
        cooldownPoint.time = Float(editedFanProfileSet.values.last!.x)
        cooldownPoint.power = Float(editedFanProfileSet.values.last!.y)
        editedProfile.cooldownPoint = cooldownPoint
    }
    
    func checkPoints() {
        
        // checking min values
        if (editedTempProfileSet.entryCount <= IK_MIN_ROAST_POINTS) {
            _ = editedTempProfileSet.values.map{$0.isRemovable = false}
        } else {
            _ = editedTempProfileSet.values.map{$0.isRemovable = true}
            editedTempProfileSet.values.first?.isRemovable = false
            editedTempProfileSet.values.last?.isRemovable = false
        }
        
        if ((editedFanProfileSet.entryCount - 2) <= IK_MIN_FAN_POINTS) {
            _ = editedFanProfileSet.values.map{$0.isRemovable = false}
        } else {
            _ = editedFanProfileSet.values.map{$0.isRemovable = true}
            editedFanProfileSet.values.first?.isRemovable = false
            editedFanProfileSet.values.last?.isRemovable = false
            editedFanProfileSet.values[editedFanProfileSet.entryCount - 2].isRemovable = false
            editedFanProfileSet.values[editedFanProfileSet.entryCount - 3].isRemovable = false
        }
        
    }
    
    @objc private func clearCurrentRoast() {
        tempAboveSet.clear()
        tempBelowSet.clear()
        rorSet.clear()
        lastRoastingPointSet.clear()
        turningPointSet.clear()
        colorChangeSet.clear()
        secondCrackSet.clear()
        firstCrackSet.clear()
        chartView.data?.notifyDataChanged()
        chartView.notifyDataSetChanged()
    }
}



extension IKTempChartView: ChartViewProtocol {
    
    func selectedRoastPoint() -> RoastTempPoint? {
        if let selectedData = self.chartView.selectedData {
            if let index = editedTempProfileSet.values.index(of: selectedData.entry) {
                return editedProfile.roastPoints[index] as? RoastTempPoint
            }
        }
        return nil
    }

    func selectedFanPoint() -> RoastFanPoint? {
        if let selectedData = self.chartView.selectedData {
            if let index = editedFanProfileSet.values.index(of: selectedData.entry), index < editedProfile.fanPoints.count {
                return editedProfile.fanPoints[index] as? RoastFanPoint
            } else {
                return nil
            }
        }
        return nil
    }
    
    func selectedCooldownPint() -> Bool {
        if let selectedData = self.chartView.selectedData {
            if let index = editedFanProfileSet.values.index(of: selectedData.entry) {
                return (editedProfile.fanPoints.count + 1) == editedFanProfileSet.values.index(of: selectedData.entry)
            }
        }
        return false
    }
    
    func enableMarkersEditing(enable: Bool) {
        firstCrackSet.editable = enable
        secondCrackSet.editable = enable
        colorChangeSet.editable = enable
        if !enable && (chartView.selectedData?.set == firstCrackSet || chartView.selectedData?.set == secondCrackSet || chartView.selectedData?.set == colorChangeSet) {
            chartView.selectedData = nil
        }
    }
    
    func setRoast(roast: IKRoast?) {
    
    }
    
    func updateAppearance() {
        if (isEditState) {
            goToEditState()
        } else {
            goToViewState()
        }
        
    }
    
    func goToEditState() {
        isEditState = true
        editedTempProfileSet.drawCircleHoleEnabled = false
        editedTempProfileSet.circleRadius = 5
        editedTempProfileSet.circleHoleRadius = 5
        editedTempProfileSet.editable = true
        
        editedFanProfileSet.drawCircleHoleEnabled = false
        editedFanProfileSet.circleRadius = 5
        editedFanProfileSet.circleHoleRadius = 5
        editedFanProfileSet.editable = true
        
        tempAboveSet.setColor(kColorLightGrey1, alpha: 0.5)
        tempBelowSet.setColor(kColorLightGrey1, alpha: 0.5)
        firstCrackSet.setColor(kColorLightGrey1, alpha: 0.5)
        secondCrackSet.setColor(kColorLightGrey1, alpha: 0.5)
        colorChangeSet.setColor(kColorLightGrey1, alpha: 0.5)
        rorSet.setColor(kColorLightGrey1, alpha: 0.5)
        if isRORHidden {
            hideROR()
        } else {
            showROR()
        }
        
        //chartView.xAxis.axisMaximum =  Double(kTimeAxisMax + 5 * 60)
        //chartView.setVisibleXRangeMaximum(Double(kTimeAxisMin))
        if let marker = chartView.marker as? ChartMarker {
            marker.isEditState = true
        }
    }
    
    func goToViewState() {
        graphWasEdited = false
        isEditState = false
        editedTempProfileSet.drawCircleHoleEnabled = false
        editedTempProfileSet.circleRadius = 0
        editedTempProfileSet.editable = false
        
        
        editedFanProfileSet.drawCircleHoleEnabled = false
        editedFanProfileSet.circleRadius = 0
        editedFanProfileSet.editable = false
        tempAboveSet.setColor(kColorExhaust, alpha: 1)
        tempBelowSet.setColor(kColorInlet, alpha: 1)
        firstCrackSet.setColor(UIColor.white, alpha: 1)
        firstCrackSet.setCircleColor(UIColor.white.withAlphaComponent(1))
        secondCrackSet.setColor(UIColor.white, alpha: 1)
        secondCrackSet.setCircleColor(UIColor.white.withAlphaComponent(1))
        turningPointSet.setColor(UIColor.white, alpha: 1)
        turningPointSet.setCircleColor(UIColor.white.withAlphaComponent(1))
        lastRoastingPointSet.setColor(UIColor.white, alpha: 1)
        lastRoastingPointSet.setCircleColor(UIColor.white.withAlphaComponent(1))
        colorChangeSet.setColor(UIColor.white, alpha: 1)
        colorChangeSet.setCircleColor(UIColor.white.withAlphaComponent(1))
        rorSet.setColor(UIColor.white, alpha: 1)
        let isLandscape = UIDevice.current.orientation.isFlat ? UIApplication.shared.statusBarOrientation.isLandscape : UIDevice.current.orientation.isLandscape
        if isLandscape {
            chartView.setVisibleXRangeMaximum(Double(25 * 60))
        } else {
            chartView.setVisibleXRangeMaximum(Double(kTimeAxisMin))
        }
        //chartView.xAxis.axisMaximum =  Double(kTimeAxisMax + 5 * 60)
        //chartView.setVisibleXRangeMaximum(Double(kTimeAxisMin))
        
        if isRORHidden {
            hideROR()
        } else {
            showROR()
        }
        
        if let marker = chartView.marker as? ChartMarker {
            marker.isEditState = false
        }
    }
    
    
    func showROR() {
        updateRorAppearance()
        chartView.leftAxis2.enabled = true
//        chartView.leftAxis2.granularity = 50
//        chartView.leftAxis2.labelCount = 10
//        chartView.leftAxis2.valueFormatter = RORTemperatureFormatter()
//        fanProfileSet.visible = false
//        tempProfileSet.visible = false
//        editedFanProfileSet.visible = false
//        editedTempProfileSet.visible = false
        
  
//        tempAboveSet.setColor(kColorExhaust, alpha: 0.4)
//        tempBelowSet.setColor(kColorInlet, alpha: 0.4)
//        firstCrackSet.setColor(UIColor.white, alpha: 0.4)
//        firstCrackSet.setCircleColor(UIColor.white.withAlphaComponent(0.4))
//        secondCrackSet.setColor(UIColor.white, alpha: 0.4)
//        secondCrackSet.setCircleColor(UIColor.white.withAlphaComponent(0.4))
//        turningPointSet.setColor(UIColor.white, alpha: 0.4)
//        turningPointSet.setCircleColor(UIColor.white.withAlphaComponent(0.4))
//        lastRoastingPointSet.setColor(UIColor.white, alpha: 0.4)
//        lastRoastingPointSet.setCircleColor(UIColor.white.withAlphaComponent(0.4))
//        colorChangeSet.setColor(UIColor.white, alpha: 0.4)
//        colorChangeSet.setCircleColor(UIColor.white.withAlphaComponent(0.4))
        chartView.notifyDataSetChanged()
        chartView.data?.notifyDataChanged()
    }

    func hideROR() {
        chartView.leftAxis2.enabled = false
//        if profile.isRoasterProfile(), IKRoastManager.sharedInstance.isRoasting() { //roasting current profile
//            fanProfileSet.visible = false
//            tempProfileSet.visible = false
//            editedFanProfileSet.visible = false
//            editedTempProfileSet.visible = false
//        } else {
//            fanProfileSet.visible = true
//            tempProfileSet.visible = true
//            editedFanProfileSet.visible = true
//            editedTempProfileSet.visible = true
//        }
        updateRorAppearance()
//        chartView.leftAxis.granularityEnabled = true
//        if (USE_FAHRENHEIT) {
//            chartView.leftAxis.granularity = 100
//        } else {
//            chartView.leftAxis.granularity = 50
//        }
//        chartView.leftAxis.labelCount = 40
//        chartView.leftAxis.valueFormatter = TemperatureFormatter()
        tempProfileSet.visible = true
        fanProfileSet.visible = true
        
//        tempAboveSet.setColor(kColorExhaust, alpha: 1)
//        tempBelowSet.setColor(kColorInlet, alpha: 1)
//        firstCrackSet.setColor(UIColor.white, alpha: 1)
//        firstCrackSet.setCircleColor(UIColor.white.withAlphaComponent(1))
//        secondCrackSet.setColor(UIColor.white, alpha: 1)
//        secondCrackSet.setCircleColor(UIColor.white.withAlphaComponent(1))
//        turningPointSet.setColor(UIColor.white, alpha: 1)
//        turningPointSet.setCircleColor(UIColor.white.withAlphaComponent(1))
//        lastRoastingPointSet.setColor(UIColor.white, alpha: 1)
//        lastRoastingPointSet.setCircleColor(UIColor.white.withAlphaComponent(1))
//        colorChangeSet.setColor(UIColor.white, alpha: 1)
//        colorChangeSet.setCircleColor(UIColor.white.withAlphaComponent(1))
        chartView.notifyDataSetChanged()
        chartView.data?.notifyDataChanged()
    }
    
    func viewWillTransition() {
        updateAppearance()
    }
    
    func updateRorAppearance() {
        if !isRORHidden {
            if roast != nil { //roasting current profile
                rorSet.visible = true
                rorProfileSet.visible = false
            } else {
                rorSet.visible = false
                rorProfileSet.visible = true
            }
        } else {
            rorSet.visible = false
            rorProfileSet.visible = false
        }
    }
    
    func updateTempPoint(roast: IKRoast?) {
        self.roast = roast
        updateRorAppearance()
        if let roaster = RoasterManager.instance().roaster, let lastPoint = roast?.roastPoints.last, profile.isRoasterProfile(), AppCore.sharedInstance.roastManager.isRoasting()  {
            let state = roaster.status.state
            //        guard RoasterManager.instance().roaster != nil && profile.isRoasterProfile() && roast != nil && (state == IK_STATUS_HEATING || state == IK_STATUS_ROASTING || state == IK_STATUS_COOLING) else {
            //            return
            //        }
            
            
            if (!chartView.userWasInteract && (state == IK_STATUS_HEATING || state == IK_STATUS_ROASTING || state == IK_STATUS_COOLING) && AppCore.sharedInstance.roastManager.roastAndCoolDownProgress() > 0 && lastPoint.time > Float(kTimeAxisMin / 2)) {
                let delta = Double(lastPoint.time - Float(kTimeAxisMin / 2))
                chartView.moveViewToAnimated(xValue: delta, yValue: 0, axis: self.chartView.leftAxis.axisDependency, duration: 0.5)
            }
        }
        
        editedTempProfileSet.visible = isEditState
        editedFanProfileSet.visible = isEditState
        
        guard let roast = roast else {
            return
        }
        
        
        
        if (roast.roastPoints.last?.time == 0) {
            let lastPoint = roast.roastPoints.last
            if (lastPoint?.time == 0) {
                if (lastPoint!.temp_above > 0) {
                    tempAboveSet.clear()
                    _ = tempAboveSet.addEntryOrdered(ChartDataEntry(time: Double(lastPoint!.time), temperature: Double(lastPoint!.temp_above)))
                    tempAboveSet.drawCirclesEnabled = true
                }
                
                if (lastPoint!.temp_below > 0) {
                    tempBelowSet.clear()
                    _ = tempBelowSet.addEntryOrdered(ChartDataEntry(time: Double(lastPoint!.time), temperature: Double(lastPoint!.temp_below)))
                    tempBelowSet.drawCirclesEnabled = true
                }
                
//                if (lastPoint!.heater > Int32(0)) {
//                    powerHeaterSet.clear()
//                    _ = powerHeaterSet.addEntryOrdered(ChartDataEntry(time: Double(lastPoint!.time), temperature: Double(lastPoint!.heater)))
//                    powerHeaterSet.drawCirclesEnabled = false
//                }
            }
        } else {
            if (tempAboveSet.entryCount < roast.roastPointsWithoutPreheating().count) {
                for index in tempAboveSet.entryCount...(roast.roastPointsWithoutPreheating().count - 1) {
                    let point = roast.roastPointsWithoutPreheating()[index]
                    if isCoolingHidden && point.state == IK_STATUS_COOLING {
                        continue
                    }
                    if (point.temp_above > 0) {
                        if tempAboveSet.entryCount > 0 && point.state == IK_STATUS_HEATING {
                            tempAboveSet.removeLast()
                            tempAboveSet.drawCirclesEnabled = true
                        } else {
                           tempAboveSet.drawCirclesEnabled = false
                        }
                        
                        _ = tempAboveSet.addEntryOrdered(ChartDataEntry(time: Double(point.time), temperature: Double(point.temp_above)))
                    }
                }
                
            }
            
            if (tempBelowSet.entryCount < roast.roastPointsWithoutPreheating().count) {
                for index in tempBelowSet.entryCount...(roast.roastPointsWithoutPreheating().count - 1) {
                    let point = roast.roastPointsWithoutPreheating()[index]
                    if isCoolingHidden && point.state == IK_STATUS_COOLING {
                        continue
                    }
                    if (point.temp_below > 0) {
                        if tempBelowSet.entryCount > 0 && point.state == IK_STATUS_HEATING {
                            tempBelowSet.removeLast()
                            tempBelowSet.drawCirclesEnabled = true
                        } else {
                            tempBelowSet.drawCirclesEnabled = false
                        }
                        _ = tempBelowSet.addEntryOrdered(ChartDataEntry(time: Double(point.time), temperature: Double(point.temp_below)))
                    }
                }
                
            }
            
//            if (powerHeaterSet.entryCount < roast.roastPoints.count) {
//                for index in powerHeaterSet.entryCount...(roast.roastPoints.count - 1) {
//                    let point = roast.roastPoints[index]
//                    powerHeaterSet.drawCirclesEnabled = false
//                    _ = powerHeaterSet.addEntryOrdered(ChartDataEntry(time: Double(point.time), temperature: Double(point.heater)))
//                }
//            }
        }
        
        if (rorSet.entryCount < roast.roastPointsWithoutPreheating().count) {
            for index in rorSet.entryCount...(roast.roastPointsWithoutPreheating().count - 1) {
                let point = roast.roastPointsWithoutPreheating()[index]
                if point.state == IK_STATUS_COOLING {
                    break
                }
                //if (point.ror_above > 0) {
                rorSet.drawCirclesEnabled = false
                let temp = USE_FAHRENHEIT ?
                    Double(point.ror_above * Float(2.0) * Float(kMaxTemperatureFahrenheit / kMaxTemperature)) + (kMaxTemperatureFahrenheit / 2.0)
                    :
                    Double(point.ror_above * Float(kRorScale)) + (kMaxTemperature / 2.0)
                _ = rorSet.addEntryOrdered(ChartDataEntry(x: Double(point.time), y: temp))
                
                //}
            }
        }
        
        if let firstCrackPoint = roast.firstCrack {
            if (firstCrackPoint.x > 0 && firstCrackSet.entryCount == 0) {
                firstCrackSet.drawIconsEnabled = true
                firstCrackSet.clear()
                let data = tempAboveSet.entryForXValue(Double(firstCrackPoint.x) , closestToY: 0)
                _ = firstCrackSet.addEntryOrdered(ChartDataEntry(x: Double(firstCrackPoint.x), y: Double(0)))
                _ = firstCrackSet.addEntryOrdered(ChartDataEntry(x: Double(firstCrackPoint.x), y: Double(chartView.leftAxis.axisMaximum - 1), data: data))
            }
        }
        
        if let secondCrackPoint = roast.secondCrack {
            if (secondCrackPoint.x > 0 && secondCrackSet.entryCount == 0) {
                secondCrackSet.clear()
                let data = tempAboveSet.entryForXValue(Double(secondCrackPoint.x), closestToY: 0)
                _ = secondCrackSet.addEntryOrdered(ChartDataEntry(x: Double(secondCrackPoint.x), y: Double(0), data:nil))
                _ = secondCrackSet.addEntryOrdered(ChartDataEntry(x: Double(secondCrackPoint.x), y: Double(chartView.leftAxis.axisMaximum - 1), data: data))
//                _ = secondCrackSet.addEntryOrdered(ChartDataEntry(time: Double(secondCrackPoint.x), temperature: Double(secondCrackPoint.y)))
            }
        }
        
        if let colorChangePoint = roast.colorChange {
            if (colorChangePoint.x > 0 && colorChangeSet.entryCount == 0) {
                colorChangeSet.clear()
                let data = tempAboveSet.entryForXValue(Double(colorChangePoint.x), closestToY: 0)
                _ = colorChangeSet.addEntryOrdered(ChartDataEntry(x: Double(colorChangePoint.x), y: Double(0), data:data))
                _ = colorChangeSet.addEntryOrdered(ChartDataEntry(x: Double(colorChangePoint.x), y: Double(chartView.leftAxis.axisMaximum - 1), data: data ))
                //_ = colorChangeSet.addEntryOrdered(ChartDataEntry(time: Double(colorChangePoint.x), temperature: Double(colorChangePoint.y)))
            }
            
        }
        
//        if let turningPoint = roast.turningPoint() {
//            if (turningPoint.x > 0) {
//                turningPointSet.clear()
//                _ = turningPointSet.addEntryOrdered(ChartDataEntry(time: Double(turningPoint.x), temperature: Double(turningPoint.y)))
//            }
//
//        }

//        if let lastRoastingPoint = roast.endRoastingPoint() {
//            if (lastRoastingPoint.time > 0) {
//                lastRoastingPointSet.clear()
//                _ = lastRoastingPointSet.addEntryOrdered(ChartDataEntry(time: Double(lastRoastingPoint.time), temperature: Double(lastRoastingPoint.temperature)))
//            }
//
//        }
        
        chartView.data?.notifyDataChanged()
        chartView.notifyDataSetChanged()
        var restHighlights = chartView.highlighted.filter { (highlight) -> Bool in
            return  highlight.dataSetIndex != dataSets?.firstIndex(of: powerHeaterSet) && highlight.dataSetIndex != dataSets?.firstIndex(of: tempAboveSet) && highlight.dataSetIndex != dataSets?.firstIndex(of: tempBelowSet) &&
                highlight.dataSetIndex != dataSets?.firstIndex(of: rorSet) && highlight.dataSetIndex != dataSets?.firstIndex(of: timelineSet) && highlight.dataSetIndex != dataSets?.firstIndex(of: firstCrackSet) && highlight.dataSetIndex != dataSets?.firstIndex(of: secondCrackSet) && highlight.dataSetIndex != dataSets?.firstIndex(of: colorChangeSet) && highlight.dataSetIndex != dataSets?.firstIndex(of: turningPointSet) && highlight.dataSetIndex != dataSets?.firstIndex(of: lastRoastingPointSet) //temp above, temp below, time, 1 crack, 2 crack, color change, turning point, last roasting point point
        }
        var highlights = [Highlight]()
        if let lastAboveEntry = tempAboveSet.entryForIndex(tempAboveSet.entryCount - 1) {
            timelineSet.clear()
            let dataEntry = ChartDataEntry(x: Double(lastAboveEntry.x), y: 0)
            _ = timelineSet.addEntryOrdered(dataEntry)
            let timePoint = Highlight(x: lastAboveEntry.x, y: 0, dataSetIndex: dataSets!.firstIndex(of: timelineSet)!)
            highlights.append(timePoint)
            let tempPointAbove = Highlight(x: lastAboveEntry.x, y: lastAboveEntry.y , dataSetIndex: dataSets!.firstIndex(of: tempAboveSet)!)
            highlights.append(tempPointAbove)
        }
        
        if let lastBelowEntry = tempBelowSet.entryForIndex(tempBelowSet.entryCount - 1) {
            let tempPointBelow = Highlight(x: lastBelowEntry.x, y: lastBelowEntry.y , dataSetIndex: dataSets!.firstIndex(of: tempBelowSet)!)
            highlights.append(tempPointBelow)
        }

        if let lastROREntry = rorSet.entryForIndex(rorSet.entryCount - 1) {
            let tempPointROR = Highlight(x: lastROREntry.x, y: lastROREntry.y , dataSetIndex: dataSets!.firstIndex(of: rorSet)!)
            highlights.append(tempPointROR)
        }
        
        let isMarkerActive = (chartView.selectedData?.set == firstCrackSet || chartView.selectedData?.set == secondCrackSet || chartView.selectedData?.set == colorChangeSet)
        
        if let firstCrackEntry = firstCrackSet.values.last {
            let firstCrackPoint = Highlight(x: firstCrackEntry.x, y: firstCrackEntry.y , dataSetIndex: dataSets!.firstIndex(of: firstCrackSet)!)
            if chartView.selectedData?.set == firstCrackSet || !isMarkerActive {
                highlights.append(firstCrackPoint)
            } else {
                highlights.insert(firstCrackPoint, at: 0)
            }
        }
        
        
        if let secondCrackEntry = secondCrackSet.values.last {
            let secondCrackPoint = Highlight(x: secondCrackEntry.x, y: secondCrackEntry.y , dataSetIndex: dataSets!.firstIndex(of: secondCrackSet)!)
            if chartView.selectedData?.set == secondCrackSet || !isMarkerActive {
                highlights.append(secondCrackPoint)
            } else {
                highlights.insert(secondCrackPoint, at: 0)
            }
        }
        
        if let colorChangeEntry = colorChangeSet.values.last {
            let colorChangePoint = Highlight(x: colorChangeEntry.x, y: colorChangeEntry.y , dataSetIndex: dataSets!.firstIndex(of: colorChangeSet)!)
            if chartView.selectedData?.set == colorChangeSet || !isMarkerActive {
                highlights.append(colorChangePoint)
            } else {
                highlights.insert(colorChangePoint, at: 0)
            }
        }
        
        if let turningPointEntry = turningPointSet.values.last {
            let turningPoint = Highlight(x: turningPointEntry.x, y: turningPointEntry.y , dataSetIndex: dataSets!.firstIndex(of: turningPointSet)!)
            highlights.append(turningPoint)
        }

        if let lastRoastingPointEntry = lastRoastingPointSet.values.last {
            let lastRoastingPoint = Highlight(x: lastRoastingPointEntry.x, y: lastRoastingPointEntry.y , dataSetIndex: dataSets!.firstIndex(of: lastRoastingPointSet)!)
            highlights.append(lastRoastingPoint)
        }
        
//        if let lastPowerHeaterEntry = powerHeaterSet.values.last {
//            let powerHeater = Highlight(x: lastPowerHeaterEntry.x, y: lastPowerHeaterEntry.y , dataSetIndex: dataSets!.firstIndex(of: powerHeaterSet)!)
//            highlights.append(powerHeater)
//        }
        
        
        chartView.highlightValues(highlights + restHighlights)
        
    }
    
    func zoom12() {
        let leftAxis = self.chartView.leftAxis
        leftAxis.granularity = 50
        chartView.setVisibleXRangeMaximum(Double(kTimeAxisMin))
        chartView.zoomAndCenterViewAnimated(scaleX: 0, scaleY: 0, xValue: 0, yValue: 0, axis: self.chartView.leftAxis.axisDependency, duration: 0.7)
        //chartView.moveViewToAnimated(xValue: 0, yValue: 0, axis: self.chartView.leftAxis.axisDependency, duration: 0.7)
    }

    func zoom24() {
        let leftAxis = self.chartView.leftAxis
        leftAxis.granularity = 50
        chartView.setVisibleXRangeMaximum(Double(25 * 60))
        chartView.zoomAndCenterViewAnimated(scaleX: 0, scaleY: 0, xValue: 0, yValue: 0, axis: self.chartView.leftAxis.axisDependency, duration: 0.7)
        //chartView.moveViewToAnimated(xValue: 0, yValue: 0, axis: self.chartView.leftAxis.axisDependency, duration: 0.7)
        
    }
    
    func zoomFit() {
        if let lastValue = fanProfileSet.values.last, let editedLastValue = editedFanProfileSet.values.last {
            let leftAxis = self.chartView.leftAxis
            leftAxis.granularity = 50
            chartView.setVisibleXRangeMaximum(max(lastValue.x + 30, editedLastValue.x + 30))
            chartView.zoomAndCenterViewAnimated(scaleX: 0, scaleY: 0, xValue: 0, yValue: 0, axis: self.chartView.leftAxis.axisDependency, duration: 0.7)
            //chartView.moveViewToAnimated(xValue: 0, yValue: 0, axis: self.chartView.leftAxis.axisDependency, duration: 0.7)
           
        }
    }
}

extension IKTempChartView: InteractiveChartViewDelegate {


    
    func chartValueMoved(_ chartView: ChartViewBase, entry: ChartDataEntry, touchFinished: Bool) {
        
        if (editedTempProfileSet.contains(entry)) {
            editedTempProfileSet.drawVerticalHighlightIndicatorEnabled = editedTempProfileSet.entryIndex(entry: entry) != 0
        } else if (editedFanProfileSet.contains(entry)) {
            editedFanProfileSet.drawVerticalHighlightIndicatorEnabled = editedFanProfileSet.entryIndex(entry: entry) != 0
        }
 
        if (editedFanProfileSet.contains(entry)) {
            editedFanProfileSet.selectedEntry = entry
            editedTempProfileSet.selectedEntry = nil
            correctFanGraph(movedEntry: entry,touchFinished: touchFinished)
            correctTempGraph(entry: editedTempProfileSet.values.last!, touchFinished: touchFinished)
        } else if (editedTempProfileSet.contains(entry)) {
            editedFanProfileSet.selectedEntry = nil
            editedTempProfileSet.selectedEntry = entry
            correctTempGraph(entry: entry, touchFinished: touchFinished)
            correctFanGraph(movedEntry: editedFanProfileSet.values[editedFanProfileSet.values.count - 3], touchFinished: touchFinished)
        }
        
        if let currentRoast = AppCore.sharedInstance.roastManager.currentRoast {
            
            let roastTime = currentRoast.roastPointsWithoutPreheating().last?.time ?? 0
            if firstCrackSet.contains(entry) {
                let secondCrackEntry = secondCrackSet.values.last
               // let colorChangeEntry = colorChangeSet.values.last
                if Float(entry.x) <= roastTime &&
                    Float(entry.x) > 10  && (secondCrackEntry == nil || (secondCrackEntry != nil && entry.x + 1 <= secondCrackEntry!.x))
                /*&& (colorChangeEntry == nil || (colorChangeEntry != nil && entry.x - 1 >= colorChangeEntry!.x))*/{
                    firstCrackSet.values.first?.x = firstCrackSet.values.last?.x ?? 0
                    AppCore.sharedInstance.roastManager.currentRoast?.firstCrack?.x = CGFloat(firstCrackSet.values.first?.x ?? 0)
                } else {
                    firstCrackSet.values.last?.x = Double(AppCore.sharedInstance.roastManager.currentRoast?.firstCrack?.x ?? 0)
                }
                firstCrackSet.values.last?.y = Double(self.chartView.leftAxis.axisMaximum - 1)
                firstCrackSet.values.last?.data = tempAboveSet.entryForXValue(firstCrackSet.values.last?.x ?? 0, closestToY: 0)
            } else if secondCrackSet.contains(entry) {
                let firstCrackEntry = firstCrackSet.values.last
                if Float(entry.x) <= roastTime &&
                    Float(entry.x) > 10 && (firstCrackEntry == nil || (firstCrackEntry != nil && entry.x - 1 >= firstCrackEntry!.x)){
                    secondCrackSet.values.first?.x = secondCrackSet.values.last?.x ?? 0
                    AppCore.sharedInstance.roastManager.currentRoast?.secondCrack?.x = CGFloat(secondCrackSet.values.first?.x ?? 0)
                } else {
                    secondCrackSet.values.last?.x = Double(AppCore.sharedInstance.roastManager.currentRoast?.secondCrack?.x ?? 0)
                }
                secondCrackSet.values.last?.y = Double(self.chartView.leftAxis.axisMaximum - 1)
                secondCrackSet.values.last?.data = tempAboveSet.entryForXValue(secondCrackSet.values.last?.x ?? 0, closestToY: 0)
            } else if colorChangeSet.contains(entry) {
                //let firstCrackEntry = firstCrackSet.values.last
                if Float(entry.x) <= roastTime &&
                    Float(entry.x) > 10/* && (firstCrackEntry == nil || (firstCrackEntry != nil && entry.x + 1 <= firstCrackEntry!.x))*/ {
                    colorChangeSet.values.first?.x = colorChangeSet.values.last?.x ?? 0
                    AppCore.sharedInstance.roastManager.currentRoast?.colorChange?.x = CGFloat(colorChangeSet.values.first?.x ?? 0)
                } else {
                    colorChangeSet.values.last?.x = Double(AppCore.sharedInstance.roastManager.currentRoast?.colorChange?.x ?? 0)
                }
                colorChangeSet.values.last?.y = Double(self.chartView.leftAxis.axisMaximum - 1)
                colorChangeSet.values.last?.data = tempAboveSet.entryForXValue(colorChangeSet.values.last?.x ?? 0, closestToY: 0)
            }
 
        }

        cooldownEntryOldValue = editedTempProfileSet.values.last!.copy() as? ChartDataEntry

        populateProfileFromChartValues()
        
        chartView.data?.notifyDataChanged()
        chartView.notifyDataSetChanged()
        graphWasEdited = true
        if self.profile.isRoasterProfile() {
            updateTempPoint(roast: AppCore.sharedInstance.roastManager.currentRoast)
        }
    }
    
    
    
    
    func correctTempGraph(entry: ChartDataEntry, touchFinished: Bool) {
        
        //checking temp chart rules
        
        //first point
        editedTempProfileSet.values.first?.x = 0
        
        //check value ranges
        
        var counter = 0

        for item in editedTempProfileSet.values {
            
            let selectedItemIndex = editedTempProfileSet.values.index(of: entry)!
            let itemIndex = editedTempProfileSet.values.index(of: item)!
            
            if (item.x >= editedTempProfileSet.values.last!.x) {
                item.x = editedTempProfileSet.values.last!.x
            }
            
            if (item.x > Double(kTimeAxisMax)) {
                item.x = Double(kTimeAxisMax)
            }
            
            if (itemIndex < selectedItemIndex) {
                if (selectedItemIndex == editedTempProfileSet.values.count - 1) {
                    if (item.x > entry.x) {
                        item.x = entry.x
                    }
                } else if (selectedItemIndex == 1) {
                    if (item.x > entry.x) {
                        item.x = entry.x
                    }
                } else {
                    if (item.x > entry.x) {
                        editedTempProfileSet.values[selectedItemIndex] = item
                        editedTempProfileSet.values[itemIndex] = entry
                        removeHighlightForEditableDatasets()
                        self.chartView.setSelectedDataEntry(setIndex: dataSets!.firstIndex(of: editedTempProfileSet)!, dataSet: editedTempProfileSet, index: itemIndex, entry: item)
                    }
                }
                
            } else {
                if (selectedItemIndex == editedTempProfileSet.values.count - 2) {
                    if (item.x < entry.x) {
                        item.x = entry.x
                    }
                } else {
                    if (item.x < entry.x) {
                        editedTempProfileSet.values[selectedItemIndex] = item
                        editedTempProfileSet.values[itemIndex] = entry
                        chartView.notifyDataSetChanged()
                        removeHighlightForEditableDatasets()
                        self.chartView.setSelectedDataEntry(setIndex: dataSets!.firstIndex(of: editedTempProfileSet)!, dataSet: editedTempProfileSet, index: itemIndex, entry: item)
                    }
                }
                
            }
            if touchFinished {
                if  counter == 1 {
                    let previousPoint = editedTempProfileSet.values[counter - 1]
                    item.x = item.x.rounded()
                    previousPoint.x = previousPoint.x.rounded()
                    if item.x <=  previousPoint.x {
                        if previousPoint.x == 0 {
                            item.x = previousPoint.x + 1
                        }
                    }
                } else if counter > 1  {
                    let previousPoint = editedTempProfileSet.values[counter - 1]
                    item.x = item.x.rounded()
                    previousPoint.x = previousPoint.x.rounded()
                    if item.x <=  previousPoint.x {
                       item.x = previousPoint.x + 1
                    }
                }
            }
            counter = counter + 1
            
        }
        let useSensorBelow = editedProfile.tempSensor == TempSensorBelow
        for dataEntry in editedTempProfileSet.values {
            dataEntry.temperature = max(kMinTemperature, Double(dataEntry.temperature))
            dataEntry.temperature = min(Double(useSensorBelow ? Float(IK_MAX_TEMP_BELOW) : Float(IK_MAX_TEMP_ABOVE)), Double(dataEntry.temperature))
            dataEntry.x = max(0, dataEntry.x)
            dataEntry.x = min(Double(kTimeAxisMax), dataEntry.x)
        }
        editedFanProfileSet.values[editedFanProfileSet.values.count - 3].x =  editedTempProfileSet.values.last!.x
        
    }
    
    func correctFanGraph(movedEntry: ChartDataEntry, touchFinished: Bool) {
        let isEntrySelected = chartView.selectedData?.entry == movedEntry
        let cooldownEntry = editedFanProfileSet.values[editedFanProfileSet.values.count - 3]
        //checking fan chart rules
        
        // first point
        editedFanProfileSet.values.first?.x = 0
        var counter = 0
        //check value ranges
        let movedItemIndex = editedFanProfileSet.values.index(of: movedEntry)!
        if (movedItemIndex < (editedFanProfileSet.entryCount - 1)) {
            if (movedEntry.x >  Double(kTimeAxisMax)) {
                movedEntry.x = Double(kTimeAxisMax)
            }
        }
        
        if (Int(editedFanProfileSet.values.last!.x - cooldownEntry.x) > IK_MAX_COOLDOWN_TIME) {
            editedFanProfileSet.values.last!.x = cooldownEntry.x + Double(IK_MAX_COOLDOWN_TIME)
        }

        for item in editedFanProfileSet.values {
            let movedItemIndex = editedFanProfileSet.values.index(of: movedEntry)!
            let itemIndex = editedFanProfileSet.values.index(of: item)!

            if (movedItemIndex == (editedFanProfileSet.entryCount - 1)) {
                if ((editedFanProfileSet.values[editedFanProfileSet.values.count - 1].x - editedFanProfileSet.values[editedFanProfileSet.values.count - 2].x) < (60 - 20)) {
                    movedEntry.x = editedFanProfileSet.values[editedFanProfileSet.values.count - 2].x + (60 - 20)
                }
            }
            

            
            editedFanProfileSet.values[editedFanProfileSet.values.count - 2].x =  editedFanProfileSet.values[editedFanProfileSet.values.count - 3].x + 20
            editedFanProfileSet.values[editedFanProfileSet.values.count - 2].y =  editedFanProfileSet.values[editedFanProfileSet.values.count - 1].y

            editedTempProfileSet.values.last!.x =  editedFanProfileSet.values[editedFanProfileSet.values.count - 3].x
            
            if (itemIndex < movedItemIndex) {
                
                
                
                if (item.x > movedEntry.x) {
                    print("itemIndex < selectedItemIndex")
                    if isEntrySelected {
                        editedFanProfileSet.values[movedItemIndex] = item
                        editedFanProfileSet.values[itemIndex] = movedEntry
                        chartView.notifyDataSetChanged()
                        removeHighlightForEditableDatasets()
                        self.chartView.setSelectedDataEntry(setIndex: dataSets!.firstIndex(of: editedFanProfileSet)!, dataSet: editedFanProfileSet, index: itemIndex, entry: item)
                    } else {
                        item.x = movedEntry.x
                    }
                }
            } else if (itemIndex > movedItemIndex) {
                if (item.x < movedEntry.x) {
                    print("itemIndex > selectedItemIndex")
                    if isEntrySelected {
                        editedFanProfileSet.values[movedItemIndex] = item
                        editedFanProfileSet.values[itemIndex] = movedEntry
                        chartView.notifyDataSetChanged()
                        removeHighlightForEditableDatasets()
                        self.chartView.setSelectedDataEntry(setIndex: dataSets!.firstIndex(of: editedFanProfileSet)!, dataSet: editedFanProfileSet, index: itemIndex, entry: item)
                    } else {
                        item.x = movedEntry.x
                    }

                }
            }
            if touchFinished {
                if  counter > 0 {
                    let previousPoint = editedFanProfileSet.values[counter - 1]
                    item.x = item.x.rounded()
                    previousPoint.x = previousPoint.x.rounded()
                    if item.x <=  previousPoint.x {
                        item.x = previousPoint.x + 1
                    }
                }
            }
            counter = counter + 1
        }

        let delta = editedFanProfileSet.values[editedFanProfileSet.values.count - 3].x - cooldownEntryOldValue!.x
            //editedFanProfileSet.values[editedFanProfileSet.values.count - 1].x = min(Double(kTimeAxisMax), editedFanProfileSet.values[editedFanProfileSet.values.count - 1].x + delta)
            editedFanProfileSet.values[editedFanProfileSet.values.count - 1].x = editedFanProfileSet.values[editedFanProfileSet.values.count - 1].x + delta
        
        for dataEntry in editedFanProfileSet.values {
          //  dataEntry.x = max(0, dataEntry.x)
          //  dataEntry.x = min(Double(kTimeAxisMax), dataEntry.x)
            dataEntry.y = max(IK_MIN_FAN_SPEED, Double(dataEntry.y))
            dataEntry.y = min(IK_MAX_FAN_SPEED, max(IK_MIN_FAN_SPEED, Double(dataEntry.y)))
        }
 
    }
    

    
    func interactiveTapGestureRecognized(_ recognizer: NSUITapGestureRecognizer) {
        lastTapedPoint = recognizer.location(ofTouch: 0, in: self)
        if (isEditState) {
            if let chartViewMarker = chartView.marker as? ChartMarker {
                if let selectedData = chartView.selectedData {
                    
                    if let deleteButtonRect = chartViewMarker.deleteButtonRect {
                        if (deleteButtonRect.contains(lastTapedPoint!)) {
                            print("touch !!!!!")
                            
                            if (editedTempProfileSet == selectedData.set as? LineChartDataSet) {
                                let index = editedTempProfileSet.values.index(of: selectedData.entry)!
                                editedProfile.roastPoints.remove(at: index)
                                
                            }
                            
                            if (editedFanProfileSet == selectedData.set as? LineChartDataSet) {
                                let index = editedFanProfileSet.values.index(of: selectedData.entry)!
                                editedProfile.fanPoints.remove(at: index)
                            }
                            
                            _ = selectedData.set.removeEntry(selectedData.entry)
                            entryWasRermoved = true
                            editedFanProfileSet.circleIndexesNotToDraw = [editedFanProfileSet.entryCount - 2]
                            chartView.data?.notifyDataChanged()
                            chartView.notifyDataSetChanged()
                            self.removeHighlightForEditableDatasets()
                            isWaitungAfterPointDeleting = true
                            
                            checkPoints()

                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                self.isWaitungAfterPointDeleting = false
                            }
                        }
                    }
                }
            }

        }
    }
    
    func didTap(oldDataEntry: ChartDataEntry?, dataEntry: ChartDataEntry?, oldDataSet: ChartDataSet?, dataset: ChartDataSet?) {
        if dataEntry != nil {
            removeHighlightForEditableDatasets()
            if editedTempProfileSet.contains(dataEntry!) {
                if editedTempProfileSet.circleIndexesNotToDraw.contains(editedTempProfileSet.entryIndex(entry: dataEntry!)) {
                    return
                }
                editedFanProfileSet.selectedEntry = nil
                editedTempProfileSet.selectedEntry = dataEntry
                let higlight = Highlight(x: dataEntry!.x, y: dataEntry!.y, dataSetIndex: (dataSets?.firstIndex(of: editedTempProfileSet))!)
                //chartView.highlightValue(higlight)
                var highlights = chartView.highlighted
                highlights.append(higlight)
                chartView.highlightValues(highlights)
            }
            if editedFanProfileSet.contains(dataEntry!) {
                if editedFanProfileSet.circleIndexesNotToDraw.contains(editedFanProfileSet.entryIndex(entry: dataEntry!)) {
                    return
                }
                editedFanProfileSet.selectedEntry = dataEntry
                editedTempProfileSet.selectedEntry = nil
                let higlight = Highlight(x: dataEntry!.x, y: dataEntry!.y, dataSetIndex: (dataSets?.firstIndex(of: editedFanProfileSet))!)
                //chartView.highlightValue(higlight)
                var highlights = chartView.highlighted
                highlights.append(higlight)
                chartView.highlightValues(highlights)
            } else {
                editedFanProfileSet.selectedEntry = nil
                editedTempProfileSet.selectedEntry = nil
            }
            
        }
        
        
        if dataset == nil && dataEntry == nil && !entryWasRermoved { //empty tap
            removeHighlightForEditableDatasets()
        } else if let chartDataEntry = dataEntry {
            if (editedTempProfileSet.contains(chartDataEntry)) {
                editedTempProfileSet.drawVerticalHighlightIndicatorEnabled = editedTempProfileSet.entryIndex(entry: chartDataEntry) != 0
            } else if (editedFanProfileSet.contains(chartDataEntry)) {
                editedFanProfileSet.drawVerticalHighlightIndicatorEnabled = editedFanProfileSet.entryIndex(entry: chartDataEntry) != 0
            }
        }
        if dataset != nil {
            chartDataSetSelected(dataSet: dataset)
        }
        entryWasRermoved = false
    }
    
    func nearestPointOnTheLine(dataSet: ChartDataSet) -> CGPoint {
        
        guard let lastTapedPoint = lastTapedPoint  else {
            return CGPoint()
        }
        
        func checkPoint(p: CGPoint, v: CGPoint, w: CGPoint) -> CGPoint {
            func sqr(x: CGFloat) -> CGFloat { return x*x }
            func dist2(v: CGPoint, w: CGPoint) -> CGFloat { return sqr(x: v.x - w.x) + sqr(x: v.y - w.y) }
            func dist(dist2: CGFloat) -> CGFloat { return CGFloat(sqrtf(Float(dist2))) }
            
            let l2 = dist2(v: v, w: w)
            if l2 == 0 {
                return v
            }
            var t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
            t = max(0, min(1, t))
            
            return CGPoint(x: v.x + t * (w.x - v.x), y: v.y + t * (w.y - v.y))
        }
        
        let toushPoint = chartView.valueForTouchPoint(point: lastTapedPoint, axis: editedTempProfileSet.axisDependency)
        if (editedTempProfileSet == dataSet) {
            for (i, editedProfilePoint) in (editedProfile.roastPoints as! [RoastTempPoint]).enumerated() {
                guard i < ((editedProfile.roastPoints as! [RoastTempPoint]).count - 1) else {
                    return toushPoint
                }
                if toushPoint.x > CGFloat(editedProfilePoint.time) && CGFloat(((editedProfile.roastPoints as! [RoastTempPoint])[i+1]).time) > toushPoint.x {
                    let v = CGPoint(x: CGFloat(editedProfilePoint.time), y: CGFloat(USE_FAHRENHEIT ? TempToFahrenheit(temp: Double(editedProfilePoint.temperature)) : Double(editedProfilePoint.temperature)))
                    let tempW = Double(((editedProfile.roastPoints as! [RoastTempPoint])[i+1]).temperature)
                    let w = CGPoint(x: CGFloat(((editedProfile.roastPoints as! [RoastTempPoint])[i+1]).time), y: CGFloat(USE_FAHRENHEIT ? TempToFahrenheit(temp: tempW) : tempW))
                    
                    return checkPoint(p: toushPoint, v: v, w: w)
                }
            }
        } else if editedFanProfileSet == dataSet {
            for (i, editedProfilePoint) in (editedProfile.fanPoints as! [RoastFanPoint]).enumerated() {
                guard i < ((editedProfile.fanPoints as! [RoastFanPoint]).count - 1) else {
                    return toushPoint
                }
                if toushPoint.x > CGFloat(editedProfilePoint.time) && CGFloat(((editedProfile.fanPoints as! [RoastFanPoint])[i+1]).time) > toushPoint.x {
                    return checkPoint(p: toushPoint, v: CGPoint(x: CGFloat(editedProfilePoint.time), y: CGFloat(editedProfilePoint.power)), w: CGPoint(x: CGFloat(((editedProfile.fanPoints as! [RoastFanPoint])[i+1]).time), y: CGFloat(((editedProfile.fanPoints as! [RoastFanPoint])[i+1]).power)))
                }
            }
        }
        
        return toushPoint
    }
    
    func chartDataSetSelected(dataSet: ChartDataSet?) {
        guard dataSet != nil && !isWaitungAfterPointDeleting else {
            if (dataSet != nil) {
                self.removeHighlightForEditableDatasets()
            }
            return
        }
        
        checkPoints()
        
        if (editedTempProfileSet == dataSet || editedFanProfileSet == dataSet) {
            print("ProfileSet")
            let p = nearestPointOnTheLine(dataSet: dataSet!)//chartView.valueForTouchPoint(point: lastTapedPoint!, axis: editedTempProfileSet.axisDependency)
            if let dataEntry = chartView.getEntryByTouchPoint(point: lastTapedPoint!) {
               let distance = sqrt(pow(dataEntry.x - Double(p.x), 2) + pow(dataEntry.y - Double(p.y), 2))
                if (distance < 25) {
                    print("same point")
                    return
                }
            }
            
            
            if (editedFanProfileSet == dataSet) {
                guard (dataSet?.entryCount)! >= 4 else {
                    removeHighlightForEditableDatasets()
                    return
                }
                
                if let cooldownPoint = dataSet?.values[(dataSet?.entryCount)! - 3] {
                    if (Double(cooldownPoint.x) <= Double(p.x)) {
                        removeHighlightForEditableDatasets()
                        return
                    }
                }
            }
            
            if (editedTempProfileSet == dataSet && editedTempProfileSet.entryCount >= IK_MAX_ROAST_POINTS) {
                return
            } else if (editedFanProfileSet == dataSet && (editedFanProfileSet.entryCount - 2) >= IK_MAX_FAN_POINTS) {
                return
            }
            
            if (editedTempProfileSet == dataSet) {
                
                let point = RoastTempPoint()
                point.time = Float(p.x)
                point.temperature = USE_FAHRENHEIT ? Float(FahrenheitToTemp(temp: Double(p.y))) : Float(p.y)
                var roastPoints = editedProfile.roastPoints as! [RoastTempPoint]
                roastPoints.append(point)
                editedProfile.roastPoints = roastPoints.sorted{$0.time < $1.time}
                graphWasEdited = true
            }
            
            if (editedFanProfileSet == dataSet) {
                let point = RoastFanPoint()
                point.time = Float(p.x)
                point.power = Float(p.y)
                var fanPoints = editedProfile.fanPoints as! [RoastFanPoint]
                fanPoints.append(point)
                editedProfile.fanPoints = fanPoints.sorted{$0.time < $1.time}
                graphWasEdited = true
            }
            let entry = ChartDataEntry(x: Double(p.x), y: Double(p.y))
            _ = dataSet!.addEntryOrdered(entry)
            editedFanProfileSet.circleIndexesNotToDraw = [editedFanProfileSet.entryCount - 2]
            chartView.data?.notifyDataChanged()
            chartView.notifyDataSetChanged()
            self.removeHighlightForEditableDatasets()
            if (editedTempProfileSet == dataSet) {
                let itemIndex = editedTempProfileSet.values.index(of: entry)!
                self.chartView.setSelectedDataEntry(setIndex: 1, dataSet: editedTempProfileSet, index: itemIndex, entry: entry)
                editedTempProfileSet.selectedEntry = entry
                editedTempProfileSet.drawVerticalHighlightIndicatorEnabled = true
            } else if (editedFanProfileSet == dataSet) {
                let itemIndex = editedFanProfileSet.values.index(of: entry)!
                self.chartView.setSelectedDataEntry(setIndex: 2, dataSet: editedFanProfileSet, index: itemIndex, entry: entry)
                editedFanProfileSet.selectedEntry = entry
                editedFanProfileSet.drawVerticalHighlightIndicatorEnabled = true
            }
            
            
            
            let highlightPoint = Highlight(x: Double(p.x), y: Double(p.y) , dataSetIndex: dataSets!.index(of: dataSet!)!)
            var highlights = chartView.highlighted
            highlights.append(highlightPoint)
            chartView.highlightValues(highlights)
        }
    }
    
}

extension IKTempChartView: ChartViewDelegate {
    func chartScaled(_ chartView: ChartViewBase, scaleX: CGFloat, scaleY: CGFloat) {
        let leftAxis = self.chartView.leftAxis
        
        leftAxis.granularity = round(50 / Double(self.chartView.scaleY) / 5) * 5
        /*
        let isLandscape = UIDevice.current.orientation.isFlat ? UIApplication.shared.statusBarOrientation.isLandscape : UIDevice.current.orientation.isLandscape
        if isLandscape {
            self.chartView.moveViewToX(0)
        }
 */
    }
}
