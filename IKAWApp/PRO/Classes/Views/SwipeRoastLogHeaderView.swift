//
//  SwipeRoastLogHeaderView.swift
//  IKAWA-Pro
//
//  Created by Admin on 10/8/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SwipeCellKit
import IKRoasterLib

class SwipeRoastLogHeaderView: NibLoadingView {

    @IBOutlet weak var tableView: UITableView!
    var swipeCellTransitionContext: SwipeActionTransitioningContext?
    let cell = SwipeTableViewCell()
    var roastLogHeaderView: RoastLogHeaderView = RoastLogHeaderView()
    var profile: Profile?
    //MARK: - Lifecicle
    override func awakeFromNib() {
        super.awakeFromNib()
        cell.contentView.addSubview(roastLogHeaderView)
        roastLogHeaderView.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(cell.contentView).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
    }
    
    //MARK: - Public
    public func populate(profile: Profile, uuid: String, backButtonAction: @escaping (()->())) {
        self.profile = profile
        if profile.archived {
            cell.delegate = nil
        } else {
            cell.delegate = self
        }
        roastLogHeaderView.numberLabel.text = uuid
        roastLogHeaderView.profileName.text = profile.getNameWithType()
        roastLogHeaderView.roundImageView.image = UIImage(named: profile.tempSensor == TempSensorBelow ? "roundInlet" : "roundExhaust")
        //numberLabel.text = String(index ?? 0)
        roastLogHeaderView.backButtonAction = backButtonAction
    }
    
    public func hideBackButton(hide: Bool) {
        roastLogHeaderView.isHidden = isHidden
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        fixTableViewInsets()
    }
    
    func fixTableViewInsets() {
        let zContentInsets = UIEdgeInsets.zero
        tableView.contentInset = zContentInsets
        tableView.scrollIndicatorInsets = zContentInsets
    }
    
}

extension SwipeRoastLogHeaderView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cell
    }
}

extension SwipeRoastLogHeaderView: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard let profile = profile else {
            return nil
        }
        if orientation == .right {
            let favoriteAction = SwipeAction(style: .default, title: nil, handler: { action, indexPath in
                NotificationCenter.default.removeObserver(self, name: Notification.Name.ProfileManagerDidChangeList, object: nil)
                _ = AppCore.sharedInstance.profileManager.favorProfile(isFavorite: !profile.favorite, profile: profile)
                let image = UIImage(named: profile.favorite ? "FavoriteHighlight" : "Favorite" )
                self.swipeCellTransitionContext?.button.setImage(image, for: .normal)
                
            })
            favoriteAction.transitionDelegate = self
            favoriteAction.image = UIImage(named: profile.favorite ? "FavoriteHighlight" : "Favorite" )
            favoriteAction.backgroundColor = kColorLightGrey3
            favoriteAction.hidesWhenSelected = true
            return [favoriteAction]
        }
        return nil
    }
    
    func visibleRect(for tableView: UITableView) -> CGRect? {
        return tableView.bounds
    }
    
}

extension SwipeRoastLogHeaderView: SwipeActionTransitioning {
    func didTransition(with context: SwipeActionTransitioningContext) -> Void {
        swipeCellTransitionContext = context
    }
}
