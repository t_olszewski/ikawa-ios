//
//  KeyboardInputView.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 9/6/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class KeyboardInputView: UIInputView {
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var leftArrow: UIButton!
    @IBOutlet weak var rightArrow: UIButton!
    private var textField: UITextField?
    
    func addConfinguration(action: @escaping (_ sender: Any) -> Void) {
        doneButton.actionHandle(controlEvents: .touchUpInside,
                                ForAction:{() -> Void in
                                    action(self.doneButton)
        })
    }
    
    func setupArrows(textField: UITextField) {
        leftArrow.isHidden = false
        let origLeftImage = leftArrow.image(for: .normal)
        let tintedLeftImage = origLeftImage?.withRenderingMode(.alwaysTemplate)
        leftArrow.setImage(tintedLeftImage, for: .normal)
        leftArrow.tintColor = .white
        rightArrow.isHidden = false
        let origRightImage = rightArrow.image(for: .normal)
        let tintedRightImage = origRightImage?.withRenderingMode(.alwaysTemplate)
        rightArrow.setImage(tintedRightImage, for: .normal)
        rightArrow.tintColor = .white
        self.textField = textField
    }
    
    @IBAction func rightButtonAction(_ sender: Any) {
        if let selectedRange = textField?.selectedTextRange {
            if let newPosition = textField?.position(from: selectedRange.start, offset: 1) {
                textField?.selectedTextRange = textField?.textRange(from: newPosition, to: newPosition)
            }
        }
    }
    
    @IBAction func leftButtonAction(_ sender: Any) {
        if let selectedRange = textField?.selectedTextRange {
            if let newPosition = textField?.position(from: selectedRange.start, offset: -1) {
                textField?.selectedTextRange = textField?.textRange(from: newPosition, to: newPosition)
            }
        }
    }
    
    static func instanceFromNib() -> KeyboardInputView {
        return UINib(nibName: "KeyboardInputView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! KeyboardInputView
    }
}

extension UIButton {
    private func actionHandleBlock(action:(() -> Void)? = nil) {
        struct __ {
            static var action :(() -> Void)?
        }
        if action != nil {
            __.action = action
        } else {
            __.action?()
        }
    }
    
    @objc private func triggerActionHandleBlock() {
        self.actionHandleBlock()
    }
    
    func actionHandle(controlEvents control :UIControl.Event, ForAction action:@escaping () -> Void) {
        self.actionHandleBlock(action: action)
        self.addTarget(self, action: "triggerActionHandleBlock", for: control)
    }
}
