//
//  SwipableTableViewCell.swift
//  CellButtons
//
//  Created by Vegard Solheim Theriault on 12/07/15.
//  Copyright © 2015 Vegard Solheim Theriault. All rights reserved.
//

import UIKit

protocol SwipableTableViewCellDelegate {
    func swipableTableViewCellDidSwipeLeftWithCell(cell: UITableViewCell)
    func swipableTableViewCellDidSwipeRightWithCell(cell: UITableViewCell)
}

class SwipableTableViewCell: UITableViewCell, CAAnimationDelegate {
    
    
    // -------------------------------
    // MARK: Public Properties
    // -------------------------------
    var isSwipable = false
    var delegate: SwipableTableViewCellDelegate?
    
    var leftColor: UIColor = kColorLightGrey2 {
        didSet {
            guard let leftLayer = leftLayer else { return }
            leftLayer.backgroundColor = leftColor.cgColor
        }
    }
    
    var rightColor: UIColor = kColorLightGrey2 {
        didSet {
            guard let rightLayer = rightLayer else { return }
            rightLayer.backgroundColor = rightColor.cgColor
        }
    }
    
    var leftConfirmationTitle: String = "Delete" {
        didSet {
            leftConfirmationTitleDidSet()
        }
    }
    
    var rightConfirmationTitle: String = "Delete" {
        didSet {
            rightConfirmationTitleDidSet()
        }
    }
    
    
    
    
    // -------------------------------
    // MARK: Private Properties
    // -------------------------------
    
    private var rightLayer: CALayer!
    private var leftLayer:  CALayer!
    
    private var leftConfirmationIndicator:  CALayer!
    private var rightConfirmationIndicator: CALayer!
    
    private var leftConfirmationIndicatorIsShowing  = false
    private var rightConfirmationIndicatorIsShowing = false
    
    private var hasBeenAwokenFromNib = false
    
    private var panRecognizer: UIPanGestureRecognizer!
    
    
    
    
    // -------------------------------
    // MARK: Private Constants
    // -------------------------------
    
    private struct SwipingValue {
        static let RatioForSnapping:                 CGFloat = 0.3 // Affects how far across the screen a cell needs to be swiped to be selected
        static let RubberCoefficient:                CGFloat = 4   // Affects how much rubber-effect the cell will have when it's been swiped far enough
        static let ConfirmationAccelerationExponent: CGFloat = 4   // Affects how fast the selection layer will catch up to the cell content when swiped
    }
    
    private struct SpringAnimationValue {
        static let Stiffness:       CGFloat = 300
        static let Damping:         CGFloat = 17
        static let InitialVelocity: CGFloat = 10
    }
    
    private struct ConfirmationIndicatorValue {
        static let Inset:               CGFloat = 16.0 // The distance the indicator will be from the edge of the cell
        static let WidthToHeightRatio:  CGFloat = 0.7  // Width of the confirmation indicator as a percentage of the indicator height
        static let HeightRatio:         CGFloat = 0.5  // Height of the confirmation indicator as a percentage of cell height.
        static let DrawingTime:  CFTimeInterval = 0.2  // The time it takes to draw the confirmation indicators
    }
    
    private struct AnimationKey {
        static let MoveLeftToCompletion               = "MoveLeftToCompletion"
        static let MoveLayerToLeftCompletion          = "MoveLayerToLeftCompletion"
        static let MoveRightToCompletion              = "MoveRightToCompletion"
        static let MoveLayerToRightCompletion         = "MoveLayerToRightCompletion"
        
        static let FadeInLeftConfirmationIndicator    = "FadeInLeftConfirmationIndicator"
        static let FadeInRightConfirmationIndicator   = "FadeInRightConfirmationIndicator"
        static let FadeOutLeftConfirmationIndicator   = "FadeOutLeftConfirmationIndicator"
        static let FadeOutRightConfirmationIndicator  = "FadeOutRightConfirmationIndicator"
        
        static let MoveBackCellAnimation                = "MoveBackCellAnimation"
        static let MoveBackRightAnimation             = "MoveBackRightAnimation"
        static let MoveBackLeftAnimation              = "MoveBackLeftAnimation"
    }
    
    private struct AnimationProperty {
        static let Opacity = "opacity"
        static let PositionX = "position.x"
    }
    
    
    
    
    
    // -------------------------------
    // MARK: Life Cycle
    // -------------------------------
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        hasBeenAwokenFromNib = true
        
        selectionStyle = UITableViewCell.SelectionStyle.none
        panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panning(recognizer:)))
        panRecognizer.delegate = self
        contentView.addGestureRecognizer(panRecognizer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Cancels the current pan if there is one
        if panRecognizer != nil {
            panRecognizer.isEnabled = false
            panRecognizer.isEnabled = true
        }
        
        resetAllLayers()
        addLayers()
    }
    
    
    // Includes removing the added layers
    private func resetAllLayers() {
        if rightLayer != nil {
            rightLayer.removeFromSuperlayer()
            rightLayer = nil
        }
        if leftLayer != nil {
            leftLayer.removeFromSuperlayer()
            leftLayer = nil
        }
        if leftConfirmationIndicator != nil {
            leftConfirmationIndicator.removeFromSuperlayer()
            leftLayer = nil
        }
        if rightConfirmationIndicator != nil {
            rightConfirmationIndicator.removeFromSuperlayer()
            rightConfirmationIndicator = nil
        }
        
        contentView.layer.position = CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    private func addLayers() {
        if hasBeenAwokenFromNib == true && rightLayer == nil && leftLayer == nil {
            addSelectionLayers()
            addConfirmationIndicatorLayers()
        }
    }
    
    
    private func rightConfirmationTitleDidSet() {
        if let rightConfirmationIndicator = self.rightConfirmationIndicator {
            rightConfirmationIndicator.removeFromSuperlayer()
        }
        
        rightConfirmationIndicator = CALayer()
        let rightLabel = CATextLayer()
        rightLabel.contentsScale = UIScreen.main.scale
        rightLabel.font = CTFontCreateWithName(kFontAvenirNextRegular as CFString, 14, nil)
        rightLabel.fontSize = 14
        let width = rightConfirmationTitle.width(withConstrainedHeight: self.contentView.frame.height, font: UIFont(name: kFontAvenirNextRegular, size: 14)!)
        rightLabel.frame = CGRect(x: self.frame.size.width - width - 30, y: self.contentView.frame.height / 2 - UIFont(name: kFontAvenirNextRegular, size: 14)!.lineHeight / 2, width: width, height: self.contentView.frame.height)
        rightLabel.string = rightConfirmationTitle
        rightLabel.alignmentMode = CATextLayerAlignmentMode.right
        rightLabel.foregroundColor = UIColor.white.cgColor
        rightConfirmationIndicator.addSublayer(rightLabel)
        rightConfirmationIndicator.opacity = 0
        self.layer.addSublayer(rightConfirmationIndicator)
    }
    
    private func leftConfirmationTitleDidSet() {
        if let leftConfirmationIndicator = self.leftConfirmationIndicator {
            leftConfirmationIndicator.removeFromSuperlayer()
        }
        
        self.leftConfirmationIndicator = CALayer()
        
        let leftLabel = CATextLayer()
        leftLabel.contentsScale = UIScreen.main.scale
        leftLabel.font = CTFontCreateWithName(kFontAvenirNextRegular as CFString, 14, nil)
        leftLabel.fontSize = 14
        leftLabel.frame = CGRect(x: 30, y: self.contentView.frame.height / 2 - UIFont(name: kFontAvenirNextRegular, size: 14)!.lineHeight / 2, width: leftConfirmationTitle.width(withConstrainedHeight: self.contentView.frame.height, font: UIFont(name: kFontAvenirNextRegular, size: 14)!), height: self.contentView.frame.height)
        leftLabel.string = leftConfirmationTitle
        leftLabel.alignmentMode = CATextLayerAlignmentMode.left
        leftLabel.foregroundColor = UIColor.white.cgColor
        leftConfirmationIndicator.addSublayer(leftLabel)
        leftConfirmationIndicator.opacity = 0
        self.layer.addSublayer(leftConfirmationIndicator)
    }
    
    // -------------------------------
    // MARK: Add Layers
    // -------------------------------
    
    private func addSelectionLayers() {
        leftLayer = CALayer()
        
        // Workaround for the fact that the layers are visible while rotating
        // The color will be set when the pan gesture is in state .Began
        leftLayer!.backgroundColor = UIColor.clear.cgColor
        leftLayer!.frame = CGRect(x:  -bounds.size.width,
                                  y:  0,
                                  width:  bounds.size.width,
                                  height: bounds.size.height)
        layer.addSublayer(leftLayer!)
        
        rightLayer = CALayer()
        
        // Workaround for the fact that the layers are visible while rotating
        // The color will be set when the pan gesture is in state .Began
        rightLayer!.backgroundColor = UIColor.clear.cgColor
        rightLayer!.frame = CGRect(x:  bounds.size.width,
                                   y:  0,
                                   width:  bounds.size.width,
                                   height: bounds.size.height)
        layer.addSublayer(rightLayer!)
    }
    
    private func addConfirmationIndicatorLayers() {
        leftConfirmationTitleDidSet()
        rightConfirmationTitleDidSet()
    }
    
    // -------------------------------
    // MARK: Pan Gesture Recognizer
    // -------------------------------
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard isSwipable == true else {
            return false
        }
        if gestureRecognizer == panRecognizer {
            let translation = panRecognizer.translation(in: self)
            if abs(translation.x) > abs(translation.y) {
                return true
            }
        }
        return false
    }
    
    @objc func panning(recognizer: UIPanGestureRecognizer) {
        if isAnimating == false {
            switch recognizer.state {
            case .began:
                // Workaround for the fact that the layers are visible while rotating
                // The color will be set to clearColor as part of the layoutSubviews() call
                leftLayer?.backgroundColor = leftColor.cgColor
                rightLayer?.backgroundColor = rightColor.cgColor
            case .changed:
                let x = layersXPositionForPansTranslationInView(translationInView: recognizer.translation(in: self))
                let y = bounds.size.height / 2
                
                if recognizer.translation(in: self).x > 0 { // Is declining
                    // Make sure the decline button is properly hidden
                    rightLayer.position = CGPoint(x: bounds.size.width * 1.5, y: bounds.size.height / 2)
                    expandAcceptLayerToX(xInput: x)
                } else { // Is declining
                    // Make sure the accept button is properly hidden
                    leftLayer.position = CGPoint(x: -(bounds.size.width / 2), y: bounds.size.height / 2)
                    expandDeclineLayerToX(xInput: x)
                }
                contentView.layer.position = CGPoint(x: x, y: y)
                
            case UIGestureRecognizerState.ended:
                if translationIsFarEnoughToSnap(translationInView: recognizer.translation(in: self)) {
                    // Move to whichever button was selected
                    if contentView.layer.position.x > bounds.size.width / 2 {
                        animateToAccept()
                    } else {
                        animateToDecline()
                    }
                } else {
                    
                    if contentView.layer.position.x > bounds.size.width / 2 {
                        animateAcceptBackToStart()
                    } else {
                        animateDeclineBackToStart()
                    }
                    
                    animateSwiperBackToStart()
                }
            case UIGestureRecognizerState.cancelled: fallthrough
            case UIGestureRecognizerState.failed:
                animateSwiperBackToStart()
            default:
                break
            }
        }
    }
    
    
    
    
    // -------------------------------
    // MARK: Animate Back to Default Position
    // -------------------------------
    
    private func animateSwiperBackToStart() {
        let from = Double(contentView.layer.position.x)
        let to   = Double(bounds.size.width) / 2
        
        let animation = moveBackAnimationWithFromValue(fromValue: from, andToValue: to)
        
        contentView.layer.setValue(NSNumber(value: to), forKeyPath: AnimationProperty.PositionX)
        contentView.layer.add(animation, forKey: AnimationKey.MoveBackCellAnimation)
    }
    
    private func animateDeclineBackToStart() {
        removeRightIndicator()
        
        let from = Double(rightLayer.position.x)
        let to   = Double(bounds.size.width * 1.5)
        
        let animation = moveBackAnimationWithFromValue(fromValue: from, andToValue: to)
        
        rightLayer.setValue(NSNumber(value: to), forKeyPath: AnimationProperty.PositionX)
        rightLayer.add(animation, forKey: AnimationKey.MoveBackRightAnimation)
    }
    
    private func animateAcceptBackToStart() {
        removeAcceptConfirmationIndicator()
        
        let from = Double(leftLayer.position.x)
        let to   = Double(bounds.size.width / 2 * -1)
        
        let animation = moveBackAnimationWithFromValue(fromValue: from, andToValue: to)
        
        leftLayer.setValue(NSNumber(value: to), forKeyPath: AnimationProperty.PositionX)
        leftLayer.add(animation, forKey: AnimationKey.MoveBackLeftAnimation)
    }
    
    private func moveBackAnimationWithFromValue(fromValue: Double, andToValue toValue: Double) -> CASpringAnimation {
        let animation               = CASpringAnimation(keyPath: AnimationProperty.PositionX)
        animation.toValue           = NSNumber(value: toValue)
        animation.fromValue         = NSNumber(value: fromValue)
        animation.stiffness         = SpringAnimationValue.Stiffness
        animation.damping           = SpringAnimationValue.Damping
        animation.initialVelocity   = SpringAnimationValue.InitialVelocity
        animation.duration          = animation.settlingDuration
        
        return animation
    }
    
    
    
    
    
    // -------------------------------
    // MARK: Animate to Completion
    // -------------------------------
    
    private var rightCompletionAnimation: CABasicAnimation?
    private var leftCompletionAnimation:  CABasicAnimation?
    
    private func animateToAccept() {
        let acceptLayerFrom = Double(leftLayer.position.x)
        let acceptLayerTo   = Double(bounds.size.width / 2)
        let acceptAnimation = completionAnimationWithFromValue(fromValue: acceptLayerFrom, andToValue: acceptLayerTo)
        
        leftLayer.setValue(NSNumber(value: acceptLayerTo), forKeyPath: AnimationProperty.PositionX)
        leftLayer.add(acceptAnimation, forKey: AnimationKey.MoveLeftToCompletion)
        
        let layerFrom = Double(layer.position.x)
        let layerTo   = Double(bounds.size.width * 1.5)
        leftCompletionAnimation = completionAnimationWithFromValue(fromValue: layerFrom, andToValue: layerTo)
        leftCompletionAnimation?.isRemovedOnCompletion = false
        leftCompletionAnimation!.delegate = self as? CAAnimationDelegate
        
        contentView.layer.setValue(NSNumber(value: layerTo), forKeyPath: AnimationProperty.PositionX)
        contentView.layer.add(leftCompletionAnimation!, forKey: AnimationKey.MoveLayerToLeftCompletion)
    }
    
    private func animateToDecline() {
        let declineLayerFrom = Double(rightLayer.position.x)
        let declineLayerTo   = Double(bounds.size.width / 2)
        let declineAnimation = completionAnimationWithFromValue(fromValue: declineLayerFrom, andToValue: declineLayerTo)
        
        rightLayer.setValue(NSNumber(value: declineLayerTo), forKeyPath: AnimationProperty.PositionX)
        rightLayer.add(declineAnimation, forKey: AnimationKey.MoveRightToCompletion)
        
        let layerFrom       = Double(contentView.layer.position.x)
        let layerTo         = Double(bounds.size.width / 2 * -1)
        rightCompletionAnimation  = completionAnimationWithFromValue(fromValue: layerFrom, andToValue: layerTo)
        rightCompletionAnimation?.isRemovedOnCompletion = false
        rightCompletionAnimation!.delegate = self as! CAAnimationDelegate
        
        contentView.layer.setValue(NSNumber(value: layerTo), forKeyPath: AnimationProperty.PositionX)
        contentView.layer.add(rightCompletionAnimation!, forKey: AnimationKey.MoveLayerToRightCompletion)
    }
    
    private func completionAnimationWithFromValue(fromValue: Double, andToValue toValue: Double) -> CABasicAnimation {
        let animation            = CABasicAnimation(keyPath: AnimationProperty.PositionX)
        animation.toValue        = NSNumber(value: toValue)
        animation.fromValue      = NSNumber(value: fromValue)
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        animation.duration       = 0.15
        
        return animation
    }
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            if let animation = contentView.layer.animation(forKey: AnimationKey.MoveLayerToLeftCompletion), animation == anim {
                // Did finish accepting
                delegate?.swipableTableViewCellDidSwipeLeftWithCell(cell: self)
            } else if let animation = contentView.layer.animation(forKey: AnimationKey.MoveLayerToRightCompletion), animation == anim {
                // Did finish declining
                delegate?.swipableTableViewCellDidSwipeRightWithCell(cell: self)
            }
        }
    }
    
    
    
    
    // -------------------------------
    // MARK: Expanding the Layers
    // -------------------------------
    
    private func expandAcceptLayerToX(xInput: CGFloat) {
        guard let acceptLayer = leftLayer else { return }
        let normalizedXInput = xInput - (bounds.size.width / 2)
        
        let destination = bounds.size.width * SwipingValue.RatioForSnapping
        let progress = normalizedXInput / destination
        
        var normalizedXToExpandTo = normalizedXInput
        if progress < 1 {
            let acceptLayerProgress = pow(progress, SwipingValue.ConfirmationAccelerationExponent)
            normalizedXToExpandTo = destination * acceptLayerProgress
            
            removeAcceptConfirmationIndicator()
        } else {
            addLeftIndicator()
        }
        
        let xToExpandTo = normalizedXToExpandTo - (bounds.size.width / 2)
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        acceptLayer.position.x = xToExpandTo
        CATransaction.commit()
    }
    
    private func expandDeclineLayerToX(xInput: CGFloat) {
        guard let declineLayer = rightLayer else { return }
        
        let snapDistance = bounds.size.width * SwipingValue.RatioForSnapping
        
        let progress = 1 - (xInput - ((bounds.size.width / 2) - snapDistance)) / snapDistance
        
        var declineLayerDistanceMoved = bounds.size.width / 2 - xInput
        if progress < 1 {
            let declineLayerProgress = pow(progress, SwipingValue.ConfirmationAccelerationExponent)
            declineLayerDistanceMoved = snapDistance * declineLayerProgress
            
            removeRightIndicator()
        } else {
            addRightIndicator()
        }
        
        let newDeclineLayerX = bounds.size.width * 1.5 - declineLayerDistanceMoved
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        declineLayer.position.x = newDeclineLayerX
        CATransaction.commit()
    }
    
    
    
    
    
    // -------------------------------
    // MARK: Draw Completion Indicators
    // -------------------------------
    
    private func addLeftIndicator() {
        if leftConfirmationIndicatorIsShowing == false {
            leftConfirmationIndicatorIsShowing = true
            
            let fadeInAnimation = CABasicAnimation(keyPath: AnimationProperty.Opacity)
            fadeInAnimation.fromValue = NSNumber(value: 0)
            //     fadeInAnimation.fromValue = NSNumber(value: Double(leftIndicator.strokeEnd))
            fadeInAnimation.toValue = NSNumber(value: 1.0)
            fadeInAnimation.duration = ConfirmationIndicatorValue.DrawingTime
            
            leftConfirmationIndicator.setValue(NSNumber(value: 1.0), forKey: AnimationProperty.Opacity)
            leftConfirmationIndicator.add(fadeInAnimation, forKey: AnimationKey.FadeInLeftConfirmationIndicator)
        }
    }
    
    private func addRightIndicator() {
        if rightConfirmationIndicatorIsShowing == false {
            rightConfirmationIndicatorIsShowing = true
            
            let fadeInAnimation = CABasicAnimation(keyPath: AnimationProperty.Opacity)
            fadeInAnimation.fromValue = NSNumber(value: Double(0))
            //     fadeInAnimation.fromValue = NSNumber(value: Double(rightIndicator.strokeEnd))
            fadeInAnimation.toValue = NSNumber(value: 1.0)
            fadeInAnimation.duration = ConfirmationIndicatorValue.DrawingTime
            
            rightConfirmationIndicator.setValue(NSNumber(value: 1.0), forKey: AnimationProperty.Opacity)
            rightConfirmationIndicator.add(fadeInAnimation, forKey: AnimationKey.FadeInRightConfirmationIndicator)
        }
    }
    
    private func removeAcceptConfirmationIndicator() {
        if leftConfirmationIndicatorIsShowing == true {
            leftConfirmationIndicatorIsShowing = false
            
            let fadeOutAnimation = CABasicAnimation(keyPath: AnimationProperty.Opacity)
            fadeOutAnimation.fromValue = NSNumber(value: Double(1))
            //      fadeOutAnimation.fromValue = NSNumber(value: Double(leftIndicator.strokeEnd))
            fadeOutAnimation.toValue = NSNumber(value: 0.0)
            fadeOutAnimation.duration = ConfirmationIndicatorValue.DrawingTime
            
            leftConfirmationIndicator.setValue(NSNumber(value: 0.0), forKey: AnimationProperty.Opacity)
            leftConfirmationIndicator.add(fadeOutAnimation, forKey: AnimationKey.FadeOutLeftConfirmationIndicator)
        }
    }
    
    private func removeRightIndicator() {
        if rightConfirmationIndicatorIsShowing == true {
            rightConfirmationIndicatorIsShowing = false
            
            let fadeOutAnimation = CABasicAnimation(keyPath: AnimationProperty.Opacity)
            fadeOutAnimation.fromValue = NSNumber(value: Double(1))
            //       fadeOutAnimation.fromValue = NSNumber(value: Double(rightIndicator.strokeEnd))
            fadeOutAnimation.toValue = NSNumber(value: 0.0)
            fadeOutAnimation.duration = ConfirmationIndicatorValue.DrawingTime
            
            rightConfirmationIndicator.setValue(NSNumber(value: 0.0), forKey: AnimationProperty.Opacity)
            rightConfirmationIndicator.add(fadeOutAnimation, forKey: AnimationKey.FadeOutRightConfirmationIndicator)
        }
    }
    
    
    
    
    
    // -------------------------------
    // MARK: Private Helpers
    // -------------------------------
    
    private var isAnimating: Bool {
        get {
            if let
                contentLayerKeys = contentView.layer.animationKeys(),
                let declineLayerKeys = rightLayer.animationKeys(),
                let acceptLayerKeys  = leftLayer.animationKeys(),
                contentLayerKeys.count > 0 ||
                    declineLayerKeys.count > 0 ||
                    acceptLayerKeys.count  > 0
            {
                return true
            } else {
                return false
            }
        }
    }
    
    private func layersXPositionForPansTranslationInView(translationInView: CGPoint) -> CGFloat {
        if !translationIsFarEnoughToSnap(translationInView: translationInView) {
            return bounds.size.width / 2 + translationInView.x
        } else {
            var nonRestrictedPart = bounds.size.width * SwipingValue.RatioForSnapping
            if translationInView.x < 0 {
                nonRestrictedPart *= -1
            }
            let restrictedPart = (translationInView.x - nonRestrictedPart) / SwipingValue.RubberCoefficient
            
            return bounds.size.width / 2 + nonRestrictedPart + restrictedPart
        }
    }
    
    private func translationIsFarEnoughToSnap(translationInView: CGPoint) -> Bool {
        return abs(translationInView.x) > SwipingValue.RatioForSnapping * bounds.size.width
    }
    
}
