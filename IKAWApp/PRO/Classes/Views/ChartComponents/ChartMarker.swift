//
//  ChartMarker.swift
//  IKAWApp
//
//  Created by Admin on 4/3/18.
//  Copyright © 2018 Admin. All rights reserved.
//
import Charts
import IKRoasterLib

class ChartMarker: MarkerView {
    var dataSet: LineChartDataSet?
    var text: String?
    var xText: String?
    var yText: String?
    var point: CGPoint?
    var isDeletePointMode = false
    var deleteButtonRect: CGRect?
    let buttonMarginX = 10
    let buttonMarginY = 15
    var isFirstPoint: Bool = false
    var isEditState: Bool = false
    
    var numberFormatter: NumberFormatter {
       let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.minimumIntegerDigits = 2
        numberFormatter.maximumFractionDigits = 0
        numberFormatter.roundingMode = .floor
        return numberFormatter
    }
//    var deleteButtonPoint: CGPoint {
//        let isLandscape = UIDevice.current.orientation.isFlat ? UIApplication.shared.statusBarOrientation.isLandscape : UIDevice.current.orientation.isLandscape
//        if  isLandscape {
//            return CGPoint(x: 70, y: 66)
//        } else {
//            return CGPoint(x: 15, y: 66)
//        }
//    }
    
    var pointTextDrawAttributes: [NSAttributedString.Key : Any] = {
        var drawAttr = [NSAttributedString.Key : Any]()
        
        drawAttr[.kern] = 1
        let font = UIFont(name: kFontAvenirNextLTProBold, size: 11)
        drawAttr[.font] = font
        drawAttr[.foregroundColor] = UIColor.white
        drawAttr[.backgroundColor] = UIColor.clear
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.paragraphSpacing = 5
        drawAttr[.paragraphStyle] = paragraphStyle
        
        return drawAttr
    }()
    
    var crackTextDrawAttributes: [NSAttributedString.Key : Any] = {
        var drawAttr = [NSAttributedString.Key : Any]()
        
        drawAttr[.kern] = 1
        let font = UIFont(name: kFontAvenirNextDemiBold, size: 11)
        drawAttr[.font] = font
        drawAttr[.kern] = 0.46
        drawAttr[.foregroundColor] = UIColor.white
        drawAttr[.backgroundColor] = kColorDarkGrey2
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.paragraphSpacing = 5
        drawAttr[.paragraphStyle] = paragraphStyle
        
        return drawAttr
    }()
    
    override func refreshContent(entry: ChartDataEntry, highlight: Highlight) {
        super.refreshContent(entry: entry, highlight: highlight)
        let chartView = self.chartView
        let index = highlight.dataSetIndex
        guard let dataSet = self.chartView?.data?.dataSets[index] as? LineChartDataSet, let dataSetType = dataSet.dataSetType  else {return}
        self.dataSet = dataSet
        //print("data set index \(index)")
        isFirstPoint = chartView!.data!.dataSets[index].entryIndex(entry: entry) == 0
        pointTextDrawAttributes[.foregroundColor] = UIColor.white
        pointTextDrawAttributes[.font] = UIFont(name: kFontAvenirNextLTProBold, size: 11)
        
        switch dataSetType {
        case .timelineSet: //timeline
            self.bounds = CGRect(x: 0, y: 0, width: 0, height: 0)
            if (entry.x == 0) {
                text = nil
                xText = nil
                yText = nil
            } else {
                xText = nil
                yText = nil
                if let seconds = numberFormatter.string(from: NSNumber(value: entry.x.truncatingRemainder(dividingBy: 60))) {
                    text = String(format: "%dm:%@s", Int(entry.x / 60), seconds)
                }
            }
            let textSize = (text ?? "").size(withAttributes: pointTextDrawAttributes)
            self.offset = CGPoint(x: 10, y: -textSize.height)
            break
        case .editedFanProfileSet: //editedFanProfileSet
            text = nil
            if let seconds = numberFormatter.string(from: NSNumber(value: entry.x.truncatingRemainder(dividingBy: 60))) {
                xText = String(format: "%d:%@", Int(entry.x / 60), seconds)
            }
            yText = String(format: "%.0f%%", entry.y)
            
            if (entry.isRemovable) {
                let image = UIImage.init(imageLiteralResourceName: "DeletePoint")
                self.bounds = CGRect(x: CGFloat(buttonMarginX), y: (-image.size.height - CGFloat(buttonMarginY)), width: image.size.width, height: image.size.height)
                isDeletePointMode = true
            } else {
                isDeletePointMode = false
            }
            break
        case .editedTempProfileSet: //editedTempProfileSet
            pointTextDrawAttributes[.foregroundColor] = UIColor.white
            text = nil
            if let seconds = numberFormatter.string(from: NSNumber(value: entry.x.truncatingRemainder(dividingBy: 60))) {
                xText = String(format: "%d:%@", Int(entry.x / 60), seconds)
            }
            let y = entry.y
//            if (USE_FAHRENHEIT) {
//                y = TempToFahrenheit(temp: y)
//            }
            yText = String(format: "%.0f\(USE_FAHRENHEIT ? "℉" : "°C")", y)
            if (entry.isRemovable) {
                let image = UIImage.init(imageLiteralResourceName: "DeletePoint")
                self.bounds = CGRect(x: CGFloat(buttonMarginX), y: (-image.size.height - CGFloat(buttonMarginY)), width: image.size.width, height: image.size.height)
                isDeletePointMode = true
            } else {
                isDeletePointMode = false
            }
            break
        case .tempBelowSet://below
                pointTextDrawAttributes[.foregroundColor] = isEditState ? kColorInlet.withAlphaComponent(0.5) : kColorInlet
                pointTextDrawAttributes[.font] = UIFont(name: kFontAvenirNextMedium, size: 12)
                pointTextDrawAttributes[.kern] = 0.6
                self.bounds = CGRect(x: 0, y: 0, width: 0, height: 0)
                xText = nil
                yText = nil
                let y = entry.y
//                if (USE_FAHRENHEIT) {
//                    y = TempToFahrenheit(temp: y)
//                }
                text = String(format: "%.0f\(USE_FAHRENHEIT ? "℉" : "°C")", round(y))
                let textSize = (text ?? "").size(withAttributes: pointTextDrawAttributes)
                self.offset = CGPoint(x: 10, y: -textSize.height / 2 - 15)
            
            break
        case .tempAboveSet://above
                pointTextDrawAttributes[.foregroundColor] = isEditState ? kColorExhaust.withAlphaComponent(0.5) : kColorExhaust
                pointTextDrawAttributes[.font] = UIFont(name: kFontAvenirNextMedium, size: 12)
                pointTextDrawAttributes[.kern] = 0.6
                self.bounds = CGRect(x: 0, y: 0, width: 0, height: 0)
                xText = nil
                yText = nil
                let y = entry.y
//                if (USE_FAHRENHEIT) {
//                    y = TempToFahrenheit(temp: y)
//                }
                text = String(format: "%.0f\(USE_FAHRENHEIT ? "℉" : "°C")", round(y))
                let textSize = (text ?? "").size(withAttributes: pointTextDrawAttributes)
                self.offset = CGPoint(x: 10, y: -textSize.height / 2 - 15)
            break
            
        case .firstCrackSet, .secondCrackSet, .colorChangeSet, .turningPointSet, .lastRoastingPointSet://first crack, second crack, changed colour, turning point, end point
                crackTextDrawAttributes[.foregroundColor] = dataSet.colors.first
                self.bounds = CGRect(x: 0, y: 0, width: 0, height: 0)
                xText = nil
                yText = nil
                var y = entry.y
                if let temp = (entry.data as? ChartDataEntry)?.y {
                    y = temp
                }
//                if (USE_FAHRENHEIT) {
//                    y = TempToFahrenheit(temp: y)
//                }
                
                if let seconds = numberFormatter.string(from: NSNumber(value: entry.x.truncatingRemainder(dividingBy: 60))) {
                    if dataSetType == .firstCrackSet || dataSetType == .secondCrackSet || dataSetType == .colorChangeSet {
                        text = String(format: "%d:%@\n%.0f\(USE_FAHRENHEIT ? "℉" : "°C")", Int(entry.x / 60), seconds, y)
                    }  else if dataSetType == .turningPointSet || dataSetType == .lastRoastingPointSet {
                        text = String(format: "TP %.1f / %d:%@", y, Int(entry.x / 60), seconds)
                    }
                    
                }
                let textSize = (text ?? "").size(withAttributes: crackTextDrawAttributes)
                self.offset = CGPoint(x: -textSize.width / 2, y: 0)
            break

        case .rorSet://ROR
            if dataSet.isVisible {
                    pointTextDrawAttributes[.foregroundColor] = dataSet.colors.first
                    pointTextDrawAttributes[.font] = UIFont(name: kFontAvenirNextMedium, size: 12)
                    pointTextDrawAttributes[.kern] = 0.6
                    self.bounds = CGRect(x: 0, y: 0, width: 0, height: 0)
                    xText = nil
                    yText = nil
                let y = entry.y
//                    if (USE_FAHRENHEIT) {
//                        y = TempToFahrenheit(temp: y)
//                    }
                let tempC = (USE_FAHRENHEIT ? round(y - kMaxTemperatureFahrenheit / 2.0) / Double(kMaxTemperatureFahrenheit / kMaxTemperature) : round(y - kMaxTemperature / 2.0)) / kRorScale
                    
                text = String(format: "%.0f\(USE_FAHRENHEIT ? "℉" : "°C")/min", USE_FAHRENHEIT ? tempC * 1.8 : tempC)
                    let textSize = (text ?? "").size(withAttributes: pointTextDrawAttributes)
                    self.offset = CGPoint(x: 10, y: -textSize.height / 2)
            }
 
            break
            
        case .powerHeaterSet://POWER HEATER
            if dataSet.isVisible {
                pointTextDrawAttributes[.foregroundColor] = dataSet.colors.first
                pointTextDrawAttributes[.font] = UIFont(name: kFontAvenirNextMedium, size: 10)
                self.bounds = CGRect(x: 0, y: 0, width: 0, height: 0)
                xText = nil
                yText = nil
                guard let y = Int(exactly: entry.y) else {
                    return
                }
                text = "\(y)%"
                let textSize = (text ?? "").size(withAttributes: pointTextDrawAttributes)
                self.offset = CGPoint(x: 10, y: -textSize.height / 2)
            }
            
            break
            
        default:
            self.bounds = CGRect(x: 0, y: 0, width: 0, height: 0)
            text = nil
            xText = nil
            yText = nil
            break
        }
    }
    
    override func draw(context: CGContext, point: CGPoint) {
        super.draw(context: context, point: point)
        self.point = point
        
        //        if dataSetIndex == 0, let text = self.text { //timeline
        //            let offset = self.offsetForDrawing(atPoint: point)
        //            let textSize = (" \(text) " as NSString).size(withAttributes: pointTextDrawAttributes)
        //            drawText(text: " \(text) " as NSString, rect: CGRect(origin: CGPoint(x: point.x + offset.x, y: point.y + offset.y), size: textSize), withAttributes: pointTextDrawAttributes)
        //        }
        
        
        
        //image.draw(at: CGPoint(x: point.x + CGFloat(buttonMarginX), y: point.y - self.frame.size.height - CGFloat(buttonMarginX)))
        guard let dataSetType = self.dataSet?.dataSetType  else {return}
        
        switch dataSetType {
        case .editedFanProfileSet, .editedTempProfileSet:
            if (isDeletePointMode) {
                deleteButtonRect = CGRect(x: 17, y: 39, width: 24, height: 24)
                //right at point
                //image.draw(at: CGPoint(x: point.x + CGFloat(buttonMarginX), y: point.y - self.frame.size.height - CGFloat(buttonMarginX)))
                //deleteButtonRect = CGRect(x: point.x + CGFloat(buttonMarginX), y: point.y - self.frame.size.height - CGFloat(buttonMarginX), width: image.size.width, height: image.size.height)
            } else {
                deleteButtonRect = nil
            }
            if (xText != nil && yText != nil) {
                if (!isFirstPoint) {
                    let xTextSize = (" \(String(describing: xText)) " as NSString).size(withAttributes: pointTextDrawAttributes)
                    drawText(text: " \(xText!) " as NSString, rect: CGRect(origin: CGPoint(x: point.x - xTextSize.width / 2, y: xTextSize.height - 5), size: xTextSize), withAttributes: pointTextDrawAttributes)
                }
                let yTextSize = (" \(String(describing: yText)) " as NSString).size(withAttributes: pointTextDrawAttributes)
                drawText(text: " \(yText!) " as NSString, rect: CGRect(origin: CGPoint(x: -40, y: point.y + 3), size: yTextSize), withAttributes: pointTextDrawAttributes)
            }
            
            if let deleteButtonRect = deleteButtonRect, isDeletePointMode  {
                let image = UIImage.init(imageLiteralResourceName: "DeletePoint")
                image.draw(in: deleteButtonRect)
            }
            
        case .tempBelowSet:
            let offset = self.offsetForDrawing(atPoint: point)
            let textSize = (" \(text!) " as NSString).size(withAttributes: pointTextDrawAttributes)
            drawText(text: " \(text!) " as NSString, rect: CGRect(origin: CGPoint(x: point.x + offset.x, y: point.y + offset.y), size: textSize), withAttributes: pointTextDrawAttributes)
            
        case .tempAboveSet:
            let offset = self.offsetForDrawing(atPoint: point)
            let textSize = (" \(text!) " as NSString).size(withAttributes: pointTextDrawAttributes)
            drawText(text: " \(text!) " as NSString, rect: CGRect(origin: CGPoint(x: point.x + offset.x, y: point.y + offset.y), size: textSize), withAttributes: pointTextDrawAttributes)
            
        case .firstCrackSet, .secondCrackSet, .colorChangeSet, .turningPointSet, .lastRoastingPointSet:
            // 1st crack, 2nd crack, changed colour, turning point, end point
            let offset = self.offsetForDrawing(atPoint: point)
            let textSize = (" \(text!) " as NSString).size(withAttributes: crackTextDrawAttributes)
            drawText(text: " \(text!) " as NSString, rect: CGRect(origin: CGPoint(x: point.x + offset.x, y: point.y + offset.y + 0), size: textSize), withAttributes: crackTextDrawAttributes)
            var image =  UIImage()
            switch dataSetType {
            case .firstCrackSet:
                image = UIImage(named: isEditState ? "FirstCrack2_Inactive" : "FirstCrack2")!
            case .secondCrackSet:
                image = UIImage(named: isEditState ? "SecondCrack2_Inactive" : "SecondCrack2")!
            case .colorChangeSet:
                image = UIImage(named: isEditState ? "ColorChange2_Inactive" : "ColorChange2")!
            default:
                ()
            }
            
            image.draw(at: CGPoint(x: point.x + offset.x + textSize.width / 2 - image.size.width / 2, y: point.y + offset.y - image.size.height - 3))
        case .rorProfileSet:
            if let dataSet = self.dataSet, dataSet.isVisible {
                let offset = self.offsetForDrawing(atPoint: point)
                let textSize = (" \(text!) " as NSString).size(withAttributes: pointTextDrawAttributes)
                drawText(text: " \(text!) " as NSString, rect: CGRect(origin: CGPoint(x: point.x + offset.x, y: point.y + offset.y), size: textSize), withAttributes: pointTextDrawAttributes)
                
            }
            
        case .rorSet:
            if let dataSet = self.dataSet, dataSet.isVisible {
                let offset = self.offsetForDrawing(atPoint: point)
                let textSize = (" \(text!) " as NSString).size(withAttributes: pointTextDrawAttributes)
                drawText(text: " \(text!) " as NSString, rect: CGRect(origin: CGPoint(x: point.x + offset.x, y: point.y + offset.y), size: textSize), withAttributes: pointTextDrawAttributes)
                
            }
            
        case .powerHeaterSet:
            if let dataSet = self.dataSet, dataSet.isVisible {
                let offset = self.offsetForDrawing(atPoint: point)
                let textSize = (" \(text!) " as NSString).size(withAttributes: pointTextDrawAttributes)
                drawText(text: " \(text!) " as NSString, rect: CGRect(origin: CGPoint(x: point.x + offset.x, y: point.y + offset.y), size: textSize), withAttributes: pointTextDrawAttributes)
            }
        default:
            ()
            
        }
        
    }
    
    func drawText(text: NSString, rect: CGRect, withAttributes attributes: [NSAttributedString.Key : Any]? = nil) {
        let size = text.size(withAttributes: attributes)
        let centeredRect = CGRect(x: rect.origin.x + (rect.size.width - size.width) / 2.0, y: rect.origin.y + (rect.size.height - size.height) / 2.0, width: size.width, height: size.height)
        text.draw(in: centeredRect, withAttributes: attributes)
    }
    
}

