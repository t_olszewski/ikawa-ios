//
//  ChartHeaderView.swift
//  IKAWA-Pro
//
//  Created by Vitaly Shurin on 6/24/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

protocol ChartHeaderViewDelegate: AnyObject {
    func didTapMenuButton()
}

class ChartHeaderView: NibLoadingView {
    
    @IBOutlet weak var titleLabel: UILabel!
    weak var delegate: ChartHeaderViewDelegate?
    
    @IBAction func menuButtonTouchUpInside(_ sender: Any) {
        if let delegate = delegate {
            delegate.didTapMenuButton()
        }
    }
    
}
