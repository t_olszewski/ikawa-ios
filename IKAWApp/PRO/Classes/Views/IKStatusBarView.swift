//
//  IKStatusBarView.swift
//  IKAWApp
//
//  Created by Admin on 3/7/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib
import SnapKit
import SDWebImage

class IKStatusBarView: NibLoadingView {
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    //@IBOutlet weak var progressView: UIView!
    @IBOutlet weak var statusColorView: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sensorLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var favoriteImage: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    
    var backButtonAction: (()->())?
    var isAnimating = false
    static var previousState = IK_STATUS_UNKNOWN
    var currentState = IK_STATUS_UNKNOWN
    var profile: Profile?
    var canAutoUpdateProfile = false
    static var wasPreheatingDone = false
    
    private static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    
    //MARK: - Lifecicle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = kColorRoasting
        layer.masksToBounds = false
        layer.shadowOffset = CGSize(width: 0, height: 5)
        layer.shadowRadius = 5
        NotificationCenter.default.addObserver(self, selector:#selector(degreeCelsiusChanged), name: NSNotification.Name.DegreeCelsiusChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(didLogout), name: NSNotification.Name.DidLogOut, object: nil)
        RoasterManager.instance().delegates.add(self)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    @objc func degreeCelsiusChanged() {
        setupInfoLabel()
    }
    
    func setupInfoLabel() {
        guard let profile = self.profile else {
            return
        }
        favoriteImage.isHidden = !profile.favorite
        let roastingTime = profile.roastingProgressTime()
        infoLabel.text = String(format: "%@ | %@", FormatHelper.timeFormat(seconds: roastingTime), FormatHelper.tempFormat(temperature: profile.finishTemp()))
        infoLabel.letterSpace = 0.5
    }
    
    func populate(profile: Profile?) {
        let state = self.staus()
        self.profile = profile
        if (self.profile == nil) {
            self.profile = state.profile
        }
        guard let profile = self.profile else {
            return
        }
        setupInfoLabel()
        updateTitles()
        
        if self.profile?.isRoasterProfile() ?? false {
            profileNameLabel.text = profile.getNameWithType()
            profileNameLabel.letterSpace = 1
            /*profileNameLabel.attributedText = self.profile!.name.attributedString(font: UIFont(name: kFontAvenirNextLTProBold, size: 12)!, color: kColorWhite)*/
            statusLabel.text = state.status ?? ""
            //statusLabel.attributedText = state.status?.attributedString(font: UIFont(name: kFontAvenirNextLTProRegular, size: 10)!, color: kColorWhite)
            sensorLabel.text = AppCore.sharedInstance.settingsManager.roasterId()
            sensorLabel.letterSpace = 0.5
            statusColorView.backgroundColor = state.color
            updateProgressBar()
        } else {
            profileNameLabel.text = profile.getNameWithType()
            profileNameLabel.letterSpace = 1
            /*profileNameLabel.attributedText = self.profile!.name.attributedString(font: UIFont(name: kFontAvenirNextLTProBold, size: 12)!, color: kColorWhite)*/
            
            //var dateStr = ""
            if let dateLastUsed = profile.dateLastUsed() {
                var dateStr = ""
                if profile.isNeverUsed() {
                    dateStr = "$new".localized
                } else {
                    if Calendar.current.isDateInToday(dateLastUsed) || dateLastUsed.timeIntervalSinceNow > 0 {
                        dateStr = NSLocalizedString("$today", comment: "")
                    } else if Calendar.current.isDateInYesterday(dateLastUsed) {
                        dateStr = NSLocalizedString("$yesterday", comment: "")
                    } else if Date().startOfWeek! <= dateLastUsed {
                        dateStr = NSLocalizedString("$this_week", comment: "")
                    } else {
                        dateStr = IKStatusBarView.formatter.string(from: dateLastUsed)
                    }
                }

                sensorLabel.text = dateStr
                sensorLabel.letterSpace = 0.5
                //dateLastRoasted.f
                /*
                if Calendar.current.isDateInToday(dateLastRoasted) || profile.dateLastRoasted.timeIntervalSinceNow > 0 {
                    dateStr = NSLocalizedString("$today", comment: "")
                } else if Calendar.current.isDateInYesterday(dateLastRoasted) {
                    dateStr = NSLocalizedString("$yesterday", comment: "")
                } else if Date().startOfWeek! <= dateLastRoasted {
                    dateStr = NSLocalizedString("$this_week", comment: "")
                } else if profile.isNeverUsed() {
                    dateStr = "$never".localized
                } else {
                    dateStr = IKStatusBarView.formatter.string(from: dateLastRoasted)
                }*/
            }
            
            //statusLabel.attributedText = dateStr.uppercased().attributedString(font: UIFont(name: kFontAvenirNextLTProRegular, size: 10)!, color: kColorWhite)
            statusColorView.backgroundColor = kColorDarkGrey1 //UIColor.black
            //progressView.isHidden = true
        }
        updateTimerLabel()
        
    }
    
    //MARK: -
    func showBackButton(action: @escaping (()->()))  {
        backButton.isHidden = false
        thumbnail.isHidden = true
        backButtonAction = action
    }
    
    func hideBackButton() {
        backButton.isHidden = true
        thumbnail.isHidden = false
    }
    
    //MARK: - Actions
    
    @IBAction func backButtonTouchUpInside(_ sender: Any) {
        backButtonAction?()
    }
    
    //MARK: - Private
    
    func updateTimerLabel() {
        if (self.profile?.isRoasterProfile() ?? false) {
            let state = RoasterManager.instance().roaster.status.state
            if state == IK_STATUS_ROASTING {
                timeLabel.isHidden = false
                timeLabel.text = AppCore.sharedInstance.roastManager.roastTime() ?? profile?.roastTime()
            } else {
                timeLabel.isHidden = true
            }
        }
    }
    
    func updateProgressBar() {
        statusColorView.backgroundColor = self.staus().color
        let roasterState = RoasterManager.instance().roaster.status.state
        let progress = AppCore.sharedInstance.roastManager.roastProgress()
        guard roasterState == IK_STATUS_ROASTING && progress > 0 else {
            statusColorView.isHidden = false
            //serialNumber = String(IKSettingsManager.sharedInstance.roasterId())
            return
        }
        //progressView.isHidden = true//false
        /*self.progressView.snp.remakeConstraints { (make) in
            make.width.equalTo(self).multipliedBy(CGFloat(progress))
        }*/
        switch (RoasterManager.instance().roaster.status.state) {
        case IK_STATUS_ROASTING:
            statusColorView.backgroundColor = kColorOnRoaster
            //progressView.backgroundColor = kColorRoasting
            statusColorView.isHidden = false
            break
        /*case IK_STATUS_COOLING:
            statusColorView.backgroundColor = kColorRed
            progressView.backgroundColor = kColorCoolingDown
            statusColorView.isHidden = false
            break*/
        default:
            //progressView.isHidden = true
            statusColorView.isHidden = false
            break
        }
        
    }
    
    func calculateState() -> RoasterState {
        var state = RoasterManager.instance().roaster.status.state
        if (state == IK_STATUS_IDLE && (IKStatusBarView.previousState == IK_STATUS_OPEN || IKStatusBarView.previousState == IK_STATUS_READY)) {
            state = IK_STATUS_READY
        }
        return state
    }
    

    
    func updateTitles() {
        let startRoast = AppCore.sharedInstance.roastManager.currentRoast != nil && profile?.isRoasterProfile() ?? false
        thumbnail.image = thumbnail.image?.imageWithColor(color1: startRoast ? kColorWhite : kColorOnRoaster)
        let textColor = startRoast ? kColorDarkGrey2 : kColorWhite
        profileNameLabel.textColor = textColor
        infoLabel.textColor = textColor
        sensorLabel.textColor = textColor
        statusLabel.textColor = textColor
        
        if let profile = profile {
            profileNameLabel.text = profile.getNameWithType()
            profileNameLabel.letterSpace = 1
            if profile.isRoasterProfile() {
                let state = self.staus()
                statusLabel.text = state.status ?? ""
            } else {
                if profile.tempSensor == TempSensorBelow {
                    statusLabel.text = "$inlet_profile".localized
                } else {
                    statusLabel.text = "$exhaust_profile".localized
                }
            }
        }

        
    }
    
    func staus() -> (status: String?, profile: Profile?, color: UIColor?) {
        
        if ((IKInterface.instance().isBluetoothEnabled() || IK_SIMULATE_ROASTER) && RoasterManager.instance().roaster == nil) {
            return (NSLocalizedString("$not_connected_title", comment: ""), nil, kColorDoserOpen)
        }
        
        let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: RoasterManager.instance().profile?.uuid ?? "")
        
        let progress = AppCore.sharedInstance.roastManager.roastProgress()
        if ((IKInterface.instance().isBluetoothEnabled() || IK_SIMULATE_ROASTER) && RoasterManager.instance().roaster != nil) {
            let state = calculateState()
            switch (state) {
            case IK_STATUS_BUSY:
                return (NSLocalizedString("$busy_title", comment: ""), nil, kColorDoserOpen)
            case IK_STATUS_COOLING:
                IKStatusBarView.wasPreheatingDone = false
                return (NSLocalizedString("$cooling_title", comment: ""), profile, kColorDoserOpen)
            case IK_STATUS_HEATING:
                IKStatusBarView.wasPreheatingDone = true
                return (NSLocalizedString("$preheating_title", comment: ""), profile, kColorReadyToRoast)
            case IK_STATUS_IDLE:
                return (NSLocalizedString("$roast", comment: ""), profile, kColorDarkGrey1)
            case IK_STATUS_OPEN:
                if (progress > 0) {
                    return (NSLocalizedString("$doser_open_title", comment: ""), profile, kColorReadyToRoast)
                } else {
                    return (NSLocalizedString("$doser_open_title", comment: ""), nil, kColorReadyToRoast)
                }
                
            case IK_STATUS_PROBLEM:
                return (NSLocalizedString("UNEXPECTED PROBLEM", comment: ""), profile, kColorDoserOpen)
            case IK_STATUS_READY:
                return (NSLocalizedString("$ready_title", comment: ""), profile, IKStatusBarView.wasPreheatingDone ? kColorOnRoaster : kColorRed)
            case IK_STATUS_ROASTING:
                IKStatusBarView.wasPreheatingDone = false
                return (NSLocalizedString("$roasting_title", comment: ""), profile, kColorRoasting)
            case IK_STATUS_NEEDS_UPDATE:
                return (NSLocalizedString("$needs_update_title", comment: ""), nil, kColorExhaust)
            case IK_STATUS_SWAP_JARS:
                return (NSLocalizedString("$swap_jars_title", comment: ""), profile, kColorRed)
            case IK_STATUS_READY_TO_BLOWOVER:
                return (NSLocalizedString("$swap_jars_title", comment: ""), profile, kColorDoserOpen)
            case IK_STATUS_TEST_MODE:
                return (NSLocalizedString("$test_mode", comment: ""), profile, kColorDoserOpen)
            default:
                break
            }
        } else {
            return (NSLocalizedString("$bluetooth_off_title", comment: ""), profile, kColorDoserOpen)
        }
        return (nil, nil, nil)
    }
    
    @objc func didLogout() {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
}

extension IKStatusBarView : IKInterfaceDelegate {
    func peripheralDidConnect(_: IKPeripheral) {
        
    }
    
    func peripheralDidDisconnect(_ peripheral: IKPeripheral!) {
        
    }
    
    func peripheral(_ peripheral: IKPeripheral!, didReceive data: Data!) {
        
    }
    
    
}

extension IKStatusBarView : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        guard self.profile?.isRoasterProfile() ?? false else {
            return
        }
        statusLabel.text = NSLocalizedString("$connecting_title", comment: "").uppercased()
        //statusLabel.attributedText = NSLocalizedString("$connecting_title", comment: "").uppercased().attributedString(font: UIFont(name: kFontAvenirNextLTProRegular, size: 14)!, color: kColorWhite, kern: 2.4)
        statusColorView.backgroundColor = kColorDoserOpen
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        self.populate(profile: self.profile)
        
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        print(RoasterManager.instance().roaster.status.state)
        guard self.profile?.isRoasterProfile() ?? false else {
            return
        }
        updateTimerLabel()
        //self.populate(profile: self.profile)
        
        let state = calculateState()
        currentState = state
        
        if (currentState != IK_STATUS_UNKNOWN) {
            IKStatusBarView.previousState = state
        }
        
        
        if ((IKInterface.instance().isBluetoothEnabled() || IK_SIMULATE_ROASTER) && RoasterManager.instance().roaster != nil) {
            switch (state) {
           /* case IK_STATUS_HEATING:
                if !isAnimating {
                    self.statusColorView.backgroundColor = kColorRed
                    
                }
                break*/
            case IK_STATUS_READY:
                if !isAnimating {
                    self.statusColorView.layer.removeAllAnimations()
                    self.statusColorView.layer.opacity = 1
                    self.statusColorView.backgroundColor = kColorOnRoaster
                    let pulseAnimation = CABasicAnimation(keyPath: "backgroundColor")
                    pulseAnimation.fromValue = kColorOnRoaster.cgColor
                    pulseAnimation.toValue = kColorLightGrey3.cgColor
                    pulseAnimation.duration = 1.0
                    pulseAnimation.repeatCount = MAXFLOAT
                    pulseAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                    pulseAnimation.autoreverses = true
                    pulseAnimation.repeatCount = .greatestFiniteMagnitude
                    pulseAnimation.isRemovedOnCompletion = false
                    self.statusColorView.layer.add(pulseAnimation, forKey: "animateColor")
                    isAnimating = true
                }
                
                break
            case IK_STATUS_READY_TO_BLOWOVER:
                self.statusColorView.layer.removeAllAnimations()
                self.statusColorView.backgroundColor = kColorDoserOpen
                isAnimating = false
                break
            case IK_STATUS_COOLING:
                break
            default:
                self.statusColorView.layer.removeAllAnimations()
                self.statusColorView.layer.opacity = 1
                isAnimating = false
                break
            }
        }
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        
        if canAutoUpdateProfile {
            self.populate(profile: profile)
        } else {
            if self.profile != nil && self.profile!.uuid == profile.uuid {
                self.populate(profile: profile)
            }
        }
    }
    
}

