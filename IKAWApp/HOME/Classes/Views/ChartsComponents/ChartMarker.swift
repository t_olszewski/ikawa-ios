//
//  ChartMarker.swift
//  IKAWApp
//
//  Created by Admin on 4/3/18.
//  Copyright © 2018 Admin. All rights reserved.
//
import Charts
import IKRoasterLib

class ChartMarker: MarkerView {
    var dataSetIndex: Int?
    var text: String?
    var xText: String?
    var yText: String?
    var dtr: Int?
    var point: CGPoint?
    var isRemovable = false
    var deleteButtonRect: CGRect?
    let buttonMarginX = 0
    let buttonMarginY = 0
    var isFirstPoint: Bool = false
    var deleteButtonPoint = CGPoint(x: 15, y: 66)

    var pointTextDrawAttributes: [NSAttributedString.Key : Any] = {
        var drawAttr = [NSAttributedString.Key : Any]()
        
        drawAttr[.kern] = 1
        drawAttr[.font] = UIFont(name: kFontAvenirNextLTProBold, size: 11)
        drawAttr[.foregroundColor] = UIColor.white
        drawAttr[.backgroundColor] = UIColor.clear
        return drawAttr
    }()
    
    override func refreshContent(entry: ChartDataEntry, highlight: Highlight) {
        super.refreshContent(entry: entry, highlight: highlight)
        let chartView = self.chartView
        let index = highlight.dataSetIndex
        dataSetIndex = index
        isFirstPoint = chartView!.data!.dataSets[index].entryIndex(entry: entry) == 0
        pointTextDrawAttributes[.foregroundColor] = UIColor.black
        pointTextDrawAttributes[.font] = UIFont(name: kFontAvenirNextLTProBold, size: 11)
        switch dataSetIndex {
        case 0: //timeline
            self.bounds = CGRect(x: 0, y: 0, width: 0, height: 0)
            if (entry.x == 0) {
                text = nil
                xText = nil
                yText = nil
            } else {
                xText = nil
                yText = nil
                text = String(format: "%@:%@ min", timeText(from: Int(entry.x / 60)), timeText(from: Int(entry.x.truncatingRemainder(dividingBy: 60))))
            }
            let textSize = (text ?? "").size(withAttributes: pointTextDrawAttributes)
            self.offset = CGPoint(x: -textSize.width / 2, y: -textSize.height)
            dtr = nil
            isRemovable = false
            break
        case 1: //editedTempProfileSet
            pointTextDrawAttributes[.foregroundColor] = kColorRed
            text = nil
            xText = String(format: "%@:%@ min", timeText(from: Int(entry.x / 60)), timeText(from: Int(entry.x.truncatingRemainder(dividingBy: 60))))
            var y = entry.y
            if (entry.data != nil) {
                y = entry.data as! Double
                if (USE_FAHRENHEIT) {
                    y = TempToFahrenheit(temp: y)
                }
            }
            yText = String(format: "%.0f\(USE_FAHRENHEIT ? "℉" : "°C")", y)
            if (entry.isRemovable) {
                let image = UIImage.init(imageLiteralResourceName: "DeletePoint")
                self.bounds = CGRect(x: CGFloat(buttonMarginX), y: (-image.size.height - CGFloat(buttonMarginY)), width: image.size.width, height: image.size.height)
                isRemovable = true
            } else {
                isRemovable = false
            }
            dtr = nil
            
            break
            
        case 3: //fanProfileSet
            text = nil
            xText = String(format: "%@:%@ min", timeText(from: Int(entry.x / 60)), timeText(from: Int(entry.x.truncatingRemainder(dividingBy: 60))))
            yText = String(format: "%.0f%%", entry.y)
            
            if (entry.isRemovable) {
                let image = UIImage.init(imageLiteralResourceName: "DeletePoint")
                self.bounds = CGRect(x: CGFloat(buttonMarginX), y: (-image.size.height - CGFloat(buttonMarginY)), width: image.size.width, height: image.size.height)
                isRemovable = true
            } else {
                isRemovable = false
            }
            dtr = nil
            break
        case 4:
            if RoasterManager.instance().roaster != nil {
                let state = RoasterManager.instance().roaster.status.state
                pointTextDrawAttributes[.foregroundColor] = (state == IK_STATUS_COOLING ? kColorCoolingDown : kColorExhaust)
                pointTextDrawAttributes[.font] = UIFont(name: kFontAvenirNextLTProBold, size: 16)
                self.bounds = CGRect(x: 0, y: 0, width: 0, height: 0)
                xText = nil
                yText = nil
                var y = entry.y
                if (entry.data != nil) {
                    y = entry.data as! Double
                    if (USE_FAHRENHEIT) {
                        y = TempToFahrenheit(temp: y)
                    }
                }
                text = String(format: "%.0f\(USE_FAHRENHEIT ? "℉" : "°C")", y)
                dtr = nil
                let textSize = (text ?? "").size(withAttributes: pointTextDrawAttributes)
                self.offset = CGPoint(x: 10, y: -textSize.height / 2)
            }
            
            break

        default:
            self.bounds = CGRect(x: 0, y: 0, width: 0, height: 0)
            text = nil
            xText = nil
            yText = nil
            dtr = nil
            isRemovable = false
            break
        }
    }
    
    override func draw(context: CGContext, point: CGPoint) {
        self.point = point
        super.draw(context: context, point: point)
        
        if (text !=  nil) {
            
            let offset = self.offsetForDrawing(atPoint: point)
            let textSize = (" \(text!) " as NSString).size(withAttributes: pointTextDrawAttributes)
            drawText(text: " \(text!) " as NSString, rect: CGRect(origin: CGPoint(x: point.x + offset.x, y: point.y + offset.y), size: textSize), withAttributes: pointTextDrawAttributes)
        } else if (isRemovable) {
            let image = UIImage.init(imageLiteralResourceName: "DeletePoint")
            image.draw(at: CGPoint(x: deleteButtonPoint.x + CGFloat(buttonMarginX), y: deleteButtonPoint.y - self.frame.size.height - CGFloat(buttonMarginX)))
            deleteButtonRect = CGRect(x: deleteButtonPoint.x + CGFloat(buttonMarginX), y: deleteButtonPoint.y - self.frame.size.height - CGFloat(buttonMarginX), width: image.size.width, height: image.size.height)
        }
        
        
        
        if (xText != nil && yText != nil) {
            if (!isFirstPoint) {
                let xTextSize = (" \(String(describing: xText)) " as NSString).size(withAttributes: pointTextDrawAttributes)
                drawText(text: " \(xText!) " as NSString, rect: CGRect(origin: CGPoint(x: point.x-35, y: xTextSize.height + 10), size: xTextSize), withAttributes: pointTextDrawAttributes)
            }
            let yTextSize = (" \(String(describing: yText)) " as NSString).size(withAttributes: pointTextDrawAttributes)
            drawText(text: " \(yText!) " as NSString, rect: CGRect(origin: CGPoint(x: -35, y: point.y - yTextSize.height), size: yTextSize), withAttributes: pointTextDrawAttributes)
        }
        
        
        
    }
    
    func drawText(text: NSString, rect: CGRect, withAttributes attributes: [NSAttributedString.Key : Any]? = nil) {
        let size = text.size(withAttributes: attributes)
        let centeredRect = CGRect(x: rect.origin.x + (rect.size.width - size.width) / 2.0, y: rect.origin.y + (rect.size.height - size.height) / 2.0, width: size.width, height: size.height)
        text.draw(in: centeredRect, withAttributes: attributes)
    }
    
    private func timeText(from number: Int) -> String {
        return number < 10 ? "0\(number)" : "\(number)"
    }

}

