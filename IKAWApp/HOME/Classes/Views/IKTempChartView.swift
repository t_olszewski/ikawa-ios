//
//  IKTempChartView.swift
//  IKAWApp
//
//  Created by Admin on 3/27/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Charts
import IKRoasterLib

public class MinuteFormatter: NSObject, IAxisValueFormatter
{
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String
    {
         return (value == 0) ? "" : String(format: "%.0f", value / 60)
    }
}

public class TemperatureFormatter: NSObject, IAxisValueFormatter
{
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String
    {
        if (USE_FAHRENHEIT) {
           return value == 0 ? "" : String(format: "%.0f℉", value)
        }
        return value == 0 ? "" : String(format: "%.0f°C", value)
    }
}



class IKTempChartView: NibLoadingView {
    
    @IBOutlet weak var chartView: InteractiveChartView!
    var profile: Profile! {
        willSet {
            print(newValue.uuid)
        }
    }
    var editedProfile: Profile!
    var timelineSet: LineChartDataSet!
    var editedTempProfileSet: LineChartDataSet!
    var tempProfileSet: LineChartDataSet!
    var fanProfileSet: LineChartDataSet!
    var tempBelowSet: LineChartDataSet!
    var fanSet: LineChartDataSet!
    var isEditState = false
    var lastTapedPoint: CGPoint?
    var dataSets: [ChartDataSet]?
    var isWaitungAfterPointDeleting = false
    var cooldownEntryOldValue: ChartDataEntry?
    var graphWasEdited = false
    var entryWasRermoved = false
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        chartView.interactiveDelegate = self
        chartView.delegate = self

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        chartView.interactiveDelegate = self
        chartView.delegate = self
    }
    
    //MARK: - Business
    func populateChart(profile :Profile?, editedProfile: Profile?) {
        self.profile = profile
        if editedProfile == nil {
            self.editedProfile = profile
        } else {
            self.editedProfile = editedProfile
        }
        chartView.minOffset = 0
        chartView.extraTopOffset = 20
        chartView.extraBottomOffset = -20
        chartView.backgroundColor = kColorWhite
        chartView.scaleXEnabled = false
        chartView.scaleYEnabled = false
        chartView.xAxis.gridLineDashLengths = [10, 0]
        chartView.xAxis.gridLineDashPhase = 0
        chartView.xAxis.gridColor = kColorChartGride
        chartView.xAxis.gridLineWidth = 0.5
        chartView.xAxis.labelTextColor = kColorChartlabelText
        chartView.xAxis.labelFont = kAxisFont
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.valueFormatter = MinuteFormatter()

        chartView.xAxis.axisMaximum =  Double(kTimeAxisMax)
        
        chartView.xAxis.axisMinimum = 0
        chartView.xAxis.drawAxisLineEnabled = false
        chartView.xAxis.granularityEnabled = true
        chartView.xAxis.granularity = 59.999
        chartView.xAxis.labelCount = 25
        
        let leftAxis = chartView.leftAxis
        leftAxis.removeAllLimitLines()
        if (USE_FAHRENHEIT) {
            leftAxis.axisMaximum = kMaxTemperatureFahrenheit
        } else {
            leftAxis.axisMaximum = kMaxTemperature
        }
        leftAxis.axisMinimum = 0
        leftAxis.gridLineDashLengths = [5, 0]
        leftAxis.drawLimitLinesBehindDataEnabled = true
        leftAxis.gridColor = kColorChartGride
        leftAxis.gridLineWidth = 0.5
        leftAxis.labelTextColor = kColorChartlabelText
        leftAxis.labelFont = kAxisFont
        leftAxis.valueFormatter = TemperatureFormatter()
        leftAxis.drawGridLinesEnabled = true
        leftAxis.granularityEnabled = true
        leftAxis.granularity = 1
        leftAxis.yOffset = -5
        leftAxis.labelPosition = .insideChart
        
        leftAxis.granularityEnabled = true
        if (USE_FAHRENHEIT) {
            leftAxis.granularity = 100
        } else {
            leftAxis.granularity = 50
        }
        leftAxis.labelCount = 40
        
        chartView.rightAxis.enabled = false
        
        chartView.legend.form = .none
        
      //  chartView.animate(xAxisDuration: 0.1)
        chartView.chartDescription?.enabled = false
        updateChartData()
        
        let marker = ChartMarker()
        marker.chartView = chartView
        chartView.marker = marker
        updateAppearance()
    }
    
    func updateChartData() {
        if (profile == nil) {
            return
        }
        
        //time graph
        timelineSet = LineChartDataSet(values: nil, label: nil)
        timelineSet.editable = false
        timelineSet.drawIconsEnabled = false
        timelineSet.drawCirclesEnabled = false
        timelineSet.lineDashLengths = [5, 0]
        timelineSet.highlightLineDashLengths = [5, 5]
        timelineSet.setColor(kColorInlet)
        timelineSet.lineWidth = 3
        timelineSet.drawCircleHoleEnabled = false
        timelineSet.drawValuesEnabled = false
        timelineSet.valueTextColor = kColorChartGride
       // timelineSet.valueFont = .systemFont(ofSize: 9)
        timelineSet.visible = false
        timelineSet.setDrawHighlightIndicators(false)
        
        
        // temperature graph
        
        let profileTempValues = (profile!.roastPoints as! [RoastTempPoint]).map { (point) -> ChartDataEntry in
            ChartDataEntry(time: Double(point.time), temperature: Double(point.temperature))
        }
        
            let color = kColorInlet
        
        
        tempProfileSet = LineChartDataSet(values: profileTempValues, label: nil)
        
        tempProfileSet.editable = false
        tempProfileSet.drawIconsEnabled = false
        tempProfileSet.lineDashLengths = [5, 0]
        tempProfileSet.highlightLineDashLengths = [5, 5]
        
        tempProfileSet.highlightLineWidth = 2
        tempProfileSet.highlightColor = color
        tempProfileSet.setDrawHighlightIndicators(true)
        
        tempProfileSet.setColor(UIColor.clear)
        tempProfileSet.setCircleColor(UIColor.clear)
        tempProfileSet.lineWidth = 0
        tempProfileSet.circleRadius = 0
        tempProfileSet.drawCircleHoleEnabled = false
        tempProfileSet.circleHoleRadius = 0
        tempProfileSet.circleHoleColor = kColorWhite
        tempProfileSet.drawValuesEnabled = false
        tempProfileSet.valueTextColor = kColorChartGride
        tempProfileSet.fillColor = kColorInlet
        tempProfileSet.fillAlpha = 0.15
        tempProfileSet.drawFilledEnabled = true
        
        let editedProfileTempValues = (editedProfile!.roastPoints as! [RoastTempPoint]).map { (point) -> ChartDataEntry in
            ChartDataEntry(time: Double(point.time), temperature: Double(point.temperature))
        }
        
        if (editedProfileTempValues.count > 1) {
            editedProfileTempValues.first?.isRemovable = false
            editedProfileTempValues.last?.isRemovable = false
        }
        
        editedTempProfileSet = LineChartDataSet(values: editedProfileTempValues, label: nil)
        
        if let firstEntry = editedProfileTempValues.first {
            editedTempProfileSet.circleIndexesNotToDraw = [editedTempProfileSet.entryIndex(entry: firstEntry)]
            chartView.notEditablePoints = [firstEntry]
        }
        
        editedTempProfileSet.editable = false
        editedTempProfileSet.drawIconsEnabled = false
        editedTempProfileSet.lineDashLengths = [5, 0]
        editedTempProfileSet.highlightLineDashLengths = [5, 5]
        
        editedTempProfileSet.highlightLineWidth = 2
        editedTempProfileSet.highlightColor = color
        editedTempProfileSet.setDrawHighlightIndicators(true)
        
        editedTempProfileSet.setColor(color)
        editedTempProfileSet.setCircleColor(color)
        editedTempProfileSet.lineWidth = 3
        editedTempProfileSet.circleRadius = 4
        editedTempProfileSet.drawCircleHoleEnabled = false
        editedTempProfileSet.circleHoleRadius = 3
        editedTempProfileSet.circleHoleColor = kColorWhite
        editedTempProfileSet.drawValuesEnabled = false
        editedTempProfileSet.valueTextColor = kColorChartGride
        
        
        // cooling graph
        
        let coolingValues = (profile!.fanPoints as! [RoastFanPoint]).map { (point) -> ChartDataEntry in
            return ChartDataEntry(x: Double(point.time), y: Double(point.power))
        }
        
        fanProfileSet = LineChartDataSet(values: coolingValues, label: nil)
        
        if  let cooldownPoint = profile!.cooldownPoint {
            let lastFanPoint = coolingValues.last
            cooldownEntryOldValue = lastFanPoint?.copy() as? ChartDataEntry
            lastFanPoint?.isRemovable = false
            let dataEntry = ChartDataEntry(x: Double(lastFanPoint!.x + 20), y: Double(cooldownPoint.power))
            _ = fanProfileSet.addEntryOrdered(dataEntry)
            fanProfileSet.circleIndexesNotToDraw = [fanProfileSet.entryIndex(entry: dataEntry)]
            dataEntry.isRemovable = false
            if (chartView.notEditablePoints != nil) {
                chartView.notEditablePoints?.append(dataEntry)
            } else {
                chartView.notEditablePoints = [dataEntry]
            }
            
            _ = fanProfileSet.addEntryOrdered(ChartDataEntry(x: Double(cooldownPoint.time), y: Double(cooldownPoint.power)))
            fanProfileSet.values.first?.isRemovable = false
            fanProfileSet.values.last?.isRemovable = false
        }
        
        
        
        fanProfileSet.editable = false
        fanProfileSet.drawIconsEnabled = false
        
        fanProfileSet.lineDashLengths = [5, 0]
        fanProfileSet.highlightLineDashLengths = [5, 5]
        
        fanProfileSet.highlightLineWidth = 2
        fanProfileSet.highlightColor = kColorFanChart
        fanProfileSet.setDrawHighlightIndicators(true)
        
        fanProfileSet.setColor(kColorFanChart)
        fanProfileSet.setCircleColor(kColorFanChart)
        fanProfileSet.lineWidth = 3
        fanProfileSet.circleRadius = 4
        fanProfileSet.drawCircleHoleEnabled = false
        fanProfileSet.circleHoleRadius = 3
        fanProfileSet.circleHoleColor = kColorWhite
        fanProfileSet.drawValuesEnabled = false
        fanProfileSet.valueTextColor = kColorChartGride
        fanProfileSet.valueFont = .systemFont(ofSize: 9)

        
        // temperature graph below
        let tempBelowValues = [ChartDataEntry]()
        
        tempBelowSet = LineChartDataSet(values: tempBelowValues, label: nil)
        tempBelowSet.editable = false
        tempBelowSet.drawIconsEnabled = false
        tempBelowSet.drawCirclesEnabled = true
        tempBelowSet.lineDashLengths = [5, 0]
        tempBelowSet.highlightLineDashLengths = [5, 5]
        tempBelowSet.setColor(kColorInlet)
        tempBelowSet.lineWidth = 2
        tempBelowSet.drawCircleHoleEnabled = false
        tempBelowSet.setCircleColor(kColorInlet)
        tempBelowSet.circleRadius = 7
        tempBelowSet.drawValuesEnabled = false
        tempBelowSet.valueTextColor = kColorChartGride
        tempBelowSet.valueFont = .systemFont(ofSize: 9)
        tempBelowSet.setDrawHighlightIndicators(false)


        // temperature graph below
        let fanValues = [ChartDataEntry]()
        
        fanSet = LineChartDataSet(values: fanValues, label: nil)
        fanSet.editable = false
        fanSet.drawIconsEnabled = false
        fanSet.drawCirclesEnabled = true
        fanSet.lineDashLengths = [5, 0]
        fanSet.highlightLineDashLengths = [5, 5]
        fanSet.setColor(kColorInlet)
        fanSet.lineWidth = 2
        fanSet.drawCircleHoleEnabled = false
        fanSet.setCircleColor(kColorGrey)
        fanSet.circleRadius = 0
        fanSet.drawValuesEnabled = false
        fanSet.valueTextColor = kColorChartGride
        fanSet.valueFont = .systemFont(ofSize: 9)
        fanSet.setDrawHighlightIndicators(false)

        


        dataSets = [timelineSet, editedTempProfileSet, tempProfileSet, fanProfileSet, tempBelowSet, fanSet]
        let data = LineChartData(dataSets: dataSets)
        
        chartView.data = data
        

        
    }
    
    func removeHighlightForEditableDatasets() {
        editedTempProfileSet.selectedEntry = nil
        fanProfileSet.selectedEntry = nil
        let highlights = chartView.highlighted.filter { (highlight) -> Bool in
            return highlight.dataSetIndex != dataSets?.firstIndex(of: editedTempProfileSet)! && highlight.dataSetIndex != dataSets?.firstIndex(of: fanProfileSet)!
        }
        chartView.highlightValues(highlights)
    }
    
    func populateProfileFromChartValues() {
        editedProfile.roastPoints = editedTempProfileSet.values.map{
            let value = RoastTempPoint()
            value.time = Float($0.x)
            value.temperature = Float($0.temperature)
            return value
        }
        
        var fanPoints = [RoastFanPoint]()
        for index in 0...fanProfileSet.entryCount - 3 {
            let point = RoastFanPoint()
            let chartEntry = fanProfileSet.values[index]
            point.time = Float(chartEntry.x)
            point.power = Float(chartEntry.y)
            fanPoints.append(point)
        }
        
        editedProfile.fanPoints = fanPoints
        let cooldownPoint = RoastFanPoint()
        cooldownPoint.time = Float(fanProfileSet.values.last!.x)
        cooldownPoint.power = Float(fanProfileSet.values.last!.y)
        editedProfile.cooldownPoint = cooldownPoint
    }
    
    func checkPoints() {
        
        // checking min values
        if (editedTempProfileSet.entryCount <= IK_MIN_ROAST_POINTS) {
            _ = editedTempProfileSet.values.map{$0.isRemovable = false}
        } else {
            _ = editedTempProfileSet.values.map{$0.isRemovable = true}
            editedTempProfileSet.values.first?.isRemovable = false
            editedTempProfileSet.values.last?.isRemovable = false
        }
        
        if ((fanProfileSet.entryCount - 2) <= IK_MIN_FAN_POINTS) {
            _ = fanProfileSet.values.map{$0.isRemovable = false}
        } else {
            _ = fanProfileSet.values.map{$0.isRemovable = true}
            fanProfileSet.values.first?.isRemovable = false
            fanProfileSet.values.last?.isRemovable = false
            fanProfileSet.values[fanProfileSet.entryCount - 2].isRemovable = false
            fanProfileSet.values[fanProfileSet.entryCount - 3].isRemovable = false
        }
        
    }
    
}



extension IKTempChartView: ChartViewProtocol {
    
    func updateAppearance() {
        if (isEditState) {
            goToEditState()
        } else {
            goToViewState()
        }
    }
    
    func goToEditState() {
        isEditState = true
        editedTempProfileSet.drawCircleHoleEnabled = true
        editedTempProfileSet.circleRadius = 8
        editedTempProfileSet.circleHoleRadius = 5
        editedTempProfileSet.editable = true
        
        fanProfileSet.drawCircleHoleEnabled = true
        fanProfileSet.circleRadius = 8
        fanProfileSet.circleHoleRadius = 5
        fanProfileSet.editable = true
        chartView.xAxis.axisMaximum =  Double(kTimeAxisMax)
        chartView.setVisibleXRangeMaximum(Double(kTimeAxisMin))
    }
    
    func goToViewState() {
        graphWasEdited = false
        isEditState = false
        editedTempProfileSet.drawCircleHoleEnabled = false
        editedTempProfileSet.circleRadius = 0
        editedTempProfileSet.editable = false
        
        
        fanProfileSet.drawCircleHoleEnabled = false
        fanProfileSet.circleRadius = 0
        fanProfileSet.editable = false
        
        chartView.xAxis.axisMaximum =  Double(kTimeAxisMax)
        chartView.setVisibleXRangeMaximum(Double(kTimeAxisMin))
    }
    
    func viewWillTransition() {
        updateAppearance()
    }
    
    func updateTempPoint(roast: IKRoast?) {
        guard let roaster = RoasterManager.instance().roaster, let lastPoint = roast?.roastPoints.last, profile.isRoasterProfile(), AppCore.sharedInstance.roastManager.isRoasting() else {
            return
        }
        let state = roaster.status.state
//        guard RoasterManager.instance().roaster != nil && profile.isRoasterProfile() && roast != nil && (state == IK_STATUS_HEATING || state == IK_STATUS_ROASTING || state == IK_STATUS_COOLING) else {
//            return
//        }
        
        
        if (!chartView.userWasInteract && (state == IK_STATUS_HEATING || state == IK_STATUS_ROASTING || state == IK_STATUS_COOLING) && AppCore.sharedInstance.roastManager.roastAndCoolDownProgress() > 0 && lastPoint.time > Float(kTimeAxisMin / 2)) {
            let delta = Double(lastPoint.time - Float(kTimeAxisMin / 2))
            chartView.moveViewToX(delta)
        }
        
        
            if (lastPoint.temp_below > 0) {
                tempBelowSet.clear()
                tempBelowSet.setCircleColor(state == IK_STATUS_COOLING ? kColorCoolingDown : kColorInlet)
                 if (state == IK_STATUS_HEATING || state == IK_STATUS_ROASTING || state == IK_STATUS_COOLING) {
                     _ = tempBelowSet.addEntryOrdered(ChartDataEntry(time: Double(lastPoint.time), temperature: Double(lastPoint.temp_below)))
                }
               
            }
        
        if (lastPoint.fanSet > 0) {
            fanSet.clear()
            if (state == IK_STATUS_HEATING || state == IK_STATUS_ROASTING || state == IK_STATUS_COOLING) {
                _ = fanSet.addEntryOrdered(ChartDataEntry(time: Double(lastPoint.time), temperature: Double(lastPoint.fanSet)))
            }
            
        }
        
        chartView.data?.notifyDataChanged()
        chartView.notifyDataSetChanged()
        var highlights = chartView.highlighted.filter { (highlight) -> Bool in
            return highlight.dataSetIndex == dataSets?.firstIndex(of: editedTempProfileSet)! || highlight.dataSetIndex == dataSets?.firstIndex(of: fanProfileSet)! || highlight.dataSetIndex == dataSets?.firstIndex(of: timelineSet)!/* || highlight.dataSetIndex == dataSets?.firstIndex(of: tempBelowSet)!*/ // temp, time
        }
        
        if (state == IK_STATUS_HEATING || state == IK_STATUS_ROASTING || state == IK_STATUS_COOLING) {
            if let lastBelowEntry = tempBelowSet.entryForIndex(tempBelowSet.entryCount - 1) {
                let tempPointAbove = Highlight(x: lastBelowEntry.x, y: lastBelowEntry.y , dataSetIndex: dataSets!.firstIndex(of: tempBelowSet)!)
                highlights.append(tempPointAbove)
            }
        }
        
        if (state == IK_STATUS_HEATING || state == IK_STATUS_ROASTING || state == IK_STATUS_COOLING) {
            if let lastFanEntry = fanSet.entryForIndex(fanSet.entryCount - 1) {
                let tempPointFan = Highlight(x: lastFanEntry.x, y: lastFanEntry.y , dataSetIndex: dataSets!.firstIndex(of: fanSet)!)
                highlights.append(tempPointFan)
            }
        }

        chartView.highlightValues(highlights)
        
    }
}

extension IKTempChartView: InteractiveChartViewDelegate {


    
    func chartValueMoved(_ chartView: ChartViewBase, entry: ChartDataEntry, touchFinished: Bool) {

        if (editedTempProfileSet.contains(entry)) {
            editedTempProfileSet.drawVerticalHighlightIndicatorEnabled = editedTempProfileSet.entryIndex(entry: entry) != 0
        } else if (fanProfileSet.contains(entry)) {
            fanProfileSet.drawVerticalHighlightIndicatorEnabled = fanProfileSet.entryIndex(entry: entry) != 0
        }

        if (fanProfileSet.contains(entry)) {
            fanProfileSet.selectedEntry = entry
            editedTempProfileSet.selectedEntry = nil
            correctFanGraph(entry: entry)
            correctTempGraph(entry: editedTempProfileSet.values.last!)
        } else if (editedTempProfileSet.contains(entry)) {
            fanProfileSet.selectedEntry = nil
            editedTempProfileSet.selectedEntry = entry
            correctTempGraph(entry: entry)
            correctFanGraph(entry: fanProfileSet.values[fanProfileSet.values.count - 3])
        }
        cooldownEntryOldValue = editedTempProfileSet.values.last!.copy() as? ChartDataEntry

        populateProfileFromChartValues()
        
        chartView.data?.notifyDataChanged()
        chartView.notifyDataSetChanged()
        graphWasEdited = true
        updateTempPoint(roast: AppCore.sharedInstance.roastManager.currentRoast)
    }
    
    func correctTempGraph(entry: ChartDataEntry) {
        
        //checking temp chart rules
        
        //first point
        editedTempProfileSet.values.first?.x = 0
        
        //check value ranges
        
        
        for item in editedTempProfileSet.values {
            
            let selectedItemIndex = editedTempProfileSet.values.index(of: entry)!
            let itemIndex = editedTempProfileSet.values.index(of: item)!

            if (item.x > Double(kTimeAxisMin)) {
                item.x = Double(kTimeAxisMin)
            }
            
            if (itemIndex < selectedItemIndex) {
                if (selectedItemIndex == editedTempProfileSet.values.count - 1) {
                    if (item.x > entry.x) {
                        item.x = entry.x
                    }
                } else if (selectedItemIndex == 1) {
                    if (item.x > entry.x) {
                        item.x = entry.x
                    }
                } else {
                    if (item.x > entry.x) {
                        editedTempProfileSet.values[selectedItemIndex] = item
                        editedTempProfileSet.values[itemIndex] = entry
                        removeHighlightForEditableDatasets()
                        self.chartView.setSelectedDataEntry(setIndex: dataSets!.firstIndex(of: editedTempProfileSet)!, dataSet: editedTempProfileSet, index: itemIndex, entry: item)
                    }

                }

            } else {
                if (selectedItemIndex == editedTempProfileSet.values.count - 2) {
                    if (item.x < entry.x) {
                        item.x = entry.x
                    }
                } else {
                    if (item.x < entry.x) {
                        editedTempProfileSet.values[selectedItemIndex] = item
                        editedTempProfileSet.values[itemIndex] = entry
                        removeHighlightForEditableDatasets()
                        self.chartView.setSelectedDataEntry(setIndex: dataSets!.firstIndex(of: editedTempProfileSet)!, dataSet: editedTempProfileSet, index: itemIndex, entry: item)
                    }
                }

            }
            
        }
        
        for dataEntry in editedTempProfileSet.values {
            dataEntry.temperature = max(kMinTemperature, Double(dataEntry.temperature))
            dataEntry.temperature = min(Double(IK_MAX_TEMP_BELOW), Double(dataEntry.temperature))
            dataEntry.x = max(0, dataEntry.x)
            dataEntry.x = min(Double(kTimeAxisMax), dataEntry.x)
        }
        fanProfileSet.values[fanProfileSet.values.count - 3].x =  editedTempProfileSet.values.last!.x
    }
    
    func correctFanGraph(entry: ChartDataEntry /*isSelected: Bool*/) {
        let cooldownEntry = fanProfileSet.values[fanProfileSet.values.count - 3]
        //checking fan chart rules
        
        // first point
        fanProfileSet.values.first?.x = 0
        
        //check value ranges
        let selectedItemIndex = fanProfileSet.values.index(of: entry)!
        if (selectedItemIndex < (fanProfileSet.entryCount - 1)) {
            if (entry.x >  Double(kTimeAxisMin)) {
                entry.x = Double(kTimeAxisMin)
            }
        }
        
        if (Int(fanProfileSet.values.last!.x - cooldownEntry.x) > IK_MAX_COOLDOWN_TIME) {
            fanProfileSet.values.last!.x = cooldownEntry.x + Double(IK_MAX_COOLDOWN_TIME)
        }

        for item in fanProfileSet.values {
            let selectedItemIndex = fanProfileSet.values.index(of: entry)!
            let itemIndex = fanProfileSet.values.index(of: item)!

            if (selectedItemIndex == (fanProfileSet.entryCount - 1)) {
                if ((fanProfileSet.values[fanProfileSet.values.count - 1].x - fanProfileSet.values[fanProfileSet.values.count - 2].x) < (60 - 20)) {
                    entry.x = fanProfileSet.values[fanProfileSet.values.count - 2].x + (60 - 20)
                }
            }
            

            
            fanProfileSet.values[fanProfileSet.values.count - 2].x =  fanProfileSet.values[fanProfileSet.values.count - 3].x + 20
            fanProfileSet.values[fanProfileSet.values.count - 2].y =  fanProfileSet.values[fanProfileSet.values.count - 1].y

            editedTempProfileSet.values.last!.x =  fanProfileSet.values[fanProfileSet.values.count - 3].x
            
            if (itemIndex < selectedItemIndex) {
                
                
                
                if (item.x > entry.x) {
                    print("itemIndex < selectedItemIndex")
                    //item.x = entry.x
                    fanProfileSet.values[selectedItemIndex] = item
                    fanProfileSet.values[itemIndex] = entry
                    removeHighlightForEditableDatasets()
                    //self.chartView.setSelectedDataEntry(setIndex: 2, dataSet: fanProfileSet, index: itemIndex, entry: item)
                }
            } else if (itemIndex > selectedItemIndex) {
                if (item.x < entry.x) {
                    print("itemIndex > selectedItemIndex")
                    //item.x = entry.x
                    fanProfileSet.values[selectedItemIndex] = item
                    fanProfileSet.values[itemIndex] = entry
                    removeHighlightForEditableDatasets()
                   // self.chartView.setSelectedDataEntry(setIndex: 2, dataSet: fanProfileSet, index: itemIndex, entry: item)
                }
            }
        }

        let delta = fanProfileSet.values[fanProfileSet.values.count - 3].x - cooldownEntryOldValue!.x
            fanProfileSet.values[fanProfileSet.values.count - 1].x = min(Double(kTimeAxisMax), fanProfileSet.values[fanProfileSet.values.count - 1].x + delta)
        
        for dataEntry in fanProfileSet.values {
            dataEntry.x = max(0, dataEntry.x)
            dataEntry.x = min(Double(kTimeAxisMax), dataEntry.x)
            dataEntry.y = max(IK_MIN_FAN_SPEED, Double(dataEntry.y))
            dataEntry.y = min(IK_MAX_FAN_SPEED, max(IK_MIN_FAN_SPEED, Double(dataEntry.y)))
        }
    }
    

    
    func interactiveTapGestureRecognized(_ recognizer: NSUITapGestureRecognizer) {
        lastTapedPoint = recognizer.location(ofTouch: 0, in: self)
        if (isEditState) {
            if let chartViewMarker = chartView.marker as? ChartMarker {
                if let selectedData = chartView.selectedData {
                    
                    if let deleteButtonRect = chartViewMarker.deleteButtonRect {
                        if (deleteButtonRect.contains(lastTapedPoint!)) {
                            print("touch !!!!!")
                            
                            if (editedTempProfileSet == selectedData.set as? LineChartDataSet) {
                                let index = editedTempProfileSet.values.index(of: selectedData.entry)!
                                editedProfile.roastPoints.remove(at: index)
                                
                            }
                            
                            if (fanProfileSet == selectedData.set as? LineChartDataSet) {
                                let index = fanProfileSet.values.index(of: selectedData.entry)!
                                editedProfile.fanPoints.remove(at: index)
                            }
                            
                            _ = selectedData.set.removeEntry(selectedData.entry)
                            entryWasRermoved = true
                            fanProfileSet.circleIndexesNotToDraw = [fanProfileSet.entryCount - 2]
                            chartView.data?.notifyDataChanged()
                            chartView.notifyDataSetChanged()
                            self.removeHighlightForEditableDatasets()
                            isWaitungAfterPointDeleting = true
                            
                            checkPoints()

                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                self.isWaitungAfterPointDeleting = false
                            }
                        }
                    }
                }
            }

        }
    }
    
    func didTap(oldDataEntry: ChartDataEntry?, dataEntry: ChartDataEntry?, oldDataSet: ChartDataSet?, dataset: ChartDataSet?) {
        if dataEntry != nil {
            removeHighlightForEditableDatasets()
            if editedTempProfileSet.contains(dataEntry!) {
                if editedTempProfileSet.circleIndexesNotToDraw.contains(editedTempProfileSet.entryIndex(entry: dataEntry!)) {
                    return
                }
                fanProfileSet.selectedEntry = nil
                editedTempProfileSet.selectedEntry = dataEntry
                let higlight = Highlight(x: dataEntry!.x, y: dataEntry!.y, dataSetIndex: (dataSets?.firstIndex(of: editedTempProfileSet))!)
                chartView.highlightValue(higlight)
            }
            if fanProfileSet.contains(dataEntry!) {
                if fanProfileSet.circleIndexesNotToDraw.contains(fanProfileSet.entryIndex(entry: dataEntry!)) {
                    return
                }
                fanProfileSet.selectedEntry = dataEntry
                editedTempProfileSet.selectedEntry = nil
                let higlight = Highlight(x: dataEntry!.x, y: dataEntry!.y, dataSetIndex: (dataSets?.firstIndex(of: fanProfileSet))!)
                chartView.highlightValue(higlight)
            }
            
        }
        
        
        if dataset == nil && dataEntry == nil && !entryWasRermoved { //empty tap
            removeHighlightForEditableDatasets()
        } else if let chartDataEntry = dataEntry {
            if (editedTempProfileSet.contains(chartDataEntry)) {
                editedTempProfileSet.drawVerticalHighlightIndicatorEnabled = editedTempProfileSet.entryIndex(entry: chartDataEntry) != 0
            } else if (fanProfileSet.contains(chartDataEntry)) {
                fanProfileSet.drawVerticalHighlightIndicatorEnabled = fanProfileSet.entryIndex(entry: chartDataEntry) != 0
            }
        }
        if dataset != nil {
            chartDataSetSelected(dataSet: dataset)
        }
        entryWasRermoved = false
    }
    
    
    func chartDataSetSelected(dataSet: ChartDataSet?) {
        guard dataSet != nil && !isWaitungAfterPointDeleting else {
            if (dataSet != nil) {
                self.removeHighlightForEditableDatasets()
            }
            return
        }
        
        
        if (editedTempProfileSet == dataSet || fanProfileSet == dataSet) {
            print("ProfileSet")
            let p = chartView.valueForTouchPoint(point: lastTapedPoint!, axis: editedTempProfileSet.axisDependency)
            if let dataEntry = chartView.getEntryByTouchPoint(point: lastTapedPoint!) {
               let distance = sqrt(pow(dataEntry.x - Double(p.x), 2) + pow(dataEntry.y - Double(p.y), 2))
                if (distance < 20) {
                    print("same point")
                    return
                }
            }
            
            
            if (fanProfileSet == dataSet) {
                guard (dataSet?.entryCount)! >= 4 else {
                    removeHighlightForEditableDatasets()
                    return
                }
                
                if let cooldownPoint = dataSet?.values[(dataSet?.entryCount)! - 3] {
                    if (Double(cooldownPoint.x) <= Double(p.x)) {
                        removeHighlightForEditableDatasets()
                        return
                    }
                }
            }
            
            if (editedTempProfileSet == dataSet && editedTempProfileSet.entryCount >= IK_MAX_ROAST_POINTS) {
                return
            } else if (fanProfileSet == dataSet && (fanProfileSet.entryCount - 2) >= IK_MAX_FAN_POINTS) {
                return
            }
            
            if (editedTempProfileSet == dataSet) {
                
                let point = RoastTempPoint()
                point.time = Float(p.x)
                point.temperature = Float(p.y)
                var roastPoints = editedProfile.roastPoints as! [RoastTempPoint]
                roastPoints.append(point)
                editedProfile.roastPoints = roastPoints.sorted{$0.time < $1.time}
                graphWasEdited = true
            }
            
            if (fanProfileSet == dataSet) {
                let point = RoastFanPoint()
                point.time = Float(p.x)
                point.power = Float(p.y)
                var fanPoints = editedProfile.fanPoints as! [RoastFanPoint]
                fanPoints.append(point)
                editedProfile.fanPoints = fanPoints.sorted{$0.time < $1.time}
                graphWasEdited = true
            }
            let entry = ChartDataEntry(x: Double(p.x), y: Double(p.y))
            _ = dataSet!.addEntryOrdered(entry)
            fanProfileSet.circleIndexesNotToDraw = [fanProfileSet.entryCount - 2]
            chartView.data?.notifyDataChanged()
            chartView.notifyDataSetChanged()
            self.removeHighlightForEditableDatasets()
             if (editedTempProfileSet == dataSet) {
                let itemIndex = editedTempProfileSet.values.index(of: entry)!
                self.chartView.setSelectedDataEntry(setIndex: 1, dataSet: editedTempProfileSet, index: itemIndex, entry: entry)
                editedTempProfileSet.selectedEntry = entry
             } else if (fanProfileSet == dataSet) {
                let itemIndex = fanProfileSet.values.index(of: entry)!
                self.chartView.setSelectedDataEntry(setIndex: 2, dataSet: fanProfileSet, index: itemIndex, entry: entry)
                fanProfileSet.selectedEntry = entry
            }
            
            
           
            let highlightPoint = Highlight(x: Double(p.x), y: Double(p.y) , dataSetIndex: dataSets!.index(of: dataSet!)!)
            var highlights = chartView.highlighted
            highlights.append(highlightPoint)
            chartView.highlightValues(highlights)
            checkPoints()
            
        }
    }
    
}

extension IKTempChartView: ChartViewDelegate {
    func chartScaled(_ chartView: ChartViewBase, scaleX: CGFloat, scaleY: CGFloat) {
        let isLandscape = UIDevice.current.orientation.isFlat ? UIApplication.shared.statusBarOrientation.isLandscape : UIDevice.current.orientation.isLandscape
        if isLandscape {
            self.chartView.moveViewToX(0)
        }
    }
}
