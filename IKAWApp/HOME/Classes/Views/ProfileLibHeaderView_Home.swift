//
//  ProfileLibHeaderView.swift
//  IKAWA-Home
//
//  Created by Admin on 10/15/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SnapKit
import IKRoasterLib



protocol ProfileLibHeaderViewDelegate: AnyObject {
    func didTapMenuButton()
    func search(text:String)
    func layoutHeaderViewDidChanged()
    func tapOnPrfile(profile: Profile)
}

class ProfileLibHeaderView_Home: NibLoadingView {
    
    fileprivate struct Constants {
        static let headerViewNavBarHeight: CGFloat = 56
        static let headerViewSearchBarHeight: CGFloat = 56
        static let headerViewStatusViewHeight: CGFloat = 72
    }
    
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var statusBarView: IKStatusBarView_Home!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cancelSearchButton: UIButton!
    
    weak var delegate: ProfileLibHeaderViewDelegate?
    var searchStr = ""

    
    override func awakeFromNib() {
        super.awakeFromNib()
        searchBar.delegate = self
        searchBar.placeholder = NSLocalizedString("$search", comment: "")
        searchBar.setImage(UIImage(), for: .clear, state: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(tapOnProfileView(_:)), name: NSNotification.Name.OpenRoasterReceipt, object: nil)
        updateStatusBarView()
        hideSearch()
        NotificationCenter.default.addObserver(self, selector:#selector(didLogout), name: NSNotification.Name.DidLogOut, object: nil)
        RoasterManager.instance().delegates.add(self)
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Actions
    
    @IBAction func menuButtonTouchUpInside(_ sender: Any) {
        if let delegate = delegate {
            delegate.didTapMenuButton()
        }
    }
    
    @IBAction func searchButtonTouchUpInside(_ sender: Any) {
        updateStatusBarView()
        showSearch()
    }
    
    @IBAction func tapOnProfileView(_ sender: Any) {
        if let delegate = delegate {
            if let profile = RoasterManager.instance().profile {
                delegate.tapOnPrfile(profile: profile)
                NavigationHelper.highlightRoastTab()
            }
            
        }
    }
    
    @IBAction func cancelSearchButtonTouchUpInside(_ sender: Any) {
        searchBar.text = ""
        resetSearch()
        hideSearch()
    }
    
    
    //MARK: - Private
    
    @objc func didLogout() {
         NotificationCenter.default.removeObserver(self)
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    func resetSearch() {
        searchStr = ""
        if let delegate = delegate {
            delegate.search(text: "")
        }
    }
    
    func updateStatusBarView() {
        var needShowStatusBarView = false
        if IK_SIMULATE_ROASTER {
            needShowStatusBarView = true
        } else if (IKInterface.instance().isConnected() || IK_SIMULATE_ROASTER) && statusBarView.profile?.isRoasterProfile() ?? false {
            needShowStatusBarView = true
        }
        
        if needShowStatusBarView {
            showStatusBar()
        } else {
            hideStatusBar()
        }
    }
    
    func hideStatusBar() {
        if (!statusBarView.isHidden) {
            statusBarView.isHidden = true
            statusBarView.snp.remakeConstraints { (make) in
                make.height.equalTo(0)
            }
            if let delegate = delegate {
                delegate.layoutHeaderViewDidChanged()
            }

        }
    }
    
    func showStatusBar() {
        
        if statusBarView.isHidden, let delegate = delegate, let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: RoasterManager.instance().profile?.uuid ?? "") {
            statusBarView.populate(profile: profile)
            statusBarView.snp.remakeConstraints { (make) in
                make.height.equalTo(72)
            }
            statusBarView.isHidden = false
            delegate.layoutHeaderViewDidChanged()

        }
    }
    
    func hideSearch() {
        if (!searchContainerView.isHidden) {
            searchContainerView.isHidden = true
            searchContainerView.snp.remakeConstraints { (make) in
                make.height.equalTo(0)
            }
            if let delegate = delegate {
                delegate.layoutHeaderViewDidChanged()
            }
            
        }
        searchBar.resignFirstResponder()
        searchButton.isHidden = false
    }
    
    func showSearch() {
        if searchContainerView.isHidden, let delegate = delegate {
            searchContainerView.snp.remakeConstraints { (make) in
                make.height.equalTo(56)
            }
            searchContainerView.isHidden = false
            delegate.layoutHeaderViewDidChanged()
            searchButton.isHidden = true
            
        }
    }
    
    //MARK: - Public
    func minHeight() -> CGFloat {
        return maxHeight()
//        return (searchContainerView.isHidden ? 0 : Constants.headerViewSearchBarHeight) +
//            (statusBarView.isHidden ? 0 : Constants.headerViewStatusViewHeight)
    }
    
    func maxHeight() -> CGFloat {
        return Constants.headerViewNavBarHeight +
        (searchContainerView.isHidden ? 0 : Constants.headerViewSearchBarHeight) +
        (statusBarView.isHidden ? 0 : Constants.headerViewStatusViewHeight)
    }
}

extension ProfileLibHeaderView_Home: RoasterManagerDelegate {
    //MARK: - RoasterManagerDelegate
    func roasterDidConnect(_ roaster: Roaster!) {
        updateStatusBarView()
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        resetSearch()
        updateStatusBarView()
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        if let profile = RoasterManager.instance().profile, statusBarView.profile?.uuid != profile.uuid  {
            if let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: profile.uuid) {
                statusBarView.populate(profile: profile)
            }
        }
        updateStatusBarView()
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        if let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: RoasterManager.instance().profile?.uuid ?? "") {
            statusBarView.populate(profile: profile)
        }
        updateStatusBarView()
    }
    
}

extension ProfileLibHeaderView_Home: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            searchBar.endEditing(true)
            return false
        }
        searchStr = NSString(string: searchBar.text!).replacingCharacters(in: range, with: text)
        updateStatusBarView()
        
        if let delegate = delegate {
            delegate.search(text: searchStr)
        }
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {

    }
}
