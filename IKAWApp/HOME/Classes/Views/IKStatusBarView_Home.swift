//
//  IKStatusBarView.swift
//  IKAWApp
//
//  Created by Admin on 3/7/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib
import SnapKit

class IKStatusBarView_Home: NibLoadingView {
    @IBOutlet weak var profileNameLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var roasterStatusLabel: UILabel!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var statusColorView: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var profileStatusLabel: UILabel!
    
    var needToHideInLandscape = false
    var isAnimating = false
    static var previousState = IK_STATUS_UNKNOWN
    var currentState = IK_STATUS_UNKNOWN
    var profile: Profile?
    var isEditedProfile = false {
        didSet {
            if isEditedProfile {
                profileStatusLabel.text = NSLocalizedString("$editing_recipe", comment: "")
                profileStatusLabel.isHidden = false
                roasterStatusLabel.isHidden = true
            } else {
                profileStatusLabel.isHidden = true
                roasterStatusLabel.isHidden = false
            }
        }
    }
    var isSendingProfile = false {
        didSet {
            if isSendingProfile {
                profileStatusLabel.isHidden = false
                roasterStatusLabel.isHidden = true
            } else {
                profileStatusLabel.isHidden = true
                roasterStatusLabel.isHidden = false
            }
        }
    }
    static var wasRoasterOpen = false
    
    private static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    //MARK: - Lifecicle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        thumbnail.layer.cornerRadius = thumbnail.bounds.width / 2
        thumbnail.layer.masksToBounds = true
        
        backgroundColor = kColorPreHeating
        
        layer.masksToBounds = false
        layer.shadowOffset = CGSize(width: 0, height: 5)
        layer.shadowRadius = 5
        roasterStatusLabel.text = ""
        
        NotificationCenter.default.addObserver(self, selector:#selector(didLogout), name: NSNotification.Name.DidLogOut, object: nil)
        RoasterManager.instance().delegates.add(self)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    //MARK: - Publick
    func populate(profile: Profile?) {
        let state = self.staus()
        self.profile = profile
        if (self.profile == nil) {
            self.profile = state.profile
        }
        
        guard self.profile != nil else {
            return
        }
        
        var placeholderKawaThumb = "ProfileThumbnailHolder"
        
        if self.profile?.isRoasterProfile() ?? false {
            placeholderKawaThumb = "ProfileIconWhite"
        }
        thumbnail.image = UIImage(named: placeholderKawaThumb)
        
        profileNameLabel.text = self.profile?.getNameWithType().replacingOccurrences(of: "\n", with: " ")
        var dateStr = ""
        if let dateLastRoasted = profile!.dateLastRoasted {
            if Calendar.current.isDateInToday(dateLastRoasted) || profile!.dateLastRoasted.timeIntervalSinceNow > 0  {
                dateStr = NSLocalizedString("$today", comment: "")
            } else if Calendar.current.isDateInYesterday(dateLastRoasted) {
                dateStr = NSLocalizedString("$yesterday", comment: "")
            } else if Date().startOfWeek! <= dateLastRoasted {
                dateStr = NSLocalizedString("$this_week", comment: "")
            } else if profile!.isNeverUsed() {
                dateStr = "$never".localized
            } else {
                dateStr = IKStatusBarView_Home.formatter.string(from: dateLastRoasted)
            }
        }
        dateLabel.text = dateStr
        
        progressView.isHidden = true
        
        if self.profile?.isRoasterProfile() ?? false {
            profileNameLabel.textColor = kColorWhite
            dateLabel.textColor = kColorWhite
            roasterStatusLabel.text = state.status
            roasterStatusLabel.textColor = kColorWhite
            statusColorView.backgroundColor = state.color
            profileStatusLabel.textColor = kColorWhite
            profileStatusLabel.textColor = kColorWhite
            updateProgressBar()
        } else {
            statusColorView.backgroundColor = kColorWhite
            profileStatusLabel.textColor = kColorDarkGrey2
            profileStatusLabel.textColor = kColorDarkGrey2
            roasterStatusLabel.text = ""
            profileNameLabel.textColor = kColorDarkGrey2
            dateLabel.textColor = kColorDarkGrey2
            roasterStatusLabel.textColor = kColorDarkGrey2
        }
        
    }
    
    
    func updateProgressBar() {
        statusColorView.backgroundColor = self.staus().color
        let roasterState = RoasterManager.instance().roaster.status.state
        let progress = AppCore.sharedInstance.roastManager.roastProgress()
        guard roasterState == IK_STATUS_ROASTING && progress > 0 else {
            statusColorView.isHidden = false
            progressView.isHidden = true
            return
        }
        progressView.isHidden = false
        self.progressView.snp.remakeConstraints { (make) in
            make.width.equalTo(self).multipliedBy(CGFloat(progress))
        }
        switch (RoasterManager.instance().roaster.status.state) {
        case IK_STATUS_ROASTING:
            statusColorView.backgroundColor = kColorPreHeating
            progressView.backgroundColor = kColorRoasting
            statusColorView.isHidden = false
            break
        case IK_STATUS_COOLING:
            statusColorView.backgroundColor = kColorRoasting
            progressView.backgroundColor = kColorCoolingDown
            statusColorView.isHidden = false
            break
        default:
            progressView.isHidden = true
            statusColorView.isHidden = false
            break
        }
        
    }
    
    //MARK: - Private
    
    func calculateState() -> RoasterState {
        var state = RoasterManager.instance().roaster.status.state
        if (state == IK_STATUS_IDLE && (IKStatusBarView_Home.previousState == IK_STATUS_OPEN || IKStatusBarView_Home.previousState == IK_STATUS_READY)) {
            state = IK_STATUS_READY
        }
        return state
    }
    
    func staus() -> (status: String?, profile: Profile?, color: UIColor?) {
        
        if ((IKInterface.instance().isBluetoothEnabled() || IK_SIMULATE_ROASTER) && RoasterManager.instance().roaster == nil) {
            return (NSLocalizedString("$not_connected_title", comment: ""), nil, kColorGrey)
        }
        
        
        let profile = RoasterManager.instance().profile
        
        let progress = AppCore.sharedInstance.roastManager.roastProgress()
        if ((IKInterface.instance().isBluetoothEnabled() || IK_SIMULATE_ROASTER) && RoasterManager.instance().roaster != nil) {
            let state = calculateState()
            switch (state) {
            case IK_STATUS_BUSY:
                return (NSLocalizedString("$busy_title", comment: ""), nil, kColorGrey)
            case IK_STATUS_COOLING:
                IKStatusBarView_Home.wasRoasterOpen = false
                return (NSLocalizedString("$cooling_title", comment: ""), profile, kColorAirflow)
            case IK_STATUS_HEATING:
                IKStatusBarView_Home.wasRoasterOpen = false
                return (NSLocalizedString("$preheating_title", comment: ""), profile, kColorPreHeating)
            case IK_STATUS_IDLE:
                return (NSLocalizedString("$idle_title", comment: ""), profile, kColorOnRoaster)
            case IK_STATUS_OPEN:
                IKStatusBarView_Home.wasRoasterOpen = true
                
                if (progress > 0) {
                    return (NSLocalizedString("$doser_open_title", comment: ""), profile, kColorGrey)
                } else {
                    return (NSLocalizedString("$doser_open_title", comment: ""), nil, kColorGrey)
                }
                
            case IK_STATUS_PROBLEM:
                return (NSLocalizedString("UNEXPECTED PROBLEM", comment: ""), profile, kColorGrey)
            case IK_STATUS_READY:
                return (NSLocalizedString("$ready_title", comment: ""), profile, kColorReadyToRoast)
            case IK_STATUS_ROASTING:
                IKStatusBarView_Home.wasRoasterOpen = false
                return (NSLocalizedString("$roasting_title", comment: ""), profile, kColorRoasting)
            case IK_STATUS_NEEDS_UPDATE:
                return (NSLocalizedString("$needs_update_title", comment: ""), nil, kColorExhaust)
            case IK_STATUS_SWAP_JARS:
                return (NSLocalizedString("$swap_jars_title", comment: ""), profile, kColorGrey)
            case IK_STATUS_READY_TO_BLOWOVER:
                return (NSLocalizedString("$swap_jars_title", comment: ""), profile, kColorGrey)
            case IK_STATUS_TEST_MODE:
                return (NSLocalizedString("$test_mode", comment: ""), profile, kColorGrey)
            default:
                break
            }
        } else {
            return (NSLocalizedString("$bluetooth_off_title", comment: ""), profile, kColorGrey)
        }
        return (nil, nil, nil)
    }
    
    
    @objc func didLogout() {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
}

extension IKStatusBarView_Home : IKInterfaceDelegate {
    func peripheralDidConnect(_: IKPeripheral) {
        
    }
    
    func peripheralDidDisconnect(_ peripheral: IKPeripheral!) {
        
    }
    
    func peripheral(_ peripheral: IKPeripheral!, didReceive data: Data!) {
        
    }
    
    
}

extension IKStatusBarView_Home : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        
        if let profile = self.profile {
            if let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: profile.uuid) {
                self.populate(profile: profile)
            }
        }
        
        
        
        guard self.profile?.isRoasterProfile() ?? false else {
            return
        }
        roasterStatusLabel.text = NSLocalizedString("$connecting_title", comment: "")
        statusColorView.backgroundColor = kColorGrey
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        
        if let profile = self.profile {
            if let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: profile.uuid) {
                self.populate(profile: profile)
            }
        }
        
        guard self.profile?.isRoasterProfile() ?? false else {
            return
        }
        if (IKInterface.instance().isBluetoothEnabled() || IK_SIMULATE_ROASTER) {
            roasterStatusLabel.text = NSLocalizedString("$not_connected_title", comment: "")
        } else {
            roasterStatusLabel.text = NSLocalizedString("$bluetooth_off_title", comment: "")
        }
        statusColorView.backgroundColor = kColorGrey
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        print(RoasterManager.instance().roaster.status.state)
        
        guard self.profile?.isRoasterProfile() ?? false else {
            return
        }
        
        if let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: RoasterManager.instance().profile?.uuid ?? "") {
            self.populate(profile: profile)
        }
        
        let state = calculateState()
        currentState = state
        
        if (currentState != IK_STATUS_UNKNOWN) {
            IKStatusBarView_Home.previousState = state
        }
        
        
        if ((IKInterface.instance().isBluetoothEnabled() || IK_SIMULATE_ROASTER) && RoasterManager.instance().roaster != nil) {
            switch (state) {
            case IK_STATUS_HEATING:
                if !isAnimating {
                    self.statusColorView.backgroundColor = kColorPreHeating
                }
                break
            case IK_STATUS_READY:
                if !isAnimating {
                    self.statusColorView.layer.opacity = 1
                    self.statusColorView.backgroundColor = kColorReadyToRoast
                }
                
                break
            case IK_STATUS_READY_TO_BLOWOVER:
                self.statusColorView.layer.removeAllAnimations()
                self.statusColorView.backgroundColor = kColorGrey
                isAnimating = false
                break
            default:
                self.statusColorView.layer.removeAllAnimations()
                self.statusColorView.layer.opacity = 1
                isAnimating = false
                break
            }
        }
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        if profile.uuid == self.profile?.uuid {
            if let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: RoasterManager.instance().profile?.uuid ?? "") {
                self.populate(profile: profile)
            }
            
        }
    }
    
}

