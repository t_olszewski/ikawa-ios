//
//  IKProfileViewCell.swift
//  IKAWApp
//
//  Created by Admin on 6/4/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SnapKit
import IKRoasterLib
import SDWebImage

class IKProfileViewCell_Home: NibLoadingView {
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    private static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()

    override func awakeFromNib() {
        super.awakeFromNib()

        thumbnailImageView.layer.cornerRadius = thumbnailImageView.bounds.width / 2
        thumbnailImageView.layer.masksToBounds = true
    }
    
    func populate(profile: Profile) {
        profileNameLabel.text = profile.getNameWithType().replacingOccurrences(of: "\n", with: " ")
        var dateStr = ""
        if let dateLastRoasted = profile.dateLastRoasted {
            if Calendar.current.isDateInToday(dateLastRoasted) || profile.dateLastRoasted.timeIntervalSinceNow > 0  {
                dateStr = NSLocalizedString("$today", comment: "")
            } else if Calendar.current.isDateInYesterday(dateLastRoasted) {
                dateStr = NSLocalizedString("$yesterday", comment: "")
            } else if Date().startOfWeek! <= dateLastRoasted {
                dateStr = NSLocalizedString("$this_week", comment: "")
            } else if profile.isNeverUsed() {
                dateStr = "$never".localized
            } else  {
                dateStr = IKProfileViewCell_Home.formatter.string(from: profile.dateLastRoasted)
            }
        }
        dateLabel.text = dateStr
        if profile.type == ProfileTypeUser {
                self.thumbnailImageView.image = UIImage(named: "ProfileIconGrey")
        } else {
            self.thumbnailImageView.image = UIImage(named: "ProfileThumbnailHolder")
        }
        
    }
    

}
