//
//  AppTheme.swift
//  IKAWA-Home
//
//  Created by Admin on 10/12/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import MaterialComponents.MaterialButtons_ButtonThemer
import MaterialComponents.MaterialColorScheme
import MaterialComponents.MaterialTypographyScheme


final class AppTheme {
    let colorScheme: MDCColorScheming
    let typographyScheme: MDCTypographyScheming
    let buttonScheme: MDCButtonScheming
    
    init(colorScheme: MDCColorScheming, typographyScheme: MDCTypographyScheming) {
        self.colorScheme = colorScheme
        self.typographyScheme = typographyScheme
        let buttonScheme = MDCButtonScheme()
        buttonScheme.colorScheme = colorScheme
        buttonScheme.typographyScheme = typographyScheme
        self.buttonScheme = buttonScheme
    }
    
    static let defaultTheme: AppTheme = {
        let colorScheme = MDCSemanticColorScheme()
        colorScheme.primaryColor = kColorWhite
        colorScheme.onPrimaryColor = kColorRed
        
        colorScheme.primaryColorVariant = .init(white: 0.7, alpha: 1)
        colorScheme.secondaryColor = UIColor(red: CGFloat(0x00) / 255.0,
                                             green: CGFloat(0xE6) / 255.0,
                                             blue: CGFloat(0x76) / 255.0,
                                             alpha: 1)
        let typographyScheme = MDCTypographyScheme()
        typographyScheme.headline1 = UIFont.systemFont(ofSize: 20)
        typographyScheme.headline2 = UIFont.systemFont(ofSize: 18)
        typographyScheme.headline3 = UIFont.systemFont(ofSize: 15)
        return AppTheme(colorScheme: colorScheme, typographyScheme: typographyScheme)
    }()
    
    static func applyTheme(to bottomNavBar: MDCBottomNavigationBar) {
        MDCBottomNavigationBarColorThemer.applySemanticColorScheme(AppTheme.defaultTheme.colorScheme, toBottomNavigation: bottomNavBar)
    }
    
}

