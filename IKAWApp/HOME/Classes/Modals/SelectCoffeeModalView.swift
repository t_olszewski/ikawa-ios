//
//  SelectCoffeeModalView.swift
//  IKAWA-Home
//
//  Created by Admin on 11/6/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SelectCoffeeModalView:  ModalView {
    
    var callback: ((_ coffee: IKCoffee) -> Void)?
    
    @IBOutlet weak var tableView: UITableView!
    var items = [IKCoffee]() {
        didSet {
           tableView.reloadData()
        }
    }

    @IBOutlet weak var background: UIView!
    @IBOutlet weak var dialog: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundView = background
        
        let fontBold = UIFont(name: kFontAvenirNextLTProBold, size: 16) ?? UIFont.boldSystemFont(ofSize: 16)
        
        dialog.layer.shadowColor = UIColor.black.cgColor
        dialog.layer.shadowOpacity = 0.5
        dialog.layer.shadowOffset = CGSize(width: 0, height: 1)
        dialog.layer.shadowRadius = 4
        
        titleLabel.attributedText = "$select_coffee".localized.attributedString(font: fontBold, color: kColorDarkGrey2)
        tableView.register(UINib(nibName: "CoffeeCell", bundle: nil), forCellReuseIdentifier: "Cell")
    }
    
    //MARK: - Actions
    
    @IBAction func coffeeNameButtonTouchUpInside(_ sender: Any) {
        dismiss(animated: true)
    }
    @IBAction func closeButtonTouchUpInside(_ sender: Any) {
        dismiss(animated: true)
    }
    
}

extension SelectCoffeeModalView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CoffeeCell
        cell.populate(coffee: items[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true)
        let coffee = items[indexPath.row]
        callback?(coffee)
    }
    
}
