//
//  PushNotificationView.swift
//  IKAWA-Home
//
//  Created by Admin on 11/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class PushNotificationView: ModalView {
    

    @IBOutlet weak var rightHeaderMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerStackView: UIStackView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak private var background: UIView!
    @IBOutlet weak private var dialog: UIView!
    
    @IBOutlet weak private var firstButton: UIButton!
    @IBOutlet weak private var secondButton: UIButton!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var descrioptionLabel: UILabel!
    typealias Callback = (() -> ())
    var title: String = ""{
        didSet {
            if title.isEmpty {
               titleLabel.isHidden = true
               // headerStackViewHeightConstraint.constant = subtitleLabel.isHidden ? 0 : subtitleLabel.frame.height
            } else {
               titleLabel.isHidden = false
               titleLabel.text = title
            }

        }
    }
    
    var subtitle: String = ""{
        didSet {
            if subtitle.isEmpty {
               subtitleLabel.isHidden = true
            } else {
                subtitleLabel.isHidden = false
                subtitleLabel.text = subtitle
            }
            
        }
    }
    
    var descr: String = ""{
        didSet {
            if descr.isEmpty {
                descrioptionLabel.isHidden = true
            } else {
                descrioptionLabel.isHidden = false
                descrioptionLabel.text = descr
            }
            
        }
    }
    
    var firstButtonTitle: String = ""{
        didSet {
            if firstButtonTitle.isEmpty {
                firstButton.isHidden = true
            } else {
                secondButton.isHidden = false
                let bold16 = UIFont(name: kFontAvenirNextDemiBold, size: 16) ?? UIFont.boldSystemFont(ofSize: 16)
                firstButton.setAttributedTitle(firstButtonTitle.attributedString(font: bold16, color: kColorRed), for: .normal)
            }

        }
    }
    
    var secondButtonTitle: String = ""{
        didSet {
            if secondButtonTitle.isEmpty {
                secondButton.isHidden = true
            } else {
                secondButton.isHidden = false
                let bold16 = UIFont(name: kFontAvenirNextDemiBold, size: 16) ?? UIFont.boldSystemFont(ofSize: 16)
                secondButton.setAttributedTitle(secondButtonTitle.attributedString(font: bold16, color: UIColor.white), for: .normal)
            }
        }
    }
    
    var firstButtonCallback: Callback?
    var secondButtonCallback: Callback?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundView = background
        
        dialog.layer.shadowColor = UIColor.black.cgColor
        dialog.layer.shadowOpacity = 0.5
        dialog.layer.shadowOffset = CGSize(width: 0, height: 1)
        dialog.layer.shadowRadius = 4
        
        firstButton.backgroundColor = UIColor.white
        firstButton.layer.borderWidth = 2
        firstButton.layer.borderColor = kColorRed.cgColor
        firstButton.layer.cornerRadius = firstButton.frame.size.height / 2
        
        secondButton.backgroundColor = kColorRed
        secondButton.layer.borderWidth = 2
        secondButton.layer.borderColor = kColorRed.cgColor
        secondButton.layer.cornerRadius = secondButton.frame.size.height / 2
        
    }
    
    func hideThumbnail() {
        thumbnailImageView.isHidden = true
        rightHeaderMarginConstraint.priority = UILayoutPriority(1000)
    }
    
    @IBAction func closeButtonTouchUpInside(_ sender: Any) {
        dismiss(animated: true)
    }
    @IBAction func firstButtonTouchUpInside() {
        dismiss(animated: true)
        firstButtonCallback?()
    }
    
    @IBAction func secondButtonTouchUpInside(_ sender: Any) {
        dismiss(animated: true)
        secondButtonCallback?()
    }
    
}
