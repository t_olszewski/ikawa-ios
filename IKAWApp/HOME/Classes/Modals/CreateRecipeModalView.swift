//
//  CreateRecipe.swift
//  IKAWA-Home
//
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class CreateRecipeModalView: ModalView {
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var dialog: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var recipeNameTitleLabel: UILabel!
    @IBOutlet weak var recipeNameTextField: UITextField!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    var cancelButtonCallback: (() -> Void)?
    var saveButtonCallback: (( _ recipeName: String) -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func show(animated: Bool, dismissCallback: (() -> ())?) {
        super.show(animated: animated, dismissCallback: dismissCallback)
        
        dialog.layoutIfNeeded()
        dialog.layer.shadowColor = UIColor.black.cgColor
        dialog.layer.shadowOpacity = 0.5
        dialog.layer.shadowOffset = CGSize(width: 0, height: 1)
        dialog.layer.shadowRadius = 4
        
        saveButton.layer.borderWidth = 2
        saveButton.layer.borderColor = kColorRed.cgColor
        saveButton.layer.cornerRadius = saveButton.frame.height / 2
        
        KeyboardAvoiding.avoidingView = dialog
        recipeNameTextField.keyboardAppearance = .light
    }
    
    //MARK: - Actions
    
    @IBAction func onButtonCancelTap() {
        dismiss(animated: true)
        endEditing(true)
        cancelButtonCallback?()
    }
    
    @IBAction func onButtonSaveTap(_ sender: Any) {
        dismiss(animated: true)
        endEditing(true)
        guard recipeNameTextField.text != "" else {
            return
        }
        saveButtonCallback?(recipeNameTextField.text!)
    }
    
    @IBAction func receiptNameTextFieldChanged(_ sender: Any) {
        if let receipName = recipeNameTextField.text {
            if receipName.count > 30 {
                let index = receipName.index(receipName.startIndex, offsetBy: 30)
                let substring = receipName[..<index]
                recipeNameTextField.text =  String(substring)
            }
        }
    }
}

