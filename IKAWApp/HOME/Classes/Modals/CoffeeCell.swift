//
//  CoffeeCell.swift
//  IKAWA-Home
//
//  Created by Admin on 11/6/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class CoffeeCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populate(coffee: IKCoffee) {
        let font = UIFont(name: kFontAvenirNextLTProRegular, size: 13) ?? UIFont.boldSystemFont(ofSize: 13)
        nameLabel.attributedText = coffee.name.attributedString(font: font, color: kColorDarkGrey2)
    }
}
