//
//  IKCreateAccountVC.swift
//  IKAWA-Home
//
//  Created by Никита on 10/10/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class IKCreateAccountVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var agreeTermsButton: UIButton!
    var agreeTerms = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        KeyboardAvoiding.avoidingView = scrollView
        AppCore.sharedInstance.loggingManager.trackScreen(name: "Sign-up Screen")
        createAccountButton.titleLabel?.text = NSLocalizedString("$create_account_title_button", comment: "")
                titleLabel.attributedText = NSLocalizedString("$create_account_title", comment: "").attributedString(font: UIFont(name: kFontAvenirNextDemiBold, size: 20)!, color: kColorGreyDark)
        emailTextField.delegate = self
        passwordTextField.delegate = self
        createAccountButton.layer.cornerRadius = createAccountButton.frame.height / 2
        createAccountButton.layer.borderColor = kColorRed.cgColor
        createAccountButton.layer.borderWidth = 2
        firstNameTextField.text = ""
        lastNameTextField.text = ""
        emailTextField.text = ""
        createAccountButton.isEnabled = false
        createAccountButton.alpha = 0.5
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    // MARK: - Actions
    
    @IBAction func agreeTermsButtonTouchUpInside(_ sender: Any) {
        if agreeTerms {
            agreeTerms = false
            agreeTermsButton.setImage(UIImage(named: "DeselectedPointIcon"), for: .normal)
            createAccountButton.isEnabled = false
            createAccountButton.alpha = 0.5
        } else {
            agreeTerms = true
            agreeTermsButton.setImage(UIImage(named: "selectedPointIcon"), for: .normal)
            createAccountButton.isEnabled = true
            createAccountButton.alpha = 1
        }
    }
    
    @IBAction func backButtonTouchUpInside(_ sender: Any) {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func registerButtonTouchUpInside(_ sender: Any) {
        self.view.endEditing(true)
        guard validatePassword() && agreeTerms else {
            return
        }
        
        
        UserDefaults.standard.set( ["email" : emailTextField.text,
                                   "firstName" : firstNameTextField.text,
                                   "lastName" : lastNameTextField.text], forKey: "accountRegistrationParameters")
        UserDefaults.standard.synchronize()
        self.view.makeToastActivity(.center)
        AppCore.sharedInstance.firebaseManager.firebaseRegisterUser(email: emailTextField.text!, password: passwordTextField.text!, callback: { (success, message) in
            DispatchQueue.main.async {
                self.view.hideToastActivity()
            }
            if (!success) {
                AppCore.sharedInstance.loggingManager.trackEvent(name: "Registration failed", label: "")
                
                let popup = ButtonsPopupModalView()
                popup.layout = .vertical
                popup.title = NSLocalizedString("$registration_failed", comment: "")
                popup.subtitle = (message ?? "").replacingOccurrences(of: ".", with: "")
                popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                }
                popup.show(animated: true, dismissCallback: {
                })
                //IKNotificationManager.sharedInstance.show(view: popup)
                
            } else {
                self.view.endEditing(true)
                AppCore.sharedInstance.loggingManager.trackEvent(name: "Registration success", label: "")
                
                let popup = ButtonsPopupModalView()
                popup.layout = .vertical
                popup.title = NSLocalizedString("$registration_success", comment: "")
                popup.subtitle = "$confirm_email_message".localized
                popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                    NavigationHelper.GoToLoginScreen()
                }
                popup.show(animated: true, dismissCallback: {
                    
                })
                //IKNotificationManager.sharedInstance.show(view: popup)
                
            }
        })

    }
    
    @IBAction func signInButtonTouchUpInside(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func termsButtonTouchUpInside(_ sender: Any) {
        //https://7puzn1bawno25nd4f1awju71-wpengine.netdna-ssl.com/wp-content/uploads/2018/05/IKAWA-Privacy-Policy-for-website-FINAL.pdf
        guard let url = URL(string: kTermsAndConditions) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }

    }
    
    @IBAction func firstNameEditButtonTouchUpInside(_ sender: Any) {
        firstNameTextField.becomeFirstResponder()
    }

    @IBAction func lastNameEditButtonTouchUpInside(_ sender: Any) {
        lastNameTextField.becomeFirstResponder()
    }
    
    @IBAction func emailEditButtonTouchUpInside(_ sender: Any) {
        emailTextField.becomeFirstResponder()
    }
    
    @IBAction func passwordEditButtonTouchUpInside(_ sender: Any) {
        passwordTextField.becomeFirstResponder()
    }
    
    //MARK: - Private
    
    func validateEmail() -> Bool {
        let valid = self.emailTextField.text?.validateEmail() ?? false
        
        if (valid) {
            self.emailTextField.textColor = kColorGreyDark
        } else {
            self.emailTextField.textColor = UIColor.red
            
            let popup = ButtonsPopupModalView()
            popup.layout = .vertical
            popup.title = NSLocalizedString("$invalid_email_title", comment: "")
            popup.subtitle = NSLocalizedString("$invalid_email_message", comment: "")
            popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            popup.show(animated: true) {
                
            }
            //IKNotificationManager.sharedInstance.show(view: popup)
        }
        
        return valid
    }
    
    func validatePassword() -> Bool {
        let valid = self.passwordTextField.text?.validatePassword() ?? false
        if (valid) {
            self.passwordTextField.textColor = kColorDarkGrey2
        } else {
            self.passwordTextField.textColor = UIColor.red
            
            let popup = ButtonsPopupModalView()
            popup.layout = .vertical
            popup.title = NSLocalizedString("$password_too_short_title", comment: "")
            popup.subtitle = NSLocalizedString("$password_too_short_message", comment: "")
            popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            popup.show(animated: true) {
                
            }
            //IKNotificationManager.sharedInstance.show(view: popup)
        }
        return valid
    }
    
    
    func validateInput() -> Bool {
        if (validateEmail() && validatePassword()) {
            return true
        } else {
            return false
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
}


extension IKCreateAccountVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

