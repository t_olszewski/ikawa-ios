//
//  IKBaseVC.swift
//  IKAWApp
//
//  Created by Admin on 3/21/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SideMenu
import IKRoasterLib

class IKBaseVC: UIViewController {
    var menuBarButtonItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
        
//        let logo = UIImage(named: "IKLogo.png")
//        let imageView = UIImageView(image:logo)
//        self.navigationItem.titleView = imageView
        
        let menuButton = UIButton(type: .custom)
        menuButton.addTarget(self, action: #selector(menuButtonTouchUpInside), for: UIControl.Event.touchUpInside)
        menuButton.setImage(UIImage(named: "HamburgerIcon"), for: .normal)
        menuButton.sizeToFit()
        menuButton.contentHorizontalAlignment = .left
        menuButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
        menuBarButtonItem = UIBarButtonItem(customView: menuButton)
        self.navigationItem.leftBarButtonItem = menuBarButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.navigationController?.navigationBar.barTintColor = kNavigationBarColor
       // self.navigationController?.navigationBar.tintColor = kNavigationBarColor
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func menuButtonTouchUpInside(sender:UIButton!) {
        guard self.tabBarController != nil else {
            return
        }
      let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
      let nc = menuStoryboard.instantiateInitialViewController()
        present(nc!, animated: true) {
            
        }
    }
}



