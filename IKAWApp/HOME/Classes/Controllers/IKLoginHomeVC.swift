//
//  IKLoginVC_Home.swift
//  IKAWA-Home
//
//  Created by Admin on 12/28/18.
//  Copyright © 2018 Admin. All rights reserved.
//


class IKLoginHomeVC: IKLoginVC {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareApirance()
    }
    
    func prepareApirance() {
        signInButton.layer.borderColor = kColorRed.cgColor
        
        //emailTextField.defaultTextAttributes = [NSAttributedString.Key.kern.rawValue: 0, NSAttributedString.Key.font.rawValue :UIFont(name: kFontAvenirNextRegular, size: 14)!, NSAttributedString.Key.foregroundColor.rawValue: kColorDarkGrey2]
        emailTextField.createBottomLine()
        
        //passwordTextField.defaultTextAttributes = [NSAttributedString.Key.kern.rawValue: 0, NSAttributedString.Key.font.rawValue :UIFont(name: kFontAvenirNextRegular, size: 14)!, NSAttributedString.Key.foregroundColor.rawValue: kColorDarkGrey2]
        passwordTextField.createBottomLine()
    }
    
    func loginHandler() {
        if (self.validateInput()) {
            self.view.makeToastActivity(.center)
            AppCore.sharedInstance.firebaseManager.firebaseLogin(email: self.emailTextField.text!, password: self.passwordTextField.text!, callback: { (success, message) in
                if (!success) {
                    self.view.hideToastActivity()
                    let confirmationDialogView = ButtonsPopupModalView()
                    confirmationDialogView.layout = .vertical
                    confirmationDialogView.title = message ?? NSLocalizedString("$login_failed", comment: "")
                    confirmationDialogView.subtitle = ""
                    
                    confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                    
                    confirmationDialogView.show(animated: true, dismissCallback: {
                        
                    })
                    //IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                    
                } else {
                    UserDefaults.standard.setValue(self.emailTextField.text, forKey: "lastUsedEmail")
                    self.saveNameIfNeeded(callback: { (error) in
                        if let appDelegate  = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.registerPushNotifications()
                        }
                        
                        if let error = error {
                            let errorDialogView = ButtonsPopupModalView()
                            errorDialogView.layout = .vertical
                            errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                            errorDialogView.subtitle = error.localizedDescription
                            errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                            errorDialogView.show(animated: true, dismissCallback: {
                                
                            })
                            //IKNotificationManager.sharedInstance.show(view: errorDialogView)
                        }
                        AppCore.sharedInstance.syncManager.restoreLastBackup(needMergingWithLocalBackup: true, callback: { (error) in
                            let userId = AppCore.sharedInstance.firebaseManager.currentUser?.uid ?? ""
                            AppCore.sharedInstance.settingsManager.confirmationDialog(name: "hideSendReceiptConfirmationDialog", hide: UserDefaults.standard.bool(forKey: "hideSendReceiptConfirmationDialog" + userId))
                            AppCore.sharedInstance.settingsManager.confirmationDialog(name: "swapJarsConfirmationDialog", hide: UserDefaults.standard.bool(forKey: "swapJarsConfirmationDialog" + userId))
                            AppCore.sharedInstance.settingsManager.confirmationDialog(name: "finishConfirmationDialog", hide: UserDefaults.standard.bool(forKey: "finishConfirmationDialog" + userId))
                            AppCore.sharedInstance.settingsManager.confirmationDialog(name: "doserIsOpenConfirmationDialog", hide: UserDefaults.standard.bool(forKey: "doserIsOpenConfirmationDialog" + userId))
                            AppCore.sharedInstance.syncManager.uploadUserLibrary(roundTimestamp: false, callback: { (error) in
                                self.view.hideToastActivity()
                                NavigationHelper.GoToMainScreen()
                                AppCore.sharedInstance.syncManager.startProfileSynchronization()
                            })
                            
                        })
                    })
                    
                    
                }
            })
        }
    }
    //MARK: - Actions
    @IBAction func loginButtonTouchUpInside(_ sender: Any) {
        self.view.endEditing(true)
        loginHandler()
    }
}
