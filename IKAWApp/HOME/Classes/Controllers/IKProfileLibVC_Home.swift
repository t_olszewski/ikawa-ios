//
//  IKProfileLibVC.swift
//  IKAWApp
//
//  Created by Admin on 3/22/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class IKProfileLibVC_Home: UIViewController {


    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: ProfileLibHeaderView_Home!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    var displayArray: [Profile]?
    var searchStr = ""
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        AppCore.sharedInstance.profileManager.sortType = .DATE_LAST_ROASTED_DESC
        
        NotificationCenter.default.addObserver(self, selector:#selector(updateTable), name: NSNotification.Name.ProfileManagerDidChangeList, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(didLogout), name: NSNotification.Name.DidLogOut, object: nil)
        _ = UITapGestureRecognizer(target: self, action: #selector(self.roasterProfileTap(sender:)))
        //statusBarView.addGestureRecognizer(tap)
       
        AppCore.sharedInstance.loggingManager.trackScreen(name: "Library Screen")
        
        headerView.delegate = self
        
        self.tableView.contentInset = UIEdgeInsets(top: self.tableView.contentInset.top, left: 0, bottom: IKTabBarController.TabBarHeight, right: 0)
        
        prepareAppearance()
        if IKInterface.instance().isConnected() || IK_SIMULATE_ROASTER {
            headerView.statusBarView.populate(profile: RoasterManager.instance().profile)
        }
        
        RoasterManager.instance().delegates.add(self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTable()
        layoutHeaderViewDidChanged()
        navigationController?.isNavigationBarHidden = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
    // MARK: - Actions

    @objc func roasterProfileTap(sender: UITapGestureRecognizer? = nil) {
        if let profile = AppCore.sharedInstance.profileManager.findProfileInListByUuid(uuid: RoasterManager.instance().profile?.uuid ?? "") {
            performSegue(withIdentifier: "Chart", sender: profile)
        }
        
    }
    
    
    // MARK: - Notification
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "Chart") {
           let chartVC = segue.destination as! IKChartVC
            chartVC.profile = sender as? Profile
        }
    }
    
    //MARK: - Private
    
    @objc func didLogout() {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.tableView.contentInset = UIEdgeInsets(top: self.tableView.contentInset.top, left: 0, bottom: IKTabBarController.TabBarHeight, right: 0)
            } else {
                self.tableView.contentInset = UIEdgeInsets(top: self.tableView.contentInset.top, left: 0, bottom: endFrame?.size.height ?? 0.0 + IKTabBarController.TabBarHeight, right: 0)
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    func prepareAppearance()  {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 70.0
        
        
        AppCore.sharedInstance.profileManager.reloadData()
        //statusBarView.populate(profile: nil)
        updateTable()
    }
    
    
   @objc func updateTable() {
        AppCore.sharedInstance.profileManager.sort()
        if searchStr.count > 0 {
            displayArray = (AppCore.sharedInstance.profileManager.profilesInLibrary ?? [Profile]()).filter { (profile) -> Bool in
                profile.name.lowercased().contains(searchStr.lowercased())
            }
            
        } else {
            displayArray = (AppCore.sharedInstance.profileManager.profilesInLibrary ?? [Profile]())
        }
        tableView.reloadData()
    }
    
    func showDeleteConfirmationDialog(profile: Profile) {
        
        let alertView = ButtonsPopupModalView()
        alertView.layout = .horisontal
        alertView.subtitle = NSLocalizedString("$delete_recipe_title", comment: "")
        alertView.setUpButton(index: .First, title: NSLocalizedString("$cancel", comment: ""), type: .Cancel) {
            if AppCore.sharedInstance.profileManager.restoreProfile(profile: profile) {
                self.updateTable()
            }
        }
        alertView.setUpButton(index: .Second, title: NSLocalizedString("$delete", comment: ""), type: .Bordered) {}
        IKNotificationManager.sharedInstance.show(view: alertView)
    }
  
}

extension IKProfileLibVC_Home: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let displayArray = displayArray else {
            return 0
        }
        return displayArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let displayArray = displayArray else {
            return UITableViewCell()
        }
        let profile = displayArray[indexPath.row]
        if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile.uuid) {
            return UITableViewCell()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SwipableTableViewCell
        cell.backgroundColor = kColorTableCellBackground
        cell.contentView.backgroundColor = kColorTableCellBackground
        let profileViewCell = cell.contentView.viewWithTag(50) as! IKProfileViewCell_Home

        profileViewCell.populate(profile: profile)

        let bgColorView = UIView()
        bgColorView.backgroundColor = kColorTableCellBackground
        cell.selectedBackgroundView = bgColorView
        
        if profile.isRoasterProfile() || profile.isReadOnly() {
            cell.isSwipable = false
        } else {
            cell.isSwipable = true
        }
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let displayArray = displayArray else {
            return
        }
        tableView.deselectRow(at: indexPath, animated: true)
        let profile = displayArray[indexPath.row]
        performSegue(withIdentifier: "Chart", sender: profile)
        
        if (IK_SIMULATE_ROASTER) {
            RoasterManager.instance().profile = profile
            IKSimulatorManager.sharedInstance.startSimulating()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let displayArray = displayArray else {
            return 0
        }
        let profile = displayArray[indexPath.row]
        if (IK_SIMULATE_ROASTER) {
            if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile.uuid) {
                return 0
            }
        } else {
            if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile.uuid && (IKInterface.instance().isConnected() || IK_SIMULATE_ROASTER)) {
                return 0
            }
        }
        
        return 72
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return statusBarView
//    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if (!IKInterface.instance().isConnected() && !IK_SIMULATE_ROASTER) {
//            return 0
//        }
//
//        if RoasterManager.instance().profile == nil {
//            return 0
//        }
//
//        return statusBarView.frame.size.height
//    }
    
}




extension IKProfileLibVC_Home: ProfileLibHeaderViewDelegate {
    func didTapMenuButton() {
        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let nc = menuStoryboard.instantiateInitialViewController()
        present(nc!, animated: true) {
            
        }
    }
    
    func search(text:String) {
        searchStr = text
        updateTable()
    }
    
    func layoutHeaderViewDidChanged() {
        headerViewHeightConstraint.constant = headerView.maxHeight()
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    func tapOnPrfile(profile: Profile) {
        performSegue(withIdentifier: "Chart", sender: profile)
    }
        
}

extension IKProfileLibVC_Home : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        updateTable()
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        updateTable()
    }
    
    func roaster(_ roaster: Roaster!, didReceiveSetting setting: Int32, withValue value: NSNumber!, success: Bool) {
        
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {

    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        updateTable()
    }
}

extension IKProfileLibVC_Home: SwipableTableViewCellDelegate {
    func swipableTableViewCellDidSwipeLeftWithCell(cell: UITableViewCell) {
        let indexPath = tableView.indexPath(for: cell)!
        if let profile = displayArray?[indexPath.row] {
            if AppCore.sharedInstance.profileManager.removeProfile(profile: profile) {
                displayArray?.remove(at: indexPath.row)
                tableView.beginUpdates()
                tableView.deleteRows(at: [indexPath], with: .fade)
                tableView.endUpdates()
            }
            showDeleteConfirmationDialog(profile: profile)
            
        }
    }
    
    func swipableTableViewCellDidSwipeRightWithCell(cell: UITableViewCell) {
        let indexPath = tableView.indexPath(for: cell)!
        if let profile = displayArray?[indexPath.row] {
            if AppCore.sharedInstance.profileManager.removeProfile(profile: profile) {
                displayArray?.remove(at: indexPath.row)
                tableView.beginUpdates()
                tableView.deleteRows(at: [indexPath], with: .fade)
                tableView.endUpdates()
            }
            showDeleteConfirmationDialog(profile: profile)
        }
    }
    
}


