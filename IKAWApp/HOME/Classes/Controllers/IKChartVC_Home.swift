//
//  IKChartVC.swift
//  IKAWApp
//
//  Created by Admin on 3/28/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Charts
import IKRoasterLib
import SnapKit
import Toast_Swift

class IKChartVC: UIViewController {

    var profile: Profile?
    var editedProfile: Profile?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var statusBarView: IKStatusBarView_Home!
    @IBOutlet weak var chartViewContainer: UIView!
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var sendToRoastButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var timeLabelContainerView: UIView!
    @IBOutlet weak var savingStackView: UIStackView!
    @IBOutlet weak var timeLabelContainerViewBottomConstraint: NSLayoutConstraint!
    
    var isProfileSendingToRoaster: Bool = false
    
    var tempChartView: IKTempChartView?
    //MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector:#selector(didLogout), name: NSNotification.Name.DidLogOut, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        self.statusBarView.thumbnail.isHidden = true
        RoasterManager.instance().delegates.add(self.statusBarView)
        RoasterManager.instance().delegates.add(self)
        prepareAppearance()
        if profile?.shouldOpenInEditMode ?? false {
            goToEditState()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let tempChartView = tempChartView {
            let pixel = tempChartView.chartView.getTransformer(forAxis: .left).pixelForValues(x: 0, y: 25)
            timeLabelContainerViewBottomConstraint.constant = tempChartView.frame.height - pixel.y - timeLabelContainerView.frame.height / 2
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
        
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
        if (RoasterManager.instance().delegates.contains(self.statusBarView)) {
            RoasterManager.instance().delegates.remove(self.statusBarView)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Actions
    
    @IBAction func backButtonTouchUpInside(_ sender: Any) {
        if (self.editedProfile != nil && (self.tempChartView!.graphWasEdited)) {
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .horisontal
            confirmationDialogView.title = NSLocalizedString("$unsaved_changes", comment: "")
            
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$cancel", comment: ""), type: .Cancel) {}
            confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$leave", comment: ""), type: .Bordered) {
                self.navigationController?.popViewController(animated: true)
            }

            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        NavigationHelper.highlightHomeTab()
        
    }
    /*
    @IBAction func cancelButtonTouchUpInside(_ sender: Any) {
        if (self.editedProfile != nil && (self.tempChartView!.graphWasEdited)) {
            let confirmationDialogView = ButtonsPopupModalView.fromNib()
            confirmationDialogView.layout = .horisontal
            confirmationDialogView.title = NSLocalizedString("$unsaved_changes", comment: "")
            confirmationDialogView.firstButtonTitle = NSLocalizedString("$no", comment: "")
            confirmationDialogView.secondButtonTitle = NSLocalizedString("$yes", comment: "")
            confirmationDialogView.firstButtonCallback = {}
            confirmationDialogView.secondButtonCallback = {
                self.goToViewState()
            }
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)

        } else {
            goToViewState()
        }
    }
    */
    // viewStateBottomBar
    @IBAction func sendToRoastButtonTouchUpInside(_ sender: Any) {
        statusBarView.isSendingProfile = true
        sendToRoastButton.isHidden = true
        sendProfile(profile: self.profile)
        isProfileSendingToRoaster = true
        updateAppearance()
    }
    
    @IBAction func shareButtonTouchUpInside(_ sender: Any) {
        AppCore.sharedInstance.profileManager.shareProfile(profile: profile!)
    }
    

    @IBAction func editButtonTouchUpInside(_ sender: Any) {
        goToEditState()
    }
    // viewStateBottomBar
//    @IBAction func deleteButtonTouchUpInside(_ sender: Any) {
//        let confirmationDialogView = ButtonsPopupModalView.fromNib()
//        confirmationDialogView.layout = .horisontal
//        confirmationDialogView.title = NSLocalizedString("$delete_profile_confirm", comment: "")
//        confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$yes", comment: ""), type: .Bordered) {
//            if (IKProfileManager.sharedInstance.removeProfile(profile: self.profile!)) {
//                self.navigationController?.popViewController(animated: true)
//            }
//        }
//        confirmationDialogView.setUpButton(index: .Second, title: NSLocalizedString("$no", comment: ""), type: .Cancel) {}
//
//        IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
//    }
    
//    @IBAction func cancelButtonTouchUpInside(_ sender: Any) {
//        AppCore.sharedInstance.loggingManager.trackEvent(name: "Profile Edit on Roaster - Cancel (changed)", label: "")
//        editProfile = nil
//        showTemperatureGraph()
//        goToViewState()
//    }
    
    
    @IBAction func saveButtonTouchUpInside(_ sender: Any) {
        save { (profile) in
           self.updateAppearance()
        }
    }
    
    @IBAction func cancelButtonTouchUpInside(_ sender: Any) {
        goToViewState()
    }
    
    
    @IBAction func menuButtonTouchUpInside(_ sender: Any) {
        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let nc = menuStoryboard.instantiateInitialViewController()
        present(nc!, animated: true) {
            
        }
    }
    //MARK: - Private

    
    func isEditState() -> Bool {
        return editedProfile != nil
    }
    
    func prepareAppearance() {
        
        saveButton.layer.borderWidth = 2
        saveButton.layer.borderColor = kColorRed.cgColor
        saveButton.layer.cornerRadius = saveButton.frame.height / 2
        saveButton.layer.shadowColor = UIColor.black.cgColor
        saveButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        saveButton.layer.shadowOpacity = 0.3
        saveButton.layer.shadowRadius = 2.0
        saveButton.layer.masksToBounds =  false
        
        cancelButton.layer.borderWidth = 2
        cancelButton.layer.borderColor = kColorWhite.cgColor
        cancelButton.layer.cornerRadius = cancelButton.frame.height / 2
        cancelButton.layer.shadowColor = UIColor.black.cgColor
        cancelButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        cancelButton.layer.shadowOpacity = 0.3
        cancelButton.layer.shadowRadius = 2.0
        cancelButton.layer.masksToBounds =  false
        
        sendToRoastButton.layer.borderWidth = 2
        sendToRoastButton.layer.borderColor = kColorRed.cgColor
        sendToRoastButton.layer.cornerRadius = sendToRoastButton.frame.height / 2
        sendToRoastButton.layer.shadowColor = UIColor.black.cgColor
        sendToRoastButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        sendToRoastButton.layer.shadowOpacity = 0.3
        sendToRoastButton.layer.shadowRadius = 2.0
        sendToRoastButton.layer.masksToBounds =  false
        
        timeLabelContainerView.layer.cornerRadius = timeLabelContainerView.frame.height / 2
        timeLabelContainerView.layer.masksToBounds = true
        timeLabelContainerView.isHidden = true
        
        guard profile != nil else {return}
        statusBarView.populate(profile: profile)
        //self.title = profile?.getNameWithRevision()
        showTemperatureGraph()
        updateAppearance()

    }
    
    func updateAppearance() {
        statusBarView.populate(profile: profile)
        if (profile?.isRoasterProfile() ?? false) || isProfileSendingToRoaster == true {
            NavigationHelper.highlightRoastTab()
        }
        updateButtonsAppearanceWithState()
        updateBackButtonAppearance()
        updateTimerLabel()
        updateTitle()
    }
    
    func updateTitle() {
        if (profile?.isRoasterProfile() ?? false) || isProfileSendingToRoaster == true {
            titleLabel.text = NSLocalizedString("$roast", comment: "")
        } else {
            titleLabel.text = NSLocalizedString("$recipe_library", comment: "")
        }
    }
    
    func updateButtonsAppearanceWithState() {
        if isEditState() {
            shareButton.isHidden = true
            editButton.isHidden = true
            savingStackView.isHidden = false
            sendToRoastButton.isHidden = true
        } else {
            savingStackView.isHidden = true
            if (RoasterManager.instance().roaster != nil && self.profile != nil && self.isRoasterProfile(profile: self.profile!)) {
                sendToRoastButton.isHidden = true
                let state = RoasterManager.instance().roaster.status.state
                if state == IK_STATUS_ROASTING { //roasting
                    shareButton.isHidden = false
                    editButton.isHidden = true
                    return
                } else if state == IK_STATUS_HEATING || state == IK_STATUS_COOLING || state == IK_STATUS_OPEN  {
                    editButton.isHidden = true
                } else {
                    shareButton.isHidden = false
                    editButton.isHidden = false
                }
            } else {
                shareButton.isHidden = false
                editButton.isHidden = false
                editButton.setImage(UIImage(named: "EditIcon"), for: .normal)
                if RoasterManager.instance().roaster == nil {
                    sendToRoastButton.isHidden = true
                } else if RoasterManager.instance().roaster != nil {
                    let state = RoasterManager.instance().roaster.status.state
                    sendToRoastButton.isHidden = (state != IK_STATUS_IDLE || isProfileSendingToRoaster)
                }
                return
            }
            
        }
        updateTimerLabel()
    }
    
    
   
    
    func updateBackButtonAppearance() {
        if navigationController?.viewControllers.count ?? 0 > 1 {
            backButton.isHidden = false
            if statusBarView.statusColorView.backgroundColor == kColorWhite {
                backButton.setImage(UIImage(named: "backArrow"), for: UIControl.State.normal)
            } else {
                backButton.setImage(UIImage(named: "backArrowWhite"), for: UIControl.State.normal)
            }
        } else {
            backButton.isHidden = true
        }

        
    }
    
    func updateTimerLabel() {
        if (self.profile?.isRoasterProfile() ?? false) {
            timeLabelContainerView.isHidden = isEditState()
            if RoasterManager.instance().roaster != nil {
                let state = RoasterManager.instance().roaster.status.state
                if !(state == IK_STATUS_SWAP_JARS || state == IK_STATUS_READY_TO_BLOWOVER || state == IK_STATUS_BUSY) {
                    timerLabel.text = AppCore.sharedInstance.roastManager.roastTimeLeft() ?? profile?.roastTime()
                }
            }
        } else {
            timeLabelContainerView.isHidden = true
        }
    }
    
    func save(callback: @escaping (_ savedProfile: Profile?)->Void) {
        AppCore.sharedInstance.loggingManager.trackEvent(name: "Profile Edit on Roaster - Save", label: "")
        if let userId = AppCore.sharedInstance.firebaseManager.currentUser?.uid {
            editedProfile?.userId = userId
        }
        if (profile?.isReadOnly())! {
            AppCore.sharedInstance.profileManager.showSaveProfileAsCopyAlert(profile: editedProfile!, title: nil) { (saveState, profile) in
                if (saveState == .SAVED_AS || saveState == .OVERWRITTEN) {
                    let needSendToRoaster = self.isRoasterProfile(profile: self.profile!)
                    self.profile = profile
                    self.goToViewState()
                    if needSendToRoaster {
                        self.isProfileSendingToRoaster = true
                        self.statusBarView.isSendingProfile = true
                        self.sendToRoastButton.isHidden = true
                        self.sendProfile(profile: self.profile)
                        self.updateTitle()
                    }
                } else {
                    AppCore.sharedInstance.loggingManager.trackEvent(name: "Profile Edit on Roaster - Cancel (changed)", label: "")
                }
                callback(profile)
            }
        } else {
            if profile!.shouldOpenInEditMode {
                _ = AppCore.sharedInstance.profileManager.overwriteProfile(oldProfile: profile!, newProfile: editedProfile!)
                self.profile = editedProfile
                self.goToViewState()
            } else {
                AppCore.sharedInstance.profileManager.saveProfile(profile: editedProfile!, existsText: NSLocalizedString("$how_to_save", comment: "")) { (saveState, profile) in
                    if (profile != nil) {
                        let needSendToRoaster = self.isRoasterProfile(profile: self.profile!)
                        self.profile = profile
                        self.goToViewState()
                        if needSendToRoaster {
                            self.isProfileSendingToRoaster = true
                            self.statusBarView.isSendingProfile = true
                            self.sendToRoastButton.isHidden = true
                            self.sendProfile(profile: self.profile)
                            self.updateTitle()
                        }
                    } else {
                        
                    }
                    callback(profile)
                }
            }

        }
    }
    
    func showTemperatureGraph() {
        self.chartViewContainer.subviews.forEach({ $0.removeFromSuperview()})
        tempChartView = IKTempChartView()
        chartViewContainer.addSubview(tempChartView!)
        tempChartView!.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(self.chartViewContainer).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
        tempChartView!.populateChart(profile:profile, editedProfile: editedProfile)
        if (self.editedProfile != nil) {
            if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile?.uuid) { //On machine already
                AppCore.sharedInstance.loggingManager.trackScreen(name: "Profile Edit On Roaster Screen")
            } else {
                AppCore.sharedInstance.loggingManager.trackScreen(name: "Profile Edit Screen")
            }
            tempChartView?.goToEditState()
        } else {
            if (RoasterManager.instance().profile != nil && RoasterManager.instance().profile.uuid == profile?.uuid) { //On machine already
                AppCore.sharedInstance.loggingManager.trackScreen(name: "Profile On Roaster Scene")
            } else {
                AppCore.sharedInstance.loggingManager.trackScreen(name: "Profile Screen")
            }
            
            tempChartView?.goToViewState()
        }
        
        updateChartIfNeeded()
        
    }
    
    
    
    func showProfileEditTable() {
        self.performSegue(withIdentifier: "ProfileEditVC", sender: nil)
    }
    
    func goToEditState() {
        if let editedProfile =  self.profile?.copy() as? Profile {
            self.editedProfile = editedProfile
            self.editedProfile?.uuid = AppCore.sharedInstance.profileManager.getRandomUuid()
            self.editedProfile?.parentUuid = self.profile?.uuid ?? ""
            self.editedProfile?.type = ProfileTypeUser
            showTemperatureGraph()
            updateButtonsAppearanceWithState()
            statusBarView.isEditedProfile = true
        }
    }

    func goToViewState() {
        self.title = profile?.name
        editedProfile = nil
        showTemperatureGraph()
//        if (tempChartView != nil) {
//            tempChartView?.goToViewState()
//        }
        updateButtonsAppearanceWithState()
        statusBarView.isEditedProfile = false
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if (tempChartView != nil) {
            tempChartView?.viewWillTransition()
        }
    }
    

    
    func updateChartIfNeeded() {
        if (chartViewContainer.subviews.count > 0 && AppCore.sharedInstance.roastManager.currentRoast?.profileUuid == self.profile?.uuid) {
            let chartView = chartViewContainer.subviews.last as! ChartViewProtocol
            chartView.updateTempPoint(roast: AppCore.sharedInstance.roastManager.currentRoast)
        }
    }
    
    func isRoasterProfile(profile: Profile) -> Bool {
        if let roasterProfile = RoasterManager.instance().profile {
            return profile.uuid == roasterProfile.uuid
        }
        return false
    }
    
    func sendProfile(profile: Profile?) {
        guard profile != nil else {
            return
        }
        if (!RoasterManager.instance().roaster.hasBelowSensor() && profile?.tempSensor == TempSensorBelow) {
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$profile_unsupported", comment: "")
            confirmationDialogView.subtitle = NSLocalizedString("$inlet_sensor_profile_error_message", comment: "")
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            return
        } else if (!RoasterManager.instance().roaster.hasAboveSensor() &&
            (profile?.tempSensor == TempSensorAbove || profile?.tempSensor == TempSensorAboveRobust)) {
            
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$profile_unsupported", comment: "")
            confirmationDialogView.subtitle = ""
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            return
        }
        RoasterManager.instance().sendProfile(profile)
    }
    
    @objc func didLogout() {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }

    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /*
        if segue.identifier == "ProfileEditVC" {
            let nc = segue.destination as! UINavigationController
            let vc = nc.topViewController as! ProfileEditVC
            vc.profile = editedProfile
            vc.callback = {(profile: Profile) in
                self.showTemperatureGraph()
            }
        }
 */
    }
    
}



extension IKChartVC : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        updateAppearance()
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        updateAppearance()
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        updateAppearance()
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        guard AppCore.sharedInstance.roastManager.currentRoast != nil else {
            if let tempChartView = tempChartView, tempChartView.tempBelowSet.values.count > 0 {
                //clear chart if roasting is done
                tempChartView.tempBelowSet.clear()
                tempChartView.chartView.notifyDataSetChanged()
            }
            return
        }
        updateChartIfNeeded()
        updateAppearance()
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        
        self.view.hideToastActivity()
        if (self.isProfileSendingToRoaster) {
            self.isProfileSendingToRoaster = false
            statusBarView.isSendingProfile = false
            sendToRoastButton.isHidden = false
            sendToRoastButton.setTitle(NSLocalizedString("$roast_this_recipe", comment: ""), for: .normal)
            let key = "hideSendReceiptConfirmationDialog" + (AppCore.sharedInstance.firebaseManager.currentUser?.uid ?? "")
            if !UserDefaults.standard.bool(forKey: key) {
                let confirmationDialogView = ButtonsPopupModalView()
                confirmationDialogView.layout = .vertical
                confirmationDialogView.title = NSLocalizedString("$press_roast_button_to_start_roasting", comment: "")
                confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                    
                }
                confirmationDialogView.setUpCheckBox(title: nil) {
                    [unowned confirmationDialogView] in
                    AppCore.sharedInstance.settingsManager.confirmationDialog(name: "hideSendReceiptConfirmationDialog", hide: confirmationDialogView.isCheckBoxSelected )
                }
                IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            }

        }
        updateAppearance()
    }
}
