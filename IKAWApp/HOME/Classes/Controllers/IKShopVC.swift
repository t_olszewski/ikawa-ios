//
//  IKShopVC.swift
//  IKAWA-Home
//
//  Created by Admin on 11/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import WebKit

class IKShopVC: UIViewController {

    var webView: WKWebView!
    var lastUserToken: String?
    var noInternetConnectionLabel: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        self.view.addSubview(webView)
        webView.navigationDelegate = self
        webView.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.edges.equalTo(self.view.safeAreaLayoutGuide)
            } else {
                make.edges.equalTo(self.view)
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadIfNeeded()
    }
    

    func reloadIfNeeded() {
        guard let user = AppCore.sharedInstance.firebaseManager.currentUser else {
            return
        }
        
        user.getIDToken { (token, error) in
            if error == nil && !(token?.isEmpty ?? true) {
                guard let url = URL(string: kAuthenticationUrl + token! + "&url=" + kShopUrl + (Configuration.environment == "DEV" ? "&dev=1" : "")) else {
                    return //be safe
                }
                let request = URLRequest(url: url)
                self.webView.load(request)
                self.lastUserToken = token
            }
            
        }
    }
    
    func openCoffee(url: String) {
        guard let user = AppCore.sharedInstance.firebaseManager.currentUser else {
            return
        }
        
        user.getIDToken { (token, error) in
            if error == nil && !(token?.isEmpty ?? true) {
                    guard let url = URL(string: kAuthenticationUrl + token! + "&url=" + url + (Configuration.environment == "DEV" ? "&dev=1" : "")) else {
                        return //be safe
                    }
                    let request = URLRequest(url: url)
                    self.webView.load(request)
                    self.lastUserToken = token
            }
        }
    }
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */



extension IKShopVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.webView.makeToastActivity(.center)
        if noInternetConnectionLabel != nil {
            noInternetConnectionLabel?.removeFromSuperview()
            noInternetConnectionLabel = nil
        }
        
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.webView.hideToastActivity()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.webView.hideToastActivity()
        
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        self.webView.hideToastActivity()
        if noInternetConnectionLabel == nil {
            noInternetConnectionLabel = UILabel()
            noInternetConnectionLabel?.textAlignment = .center
            noInternetConnectionLabel?.attributedText = NSLocalizedString("No internet connection...", comment: "").attributedString()
            self.webView.addSubview(noInternetConnectionLabel!)
            noInternetConnectionLabel?.snp.makeConstraints({ (make) in
                make.edges.equalTo(webView).inset(UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20))
            })
        }
    }
}
