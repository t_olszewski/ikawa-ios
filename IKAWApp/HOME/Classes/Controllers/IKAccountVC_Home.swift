//
//  IKAccountVC_Home.swift
//  IKAWA-Pro
//
//  Created by Admin on 8/5/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class IKAccountVC_Home: IKAccountVC {
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField?
    override func viewDidLoad() {
        super.viewDidLoad()

        firstNameTextField.text = ""
        lastNameTextField?.text = ""
        
        AppCore.sharedInstance.firebaseManager.getUserInfo { (userInfo) in
            self.firstNameTextField.text = userInfo?.firstName ?? ""
            self.lastNameTextField?.text = userInfo?.lastName ?? ""
        }
        
    }
    

    override func saveNameIfNeeded(callback:@escaping ((_ error: Error?)->Void)) {
        guard let user = AppCore.sharedInstance.firebaseManager.currentUser else {
            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "user not initialized"])
            callback(error)
            return
        }
        
        var firstName = ""
        var lastName = ""
        
        AppCore.sharedInstance.firebaseManager.getUserInfo { (userInfo) in
            firstName = userInfo?.firstName ?? ""
            lastName = userInfo?.lastName ?? ""
            if firstName != self.firstNameTextField.text || lastName != self.lastNameTextField?.text {
                AppCore.sharedInstance.loggingManager.updateUserInfo(firstName: self.firstNameTextField.text ?? "", lastName: self.lastNameTextField?.text ?? "", accountName: "", userType: nil, callback: {(success, message) in
                    self.view.hideToast()
                    if !success {
                        if let message = message {
                            let popup = ButtonsPopupModalView()
                            popup.layout = .vertical
                            popup.title =  message
                            popup.subtitle = ""
                            popup.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                            
                            IKNotificationManager.sharedInstance.show(view: popup)
                            let error = NSError(domain:"com.ikawa", code:401, userInfo:[NSLocalizedDescriptionKey: "user not initialized"])
                            callback(error)
                        }
                        return
                        
                    } else {
                        self.nameWasChanged = true
                        callback(nil)
                    }
                })
            } else {
                callback(nil)
            }
        }
        
    }

}
