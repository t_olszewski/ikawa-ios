//
//  IKForgotPasswordVC.swift
//  IKAWA-Home
//
//  Created by Admin on 12/6/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class IKForgotPasswordVC: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var signInLabel: UILabel!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    
    var callback : ((_ email: String) -> ())?
    @IBOutlet weak var loginBackgroundImageView: UIImageView!
    @IBOutlet weak var signInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        #if TARGET_HOME
        signInButton.layer.cornerRadius = signInButton.frame.height / 2
        signInButton.layer.borderWidth = 2
        signInButton.layer.borderColor = kColorRed.cgColor
        #endif
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    // MARK: - Action
    @IBAction func signInButtonTouchUpInside(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        #if TARGET_PRO
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backAction)))
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        goButton.layer.cornerRadius = goButton.frame.height / 2
        emailTextField.text = UserDefaults.standard.string(forKey: "lastUsedEmail")
        emailTextField.delegate = self
        setupEmailPlaceholder()
        signInButton.layer.cornerRadius = signInButton.frame.height / 2
        emailTextField.setLeftPaddingPoints(17)
        emailTextField.layer.masksToBounds = true
        emailTextField.layer.cornerRadius = 8
        #endif
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    private func setupEmailPlaceholder() {
        let attributes = [
            NSAttributedString.Key.foregroundColor: kColorLightGrey3,
            NSAttributedString.Key.font : UIFont(name: "AvenirNext-Regular", size: 14)
        ]
        emailTextField.attributedPlaceholder = NSAttributedString(string: "$login_email".localized, attributes: attributes as [NSAttributedString.Key : Any])
    }
    
    @objc private func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        if let email = emailTextField.text, !email.isEmpty {
            UserDefaults.standard.set(email, forKey: "lastUsedEmail")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    private func validateEmail() -> Bool {
        guard let email = emailTextField.text  else {
            return false
        }
        
        if (AppCore.sharedInstance.firebaseManager.validateEmailRegEx(email: email)) {
            return true
        } else {
            let confirmationDialogView = ButtonsPopupModalView()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$invalid_email_title", comment: "")
            confirmationDialogView.subtitle = NSLocalizedString("$invalid_email_message", comment: "")
            confirmationDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                //return false
            }
            confirmationDialogView.show(animated: true) {
            }
            return false
        }
    }
    
    @IBAction func goButtonTouchUpInside(_ sender: Any) {
        guard let email = emailTextField.text  else {
            return
        }
        
        guard validateEmail() else {
            return
        }
        
        view.endEditing(true)
        AppCore.sharedInstance.firebaseManager.firebaseResetPassword(email: email, callback: {[weak self] (success, message) in
            self?.view.hideToastActivity()
            if success {
                UIView.animate(withDuration: 0.3, animations: {
                    self?.firstView.layer.opacity = 0
                    self?.firstView.alpha = 0
                    self?.secondView.layer.opacity = 1
                    self?.secondView.alpha = 1
                    self?.firstView.isHidden = true
                    self?.secondView.isHidden = false
                    self?.stackView.layoutIfNeeded()
                })
            }
            }, makeToastActivity: { [weak self] in
                self?.view.makeToastActivity(.center)
        })
        
    }
    /* 
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension IKForgotPasswordVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.placeholder = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.goButtonTouchUpInside(self)
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        #if TARGET_PRO
        setupEmailPlaceholder()
        #endif
    }
}
