//
//  IKMenuVC_Home.swift
//  IKAWApp
//
//  Created by Admin on 3/21/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib
import SDWebImage
import FirebaseUI
import Alamofire
import MessageUI

class IKMenuVC: IKBaseVC {
    let menuItems: Array<(title: String, image: String)> = [
        (NSLocalizedString("$menu_settings", comment: ""), "HomeMenuSettingsIcon"),
        //(NSLocalizedString("$menu_account", comment: ""), "HomeMenuAccountIcon"),
        
        //(NSLocalizedString("$menu_hot_air_community", comment: ""), "HomeMenuHotAirCommunityIcon"),
        (NSLocalizedString("$menu_shop", comment: ""), "ic_shop_side_menu"),
        (NSLocalizedString("$menu_information", comment: ""), "MenuInfoIcon"),
        (NSLocalizedString("$menu_feedback", comment: ""), "FeedbackMenuIcon"),
        (NSLocalizedString("$log_out", comment: ""), "logout")
    ]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var simulatorSwitch: UISwitch!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileEmail: UILabel!
    @IBOutlet weak var centerYConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraintProfileImage: NSLayoutConstraint!
    let userPlaceholderImage = UIImage(named: "MenuImagePlaceholder")
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBar.barStyle = .black
        #if TARGET_DEV
        versionLabel.text = "Version: \((Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String) + "  Build:") \(Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String)"
        #else
        versionLabel.text = ""
        #endif

        RoasterManager.instance().delegates.add(self)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let headerView = tableView.tableHeaderView {
                headerView.frame = CGRect(x: headerView.frame.origin.x, y: headerView.frame.origin.y, width: headerView.frame.width, height: 120)
            }
        }
        
        if AppCore.sharedInstance.firebaseManager.currentUser?.email == nil {
            tableView.tableHeaderView = nil
        } else {
            profileImage.layer.cornerRadius = profileImage.bounds.width / 2
            profileImage.layer.masksToBounds = true
            
            profileImage.image = UIImage(named: "UserPlaceholder")
            profileName.text = ""
            AppCore.sharedInstance.firebaseManager.getUserInfo { (userInfo) in
                if let userInfo = userInfo {
                    self.profileName.text = (userInfo.firstName ?? "") + " " + (userInfo.lastName ?? "")
                }
            }
            //            IKFirebaseManager.sharedInstance.currentUser?.reload(completion: { (error) in
            //                self.profileName.text = (IKFirebaseManager.sharedInstance.currentUser?.displayName ?? "User Name")
            //            })
            profileEmail.text = AppCore.sharedInstance.firebaseManager.currentUser?.email ?? ""
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.layoutIfNeeded()
        centerYConstraint.constant = -20
        leadingConstraintProfileImage.constant = 16
        view.updateConstraintsIfNeeded()
        loadAvatar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profileImage.layer.cornerRadius = profileImage.bounds.width / 2
    }
    
    deinit {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    // MARK: - Private
    func loadAvatar() {
        guard let userId = AppCore.sharedInstance.firebaseManager.currentUser?.uid else {
            return
        }
        CasheManager.sharedInstance.getAvatarImage { (image, error) in
            if let image = image {
                self.profileImage.image = image
            } else {
                self.profileImage.image = self.userPlaceholderImage //UIImage(named: "UserPlaceholder")
            }
        }
    }
    
    //MARK: - Actions
    
    @IBAction func tapOnAccount(_ sender: Any) {
        dismiss(animated: true, completion: {
            
        })
        NavigationHelper.GoToAccountScreen()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension IKMenuVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let titleLabel = cell.viewWithTag(11) as! UILabel
        titleLabel.text = menuItems[indexPath.row].title
        let thumbnail = cell.viewWithTag(12) as! UIImageView
        
        thumbnail.image = UIImage.init(named: menuItems[indexPath.row].image)
        let bgColorView = UIView()
        bgColorView.backgroundColor = kColorGrey
        cell.selectedBackgroundView = bgColorView
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch menuItems[indexPath.row].title {
        case NSLocalizedString("$menu_settings", comment: ""): //settings
            AppCore.sharedInstance.loggingManager.trackEvent(name: "Open Settings", label: "")
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
        case NSLocalizedString("$menu_hot_air_community", comment: ""): //Hot Air Community
            //            guard let user = IKFirebaseManager.sharedInstance.currentUser else {
            //                return
            //            }
            
            guard let url = URL(string: "http://community.ikawacoffee.com") else {
                return //be safe
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
            
            //            user.getIDToken { (token, error) in
            //                if error == nil && !(token?.isEmpty ?? true) {
            //                    guard let url = URL(string: "https://www.ikawacoffee.com/firebase-auth.php?url=http://community.ikawacoffee.com&token=" + token!) else {
            //                        return //be safe
            //                    }
            //
            //                    if #available(iOS 10.0, *) {
            //                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //                    } else {
            //                        UIApplication.shared.openURL(url)
            //                    }
            //                }
            //
            //            }
            
            dismiss(animated: true, completion: {
                
            })
            
        case NSLocalizedString("$menu_shop", comment: ""): //menu iKAWA at home
            guard let user = AppCore.sharedInstance.firebaseManager.currentUser else {
                return
            }
            NavigationHelper.openShop()
            //            user.getIDToken { (token, error) in
            //                if error == nil && !(token?.isEmpty ?? true) {//https://www.ikawacoffee.com/at-home/shop/
            //                    guard let url = URL(string: kAuthenticationUrl + token!) else {
            //                        return //be safe
            //                    }
            //
            //                    if #available(iOS 10.0, *) {
            //                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //                    } else {
            //                        UIApplication.shared.openURL(url)
            //                    }
            //                }
            //
            //            }
            
            dismiss(animated: true, completion: {
                
            })
            
        case NSLocalizedString("$menu_information", comment: ""): //info
            
            
            dismiss(animated: true, completion: {
                NavigationHelper.GoToTutorialScreen()
            })
            
        case NSLocalizedString("$menu_feedback", comment: ""): //info
            dismiss(animated: true, completion: {
                self.createFeedback()
            })
            
            
        case NSLocalizedString("$log_out", comment: ""): //info
            if NetworkReachabilityManager()?.isReachable ?? false {
                if UserDefaults.standard.bool(forKey: "db_changed") {
                    UIApplication.shared.keyWindow?.makeToastActivity(.center)
                    AppCore.sharedInstance.syncManager.stopSynchronization()
                    AppCore.sharedInstance.syncManager.uploadUserLibrary(roundTimestamp: false, callback: { (error) in
                        if let error = error {
                            let errorDialogView = ButtonsPopupModalView()
                            errorDialogView.layout = .vertical
                            errorDialogView.title = NSLocalizedString("$cropster_error_title", comment: "")
                            errorDialogView.subtitle = error.localizedDescription
                            errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {}
                            
                            IKNotificationManager.sharedInstance.show(view: errorDialogView)
                        }
                        UIApplication.shared.keyWindow?.hideToastActivity()
                        self.logout()
                        
                    })
                    
                } else {
                    AppCore.sharedInstance.syncManager.stopSynchronization()
                    logout()
                }
                
            } else {
                let errorDialogView = ButtonsPopupModalView()
                errorDialogView.layout = .vertical
                errorDialogView.title = NSLocalizedString("$no_internet_connection", comment: "")
                errorDialogView.subtitle = ""
                errorDialogView.setUpButton(index: .First, title: NSLocalizedString("$ok", comment: ""), type: .Bordered) {
                }
                IKNotificationManager.sharedInstance.show(view: errorDialogView)
            }
        default: break
            
        }
    }
    
    
    
    func logout() {
        UserDefaults.standard.removeObject(forKey: "tutorialWasShowed")
        UserDefaults.standard.synchronize()
        AppCore.sharedInstance.firebaseManager.firebaseLogout()
        dismiss(animated: true, completion: {
            
        })
        clearTempFolder()
        AppCore.sharedInstance.profileManager.profiles = [Profile]()
        NavigationHelper.GoToLoginScreen()
        if let appDelegate  = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.unregisterPushNotifications()
        }
        NotificationCenter.default.post(name: Notification.Name.DidLogOut, object: nil)
    }
}

extension IKMenuVC : RoasterManagerDelegate {
    //MARK: - RoasterManagerDelegate
    
    func roasterDidConnect(_ roaster: Roaster!) {
        print("!!!!!!!!!  roasterDidConnect")
        tableView.reloadData()
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        print("!!!!!!!!!  roasterDidDisconnect")
        tableView.reloadData()
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
    }
}


extension IKMenuVC : MFMailComposeViewControllerDelegate {
    func createFeedback() {
        if MFMailComposeViewController.canSendMail() {
            if let mailComposeViewController = configuredMailComposeViewController() {
                let appDelegate  = UIApplication.shared.delegate as! AppDelegate
                let viewController = appDelegate.window!.rootViewController!
                viewController.present(mailComposeViewController, animated: true, completion: nil)
            }
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController? {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setSubject("Home App User Feedback")
        mailComposerVC.setToRecipients(["HomeAppFeedback@ikawacoffee.com"])
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let alert = UIAlertController(title: "Error", message: "No mail editor configured on device.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
        }))
        let vc = UIApplication.shared.keyWindow?.rootViewController
        vc!.present(alert, animated: true, completion: {})
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case .cancelled, .saved:
            break
        case .sent:
            let alert = UIAlertController(title: "Success", message: "The csv data was sent successfully.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
            }))
            let vc = UIApplication.shared.keyWindow?.rootViewController
            vc!.present(alert, animated: true, completion: {})
            AppCore.sharedInstance.loggingManager.trackUIAction(action: "Roast CSV Mailed", label: "")
        case .failed:
            let alert = UIAlertController(title: "Error", message: "The message could not be send.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
            }))
            let vc = UIApplication.shared.keyWindow?.rootViewController
            vc!.present(alert, animated: true, completion: {})
        }
        controller.dismiss(animated: true, completion: nil)
        
    }
}



