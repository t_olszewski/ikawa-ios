//
//  NavigationHelper.swift
//  IKAWA-Home
//
//  Created by Admin on 10/26/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class NavigationHelper: NSObject {

    class func GoToLoginScreen() {
        UserDefaults.standard.removeObject(forKey: "tutorialWasShowed")
        UserDefaults.standard.synchronize()

        IKInterface.instance()?.disconnect()
        IKInterface.instance()?.stopScan()
        let accountStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let accountNC = accountStoryboard.instantiateInitialViewController()
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = accountNC
    }
    
    class func GoToMainScreen() {
        if !UserDefaults.standard.bool(forKey: "tutorialWasShowed") {
            NavigationHelper.GoToTutorialScreen()
        } else {
            IKInterface.instance()?.connect()
            let accountStoryboard = UIStoryboard(name: "Main_Home", bundle: nil)
            let accountNC = accountStoryboard.instantiateInitialViewController()
            let appDelegate  = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = accountNC
        }

    }
    
    
    class func GoToAccountScreen() {
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        //        if let tabBarController = appDelegate.window!.rootViewController as? IKTabBarController {
        //            tabBarController.openAccount()
        //        }
        //
        let accountStoryboard = UIStoryboard(name: "Account", bundle: nil)
        let accountVC = accountStoryboard.instantiateViewController(withIdentifier: "AccountVC")
        appDelegate.window!.rootViewController?.present(accountVC, animated: true, completion: {
            
        })
    }
    
    class func GoToTutorialScreen() {
        UserDefaults.standard.set(true, forKey: "tutorialWasShowed")
        UserDefaults.standard.synchronize()
        
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        if let tabBarController = appDelegate.window!.rootViewController as? IKTabBarController {
            tabBarController.openTutorial()
        } else {
            let tutorialStoryboard = UIStoryboard(name: "Tutorial", bundle: nil)
            let tutorialNC = tutorialStoryboard.instantiateInitialViewController()
            appDelegate.window!.rootViewController = tutorialNC
            IKNotificationManager.sharedInstance.update()
        }
    }
    
    class func openProfile(profile: Profile) {
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        var profileLibVC: IKProfileLibVC_Home?
        if let tabbar = appDelegate.window!.rootViewController as? IKTabBarController {
            tabbar.selectItem(index: TabBarItem.home.rawValue)
            if let nc = tabbar.viewControllers?[TabBarItem.home.rawValue] as? UINavigationController  {
                nc.popToRootViewController(animated: false)
                profileLibVC = nc.viewControllers[0] as? IKProfileLibVC_Home
            }
        } else {
            GoToMainScreen()
            if let tabbar = appDelegate.window!.rootViewController as? IKTabBarController {
                tabbar.selectItem(index: TabBarItem.home.rawValue)
                if let nc = tabbar.viewControllers?[TabBarItem.home.rawValue] as? UINavigationController  {
                    nc.popToRootViewController(animated: false)
                    profileLibVC = nc.viewControllers[0] as? IKProfileLibVC_Home
                }
            }
            
        }
        
        if let profileLibVC = profileLibVC {
            profileLibVC.performSegue(withIdentifier: "Chart", sender: profile)
        }

    }
    
    class func openShopWithCoffee(url: String) {
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        var shopVC: IKShopVC?
        if let tabbar = appDelegate.window!.rootViewController as? IKTabBarController {
            tabbar.selectItem(index: TabBarItem.shop.rawValue)
            if let nc = tabbar.viewControllers?[TabBarItem.shop.rawValue] as? UINavigationController  {
                nc.popToRootViewController(animated: false)
                shopVC = nc.viewControllers[0] as? IKShopVC
            }
        } else {
            GoToMainScreen()
            if let tabbar = appDelegate.window!.rootViewController as? IKTabBarController {
                tabbar.selectItem(index: TabBarItem.shop.rawValue)
                if let nc = tabbar.viewControllers?[TabBarItem.shop.rawValue] as? UINavigationController  {
                    nc.popToRootViewController(animated: false)
                    shopVC = nc.viewControllers[0] as? IKShopVC
                }
            }
            
        }
        
        if let shopVC = shopVC {
            shopVC.openCoffee(url: url)
        }
    }
    
    class func openShop() {
        guard let user = AppCore.sharedInstance.firebaseManager.currentUser else {
            return
        }
        
        user.getIDToken { (token, error) in
            if error == nil && !(token?.isEmpty ?? true) {
                guard let url = URL(string: kAuthenticationUrl + token! + "&url=" + kShopUrl + (Configuration.environment == "DEV" ? "&dev=1" : "")) else {                    return //be safe
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            
        }
        
        /*
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        var shopVC: IKShopVC?
        if let tabbar = appDelegate.window!.rootViewController as? IKTabBarController {
            tabbar.selectItem(index: TabBarItem.shop.rawValue)
            if let nc = tabbar.viewControllers?[TabBarItem.shop.rawValue] as? UINavigationController  {
                nc.popToRootViewController(animated: false)
                shopVC = nc.viewControllers[0] as? IKShopVC
            }
        } else {
            GoToMainScreen()
            if let tabbar = appDelegate.window!.rootViewController as? IKTabBarController {
                tabbar.selectItem(index: TabBarItem.shop.rawValue)
                if let nc = tabbar.viewControllers?[TabBarItem.shop.rawValue] as? UINavigationController  {
                    nc.popToRootViewController(animated: false)
                    shopVC = nc.viewControllers[0] as? IKShopVC
                }
            }
            
        }
 */
    }
    
    class func highlightRoastTab() {
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        if let tabbar = appDelegate.window!.rootViewController as? IKTabBarController {
            tabbar.highlightTab(index: TabBarItem.roast.rawValue)
        }
    }
    
    class func highlightHomeTab() {
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        if let tabbar = appDelegate.window!.rootViewController as? IKTabBarController {
            tabbar.highlightTab(index: TabBarItem.home.rawValue)
        }
    }
    
}
