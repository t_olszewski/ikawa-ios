//
//  IKTypes.h
//  Ikawa
//
//  Created by Tijn Kooijmans on 18/04/14.
//
//

#ifndef Ikawa_IKTypes_h
#define Ikawa_IKTypes_h

typedef enum RoasterState
{
    IK_STATUS_UNKNOWN = -1,
    IK_STATUS_IDLE = 0,
    IK_STATUS_HEATING = 1,
    IK_STATUS_READY = 2,
    IK_STATUS_ROASTING = 3,
    IK_STATUS_BUSY = 4,
    IK_STATUS_COOLING = 5,
    IK_STATUS_OPEN = 6,
    IK_STATUS_PROBLEM = 7,
    IK_STATUS_READY_TO_BLOWOVER = 8,
    IK_STATUS_TEST_MODE = 9,
    IK_STATUS_SWAP_JARS = 0xF1,
    IK_STATUS_NEEDS_UPDATE = 0xFF
} RoasterState;

typedef enum RoasterError {
    RoasterErrorNone = 1,
    RoasterErrorBoardTooHot = 2,
    RoasterErrorAboveBeansTooHot = 3,
    RoasterErrorAboveBeansTooCold = 4,
    RoasterErrorFanNotSpinning = 5,
    RoasterErrorMotorFailure = 6,
    RoasterErrorBelowBeansTooHot = 7,
    RoasterErrorBelowBeansTooCold = 8
} RoasterError;

typedef enum RoasterVariant {
    RoasterVariantPRO = 0,
    RoasterVariantNESPRESSO = 1,
    RoasterVariantBARE = 2,
    RoasterVariantHOME = 3
} RoasterVariant;

typedef enum RoasterType {
    RoasterTypeV1 = 0,
    RoasterTypeV2 = 1,
    RoasterTypeV3 = 2,
    RoasterTypeV4 = 3,
    RoasterTypeV5 = 4,
} RoasterType;

typedef enum TempSensor {
    TempSensorAbove = 0,
    TempSensorBelow = 1,
    TempSensorAboveRobust = 2,
} TempSensor;

typedef enum SettingsType {
    SettingsTypeUint8 = 1,
    SettingsTypeUint16 = 2,
    SettingsTypeUint32 = 3,
    SettingsTypeFloat = 4,
} SettingsType;

typedef enum AboveSensorType {
    AboveSensorTypeUnset = 0,
    AboveSensorTypeFast = 1,
    AboveSensorTypeRobust = 2
} AboveSensorType;

typedef enum RoasterVoltage {
    RoasterVoltageUnset = 0,
    RoasterVoltage100 = 100,
    RoasterVoltage120 = 120,
    RoasterVoltage230 = 230
} RoasterVoltage;

typedef enum RoasterSettingId {
    SettingIdRoasterVoltage = 153,
    SettingIdAboveSensorType = 154,
    SettingIdBlowoverHeaterPower = 155,
    SettingIdTimeMultiplier = 158,
    SettingIdFanOpenLoopControl = 171,
    SettingIdLastServiceDate = 174,
    SettingIdRoastCountSinceService = 175
    
} RoasterSettingId;

#endif
