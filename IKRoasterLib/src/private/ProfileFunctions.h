//
//  ProfileFunctions.h
//  FFGlobalAlertController
//
//  Created by Tijn Kooijmans on 05/03/2018.
//

#import <Foundation/Foundation.h>
#include "spec.pb.h"

@class Profile;

@interface ProfileFunctions : NSObject

+ (RoastProfile)convertToProtobuf:(Profile*)profile;
+ (Profile*)convertFromProtobuf:(RoastProfile)profile;

@end
