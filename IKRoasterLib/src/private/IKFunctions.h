//
//  IKFunctions.h
//  IkawaTool
//
//  Created by Tijn Kooijmans on 24/09/14.
//  Copyright (c) 2014 Studio Sophisti. All rights reserved.
//

#ifndef __IkawaTool__IKFunctions__
#define __IkawaTool__IKFunctions__

#include <stdio.h>
#include <string>

uint16_t crc16(char* data, int length, uint16_t initialValue);
std::string asciiToHex(std::string string);
void hexToAscii(const std::string &in, std::string &out);

#endif /* defined(__IkawaTool__IKFunctions__) */
