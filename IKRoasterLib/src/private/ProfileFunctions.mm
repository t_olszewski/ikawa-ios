//
//  ProfileFunctions.m
//  FFGlobalAlertController
//
//  Created by Tijn Kooijmans on 05/03/2018.
//

#import "ProfileFunctions.h"
#import "Profile.h"
#include "IKFunctions.h"
#include "spec.pb.h"

@implementation ProfileFunctions

+ (RoastProfile)convertToProtobuf:(Profile*)profile
{
    RoastProfile pb;
    pb.set_name(std::string([profile.name UTF8String]));
    
    RoastProfileExtension *roastProfileExtension = new RoastProfileExtension();
    
    roastProfileExtension->set_user_id(std::string([(profile.userId ? profile.userId : @"")  UTF8String]));
    roastProfileExtension->set_coffee_name(std::string([(profile.coffeeName ? profile.coffeeName : @"") UTF8String]));
    roastProfileExtension->set_coffee_id(std::string([(profile.coffeeId ? profile.coffeeId : @"") UTF8String]));
    roastProfileExtension->set_coffee_web_url(std::string([(profile.coffeeWebUrl ? profile.coffeeWebUrl : @"") UTF8String]));
    roastProfileExtension->set_profile_type(profile.type);
    
    pb.set_allocated_profile_extension(roastProfileExtension);
    
    pb.set_schema(profile.schemaVersion);
    
    std::string uuid = std::string([profile.uuid UTF8String]);
    uuid.erase (std::remove(uuid.begin(), uuid.end(), ' '), uuid.end());
    uuid.erase (std::remove(uuid.begin(), uuid.end(), '-'), uuid.end());
    
    std::string _uuidAscii;
    hexToAscii(uuid, _uuidAscii);
    pb.set_id(_uuidAscii);
    
    if (profile.tempSensor == TempSensorBelow) {
        pb.set_temp_sensor(RoastProfile_TempSensor_BELOW_BEANS);
    } else {
        pb.set_temp_sensor(RoastProfile_TempSensor_ABOVE_BEANS);
    }
    
    int i=0;
    for (RoastTempPoint *point in profile.roastPoints)
    {
        if (i==0)
            point.time = 0;
        
        TempPoint *tp = pb.add_temp_points();
        tp->set_time(point.time * 10);
        tp->set_temp(point.temperature * 10);
        
        ++i;
    }
    
    i = 0;
    for (RoastFanPoint *point in profile.fanPoints)
    {
        if(i==0)
            point.time = 0;
        
        FanPoint *fp = pb.add_fan_points();
        fp->set_time(point.time * 10);
        fp->set_power(point.power / 100.0 * 255);
        
        ++i;
    }
    
    FanPoint *cooldownFp = new FanPoint();
    cooldownFp->set_time(profile.cooldownPoint.time * 10);
    cooldownFp->set_power(profile.cooldownPoint.power / 100 * 255);
    pb.set_allocated_cooldown_fan(cooldownFp);
    
    return pb;
}

+ (Profile*)convertFromProtobuf:(RoastProfile)pb
{
    Profile *profile = [[Profile alloc] init];
    
    // Error handling
    if(pb.name().length() == 0) return nil; // No name
    //if(pb.id().length() == 0) return false; // No id
    if(pb.temp_points_size() < 2) return nil; // Too few temp points
    if(pb.fan_points_size() < 2) return nil; // Too few fan points
    
    profile.name = [NSString stringWithUTF8String:pb.name().c_str()];
    
    if (pb.profile_extension().ByteSize() != 0) {
        profile.coffeeName = (pb.profile_extension().coffee_name().length() == 0) ? @"" : [NSString stringWithUTF8String:pb.profile_extension().coffee_name().c_str()];
        profile.coffeeId = (pb.profile_extension().coffee_id().length() == 0) ? @"" : [NSString stringWithUTF8String:pb.profile_extension().coffee_id().c_str()];
        profile.coffeeWebUrl = (pb.profile_extension().coffee_web_url().length() == 0) ? @"" : [NSString stringWithUTF8String:pb.profile_extension().coffee_web_url().c_str()];
        profile.userId = (pb.profile_extension().user_id().length() == 0) ? @"" : [NSString stringWithUTF8String:pb.profile_extension().user_id().c_str()];
        profile.type = (ProfileType)pb.profile_extension().profile_type();
    } else {
        profile.type = ProfileTypeUndefined;
    }

    profile.schemaVersion = pb.schema();
    profile.uuid = [NSString stringWithUTF8String:asciiToHex(pb.id()).c_str()];
    profile.tempSensor = (TempSensor)pb.temp_sensor();
    
    NSMutableArray *roastPoints = [NSMutableArray arrayWithCapacity:20];
    NSMutableArray *fanPoints = [NSMutableArray arrayWithCapacity:20];
    for(int i=0; i<pb.temp_points_size(); i++)
    {
        RoastTempPoint *rp = [[RoastTempPoint alloc] init];
        rp.time = pb.temp_points(i).time() / 10.0f;
        rp.temperature = pb.temp_points(i).temp() / 10.0f;
        [roastPoints addObject:rp];
    }
    
    for(int i=0; i<pb.fan_points_size(); i++)
    {
        RoastFanPoint *fp = [[RoastFanPoint alloc] init];
        fp.time = pb.fan_points(i).time() / 10.0f;
        fp.power = pb.fan_points(i).power() / 255.0f * 100;
        [fanPoints addObject:fp];
    }
    profile.roastPoints = roastPoints;
    profile.fanPoints = fanPoints;
    profile.cooldownPoint = [[RoastFanPoint alloc] init];
    profile.cooldownPoint.time = pb.cooldown_fan().time() / 10.0f;
    profile.cooldownPoint.power = pb.cooldown_fan().power() / 255.0f * 100;
    
    return profile;
}

@end
