//
//  Roaster.h
//  IkawaTool
//
//  Created by Tijn Kooijmans on 15/09/14.
//  Copyright (c) 2014 Studio Sophisti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IKTypes.h"

@class RoasterStatus;

@interface Roaster : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) int roasterId;
@property (nonatomic, assign) int firmwareVersion;
@property (nonatomic, strong) NSString *firmwareBuild;
@property (nonatomic, assign) RoasterVariant variant;
@property (nonatomic, assign) RoasterType type;
@property (nonatomic, assign) int roastCount;
@property (nonatomic, assign) int roastCountSinceService;
@property (nonatomic, strong) RoasterStatus *status;
@property (nonatomic, assign) int error;
@property (nonatomic, assign) int testStatus;
@property (nonatomic, assign) int currentTest;
@property (nonatomic, assign) RoasterVoltage roasterVoltage;
@property (nonatomic, assign) AboveSensorType aboveSensorType;
@property (nonatomic, assign) float timeMultiplier;
@property (nonatomic, strong) NSString *fanOpenLoopControl;
@property (nonatomic, strong) NSDate *lastServiceDate;
@property (nonatomic, strong) NSMutableDictionary *settings;

- (NSString*)variantName;
- (NSString*)typeName;
- (NSArray*)sortedSettings;
- (NSString*)errorName;

@end

@interface RoasterStatus : NSObject

@property (nonatomic, assign) float time;
@property (nonatomic, assign) float temp_above;
@property (nonatomic, assign) float temp_below;
@property (nonatomic, assign) float temp_board;
@property (nonatomic, assign) float ror_above;
@property (nonatomic, assign) float fanSet;
@property (nonatomic, assign) RoasterState state;
@property (nonatomic, assign) int heater;
@property (nonatomic, assign) float p;
@property (nonatomic, assign) float i;
@property (nonatomic, assign) float d;
@property (nonatomic, assign) float j;
@property (nonatomic, assign) float setpoint;
@property (nonatomic, assign) int fanSpeed;
@property (nonatomic, assign) float fan_p;
@property (nonatomic, assign) float fan_i;
@property (nonatomic, assign) float fan_d;
@property (nonatomic, assign) int fan_rpm_set;
@property (nonatomic, assign) float fan_power;
@property (nonatomic, assign) int error_code;
@property (nonatomic, assign) bool relay_state;

- (NSString*)stateName;

@end
