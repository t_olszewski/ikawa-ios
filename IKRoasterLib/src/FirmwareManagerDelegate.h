//
//  IKFirmwareManagerDelegate.h
//  HelloCpp
//
//  Created by Tijn Kooijmans 17 april 2014
//
//

#import <Foundation/Foundation.h>

@protocol FirmwareManagerDelegate <NSObject>

@optional
-(void)firmwareProgressChanged:(float) progress;
-(void)firmwareUpdateStarted;
-(void)firmwareUpdateComplete;
-(void)firmwareUpdateFailed;
-(void)firmwareJumpedToApp;

@end