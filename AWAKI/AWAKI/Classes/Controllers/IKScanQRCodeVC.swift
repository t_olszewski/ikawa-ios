//
//  IKScanQRCodeVC.swift
//  AWAKI
//
//  Created by Admin on 12/20/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import QRCodeReader
import SnapKit
import IKRoasterLib

class IKScanQRCodeVC: UIViewController {

    @IBOutlet weak var readerContainerView: UIView!
    var sendingProfileToRoaster: Bool = false
    var sendedProfile: Profile?
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showCancelButton = false
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        readerContainerView.addSubview(readerVC.view)
        readerVC.view.snp.makeConstraints { (maker) in
            maker.edges.equalTo(readerContainerView).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let resultStr = result?.value, let url = URL(string: resultStr) {
                self.handleOpenUrl(url: url)
            }
        }
        RoasterManager.instance().delegates.add(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        readerVC.startScanning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    deinit {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }

    func handleOpenUrl(url: URL) {
     //   let url = URL(string: "ikawahome://profile/?CAESEE4pxSSIz0ibqAmTst+AR9QaAzU1NSIFCAAQ9AMiBgi5AxDIECIGCJ8IEMUTIgYIjw0QjRMiBgiYERCSEiIGCJwcEOkSKgUIABDFASoGCJwcEKIBMAE6BgjJIxDMAUIDNTU1ShxodEhtUzhQQkw1UUlkRENYTDNlSW1WOXlMaEEyUiAwMTAzNzU3NDAzQkE0NzQwQjU3REYyQ0Q0NTdDOTFBOFoWaHR0cDovL2lrYXdhY29mZmVlLmNvbQ==")!
        if (url.absoluteString.range(of: "/profile") != nil) {
            if let query = url.query {
                if (query.count == 32) {
                    //query contains uuid
                    //TODO: implement!
                    showUnsupportedProfileAlert()
                } else {
                    guard let profile = IKProfileManager.sharedInstance.base64DecodeProfile(profile: query) else {
                        showUnsupportedProfileAlert()
                        return
                    }
                    
                    print(profile.name)
                    self.view.makeToastActivity(.center)
                    sendingProfileToRoaster = true
                    sendedProfile = profile
                    IKProfileManager.sharedInstance.sendProfile(profile: profile)
                    //performSegue(withIdentifier: "IKRoastingVC", sender: profile.coffeeWebUrl)
                }
            }
            
        } else {
           showUnsupportedProfileAlert()
        }
    }
    
    func showUnsupportedProfileAlert() {
        let confirmationDialogView = ButtonsPopupModalView.fromNib()
        confirmationDialogView.layout = .vertical
        confirmationDialogView.title = NSLocalizedString("$profile_format_unsupported", comment: "")
        confirmationDialogView.firstButtonTitle = NSLocalizedString("$ok", comment: "")
        confirmationDialogView.secondButtonTitle = ""
        confirmationDialogView.firstButtonCallback = {
            confirmationDialogView.dismiss(animated: true)
            self.readerVC.startScanning()
        }
        confirmationDialogView.secondButtonCallback = {
            confirmationDialogView.dismiss(animated: true)
            self.readerVC.startScanning()
        }
        IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension IKScanQRCodeVC : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {

    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {

    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {

    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        
        self.view.hideToastActivity()
        if (self.sendingProfileToRoaster) {
            self.sendingProfileToRoaster = false
            if let sendedProfile = self.sendedProfile {
                performSegue(withIdentifier: "IKRoastingVC", sender: sendedProfile.coffeeWebUrl)
            }
            
//            let confirmationDialogView = ButtonsPopupModalView.fromNib()
//            confirmationDialogView.layout = .vertical
//            confirmationDialogView.title = NSLocalizedString("$profile_received", comment: "")
//            confirmationDialogView.subtitle = ""
//            confirmationDialogView.firstButtonTitle = NSLocalizedString("$ok", comment: "")
//            confirmationDialogView.secondButtonTitle = ""
//            confirmationDialogView.firstButtonCallback = {
//                confirmationDialogView.dismiss(animated: true)
//            }
//            confirmationDialogView.secondButtonCallback = {
//                confirmationDialogView.dismiss(animated: true)
//            }
//            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            
        }
}
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "IKRoastingVC" {
            let vc = segue.destination as! IKRoastingVC
            vc.url = sender as? String
        }
    }
}
