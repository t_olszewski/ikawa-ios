//
//  MainVC.swift
//  AWAKI
//
//  Created by Admin on 12/20/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class IKRoastrerConnectionVC: UIViewController {

    var lastError: Int32 = Int32(RoasterErrorNone.rawValue)
    @IBOutlet weak var statusLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RoasterManager.instance().delegates.add(self)
        statusLabel.text = NSLocalizedString("$not_connected_title", comment: "")
    }
    
    deinit {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func populate() {
        if navigationController?.topViewController == self {
            if (IKInterface.instance().isBluetoothEnabled() && RoasterManager.instance().roaster != nil) {
                performSegue(withIdentifier: "IKScanQRCodeVC", sender: nil)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension IKRoastrerConnectionVC : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        navigationController?.popToRootViewController(animated: false)
        statusLabel.text = NSLocalizedString("$connecting_title", comment: "")
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        navigationController?.popToRootViewController(animated: false)
        if IKInterface.instance().isBluetoothEnabled() {
            statusLabel.text = NSLocalizedString("$not_connected_title", comment: "")
        } else {
            statusLabel.text = NSLocalizedString("$bluetooth_off_title", comment: "")
        }
        //performSegue(withIdentifier: "IKScanQRCodeVC", sender: nil)
    }
    
    func roaster(_ roaster: Roaster!, didReceiveSetting setting: Int32, withValue value: NSNumber!, success: Bool) {
        
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        if (roaster.error > 1 && roaster.error != lastError) {
            lastError = roaster.error
            let popup = ButtonsPopupModalView.fromNib()
            popup.layout = .vertical
            popup.title = NSLocalizedString("$roaster_issue_title", comment: "")
            popup.subtitle = ""
            popup.firstButtonTitle = NSLocalizedString("$ok", comment: "")
            popup.secondButtonTitle = ""
            popup.firstButtonCallback = {
                popup.dismiss(animated: true)
            }
            popup.secondButtonCallback = {
                popup.dismiss(animated: true)
            }
            IKNotificationManager.sharedInstance.show(view: popup)
        }
        populate()
        
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        
    }
    
    func roasterDidDidReceiveFirmwareVersion(_ roaster: Roaster!) {

    }
}
