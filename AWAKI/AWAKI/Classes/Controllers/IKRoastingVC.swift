//
//  IKRoastingVC.swift
//  AWAKI
//
//  Created by Admin on 12/20/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SnapKit
import WebKit
import IKRoasterLib

class IKRoastingVC: UIViewController {

    @IBOutlet weak var statusBarView: IKStatusBarView!
    var webView: WKWebView!
    var url: String?
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scanButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.navigationDelegate = self
        containerView.addSubview(webView)
        webView.snp.makeConstraints { (maker) in
            maker.margins.equalToSuperview()
        }
        var webUrl = URL(string:"https://www.ikawacoffee.com/at-home/")
        if let url = self.url {
            if let url = URL(string:url) {
                webUrl = url
            }
        }
        if let webUrl = webUrl {
            let request = URLRequest(url: webUrl)
            webView.load(request)
            self.view.makeToastActivity(.center)
        }

        
        RoasterManager.instance().delegates.add(self)
        RoasterManager.instance().delegates.add(statusBarView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    deinit {
        if (RoasterManager.instance().delegates.contains(statusBarView)) {
            RoasterManager.instance().delegates.remove(statusBarView)
        }
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    //MARK: - Actions
    @IBAction func scanButtonTouchUpInside(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func accountButtonTouchUpInside(_ sender: Any) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension IKRoastingVC: WKNavigationDelegate {
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.view.hideToastActivity()
    }
    
    public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
         self.view.hideToastActivity()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.view.hideToastActivity()
    }
    
}

extension IKRoastingVC : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
        
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
        
    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        print(RoasterManager.instance().roaster.status.state)
        
        if (IKInterface.instance().isBluetoothEnabled() && RoasterManager.instance().roaster != nil) {
            let state = roaster.status.state
            if state != IK_STATUS_HEATING &&
            state != IK_STATUS_ROASTING &&
            state != IK_STATUS_COOLING &&
            state != IK_STATUS_READY_TO_BLOWOVER &&
            state != IK_STATUS_SWAP_JARS {
                scanButton.isHidden = false
            } else {
                scanButton.isHidden = true
            }
        }
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        
        
    }
    
}
