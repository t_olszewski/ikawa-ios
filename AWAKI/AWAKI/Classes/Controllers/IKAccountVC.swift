//
//  IKAccountVC_Home.swift
//  IKAWA-Home
//
//  Created by Admin on 10/30/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class IKAccountVC: UIViewController {

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var restoreBackupButton: UIButton!
    @IBOutlet weak var logOutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailLabel.attributedText = (IKFirebaseManager.sharedInstance.currentUser?.email ?? "").attributedString(font: UIFont(name: kFontAvenirNextLTProRegular, size: 16)!, color: kColorGreyDark)
    
//        logOutButton.setAttributedTitle(NSLocalizedString("$log_out", comment: "").attributedString(font: UIFont(name: kFontAvenirNextLTProBold, size: 16)!, color: UIColor.white), for: .normal)
    }
    
    
    @IBAction func logOutButtonTouchUpInside(_ sender: Any) {
        IKFirebaseManager.sharedInstance.firebaseLogout()
        NavigationHelper.GoToLoginScreen()
    }
    
    @IBAction func cancelButtonTouchUpInside(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}


