//
//  IKLoginVC.swift
//  IKAWA-Home
//
//  Created by Никита on 10/10/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Toast_Swift
import IHKeyboardAvoiding

class IKLoginVC: UIViewController {
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var signInLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var dontHaveAccountLabel: UILabel!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        KeyboardAvoiding.avoidingView = loginView
//        loginView.layer.shadowColor = UIColor.black.cgColor
//        loginView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//        loginView.layer.shadowRadius = 3.0
//        loginView.layer.shadowOpacity = 0.5
        emailTextField.defaultTextAttributes = [NSAttributedStringKey.kern.rawValue: 0, NSAttributedStringKey.font.rawValue :UIFont(name: kFontAvenirNextLTProRegular, size: 14)!, NSAttributedStringKey.foregroundColor.rawValue: kColorGreyDark]
        emailTextField.createBottomLine()
        emailTextField.delegate = self
        passwordTextField.defaultTextAttributes = [NSAttributedStringKey.kern.rawValue: 0, NSAttributedStringKey.font.rawValue :UIFont(name: kFontAvenirNextLTProRegular, size: 14)!, NSAttributedStringKey.foregroundColor.rawValue: kColorGreyDark]
        passwordTextField.createBottomLine()
        passwordTextField.delegate = self
    }
    

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: - Actions
    @IBAction func loginButtonTouchUpInside(_ sender: Any) {
        if (self.validateInput()) {
            self.view.makeToastActivity(.center)
            IKFirebaseManager.sharedInstance.firebaseLogin(email: self.emailTextField.text!, password: self.passwordTextField.text!, callback: { (success, message) in
                self.view.hideToastActivity()
                if (!success) {
                    let confirmationDialogView = ButtonsPopupModalView.fromNib()
                    confirmationDialogView.layout = .vertical
                    confirmationDialogView.title = NSLocalizedString("$login_failed", comment: "")
                    confirmationDialogView.subtitle = ""
                    confirmationDialogView.firstButtonTitle = NSLocalizedString("$ok", comment: "")
                    confirmationDialogView.secondButtonTitle = ""
                    confirmationDialogView.firstButtonCallback = {
                        confirmationDialogView.dismiss(animated: true)
                    }
                    confirmationDialogView.secondButtonCallback = {
                        confirmationDialogView.dismiss(animated: true)
                    }
                    IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
                    
                } else {
                   NavigationHelper.GoToMainScreen()
                }
            })
        }
        
    }
    
    @IBAction func forgotPasswordButtonTouchUpInside(_ sender: Any) {
        if (validateEmail()) {
            self.view.makeToastActivity(.center)
            IKFirebaseManager.sharedInstance.firebaseResetPassword(email: self.emailTextField.text!) { (success, message) in
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    if (!success) {
                        
                        let errorDialogView = ButtonsPopupModalView.fromNib()
                        errorDialogView.layout = .vertical
                        errorDialogView.title = NSLocalizedString("$password_reset_failed", comment: "")
                        errorDialogView.subtitle = ""
                        errorDialogView.firstButtonTitle = NSLocalizedString("$ok", comment: "")
                        errorDialogView.secondButtonTitle = ""
                        errorDialogView.firstButtonCallback = {
                            errorDialogView.dismiss(animated: true)
                        }
                        errorDialogView.secondButtonCallback = {
                            errorDialogView.dismiss(animated: true)
                        }
                        IKNotificationManager.sharedInstance.show(view: errorDialogView)
                    } else {
                        self.performSegue(withIdentifier: "IKForgotPasswordVC", sender: nil)
                    }
                }
            }
        }
    }
    
    
    //MARK: - Private
    
    func validateEmail() -> Bool {
        let emailRegEx = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\\]?)$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let valid = emailTest.evaluate(with: self.emailTextField.text)
        
        if (valid) {
            self.emailTextField.textColor = kColorGreyDark
        } else {
            self.emailTextField.textColor = UIColor.red
            
            let confirmationDialogView = ButtonsPopupModalView.fromNib()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$invalid_email_title", comment: "")
            confirmationDialogView.subtitle = NSLocalizedString("$invalid_email_message", comment: "")
            confirmationDialogView.firstButtonTitle = NSLocalizedString("$ok", comment: "")
            confirmationDialogView.secondButtonTitle = ""
            confirmationDialogView.firstButtonCallback = {
                confirmationDialogView.dismiss(animated: true)
            }
            confirmationDialogView.secondButtonCallback = {
                confirmationDialogView.dismiss(animated: true)
            }
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)

        }
        
        return valid
    }
    
    func validatePassword() -> Bool {
        let valid = (self.passwordTextField.text?.count ?? 0) > 7
        if (valid) {
            self.passwordTextField.textColor = kColorGreyDark
        } else {
            self.passwordTextField.textColor = UIColor.red
            
            let confirmationDialogView = ButtonsPopupModalView.fromNib()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$password_too_short_title", comment: "")
            confirmationDialogView.subtitle = NSLocalizedString("$password_too_short_message", comment: "")
            confirmationDialogView.firstButtonTitle = NSLocalizedString("$ok", comment: "")
            confirmationDialogView.secondButtonTitle = ""
            confirmationDialogView.firstButtonCallback = {
                confirmationDialogView.dismiss(animated: true)
            }
            confirmationDialogView.secondButtonCallback = {
                confirmationDialogView.dismiss(animated: true)
            }
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)

        }
        return valid
    }
    
    
    func validateInput() -> Bool {
        if (validateEmail() && validatePassword()) {
            return true
        } else {
            return false
        }
    }
    
    
}

extension IKLoginVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            passwordTextField.resignFirstResponder()
        }
        return true
    }
}
