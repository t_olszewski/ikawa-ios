//
//  IKCreateAccountVC.swift
//  IKAWA-Home
//
//  Created by Никита on 10/10/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class IKCreateAccountVC: UIViewController {

    @IBOutlet weak var сreateAccountView: UIView!
    @IBOutlet weak var createAccountLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var alreadyAnAccountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        KeyboardAvoiding.avoidingView = сreateAccountView
        сreateAccountView.layer.shadowColor = UIColor.black.cgColor
        сreateAccountView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        сreateAccountView.layer.shadowRadius = 3.0
        сreateAccountView.layer.shadowOpacity = 0.5
        emailTextField.defaultTextAttributes = [NSAttributedStringKey.kern.rawValue: 0, NSAttributedStringKey.font.rawValue :UIFont(name: kFontAvenirNextLTProRegular, size: 14)!, NSAttributedStringKey.foregroundColor.rawValue: kColorGreyDark]
        emailTextField.createBottomLine()
        emailTextField.delegate = self
        passwordTextField.defaultTextAttributes = [NSAttributedStringKey.kern.rawValue: 0, NSAttributedStringKey.font.rawValue :UIFont(name: kFontAvenirNextLTProRegular, size: 14)!, NSAttributedStringKey.foregroundColor.rawValue: kColorGreyDark]
        passwordTextField.createBottomLine()
        passwordTextField.delegate = self
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Actions
    
    @IBAction func registerButtonTouchUpInside(_ sender: Any) {
        performSegue(withIdentifier: "IKPolicyVC", sender: nil)
//        if (self.validateInput()) {
//            performSegue(withIdentifier: "IKPolicyVC", sender: nil)
//        }
    }
    
    @IBAction func signInButtonTouchUpInside(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: - Private
    
    
    func validateEmail() -> Bool {
        let emailRegEx = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\\]?)$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let valid = emailTest.evaluate(with: self.emailTextField.text)
        
        if (valid) {
            self.emailTextField.textColor = kColorGreyDark
        } else {
            self.emailTextField.textColor = UIColor.red
            
            let popup = ButtonsPopupModalView.fromNib()
            popup.layout = .vertical
            popup.title = NSLocalizedString("$invalid_email_title", comment: "")
            popup.subtitle = NSLocalizedString("$invalid_email_message", comment: "")
            popup.firstButtonTitle = NSLocalizedString("$ok", comment: "")
            popup.secondButtonTitle = ""
            popup.firstButtonCallback = {
                popup.dismiss(animated: true)
            }
            popup.secondButtonCallback = {
                popup.dismiss(animated: true)
            }
            IKNotificationManager.sharedInstance.show(view: popup)
        }
        
        return valid
    }
    
    func validatePassword() -> Bool {
        let valid = (self.passwordTextField.text?.count ?? 0) > 7
        if (valid) {
            self.passwordTextField.textColor = kColorGreyDark
        } else {
            self.passwordTextField.textColor = UIColor.red
            
            let popup = ButtonsPopupModalView.fromNib()
            popup.layout = .vertical
            popup.title = NSLocalizedString("$password_too_short_title", comment: "")
            popup.subtitle = NSLocalizedString("$password_too_short_message", comment: "")
            popup.firstButtonTitle = NSLocalizedString("$ok", comment: "")
            popup.secondButtonTitle = ""
            popup.firstButtonCallback = {
                popup.dismiss(animated: true)
                
            }
            popup.secondButtonCallback = {
                popup.dismiss(animated: true)
            }
            IKNotificationManager.sharedInstance.show(view: popup)
        }
        return valid
    }
    
    
    func validateInput() -> Bool {
        if (validateEmail() && validatePassword()) {
            return true
        } else {
            return false
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "IKPolicyVC" {
            if let vc = segue.destination as? IKPolicyVC {
                vc.email = self.emailTextField.text!
                vc.password = self.passwordTextField.text!
                
            }
        }
    }
}


extension IKCreateAccountVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            passwordTextField.resignFirstResponder()
        }
        return true
    }
}

