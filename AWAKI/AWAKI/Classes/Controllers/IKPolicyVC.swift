//
//  IKPolicyVC.swift
//  IKAWA-Home
//
//  Created by Admin on 12/7/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class IKPolicyVC: UIViewController {

    var email: String!
    var password: String!
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var goToPolicyButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    // MARK: - Actiond
    
    @IBAction func goToPolicyButtonTouchUpInside(_ sender: Any) {
        //https://7puzn1bawno25nd4f1awju71-wpengine.netdna-ssl.com/wp-content/uploads/2018/05/IKAWA-Privacy-Policy-for-website-FINAL.pdf
        guard let url = URL(string: "https://www.ikawacoffee.com/legal/") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    @IBAction func okButtonTouchUpInside(_ sender: Any) {
        self.view.makeToastActivity(.center)
        IKFirebaseManager.sharedInstance.firebaseRegisterUser(email: email, password: password, callback: { (success, message) in
            DispatchQueue.main.async {
                self.view.hideToastActivity()
            }
            if (!success) {
                let popup = ButtonsPopupModalView.fromNib()
                popup.layout = .vertical
                popup.title = NSLocalizedString("$registration_failed", comment: "")
                popup.subtitle = ""
                popup.firstButtonTitle = NSLocalizedString("$ok", comment: "")
                popup.secondButtonTitle = ""
                popup.firstButtonCallback = {
                    popup.dismiss(animated: true)
                    self.navigationController?.popViewController(animated: true)
                    
                }
                popup.secondButtonCallback = {
                    popup.dismiss(animated: true)
                }
                IKNotificationManager.sharedInstance.show(view: popup)
                
            } else {
                let popup = ButtonsPopupModalView.fromNib()
                popup.layout = .vertical
                popup.title = NSLocalizedString("$registration_success", comment: "")
                popup.subtitle = ""
                popup.firstButtonTitle = NSLocalizedString("$ok", comment: "")
                popup.secondButtonTitle = ""
                popup.firstButtonCallback = {
                    popup.dismiss(animated: true)
                    NavigationHelper.GoToLoginScreen()
                    
                }
                popup.secondButtonCallback = {
                    popup.dismiss(animated: true)
                }
                IKNotificationManager.sharedInstance.show(view: popup)
                
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
