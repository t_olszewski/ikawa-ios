//
//  IKRoast.swift
//  IKAWApp
//
//  Created by Admin on 4/2/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import IKRoasterLib

class IKRoast: NSObject {

    var machineUuid: String?
    var profileUuid: String?
    var roastData: String?
    var uuid: String?
    var firstCrack: CGPoint?
    var secondCrack: CGPoint?
    var colorChange: CGPoint?
    var date: TimeInterval?
    
    var roastPoints = [RoasterStatus]()
    
    init(machineUuid: String!, profileUuid: String!) {
        self.date = Date().timeIntervalSince1970
        self.uuid = UUID().uuidString.replacingOccurrences(of: "-", with: "")
        self.machineUuid = machineUuid
        self.profileUuid = profileUuid
        super.init()
    }
    
    func lastRoastingPoint() -> RoastTempPoint {
        let resultPoint = RoastTempPoint()
        resultPoint.time = 0
        resultPoint.temperature = 0
        for point in roastPoints {
            if (point.state == IK_STATUS_ROASTING && point.time > resultPoint.time) {
                resultPoint.time = point.time
                resultPoint.temperature = point.temp_above
            }
        }
        return resultPoint
    }
    
}
