//
//  IKStatusBarView.swift
//  IKAWApp
//
//  Created by Admin on 3/7/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib
import SnapKit

    class IKStatusBarView: NibView {
    @IBOutlet weak var statusColorView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    
    static var previousState = IK_STATUS_UNKNOWN
    var currentState = IK_STATUS_UNKNOWN
    var canAutoUpdateProfile = false
    var isAnimating = false
    static var wasRoasterOpen = false
        

        
    //MARK: - Private
    
    override func awakeFromNib() {
        super.awakeFromNib()
        statusColorView.layer.masksToBounds = true
        statusColorView.layer.cornerRadius = statusColorView.frame.width / 2
        updateProgressBar()
    }
    
    func updateProgressBar() {
        statusColorView.backgroundColor = self.staus().color
        statusLabel.text = self.staus().status
    }
    
    func calculateState() -> RoasterState {
        var state = RoasterManager.instance().roaster.status.state
        if (state == IK_STATUS_IDLE && (IKStatusBarView.previousState == IK_STATUS_OPEN || IKStatusBarView.previousState == IK_STATUS_READY)) {
            state = IK_STATUS_READY
        }
        return state
    }

        
    func staus() -> (status: String?, color: UIColor?) {
        
        if (IKInterface.instance().isBluetoothEnabled() && RoasterManager.instance().roaster == nil) {
            return (NSLocalizedString("$not_connected_title", comment: ""), kColorGreyDark)
        }
        
        let progress = IKRoastManager.sharedInstance.roastProgress()
        if (IKInterface.instance().isBluetoothEnabled() && RoasterManager.instance().roaster != nil) {
            let state = calculateState()
            switch (state) {
            case IK_STATUS_BUSY:
                return (NSLocalizedString("$busy_title", comment: ""), kColorGreyDark)
            case IK_STATUS_COOLING:
                IKStatusBarView.wasRoasterOpen = false
                return (NSLocalizedString("$cooling_title", comment: ""), kColorBlue)
            case IK_STATUS_HEATING:
                IKStatusBarView.wasRoasterOpen = false
                return (NSLocalizedString("$preheating_title", comment: ""), kColorRed)
            case IK_STATUS_IDLE:
                return (NSLocalizedString("$idle_title", comment: ""), kColorGreen)
            case IK_STATUS_OPEN:
                IKStatusBarView.wasRoasterOpen = true

                if (progress > 0) {
                    return (NSLocalizedString("$doser_open_title", comment: ""), kColorGreyDark)
                } else {
                    return (NSLocalizedString("$doser_open_title", comment: ""), kColorGreyDark)
                }

            case IK_STATUS_PROBLEM:
                return (NSLocalizedString("UNEXPECTED PROBLEM", comment: ""), kColorGreyDark)
            case IK_STATUS_READY:
                    return (NSLocalizedString("$ready_title", comment: ""), kColorGreen)
            case IK_STATUS_ROASTING:
                IKStatusBarView.wasRoasterOpen = false
                return (NSLocalizedString("$roasting_title", comment: ""), kColorRed)
            case IK_STATUS_NEEDS_UPDATE:
                return (NSLocalizedString("$needs_update_title", comment: ""), kColorOrange)
            case IK_STATUS_SWAP_JARS:
                return (NSLocalizedString("$swap_jars_title", comment: ""), kColorGreen)
            case IK_STATUS_READY_TO_BLOWOVER:
                return (NSLocalizedString("$blowover_ready_title", comment: ""), kColorGreen)
            case IK_STATUS_TEST_MODE:
                 return (NSLocalizedString("$test_mode", comment: ""), kColorGreyDark)
            default:
                break
            }
        } else {
            return (NSLocalizedString("$bluetooth_off_title", comment: ""), kColorGreyDark)
        }
             return (nil, nil)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

extension IKStatusBarView : IKInterfaceDelegate {
    func peripheralDidConnect(_: IKPeripheral) {
        
    }
    
    func peripheralDidDisconnect(_ peripheral: IKPeripheral!) {
        
    }
    
    func peripheral(_ peripheral: IKPeripheral!, didReceive data: Data!) {
        
    }
    

}

extension IKStatusBarView : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {

    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {

    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {
        
    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {
        print(RoasterManager.instance().roaster.status.state)
        updateProgressBar()
        let state = calculateState()
        currentState = state
        
        if (currentState != IK_STATUS_UNKNOWN) {
            IKStatusBarView.previousState = state
        }
        
        if (IKInterface.instance().isBluetoothEnabled() && RoasterManager.instance().roaster != nil) {
            switch (state) {
            case IK_STATUS_READY_TO_BLOWOVER:
                if !isAnimating {
                    self.statusColorView.layer.removeAllAnimations()
                    self.statusColorView.backgroundColor = kColorGreen
                    let pulseAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.backgroundColor))
                    pulseAnimation.fromValue = kColorGreen.cgColor
                    pulseAnimation.toValue = kColorGreen.withAlphaComponent(0).cgColor
                    pulseAnimation.duration = 0.3
                    pulseAnimation.repeatCount = MAXFLOAT
                    pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    pulseAnimation.autoreverses = true
                    pulseAnimation.repeatCount = .greatestFiniteMagnitude
                    pulseAnimation.isRemovedOnCompletion = false
                    self.statusColorView.layer.add(pulseAnimation, forKey: "animateColor")
                    isAnimating = true
                }
                break
            default:
                self.statusColorView.layer.removeAllAnimations()
                self.statusColorView.layer.opacity = 1
                isAnimating = false
                break
            }
        }
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {
        

    }
    
}

