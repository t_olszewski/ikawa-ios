//
//  Modal.swift
//  ModalView
//

import Foundation

import UIKit
import SnapKit


class ModalView: NibView {
    var dismissCallback: (()->())?
    var backgroundView = UIView()
    var dialogView = UIView()
    
    func show(animated:Bool, dismissCallback: (()->())? = nil){
        self.dismissCallback = dismissCallback
        self.backgroundView.alpha = 0
        if var topController = UIApplication.shared.delegate?.window??.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.view.addSubview(self)
            self.snp.makeConstraints { (make) in
                make.edges.equalTo(topController.view).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            }
        }
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.backgroundView.alpha = 1.0
            })
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
                //self.dialogView.center  = self.center
            }, completion: { (completed) in
                
            })
        }else{
            self.backgroundView.alpha = 1.0
            //self.dialogView.center  = self.center
        }
    }
    
    func dismiss(animated:Bool){
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.backgroundView.alpha = 0
            }, completion: { (completed) in
                
            })
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
                //self.dialogView.center = CGPoint(x: self.center.x, y: self.frame.height + self.dialogView.frame.height/2)
            }, completion: { (completed) in
                self.removeFromSuperview()
            if let dismissCallback = self.dismissCallback {
                dismissCallback()
            }
            })
        }else{
            self.removeFromSuperview()
            if let dismissCallback = self.dismissCallback {
                dismissCallback()
            }

        }
        
    }
}




