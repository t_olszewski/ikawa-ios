//
//  YesNoMadalVIew.swift
//  IKAWA-Home
//
//  Created by Никита on 11/10/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

enum TwoButtonsModalViewLayout {
    case horisontal, vertical
}

class ButtonsPopupModalView: ModalView {
    
    @IBOutlet weak private var background: UIView!
    @IBOutlet weak private var dialog: UIView!
    
    @IBOutlet weak private var firstButton: UIButton!
    @IBOutlet weak private var secondButton: UIButton!
    
    @IBOutlet weak private var thirdButton: UIButton!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    @IBOutlet weak var stackView: UIStackView!
    
    typealias Callback = (() -> Void)
    
    var layout: TwoButtonsModalViewLayout = .horisontal
    var title: String = ""
    var subtitle: String = ""
    var firstButtonTitle: String = ""
    var secondButtonTitle: String = ""
    var thirdButtonTitle: String = ""
    
    
    var firstButtonCallback: Callback?
    var secondButtonCallback: Callback?
    var thirdButtonCallback: Callback?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundView = background
        dialogView = dialog
        
        dialog.layer.shadowColor = UIColor.black.cgColor
        dialog.layer.shadowOpacity = 0.5
        dialog.layer.shadowOffset = CGSize(width: 0, height: 1)
        dialog.layer.shadowRadius = 4

    }
    
    @IBAction func firstButtonTouchUpInside() {
        firstButtonCallback?()
    }
    
    @IBAction func secondButtonTouchUpInside(_ sender: Any) {
        secondButtonCallback?()
    }
    
    @IBAction func thirdButtonTouchUpInside(_ sender: Any) {
        thirdButtonCallback?()
    }
    
    override func show(animated: Bool, dismissCallback: (() -> ())?) {
        super.show(animated: animated, dismissCallback: dismissCallback)
        correctAppearance()
    }
    
    func correctAppearance() {
        switch layout {
        case .horisontal:
            self.stackView.axis = .horizontal
//            self.firstButton.backgroundColor = kColorRedLight
//            self.secondButton.backgroundColor = kColorRed
        case .vertical:
            self.stackView.axis = .vertical
//            self.firstButton.backgroundColor = kColorRed
//            self.secondButton.backgroundColor = kColorRedLight
        }
        if title.isEmpty {
            titleLabel.isHidden = true
        } else {
            titleLabel.isHidden = false
            titleLabel.attributedText = title.attributedString(font: UIFont(name: kFontAvenirNextLTProBold, size: 16) ?? UIFont.boldSystemFont(ofSize: 16), color: kColorGreyDark)
        }
        
        if subtitle.isEmpty {
            subtitleLabel.isHidden = true
        } else {
            subtitleLabel.isHidden = false
            subtitleLabel.attributedText = subtitle.attributedString(font: UIFont(name: kFontAvenirNextLTProRegular, size: 16) ?? UIFont.boldSystemFont(ofSize: 16), color: kColorGreyDark)
        }
        
        if firstButtonTitle.isEmpty {
            firstButton.isHidden = true
        } else {
            firstButton.isHidden = false
            let bold16 = UIFont(name: kFontAvenirNextLTProBold, size: 16) ?? UIFont.boldSystemFont(ofSize: 16)
            firstButton.setAttributedTitle(firstButtonTitle.attributedString(font: bold16, color: UIColor.white), for: .normal)
        }
        
        if secondButtonTitle.isEmpty {
            secondButton.isHidden = true
        } else {
            secondButton.isHidden = false
            let bold16 = UIFont(name: kFontAvenirNextLTProBold, size: 16) ?? UIFont.boldSystemFont(ofSize: 16)
            secondButton.setAttributedTitle(secondButtonTitle.attributedString(font: bold16, color: UIColor.white), for: .normal)
        }
        
        if thirdButtonTitle.isEmpty {
            thirdButton.isHidden = true
        } else {
            thirdButton.isHidden = false
            let bold16 = UIFont(name: kFontAvenirNextLTProBold, size: 16) ?? UIFont.boldSystemFont(ofSize: 16)
            thirdButton.setAttributedTitle(thirdButtonTitle.attributedString(font: bold16, color: UIColor.white), for: .normal)
        }
    }
}
