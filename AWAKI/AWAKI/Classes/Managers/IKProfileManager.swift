//
//  IKProfileManager.swift
//  IKAWApp
//
//  Created by Admin on 3/16/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import Toast_Swift
import IKRoasterLib




class IKProfileManager: NSObject {
    
    typealias Callback = (_ success: Bool) -> Void
    
    static let sharedInstance = IKProfileManager()
    
    
    override init() {
        super.init()
    }
    
 
    func base64EncodeProfile(profile: Profile) -> String? {
        guard let profileData = profile.convertToProtobuf() else {
            return nil
        }
        return profileData.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }
    
    func base64DecodeProfile(profile: String) -> Profile? {
        guard let data = Data(base64Encoded: profile) else { return nil }
        print("Profile %@", profile)
        return Profile.convert(fromProtobuf: data)
    }
    
    
    func sendProfile(profile: Profile?) {
        guard profile != nil else {
            return
        }
        if (!RoasterManager.instance().roaster.hasBelowSensor() && profile?.tempSensor == TempSensorBelow) {
            let confirmationDialogView = ButtonsPopupModalView.fromNib()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$profile_unsupported", comment: "")
            confirmationDialogView.subtitle = NSLocalizedString("$inlet_sensor_profile_error_message", comment: "")
            confirmationDialogView.firstButtonTitle = NSLocalizedString("$ok", comment: "")
            confirmationDialogView.secondButtonTitle = ""
            confirmationDialogView.firstButtonCallback = {
                confirmationDialogView.dismiss(animated: true)
            }
            confirmationDialogView.secondButtonCallback = {
                confirmationDialogView.dismiss(animated: true)
            }
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            return
        } else if (!RoasterManager.instance().roaster.hasAboveSensor() &&
            (profile?.tempSensor == TempSensorAbove || profile?.tempSensor == TempSensorAboveRobust)) {
            
            let confirmationDialogView = ButtonsPopupModalView.fromNib()
            confirmationDialogView.layout = .vertical
            confirmationDialogView.title = NSLocalizedString("$profile_unsupported", comment: "")
            confirmationDialogView.subtitle = ""
            confirmationDialogView.firstButtonTitle = NSLocalizedString("$ok", comment: "")
            confirmationDialogView.secondButtonTitle = ""
            confirmationDialogView.firstButtonCallback = {
                confirmationDialogView.dismiss(animated: true)
            }
            confirmationDialogView.secondButtonCallback = {
                confirmationDialogView.dismiss(animated: true)
            }
            IKNotificationManager.sharedInstance.show(view: confirmationDialogView)
            return
        }
        RoasterManager.instance().sendProfile(profile)
    }
 
}
