//
//  ExRoasterManager.swift
//  IKAWApp
//
//  Created by Admin on 3/7/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class IKRoastManager: NSObject {
    static let sharedInstance = IKRoastManager()
    var currentRoast :IKRoast?
    var lastRoast :IKRoast?
    var previousState :RoasterState?
    var isProfileUpdating = false
    
    override init() {
        super.init()
        RoasterManager.instance().delegates.add(self)
    }
    
    deinit {
        if (RoasterManager.instance().delegates.contains(self)) {
            RoasterManager.instance().delegates.remove(self)
        }
    }
    
    //MARK: - Public
    
    func roastAndCoolDownProgress() -> Float {
        if RoasterManager.instance().roaster == nil {
            return 0
        }
        if (IKRoastManager.sharedInstance.currentRoast?.roastPoints.count == 0) {
            return 0
        }
        
        guard let lastPoint = IKRoastManager.sharedInstance.currentRoast?.roastPoints.last else {
            return 0
        }
        
        return (RoasterManager.instance().profile.cooldownPoint == nil) ? 0 : (lastPoint.time) / Float(RoasterManager.instance().profile.cooldownPoint.time)
    }
    
    func roastProgress() -> Float {
        guard let lastPoint = IKRoastManager.sharedInstance.currentRoast?.roastPoints.last else {
            return 0
        }
        
        guard RoasterManager.instance().profile != nil, let profilePoints = RoasterManager.instance().profile.roastPoints, let lastProfilePoint = profilePoints.last as? RoastTempPoint else {
            return 0
        }
        
        return Float(lastPoint.time) / Float(lastProfilePoint.time)
    }
    
 
    
    //MARK: - Private
    
    func startNewRoast() {
        let profileOnRoaster = RoasterManager.instance().profile
        if (profileOnRoaster == nil) {
            return
        }
        profileOnRoaster?.dateLastRoasted = Date()
        let roaster = RoasterManager.instance().roaster
        currentRoast = IKRoast(machineUuid: String(roaster!.roasterId), profileUuid: profileOnRoaster?.uuid)
    }
    
    func isRoasting() -> Bool {
        return currentRoast != nil
    }
    
}

extension IKRoastManager : RoasterManagerDelegate {
    func roasterDidConnect(_ roaster: Roaster!) {
    }
    
    func roasterDidDisconnect(_ roaster: Roaster!) {
    }
    
    func roaster(_ roaster: Roaster!, didReceiveSetting setting: Int32, withValue value: NSNumber!, success: Bool) {

    }
    
    func roasterDidUpdateInfo(_ roaster: Roaster!) {

    }
    
    func roasterDidUpdateStatus(_ roaster: Roaster!) {

        // Check: Roasting is done?
        if (isRoasting() && roaster.status.state == IK_STATUS_IDLE) {
            
            lastRoast = currentRoast
            //currentRoast = nil
        }
        
        // Check Started roasting?
        let roasterState = RoasterManager.instance().roaster.status.state
        if ((roasterState == IK_STATUS_HEATING || roasterState == IK_STATUS_ROASTING || roasterState == IK_STATUS_COOLING) && (currentRoast == nil || previousState == IK_STATUS_IDLE)) {
            print("Roast started!")
            startNewRoast()
        }
        
        previousState = roasterState
        
        if let roast = currentRoast {
            if let roasterStatus = RoasterManager.instance().roaster.status {
                

                
                if (roast.roastPoints.count > 1 && roast.roastPoints[roast.roastPoints.index(of: roast.roastPoints.last!)! - 1].time > roast.roastPoints.last!.time) {
                    return
                }
                
                if (roasterStatus.time == 0 && roast.roastPoints.count > 0) {
                    roast.roastPoints[roast.roastPoints.count - 1] = roasterStatus
                } else {
                    roast.roastPoints.append(roasterStatus)
                }
                
                
            }
        }
        
    }
    
    func roaster(_ roaster: Roaster!, didUpdate profile: Profile!) {

    }
}
