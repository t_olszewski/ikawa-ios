//
//  IKAccountManager.swift
//  IKAWApp
//
//  Created by Admin on 3/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

final class IKFirebaseManager: NSObject {
    
    typealias RegisterCallback = (_ success: Bool, _ message: String?) -> Void
    typealias LoginCallback = (_ success: Bool, _ message: String?) -> Void
    typealias SendDataCallback = (_ success: Bool, _ message: String?) -> Void

    
    var currentUser: User?
    static let sharedInstance = IKFirebaseManager()

    func firebaseInit() {
        let filename = "GoogleService-Info"
        let filePath = Bundle.main.path(forResource: filename, ofType: "plist")
        let options = FirebaseOptions(contentsOfFile:filePath!)
        FirebaseApp.configure(options: options!)
        currentUser = Auth.auth().currentUser
        
        
        if (currentUser != nil) {
            print("User logged in")
            if (!currentUser!.isAnonymous) {
                if let username = currentUser!.email {
                    UserDefaults.standard.setValue(username, forKey: "username")
                }
            }
            
        } else {
            print("User not logged in")
            //log in anonymously
            self.firebaseLoginAnonimous(callback: { (success, message) in
                if (!success) {
                    print("Anonymous authentication failed")
                } else {
                    print("Anonymous authentication success")
                }
            })
        }
        Auth.auth().useAppLanguage()
    }
    
    
    func firebaseRegisterUser(email: String, password: String, callback: @escaping RegisterCallback) {
        Auth.auth().createUser(withEmail: email.lowercased(), password: password) { (authDataResult, error) in
            if (error != nil) {
                callback(false, error?.localizedDescription)
            } else if (authDataResult != nil) {
                authDataResult!.user.sendEmailVerification(completion: { (error) in
                    if (error == nil) {
                        callback(true, "")
                    } else {
                        callback(false, error?.localizedDescription)
                    }
                })
            }
        }
    }
    
    
    func firebaseLogin(email: String, password: String, callback: @escaping LoginCallback) {
        Auth.auth().signIn(withEmail: email.lowercased(), password: password) { (authDataResult, error) in
            if (error != nil) {
                print("Authentication failed")

                callback(false, error?.localizedDescription)
            } else if (authDataResult != nil) {
                if (authDataResult!.user.isEmailVerified) {
                    self.currentUser = authDataResult!.user
                    callback(true, "")
                    print("Authentication success")
                } else {
                    print("Authentication failed")
                    callback(false, NSLocalizedString("$email_not_verified", comment:""))
                        authDataResult!.user.sendEmailVerification(completion: nil)
                }
            }
        }
    }
    
    
    func firebaseLoginAnonimous(callback: @escaping LoginCallback) {
        Auth.auth().signInAnonymously { (authDataResult, error) in
            if (error != nil) {
                callback(false, error?.localizedDescription)
            } else if (authDataResult != nil) {
                self.currentUser = authDataResult!.user
                callback(true, "")
            }
        }
    }
    
    
    func firebaseResetPassword(email: String, callback: @escaping RegisterCallback) {
        Auth.auth().sendPasswordReset(withEmail: email.lowercased()) { (error) in
            if (error == nil) {
                callback(true, "")
            } else {
                callback(false, error?.localizedDescription)
            }
        }
    }
    
    
    func firebaseLogout() {
        do {
            try Auth.auth().signOut()
            currentUser = nil
        } catch let error {
            // handle error here
            print("Error trying to sign out of Firebase: \(error.localizedDescription)")
        }
    }


}
