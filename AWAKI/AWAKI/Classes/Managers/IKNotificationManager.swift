//
//  IKPushNotificationManager.swift
//  IKAWA-Home
//
//  Created by Admin on 11/12/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class IKNotificationManager: NSObject {
static let sharedInstance = IKNotificationManager()
    var modalViews: [ModalView] = []
    var showedView: ModalView?
    
    func show(view: ModalView) {
        if showedView == nil {
            showedView = view
            view.show(animated: true, dismissCallback: {
                let serialQueue = DispatchQueue(label: "com.ikawa")
                serialQueue.sync {
                    self.showedView = nil
                    if (self.modalViews.count > 0) {
                        self.show(view: self.modalViews.remove(at: 0))
                    }
                }
            })
        } else {
         modalViews.append(view)
        }
    }
    
}
