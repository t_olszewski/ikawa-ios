//
//  NavigationHelper.swift
//  IKAWA-Home
//
//  Created by Admin on 10/26/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

class NavigationHelper: NSObject {

    class func GoToLoginScreen() {
        IKInterface.instance()?.disconnect()
        IKInterface.instance()?.stopScan()
        let accountStoryboard = UIStoryboard(name: "Account", bundle: nil)
        let accountNC = accountStoryboard.instantiateInitialViewController()
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = accountNC
    }
    
    class func GoToMainScreen() {
        IKInterface.instance()?.connect()
        let accountStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let accountNC = accountStoryboard.instantiateInitialViewController()
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = accountNC
    }
    

    
}
