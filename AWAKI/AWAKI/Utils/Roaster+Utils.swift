//
//  Roaster+Utils.swift
//  IKAWApp
//
//  Created by Admin on 4/23/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import IKRoasterLib

extension Roaster {
    
    func hasBelowSensor() -> Bool {
        return (type.rawValue > RoasterTypeV2.rawValue && variant == RoasterVariantPRO) || variant == RoasterVariantHOME
    }

    func hasAboveSensor() -> Bool {
        return variant != RoasterVariantHOME
    }
    
}
