//
//  UIUtills.swift
//  IKAWApp
//
//  Created by Admin on 3/5/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

let kFontAvenirNextLTProRegular = "AvenirNextLTPro-Regular"
let kFontAvenirNextLTProBold = "AvenirNextLTPro-Bold"

let kColorGreyDark = UIColorFromRGB(89, g: 90, b: 87)
let kColorRedLight = UIColorFromRGB(255, g: 96, b: 72)
let kColorRed = UIColorFromRGB(223, g: 35, b: 29)
let kColorBlue = UIColorFromRGB(72, g: 171, b: 255)
let kColorGreen = UIColorFromRGB(105, g: 157, b: 71)
let kColorOrange = UIColorFromRGB(215, g: 158, b: 63)

func UIColorFromHexRGB(_ rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}


func UIColorFromRGB(_ r: Float, g: Float, b: Float) -> UIColor {
    return UIColor(red: CGFloat(r/255), green: CGFloat(g/255), blue: CGFloat(b/255), alpha: CGFloat(1.0))
}

func UIColorFromRGBA(_ r: Float, g: Float, b: Float, alpha: Float) -> UIColor {
    return UIColor(red: CGFloat(r/255), green: CGFloat(g/255), blue: CGFloat(b/255), alpha: CGFloat(alpha))
}




