//
//  UITextField+Utils.swift
//  IKAWA-Home
//
//  Created by Никита on 10/10/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func createBottomLine(color: UIColor = UIColor.darkGray) {
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
