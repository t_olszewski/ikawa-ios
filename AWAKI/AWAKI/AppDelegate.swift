//
//  AppDelegate.swift
//  AWAKI
//
//  Created by Admin on 12/19/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IKRoasterLib

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        #if !TARGET_DEV
        Thread.sleep(forTimeInterval: 5.0)
        #endif
       IKFirebaseManager.sharedInstance.firebaseInit()
        if let user = IKFirebaseManager.sharedInstance.currentUser {
            if user.isAnonymous || !user.isEmailVerified {
                NavigationHelper.GoToLoginScreen()
            } else {
                NavigationHelper.GoToMainScreen()
            }
        } else {
            NavigationHelper.GoToLoginScreen()
        }
        
//        let base64Str = "CAESEPSjnKCik0qynk+nEmVWY/YaF0tlbnlhIEFBIE1hZ2Fuam8gRmlsdGVyIgUIABD0AyIGCOQNEIQTIgYI3BAQvxQiBgjrEhCEEyIGCKYeENkSKgUIABDFASoGCKYeEKEBMAE6BgjTJRDMAUIKCgASABoAIgAoAQ=="
//        let webURL = "https://www.ninetypluscoffee.com/core-values/"
//        if let profile = IKProfileManager.sharedInstance.base64DecodeProfile(profile: base64Str) {
//            IKProfileManager.sharedInstance.base64EncodeProfile(profile: profile)
//            profile.coffeeWebUrl = webURL
//            print("profile=https://share.ikawa.support/profile_home/?" + IKProfileManager.sharedInstance.base64EncodeProfile(profile: profile)!)
//        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        IKInterface.instance()?.connect()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    



}

