
import Foundation

struct Configuration {

    static let apiServer: String = {
        if let result = infoDict["API_SERVER"] as? String {
            return result
        }
        return ""
    }()

    fileprivate static var infoDict: [String: Any] {
        get {
            if let dict = Bundle.main.infoDictionary {
                return dict
            } else {
                fatalError("Plist file not found")
            }
        }
    }

}
